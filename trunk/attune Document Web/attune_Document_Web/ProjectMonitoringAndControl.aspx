﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ProjectMonitoringAndControl.aspx.cs" Inherits="attune_Document_Web.ProjectMonitoringAndControl" %>

<asp:Content ID="projectMonitoringControlPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project Monitoring and Control Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>attune executes SAP developments, new technology software developments, enhancements and maintenance projects for its clients. The term “project” is used to describe the full set of activities from the time the proposal is accepted (or a contract is signed) to the time all the software and services are delivered according to the accepted SOW/ contract.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The scope of the Monitoring and Control is "To track project progress, schedule, risks, commitments, stakeholder involvement, dependencies, issues, corrective and preventive actions and metrics; to take corrective measures when any of the parameters deviate from the plan.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SOW  - </strong>Statement Of Work</p>
                                <p><strong>VPS - </strong>Vice President Services</p>
                                <p><strong>PC  - </strong>Process Consultant</p>
                                <p><strong>PM - </strong>Project Manager</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>QA - </strong>Quality Assurance</p>
                                <p><strong>RM - </strong>Risk Management</p>
                          </div>
                          <div class="col-xs-6 col-md-4">  
                                <p><strong>M&A  - </strong>Measurement and Analysis</p>
                                <p><strong>TM - </strong>Training Manager</p>
                                <p><strong>PAD - </strong>Project Approach Document</p>
                                <p><strong>PSTS - </strong>Project Status Tracking Sheet</p>
                          </div>

                        </div>

                        <div class="data">
                            <h3>Process and Steps</h3>     

                            <p>Once the project is planned, the PM monitors the progress against the plan to identify and resolve issues in a timely manner. For this, the PM gathers the information needed, analyzes it and identifies deviations that are significant. Corrective actions are identified for all issues and tracked to closure. Corrective actions may include re-planning and establishing new agreements. </p>

                            <p>When the project is completed, the PM gathers the project related data and documents. The PM prepares a Project Closure Report and hands over the project-related work-products, documents, records, and the Project Closure Report to the PC.</p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="14"><strong>Execute & Monitor</strong></td>
                                            <td><p>The PM executes the project using the PAD and the detailed schedule, and monitors the progress against these to identify deviations. The issues identified are documented in the Project Status Tracking Sheet so that timely corrective actions can be identified and taken.</p></td>
                                            <td rowspan="14">PM, TL, OM, PC</td>
                                            <td rowspan="5">Project Status Tracking Sheet,  Project Skill Tracking Sheet, Project Schedule, Time Shee</td>                                    
                                            <td rowspan="14">SOW, PAD</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>On a periodic basis, the PM updates the detailed schedule for the next period based on the current status and planned activities. Consistency is maintained between the detailed schedule and the PAD.</p> 
                                                <p>The PM identifies activities on the critical path and informs the team about these activities.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM ensures that project team members join the project as per the plan and the Team Requisition raised earlier.</p>
                                                <p>PM  briefs new project team members on:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Project scope</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Client</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Environment</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Team roles and responsibilities</p>

                                                <p>Training identified in the PAD is provided in coordination with the TM and included in the Training Plan. Project Skills Tracker is updated as people join the project and gain skills through training or on-the-job experience.</p>
                                                <p>(Form: Project Skills Tracker, Training Plan)</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM allocates tasks to the project team members and update detailed schedule.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Gather information on the progress from team members using the reporting mechanism specified in the PAD at the defined periodicity. This typically includes timesheets and the delivery schedule. The PM discusses the status of other activities as required with the project team members to obtain the data required to monitor and track the project progress. The overall status of the project is reviewed in team meetings and prepared to report weekly basis.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM compares actual progress with the Project Status Tracking Sheet (PSTS) - milestones and the detailed schedule to check for variances. This comparison is performed for:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Completion of activities and achievement of milestones against plan </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Actual against planned effort </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Availability of resources against planned requirements (such as people, hardware, software, work environment)</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Resource utilization against planned utilization </p>

                                                <p>The PM analyses the variances to understand their reasons and impact on the project. Significant variances or other issues that may need corrective actions are tracked in PSTS. </p></td>
                                            <%--<td></td>--%>
                                            <td rowspan="3">PSTS</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM collects defect data from work-product reviews, unit testing, software integration, sub-system testing, system testing and acceptance testing to update the Operations status Report </p>
                                                <p>The PM analyses the defect related data to understand impact on the project. Significant issues that may need corrective actions are tracked with respective action items.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM reviews the Project's Process and the tailoring done to identify changes to the Project's Process. Actions identified during this review are noted in the PSTS.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM updates the Project Skills Tracker and uses it to monitor the knowledge and skills of the team members. </p></td>
                                            <%--<td></td>--%>
                                            <td>Skill assessment sheet</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM reviews the status against the Project Status Tracking Sheet – Milestones & Schedule to ensure monitoring and control of all project activities. </p>
                                                <p>The PM also reviews status against all project-related plans and documents </p>
                                                <p>The significant issues and their impact are documented in the Risks, issues & etc.</p></td>
                                            <%--<td></td>--%>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM consolidates the project progress, performance and issues. Issues identified are noted in the tracking sheet.</p></td>
                                            <%--<td></td>--%>
                                            <td>PSTS</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Formal milestone reviews are conducted as planned in the PAD. The PM, project team, practice head and RD participate in phase-end/ milestone review. The following activities are performed at this review:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Effort is estimated based on the re-estimated or changed scope till date</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The schedule for future activities is recalculated</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Risks are revisited</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Defect data are analyzed to identify common defects and corrective actions</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Lessons learnt during project execution are identified  </p>
                                                <p>The PAD and/or Project’s Process may be amended and reviewed as when required.</p></td>
                                            <%--<td></td>--%>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM sends the report of milestone/ phase-end review along with contributions to Practice Head
                                                <p>Issues identified are noted in the Issues Tracking Register.</p></td>
                                            <%--<td></td>--%>
                                            <td>Project Closure report</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM reviews the PSTS and the Project plans to identify the needs of any additional communication to the relevant stakeholders and communicate the same.</p></td>
                                            <%--<td></td>--%>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="7"><strong>Manage Corrective Actions to Closure</strong></td>
                                            <td><p>The PM executes the project using the PAD and the detailed schedule, and monitors the progress against these to identify deviations. The issues identified are documented in the Project Status Tracking Sheet so that timely corrective actions can be identified and taken.</p></td>
                                            <td rowspan="7">PM, GM</td>
                                            <td rowspan="4">PSTS,  Skill Tracker, Training Plan </td>                                    
                                            <td rowspan="7">PAD, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>PM examines the tracking sheets to identify issues where corrective actions have not yet been defined. </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM identifies possible corrective actions to ensure that the project stays on track. </p>
                                                <p>This may include:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Re-training team members/ acquiring team members with different skills</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Considering use of tools/ templates</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Changing the Project's Process</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Changing the PAD and other project plans</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Changing the detailed schedule because of rescheduling some activities, and adding or deleting others</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>In case identified corrective actions impact the commitments made to the client, the PM discusses these with the RD before they are discussed with or communicated to the client. </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>For identified corrective actions, the PM tracks the actions to closure. </p></td>
                                            <%--<td></td>--%>
                                            <td rowspan="3">PSTS</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM analyses corrective actions to determine their effectiveness. Results of corrective actions are documented and analyzed with respect to the planned results of the corrective actions.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM gathers the project related data and documents other than those already baselined.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="7"><strong>Close Project</strong></td>
                                            <td><p>The PM collects feedback from internal and external stakeholders with in the project as required by the Project Closure Report.</p>
                                                <p>(Template: Project Closure Report )</p></td>
                                            <td rowspan="7">PM, GM, PC</td>
                                            <td rowspan="7">PSTS, Project Closure Report  </td>                                    
                                            <td rowspan="7">PAD, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM, along with the team prepares the Project Closure Report.</p>
                                                <p>(Template: Project Closure Report)</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM ensures that project end backups are taken. Generally it happens through organizational backup procedure.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM submits the Project Closure Report to the Practice Head.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The SVP studies the Project Closure report and decides whether a project end de-briefing is required. If so, the SVP arranges and coordinates the project end de-briefing that is attended by the PM, PC, Analysts/ Designers, etc. </p>
                                                <p>The meeting is recorded and these minutes are attached to the Project Closure Report.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM hands over project related work-products, documents, records, and the Project Closure Report to the Practice Head. This is done in terms of keeping all updated documents in ONEattune.com / VSS</p>
                                                <p>Refer : attune Process Management Process.docx</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM and the team are considered de-allocated from the project which happens automatically in the current process, and there will not be a formal de-allocation note or notification</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Successful completion of project high level plan</p> 
                                            <p>•	Project started as per contract/ proposal</p>

                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Project Closure Report and other project material handed over to the VPS</p>
                                            <p>•	Closure and verification of all issues in the Review Record </p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	PAD / Project Charter</p>
                                            <p>•	Project Management Related Plans</p>
                                            <p>•	Detailed Project Schedule</p>
                                            <p>•	Project Status Tracking Sheet (Risks, Issues, Decisions, Dependencies, Milestone, CR logs)</p>
                                            <p>•	Minutes of Meetings</p>
                                            <p>•	Time entry on PSA system</p>
                                            <p>•	All project data including plans, status reports and working papers, proposals, client meeting minutes</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	Minutes of Meeting from Internal Discussions, Client Discussions, etc.</p>
                                            <p>•	Detailed Project Schedule</p>
                                            <p>•	Time Sheets</p>
                                            <p>•	Records of phase-end / sprint / milestone reviews</p>
                                            <p>•	Project Skill Tracker</p>
                                            <p>•	Project Status Tracking Sheet (Risks, Issues, Decisions, Dependencies, Milestone, CR logs)</p>
                                            <p>•	Project Closure Report (with attached minutes of project de-briefing)</p>

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Project Closure Report and other project material handed over to the VPS </p>
                                            <p>•	Closure and verification of all issues in the Review Record </p>

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The Team Member’s Weekly Progress Report may not be collected in projects with less than 5 members, provided a weekly team meeting is held with all team members.</p>
                                            <p>•	There may be no phase-end/ milestone reviews for projects of less than 2 month duration or for projects with less than 3 team members</p>
                                            <p>•	For long-duration maintenance projects, the milestone reviews or sprint reviews may be scheduled on time-duration basis (e.g., quarterly) instead of phase ends.</p>
                                            <p>•	For projects of less than three months duration, the Project Status Report needs to be prepared every two weeks.</p>
                                            <p>•	Project’s Closure activities will not be applicable for projects that are running on a continuous basis, where project management and direction is decided by customer</p> 
                                        </div>
                                    </td>
                                    </tr>    
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Project Monitoring And Control Process.pdf">Download Project Monitoring And Control Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>                                                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\Project Skills Tracker.xlsx">1. Project Skill Tracker</a></p> 
                                    <p><a href="References\Project Status Tracking Sheet.xlsm">2. Project Status Tracking Sheet</a></p>                                     
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>       
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                                                                                                               

<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>


