﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="TrainingProjectLeaders.aspx.cs" Inherits="attune_Document_Web.Training1" %>

<asp:Content ID="TrainingProjectLeadersPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>aIR 2.0 Introduction for Project Leaders</h1>

                        <h3>Presentations</h3>
                        <div class="data">
                                <p class="tab-content" style="padding-left: 20px"><a href="References\aIR 2_0 Introduction and Overview for Project Leaders.pptx">1.  aIR 2.0 Introduction and Overview for Project Leaders</a></p>  
                        </div>

                        <h3>Videos</h3>
                        <div class="data">
                                <p class="tab-content" style="padding-left: 20px"><a href="https://life.oneattune.com/mlink/file/NTI3NzU" target="_blank">1.  aIR 2.0 Introduction for Project Leaders - Recording</a></p>  
<%--                                <p><a href="https://life.oneattune.com/mlink/file/MzQ2MzY" target="_blank">1.  attune PMO Project Governance</a></p> --%>

                        </div>

        <!-- /#page-content-wrapper -->

</asp:Content>




