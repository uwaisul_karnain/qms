﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Samples.aspx.cs" Inherits="attune_Document_Web.Samples" %>


<asp:Content ID="sampleContent" ContentPlaceHolderID="MainContentPageSection" runat="server">
    
    <!-- Page Content -->
        <h1>Samples</h1>

        <div class="row">
                <div class="col-xs-6 col-md-4">
                    <h3>FRS/Agile attune Implementation Roadmap</h3>    

                    <h2>Project Preparation</h2>
                            <p><a href="References\BRS -Action items Log Sample.xlsx">1.  BRS - Action items Log Sample</a></p>
                            <p><a href="References\Business Process Tracker - BRS Sample.xls">2. Business Process Tracker - BRS Sample</a></p>
                            <p><a href="References\BRS - GAP List Sample.xlsx">3.  BRS - GAP List Sample</a></p>   
                            <p><a href="References\BRS Meeting Minutes - OC08 Order Allocation Sample.docx">4.  BRS Meeting Minutes - OC08 Order Allocation Sample</a></p>
                            
                    <p><a href="References\BRS Walkthrough Presentation Day 1 Sample.pptx">5.  BRS Walkthrough Presentation Day 1 Sample</a></p>
                            <p><a href="References\Onboarding Guide Sample.docx">6.  Onboarding Guide Sample</a></p> 
                            <p><a href="References\Project Organizational Chart Sample.pptx">7.  Project Organizational Chart Sample</a></p> 
                            <p><a href="References\WorkShop Calendar - FRS Sample.xlsx">8.  WorkShop Calendar - FRS Sample</a></p>
                           
                            
                    <h2>Building the Future Reference Solution</h2>
                            <p><a href="References\Convergence Divergence Worksheet Long form.xlsx">1.  Convergence Divergence Worksheet Long form</a></p>
                            <p><a href="References\Convergence Divergence Worksheet Sample.xlsx">2.  Convergence Divergence Worksheet Sample</a></p>
                            <p><a href="References\E2E Flow HighLevel Overview Sample.vsd">3.  E2E Flow HighLevel Overview Sample</a></p>
                            <%--<p><a href="References\FRS-Proc Ref Doc PP12 Segmentation Sample.docx">4.  FRS-Proc Ref Doc PP12 Segmentation Sample</a></p>--%>
                            <p><a href="References\FRSBusinessProcessReferenceDocumentS.docx">4.  FRS Business Process Reference Document</a></p>
                            <p><a href="References\FRS PP12 Segmentation Presentation V01.2.pptx">5.  FRS PP12 Segmentation Presentation</a></p>
                            <p><a href="References\Sprint Backlog - Template with Sample Data.xlsx">6.  Sprint Backlog - Template with Sample Data</a></p>
                            
                    <h2>Agile Build and Unit Test</h2>
                            <p><a href="References\IST1 Sprint Plan Sample.xlsx">1.  Integration Test Script</a></p>
                            <p><a href="References\Sample FS Conversions.docx">2. Sample FS Conversions</a></p> 
                            <p><a href="References\Sample FS Enhancements.docx">3. Sample FS Enhancements</a></p>
                            <p><a href="References\Sample FS Interface.docx">4. Sample FS Interface</a></p> 
                            <p><a href="References\Sample FS Reports.docx">5. Sample FS Reports</a></p>
                            <p><a href="References\Sample RICEFW Development Process.pptx">6. Sample RICEFW Development Process</a></p>        

                    <h2>Final Preparation and Cutover</h2> 

                            <p><a href="References\Live Cutover Calendar Views Sample.pptx">1. Live Cutover Calendar Views Sample</a></p>
                                                                         
                </div>
            <div class="col-xs-6 col-md-4">
                    <h3>Traditional attune Implementation Roadmap</h3>    

                    <h2>Project Preparation</h2>   

                            <p><a href="References\Onboarding Guide Sample.docx">1.  Onboarding Guide Sample</a></p> 
                            <p><a href="References\Project Organizational Chart Sample.pptx">2.  Project Organizational Chart Sample</a></p> 

                    <h2>Business Blueprinting</h2>   

                            <p><a href="References\FRSBusinessProcessReferenceDocumentS.docx">1.   Business Process Document</a></p> 

                    <h2>Realization-Build</h2> 

                            <p><a href="References\Sample FS Conversions.docx">1. Sample FS Conversions</a></p> 
                            <p><a href="References\Sample FS Enhancements.docx">2. Sample FS Enhancements</a></p>
                            <p><a href="References\Sample FS Interface.docx">3. Sample FS Interface</a></p> 
                            <p><a href="References\Sample FS Reports.docx">4. Sample FS Reports</a></p>
                            <p><a href="References\Sample RICEFW Development Process.pptx">5. Sample RICEFW Development Process</a></p>        

                    <h2>Final Preparation</h2> 

                            <p><a href="References\Live Cutover Calendar Views Sample.pptx">1. Live Cutover Calendar Views Sample</a></p>
                                                                         
                </div>

        </div>     
    <!-- /#page-content-wrapper -->
</asp:Content>

