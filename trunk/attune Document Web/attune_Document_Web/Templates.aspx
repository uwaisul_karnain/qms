﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Templates.aspx.cs" Inherits="attune_Document_Web.Templates" %>

<asp:Content ID="templateContent" ContentPlaceHolderID="MainContentPageSection" runat="server">
    
    <!-- Page Content -->
        <h1>Templates</h1>

        <div class="row">
                <div class="col-xs-6 col-md-4">
                    <h3>FRS/Agile attune Implementation Roadmap</h3>    

                            <p><a href="References\Weekly Project Status Report - Template Sample and Instructions.xlsm">Project Status Report</a></p>

                    <h2>Project Preparation</h2>   

                            <p><a href="References\ABAP Development Guidelines.docx">1.  ABAP Development Guidelines</a></p>
                            <p><a href="#">2. Analytics Development Standards</a></p>  
                            <p><a href="References\Business Transformation Strategy.docx">3. Business Transformation Strategy</a></p>
                            <p><a href="#">4. Change Impact Assessment Strategy</a></p>  
                            <p><a href="References\Change Management Strategy.docx">5. Change Management Strategy</a></p> 
                            <p><a href="References\Coding Standards Java.doc">6.  Coding Standards Java</a></p>    
                            <p><a href="References\Coding Standards Microsoft.Net.docx">7.  Coding Standards Microsoft.Net</a></p>  
                            <p><a href="References\Communication Strategy.docx">8. Communication Strategy</a></p>
                            <p><a href="#">9. Data Migration and Cutover Strategy</a></p>  
                            <p><a href="References\End User Training Strategy.docx">10. End User Training Strategy</a></p> 
                            <p><a href="References\Executive_Alignment_Strategy.docx">11. Executive Alignment Strategy</a></p>
                            <p><a href="References\Executive Alignment Workshop.pptx">12. Executive Alignment Workshop</a></p>
                            <p><a href="#">13. FRS Workshop Tracker</a></p>
                            <p><a href="#">14. FRS Workshop Schedule</a></p>
                            <p><a href="References\Fundamentals of Change Mgmt for Key Stakeholders.pptx">15. Fundamentals of Change Management for Key Stakeholders</a></p>
                            <p><a href="#">16. High-level Timeline</a></p>
                            <p><a href="References\Initial 30 Day Communication Plan.docx">17. Initial 30 Day Communication Plan</a></p> 
                            <p><a href="References\Initial Business Transformation Readiness Assessment Approach.docx">18. Initial Business Transformation Readiness Assessment Approach</a></p>    
                            <p><a href="References\Initial Business Transformation Readiness Report.pptx">19. Initial Business Transformation Readiness Report</a></p>  
                            <p><a href="References\System Integration Landscape.vsd">20. Integration Landscape Diagram</a></p>
                            <p><a href="References\Issue Management Process.docx">21. Issue Management Process</a></p>
                            <p><a href="#">22. Knowledge Transfer Strategy Leading Change for Leaders Workshop Materials</a></p>
                            <p><a href="References\Master Project Schedule.mpp">23. Master Project Schedule</a></p>
                            <p><a href="References\Meeting Minutes.docx">24. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">25. Milestone Report</a></p>
                            <p><a href="References\Onboarding Guide.docx">26. Onboarding Guide</a></p> 
                            <p><a href="References\Project Charter.docx">27.  Project Charter</a></p>
                            <p><a href="References\aIRProjectGovernanceOverviewT.pptx">28.  Project Governance</a></p>
                            <p><a href="References\SAPImplementationProjectGoverningBodiesT.pptx">29.  Project Governing Bodies</a></p>
                            <p><a href="References\Project Organizational Chart.pptx">30.  Project Organizational Chart</a></p>
                            <p><a href="References\Project Status Tracking Sheet.xlsm">31.  Project Status Tracking Sheet</a></p>
                            <p><a href="References\Project Team Training Strategy.docx">32. Project Team Training Strategy</a></p>
                            <p><a href="#">33.  Resource Plan</a></p>
                            <p><a href="References\Risk Management Plan.docx">34.  Risk Management Plan</a></p>
                            <p><a href="References\SAP System Landscape Stratergy.docx">35.  SAP System Landscape Strategy</a></p>
                            <p><a href="References\ScopeManagement&ChangeRequestProcessT.docx">36.  Scope Management & Change Request Process</a></p>
                            <p><a href="#">37.  Security Strategy</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">38. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">39. Stage Gate Review</a></p>
                            <p><a href="References\Stakeholder Identification with Grid Mapping.xlsx">40. Stakeholder Identification with Grid Mapping</a></p>
                            <p><a href="#">41. Support Strategy</a></p>  
                            <p><a href="References\Testing Strategy.docx">42. Testing Strategy</a> </p>
   
                    
                    <h2>Building the Future Reference Solution</h2>    

                            
                            <p><a href="References\Change_Champion_Change_Agent_Strategy.docx">1. Change Champion Change Agent Strategy</a></p> 
                            <p><a href="References\Change_Impact_Assessment.docx">2. Change Impact Assessment</a></p> 
                            <%--<p><a href="References\Change Impact Assessment Strategy.docx">4. Change Impact Assessment Strategy</a></p>--%> 
                            <p><a href="References\Change Impact Assessment Workbook.xlsx">3. Change Impact Assessment Workbook</a></p> 
                            <%--<p><a href="References\Defect Management Process.docx">6. Defect Management Process</a></p>--%>
                           <%-- <p><a href="References\Detailed Build Schedule.mpp">7. Sprint Plan</a></p>--%>
                            <%--<p><a href="References\End User Training Curriculum With Calendar Option.xlsx">9. End User Training Curriculum With Calendar Option</a></p>--%>
                            <p><a href="References\attune BPD Template.docx">4. FRS Business Process Reference Document</a></p>
                            <p><a href="#">5. FRS Walkthrough Presentation</a></p>
                            <p><a href="#">6. FRS Workshop Schedule</a></p> 
                            <p><a href="References\GAP List.xlsx">7. GAP List</a></p>
                            <p><a href="References\Integrated Communication Plan.xlsx">8. Integrated Communication Plan</a></p> 
                            <p><a href="References\Integration Test Script Development Tracker.xlsx">9. Integration Test Script Development Tracker</a></p>
                            <p><a href="References\Key Stakeholder Leadership Action Plan.docx">10. Key Stakeholder Leadership Action Plan</a></p> 
                            <p><a href="References\Knowledge Transfer Tracking.xlsx">11. Knowledge Transfer Tracking</a></p> 
                            <p><a href="#">12. Meeting Minutes of FRS Workshop</a></p>
                            <p><a href="References\Milestone Report.xlsx">13. Milestone Report</a></p>
                            <p><a href="References\Backlog-Sprint-Plan-Template.xlsx">14. Product Backlog</a></p>
                            <%--<p><a href="References\RICEFW List.xlsx">17. RICEFW Objects List</a></p>--%>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">15. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Backlog-Sprint-Plan-Template.xlsx">16. Sprint Plan</a></p>
                            <p><a href="References\Stage Gate Review.pptx">17. Stage Gate Review</a></p>
                            <p><a href="#">18. Steering Committee Meeting Update</a></p>
                            <%--<p><a href="References\Training Development and Standards.docx">20. Training Development and Standards</a></p>--%>
                            <p><a href="#">19. Weekly PMO Meeting Update</a></p>
                            <p><a href="#">20. Weekly Team Leads Meeting Update</a></p>  

                    <h2>Agile Build and Unit Test</h2>
                     
                            <p><a href="#">1. Agile Build Kick-off Presentation</a></p>
                            <p><a href="References\Business_Role_Mapping.docx">2. Business Role Mapping</a></p>
                            <p><a href="References\Communication Effectiveness Survey.xlsx">3. Communication Effectiveness Survey</a></p>
                            <p><a href="#">4. Data Cleansing and Validation Procedures</a></p>
                            <p><a href="#">5. Defect Management Process</a></p>
                            <p><a href="#">6. End User Training Curriculum With Calendar Option</a></p>
                            <p><a href="References\End User ILT Course.pptx">7. End User ILT Course</a></p>
                            <p><a href="References\End User Training Course Outline.docx">8. End User Training Course Outline</a></p>
                            <p><a href="References\End User Training Curriculum.xlsx">9. End User Training Curriculum</a></p>
                            <p><a href="#">10. End User Training Development Processes, Standards, and Templates</a></p>  
                            <p><a href="References\FS template Conversion.docx">11. FS template Conversion</a></p> 
                            <p><a href="References\FS template Enhancements.docx">12. FS template Enhancements</a></p>
                            <p><a href="References\FS template Interface.docx">13. FS template Interface</a></p> 
                            <p><a href="References\FS template Reports & Forms.docx">14. FS template Reports & Forms</a></p>    
                            <p><a href="References\Test Scenario Description.docx">15. Integration Test Scenarios</a></p>
                            <p><a href="References\Integration Test Script.xlsx">16. Integration Test Script</a></p>
                            <p><a href="References\Testing Daily Schedule_Single Instance per Test.xlsx">17. Integration Testing Schedule</a></p>
                            <p><a href="References\A Day in the Life of a Tester.pptx">18. Integration Testing Training Materials</a></p>
                            <p><a href="#">19. Integration Tester Training Schedule</a></p>
                            <p><a href="#">20. Integration Testing Script Development Tracker</a></p>
                            <p><a href="#">21. Integration Testing Status Report</a></p>
                            <p><a href="References\Meeting Minutes.docx">22. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">23. Milestone Report</a></p>
                            <p><a href="#">24. Mock 0 Schedule</a></p>
                            <p><a href="#">25. Mock 1 Cutover Schedule</a></p>
                            <p><a href="#">26. Presentation</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">27. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="#">28. Sprint Retrospective document</a></p>
                            <p><a href="#">29. Sprint Plan</a></p>
                            <p><a href="References\Stage Gate Review.pptx">30. Stage Gate Review</a></p>
                            <p><a href="#">31. User Stories</a></p>
                            <p><a href="#">32. User Story Acceptance Test Results</a></p>

                    <h2>Testing</h2> 

                            <p><a href="References\Batch Schedule.xlsx">1. Batch Job Schedule</a></p>
                            <p><a href="#">2. Business Continuity Plan</a></p>    
                            <p><a href="References\Communication Effectiveness Survey.xlsx">3. Communication Effectiveness Survey</a></p>
                            <p><a href="#">4. Controlled Resumption Scripts</a></p>
                            <p><a href="#">5. End User Solution Showcase schedule</a></p>
                            <p><a href="#">6. End User Training Materials</a></p>
                            <p><a href="#">7. End User Training Schedule</a></p>
                            <%--<p><a href="References\End User Training Curriculum With Calendar Option.xlsx">3. End User Training Curriculum With Calendar Option</a></p>--%>
                            <p><a href="References\Live Cutover Plan.docx">8. Live Cutover Plan</a></p>
                            <p><a href="#">9. Live Cutover Schedule</a></p>
                            <p><a href="References\Meeting Minutes.docx">10. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">11. Milestone Report</a></p>
                            <p><a href="#">12. Performance/Stress Test scripts</a></p>
                            <p><a href="#">13. Post-Go-Live Support Plan</a></p>
                            <p><a href="#">14. Post-Go-Live Support Procedures</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">15. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">16. Stage Gate Review</a></p>
                            <p><a href="References\Train the Trainer Part 1 Program Overview.docx">17. Train the Trainer Part 1 Program Overview</a></p>                         

                    <h2>Final Preparation and Cutover</h2> 

                            <p><a href="#">1. Controlled Resumption Scripts</a></p>
                            <p><a href="References\Controlled Resumption Scripts.xlsx">2. Controlled Resumption Scripts</a></p>
                            <p><a href="References\Go-live Readiness Checklist.xlsx">3. Go-live Readiness Checklist</a></p>
                            <p><a href="#">4. Go/No-Go Decision Dashboard</a></p>
                            <p><a href="References\Meeting Minutes.docx">5. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">6. Milestone Report</a></p>
                            <p><a href="#">7. Post Go-live Support Coverage Schedule</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">8. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">9. Stage Gate Review</a></p>
                            <p><a href="References\Training Evaluation.xlsx">10. Training Evaluation</a></p>
                            <p><a href="References\User Auth  SAP ABAP Projects.xlsx">11. User ID List and Authorization Matrix</a></p> 

                    <h2>Post-Go Live</h2> 

                            <p><a href="#">1. Daily Post-Go-Live Support Status Communications</a></p>
                            <p><a href="References\Lessons Learned User Focus Group.docx">2. Lessons Learned User Focus Group</a></p>
                            <p><a href="References\Meeting Minutes.docx">3. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">4. Milestone Report</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">5. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">6. Stage Gate Review</a></p>
                                                                         
                </div>
                <div class="col-xs-6 col-md-4"> 
                    <h3>Traditional attune Implementation Roadmap</h3>    

                            <p><a href="References\Weekly Project Status Report - Template Sample and Instructions.xlsm">Project Status Report</a></p>

                    <h2>Project Preparation</h2>   

                            <p><a href="References\ABAP Development Guidelines.docx">1.  ABAP Development Guidelines</a></p> 
                            <p><a href="References\Blueprint Tracker.xls">2. Blueprint Tracker</a></p> 
                            <p><a href="References\Business Blueprint Workshop Schedule.xlsx">3. Business Blueprint Workshop Schedule</a></p> 
                            <p><a href="References\Business Transformation Strategy.docx">4. Business Transformation Strategy</a></p> 
                            <p><a href="References\Change Management Strategy.docx">5. Change Management Strategy</a></p> 
                            <p><a href="References\Coding Standards Java.doc">6.  Coding Standards Java</a></p>    
                            <p><a href="References\Coding Standards Microsoft.Net.docx">7.  Coding Standards Microsoft.Net</a></p>  
                            <p><a href="References\Communication Strategy.docx">8. Communication Strategy</a></p> 
                            <p><a href="References\End User Training Strategy.docx">9. End User Training Strategy</a></p> 
                            <p><a href="References\Executive_Alignment_Strategy.docx">10. Executive Alignment Strategy</a></p>
                            <p><a href="References\Executive Alignment Workshop.pptx">11. Executive Alignment Workshop</a></p>
                            <p><a href="References\Fundamentals of Change Mgmt for Key Stakeholders.pptx">12. Fundamentals of Change Management for Key Stakeholders</a></p>
                            <p><a href="References\Initial 30 Day Communication Plan.docx">13. Initial 30 Day Communication Plan</a></p> 
                            <p><a href="References\Initial Business Transformation Readiness Assessment Approach.docx">14. Initial Business Transformation Readiness Assessment Approach</a></p>    
                            <p><a href="References\Initial Business Transformation Readiness Report.pptx">15. Initial Business Transformation Readiness Report</a></p>  
                            <p><a href="References\System Integration Landscape.vsd">16. Integration Landscape Diagram</a></p>
                            <p><a href="References\Issue Management Process.docx">17. Issue Management Process</a></p>
                            <p><a href="References\Master Project Schedule.mpp">18. Master Project Schedule</a></p>
                            <p><a href="References\Meeting Minutes.docx">19. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">20. Milestone Report</a></p>
                            <p><a href="References\Onboarding Guide.docx">21. Onboarding Guide</a></p> 
                            <p><a href="References\Project Charter.docx">22.  Project Charter</a></p>
                            <p><a href="References\aIRProjectGovernanceOverviewT.pptx">23.  Project Governance</a></p>
                            <p><a href="References\SAPImplementationProjectGoverningBodiesT.pptx">24.  Project Governing Bodies</a></p>
                            <p><a href="References\Project Organizational Chart.pptx">25.  Project Organizational Chart</a></p>
                            <p><a href="References\Project Status Tracking Sheet.xlsm">26.  Project Status Tracking Sheet</a></p>
                            <p><a href="References\Project Team Training Strategy.docx">27. Project Team Training Strategy</a></p>
                            <p><a href="References\Risk Management Plan.docx">28.  Risk Management Plan</a></p>
                            <p><a href="References\SAP System Landscape Stratergy.docx">29.  SAP System Landscape Stratergy</a></p>
                            <p><a href="References\Scope Management & Change Request Process.docx">30.  Scope Management & Change Request Process</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">31. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">32. Stage Gate Review</a></p>
                            <p><a href="References\Stakeholder Identification with Grid Mapping.xlsx">33. Stakeholder Identification with Grid Mapping</a></p> 
                            <p><a href="References\Testing Strategy.docx">34. Testing Strategy</a></p>
   
                    
                    <h2>Business Blueprinting</h2>    

                            <p><a href="References\Business Blueprint Workshop Schedule.xlsx">1. Business Blueprint Workshop Schedule</a></p>
                            <p><a href="References\attune BPD Template.docx">2. Business Process Document</a></p>
                            <p><a href="References\Change_Champion_Change_Agent_Strategy.docx">3. Change Champion Change Agent Strategy</a></p> 
                            <p><a href="References\Change_Impact_Assessment.docx">4. Change Impact Assessment</a></p> 
                            <p><a href="References\Change Impact Assessment Strategy.docx">5. Change Impact Assessment Strategy</a></p> 
                            <p><a href="References\Change Impact Assessment Workbook.xlsx">6. Change Impact Assessment Workbook</a></p> 
                            <p><a href="References\Defect Management Process.docx">7. Defect Management Process</a></p>
                            <p><a href="References\Detailed Build Schedule.mpp">8. Detailed Build Schedule</a></p>
                            <p><a href="References\Detailed Build Schedule Guidelines.docx">9. Detailed Build Schedule Guidelines</a></p>
                            <p><a href="References\End User Training Curriculum With Calendar Option.xlsx">10. End User Training Curriculum With Calendar Option</a></p> 
                            <p><a href="References\GAP List.xlsx">11. GAP List</a></p>
                            <p><a href="References\Integrated Communication Plan.xlsx">12. Integrated Communication Plan</a></p> 
                            <p><a href="References\Integration Test Script Development Tracker.xlsx">13. Integration Test Script Development Tracker</a></p>
                            <p><a href="References\Key Stakeholder Leadership Action Plan.docx">14. Key Stakeholder Leadership Action Plan</a></p> 
                            <p><a href="References\Knowledge Transfer Tracking.xlsx">15. Knowledge Transfer Tracking</a></p> 
                            <p><a href="References\Meeting Minutes.docx">16. Meeting Minutes of Business Blueprint Workshop</a></p>
                            <p><a href="References\Milestone Report.xlsx">17. Milestone Report</a></p>
                            <p><a href="References\RICEFW List.xlsx">18. RICEFW Objects List</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">19. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">20. Stage Gate Review</a></p>
                            <p><a href="References\Training Development and Standards.docx">21. Training Development and Standards</a></p>  

                    <h2>Realization-Build</h2> 

                            <p><a href="References\Business_Role_Mapping.docx">1. Business Role Mapping</a></p>
                            <p><a href="References\Communication Effectiveness Survey.xlsx">2. Communication Effectiveness Survey</a></p>
                            <p><a href="References\Detailed Build Schedule.mpp">3. Detailed Build Schedule</a></p>
                            <p><a href="References\Detailed Build Schedule Guidelines.docx">4. Detailed Build Schedule Guidelines</a></p>
                            <p><a href="References\End User ILT Course.pptx">5. End User ILT Course</a></p> 
                            <p><a href="References\End User Training Course Outline.docx">6. End User Training Course Outline</a></p>
                            <p><a href="References\End User Training Curriculum.xlsx">7. End User Training Curriculum</a></p> 
                            <p><a href="References\FS template Conversion.docx">8. FS template Conversion</a></p> 
                            <p><a href="References\FS template Enhancements.docx">9. FS template Enhancements</a></p>
                            <p><a href="References\FS template Interface.docx">10. FS template Interface</a></p> 
                            <p><a href="References\FS template Reports & Forms.docx">11. FS template Reports & Forms</a></p>    
                            <p><a href="References\Test Scenario Description.docx">12. Integration Test Scenarios</a></p>
                            <p><a href="References\Integration Test Script.xlsx">13. Integration Test Script</a></p>
                            <p><a href="References\Testing Daily Schedule_Single Instance per Test.xlsx">14. Integration Testing Schedule</a></p>
                            <p><a href="References\A Day in the Life of a Tester.pptx">15. Integration Testing Training Materials</a></p>
                            <p><a href="References\Meeting Minutes.docx">16. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">17. Milestone Report</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">18. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">19. Stage Gate Review</a></p>

                    <h2>Realization-Testing</h2> 

                            <p><a href="References\Batch Schedule.xlsx">1. Batch Job Schedule</a></p>    
                            <p><a href="References\Communication Effectiveness Survey.xlsx">2. Communication Effectiveness Survey</a></p>
                            <p><a href="References\End User Training Curriculum With Calendar Option.xlsx">3. End User Training Curriculum With Calendar Option</a></p>
                            <p><a href="References\Live Cutover Plan.docx">4. Live Cutover Plan</a></p>
                            <p><a href="References\Meeting Minutes.docx">5. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">6. Milestone Report</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">7. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">8. Stage Gate Review</a></p>
                            <p><a href="References\Train the Trainer Part 1 Program Overview.docx">9. Train the Trainer Part 1 Program Overview</a></p>                         

                    <h2>Final Preparation</h2> 

                            <p><a href="References\Controlled Resumption Scripts.xlsx">1. Controlled Resumption Scripts</a></p>
                            <p><a href="References\Go-live Readiness Checklist.xlsx">2. Go-live Readiness Checklist</a></p>
                            <p><a href="References\Go Live Readiness Scorecard.pptx">3. Go Live Readiness Scorecard</a></p>
                            <p><a href="References\Meeting Minutes.docx">4. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">5. Milestone Report</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">6. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">7. Stage Gate Review</a></p>
                            <p><a href="References\Training Evaluation.xlsx">8. Training Evaluation</a></p>
                            <p><a href="References\User Auth  SAP ABAP Projects.xlsx">9. User ID List and Authorization Matrix</a></p> 

                    <h2>Post-Go Live</h2> 

                            <p><a href="References\Lessons Learned User Focus Group.docx">1. Lessons Learned User Focus Group</a></p>
                            <p><a href="References\Meeting Minutes.docx">2. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">3. Milestone Report</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">4. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">5. Stage Gate Review</a></p>
                    

                </div>
                <div class="col-xs-6 col-md-4"> 
                    <h3>Governing Principles</h3>

                    <h2>Process Tailoring</h2>

                            <p><a href="References\Process Tailoring Repository.xlsx">1. Process Tailoring Record</a></p>                                                                                              

                    <h2>Project Planning</h2>
  
                            <p><a href="References\Project Effort Estimation Sheet - 3 Point Estimation.xls">1. Effort Estimate for Software Developments</a></p>
                            <p><a href="References\Project Approach Document.docx">2. Project Approach Document</a></p>
                            <p><a href="References\Project Charter.docx">3. Project Charter</a></p>
                            <p><a href="References\Project Status Tracking Sheet.xlsm">4. Project Status Tracking Sheet</a></p>
                            <p><a href="References\Process Tailoring Repository.xlsx">5. Process Tailoring Record</a></p>
                            <p><a href="References\Project Skills Tracker.xlsx">6. Skill Tracker</a></p> 

                    <h2>Project Monitoring & Control</h2>

                            <p><a href="References\Project Skills Tracker.xlsx">1. Project Skill Tracker</a></p> 
                            <p><a href="References\Project Status Tracking Sheet.xlsm">2. Project Status Tracking Sheet</a></p>  
                       
                    <h2>Measurements and Analysis</h2>

                            <p><a href="References\MOD.xlsx">1. Metric Objective Description</a></p> 

                    <%--<h2>Project Closure</h2>--%>

                    <h2>Process Improvements</h2>

                            <p><a href="References\Process Improvement Request Form.docx">1. Process Improvement Request Form.docx</a></p>                                                                                            
                            <p><a href="References\Process Improvement Request Log.xlsx">2. Process Improvement Request Log</a></p>                                                                                            
                        


                </div>
        </div>     
    <!-- /#page-content-wrapper -->
</asp:Content>
