﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ProcessTailoring.aspx.cs" Inherits="attune_Document_Web.ProcessTailoring" %>

<asp:Content ID="pTPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Process Tailoring Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>A well-defined project implementation methodology and development methodology is critical for success in projects. Process tailoring refers to the activity of tuning a standardized process to meet the needs of a specific project. All projects should conduct their process tailoring in the project preparation stage with help of practice heads.  </p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>attune Implementation Roadmap (aIR) enables project teams to use standard processes, templates, guidelines and  methodology.</p>
                            <p>The scope of the Process Tailoring process describes the setup project leadership should execute during the time of process tailoring.  After initial process baseline, project team should execute the project as per the tailored project process. If required, project team need to re- tailor with help of Project Manager (PM) and Practice Head.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>PAD  - </strong>Project Approach Document</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>PM - </strong>Project Manager</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>aIR - </strong>attune Implementation Roadmap</p>
                                <p><strong>SA - </strong>Solutions Architect</p>
                          </div>
                        </div>

                        <div class="data">
                            <h3>Process and Steps</h3>     
                            <h4>1.	Process Tailoring </h4>   
                            <p>With the help of startup documents, understand Project context and Specific Process Needs.</p>
                            <p>Identify the deviation of standard attune process in the project with respect to client requests, project context, and execution mode.</p>
                            <p>With the standard process, identify the activities that will (may not) be carried out for project specific reasons. </p>
                            <p>If project execution method is different to attune’s defined process and activities, identify the differentiation.</p>
                            <p>If client mandates, to use their own templates, guidelines, standards and process, identify the usage and gaps between client's & attune templates.</p>
                            <p>Follow attune define process in all other instances.</p>

                            <h4>2.	Documentation </h4>   
                            <p>Document the above stated process with required detailed justifications for deviations.</p>

                            <h4>3.	Approval</h4>   
                            <p>•	Get the Practice head’s approval for tailored project process.
                               •	Share the tailored project process with the project team
                            </p>

                            <h4>4.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <td rowspan="4">Process Tailoring </td>
                                        <td>Identify project context and applicable aIR methodology for the project </td>
                                        <td>PM, Practice Head</td>
                                        <td rowspan="4"></td>                                    
                                        <td rowspan="4">SOW, PAD</td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Tailor project  process tailoring record  or do changes to accommodate changes in the  project</td>
                                        <td>PM, Practice Head, SA</td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Review process tailoring record by process Practice Head</td>
                                        <td>Practice Head</td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Incorporate review feedbacks</td>
                                        <td>PM, TL</td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr>
                                        <td rowspan="2">Leadership review</td>
                                        <td>Review and approve process tailoring record </td>
                                        <td>SVP</td>
                                        <td></td>                                    
                                        <td rowspan="2">aPTR</td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Upload baseline</td>
                                        <td>PM, TL</td>
                                        <td>One attune backup</td>                                    
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr>
                                        <td rowspan="3">Monitoring </td>
                                        <td>Conduct the project reviews based on the tailored project process</td>
                                        <td>Practice Head</td>
                                        <td rowspan="3"></td>                                    
                                        <td rowspan="3">Project Audit reports, PAD</td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Report review findings</td>
                                        <td>Practice Head</td>
                                        <%--<td></td>--%>                                    
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Track to closure those findings and obtain approval.</td>
                                        <td>PM, TL</td>
                                        <%--<td></td>--%>                                    
                                        <%--<td></td>--%>
                                        </tr>      
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Project SOW signed-off </p>
                                            <p>•	Project kicked off for execution</p>
                                            <p>•	Required resources for Project Manager (PM) and Project Lead (PL) are identified with respective roles and responsibilities</p>
                                            <p>•	PM and TL is trained to do the process tailoring</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Process Tailoring record prepared, reviewed, approved and maintained</p>
                                            <p>•	Regular project reviews conducted and findings are tracked to closure.</p>
                                            <p>•	Manage project baselines and project closure activities.</p>  
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Project SOW</p>
                                            <p>•	Project Approach Document / Project Charter Document</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project Process Tailoring Record </p>
                                            <p>•	Project status review report</p>
                                            <p>•	Process tailoring record review entry</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Number of review finding per project</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	Process Tailoring record is a mandatory for any project running in the delivery center.</p>
                                        </div>
                                    </td>
                                    </tr>   

                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Process Tailoring Process.pdf">Download Process Tailoring Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>                                                                                             
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\Process Tailoring Repository.xlsx">1. Process Tailoring Record</a></p>                                                                                                
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>          
                                    <p><a href="References\Agile Project Process.pptx">1. Agile Project Process</a></p>
                                    <p><a href="References\Life Cycle Definitions.docx">2. Life Cycle Definitions</a></p>                                                           
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                          
                         

<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>
