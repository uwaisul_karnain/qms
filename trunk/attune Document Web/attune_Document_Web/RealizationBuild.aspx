﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="RealizationBuild.aspx.cs" Inherits="attune_Document_Web.RealizationBuild" %>

<asp:Content ID="RealizationBuildPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Realization - Build  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Build phase of Realization of a SAP implementation project lifecycle.  It covers the activities that need to be performed within the Build phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The expected deliverables</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>

                            <p>The activities related to the Build phase that are defined here derive the process followed during this phase of the project.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>BBP - </strong>Business Blueprint Document</p>
                                <p><strong>BP - </strong>Business Process</p>
                                <p><strong>BPO - </strong>Business Process Owner</p>
                                <p><strong>CR - </strong>Change Requests</p>
                                <p><strong>FC - </strong>Functional Consultant</p>
                                <p><strong>FICO - </strong>Finance and Controlling</p>
                                <p><strong>FL - </strong>Functional Lead</p>
                                <p><strong>FS - </strong>Functional Specification</p>
                                <p><strong>MM - </strong>Materials Management</p>
                                <p><strong>PC - </strong>Project Charter</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PGM - </strong>Program Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PMO - </strong>Project Management Office</p>
                                <p><strong>PP - </strong>Planning and Production</p>
                                <p><strong>PPL - </strong>Project Plan</p>
                                <p><strong>RICEF - </strong>Reports, Interfaces, Custom Enhancements and Forms</p>
                                <p><strong>RM - </strong>Resource Manager</p>
                                <p><strong>SA - </strong>Solution Architect</p>
                                <p><strong>SD - </strong>Sales and Distribution</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>TR - </strong>Transport Request</p>
                                <p><strong>TS - </strong>Technical Specification</p>
                                <p><strong>UAT - </strong>User Acceptance Testing</p>
                                <p><strong>WBS - </strong>Work Breakdown Structure</p>
                                <p><strong>WP - </strong>Work Package</p>
                          </div>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>

                        <div class="data">
                            <h3>Process and Steps</h3>     
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived the PMI and the SAP ASAP Focus methodologies. Within the aIR methodology, the Realization phase follows the Business Blueprint phase. The Realization phase is broken further into two, namely Build and Testing. The Build part of the Realization covers the activities performed to realize the proposed solution mentioned in detail in the Business Blueprint Document. </p>
                            <p>It is in this phase that the project team defines the WBS or the WP, as per the finalized RICEF and Configuration list for every business process, with the agreed naming conventions. The resources from the project team (the FCs and TCs) are then allocated to each WBS or WP. The required specification documents for the RICEFs and Configurations are prepared by the resources, which are then reviewed together with the relevant stakeholders from the customer, and the necessary approvals are obtained prior to starting the development or configuration. The developments are carried out as per the given standards and guidelines. Developments and configuration are tracked by the Leads using the tracking sheet and by the Project Manager using the Detailed Build Schedule in order to ensure that the progress is as per the plan. The unit testing is completed and the results are documented for the configured and developed objects, one by one, in order to ensure that these objects function per the requirements and are fit for the purpose.  Finally, string testing of all interfaces is done.</p>
                            <p>During the Blueprint Phase, the Development Environment is set up and put in place as defined in the Customer Ecosystem Architecture and the System Landscape Diagram.  During the Realization Build Phase, the Quality Environment is set up.</p>
                            <p>When the development is completed for data migration, the Data Migration Team will complete a Mock 0 conversion in the Development environment. After Mock 0, the Mock 1 Cutover is done in the QA environment.</p>
                            <p>The ground work required to start the Integration testing is done in this phase as well. The end to end business scenarios which are to be used for the Integration testing are defined. The test scripts are prepared as per the defined scenarios and validated by the relevant stakeholders from the customer. The detailed project schedule is updated to define the timeline for the testing. A schedule is prepared for Integration testing and is shared with the relevant stakeholders from the customer.</p>
                            <p>In parallel, a training schedule is defined in order to train the users who are to be part of the testing team confirmed by the customer. The detailed project schedule also includes the timelines for the training. The required training materials are created as well. </p>
                            <p>The Project Status Tracking Sheet is used to document all Issues, CRs, Assumptions, Risks, Decisions, and Action Items highlighted and discussed during this phase.</p>
                            <p>The table below highlights each of the activities mentioned above in detail. It provides information on the activities, the responsible person or team, the documents created in each activity (i.e. artifacts) and the reference documents used. </p>


                            <h4>Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="7">Realization - Execution of Build</td>
                                            <td><p><strong>1.	Prepare the Build Tracker</strong></p>
                                                <p>Prepare the Build Tracker for the WP or WBS based on the RICEFW Object List in order to track the progress of the developments. Should cover the following:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The assignment of the WBS or WP to the project resources in order to complete the identified RICEFW objects</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Define the timeline for completion of the FS, review and approval of the FS, completion of the TS, completion of development, and completion of Functional Unit testing of the objects that are to be developed</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Update of the detailed Build Schedule with the timelines </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Share the timeline with the expected completion dates to the project team and all relevant stakeholders </p></td>
                                            <td>TL  / FL / TC / FC / PM</td>
                                            <td>Build Tracker / Updated Detailed Build Schedule</td>                                    
                                            <td>RICEFW Object List </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>2.	Prepare Functional Specification Documents for Development Objects</strong></p>
                                                <p>This activity covers the activities that lead to the review and approval of the Functional Specifications. The list of activities include:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Preparation of the FS based on the WP or WBS. The specification is for Development Objects with the details of the purpose and use of the object with applicable test cases, together with the logic to be used to develop the object</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The review and the approval of the Functional Specification is done according to the Functional specification review and approval process by the relevant stakeholder from the Customer, the TL, and the TC assigned to the object.</p></td>
                                            <td>PM / SA / FL / TL / FC / TC, Customer stakeholders </td>
                                            <td>Functional Specification Document for Development Objects / Detailed Project Schedule / Updated Business Blueprint documents</td>                                   
                                            <td>RICEFW Object List / Business Blueprint Document / Build Tracker, FS Approval Procedure </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>3.	Complete the Required Configuration</strong></p>
                                                <p>This activity is related to configuration of the required objects:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Configurations start with the Organizational Structure</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	With the Organizational structure in place, the other objects within each module are configured </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The configuration objects are defined based on the Business Process Documents related to each module</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The Functional Leads create and use the List of Configuration work packages to monitor the progress and to provide updates to the PM</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The PM updates the detailed Build Schedule with the status of the configuration work packages that is given to them by the FL’s.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The TRs created as a result of Configurations are updated into the TR Tracking Sheet</p></td>
                                            <td>FL / FC</td>
                                            <td>List of Configuration Objects / TR Tracking Sheet / Updated Business Blueprint documents</td>                                   
                                            <td>Business Process Documents / Business Blueprint Document</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>4.	Build Development Objects </strong></p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Once the approval for the FS is obtained, the TS is prepared (this is only for development objects) </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The TS is then reviewed by the TL</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	With the review, certain updates may take place in the TS</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	With the FS and TS complete, the development takes place in the development client in the Development box for all the reports, interfaces, conversion tools, Enhancements and Forms</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The already prepared Build Tracker is used to track the progress of the developments </p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Update all relevant stakeholders on the progression of the Build </p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The Transport Requests generated during the build are captured using the Transport Tracking Sheet</p></td>
                                            <td>PM / SA / FL / TL / FC / TC / All relevant stakeholders from Customer </td>
                                            <td>Approved Functional and Technical Specification Documents / Build Tracker / TR Tracking Sheet / Project Status Tracking Sheet / Updated Business Blueprint documents</td>                                   
                                            <td>RICEFW List and Configuration List / Business Blueprint Document</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>5.	Perform Unit Tests</strong></p>
                                                <p>With this activity, the project team would:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Complete the Unit testing for the completed configuration work packages and the development objects following the test scripts written in the Functional/Technical Specifications </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The Unit Testing client in the development box is used for this purpose </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Any issues identified are tracked using an Issue Log and are rectified</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Screen shots and other results information for each work package is documented in the Unit Test Results document</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Once the testing completes with no issues, the Unit testing is considered complete</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Update all relevant stakeholders on the progression of Unit testing</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	FL and TL release the related TRs to QA after verification that the Business Blueprint documents and Functional/Technical Specifications are updated with all CR’s and refinements of the solution during the Build Phase</p></td>
                                            <td>PM / FL / TL / FC / TC </td>
                                            <td>Unit Testing Issue Log / Unit Test Results document</td>                                   
                                            <td>Approved Functional and Technical Specification Documents / Build Tracker</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>6.	Perform String Testing </strong></p>
                                                <p>The string testing is done for the Interface objects in order to confirm the data flow from the source system to the target system.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The string testing is done in the Unit testing client in the development system.  </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Screen shots and other results information for each interface is documented in the Unit Test Results document</p></td>
                                            <td>FL / TL / FC / TC</td>
                                            <td>Unit Testing Issue Log / Unit Test Results document</td>                                   
                                            <td>Approved Functional and Technical Specification Documents / Build Tracker</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>7.	Conduct Mock 0</strong></p>
                                                <p>The Mock 0 is the unit/string test of data migration programs.  It is a full data migration in the development unit test box.  Extract programs, load programs, automated validation routines, data cleansing processes and data validation processes are fully tested.  The extracts and loads are repeated for data that does not load until 100% of data is migrated or there is agreement with the customer and the PMO that it will not be loaded during the Mock 0.  Mock 1 cutover uses the same programs and data to save time.</p>
                                                <p class="tab-content" style="padding-left: 20px">This step covers:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The PM prepares the Mock 0 Schedule which consists of all the activities to be performed during the Mock 0</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The PM kicks off each activity in the Mock 0 Schedule in an email and tracks the completion of the activity.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The Mock 0 is done in the Unit testing client in the Development box to test the Data Migration programs developed as part of the RICEFW list</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	A full set of legacy system production data as of the end of a fiscal month is used for this purpose.  A copy of the legacy production system is taken and installed on a test database with limited access (i.e. frozen database).  This frozen database is used exclusively to extract data for the Mock 0 and the subsequent Mock 1 Cutover.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The data to be migrated is extracted from the frozen database  or manually prepared in the formats required for the Data Conversion Tools (the same tools used during Mock Cutover 1 during the Build phase) </p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The extracted data is reviewed and validated by the relevant stakeholders from the customer.  Issues reported are tracked and resolved.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The data is then uploaded into the unit test client starting with the master data and then moving to the transactional data.</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	The uploaded data is validated by the relevant customer stakeholders using the Data Validation Procedures.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	The process is repeated for data that cannot be loaded until all of the data has either been loaded or exceptions are approved by the PMO.</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	Any issues faced are captured and resolved.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p></td>
                                            <td>PM /SA / FL / TL / FC / TC</td>
                                            <td>Data Migration Templates  / Unit Testing Issue Log </td>                                   
                                            <td>Approved Functional and Technical Specification Documents / Build Tracker</td>
                                        </tr>

                                        <tr>
                                            <td rowspan="9">Realization – Prepare For The Test Phase</td>
                                            <td><p><strong>8.	Set up the Quality Environment</strong></p>
                                                <p>During the Build Phase, the quality environment is prepared according to the approved Client Eco System Architecture.   </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	With the exception of unit testing, all testing takes place in the quality environment.   Separate clients are set up for each cycle of Integration Testing.  Regression testing and Performance/Stress Testing shares the INT 2 client. </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	3rd party test systems and middleware are connected to the SAP test clients.  </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The INT 1 test client is set up first.  It is used during the Build Phase for Mock 1 cutover.  All other clients are required for the Test Phase.</p></td>
                                            <td>PM / Basis Consultant / SAP Security </td>
                                            <td>Updated System Landscape Diagram / Updated Customer Ecosystem Architecture</td>                                    
                                            <td>Project Charter / Project Kick Off Document, Transport Management Procedures / Customer Ecosystem Architecture / System Landscape Diagram</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>9.	Conduct Mock 1 Cutover</strong></p>
                                                <p>The Mock 1 Cutover tests the set-up of the system environment with migrated data.  Mock 1 cutover uses the same programs and data used in the Mock 0.  It is a full data migration to the Quality environment.  100% of the test data is loaded.  Exceptions are approved by the PMO.   Migrated data is used for INT 1.  Time estimated to do Mock 1 Cutover is approximately half of the Mock 0 duration.</p>
                                                <p>In this step:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The PM prepares the schedule to conduct the mock cutover with all the required activities maintained</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The PM kicks off each activity in the Mock Cutover Schedule in an email and tracks the completion of the activity.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The INT 1 quality client is built by the Basis Team and released to the DM Team.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	All released TRs are moved to the INT 1 Quality system so that all required configurations and developments are moved</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The FC and TC perform required set up activities in the INT 1 quality client.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The selected Legacy system data (used for Mock 0) which is in the required template/format is used once again to load the data into the INT 1 client starting with the master data and then moving to the transactional data.</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The migrated data is reviewed and validated in the INT 1 quality environment by the relevant stakeholders from the customer.   Issues reported are tracked and resolved.   The goal is to accurately load 100% of the legacy data.  Exceptions are approved by the PMO.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	The FC and TC perform any required post data migration set up activities in the INT 1 quality client.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	The middleware and 3rd party system cutover activities are performed and validated.</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	Once it is confirmed that everything is in order, the Dry Run or Mock Cutover 1 is considered complete and the INT 1 quality environment ready for conducting the preliminary end-to-end process testing performed at the beginning of the Realization - Test Phase.</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	The status and the results of the Mock 1 cutover are shared daily with all relevant stakeholders of the customer, the customer’s core project team and, if required, the customer’s and attune management team.</p>
                                                <p class="tab-content" style="padding-left: 20px">l.	The actual time taken to complete Mock 1 cutover activities is captured.  The Mock 2 schedule will be based on these times.</p></td>
                                            <td>PM /FL / TL / FC / TC</td>
                                            <td>Mock Cutover Schedule </td>                                   
                                            <td>Date Migration Templates</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>10.	Prepare Integration Testing Schedule and Scripts</strong></p>
                                                <p>The following activities are completed here:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The end to end scenarios to be used for Integration Testing are defined</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	These scenarios are validated and confirmed by the relevant stakeholders from the customer</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The scenarios are documented.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	 Test Scripts are created that cover each business scenario and every RICEFW object.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The required Master data to be used for the completion of the test scripts is identified</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The testing team is identified and confirmed from the customer for Integration Testing</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	Prepare the detailed testing schedule (i.e. by day, by script, by resource).  Adjust resources to fit within the testing timeline or get PMO approval to adjust the testing timeline.</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Update the Baseline Project Schedule with the high-level testing timeline. </p></td>
                                            <td>TSC / PM  / FL / TL / FC / TC / BTS / Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Finalized Integration Testing Schedule /  Finalized Test Scripts / Updated Baseline Project Schedule/ Integration Testing Preparations Status Report</td>                                   
                                            <td>Business Process List / To-Be Business  Documents with process flow diagrams / Business Blueprint Document / RICEFW Object List / Configuration Object List</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>11.	Finalize Testing Defect Management Process</strong></p>
                                                <p>The Testing Defect Management process is reviewed with the customer, tailored, if required, and finalized.</p></td>
                                            <td>TSC / BTS / Customer’s Core Project team </td>
                                            <td>Defect Management Process document</td>                                   
                                            <td>Defect Management Process template</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>12.	Finalize Testing Status Report format</strong></p>
                                                <p>The daily integration testing status report format is tailored and finalized. </p></td>
                                            <td>TSC/BTS </td>
                                            <td>Testing Status Report</td>                                   
                                            <td>Testing Strategy Document</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>13.	Prepare Tester Training Schedule</strong></p>
                                                <p>With the testing team from the customer identified and confirmed, the project team needs to work together with the customer core project team in order to train the testing team so that they will be able to follow the script and complete the testing. Therefore, both teams work together and prepare a training schedule which is later ratified and agreed upon with the relevant stakeholders from the customer. </p>
                                                <p>The planned training would cover: </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	SAP Navigation</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The business processes covered within each SAP module (focusing mainly on the end to end business scenarios identified for Testing)</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The features and functions within each business process</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The concept of master data and how it is used</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The concept of the transactional data and how transactions are entered in SAP for each process together with the main SAP transaction needed for the completion of the testing</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Hands on training with access to SAP system (In general it is the Unit Testing client that is used for this purpose)</p>
                                                <p>Once the training schedule is in place, the detailed project schedule is updated to derive the timelines required to complete the training</p></td>
                                            <td>PM  / FL / TL / FC / TC / BTS / Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Finalized Training Schedule / Updated Detailed Project Schedule</td>                                   
                                            <td>Testing Schedule /  Test Scripts / To-Be Business Documents with process flow diagrams / Business Blueprint Document</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>14.	Prepare Tester Training Materials</strong></p>
                                                <p>This activity is involved in preparing the training manuals required for the training of the testing team to be involved in Integration Testing.The training materials can consists of MS Power Point presentation slides and/or MS Word/PDF documents with screen shots displaying </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	To-be business processes</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	SAP Navigation</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The different transactions used to maintain Master data required for an end to end business scenario</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The different transactions used to maintain transactional data for an end to end business scenario</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Simulations and Business Process Procedures can also be used if they are available.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The process and tools used for executing integration testing are also presented.</p></td>
                                            <td>FL / FC / BTS / Customer Core Project team</td>
                                            <td>All required Training Materials</td>                                   
                                            <td>Training Schedule / Test Scripts / To-Be Business Documents with process flow diagrams / Business Blueprint Document</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>15.	Prepare End User Training Curriculum and Course Outlines</strong></p>
                                                <p>The End User Training curriculum lists the courses that will be taught, the owner for each course and the business roles that will take each course. The curriculum list will become the basis for the development tracker. A course outline is written for each course listed in the curriculum.</p></td>
                                            <td>TM / Customer Core Project Team</td>
                                            <td>End User Training Curriculum</td>                                   
                                            <td>Business Blueprint Documents, BPML, Change Impact assessment</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>16.	Prepare the End User Training development tracker and course material templates</strong></p>
                                                <p>Based on the curriculum list, a development tracker is created to track the status of developing the required end user training materials for each course.</p>
                                                <p>Templates are created based on the specific project standards and customer logos and presentation standards.</p></td>
                                            <td>TM</td>
                                            <td>Training Development tracker and course material templates</td>                                   
                                            <td>Training development standards and templates</td>
                                        </tr>


                                        <tr>
                                            <td rowspan="3">Realization - Communication</td>
                                            <td><p><strong>17.	Communication Effectiveness Check</strong></p>
                                                <p>The Business Transformation Team will develop a set of key questions to ascertain the effectiveness of the communication channels. Based on feedback, additional adjustment will be made to the integrated communication plan </p></td>
                                            <td>BTS/Customer’s Core Project Team</td>
                                            <td>Communication Effectiveness Survey</td>                                    
                                            <td>Integrated Communication Plan </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>18.	Prepare Business Role descriptions and mapping to security roles</strong></p>
                                                <p>BTS will work with Business Process Owners to define both existing and new business roles. Once the business roles are defined and approved, the security team will define security roles and the authorizations schema. BTS may facilitate the discussion between the security team and the business process owners.</p></td>
                                            <td>BTS / BPO / Security</td>
                                            <td>Business Role Mapping Document</td>                                   
                                            <td>BPML, Blueprint Documents, Change Impact Assessment</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>19.	Store all documents created and updated by attune during the Phase on ONEattune in the Project workspace</strong></p>
                                                <p>It is important to retain attune intellectual property on ONE attune.  This activity applies to every member of the team.  It includes documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </p></td>
                                            <td>All attune project team members</td>
                                            <td>Updated ONEattune Project work space</td>                                   
                                            <td>Documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </td>
                                        </tr>
                                                                                                                                                                                                                                                                                                                              
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Acceptance of proposed solution and sign off of the Business Blueprint document</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Development environment ready </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Detailed Build Schedule prepared and resources leveled</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Unit tests passed for configured objects. Defects fixed and retested.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Configured QA system ready for integration testing</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Complete, updated and approved Functional and Technical Specification documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Unit tests passed for all developed objects. Defects fixed and retested.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed string testing of all interfaces</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved RICEFW objects ready for integration testing</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Business Blueprint documents and System Landscape Diagram with changes related to approved CR’s and refinement of the solution during the Build Phase</p>
                                            <p class="tab-content" style="padding-left: 20px">•	100% of Mock 0 data is converted, exceptions approved by the PMO </p>
                                            <p class="tab-content" style="padding-left: 20px">•	100% of Mock 1 Cutover completed, exceptions approved by the PMO</p>
                                            <p class="tab-content" style="padding-left: 20px">•	QA environment ready for Integration Testing with all configurations and developments, including SAP, middleware, EDI, and 3rd party system changes, if required</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved security roles required for integration testing</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Integration Testing Schedule and Scripts</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Integration Testing Training Materials</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Exit Stage Gate Review with all items passed and exceptions approved by Steering Committee</p> 

                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	To-Be Business Process Documents with process flow diagrams</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Document</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	RICEF Object List</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	List of Objects to be configured</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	attune Development Standards & Guidelines document</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Project Status Tracking Sheet</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	System Landscape Diagram</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Detailed Build Schedule</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Training Development Process, Standards and Templates</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Testing Script Development Tracker</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Test Script Template</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Change Impact Assessment</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Integrated Communication Plan</p> 

                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Updated System Landscape Diagram</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Business Blueprint Documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Functional / Technical Specification documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Master Project  Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Detailed Build Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Functional Unit Test Results</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Mock 0 Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Mock 1 Cutover Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Communication effectiveness survey</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Integrated communication plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Testing Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	End User Solution Showcase schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Testing Status Report</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Test Scripts</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Final Tester Training Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All required Tester Training Materials</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Project Status Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	End User Training Curriculum and Development Tracker</p>
                                            <p class="tab-content" style="padding-left: 20px">•	End User Training Course Outlines</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Test Scenarios</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Role Mapping Design (including business roles mapped to security roles)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	End User ILT Course Presentation</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Data Cleansing Process and Procedures</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Realization Kick-off Presentation</p> 

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Realization - Build Phase profitability</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Percentage of the approved FS documents (100% as a target)</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Percentage of Completeness of the Unit testing </p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Effort for Peer Review VS Number of Findings</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Percentage of data migrated for Integration Testing</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Effort Variance in completion of activities in Build versus Plan</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Percentage of testers attending training (100% as a target)</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Level of training effectiveness for testers</p> 

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	The project may decide to use client defined templates and standards for the deliverables from within the Realization Build phase. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p> 

                                            <p class="tab-content" style="padding-left: 20px">•	Certain activities of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p> 

                                            <p class="tab-content" style="padding-left: 20px">•	Certain activities of this process may be delivered in another phase of the project, as per the contractual requirements.  This tailoring is documented in the Project’s Process</p> 

                                            <p class="tab-content" style="padding-left: 20px">•	In case the client or another team representing the client is performing some parts of the activities and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p> 

                                        </div>
                                    </td>
                                    </tr>     
                                           
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Realization - Build Phase Process.pdf">Download Realization - Build Phase Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>                                     
                                                                                   
                                </tbody>
                                </table>
                            </div>
                        </div>

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <h3>Document Templates</h3>       
                                <p><a href="References\FS template Conversion.docx">1. FS template Conversion</a></p> 
                                <p><a href="References\FS template Enhancements.docx">2. FS template Enhancements</a></p>
                                <p><a href="References\FS template Interface.docx">3. FS template Interface</a></p> 
                                <p><a href="References\FS template Reports & Forms.docx">4. FS template Reports & Forms</a></p>                                              
                                <p><a href="References\Business_Role_Mapping.docx">5. Business Role Mapping</a></p> 
                                <p><a href="References\PMO Detailed Build Plan.mpp">6. PMO Detailed Build Plan.mpp</a></p> 
                                <p><a href="References\Communication Effectiveness Survey.xlsx">7. Communication Effectiveness Survey</a></p>
                                <p><a href="References\End User ILT Course.pptx">8. End User ILT Course</a></p> 
                                <p><a href="References\End User Training Course Outline.docx">9. End User Training Course Outline</a></p>
                                <p><a href="References\End User Training Curriculum.xlsx">10. End User Training Curriculum</a></p> 
                                <p><a href="References\Integration Test Script.xlsx">11. Integration Test Script</a></p>

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Guidelines</h3>   
                                <p><a href="References\BW Design, Development and Naming standards.docx">1. BW Design, Development and Naming standards</a></p> 
                               <p><a href="References\PMO Detailed Build Plan Guidelines.docx">2. PMO Detailed Build Plan Guidelines</a></p>                                                   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Samples</h3>
                                <p><a href="References\Sample FS Conversions.docx">1. Sample FS Conversions</a></p> 
                                <p><a href="References\Sample FS Enhancements.docx">2. Sample FS Enhancements</a></p>
                                <p><a href="References\Sample FS Interface.docx">3. Sample FS Interface</a></p> 
                                <p><a href="References\Sample FS Reports.docx">4. Sample FS Reports</a></p>
                          </div>
                        </div>   --%> 
    
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <%--<h3>Functional & Technical</h3>--%>
                                <h4>Templates</h4>      
                                <p><a href="References\Business_Role_Mapping.docx">1. Business Role Mapping</a></p>
                                <p><a href="References\Communication Effectiveness Survey.xlsx">2. Communication Effectiveness Survey</a></p>
                                <p><a href="References\Detailed Build Schedule.mpp">3. Detailed Build Schedule</a></p>
                                <p><a href="References\Detailed Build Schedule Guidelines.docx">4. Detailed Build Schedule Guidelines</a></p>
                                <p><a href="References\End User ILT Course.pptx">5. End User ILT Course</a></p> 
                                <p><a href="References\End User Training Course Outline.docx">6. End User Training Course Outline</a></p>
                                <p><a href="References\End User Training Curriculum.xlsx">7. End User Training Curriculum</a></p> 
                                <p><a href="References\FS template Conversion.docx">8. FS template Conversion</a></p> 
                                <p><a href="References\FS template Enhancements.docx">9. FS template Enhancements</a></p>
                                <p><a href="References\FS template Interface.docx">10. FS template Interface</a></p> 
                                <p><a href="References\FS template Reports & Forms.docx">11. FS template Reports & Forms</a></p>                                    
                                <p><a href="References\Test Scenario Description.docx">12. Integration Test Scenarios</a></p>
                                <p><a href="References\Integration Test Script.xlsx">13. Integration Test Script</a></p>
                                <p><a href="References\Testing Daily Schedule_Single Instance per Test.xlsx">14. Integration Testing Schedule</a></p>
                                <p><a href="References\A Day in the Life of a Tester.pptx">15. Integration Testing Training Materials</a></p>
                                <p><a href="References\Meeting Minutes.docx">16. Meeting Minutes</a></p>
                                <p><a href="References\Milestone Report.xlsx">17. Milestone Report</a></p>
                                <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">18. Services Methodology Operational Review Evaluation</a></p>
                                <p><a href="References\Stage Gate Review.pptx">19. Stage Gate Review</a></p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>PMO & Testing</h3>--%>   
                                <h4>Guidelines</h4> 
                                <p><a href="References\ABAP Development Guidelines.docx">1.  ABAP development Guidelines</a></p>  
                                <p><a href="References\Coding Standards Java.doc">2.  Coding Standards Java</a></p>    
                                <p><a href="References\Coding Standards Microsoft.Net.docx">3.  Coding Standards Microsoft.Net</a></p>  
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>Business Transformation</h3>--%>
                                <h4>Samples</h4> 
                                <p><a href="References\Sample FS Conversions.docx">1. Sample FS Conversions</a></p> 
                                <p><a href="References\Sample FS Enhancements.docx">2. Sample FS Enhancements</a></p>
                                <p><a href="References\Sample FS Interface.docx">3. Sample FS Interface</a></p> 
                                <p><a href="References\Sample FS Reports.docx">4. Sample FS Reports</a></p>
                                <p><a href="References\Sample RICEFW Development Process.pptx">5. Sample RICEFW Development Process</a></p>
                          </div>
                        </div>                        

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                        </div> 

                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                        </div> 
                      --%>        
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>

