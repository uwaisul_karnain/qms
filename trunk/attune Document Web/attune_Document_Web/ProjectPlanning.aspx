﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ProjectPlanning.aspx.cs" Inherits="attune_Document_Web.ProjectPlanning" %>

<asp:Content ID="projectPlanningPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project Planning Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>attune executes SAP Implementations and developments, new technology software developments, enhancements and maintenance projects for its clients. The project planning process is a discipline to organize and manage resources in a way that the project is completed within its defined scope, quality, time and cost constraints.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The term “project” is used to describe the full set of activities from the time the proposal is accepted (or a contract is signed) to the time the solution and services are delivered according to the accepted proposal/ contract.</p>
                            <p>The project planning process ensures that a project is initiated and planned in a methodical manner.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>CM Plan - </strong>Configuration Management Plan</p>
                                <p><strong>DAR - </strong>Decision Analysis and Resolution</p>
                                <p><strong>FC  - </strong>Functional Consultant</p>
                                <p><strong>HRE - </strong>Human Recourses Executive</p>
                                <p><strong>M&A  - </strong>Measurement and Analysis</p>
                                <p><strong>MPP  - </strong>Microsoft Project Plan</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PAD - </strong>Project Approach Document</p>                                
                                <p><strong>PC  - </strong>Process Consultant</p>
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>PM - </strong>Project Manager</p>  
                                <p><strong>QA Plan  - </strong>Quality Assurance Plan</p>        
                                <p><strong>SA  - </strong>Solution Architect</p>
                         </div> 
                         <div class="col-xs-6 col-md-4">  
                                <p><strong>SOW  - </strong>Statement Of Work</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>VP - </strong>Vice President</p>
                                <p><strong>WBS - </strong>Work Breakdown Structure</p>
                                <p><strong>PSTS - </strong>Project Status Tracking Sheet</p>
                          </div>
                        </div>

                        <div class="data">
                            <h3>Process and Steps</h3>     

                            <p>After the client first level of discussions and or draft proposal or a contract, the VP Services assigns the project to a PM/PL. </p>
                            <p>The PM/PL plans and documents the Project’s Processes with the help of project Solutions/ Technical architect(s) and Functional & Technical consultants. This is reviewed and approved by the Program Manager and Services Methodology Practice Lead.</p>
                            <p>The PM with the help of Solutions/ Technical architect(s), Functional & Technical consultants, practice leads and VP services, creates the Project Charter &/or PAD using the standard template. The Project Charter &/or PAD is created at the beginning of the project and kept up-to-date as the project progresses. The Project Charter &/or PAD contains sufficient detail to monitor and exercise control over the project execution. It is supported with a detailed schedule that contains activities and resource assignments for all of the subsequent phases of the project. </p>
                            <p>In addition to the Project Charter &/or PAD, project planning includes project-related plans and documents which are used to execute the project as we defined in the aIR methodology.</p>
                            <p>All of these are prepared using appropriate processes, in parallel with the Project Charter &/or PAD. All of the plans are compatible with each other and with the proposal/ contract.</p>
                            <p>Once all of the plans are created, reviewed and base-lined, the project is executed, monitored and controlled using the Integrated Project Monitoring and Control process.</p>
                            <p>Detailed project sub plans will be created during the project execution phases.</p>

                            1. Steps                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="15"><strong>Initiate, Prepare and conduct </strong></td>
                                            <td><p>With the proposed SOW or client acceptance to the project, VP – Servicers create the project in PSA and allocates the Project Manager to the Project.</p> </td>
                                            <td>VP - Services</td>
                                            <td>PSA project</td>                                    
                                            <td rowspan="4">Draft SOW , Initial estimations, propose solution (optional)</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Send out resource requests to VP Services with in the region. For offshore resources and or when resources are not available within the region, VP Services send the request to different region.</p></td>
                                            <td>VP – Services/ Project Manager</td>
                                            <td>PSA resource request </td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Resource allocation based on the resource request and resources assignment in PSA. </p></td>
                                            <td>VP – Services </td>
                                            <td>PSA assignment</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare project onboarding guide</p></td>
                                            <td>PM, Offshore PL</td>
                                            <td>Project onboarding guide </td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Project prep phase activities – Customer shared.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	High level project plan</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Detailed workshop schedule</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Project kickoff presentation</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Project charter </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Estimations revalidation (size and effort) and check alignment to client expectations &/or objectives</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Capture and track Risks, issues, assumptions, decisions, dependencies  </p></td>
                                            <td>PM, PL, SA, TA, FC, TC, Customer stakeholders</td>
                                            <td>Plan, Project Kickoff PPT, Project Charter, Project Status Tracking Sheet, Estimation sheet</td>
                                            <td>Initial estimations, SOW</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Customer signoff for all above documents</p></td>
                                            <td>Customer, PM</td>
                                            <td>All customer shared artifacts</td>
                                            <td></td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Project prep phase activities – Internal</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Project approach Document</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Capture and track Risks, issues, assumptions, decisions, dependencies  </p></td>
                                            <td>PM, PL, SA, TA, FC, TC</td>
                                            <td>PAD, Project Status Tracking Sheet</td>
                                            <td>PAD, Project Charter, Kickoff PPT</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Re-negotiation on initial commitments. </p></td>
                                            <td>FC, TA, PM / PL, Customer</td>
                                            <td>Estimation, Project schedule</td>
                                            <td rowspan="3">PAD, Project Charter, Kickoff PPT</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Update project schedule and Project Status Tracking Sheet.</p></td>
                                            <td>PM, PL, SA, TA, FC, TC</td>
                                            <td>PSA, MPP, Project tracking system, Burn Down Charts</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Define measureable objectives to the project that align with project and attune success factors.</p></td>
                                            <td>PM, PL, SA, TA, FC, TC</td>
                                            <td>Project Charter, PAD</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Conduct project team internal Skill assessment and realign project resources if necessary </p></td>
                                            <td>PM, PL, SA, TA, FC, TC</td>
                                            <td>Internal Skill assessment </td>
                                            <td rowspan="4"></td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Plan required internal trainings against skill gaps</p></td>
                                            <td>PM/PL,HRE, SA,VP Services, TA</td>
                                            <td>Internal Training plan</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Conduct project process tailoring and get the approval from practice leads</p></td>
                                            <td>PM, PL, SA, TA, FC, TC, Practice Lead</td>
                                            <td>Approved PTR</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Establish the project governance model as per the project charter </p></td>
                                            <td>PM, PL , VP services, Program Manager, Practice Lead, Customer</td>
                                            <td>Execution of governance model , Minutes of Meetings, Action items</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Plan and Conduct the project kickoff </p></td>
                                            <td>attune project team, Customer project team, relevant stakeholders </td>
                                            <td>Project kickoff PPT</td>
                                            <td>PA, Project Charter</td>
                                        </tr>
                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Project high level schedule and delivery model</p>
                                            <p>•	Project resources plan, offshore / onsite project roles and responsibilities</p>
                                            <p>•	Proposed contract (SOW)</p>
                                            <p>•	Customer charter presentation / document</p>
                                            <p>•	Project created in PSA</p>
                                            <p>•	PM or Project Lead, Functional and Technical Leads are assigned to the project</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Project Approach &/or Charter Document created, including all supporting documents</p>
                                            <p>•	Completed Project Kickoff presentation</p>
                                            <p>•	Completed detailed project schedule</p>
                                            <p>•	People are assigned for project execution activities</p>
                                            <p>•	Commitment to all plans by the relevant stakeholder </p>
                                            <p>•	Project governance model defined</p>

                                            <p class="tab-content" style="padding-top: 20px"><strong>SAP</strong></p>
                                            <p>•	Completed blue print workshop schedule with the relevant stakeholders defined</p>
                                            <p>•	Completed customer boot camp training (Optional)</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Proposed contract (SOW)</p>
                                            <p>•	Working papers (including the estimation sheets) accompanying the proposal/ contract</p>
                                            <p>•	Records of meetings with client/ correspondence</p>
                                            <p>•	Proposed solution (optional) </p>
                                            <p>•	Existing attune solution or product (optional)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project Approach &/or Charter Document</p>
                                            <p>•	Project Kick-off presentation</p>
                                            <p>•	Minutes of meeting Related to Kick-Off</p>
                                            <p>•	Detailed Project Schedule</p>
                                            <p>•	Project Skill Tracker</p>
                                            <p>•	Training Requisition</p>
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	Process Tailoring Record</p>
                                            <p>•	System Landscape document</p>
                                            <p>•	Blueprint workshop schedule</p>
                                            <p>•	Project related strategy documents ( Business transformation, Training, Testing, Data Conversion and Executive Leadership alignment)</p>
                                            <p>•	Work Break Down Structure &/or  3 point Estimation Sheet</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort spent on project estimation</p>
                                            <p>•	Effort for the reviews of  Project Charter, PAD, Estimation, and Process Tailoring  and number of review comments</p>
                                            <p>•	Schedule and Effort Variance</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The PAD may include the Project Configuration Management Plan and the Project’s Process in a single document (see tailoring options in the Configuration Management Process). </p>
                                            <p>•	There may be no phase-end/ milestone reviews for projects of less than one month duration. </p>
                                            <p>•	In some cases, the client may insist on using their templates for PAD and Project Status Report. In such cases, the PM will map the client’s templates to attune templates and ensures that all aspects of attune templates are covered. Addendums should be provided to cover the gaps in the client’s templates.</p>
                                            <p>•	For Projects less than 20 man days effort, PM together with Program Manager and VP Services can decide on a more light weight process, which should be documented in the PAD</p> 
                                        </div>
                                    </td>
                                    </tr>  
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Project Planning Process.pdf">Download Project Planning Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>                                                                                             
                                </tbody>
                                </table>
                            </div>
                        </div>
  
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\Project Effort Estimation Sheet - 3 Point Estimation.xls">1. Effort Estimate for Software Developments</a></p>
                                    <p><a href="References\Project Approach Document.docx">2. Project Approach Document</a></p>
                                    <p><a href="References\Project Charter.docx">3. Project Charter</a></p>
                                    <p><a href="References\Project Status Tracking Sheet.xlsm">4. Project Status Tracking Sheet</a></p>
                                    <p><a href="References\Process Tailoring Repository.xlsx">5. Process Tailoring Record</a></p>
                                    <p><a href="References\Project Skills Tracker.xlsx">6. Skill Tracker</a></p>                                          
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>       
                                    <p><a href="References\MOD.xlsx">1. Metric Objective Description (attune MOD)</a></p>  
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                        
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>
