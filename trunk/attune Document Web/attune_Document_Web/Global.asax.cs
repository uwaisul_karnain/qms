﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;


namespace attune_Document_Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GoogleAuthManager.Instance.Configure(new GoogleAuthRequest()
            {
                //ClientId =
                //    "85538684700-62ci967tevtt9euah8i770jtmh8kn1tf.apps.googleusercontent.com",
                //Secret = "SiXzVhfR8a1ewGQaXuahGt7x",
                ////RedirectUrl = "http://localhost:51988/Default.aspx",

                //ClientId =
                //    "726683419182-qf3dq3m4998872s98gp93qvclr1m948b.apps.googleusercontent.com",
                //Secret = "jgQNNlye3fP55Pi5kEMa2ihW",
                //RedirectUrl = "https://air.go-attune.com/Index.aspx",

                // aIR
                //ClientId =
                //    "219710398591-rdd93188chsjov677sfrjkuc6i8ckaho.apps.googleusercontent.com",
                //Secret = "GY6O2m-UXjO4Zozook2x2RAL",
                //RedirectUrl = "https://air.go-attune.com/aIR/Index.aspx",

                // aIR3
                //ClientId =
                //    "878283963884-soakjmhtgaqn6es72t85acu3v4i3lu60.apps.googleusercontent.com",
                //Secret = "QgT46dJlsu1j5w8VesS9NrXa",
                //RedirectUrl = "https://air.go-attune.com/aIR3/Index.aspx",

                // Development
                ClientId =
                    "433176974277-dm54ru2cmpossrbildt5a4cpmnqoe24t.apps.googleusercontent.com",
                Secret = "gSaaJ481i2rNtklWvghCP5fb",
                RedirectUrl = "http://localhost:65205/Index.aspx",


                Scope = @"https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"
            });
        }

    }
}
