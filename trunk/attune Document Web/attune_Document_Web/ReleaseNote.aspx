﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ReleaseNote.aspx.cs" Inherits="attune_Document_Web.ReleaseNote" %>

<asp:Content ID="releaseNotePageContent" ContentPlaceHolderID="MainContentPageSection" runat="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Release Note</h1>

                        <div class="data">
                            <h2>Features Implemented </h2>    

                            <p>•	UI Changes Requested by the Senior Management </p>
                            <p>•	Add Google Authentication and Access Via ONE attune </p>
                            <p>•	Update with Latest Process Documents of Enterprise Applications </p>

                            <h2>Bug Fixes </h2>    

                            <p> </p>

                            <h2>Version</h2>
                            <p><strong>QMS.00.05</strong></p>

                        </div>
                 
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>

