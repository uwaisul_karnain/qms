﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="TrainingConsultants.aspx.cs" Inherits="attune_Document_Web.TrainingConsultants" %>

<asp:Content ID="TrainingConsultantsPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>aIR 2.0 Introduction for Consultants</h1>

                        <h3>Presentations</h3>
                        <div class="data">
                                <p class="tab-content" style="padding-left: 20px"><a href="References\aIR 2_0 Introduction and Overview for Consultants.pptx">1.  aIR 2.0 Introduction for Consultants</a></p>  
                        </div>

                        <h3>Videos</h3>
                        <div class="data">
                                <p class="tab-content" style="padding-left: 20px"><a href="https://life.oneattune.com/mlink/file/NTkzNDg" target="_blank">1.  aIR 2.0 Introduction for Consultants - Recording</a></p>  

                        </div>

        <!-- /#page-content-wrapper -->

</asp:Content>
