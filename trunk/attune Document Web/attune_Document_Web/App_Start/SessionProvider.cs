﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace attune_Document_Web.App_Start
{
    public class SessionProvider
    {
        public static UserProfle LoggedUser
        {
            get { return HttpContext.Current.Session["LoggedUser"] as UserProfle; }
            set { HttpContext.Current.Session["LoggedUser"] = value; }
        }
    }


}