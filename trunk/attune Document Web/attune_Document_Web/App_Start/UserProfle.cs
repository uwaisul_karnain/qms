﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace attune_Document_Web.App_Start
{
    public class UserProfle
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string ProfileImageUrl { get; set; }        
    }
}