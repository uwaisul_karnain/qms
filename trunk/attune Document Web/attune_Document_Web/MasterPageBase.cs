using System;
using System.Web.UI;

namespace attune_Document_Web
{
    public class MasterPageBase : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!GoogleAuthManager.Instance.IsAuthenticated(Context))
                GoogleAuthManager.Instance.ValidateRequest(Context);
        }
    }
}