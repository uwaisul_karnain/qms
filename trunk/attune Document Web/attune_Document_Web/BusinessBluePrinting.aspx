﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="BusinessBluePrinting.aspx.cs" Inherits="attune_Document_Web.BusinessBluePrinting" %>

<asp:Content ID="bluePrintingPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Business Blueprinting  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Blueprint phase of the SAP implementation project lifecycle.  It covers the activities that need to be performed within this phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>BBP - </strong>Business Blueprint Document</p>
                                <p><strong>BP - </strong>Business Process</p>
                                <p><strong>BPO - </strong>Business Process Owner</p>
                                <p><strong>BTS - </strong>Business Transformation Services</p>
                                <p><strong>CR - </strong>Change Requests</p>
                                <p><strong>FC - </strong>Functional Consultant</p>
                                <p><strong>FICO - </strong>Finance and Controlling</p>
                                <p><strong>ILT - </strong>Instructor Led Training</p>
                                <p><strong>FL - </strong>Functional Lead</p>
                                <p><strong>FS - </strong>Functional Specification</p>
                                <p><strong>MM - </strong>Materials Management</p>                                
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PC - </strong>Project Charter</p>
                                <p><strong>PGM - </strong>Program Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PMO - </strong>Project Management Office</p>
                                <p><strong>PP - </strong>Planning and Production</p>
                                <p><strong>PPL - </strong>Project Plan</p>
                                <p><strong>QM - </strong>Quality Management</p>
                                <p><strong>RM - </strong>Resource Manager</p>
                                <p><strong>SA - </strong>Solution Architect</p>
                                <p><strong>RICEFW - </strong>Reports, Interfaces, Conversions, Enhancement, Forms and Workflow</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SD - </strong>Sales and Distribution</p>
                                <p><strong>SME - </strong>Subject Matter Experts</p>
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>TR - </strong>Transport Request</p>
                                <p><strong>TS - </strong>Technical Specification</p>
                                <p><strong>UAT - </strong>User Acceptance Testing</p>
                                <p><strong>WBS - </strong>Work Breakdown Structure</p>
                                <p><strong>WP - </strong>Work Package</p>
                          </div>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h3>Process and Steps</h3>     
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived from PMI and SAP ASAP Focus methodologies.  Within the aIR methodology, the Business Blueprint phase follows the Project Preparation Phase. The activities begin straight after the completion of the Project Preparation Phase Stage Gate sign-off.   This is the phase within which the existing business practices are understood, the requirements are elicited and business impacts are evaluated. </p>
                            <p>The project team (both attune and customer team members) are aligned by functional area (e.g. Order-To-Cash, Procure-to-Pay, Retail Operations, Finance). The business processes covered in each area are listed, and meetings are held as per the Business Blueprint Workshop Schedule prepared during the Project Preparation Phase.  Common workshops are held where the topic covers multiple modules, such as the organizational structure.  All relevant stakeholders, business process owners, heads of departments are invited to these workshops in order to explain the current practices and discuss the to-be business requirements. The requirements and business practices form the basis to derive the proposed solution which is then explained in detail in the Business Blueprint Document. One of the key activities to gain approval of the proposed solution is a Design Walkthrough conducted by the consultants at the end of the Blueprint Phase. </p> 
                            <p>The Project Status Tracking Sheet is used to document and track the Gaps, Issues, Changes, Risks, Assumptions, Decisions, and Action items in this phase as well as in the subsequent phases that follow. </p>
                            <p>The Business Blueprint Document that is created in this phase forms the basis for the subsequent activities to be completed in the Build phase in Realization. </p>
                            <p>Preparation activities for the Build Phase, such as the creation of the development environment and alignment on development standards and methodology, are carried out by the technical team while the Business Blueprint Workshops are carried out.</p>
                            <p>During the Blueprint phase, BTS team members participate in these workshops to provide guidance and input in identifying the business impacts associated with the proposed solution per functional areas.  Typically, a BTS team member may cover 2 functional areas and work closely with customer team members to capture the relevant business impacts ultimately leading to the creation of the Change Impact Assessment document. </p>
                            <p>The Change Impact Assessment documents any changes resulting from a deviation from the “AS-IS” to the “TO-BE” process and is typically categorized into People, Process and or Technology.  This document becomes the foundational document that feeds into the creation of all Business Transformation activities.  </p>


                            <h4>Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="15">Business Blueprint Execution</td>
                                            <td><p><strong>1.	Conduct Workshops</strong></p>
                                                <p>The sessions are held to review the current business practices and to obtain the requirements from the customer. The workshops should aim to focus on</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Review customer’s  business process and practices (together with a visual representation of the flow within each process), identifying the pain points and areas of improvements</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Capture and document the requirements to be covered within each business process in the To-Be solution </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Schedule and conduct follow-up sessions as needed to close all open actions and questions.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Capture and document change impacts resulting in any changes from the As-Is to the To-Be process. </p></td>
                                            <td>PM  / SA / FL / TL / FC / TC / Customer Project team members / All relevant stakeholders/ BTS</td>
                                            <td>Meeting Minutes of Workshops / Project Status Tracking Sheet </td>                                    
                                            <td>Business Blueprint Workshop Schedule / As-Is Business  Process Documents with process flow diagrams (prepared by Customer)</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>2.	Identify the Gaps</strong></p>
                                                <p>The project team identifies which processes/requirements can be mapped using SAP Standard and which processes/requirements are considered as Gaps. </p>
                                                <p>The Gap identification should cover:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Identify and document the processes /requirements that can be addressed using SAP standard functionality</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Identify the possible Gaps </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Discuss and agree on the possible solution options to map the processes / requirements, including integration with external systems</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Use Hands on system demonstrations when necessary in order to further elaborate on the possible solutions (optional)</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Discuss and agree if some of these Gaps can be met with a business process change rather than doing customizing in the system and document implications of such decision from a BT angle (people, process and technology)</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Document all agreed solutions in the related Business Blueprint document.  </p></td>
                                            <td>PM  / SA / FL / TL / FC / TC / BTS /  All relevant stakeholders from Customer </td>
                                            <td>GAP List / Meeting Minutes of Workshops / Project Status Tracking Sheet </td>
                                            <td>As-Is Business  Process Documents with process flow diagrams / Meeting Minutes of Workshops</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>3.	Design the To-Be Solution</strong></p>
                                                <p>This is the designing of the overall integrated solution done by the Solution Architect along with the Functional and Technical Leads from attune and the customer.  The Systems Landscape Design is finalized.</p></td>
                                            <td>PM  / SA / FL / TL / FC / TC</td>
                                            <td>System Landscape Design</td>
                                            <td>As-Is Business  Process Documents with process flow diagrams / Meeting Minutes of Workshops</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>4.	Complete and Sign-off Business Blueprint Documents</strong></p>
                                                <p>The project team documents the To-Be solution for each process with a visual flow diagram</p>
                                                <p>The team completes the Business Blueprint Document.</p>
                                                <p>The customer reviews and provides the sign off on the Business Blueprint Document.</p></td>
                                            <td>PM  / SA / FL / TL / FC / TC</td>
                                            <td>To-Be Business Process Documents with process flow diagrams / Business Blueprint Document</td>
                                            <td>As-Is Business  Process Documents with process flow diagrams </td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>5.	Complete Change Impact Assessment</strong></p>
                                                <p>The BTS team captures any Change Impacts from the blueprint sessions and completes the Change Impact Assessment.  The Change Impact Assessment  documents key changes arising from the proposed solution </p></td>
                                            <td>BTS / Customer Project Tea/SMEs </td>
                                            <td>Change Impact Assessment</td>
                                            <td>Business Blueprint Documents</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>6.	Hold Solution Design Walkthrough </strong></p>
                                                <p>The team conducts a 2-3 day walkthrough of the Solution design for the customer’s Project Team and all relevant stakeholders.  The walkthrough is done using Power Point and system demos in the sandbox environment.  End-to-end business processes are covered.  The customer accepts the proposed solution presented in the Design Walkthrough. </p></td>
                                            <td>PM  / SA / FL / TL / FC / TC / All relevant stakeholders</td>
                                            <td>Design Walkthrough presentation</td>
                                            <td>System Landscape Design, Business Blueprint documents</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>7.	Define the Configuration and Development Objects</strong></p>
                                                <p>Identify</p>
                                                <p>a.	The finalized list of objects to be configured</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The finalized list must include:</p>
                                                <p class="tab-content" style="padding-left: 40px">i.	Data  Extractions Tools and Formats</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	Data conversion Tools and Formats</p>
                                                <p class="tab-content" style="padding-left: 40px">iii.	All required development objects (interfaces, EDI objects, reports, enhancement programs, Outputs and forms)</p>
                                                <p class="tab-content" style="padding-left: 40px">iv.	All required objects to be configured</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Identify a naming convention for the objects. This naming convention is then shared with the customer teams and agreed upon. All the objects are to be named and referred as per the decided convention </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Estimate each configuration and development object</p></td>
                                            <td>PM  / SA / FL / TL / FC / TC </td>
                                            <td>RICEFW Object List / List of Objects to be configured </td>
                                            <td>GAP List / As-Is Business  Process Documents with process flow diagrams  / Business Blueprint documents</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>8.	Prepare the Detailed Build Schedule, Estimate the Build Effort, and Ratify</strong></p>
                                                <p>The PM creates the Detailed Build Schedule utilizing the estimated effort from the defined objects, the Build timelines and the resource plan.  The SA and the Leads collaborate during the ratification process.  The Build Schedule includes all systems impacted, including modifications by external parties to their systems, if these modifications are part of the overall solution.  </p></td>
                                            <td>PM  / SA / FL / TL / FC / TC / Offshore Development Manager</td>
                                            <td>Detailed Build Schedule</td>
                                            <td>RICEFW Object List / List of Objects to be configured  / Project Schedule / Resource Plan</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>9.	Complete Integrated Communication Plan</strong></p>
                                                <p>Based on the Change Impact Assessment and the Stakeholder Assessment, a detailed integrated communication plan is developed. </p></td>
                                            <td>BTS/Customer Project Team/ HR/Customer Communications Team/Key Stakeholders </td>
                                            <td>Integrated Communication Plan</td>
                                            <td>Stakeholder Assessment, Change Impact Assessment</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>10.	Prepare Key Leadership Action Plans and Key Performance Indicators</strong></p>
                                                <p>Key leadership action plans are developed to help coach and drive key leader’s continued support and involvement. The Key Performance Indicators can be developed in conjunction with this activity. </p></td>
                                            <td>BTS/Customer Project Team/ HR/Customer Communications Team/Key Stakeholders</td>
                                            <td>Leadership Action Plans KPIs</td>
                                            <td>Stakeholder Assessment, Change Impact Assessment, Integrated Communication Plan </td>
                                        </tr>   
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>11.	Prepare Change Agent Strategy</strong></p>
                                                <p>Key stakeholders known as Change Agents are identified to help drive the business adoption of the new solution.  These individuals have either formal or informal influence/credibility with the organization.  The Change Agent Strategy document provides a guide and reference on how these Change Agents will carry out their roles. </p></td>
                                            <td>BTS/Customer Project Team/ HR/Customer Communications Team/Key Stakeholders</td>
                                            <td>Change Agent Strategy/Workshop </td>
                                            <td>Stakeholder Assessment, Change Impact Assessment, Integrated Communication Plan</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>12.	Prepare End User Training development process, standards and templates.</strong></p>
                                                <p>During the Blueprint phase preparation for developing End User training begins. Preparation includes the documenting of the development process and standards for the training materials. Customization of the templates for the various documents is completed. </p></td>
                                            <td>BTS / Training Lead</td>
                                            <td>Development Process, Standards and style sheets, ILT materials templates</td>
                                            <td>Baseline templates</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>13.	Prepare Integration Testing Script Development Tracker</strong></p>
                                                <p>This tracker is used to list out the business scenarios and variants for which scripts are to be created. The tracker lists the owner of the script and target dates for completing each step in the development and approval process</p></td>
                                            <td>TC / FL</td>
                                            <td>Integration Test Script Tracker</td>
                                            <td>BPML, RICEFW list, Blueprint document</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>14.	Prepare Knowledge Transfer Plan and Checklist</strong></p>
                                                <p>The Knowledge transfer plan describes how the knowledge transfer strategy will be implemented and the checklist tracks to completion of listed knowledge transfer events.</p></td>
                                            <td>BTS / PM / FL </td>
                                            <td>Plan and checklist documents</td>
                                            <td>wledge Transfer Strategy</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>15.	Prepare Defect Management Process</strong></p>
                                                <p>Based on current project landscape and tools available, the Defect Management Process is determined and documented.</p></td>
                                            <td>BTS / Testing Lead</td>
                                            <td>Defect Management Process Document</td>
                                            <td>Baseline template, System Landscape Design</td>
                                        </tr>

                                        <tr>
                                            <td rowspan="5">Prepare for the Build Phase</td>
                                            <td><p><strong>16.	Build and set up the development environment</strong></p>
                                                <p>During the Blueprint Phase the development environment is prepared according to the approved Client Eco System Architecture.  It must be ready to begin development at the beginning of the Build Phase.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The setup of the Development box with the separate clients clearly identified for: </p>
                                                <p class="tab-content" style="padding-left: 40px">i.	Configuration only client: Also known as the Golden client. This is the system purely for configuration changes. </p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	Development only client: This is the system where all developments are carried out. </p>
                                                <p class="tab-content" style="padding-left: 40px">iii.	Unit Testing Client: This system would consist of both Configuration changes and Development changes in order to perform Unit Testing.  Mock 0 will be done in this client. </p></td>
                                            <td>TA / TL / TC</td>
                                            <td>Development environment ready for the Build</td>                                    
                                            <td>Client Eco System Architecture </td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>17.	Confirm Development Procedures, Development Standards, the Offshore Model, and Transport Management Procedures</strong></p>
                                                <p>During the Blueprint Phase, the following activities are done as part of this activity:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	the attune and the Customer’s Technical Teams agree on the development standards that will be followed</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The offshore vs onsite development model is confirmed and communicated</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Technical procedures are agreed to such as Functional/Technical spec approval processes, transport management procedures, system freeze procedures, etc.</p></td>
                                            <td>TA / TL / TC</td>
                                            <td>Development Procedures document </td>
                                            <td>Development Standards / Development models / Transport Management Procedures</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>18.	Prepare meeting presentation templates</strong></p>
                                                <p>Presentation templates for the Team Leads, PMO and Steering Committee meetings are prepared, reviewed and approved.</p></td>
                                            <td>PGM / PM</td>
                                            <td>Presentation templates</td>
                                            <td>Project communication and PowerPoint standards</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>19.	Onboard the Development Team</strong></p>
                                                <p>PM works with the TA and TL and Regional Professional Services VP’s to identify the offshore and onsite developers that will be added to the project team for the Build Phase.</p></td>
                                            <td>PM / TA / TL </td>
                                            <td></td>
                                            <td>Resource Plan</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>20.	Store all documents created and updated by attune during the Phase on ONEattune in the Project workspace</strong></p>
                                                <p>It is important to retain attune intellectual property on ONE attune.  This activity applies to every member of the team.  It includes documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </p></td>
                                            <td>All attune project team members</td>
                                            <td>Updated ONEattune Project work space</td>
                                            <td>Documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </td>
                                        </tr>
                                   
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Signed off Project Charter by Customer</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Project Kick Off </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Master Project Schedule for Blueprint phase </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Blueprint Workshop Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Sandbox environment in place for system demos</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Blueprint Preparation Check List fulfilled</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Customer Project Team Training completed</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Customer resources committed to attend scheduled workshops</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Blueprint workshop invitations sent out</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Successful completion of the Business Blueprint workshops</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed To-Be Process Flows</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized RICEFW Objects List</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized List of Configuration Objects</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Change Impact Assessment</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Key Performance Indicators for project reporting</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Acceptance and sign-off of the Business Blueprint documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Solution Design walkthrough and Sign-off</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Final Project Scope</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Exit Stage Gate Review with all items passed and exceptions approved by Steering Committee</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Signed off Statement of Work/Contract </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Project Charter, includes strategy documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Initial 30 Day Communication Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Initial Business Transformation Readiness Assessment Report</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Project Status Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Final Resource Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Baseline Project Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Workshop Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Blueprint Tracker</p>
                                            <p class="tab-content" style="padding-left: 20px">•	System Landscape Design</p>
                                            <p class="tab-content" style="padding-left: 20px">•	SAP Systems Landscape Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">•	As-Is Business Process Documents (Optional)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Initial Business Transformation Readiness Assessment (Optional)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Document</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Business Process Master List (BPML)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Change Champion/Change Agent Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Change Impact Assessment</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Change Impact Assessment Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Exit Stage Gate Review with all items passed and exceptions approved by Steering Committee</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Defect Management Process</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Detailed Build Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Development Procedures document</p>
                                            <p class="tab-content" style="padding-left: 20px">•	End User Training Curriculum with Calendar Option</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Gap List</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integrated Communication Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Testing Script Development Tracker</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Key Stakeholder Leadership Action Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Knowledge Transfer Plan and Checklist</p>
                                            <p class="tab-content" style="padding-left: 20px">•	List of Configuration Objects</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Meeting minutes of Business Blueprint workshops</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Weekly PMO Meeting update</p>
                                            <p class="tab-content" style="padding-left: 20px">•	RICEFW Object List</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Solution Design Walkthrough presentation</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Steering Committee Meeting update</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Weekly Team Leads Meeting update</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Training Development Process, Standards and Templates</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated SAP System Landscape Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Project Status Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Strategy documents and Project Charter</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Integration Landscape Diagram</p>


                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Phase profitability</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Schedule and effort variance for Business Blueprint Phase</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Review Efficiency</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Quality of requirements captured and how well solution fits – measured by number of subsequent change requests</p>
                                        </div>
                                    </td>
                                    </tr>  
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	The project may decide to use client defined templates and standards for the deliverables from within the Blueprinting phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Certain activities of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Certain activities of this process may be delivered in another phase of the project, as per the contractual requirements.  This tailoring is documented in the Project’s Process</p>
                                            <p class="tab-content" style="padding-left: 20px">•	In case the client or another team representing the client is performing some parts of the activities and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>
                                        </div>
                                    </td>
                                    </tr>    
                                     
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Business Blueprint Phase Process.pdf">Download Business Blueprint Phase Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr> 

                                </tbody>
                                </table>
                            </div>
                        </div>

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <h3>Document Templates</h3>                       
                                <p><a href="References\Change_Champion_Change_Agent_Strategy.docx">1. Change Champion Change Agent Strategy</a></p> 
                                <p><a href="References\Change_Impact_Assessment.docx">2. Change Impact Assessment</a></p> 
                                <p><a href="References\Defect Management Process.docx">3. Defect Management Process</a></p> 
                                <p><a href="References\Integrated Communication Plan.xlsx">4. Integrated Communication Plan</a></p> 
                                <p><a href="References\Key_Stakeholder_Leadership_Action_Plan.docx">5. Key Stakeholder Leadership Action Plan</a></p> 
                                <p><a href="References\Knowledge Transfer Tracking.xlsx">6. Knowledge Transfer Tracking</a></p> 
                                <p><a href="References\Training Development and Standards.docx">7. Training Development and Standards</a></p> 
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Guidelines</h3>                                                     
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Samples</h3>
                          </div>
                        </div>  --%>

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <%--<h3>Functional & Technical</h3>--%>     
                                <h4>Templates</h4>  
                                <p><a href="References\Business Blueprint Workshop Schedule.xlsx">1. Business Blueprint Workshop Schedule</a></p>
                                <p><a href="References\Change_Champion_Change_Agent_Strategy.docx">2. Change Champion Change Agent Strategy</a></p> 
                                <p><a href="References\Change_Impact_Assessment.docx">3. Change Impact Assessment</a></p> 
                                <p><a href="References\Change Impact Assessment Strategy.docx">4. Change Impact Assessment Strategy</a></p> 
                                <p><a href="References\Change Impact Assessment Workbook.xlsx">5. Change Impact Assessment Workbook</a></p> 
                                <p><a href="References\Defect Management Process.docx">6. Defect Management Process</a></p>
                                <p><a href="References\Detailed Build Schedule.mpp">7. Detailed Build Schedule</a></p>
                                <p><a href="References\Detailed Build Schedule Guidelines.docx">8. Detailed Build Schedule Guidelines</a></p>
                                <p><a href="References\End User Training Curriculum With Calendar Option.xlsx">9. End User Training Curriculum With Calendar Option</a></p> 
                                <p><a href="References\GAP List.xlsx">10. GAP List</a></p>
                                <p><a href="References\Integrated Communication Plan.xlsx">11. Integrated Communication Plan</a></p> 
                                <p><a href="References\Integration Test Script Development Tracker.xlsx">12. Integration Test Script Development Tracker</a></p>
                                <p><a href="References\Key Stakeholder Leadership Action Plan.docx">13. Key Stakeholder Leadership Action Plan</a></p> 
                                <p><a href="References\Knowledge Transfer Tracking.xlsx">14. Knowledge Transfer Tracking</a></p> 
                                <p><a href="References\Meeting Minutes.docx">15. Meeting Minutes of Business Blueprint Workshop</a></p>
                                <p><a href="References\Milestone Report.xlsx">16. Milestone Report</a></p>
                                <p><a href="References\RICEFW List.xlsx">17. RICEFW Objects List</a></p>
                                <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">18. Services Methodology Operational Review Evaluation</a></p>
                                <p><a href="References\Stage Gate Review.pptx">19. Stage Gate Review</a></p>
                                <p><a href="References\Training Development and Standards.docx">20. Training Development and Standards</a></p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>PMO & Testing Templates</h3>--%>   
                                <h4>Guidelines</h4> 

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>Business Transformation Templates</h3>--%>
                                <h4>Samples</h4> 

                          </div>
                        </div>                        

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                        </div> 

                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                        </div>   --%>  
                                 
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>
