﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="InternalAudit.aspx.cs" Inherits="attune_Document_Web.InternalAudit" %>

<asp:Content ID="internalAuditPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Internal Audit Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>Internal audit is an important activity that supports the effective implementation of attune’s processes for areas that are not covered by the QA process. Internal audits verify the compliance to the defined processes, check their effectiveness, identify non-compliances and track the related action points to closure. Persons independent of the area being audited perform internal audits.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The areas that are verified by the Internal Audit process are:</p>
                            <p class="tab-content" style="padding-left: 20px">•	Preparation and review of proposals and contracts</p>
                            <p class="tab-content" style="padding-left: 20px">•	QA process</p>
                            <p class="tab-content" style="padding-left: 20px">•	Training process</p>
                            <p class="tab-content" style="padding-left: 20px">•	Process Management process</p>
                            <p class="tab-content" style="padding-left: 20px">•	Organizational level metrics management activities</p>
                            <p class="tab-content" style="padding-left: 20px">•	Management Review process</p>

                            <p>Randomly selected projects may be added to audit if recommended by the Group Heads / VP Services.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>OM  - </strong>Operations Manager</p>
                                <p><strong>PC  - </strong>Process Consultant</p>
                                <p><strong>GM - </strong>Group Manager</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>TL - </strong>Technical Lead</p>                                                             
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>QA - </strong>Quality Assurance</p> 
                                <p><strong>aPTR - </strong>Attune Process Tailoring Repository</p>
                                <p><strong>SEPG - </strong>Software Engineering Process Group</p>                                
                          </div>
                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>Four cycles of internal audits (one internal audit cycle for every three months) are performed in every financial year. The PC prepares a Quarterly Internal Audit Plan before the start of every quarter. This plan specifies the months in which internal audits will be conducted.</p>
                            <p>Before the start of each internal audit cycle, the PC prepares a detailed Internal Audit Schedule, in which the auditees, the auditors, the date and time of the audit are identified. This schedule is circulated to all auditors and auditees.</p>
                            <p>The identified auditors and auditees meet according to the audit schedule. The auditors interview the auditees to ascertain compliance to the defined processes. Non-compliances are documented. For each area/ project audited, an Internal Audit Report is submitted by the auditor to the PC and the manager of the area/project audited.</p>
                            <p>Auditee managers, in consultation with the PC, identify and document the action plans. Auditee managers implement these action plans, and the PC tracks these actions to completion.</p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td rowspan="3"><strong>Plan</strong></td>
                                            <td><p>Study input documents and determines the following:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The overall frequency of internal audits</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The number of times each area/ group must be audited</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The number of projects that need to be selected randomly in each cycle</p></td>
                                            <td rowspan="3">PC</td>
                                            <td>Audit Plan/SEPG Plan</td>                                    
                                            <td rowspan="3">aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare the Quarterly Internal Audit Plan, ensuring the following:/p>
                                                <p class="tab-content" style="padding-left: 20px">•	At least one internal audit cycle is scheduled every three months/p>
                                                <p class="tab-content" style="padding-left: 20px">•	Every area is covered at least three times a year/p>
                                                <p class="tab-content" style="padding-left: 20px">•	Every cycle covers at least two randomly selected projects/p>

                                                <p>(Form: Internal Audit Plan / SEPG Plan)</p></td>
                                            <%--<td></td>--%>
                                            <td rowspan="2">Quarterly Audit Plan</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Distribute / circulate a copy of the Quarterly Audit plan to relevant parties</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="4"><strong>Schedule</strong></td>
                                            <td><p>Study input documents and determines the following:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The overall frequency of internal audits</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The number of times each area/ group must be audited</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The number of projects that need to be selected randomly in each cycle</p></td>
                                            <td>VP Services, PC, GM</td>
                                            <td rowspan="4">Quarterly Audit Plan</td>                                    
                                            <td rowspan="4">aPTR, SEPG Plan</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Identify auditors to audit the areas and projects. </p>
                                                <p>Guidelines for auditor assignment are as follows:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The auditor should be a trained auditor </p>
                                                <p class="tab-content" style="padding-left: 20px">•	The auditor must be independent of the auditee area/  project (no direct reporting relationship between the auditor and the auditee)</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The auditor must have sufficient knowledge of the functioning of the auditee area/ project</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The auditor must have sufficient technical and application knowledge required.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	If possible, the same auditor from the previous cycle should be retained</p></td>
                                            <td rowspan="3">PC</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Coordinate and communicate with the auditors and the auditees to finalize the dates and times of the internal audits and prepare the schedule for audits.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Circulate / distribute the Internal Audit Schedule to:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	VP Services</p>
                                                <p class="tab-content" style="padding-left: 20px">•	All GMs</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Direct managers/leads of the auditee areas/ projects</p>
                                                <p class="tab-content" style="padding-left: 20px">•	All Auditors</p>

                                                <p>This activity is completed one week before the start of the audit.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>      
                                        </tr>

                                        <tr>
                                            <td rowspan="4"><strong>Conduct</strong></td>
                                            <td><p>Identified auditors study the process documents of the assigned auditee area/ project. Auditors prepare notes, questions and checklists that they will use during the audit.</p></td>
                                            <td>Auditors, PCs</td>
                                            <td>Internal Audit Checklist</td>                                    
                                            <td rowspan="6">aPTR, SEPG Plan, Previous Audit Reports</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The auditor and the auditee meet at the auditee’s workplace on the scheduled date and time.</p>
                                                <p>(Form: Internal Audit Report)</p></td>
                                            <td>Auditor, PC, PM/PL, TL</td>
                                            <td rowspan="3">Internal Audit Report</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The auditor interviews the auditee (using the notes, questions and checklists prepared) to find objective evidence of compliance to the processes.</p> 
                                                <p>Typical items to check for include:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Existence of documents, report and plans </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Existence of properly filled up forms</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Existence of review and approval records</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Timeliness of the action items</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Appropriate knowledge of the processes by the auditee staff for the activities executed by them</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Customer complaints</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Complaints/ dissatisfaction by other groups in attune.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Complaints/ dissatisfaction by vendors/ dealers</p></td>
                                            <td rowspan="2">Auditor, PC</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare the internal audit report with compliance and Non-Compliance details with descriptions of observations, weaknesses and improvement required and circulate it with the relevant parties.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>      
                                        </tr>

                                        <tr>
                                            <td rowspan="3"><strong>Closure</strong></td>
                                            <td><p>Identify proposed action plans for each non-compliance and make sure that the corrective actions are implemented and On completion inform the auditor/PC.</p></td>
                                            <td>PM,PL</td>
                                            <td rowspan="2">Internal Audit Report</td>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Verify and confirm the action has been taken and mark as “closed” in his/ her copy</p></td>
                                            <td>Auditor, PC</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>All Non-Compliance & Corrective Actions are closed within one month of their being raised. </p>
                                                <p>Issues still open for more than one month are escalated by the PC to the GM of the auditee area/ project.</p></td>
                                            <td>PC</td>
                                            <td>Audit Summary Report</td>
                                            <td>Internal Audit Report</td>
                                        </tr>


                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Closure and verification of all issues in the Review Record</p>
                                            <p>•	Receipt of the Internal Audit Report with the attachments by the PC and auditee manager</p>
                                            <p>•	Circulation/ distribution of the Quarterly Internal Audit Summary to all GMs</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>Successful completion of internal reviews, external reviews and sign-offs of SRS and the Acceptance Test Plan </p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Running Project List ( information)</p>
                                            <p>•	Quarterly Audit Plan </p>
                                            <p>•	Internal Audit Reports (of the previous cycle)</p>
                                            <p>•	Project Status Reports for ongoing projects </p>                                            
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Internal Audit Report</p>
                                            <p>•	Internal Audit Schedule</p>
                                            <p>•	Quarterly Audit Plan</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Number of Audit finding per project</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	Criteria different from that defined in this process may be used for random selection of projects for internal audits, for example, customer request for audits for a specific project</p>

                                            <p>•	Auditors may be drawn from outside of attune. In such a case, they should be familiarized with attune’s processes.</p>

                                            <p>•	The PC may appoint someone to confirm the closure of the actions on non-compliances. Such a person is always independent of the area audited.</p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\attune Audit Checklist.xlsx">1. attune Audit Checklist.xlsx</a></p>
                                    <p><a href="References\attune IT Process Validation.xlsx">2. attune IT Process Validation Checklist</a></p>
                                    <p><a href="References\attune SEPG Validation Checklist.xlsx">3. attune SEPG Validation Checklist</a></p>
                                    <p><a href="References\attune SEPG Plan.pptx">4. attune SEPG Plan</a></p>                                                                                                 
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>                                        
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                     

                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>





