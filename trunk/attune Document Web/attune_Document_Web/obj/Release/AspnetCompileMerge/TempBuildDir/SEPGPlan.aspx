﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SEPGPlan.aspx.cs" Inherits="attune_Document_Web.SEPGPlan" %>

<asp:Content ID="sEPGPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Software Engineering Process Group (SEPG) Activities </h1>
                        <h3>Objectives</h3>
                        <div class="data">
                            <p>•	To plan and define the attune project process for SAP and Software Development (SWD), train the delivery teams and other relevant support teams, adhere to industry best practices, pilot and rollout the process. </p>
                            <p>•	Make the process available to the delivery teams for use.</p>
                            <p>•	Evaluate the effectiveness  of the process with collecting the feedback from the project teams and identify opportunities for process improvements.</p>
                            <p>•	Establish Reuse, risks, issues, past data, measurement data and lesson leant repositories and share the same with all respective stakeholders.</p>
                            <p>•	To identify areas for improvements and Process and Technology Innovations, as appropriate, from the industry trend and Business Objectives.</p>
                            <p>•	Improve the client satisfaction reducing the defects slippage to customer.</p>
                            <p>•	Strengthened the auditing process with coverage and completeness to provide early indication to leadership. </p>
                            <p>•	To ensure that project governance and governance artifacts are in place.</p>
                            <p>•	To support the project teams to identify corrective/preventive actions of issues faced.</p>
                            <p>•	To align organization trainings and the business objectives.</p>
                        </div>

                        <div class="data">
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\attune SEPG Plan.pptx">attune SEPG Plan</a></p>                                                                                              
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>    
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div> 

                            <p><a href="https://life.oneattune.com/ce/pulse/user/teams/project_teams/uploaded_files?project_id=1380">Assurance</a></p>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

</asp:Content>

