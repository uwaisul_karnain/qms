﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ArchitectureDesign.aspx.cs" Inherits="attune_Document_Web.ArchitectureDesign" %>

<asp:Content ID="aDPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>
                        <h1>Architecture Design Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>Architecture and Design process explain how project requirements transform into a high level design of the system-to-be and that will evolve a robust architecture for the system. Based on the high level design develop Low level detailed design of the System to be implemented.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>The scope of the Architecture and Design process consist with two major areas</p>
                            <p class="tab-content" style="padding-left: 20px">•	Develop high level design of the system-to-be implemented or refine the existing project an Architecture.</p>
                            <p class="tab-content" style="padding-left: 20px">•	Develop Low level detailed design of the System to be implemented with detail database design and detail system behaviors.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>PAD  - </strong>Project Approach Document</p>
                                <p><strong>SRS  - </strong>System Requirement Specification</p>
                                <p><strong>QATP  - </strong>Quality Assurance Test Plan</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>AD - </strong>Architecture Document</p>
                                <p><strong>DD - </strong>Design Document</p>
                                <p><strong>PC - </strong>Process Consultant</p>
                                <p><strong>PM - </strong>Project Manager</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>AR - </strong>Architect</p>
                                <p><strong>SD - </strong>Software Developer</p>
                          </div>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    
                            <h4>1.	Refine/Define Architecture </h4>   
                            <p>To analyze and define the software architecture based on architecturally significant requirements.  That are typically architecturally significant include performance, scaling, process and thread synchronization, and distribution.</p>
                            <p>Main goals to define an initial sketch of the architecture of the system:</p>
                            <p class="tab-content" style="padding-left: 20px">•	Analyze and define an initial set of architecturally significant elements.</p>
                            <p class="tab-content" style="padding-left: 20px">•	Identify the set of Core Architectural concerns to be addressed, which will form the basis to design Core Technical Services.</p>
                            <p class="tab-content" style="padding-left: 20px">•	Define the layering and organization of the system</p>

                            <h4>2.	Steps for Refine/Define Architecture </h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="">
                                            <td rowspan="4">Refine/Define Architecture</td>
                                            <td><p>•	Consider performance, scaling, process and thread synchronization, and distribution parameters. Analyze and define an initial set of architecturally significant elements.</p>
                                                <p>•	Define the basis to design Core Technical Services. Understand the set of Core Architectural concerns to be addressed. </p>
                                                <p>•	Define Interfaces and  the layering structure of the system.</p>
                                                <p>•	Construct & Assess viability of Architectural Proof-of-Concept is about showing that there exists a solution, which will satisfy the architecturally significant requirements, thus showing that the system, as envisioned, is feasible.</p>
                                            </td>
                                            <td>PM, TL</td>
                                            <td>AD</td>                                    
                                            <td rowspan="4">SOW, PAD</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>If system should adhere to existing architecture (either from a prior project or iteration), understand the minimum impact with best solution. To support new behavior the system change requests may need to be created to change the architecture to account.</p></td>
                                            <td>PM, TL</td>
                                            <td>AD, CR Log</td>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Complete the high-level design of the system and sub-systems by completing the following:</p> 
                                                <p> •	Establish design evaluation criteria (like modularity, portability, scalability, maintainability, usability) and prioritize them</p> 
                                                <p> •	Decide on effective design methods (including activities, tools and techniques); prototyping and simulation may be considered</p>
                                                <p>Design the system and sub-systems using the methods, tools and criteria; ensuring that the design meets the requirements allocated to the system and sub-systems</p>
                                            </td>
                                            <td>PM, TL</td>
                                            <td>One attune Backup</td>  
                                            <%--<td></td>--%>                                  
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Define all Intellectual Properties as part of Architecture Document and get the approval from the respective practice leads.</p> 
                                                <p>Example: Open source tools, free ware, etc.</p></td>
                                            <td>PM, TL</td>
                                            <td>AD</td>  
                                            <%--<td></td>--%>                                  
                                        </tr>

                                    </tbody>
                                    </table>
                                </div>
                            </div>


                            <h4>3.	Detail Design </h4>   
                            <p>To transform system architecture requirements to detail objects/ elements requirement. This sub process should focus main 3 elements.</p>

                            <p>•	Understand and design classes, subsystems and interfaces.</p>
                            <p>•	Transform system architecture requirements to the database schema which fulfills the applications functionality and performance requirements</p>
                            <p>•	To design the User Interface for the system</p>

                            <h4>4.	Steps for Detail Design </h4>

                                     
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="">
                                            <td>Detail Design</td>
                                            <td><p>•	Identify and design classes, subsystems and interfaces.</p>
                                                <p>•	Objects, Classes and subsystems and their interfaces are identified and documented.</p>
                                                <p>•	Identify and document necessary interactions between design elements</p> 
                                                <p>•	Identify user interface of the application based on the business logic.</p>
                                                <p class="tab-content" style="padding-left: 20px">     o	Includes identifying the UI elements, collecting the requirements for User Interface</p>
                                                <p>•	Understating the end user need,  Create Prototype, UI Mockups (visuals) or wire frames</p>
                                                <p>•	Identify and document appropriate Database representation model for system to be (Model the Database)</p>
                                                <p class="tab-content" style="padding-left: 20px">     o	Identify the entities </p>
                                                <p class="tab-content" style="padding-left: 20px">     o	Identify the relationships between the entities</p>
                                                <p class="tab-content" style="padding-left: 20px">     o	Identify the constraints needed</p>
                                                <p class="tab-content" style="padding-left: 20px">     o	Normalize the model</p>
                                                <p class="tab-content" style="padding-left: 20px">     o	Prepare the relationship Diagrams representing the data model</p>
                                                <p>Incorporate design review feedbacks if applicable</p>
                                            </td>
                                            <td>Technical Lead, Architect</td>
                                            <td>DD</td>                                    
                                            <td rowspan="2">AD, SRS, PAD, QATP</td>
                                        </tr>
                                        <tr class="">
                                            <td>Review Detail Design</td>
                                            <td><p>Review detail design document and provide timely feedbacks</p></td>
                                            <td>AR, SD</td>
                                            <td>AD, CR Log</td>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                                              

                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <div>
                                                <p><strong>Refine/Define Architecture</strong></p>
                                                <p>•	Project SOW signed-off</p>
                                                <p>•	Project System Requirement specification</p>
                                                <p>•	Existing project Architecture Document available to validate (only responsibility on refine the Architecture)</p>
                                            </div>
                                            <div class="tab-content" style="padding-top: 20px">
                                                <p><strong>Detail Design</strong></p>
                                                <p>•	Project Architecture Document finalized for the particular phase/release.</p>
                                                <p>•	High level use cases are define for respective phase/release.</p>
                                            </div> 
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <div>
                                                <p><strong>Refine/Define Architecture</strong></p>
                                                <p>•	Defined and Documented Architecture, with patterns and modeling conventions for the system </p>
                                                <p>•	Defined Design Model overview and Deployment Model overview</p>
                                                <p>•	Architectural concerns and core technical services are addressed through selection of appropriate design mechanisms.</p>
                                                <p>•	Design mechanisms identified and mapped to Implementation mechanisms.</p>
                                            </div>
                                            <div class="tab-content" style="padding-top: 20px">
                                                <p><strong>Detail Design</strong></p>
                                                <p>•	Application classes and subsystems and their interfaces are identified and documented</p>
                                                <p>•	All necessary interactions between design elements are documented with Navigation Map</p>
                                                <p>•	Design of the data model is completed.</p>
                                            </div> 
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div>
                                            <p><strong>Refine/Define Architecture</strong></p>
                                            <p>•	Architecture Document</p>
                                            <p>•	Project Engineering </p>
                                            <p>•	SRS</p>
                                        </div>
                                        <div class="tab-content" style="padding-top: 20px">
                                            <p><strong>Detail Design</strong></p>
                                            <p>•	SRS.</p>
                                            <p>•	Business Requirements Specifications.</p>
                                            <p>•	Project Engineering Guidelines</p>
                                            <p>•	UI Guideline</p>
                                            <p>•	Architecture Document</p>
                                        </div> 
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <div>
                                                <p><strong>Refine/Define Architecture</strong></p>
                                                <p>•	Architecture Document</p>
                                            </div>
                                            <div class="tab-content" style="padding-top: 20px">
                                                <p><strong>Detail Design</strong></p>
                                                <p>•	Detail Design Document</p>
                                            </div> 
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <div>
                                                <p><strong>Refine/Define Architecture</strong></p>
                                                <p>•	Effort for developing Architecture </p>
                                                <p>•	Effort spent on Peer Review </p>
                                                <p>•	Number of review comments</p>
                                            </div>
                                            <div class="tab-content" style="padding-top: 20px">
                                                <p><strong>Detail Design</strong></p>
                                                <p>•	Effort for developing Detail Design </p>
                                                <p>•	Effort spent on Peer Review </p>
                                                <p>•	Number of review comments</p>
                                            </div> 
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <div>
                                                <p><strong>Refine/Define Architecture</strong></p>
                                                <p>•	If it is a defect fixing or customer define architecture project this section is out of scope. All other projects it is mandatory.</p>
                                            </div>
                                            <div class="tab-content" style="padding-top: 20px">
                                                <p><strong>Detail Design</strong></p>
                                                <p>•	If it is not a defect fixing project this is a mandatory process for all projects</p>
                                            </div> 
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>                       
                                    <p><a href="References\attune Architecture Design.docx">1. attune Architecture Design</a></p>
                                    <p><a href="References\attune Detail Design.docx">2. attune Detail Design</a></p>
                                    <p><a href="References\attune Integration Plan.docx">3. attune Integration Plan</a></p>                     
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>                                                     
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>
  
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>
