﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="RequirementAnalysis.aspx.cs" Inherits="attune_Document_Web.RequirementAnalysis" %>

<asp:Content ID="requirementAnalysisPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Requirement Analysis Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This process describes the requirement analysis activities in software engineering lifecycle that need to be carried out in software development and maintenance projects.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>Projects are executed in a phased manner and the applicability of the various phases described in this process will depend on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The stage at which the project starts</p>
                            <p class="tab-content" style="padding-left: 20px">•	The expected deliverables</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>
                            <p class="tab-content" style="padding-left: 20px">•	The methodology imposed by the client</p>

                            <p>The requirement analysis activities defined here, along with the above, are used to derive the Project's Process at the beginning of the project (the Define Project's Process procedure in the Project Planning process)</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PM  - </strong>Project Manager</p>
                                <p><strong>AR  - </strong>Architect</p>
                                <p><strong>TL - </strong>Team Lead</p>
                                <p><strong>BA - </strong>Business Anasyst</p>
                                <p><strong>QA - </strong>Quality Assurance</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>UID - </strong>User Interface Designer</p>
                                <p><strong>RM - </strong>Requirement Management</p>
                                <p><strong>RTM  - </strong>Requirement Traceability matrix</p>
                                <p><strong>SRS - </strong>System Requirements Specification</p>
                                <p><strong>RCS  - </strong>Requirement Clarification Sheet</p>
                                
                          </div>
                          <div class="col-xs-6 col-md-4"> 

                                <p><strong>ATP - </strong>Acceptance Test Plan</p>
                                <p><strong>DAR  - </strong>Decision Analysis and Resolution</p>
                                <p><strong>SOW  - </strong>Statement of Work</p>
                                <p><strong>PAD  - </strong>Project Approach Document</p>
                                <p><strong>aPTR  - </strong>attune Process Tailoring Process</p>
                          </div>
                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>The software development lifecycles followed by attune are Agile and Iterative methods. Iterative, in which the waterfall life cycle model (also called the linear sequential or classical model) will be repeated. Therefore, requirement analysis triggered as iterative manner for project which follows the iterative methodology. Agile project on the other hand uses the scrum methodology on such project.</p>
                            <p>The first step in any project comprises elicitation and documentation of the client’s requirements. Requirements are first gathered when the solution proposal is made and contract is signed, as described in the RM process. These initial requirements are then elaborated through elicitation and documentation in the Perform Requirements Analysis procedure described here. The signed-off User Requirements generated form the basis for subsequent activities. Acceptance tests are also defined.</p>
                            <p>All the procedures incorporate work-product reviews required for verification, and the CM activities for version control and establishing and maintaining baselines. Requirements are managed using the RM process (including maintenance of bi-directional traceability).</p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="12"><strong>Analyze Procedure</strong></td>
                                            <td><p>Study the initial requirements as established in Solution Proposal/contract and any relevant client correspondence.</p></td>
                                            <td rowspan="3">PM, BA, TL</td>
                                            <td rowspan="2">SRS</td>                                    
                                            <td rowspan="12">SOW, Solution Proposal, aPTR, PAD</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Identifies the relevant requirements providers from the customer organization to provide information needed for defining requirements for the proposed system.</p>
                                                <p>Requirements providers are selected based on criteria’s  which they;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Have the authority to decide on the requirements</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Have the information needed to detail the requirements, or have adequate access to persons who have the information</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Should adequately represent the stakeholders (business and technical) of the proposed system.</p>

                                                <p>Stakeholders may include various impacted client departments/ divisions, and government and statutory bodies)</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>User requirements are elicited and documented.</p> 
                                                <p>Sub-steps are performed sequentially or in parallel. They may have to be performed multiple times to compile the overall user requirements, which are to be documented Requirement clarification sheet or Product Backlog.</p>
                                                <p>The RTM is updated for bi-directional traceability.</p>
                                                <p>(Template: System Requirements Specification, Product Backlog, Requirement Clarification Sheet)</p></td>
                                            <%--<td></td>--%>
                                            <td>SRS, RCS / Product Backlog, RTM</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Requirement workshops are carried out to elicit customer needs, expectations, constraints and interfaces using suitable techniques from the identified requirements providers.</p>
                                                <p>The techniques used could include a combination of:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Individual/Group meetings </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Interviews</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Questionnaires and Surveys</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Technology demonstrations</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Prototyping</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Wireframes and Screen Mockups </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Study of organization procedures</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Study of existing systems </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Reverse engineering</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Study of correspondence, problem reports, etc.</p>

                                                <p>The data collection and analysis is documented and retained.</p>
                                                <p>(Template: Requirement clarification sheet, Product Backlog)</p></td>
                                            <td>BA, UID, PM</td>
                                            <td>RCS,Product Backlog</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Consolidate inputs obtained from the previous sub-step. In the process:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	missing information is identified and obtained</p>
                                                <p class="tab-content" style="padding-left: 20px">•	conflicting requirements are identified and conflicts are resolved</p>
                                                <p class="tab-content" style="padding-left: 20px">•	expectations and constraints with respect to design are identified </p>
                                                <p class="tab-content" style="padding-left: 20px">•	expectations and constraints with respect to various testing activities are identified </p>

                                                <p>All the above is done with the involvement of all requirements providers identified.</p></td>
                                            <td>BA</td>
                                            <td>SRS</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare SRS document based on the analysis done and update RTM</p>
                                                <p>(Template : System Requirements Specification, Requirement Traceability Matrix )</p></td>
                                            <td>BA</td>
                                            <td>SRS RTM</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Review of SRS document. The review comments are incorporated and verified.</p></td>
                                            <td>TL, PM, BA</td>
                                            <td>SRS RTM</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Derive and document sub-system level requirements and functionality to compile the overall user requirements in SRS. </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Develop an overall architecture of the system by breaking the system into sub-systems.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify the requirements of each sub-system in technical terms.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify additional requirements derived from design constraints. This may involve identifying changes to initial requirements in the SRS</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify design constraints and dependencies of each sub-system.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify interfaces between the sub-systems and the external environment.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Develop interface requirements</p>
                                                <p>(Template : System Requirements Specification)</p></td>
                                            <td>BA, PM, AR</td>
                                            <td>SRS</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Analyze and Establish operational concepts and scenarios, including functionality, performance, maintenance, support and disposal. Consider:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Environment in which the system will operate</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The typical transactions that will be processed by the system</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The exceptional conditions that the system will be subjected to</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Detailed functions</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Grouping of functions</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Time-sequencing of functions</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Allocation of functions to sub-systems</p></td>
                                            <td rowspan="3">BA</td>
                                            <td rowspan="3">SRS</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Analyze requirements to achieve balance. This would involve:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Assessments of risks related to requirements</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Usage of models (like break-even)</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Simulation/ prototyping of parts of the system</p>
                                                <p>Ensure that all stakeholder needs and constraints are addressed.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Validate requirements to ensure that the system will perform in the user’s environment as appropriate. This could involve:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Review of prototypes and simulations</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Review of requirements gathered till now</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Prioritizing the requirements</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Review the complete SRS. The review comments are incorporated and verified.</p></td>
                                            <td>PM, Client</td>
                                            <td>SRS Review Comments</td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="6"><strong>Acceptance Testing</strong></td>
                                            <td><p>The SRS is used to derive a detailed Acceptance Test Plan. This may be done jointly with the client.</p>
                                                <p>The Acceptance Test Plan is to be executed by the client to gain confidence in the system and to accept it, and should clearly specify the criteria for successful acceptance testing.</p>
                                                <p>The Acceptance Test Plan contains traceability information with linkages to the SRS. The sub-steps given are used to prepare an Acceptance Test Plan</p>
                                                <p>(Template: Acceptance Test Plan)</p></td>
                                            <td>BA/QA</td>
                                            <td rowspan="4">ATP</td>                                    
                                            <td rowspan="6">SOW, Solution Proposal, SRS</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Determine products for user to use during acceptance testing are selected:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Determine the user requirements and functionality.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Determine the sub-systems and units to be tested.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Select the testing methodology</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Prepare the User Stories for Acceptance Testing</p></td>
                                            <td rowspan="2">BA, AR</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>User acceptance testing environment is defined:
                                                <p class="tab-content" style="padding-left: 20px">•	Identify acceptance test environment requirements
                                                <p class="tab-content" style="padding-left: 20px">•	Identify client/ user supplied components to be used for or integrated into the acceptance testing
                                                <p class="tab-content" style="padding-left: 20px">•	Identify test equipment and tools
                                                <p class="tab-content" style="padding-left: 20px">•	Identify resources required for acceptance testing</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepares the Acceptance Test Plan document based on the analysis done.</p>
                                                <p>(Template : Acceptance Test Plan)</p></td>
                                            <td>BA/QA</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Update the RTM with acceptance test information and incorporates links needed for bi-directional traceability.</p></td>
                                            <td>BA, AR</td>
                                            <td>RTM</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Review the complete ATP. The review comments are incorporated and verified. </p></td>
                                            <td>PM,  Client</td>
                                            <td>ATP Review Comments</td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="3"><strong>Baseline Requirement</strong></td>
                                            <td><p>The SRS and the Acceptance Test Plan are renewed by the External party (Client) and review comments are incorporated.</p></td>
                                            <td>PM, BA/QA, Client</td>
                                            <td>SRS, ATP, Review Records</td>                                    
                                            <td rowspan="3">SOW, Solution Proposal, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Sign-off / confirmation is taken after the external review comments are incorporated</p></td>
                                            <td>PM,  Client</td>
                                            <td>ATP Review Comments</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM ensures that the SRS and the Acceptance Test Plan are baselined as soon as they are reviewed OK.</p></td>
                                            <td>PM</td>
                                            <td>SRS, ATP</td>
                                            <%--<td></td>--%>
                                        </tr>

                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Acceptance of the Solution Proposal or Signing of a Contract</p>
                                            <p>•	Statement of Work</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>Successful completion of internal reviews, external reviews and sign-offs of SRS and the Acceptance Test Plan </p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Acceptance of proposed solution/ signing of contract (SOW) </p>
                                            <p>•	Requirements database </p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	SRS</p>
                                            <p>•	Acceptance Test Plan</p>
                                            <p>•	Product Backlog</p>
                                            <p>•	Requirement Clarification Sheet</p>
                                            <p>•	RTM</p>
                                            <p>•	Review Records & Review Log</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort for Requirement Analysis</p>
                                            <p>•	Effort for Reviews</p>
                                            <p>•	Number of review comments</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The project may decide to use client defined templates and standards for the deliverables from software life cycle phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>

                                            <p>•	Certain procedures of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented in the Project’s Process.</p>

                                            <p>•	In case the client or another team representing the client is performing some parts of the lifecycle and providing deliverables to the project team, these deliverables are reviewed for completeness.</p>

                                            <p>•	For small projects (less than 3 months calendar time and less than 6 person months effort), the project may merge phases and the deliverables of phases. The merged deliverables should cover all the deliverables of the phases being merged. Such tailoring is documented in the Project’s Process.</p>

                                            <p>•	Client Sign-off of User Requirements and Acceptance Plan will be decided at the project planning stage by the PM/GM since most of the time we do not deal with the end-client and some clients will only respond when they don’t agree on some contents</p>

                                            <p>•	DAR process to decide the designed method will be used only when we have the option of deciding it, since on occasions designs are given by customer him self</p> 
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <h3>Document Templates</h3>                       
                                <p><a href="References\attune System Requirement Specification.docx">1. attune System Requirement Specification</a></p>
                                <p><a href="References\attune Acceptance Test Plan.docx">2. attune Acceptance Test Plan</a></p>
                                <p><a href="References\attune Product Backlog.xls">3. attune Product Backlog</a></p>
                                <p><a href="References\attune Requirement Clarification Tracking Sheet.xls">4. attune Requirement Clarification Tracking Sheet</a></p>
                                <p><a href="References\attune RTM.xlsx">5. attune Requirement Traceability Matrix</a></p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Guidelines</h3>                                                     
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Samples</h3>
                          </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>




