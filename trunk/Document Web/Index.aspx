﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="_Default" masterPageFile="~/MasterPage.master" %>

<asp:Content ID="HLDPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">
  
                    <!-- Page Content -->
                    <div id="page-content-wrapper" >
                        <div class="col-xs-12 text-center"><h1 class="main-title">attune Processes</h1></div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>
                                        <div>                                           

                                            <!-- Page Content -->
                                            <div id="page-content-wrapper" class="dash-page">
                                                 
                                                <div class="container-fluid">
                                                    <div class="row">
                                                       
                                                        <div class="col-xs-12 col-lg-3 text-left">
                                                            <div class="comm-radius">
                                                                <h2>Implementation Methodology</h2>
                                                                <div class="data">
                                                                    <h3><a href="AIRMethodDiagram.aspx">Enterprise Applications</a></h3>                            
                                                                    <%--<p><strong>aIR Methodology</strong></p>
                                                                    <p>aIR Description</p>--%>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-lg-3 text-left">
                                                            <div class="comm-radius">
                                                                <h2>Development Methodology</h2>
                                                                <div class="data">
                                                                    <h3><a href="AgileMethodDiagram.aspx">Agile Methodology</a></h3>
                                                                    <%--<p><strong>Agile Methodology Description</strong></p>
                                                                    <p>Agile Description</p>--%>                            
                                                                </div>
                                                                <div class="data">
                                                                    <h3><a href="IterativeMethodDiagram.aspx">Iterative Methodology</a></h3>                                                                    
                                                                    <%--<p>Iterative Methods Description</p>--%>
                                                                </div>
                                                                <div class="data">
                                                                    <h3><a href="WaterfallMethodDiagram.aspx">Waterfall Methodology</a></h3>                                                                    
                                                                    <%--<p>Iterative Methods Description</p>--%>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-lg-3 text-left">
                                                            <div class="comm-radius">
                                                                <h2>Solutioning/Consulting Methodology</h2>
                                                                <div class="data">
                                                                    <%--<h3><a href="AIRMethodDiagram.aspx">Enterprise Applications</a></h3>   --%>                         
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-lg-3 text-left">
                                                            <div class="comm-radius">
                                                                <h2>General</h2>
                                                                    <div class="data">
                                                                        <h3><a href="ProcessTailoring.aspx">Process Tailoring</a></h3>
                                                                        <h3><a href="RiskManagement.aspx">Risk Management</a></h3>
                                                                        <h3><a href="ProjectPlanning.aspx">Project Planning</a></h3>
                                                                        <h3><a href="ProjectMonitoringAndControl.aspx">Project Monitoring & Control</a></h3>    
                                                                        <h3><a href="MeasurementandAnalysis.aspx">Measurements and Analysis</a></h3>                                                                        
                                                                        <h3><a href="DecisionAnalysisResolution.aspx">Decision Analysis Resolution</a></h3> 
                                                                        <h3><a href="ConfigurationManagement.aspx">Configuration Management</a></h3>                                                                        
                                                                        <h3><a href="ProjectClosure.aspx">Project Closure</a></h3>
                                                                        <h3><a href="TrainingProcess.aspx">Training Process</a></h3>
                                                                        <h3><a href="Policy.aspx">Organization Policy</a></h3>
                                                                        <h3><a href="ProcessManagement.aspx">Process Management</a></h3>                        
                                                                        <h3><a href="ProcessImprovement.aspx">Process Improvements</a></h3>
                                                                        <h3><a href="InternalAudit.aspx">Internal Auditing</a></h3>
                                                                        <h3><a href="ExternalAudit.aspx">External Auditing</a></h3>
                                                                        <h3><a href="SEPGPlan.aspx">SEPG</a></h3> 
                                                                    </div>
                                                            </div>
                                                        </div>
                    
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /#page-content-wrapper -->


                                        </div>                                    

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /#page-content-wrapper -->

</asp:Content>