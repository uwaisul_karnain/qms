﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProcessManagement.aspx.cs" Inherits="ProcessManagement" %>

<asp:Content ID="processManagementPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Process Management Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The objective of process management function in attune is to ensure that appropriate processes and work environment standards are defined and maintained for solution developments and related support functions.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>Process management involves: </p>
                            <p class="tab-content" style="padding-left: 20px">•	Deciding the focus of organizational process activities</p>
                            <p class="tab-content" style="padding-left: 20px">•	Planning for process management activities</p>
                            <p class="tab-content" style="padding-left: 20px">•	Definition and maintenance of policies, processes, procedures, templates, forms, checklists and guidelines</p>
                            <p class="tab-content" style="padding-left: 20px">•	Performing periodic process assessments to identify the strengths and weaknesses with respect to identified models</p> 
                            <p class="tab-content" style="padding-left: 20px">•	Improving work environment </p>
                            <p class="tab-content" style="padding-left: 20px">•	Tracking process improvement actions to closure</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SEPG  - </strong>Software Engineering Process Group</p>
                                <p><strong>BDGH  - </strong>Business Development Group Head</p>
                                <p><strong>PMO - </strong>Project Management Office</p>
                                <p><strong>PC - </strong>Process Consultant</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PL  - </strong>Project Lead</p>
                                <p><strong>PCR - </strong>Process Change Request</p>
                                <p><strong>PI  - </strong>Process Improvements</p>
                          </div>
                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>The purpose of process management process is to have effective and efficient processes to the organization. The activities required for this are planned as a Software Process Improvement project. This plan addresses on-going process definition and maintenance, the maintenance data repositories, and the maintenance of work environment standards. In addition to this, processes are assessed periodically, and the assessment and other data are used to identify areas for process improvement.</p>

                            <p>The Process Consultant and Project Management Office for Processes decide the process focus of the year. Based on this, Process Consultant prepares a Software Engineering Process Group Plan (SEPG Plan). The SEPG Plan is reviewed and approved by the Operations Manager.</p>

                            <p>Typical activities that are planned and executed as part of the SEPG activities are:</p>

                            <p class="tab-content" style="padding-left: 20px">•	Definition and maintenance of policies, processes/ procedures, checklists, templates, forms and guidelines</p>
                            <p class="tab-content" style="padding-left: 20px">•	Definition and maintenance of work environment standards</p>
                            <p class="tab-content" style="padding-left: 20px">•	Coordination with the Training Team for training required for performing process management activities</p> 
                            <p class="tab-content" style="padding-left: 20px">•	Coordination with the Training Team for training to be provided to the staff on the defined processes</p>
                            <p class="tab-content" style="padding-left: 20px">•	Management of the repositories with appropriate indexing and identification of exemplary artifacts for ease of use by projects and related supporting functions.</p>
                            <p class="tab-content" style="padding-left: 20px">•	Coordination of periodic internal and external process assessments</p>

                            <p>The Process Consultant prepares Quarterly status reports tracking the progress of the process management activities. Process management activities are also subject to periodic internal audits.</p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="4"><strong>Prepare and Maintain SEPG Plan</strong></td>
                                            <td><p>Process Consultant and PMO team collect the annual business plans / Process Improvements from senior management.</p>
                                                <p>The focus for process activities for the year. This covers:</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Process performance objectives for the year, (e.g., targets for customer satisfaction, Defect rate, Effort variance)</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Adoption of process models and standards</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Adoption of external work environment standards</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Other major process-related initiatives for the year (e.g., adoption of new methodologies, tools, etc.)</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Major milestones (assessments, certifications) for process management</p></td>
                                            <td rowspan="4">PC, OM,  PMO Team</td>
                                            <td rowspan="4">SEPG Plan</td>                                    
                                            <td rowspan="4">aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Based on the defined focus, PC prepare the SEPG plan.</p>
                                                <p>The SEPG plan includes:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The process improvement approach and objectives for the year</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Roles and responsibilities</p>
                                                <p class="tab-content" style="padding-left: 20px">•	New initiative Road Maps</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Process Improvements and Milestones</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Management and Senior Management Review the SEPG Plan</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>OM review and approve the SEPG Plan</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="4">Define and Maintain Processes</td>
                                            <td><p>This procedure handles Process Change Requests (PCRs). PCRs may relate to creation or modification of process assets like policies, processes, life cycle descriptions, procedures, templates, forms, checklists and guidelines. Any member of the organization has the ability to raise a PCR.</p>
                                            <td>PMO, OM, PC</td>
                                            <td rowspan="4">SEPG Plan</td>
                                            <td rowspan="4">aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Every 3 months’ time, PCRs are validated and evaluated by the PMO team and decide the approval or rejection of the PCRs</p></td>
                                            <td rowspan="3">PC, PM/PL </td>
                                            <%--<td></td>--%>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>These validated and approved PCRs are updated to the SEPG Plan as new PIs and Milestones</p>
                                                <p>Respective changes are informed to requested users and groups</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The status of the process improvements are maintained in the SEPG plan. The targeted completion date, percentage completed or implemented are maintained and tracked to closure.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="4">Perform Process Assessments</td>
                                            <td><p>Based on the SEPG Plan, the process consultant scopes the assessment in terms of:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The organizational scope</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Sponsors for the assessment</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The model and subset of the model being assessed for</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Methodology to be used for the assessment</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Duration of the assessment</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Involvement of the employees during the assessment</p></td>
                                            <td rowspan="4">PMO, OM, PC, PM/PL, External Auditor</td>
                                            <td rowspan="4">Audit Report</td>
                                            <td rowspan="4">SEPG Plan, Previous Audit Findings</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PC coordinate with the PMO and OM in terms of approving the budget, identifying the projects for the assessment, reserving internal resources and schedule.</p>
                                                <p>The PC then inform the assessment teams and related members about the schedule and preparation areas for the assessment</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The Process Improvement Team, along with the PC, provides various briefings and training required for the assessment team members and assessment participants. The Process improvement team supports and guide the assessment team in improving the identified PIs and tracking them to closure with each assessment team.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The assessment take place as scheduled with the auditor, PCs and assessment team members.</p>
                                                <p>The assessor prepare a summary of the assessment finding and communicate to PC and the attune PMO.</p>
                                                <p>Relevant assessment team members (PM/PLs, TLs) are informed about the area of improvements required and suggestions.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="4"><strong>Coordinate and Monitor</strong></td>
                                            <td><p>The SEPG plan is updated according to the findings assessed by the external auditor.</p></td>
                                            <td rowspan="3">PC, PM/PL</td>
                                            <td rowspan="4">Internal Audit Report</td>
                                            <td rowspan="4">SEPG Plan, Previous Internal and External Audit Findings</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The internal process improvement assessments are conducted by the internal process improvement team in every month.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The assessment team is informed about the schedule, duration for the audit and area reviewed.</p>
                                                <p>The team will be assessed to check whether;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	SEPG plan process improvements items are implemented and tracked to closure</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The previous external assessment findings are implemented tracked to closure</p>  
                                                <p class="tab-content" style="padding-left: 20px">•	The previous internal assessment findings are implemented tracked to closure</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>An internal audit report is prepared by the assessment team and presented to the attune senior management</p></td>
                                            <td>PC</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Organization Business Objective setting</p>
                                            <p>•	Annual business plans / Process Objectives defined</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Approval of the SEPG Plan 
                                            <p>•	Completion of the PCR and preparation of the roll-out action points</p>
                                            <p>•	Presentation of the assessment report and conversion of the findings to SEPG Process Improvements and Milestones</p>
                                            <p>•	Completion of presentation and discussion of the Process Management Status Report</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Annual business plans / Process Objectives </p>
                                            <p>•	SEPG Plan</p>
                                            <p>•	Defined policies, processes, procedures, templates, forms, checklists and guidelines</p>
                                            <p>•	Assessment Reports ( Internal and External Audit Reports)</p>
                                            <p>•	Process Change Requests (PCRs) received</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	SEPG Plan</p> 
                                            <p>•	Process Change Request</p>
                                            <p>•	Updated SEPG Process Improvement Items and Milestones</p>
                                            <p>•	Modified policies, processes, procedures, templates, forms, checklists and guidelines</p>
                                            <p>•	Assessment reports (Internal and External)</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Number of Audit finding per project</p>
                                            <p>•	Number of PCR’s integrated to the system</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	Process Improvements are identified, reviewed and planned in the SEPG plan.</p>
                                            <p>•	The review process and approval process by the Operations Manager is stated in the SEPG Plan</p>
                                            <p>•	The Process Improvements action items stated in SEPG plan are executed, monitored and implemented by the PCs of SEPG</p> 
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\attune SEPG Plan.pptx">1. attune SEPG Plan</a></p>                                                                                                  
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>       
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                     
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>

