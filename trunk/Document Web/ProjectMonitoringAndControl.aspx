﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProjectMonitoringAndControl.aspx.cs" Inherits="ProjectMonitoringAndControl" %>

<asp:Content ID="projectMonitoringControlPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project Monitoring and Control Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>attune executes SAP developments, new technology software developments, enhancements and maintenance projects for its clients. The term “project” is used to describe the full set of activities from the time the proposal is accepted (or a contract is signed) to the time all the software and services are delivered according to the accepted SOW/ contract.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The scope of the Monitoring and Control is "To track project progress, schedule, risks, commitments, stakeholder involvement, dependencies, issues, Corrective and preventive actions and metrics; to take corrective measures when any of the parameters deviate from the plan</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>OM  - </strong>Operations Manager</p>
                                <p><strong>PC  - </strong>Process Consultant</p>
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>CM  - </strong>Configuration Manager</p>
                                <p><strong>TM - </strong>Training Manager</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>QA - </strong>Quality Assurance</p>
                                <p><strong>RM - </strong>Risk Management</p>
                                <p><strong>M&A  - </strong>Measurement and Analysis</p>
                                <p><strong>WBS - </strong>Work Breakdown Structure</p>
                                <p><strong>SOW  - </strong>Statement Of Work</p>
                                
                          </div>
                          <div class="col-xs-6 col-md-4">  
                                <p><strong>PAD - </strong>Project Approach Document</p>
                                <p><strong>DAR - </strong>Decision Analysis and Resolution</p>
                                <p><strong>QA Plan  - </strong>Quality Assurance Plan</p>
                                <p><strong>CM Plan - </strong>Configuration Management Plan</p>
                                <p><strong>PSTS - </strong>Project Status Tracking Sheet</p>
                          </div>

                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>Once the project is planned, the PM monitors the progress against the plan to identify and resolve issues in a timely manner. For this, the PM gathers the information needed, analyzes it and identifies deviations that are significant. Corrective actions are identified for all issues and tracked to closure. Corrective actions may include re-planning and establishing new agreements</p>

                            <p>When the project is completed, the PM gathers the project related data and documents. The PM prepares a Project Closure Report and hands over the project-related work-products, documents, records, and the Project Closure Report to the PC.</p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="8"><strong>Execute & Monitor</strong></td>
                                            <td><p>The PM executes the project using the PAD and the detailed schedule, and monitors the progress against these to identify deviations. The issues identified are documented in the Project Status Tracking Sheet so that timely corrective actions can be identified and taken.</p></td>
                                            <td rowspan="8">PM, TL, OM, PC</td>
                                            <td rowspan="5">PSTS,  Project Skill Tracking Sheet, Project Schedule, Time Sheet</td>                                    
                                            <td rowspan="8">SOW, PAD</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>On a periodic basis, the PM updates the detailed schedule for the next period based on the current status and planned activities. Consistency is maintained between the detailed schedule and the PAD.</p>
                                                <p>The PM identifies activities on the critical path and informs the team about these activities.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM ensures that project team members join the project as per the plan and the Team Requisition raised earlier.</p>
                                                <p>PM  briefs new project team members on:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Project scope</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Client</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Environment</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Team roles and responsibilities</p>
                                                <p>Training identified in the PAD is provided in coordination with the TM and included in the Training Plan. Project Skills Tracker is updated as people join the project and gain skills through training or on-the-job experience.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM allocates tasks to the project team members and update detailed schedule.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM gathers information on the progress from team members using the reporting mechanism specified in the PAD at the defined periodicity. This typically includes timesheets and the delivery schedule. The PM discusses the status of other activities as required with the project team members to obtain the data required to monitor and track the project progress. The overall status of the project is reviewed in team meetings and prepared to report weekly basis.</p>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                       <tr>
                                            <%--<td></td>--%>
                                            <td><p>The PM compares actual progress with the Project Status Tracking Sheet (PSTS) - milestones and the detailed schedule to check for variances. This comparison is performed for:</p>
                                                <p  class="tab-content" style="padding-left: 20px">•	Completion of activities and achievement of milestones against plan </p>
                                                <p  class="tab-content" style="padding-left: 20px">•	Actual against planned effort </p>
                                                <p  class="tab-content" style="padding-left: 20px">•	Availability of resources against planned requirements (such as people, hardware, software, work environment)</p>
                                                <p  class="tab-content" style="padding-left: 20px">•	Resource utilization against planned utilization </p>
                                                <p class="tab-content" style="padding-top: 20px">The PM analyses the variances to understand their reasons and impact on the project. Significant variances or other issues that may need corrective actions are tracked in PSTS.</p></td>
                                            <%--<td></td>--%>
                                            <td rowspan="3">PSTS</td>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM sends the report of milestone/ phase-end review along with contributions to PC</p>
                                                <p>Issues identified are noted in the Issues Tracking Register.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM reviews the PSTS and the Project plans to need of any additional communication to the relevant stakeholders and communicate the same.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr class="">
                                            <td rowspan="7">Manage Corrective Actions to Closure</td>
                                            <td><p>On an ongoing basis, the PM uses the PSTS to perform the following steps. </p></td>
                                            <td rowspan="7">PM, GM</td>
                                            <td rowspan="4">PSTS,  Skill Tracker,Training Plan</td>
                                            <td rowspan="14">PAD, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>PM examines the tracking sheets to identify issues where corrective actions have not yet been defined. </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM identifies possible corrective actions to ensure that the project stays on track. </p>
                                                <p>This may include:</p>
                                                <p>•	Re-training team members/ acquiring team members with different skills</p>
                                                <p>•	Considering use of tools/ templates</p>
                                                <p>•	Changing the Project's Process</p>
                                                <p>•	Changing the PAD and other project plans</p>
                                                <p>•	Changing the detailed schedule because of rescheduling some activities, and adding or deleting others</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>In case identified corrective actions impact the commitments made to the client, the PM discusses these with the OM before they are discussed with or communicated to the client. </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>For identified corrective actions, the PM tracks the actions to closure. </p></td>
                                            <%--<td></td>--%>
                                            <td rowspan="3">PSTS</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM analyses corrective actions to determine their effectiveness. Results of corrective actions are documented and analyzed with respect to the planned results of the corrective actions.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM gathers the project related data and documents other than those already baselined.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr class="">
                                            <td rowspan="7">Close Project</td>
                                            <td><p>The PM collects feedback from persons within and outside the project as required by the Project Closure Report.</p>
                                                <p>(Template: Project Closure Report )</p></td>
                                            <td rowspan="7">PM, GM, PC</td>
                                            <td rowspan="7">PSTS,  Project Closure Report</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM, along with the team prepares the Project Closure Report.</p>
                                                <p>(Template: Project Closure Report)</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM ensures that project end backups are taken. Generally it happens through organizational backup procedure.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM submits the Project Closure Report to the OM.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The OM studies the Project Closure report and decides whether a project end de-briefing is required. If so, the OM arranges and coordinates the project end de-briefing that is attended by the PM, PC, Analysts/ Designers, etc. </p>
                                                <p>The meeting is recorded and these minutes are attached to the Project Closure Report.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM hands over project related work-products, documents, records, and the Project Closure Report to the PC. This is done in terms of keeping all updated documents in Oneattune.com / VSS</p>
                                                <p>Refer : attune Process Management Process.docx</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM and the team are considered de-allocated from the project which happens automatically in the current process, and there will not be a formal de-allocation note or notification</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Successful completion of project high level plan</p>
                                            <p>•	On-going till project is completed</p>
                                            <p>•	Project started as per contract/ proposal</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Project Closure Report and other project material handed over to the OM </p>
                                            <p>•	Closure and verification of all issues in the Review Record </p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	PAD / Project Charter</p>
                                            <p>•	Project Management Related Plans</p>
                                            <p>•	Detailed Project Schedule</p>
                                            <p>•	Project Status Tracking Sheet (Risks, Issues, Decisions, Dependencies, Milestone, CR logs)</p>
                                            <p>•	Minutes of Meetings</p>
                                            <p>•	Time entry on PSA system</p>
                                            <p>•	All project data including plans, status reports and working papers, Proposals, Client Meeting Minutes</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	Minutes of Meeting from Internal Discussions, Client Discussions, etc.</p>
                                            <p>•	Detailed Project Schedule</p>
                                            <p>•	Time Sheets</p>
                                            <p>•	Records of phase-end / sprint / milestone reviews</p>
                                            <p>•	Project Skill Tracker</p>
                                            <p>•	Project Status Tracking Sheet (Risks, Issues, Decisions, Dependencies, Milestone, CR logs)</p>
                                            <p>•	Project Closure Report (with attached minutes of project de-briefing)</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Number of KM records submitted to the attune KM group</p>
                                            <p>•	Metric deviations and corrective actions</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The Team Member’s Weekly Progress Report may not be collected in projects with less than 5 members, provided a weekly team meeting is held with all team members.</p>
                                            <p>•	There may be no phase-end/ milestone reviews for projects of less than 2 month duration or for projects with less than 3 team members</p>
                                            <p>•	For long-duration maintenance projects, the milestone reviews or sprint reviews may be scheduled on time-duration basis (e.g., quarterly) instead of phase ends.</p>
                                            <p>•	For projects of less than three months duration, the Project Status Report needs to be prepared every two weeks.</p> 
                                            <p>•	Project’s Closure activities will not be applicable for projects that are running on a continuous basis, where project management and direction is decided by customer</p> 
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                        
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\attune Project Status Tracking Sheet.xlsx">1. attune Status Tracking Sheet</a></p>
                                    <p><a href="References\attune Project Skills Tracker.xlsx">2. attune Skill Tracker</a></p>                                      
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>       
                                    <p><a href="References\attune Guidelines for Detailed Scheduling.docx">1. attune Guidelines for Detailed Scheduling</a></p> 
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                                                                                                               

                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>

