﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AIRMethodDiagram.aspx.cs" Inherits="SAPWebPages_AIRMethodDiagram" %>

<asp:Content ID="aIRDiagramPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>
                        <div class="disc">
                            <div class="diagram">
                                <div class="litetooltip-hotspot-wrapper" style="max-width: 1020px">
                                    <div class="litetooltip-hotspot-container" style="padding-bottom: 75%">

                                        <img src="images/aIRDiagram.jpg" alt="" usemap="#Map" />
                                        <map name="Map" id="Map">
                                    <%--        <area alt="" title="" href="ProjectPreparation.aspx" shape="poly" coords="215,68,239,109,215,148,339,144,364,106,338,69" />
                                            <area alt="" title="" href="BusinessBluePrinting.aspx" shape="poly" coords="350,67,378,109,353,145,470,147,496,107,469,70" />
                                            <area alt="" title="" href="Build.aspx" shape="poly" coords="484,68,510,110,485,146,592,147,617,107,593,69" />
                                            <area alt="" title="" href="IntegrationTestingAndUAT.aspx" shape="poly" coords="603,68,625,106,602,145,724,145,750,107,725,69" />
                                            <area alt="" title="" href="FinalPreparationAndCutover.aspx" shape="poly" coords="736,68,764,106,738,147,864,147,890,107,863,68" />
                                            <area alt="" title="" href="#" shape="poly" coords="881,68,909,108,883,145,999,145,1028,109,999,69" />
                                            <area alt="" title="" href="#" shape="poly" coords="215,67,240,110,215,147,340,143,365,108,342,69" />--%>

                                            <area alt="" title="" href="ProjectPreparation.aspx" shape="poly" coords="115,65,289,64,326,118,287,171,115,170,149,114,150,116,152,119" />
                                            <area alt="" title="" href="BusinessBluePrinting.aspx" shape="poly" coords="312,169,472,169,510,118,471,65,311,65,346,116" />
                                            <area alt="" title="" href="Build.aspx" shape="poly" coords="495,173,665,171,702,117,663,62,497,62,534,116" />
                                            <area alt="" title="" href="IntegrationTestingAndUAT.aspx" shape="poly" coords="669,173,846,173,886,119,847,62,669,62,705,117" />
                                            <area alt="" title="" href="FinalPreparationAndCutover.aspx" shape="poly" coords="870,168,1044,168,1081,116,1042,64,869,63,902,116" />
                                            <area alt="" title="" href="#" shape="poly" coords="1070,169,1227,169,1265,116,1227,64,1067,62,1103,116" />
   
                                        </map>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

</asp:Content>

