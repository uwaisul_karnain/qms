﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProcessImprovement.aspx.cs" Inherits="ProcessImprovement" %>

<asp:Content ID="processImprovementDiagramPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Process Management Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This process handles the Process Improvement Requests (PIRs). PIRs may relate to creation or modification of process assets like policies, processes, life cycle definitions, templates, forms, checklists. </p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PIR  - </strong>Process Improvement Request</p>                                
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PSTS  - </strong>Process Status Tracking Sheet</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PI - </strong>Process Improvement</p>
                          </div>
                        </div>

                          <div class="data">
                            <h2>Procedure</h2>    

                            <p class="tab-content" style="padding-left: 20px">•	Any member of the organization has the ability to raise a PIR.</p>
                            <p class="tab-content" style="padding-left: 20px">•	All PIRs should be updated to attune Process Improvement Requests Sheet.</p>
                            <p class="tab-content" style="padding-left: 20px">•	Every 3 months’ time, PIRs are validated and evaluated by the panel of respective Practice Heads and decide the approval or rejection of the PIRs.</p> 
                            <p class="tab-content" style="padding-left: 20px">•	If a particular PIR is rejected by the panel, the rejected PIR details are communicated with the respective team members.</p>
                            <p class="tab-content" style="padding-left: 20px">•	These validated and approved PIRs are incorporated to organization process improvement list and respective team will track the tasks, milestones and etc.</p>
                            <p class="tab-content" style="padding-left: 20px">•	The targeted completion date, percentage completed or implemented are maintained and tracked to closure.</p>
                            </div>

                        <div class="disc">
                            <div class="diagram"  style="padding-top: 60px">
                                <div class="litetooltip-hotspot-wrapper" style="max-width: 1020px">
                                    <div class="litetooltip-hotspot-container" style="padding-bottom: 50%">
                                    <img src="images/ProcessImprovement.jpg" class="img-responsive" data-width="1020" data-height="665" />

                                    <div class="hotspot" style="top: 5.359477124183006%; left: 2.892156862745098%; width: 4.215686274509804%; height: 17.647058823529413%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748164896" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="2.892156862745098" data-hotspot-y="5.359477124183006" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    <div class="hotspot" style="top: 23.398692810457515%; left: 2.696078431372549%; width: 4.6078431372549025%; height: 4.836601307189543%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748173260" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="2.696078431372549" data-hotspot-y="23.398692810457515" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    <div class="hotspot" style="top: 29.673202614379086%; left: 2.696078431372549%; width: 4.313725490196078%; height: 31.372549019607842%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748179444" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="2.696078431372549" data-hotspot-y="29.673202614379086" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    <div class="hotspot" style="top: 63.39869281045751%; left: 2.401960784313726%; width: 4.509803921568627%; height: 30.84967320261438%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748184700" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="2.401960784313726" data-hotspot-y="63.39869281045751" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    <div class="hotspot" style="top: 14.37908496732026%; left: 7.990196078431372%; width: 10.392156862745098%; height: 6.405228758169934%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748192323" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="7.990196078431372" data-hotspot-y="14.37908496732026" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    </div>
                                    </div>
                            </div>
                        </div>

                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\attune Process Improvement Requests.xlsx">1. attune Process Improvement Requests</a></p>   
                                    <p><a href="References\attune Define and Execute Process PSTS.xlsx">2. attune Define and Execute Process PSTS</a></p>                                                                                               
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>    
                                    <p><a href="References\attune SEPG Plan.pptx">1. attune SEPG Plan</a></p>   
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>     
                    
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

</asp:Content>



