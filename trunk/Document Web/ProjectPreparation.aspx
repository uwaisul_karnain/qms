﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProjectPreparation.aspx.cs" Inherits="SAPWebPages_ProjectPreparation" %>

<asp:Content ID="projectPreparationPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Preparation  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Project Preparation phase of a SAP implementation project lifecycle.  It covers the activities that need to be performed as well as the deliverables within the Project Preparation phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities are described in this process depends on:</p>
                            <p>•	The contractual requirements</p>
                            <p>•	The methodology imposed by the client</p>

                            <p>The activities to be fulfilled in this phase is mentioned in detail under the Process Overview of this document.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PSD - </strong>Professional Services Director</p>
                                <p><strong>SVP - </strong>Senior Vice President</p>
                                <p><strong>VPS - </strong>Vice President Services</p>
                                <p><strong>PGM - </strong>Project Group Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>                 
                                <p><strong>RM - </strong>Resource Manager</p>          
                          </div>
                          <div class="col-xs-6 col-md-4">                                
                                <p><strong>SA - </strong>Solution Architect</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>FC - </strong>Functional Consultant</p>
                          </div>
                          <div class="col-xs-6 col-md-4">                                
                                <p><strong>FL - </strong>Functional Lead</p>
                                <p><strong>PC - </strong>Project Charter</p>
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>pp - </strong>Project Plan</p>
                                <p><strong>SOW - </strong>Statement of Work</p> 
                          </div>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h2>Process and Steps</h2>    
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived using the SAP ASAP Focus methodology. The Project Preparation is the first phase within the aIR methodology. During the Project Preparation Phase, the project scope, the project plan, the project resources, and project approach are agreed upon. In addition, the required standards and procedures are agreed upon. These standards and procedures cover the topics such as scope management, change control, issue management, escalation and resolution, documentation, and risk management. The Project Status Tracking Sheet is prepared and put in place in order to track the Issues, Changes, Risks, Assumptions and Action items in this phase as well as in the subsequent phases that follow.</p>
                            <p>Therefore, the steps or procedures that are followed within the project preparation phase are described in the section below. The Project Charter that is generated in this phase forms the basis for the implementation approach of the project. </p>
                            <p>It must be highlighted that in case the customer or another team representing the customer is performing some parts of the tasks or deliverables in this phase, these tasks or deliverables are reviewed for completeness by the relevant stakeholders. Handling of customer completed tasks or deliverables for each phase is captured as part of Customer Deliverables in the Project Charter Document</p>


                            <h4>1.	Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="10">Project Preparation Execution</td>
                                            <td><p><strong>1.	Define project scope</strong></p>
                                                <p>The high-level project scope is defined. The project scope should cover the following:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The organization’s business units included within the SAP Implementation </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The business process areas that are to be covered within the implementation</p></td>
                                            <td>PGM / PM / SA / FL / TL / VPS / SVP</td>
                                            <td>Project Charter</td>                                    
                                            <td>Proposed Statement of Work / Contract / Request for Proposal / Customer communication</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>2.	Define Project Approach</strong></p>
                                                <p>The project approach is defined to cover the implementation methodology as well as the standards to be used. The approach focuses on:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The project objectives</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The project timeline</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The different phases of the implementation</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The key deliverables within each phase</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Identify the entry and exit criteria for the subsequent phases of the project lifecycle</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Identify the procedures for Scope, Change, Issue, Risk, Assumption and Dependency Management within each phase</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The strategies defined to manage  </p>
                                                <p class="tab-content" style="padding-left: 40px">i.	Business Transformation</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	Leadership Alignment</p>
                                                <p class="tab-content" style="padding-left: 40px">iii.	Systems Landscape details</p>
                                                <p class="tab-content" style="padding-left: 40px">iv.	Training</p>
                                                <p class="tab-content" style="padding-left: 40px">v.	Testing</p>
                                                <p class="tab-content" style="padding-left: 40px">vi.	Data Conversions (to migrate data from Legacy system to SAP) </p></td>
                                            <td>PGM / PM / SA / FL / TL</td>
                                            <td>Project Charter / Project Schedule (MPP) / Project Status Tracking Sheet</td>
                                            <td>Proposed Statement of Work / Contract </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>3.	Define the Project Governance</strong></p>
                                                <p>The Project Organization Structure is defined. The governance structure with the roles and responsibilities, the escalation and resolution process, the meeting frequency and agenda are all defined</p></td>
                                            <td>PGM / PM / Customer PM / Relevant Stakeholders from customer</td>
                                            <td>Project Charter</td>
                                            <td>Proposed Statement of Work / Contract</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>4.	Assign the Resources</strong></p>
                                                <p>Assign all required resources for the project</p></td>
                                            <td>PSD / PGM / PM / VPS / Customer PM</td>
                                            <td>Final Resource Plan </td>
                                            <td>Project Schedule / Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>5.	Onboard the project team with internal kick-off</strong></p>
                                                <p>Bring the team up to speed regarding:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The project location</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Project related logistics</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Project Timeline</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Project Objectives</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Governance structure</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Roles and responsibilities</p></td>
                                            <td>PSD / PGM / PM </td>
                                            <td>Administrative On-boarding Guide / On-boarding Check List</td>
                                            <td>Project Org Chart / Statement of Work / Contract / Request for Proposal</td>
                                        </tr>

                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>6.	Request setup of project in PSA</strong></p>
                                                <p>The project is created in PSA, and the resource allocations are made</p></td>
                                            <td>VPS / PSA Resources</td>
                                            <td>Not Available</td>
                                            <td>Proposed Statement of Work / Contract / Final Resource Plan</td>
                                        </tr>

                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>7.	Internal review of Project Charter</strong></p>
                                                <p>The completed project charter document is reviewed internally to ensure the completeness and review comments are incorporated</p></td>
                                            <td>PGM / PM / SA / / VPS / SVP / Practice Leads</td>
                                            <td>Project Charter</td>
                                            <td>Proposed Statement of Work / Contract</td>
                                        </tr>

                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>8.	Project Kick Off Presentation</strong></p>
                                                <p>The project is officially kicked off with the involvement of all relevant stakeholders</p></td>
                                            <td>PGM / PM / SA / FL / TL  / FC / TC / Customer project team / All relevant stakeholders</td>
                                            <td>Project Kick Off Presentation</td>
                                            <td>Project Charter / Statement of Work / Contract</td>
                                        </tr>

                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>9.	Prepare for Blueprint</strong></p>
                                                <p>The team completes the preparation to start the Blueprint workshops covering the following:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Identify the Business Process List</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Identify the key stakeholders/process owners to be involved in the workshops</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Prepare the Blueprint Workshops schedule </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Send out the invites to the relevant stakeholders / process owners for the workshops</p></td>
                                            <td>PGM / PM / SA / FL / TL / FC / TC / Customer project team</td>
                                            <td>Blueprint Workshop Schedule / Blueprint preparation check list</td>
                                            <td>Project Charter / Proposed SOW</td>
                                        </tr>

                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>10.	Set up Sandbox environment</strong></p></td>
                                            <td>SAP Security / PM</td>
                                            <td>Not Available</td>
                                            <td>Project Charter</td>
                                        </tr>



                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Proposed Statement of Work or Contract</p>
                                            <p>•	Customer charter document / presentation</p>
                                            <p>•	Project delivery model</p>

                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Review and sign off of Project Charter by Customer</p>
                                            <p>•	On-boarding Check List fulfilled (Optional) </p>
                                            <p>•	Completed Project Kick Off </p>
                                            <p>•	Completed Project Schedule</p>
                                            <p>•	Completed Blueprint Workshop Schedule</p>
                                            <p>•	Sandbox environment in place</p>
                                            <p>•	Blueprint Preparation Check List fulfilled</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Request for Proposal</p>
                                            <p>•	Proposed Statement of Work</p>
                                            <p>•	Proposed Contract</p>
                                            <p>•	Pre-sales customer meeting minutes (Optional)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project Charter</p>
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	Final Resource Plan</p>
                                            <p>•	Project Schedule</p>
                                            <p>•	Administrative On-boarding Guide</p>
                                            <p>•	On-boarding Check List</p>
                                            <p>•	Project Kick Off Presentation</p>
                                            <p>•	Business Blueprint Workshop Schedule</p>
                                            <p>•	Business Blueprint Preparation check list</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Schedule and Effort variance on the Project Preparation phase</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The project may decide to use client defined templates and standards for the deliverables from within the Project Preparation phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process</p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <h2>Document Templates</h2>                       
                        

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <h3>Document Templates</h3>                       
                                <%--<p><a href="References\attune Business Blueprint Process.docx">1. attune Business Blueprint Process</a></p>--%>
                                <p><a href="References\attune Project Charter.pptx">1. attune Project Charter Template</a></p>    
                                <p><a href="References\attune Project Status Tracking Sheet.xlsx">2. attune Project Status Tracking Sheet Template</a></p>  
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Guidelines</h3>                                                     
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Samples</h3>
                          </div>
                        </div>                        
                        
                                                                                
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>

