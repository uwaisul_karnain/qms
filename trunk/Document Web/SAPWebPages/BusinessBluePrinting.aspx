﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BusinessBluePrinting.aspx.cs" Inherits="BluePrinting" %>

<asp:Content ID="bluePrintingPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Business Blueprinting  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Blueprint phase of a SAP implementation project lifecycle.  It covers the activities that need to be performed as well as the deliverables within the Blueprinting phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The stage at which the project starts</p>
                            <p class="tab-content" style="padding-left: 20px">•	The expected deliverables</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>
                            <p class="tab-content" style="padding-left: 20px">•	The methodology imposed by the client</p>
                            <p>The business blueprinting activities defined here, are used to derive the project's process during the initial phase of the project (the defined project's process procedure in the Integrated Project Planning process)</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PGM - </strong>Project Group Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PL - </strong>Project Lead</p>
                          </div>
<%--                          <div class="col-xs-6 col-md-4">
                                <p><strong>AR - </strong>Architect</p>
                                <p><strong>AD - </strong>Architecture Document</p>
                                <p><strong>DD - </strong>Design Document</p>
                          </div>--%>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h2>Process and Steps</h2>    
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived using the SAP ASAP Focus methodology. The Business Blueprinting is the first phase within the aIR methodology. Within this phase, the activities begin with the Project Kick-Off and is followed by the Requirement Gathering stage, which begins with a study of the As-Is business processes.</p>
                            <p>The As-Is process study helps to understand the current business practices and to identify the specific pain points that may need to be addressed. During this stage, elicitation and documentation of the client’s requirements takes place. The Requirements are gathered for each business process flow within the As-Is process study. These initial requirements are then elaborated through discussions with the business teams responsible for each process. It is with these discussions that the possible solutions are identified.</p>
                            <p>Therefore, the steps or procedures that are followed within the business blueprinting exercise are described here. The final business blueprint document that is generated forms the basis for subsequent activities.</p>
                            <p>All the procedures incorporate the CM activities for version control and establishing and maintaining baselines. Requirements are managed using the RM process.</p>


                            <h4>1.	Steps</h4>
                                     
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="9">Procedure</td>
                                            <td><p>The PM prepares the Project Kick Off deck and presents it to the relevant stakeholders. The project teams from the customer and attune are introduced along with the scope of the project as well as the implementation approach</p></td>
                                            <td>PGM and PM</td>
                                            <td>Project Kick Off Presentation</td>                                    
                                            <td rowspan="2">SOW, RFP,  Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM and Module Leads studies the As-Is Process documents and the initial requirements as established in RFP. The PM and Leads also study any relevant client correspondence.</p></td>
                                            <td>PM, Module Leads</td>
                                            <td>As-Is Process Documents, Project Plan, Project Kick Off presentation</td>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM and the Module Leads plan the requirement gathering workshops, together with the customer’s team. The team identifies the relevant requirements providers from the customer organization to provide information needed to understand the current business practices and for defining requirements for the To-Be solution</p>
                                                <p>For this selection of requirements providers, the following criteria are used:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The requirements providers have the information needed to detail the current practices and the requirements, or have adequate access to persons who have the information</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The requirements providers identified have the authority to decide on the requirements</p>

                                                <p>The requirements providers selected should adequately represent the business stakeholders from the customer organization, so that the requirements collected are complete. Stakeholders represent the customer departments and business divisions responsible for the current business processes.</p>
                                            </td>
                                            <td>PM, Module Leads, Solution Architect</td>
                                            <td>Requirement Gathering Workshop schedule</td>  
                                            <td>Project Charter, Project Plan, Project Kick Off Presentation</td>                                  
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>User requirements are elicited and documented. </p>
                                                <p>For this, the sub-steps given below are performed sequentially or in parallel. They may have to be performed multiple times to compile the overall user requirements, which are to be documented.</p></td>
                                            <td>PM, Module Leads, Solution Architect, Business Process Owners, Business Users</td>                                    
                                            <td>Meeting Minutes, Email correspondences</td>
                                            <td>As-Is Business Process Documents, Requirement Gathering Workshop Schedule</td>
                                        </tr>
                                        <tr>
                                            <%--<td></td>--%>
                                            <td><p>The team are divided by Module, and the workshops are held with each business process team relevant for the Module, in order to review the As-is process and the pain points to be addressed.</p> 
                                            <p>The techniques used could include a combination of:</p>
                                            <p class="tab-content" style="padding-left: 20px">•	individual meetings </p>
                                            <p class="tab-content" style="padding-left: 20px">•	group meetings</p>
                                            <p class="tab-content" style="padding-left: 20px">•	interviews</p>
                                            <p class="tab-content" style="padding-left: 20px">•	questionnaires</p>
                                            <p class="tab-content" style="padding-left: 20px">•	surveys</p>
                                            <p class="tab-content" style="padding-left: 20px">•	study of organization procedures</p>
                                            <p class="tab-content" style="padding-left: 20px">•	study of existing systems </p>
                                            <p class="tab-content" style="padding-left: 20px">•	study of reports</p>
                                            <p class="tab-content" style="padding-left: 20px">•	demonstrations</p>
                                            <p class="tab-content" style="padding-left: 20px">•	prototyping</p>
                                            <p>The data collection and analysis is documented and retained.</p></td>
                                            <td>PM, Module Leads, Solution Architect, Module level consultants</td>
                                            <td>Meeting Minutes, Email correspondences</td>                                    
                                            <td>As-Is Business Process Documents</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The Module Leads and Solution Architect consolidate inputs obtained from the previous sub-step. In the process:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	missing information is identified and obtained</p>
                                                <p class="tab-content" style="padding-left: 20px">•	conflicting requirements are identified and clarifications are obtained</p>
                                                <p class="tab-content" style="padding-left: 20px">•	expectations and constraints with respect to solution are identified </p>
                                                <p class="tab-content" style="padding-left: 20px">•	expectations and constraints with respect to various testing activities are identified </p>
                                                <p>All the above is done with the involvement of all requirements providers identified.</p>
                                                <p>(Templates: Process flow Documents and Meeting Minutes)</p>
                                            </td>
                                            <td>Module Leads, Solution Architect, Module level consultants</td>
                                            <td rowspan="3">Meeting Minutes, Email correspondences</td>  
                                            <td rowspan="3">As-Is Business Process Documents</td>                                  
                                        </tr>
                                        <tr>
                                            <%--<td></td>--%>
                                            <td><p>The Module Leads and Solution Architect gather the data collected as per the sub-process above and prepare the To-Be solution and identify the possible gaps. </p>
                                                <p>(Template : GAP Analysis and To-Be process flow document)</p>
                                            </td>
                                            <td>Module Leads, Solution Architect, Module level consultants</td>
                                            <%--<td></td>--%>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr>
                                            <%--<td></td>--%>
                                            <td><p>Customized solutions required to fulfil the gaps are derived. The estimates are provided.</p>
                                                <p>(Template : RICEF Objects)</p>
                                            </td>
                                            <td>Module Leads, Module Level Consultants, Solution Architect, Technical Lead</td>
                                            <%--<td></td>--%>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr>
                                            <%--<td></td>--%>
                                            <td><p>The attune project team prepares and completes the Business Blueprint Document which illustrates and describes the To-Be solution which also includes the process flows and the customization included within each process to fill the gaps identified.</p>
                                                <p>(Template : Business Blueprint Document and Presentation)</p>
                                            </td>
                                            <td>PM, Module Leads, Module Level consultants, Solution Architect and Technical Lead</td>
                                            <td>Business Blueprint Document and Presentation</td>                                    
                                            <td>Gap Analysis, RICEF Object List, As-Is and To-Be Process Flow Documents</td>
                                        </tr>                                                                                     
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>Acceptance of the Proposed Solution as per the Business Blueprint document by means of a customer sign off. </p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>Successful completion of Project Kick Off, Requirement Gathering workshops, and sign-off of Business Blueprint</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Request for Proposal</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Statement of Work</p>
                                            <p class="tab-content" style="padding-left: 20px">•	As-Is Business Process documents</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Project Charter</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Project Kick Off Presentation</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Requirement Gathering Workshop Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	As-Is Business Process Documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	RICEF Object List</p>
                                            <p class="tab-content" style="padding-left: 20px">•	To-Be Process Flow Documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Document</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Number of Audit finding per project</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	The project may decide to use client defined templates and standards for the deliverables from within the Blueprinting phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Certain procedures of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented in the Project’s Process.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	In case the client or another team representing the client is performing some parts of the lifecycle and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables is defined in the Client Supplied Product Handling section of the Project Charter Document.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Client Sign-off of Business Blueprint Document will be decided at the project planning stage by the PGM/Business PM.</p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <h2>Document Templates</h2>                       
                        <p><a href="References\SAP Templates\attune Business Blueprint Template.docx">1. attune Business Blueprint</a></p>
                        <p><a href="References\SAP Templates\attune Integration Test Template.xls">2. attune Integration Test Template</a></p>                                   
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>
