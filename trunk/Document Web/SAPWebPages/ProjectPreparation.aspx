﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProjectPreparation.aspx.cs" Inherits="SAPWebPages_ProjectPreparation" %>

<asp:Content ID="projectPreparationPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Preparation  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Project Preparation phase of a SAP implementation project lifecycle.  It covers the activities that need to be performed as well as the deliverables within the Project Preparation phase. </p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p>•	The stage at which the project starts</p>
                            <p>•	The expected deliverables</p>
                            <p>•	The contractual requirements</p>
                            <p>•	The methodology imposed by the client</p>
                            <p>The project preparation activities defined here, are used to derive the project's process during this phase of the project (the defined project's process procedure in the Integrated Project Planning process)</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PGM - </strong>Project Group Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PL - </strong>Project Lead</p>
                          </div>
<%--                          <div class="col-xs-6 col-md-4">
                                <p><strong>AR - </strong>Architect</p>
                                <p><strong>AD - </strong>Architecture Document</p>
                                <p><strong>DD - </strong>Design Document</p>
                          </div>--%>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h2>Process and Steps</h2>    
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived using the SAP ASAP Focus methodology. The Project Preparation is the first phase within the aIR methodology. During the Project Preparation Phase, the scope, plans, resourcing, roles and responsibilities, and project structure are agreed upon and ratified. In addition, project methodologies, standards, and procedures will be agreed upon by covering items such as scope management, change control, issue management and escalation/resolution, documentation standards, and risk management.  The project objectives are validated and all initiation activities are documented in the Project Charter</p>
                            <p>In preparation for the Blueprinting Phase, the project team will assess the Customer’s organizational readiness for change throughout the organization, analyze the results, and work with both the project team and management to plan and prepare for successful engagement and buy-in by all users. Risks will be outlined, best communication methods will be established, and mitigation plans will be developed.</p>
                            <p>Therefore, the steps or procedures that are followed within the project preparation exercise are described here. The final Project Charter that is generated forms the basis for the implementation approach of the project. </p>


                            <h4>1.	Steps</h4>
                                     
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="5">Project Initiation</td>
                                            <td><p>Document project parameters</p

                                                <p>Document project governance processes</p

                                                <p>Identify Entrance and Exit Criteria for each phase</p

                                                <p>Develop Project Risk Assessment </p></td>
                                            <td>Program Manager / Project Manager</td>
                                            <td><p>Project Org Chart</p>
                                                <p>Project governance procedures document</p>
                                                <p>Risk Management Plan and Issue Log</p>
                                                <p>Scope/Change Request Management and log</p></td>                                    
                                            <td>Statement of Work / Contract / Request for Proposal</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Finalize High-Level Project Schedule</p>
                                                <p>Validate the high-level project schedule</p></td>
                                            <td><p>Program Manager / Project Manager / Professional Service Director / Resource Manager</p>
                                                <p>Project Manager / Solution Architect / Module Leads / Technical Leads</p></td>
                                            <td>High-level timeline, Resource Plan, Baseline Project Plan or Schedule</td>
                                            <td>Project Org Chart / Statement of Work / Contract / Request for Proposal</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Onboard the project team</p></td>
                                            <td>Program Manager / Project Manager</td>
                                            <td>Administrative Onboarding Guide</td>
                                            <td>Project Org Chart / Statement of Work / Contract / Request for Proposal</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare Project Charter</p></td>
                                            <td>Program Manager / Project Manager / Professional Service Director / Solution Architect / Module Leads / Technical Leads</td>
                                            <td>Project Charter</td>
                                            <td>Statement of Work / Contract / Request for Proposal / High-level timeline / Resource Plan / Baseline Project Plan or Schedule </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Request setup of project in PSA</p></td>
                                            <td>Program Manager / Project Manager / Professional Service Director / Resource Manager</td>
                                            <td>Not Available</td>
                                            <td>Statement of Work / Contract / Request for Proposal / Resource Plan</td>
                                        </tr>

                                        <tr>
                                            <td>Project Kick Off</td>
                                            <td><p>Prepare Project Kick Off Presentation</p></td>
                                            <td>Program Manager / Project Manager / Solution Architect / Module Leads / Technical Leads</td>
                                            <td>Project Kick Off Deck</td>                                    
                                            <td>Project Charter / Statement of Work / Contract / Request for Proposal</td>
                                        </tr>                                                                              
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>Acceptance of the Statement of Work or Request for Proposal or Contract</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>Successful completion of Project Charter, along with the review and approval of the document by the Customer. May include any other criteria as agreed upon with the customer.</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Request for Proposal</p>
                                            <p>•	Statement of Work</p>
                                            <p>•	Contract</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project Org Chart</p>
                                            <p>•	Project governance procedures document</p>
                                            <p>•	Risk Management Plan and Issue Log</p>
                                            <p>•	Scope/Change Request Management and log</p>
                                            <p>•	High-level timeline</p>
                                            <p>•	Resource Plan</p>
                                            <p>•	Baseline Project Plan or Schedule</p>
                                            <p>•	Administrative Onboarding Guide</p>
                                            <p>•	Project Charter</p>
                                            <p>•	Project Kick Off Deck</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	TBD – To be Defined</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The project may decide to use client defined templates and standards for the deliverables from within the Blueprinting phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>

                                            <p>•	Certain procedures of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented in the Project’s Process.</p>

                                            <p>•	In case the client or another team representing the client is performing some parts of the lifecycle and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables is defined in the Client Supplied Product Handling section of the Project Charter Document. </p>

                                            <p>•	Client Sign-off of Business Blueprint Document will be decided at the project planning stage by the PGM/Business PM.</p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <h2>Document Templates</h2>                       
<%--                        <p><a href="References\SAP Templates\attune Business Blueprint Template.docx">1. attune Business Blueprint</a></p>
                        <p><a href="References\SAP Templates\attune Integration Test Template.xls">2. attune Integration Test Template</a></p>    --%>                               
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>

