﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProcessTailoring.aspx.cs" Inherits="ProcessTailoring" masterPageFile="~/MasterPage.master" %>

<asp:Content ID="pTPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Process Tailoring Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>Process tailoring is a key activity necessary to configure the best suit process for a project. Tailored process describes the activities required to determine appropriate applicable processes and tools for the project. This include the required process and tools support the project team on smooth project environment activities.
                            All projects should conduct their process tailoring with the project kickoff and communicate the same to leadership team.
                            </p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The scope of the Process Tailoring process cover project and applicable activities in the project execution in attune.  Project should execute the project as per the tailored project process. If required, project team do the re- tailoring with help of Project Manager (PM) and Project Lead (PL).</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>PAD  - </strong>Project Approach Document</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>PM - </strong>Project Manager</p>
                          </div>
                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    
                            <h4>1.	Process Tailoring </h4>   
                            <p>With help of startup documents, understand Project context and Specific Process Needs.
                                Identify the deviation of standard attune process in the project with respect to client requests, project context, and execution mode.
                                With the standard process, identify the activities that will (may be not) be carried out for project specific reasons. 
                                If project execution method is different to the attune define process and activities, identify the differentiation.
                                If client mandate to use their own templates, guidelines, standards and process, identify the usage and differentiations
                                Follow the attune define process in all other instances
                            </p>

                            <h4>2.	Documentation </h4>   
                            <p>Document the above stated process with required detailed justifications for deviations.</p>

                            <h4>3.	Approval</h4>   
                            <p>•	Get the program manager’s approval tailored project process.
                               •	If project manager/ project coordinator received any review comments from the program manager. He should incorporate those in to process tailoring record and get the approval again.
                                Share the tailored project process with the project team.
                            </p>

                            <h4>4.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <td rowspan="4">Process Tailoring </td>
                                        <td>Identify project context and applicable process in the project </td>
                                        <td>PM, TL</td>
                                        <td rowspan="4"></td>                                    
                                        <td rowspan="4">SOW, PAD</td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Tailor project  process tailoring record  or do changes to accommodate changes in the  project</td>
                                        <td>PM, TL</td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Review process tailoring record by process quality consultant</td>
                                        <td>PQC</td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Incorporate review feedbacks</td>
                                        <td>PM, TL</td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr>
                                        <td rowspan="2">Leadership review</td>
                                        <td>Review and approve process tailoring record </td>
                                        <td>RD</td>
                                        <td></td>                                    
                                        <td>aPTR</td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Upload baseline</td>
                                        <td>PM, TL</td>
                                        <td>One attune backup</td>                                    
                                        <td></td>
                                        </tr>
                                        <tr>
                                        <td rowspan="3">Audit</td>
                                        <td>Auditor conduct the project audit based on project process tailoring record</td>
                                        <td>PL, PM, Auditor</td>
                                        <td rowspan="3"></td>                                    
                                        <td>Project Audit reports, PAD</td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Report process audit findings</td>
                                        <td>Auditor</td>
                                        <%--<td></td>--%>                                    
                                        <td></td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Team track those findings to closure and get the approval</td>
                                        <td>PM, TL</td>
                                        <%--<td></td>--%>                                    
                                        <td></td>
                                        </tr>      
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Project SOW signed-off </p>
                                            <p>•	Project kicked off for execution</p>
                                            <p>•	Required resources for Project Manager (PM) and Project Lead (PL) are identified with respective roles and responsibilities</p>
                                            <p>•	PM and TL is trained to do the process tailoring</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Project SOW</p>
                                            <p>•	Project Approach Document</p>
                                            <p>•	Project Charter Document</p>  
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Project SOW</p>
                                            <p>•	Project Approach Document</p>
                                            <p>•	Configuration Management process and templates</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project Process Tailoring Record </p>
                                            <p>•	Project delivery process  audit report</p>
                                            <p>•	Process tailoring record review entry</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Number of Audit finding per project</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	Process Tailoring record is a mandatory for any project running in the delivery center.</p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\attune Process Tailoring Repository.xlsx">1. attune Process Tailoring Repository</a></p>                                                                                                
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>          
                                    <p><a href="References\attune Agile Project Process.pptx">1. attune Agile Project Process</a></p>
                                    <p><a href="References\attune Life Cycle Definitions.docx">2. attune Life Cycle Definitions</a></p>  
                                    <p><a href="References\SAP Project Life Cycle.docx ">3. SAP Project Life Cycle</a></p>                          
                                                          
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                          
                         

                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>