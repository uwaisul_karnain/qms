﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Examination_Portal.Startup))]
namespace Examination_Portal
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
