﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Questions.aspx.cs" Inherits="Examination_Portal.FullScree" %>--%>
<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Questions.aspx.cs" Inherits="Examination_Portal.FullScree" %>


<asp:Content ID="QuestionContent" ContentPlaceHolderID="MainContentPageSection" runat="server">

    <style>
        .innerSectiontop {
            padding: 0px;
        }
        .Qlist_area {
            height: 80vh;
            margin-top:80px;
            padding-bottom:100px;
        }
        .Qheader {
            background: #eaeaea none repeat scroll 0 0;
            height: 80px;
            position: absolute;
            z-index: 100;
            
        }
        body {
            overflow: hidden;
        }
    </style>

    <form id="form1" runat="server" >

        

<%--        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT Top 10 [ExamId], [QuestionId], [Question], [Answer1], [Answer2], [Answer3], [Answer4], [CorrectAnswer], [Marks] FROM [ExamQuestions] ORDER BY NEWID()"></asp:SqlDataSource>--%>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommandType="StoredProcedure" SelectCommand="SP_ExamQuestions">
        <SelectParameters>
            <asp:Parameter Name="examId" Type="Int32" DefaultValue="1"/>
        </SelectParameters>
        </asp:SqlDataSource>

        <div class="Qheader">

            <span class="txt"><%= examNAME%></span>
           
            <div class="col-xs-6 col-md-4" style="float:right;">
                <div class="areaQuestion" style="padding-top:10px; padding-bottom:10px">
                    <asp:ScriptManager ID="ScriptManager" runat="server">
                    </asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel" runat="server" Width="75%">
                            </asp:Panel>
                            <asp:Label ID="LabelText" runat="server" Text="Time Remainng :  " Font-Size="Large" ForeColor="#919191" Font-Names="Verdana" style="padding-bottom:10px"></asp:Label>
                            <asp:Label ID="TimerLabel" runat="server" Font-Size ="X-Large" ForeColor ="#919191" Font-Names="Verdana" BorderColor="#B1B1B1" BorderStyle="Solid" BorderWidth="1px"></asp:Label><br />
                            
                            <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                            </asp:Timer>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <div class="Qlist_area">
            <asp:ListView runat="server" ID="ListViewQuestions" DataSourceID="SqlDataSource1" >
            <ItemTemplate>
            <tr runat="server">

              <td runat="server">
                    
                    <%--<strong>ExamId:</strong>--%><asp:Label ID="examId" runat="server" Text='<%# Eval("ExamId") %>' Visible="false"></asp:Label>
                    
                    <div class="areaQuestion" style="padding: 60px 20px 0px 80px;" >
                                       
                        <asp:Label ID="questionNumber" GroupName='<%# Eval("QuestionId") %>' Text = '<%# (((ListViewDataItem)Container).DisplayIndex + 1) + " .  " %>' runat="server" Font-Bold ="true"></asp:Label>
                        <asp:Label ID="question" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Question") %>' Font-Bold ="false"></asp:Label><br />
                        <%--<asp:Image ID="A1Img" GroupName='<%# Eval("QuestionId") %>'  runat ="server" ImageUrl='<%#Eval("~/Images/airDiagram.jpg") %>' />--%>

                        <div class="areaQuestionIn" style="padding: 40px 0px 20px 20px;" >
                            <div class="col-xs-6 col-md-6">
                                  <asp:RadioButton ID="Answer1" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer1") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton><br />
                                  <asp:RadioButton ID="Answer2" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer2") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton><br />                                 
                                  <%--<asp:Image ID="A1Img" runat="server" ImageUrl="Images/Questions/airDiagram.jpg.png" />--%>
                                            <%--<ItemTemplate>
                                                <%--<img src='<%#Eval("Images/Questions/airDiagram.jpg.png") %>' alt="" />--%>

                                            <%--</ItemTemplate>--%>
                            </div>
                            <div class="col-xs-6 col-md-6">
                                    <asp:RadioButton ID="Answer3" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer3") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton><br />
                                    <asp:RadioButton ID="Answer4" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer4") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton>
                            </div>
                            <%--<strong>Question:</strong>--%><asp:Label ID="QuestionID" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("QuestionId") %>' Visible ="false"></asp:Label>
                            <%--<strong>Correct Answer:</strong>--%><asp:Label ID="CorrectAnswer" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("CorrectAnswer") %>' Visible ="false" ForeColor="SandyBrown"></asp:Label><br />                                                   
                            <%--<strong>Group Id:</strong>--%><asp:Label ID="GroupId" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("GroupId") %>' Visible ="false" ForeColor="SandyBrown"></asp:Label><br />                                                   

                        </div>
                    </div>
              </td>
   
            </tr>
          </ItemTemplate>
        </asp:ListView>

            <div class="tab-content" style="padding-top: 100px; padding-left: 200px; padding-bottom: 100px">
                <asp:Button ID="ButtonSubmit" runat="server"  Text="Submit" OnClick="ButtonSubmit_Click" />
            </div>

        </div>

        
        

    </form>

    <script type="text/javascript" >

        jQuery(document).ready(function () {
            $('.leftPanel')
            .append('<div class="lock"></div>');
        })
        

</script>

</asp:Content>
