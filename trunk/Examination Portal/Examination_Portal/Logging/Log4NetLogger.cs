﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Portal.Infrastructure.Core.Logging;
using log4net;
using log4net.Appender;
using log4net.Config;

namespace Portal.Infrastructure.CrossCutting.Logging
{
    public class Constants
    {
        public const string DEFAULT_LOGGER = "General";
    }

    public class Log4NetLogger : ILogger
    {
        private readonly ILog _logger;

        public Log4NetLogger()
        {
            XmlConfigurator.Configure();
            _logger = LogManager.GetLogger(Constants.DEFAULT_LOGGER);
        }

        //public Log4NetLogger(string name = Constants.DEFAULT_LOGGER)
        //{
        //    _logger = LogManager.GetLogger(name ?? Constants.DEFAULT_LOGGER);
        //}

        //public Log4NetLogger(Type type)
        //{
        //    _logger = LogManager.GetLogger(type);
        //}

        #region ILogger Members

        /// <summary>
        /// Clears the log.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public bool ClearLog(string name)
        {
            try
            {
                return ClearRollingAppender(name);
            }
            catch (Exception exception)
            {
                _logger.Fatal("Clear Failed.", exception);
                return false;
            }
        }

        /// <summary>
        /// Debugs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Debug(object message)
        {
            _logger.Debug(message);
        }

        /// <summary>
        /// Debugs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public void Debug(string message, Exception exception)
        {
            _logger.Debug(message, exception);
        }

        /// <summary>
        /// Debugs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public void Debug(string message, params object[] args)
        {
            if (String.IsNullOrWhiteSpace(message)) return;
            _logger.Debug(string.Format(CultureInfo.InvariantCulture, message, args));
        }

        /// <summary>
        /// Infoes the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Info(object message)
        {
            _logger.Info(message);
        }

        /// <summary>
        /// Infoes the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public void Info(string message, Exception exception)
        {
            _logger.Info(message, exception);
        }

        /// <summary>
        /// Infoes the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public void Info(string message, params object[] args)
        {
            if (String.IsNullOrWhiteSpace(message)) return;
            _logger.Info(string.Format(CultureInfo.InvariantCulture, message, args));
        }

        /// <summary>
        /// Warnings the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Warning(object message)
        {
            _logger.Warn(message);
        }

        /// <summary>
        /// Warnings the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public void Warning(string message, Exception exception)
        {
            _logger.Warn(message, exception);
        }

        /// <summary>
        /// Warnings the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public void Warning(string message, params object[] args)
        {
            if (String.IsNullOrWhiteSpace(message)) return;
            _logger.Warn(string.Format(CultureInfo.InvariantCulture, message, args));
        }

        /// <summary>
        /// Errors the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Error(object message)
        {
            _logger.Error(message);
        }

        /// <summary>
        /// Errors the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public void Error(string message, Exception exception)
        {
            _logger.Error(message, exception);
        }

        /// <summary>
        /// Errors the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public void Error(string message, params object[] args)
        {
            if (String.IsNullOrWhiteSpace(message)) return;
            _logger.Error(string.Format(CultureInfo.InvariantCulture, message, args));
        }

        /// <summary>
        /// Fatals the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        public void Fatal(object message)
        {
            _logger.Fatal(message);
        }

        /// <summary>
        /// Fatals the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="exception">The exception.</param>
        public void Fatal(string message, Exception exception)
        {
            _logger.Fatal(message, exception);
        }

        /// <summary>
        /// Fatals the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="args">The args.</param>
        public void Fatal(string message, params object[] args)
        {
            if (String.IsNullOrWhiteSpace(message)) return;
            _logger.Fatal(string.Format(CultureInfo.InvariantCulture, message, args));
        }

        #endregion

        private bool ClearRollingAppender(string name)
        {
            RollingFileAppender app = null;
            foreach (IAppender ap in _logger.Logger.Repository.GetAppenders().Where(ap => ap.Name == name))
                app = (RollingFileAppender) ap;

            if (app == null)
                throw new Exception("RollingFileAppender not found!");

            string currentPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string logFilePath = string.Format("{0}/{1}", currentPath, app.File);

            if (File.Exists(logFilePath))
            {
                LogManager.Shutdown();
                File.Delete(logFilePath);
                XmlConfigurator.Configure();
                return true;
            }
            return false;
        }
    }
}