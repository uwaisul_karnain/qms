﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Examination_Portal
{
    public partial class Exams : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ExamsDetailGrid.SelectedIndex = 0;
                ExamsDetailGrid_SelectedIndexChanged(null, null);

                //FillExamGroupsGrid();
                //ExamGroupsGrid.DataSource = SqlDataSource_ExamGroups;
                //ExamGroupsGrid.DataBind();
                //SqlDataSource_ExamGroups.SelectCommand = "SELECT [ExamGroupQuestionId], [ExamId], EG.[GroupId], [GroupName], [QuestionCount] FROM [dbo].[ExamGroupQuestionCount] EG LEFT OUTER JOIN [dbo].[QuestionGroups] G ON EG.GroupId = G.GroupId WHERE ExamId = " + Convert.ToInt32(txtExamId.Text.ToString()) + " ORDER BY GroupName";

                //SqlDataSource_ExamGroups.DataBind();
                //ExamGroupsGrid.DataSource = SqlDataSource_ExamGroups;
                //ExamGroupsGrid.DataBind();
            }
        }

        protected void ButtonAddGroup_Click(object sender, EventArgs e)
        {
            if (txtQuestionArea.Text.Trim() == "")
            {
                lblAreaStar.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Please enter Question Area');", true);
                return;
            }
            else
                lblAreaStar.Visible = false;

            if (txtAreaQuestions.Text.Trim() == "")
            {
                lblAreaQuestionsStar.Visible = true;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Please enter Number Questions to be selected from the Question Area');", true);
                return;
            }
            else
                lblAreaQuestionsStar.Visible = false;

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("RowNumber", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("GroupId", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("ExamId", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("GroupName", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("QuestionCount", Type.GetType("System.Int32")));

            //int rowIndex = 0;
            //int rowNumber = 0;
            //if (ViewState["CurrentTable"] != null)
            //{
            DataTable dtCurrentTable = (DataTable)Session["ExamAreaGroupsGrid"];

            //DataRow drCurrentRow = null;
            //if (ExamAreaGroupsGrid.Rows.Count > 0)
            //{
            //    for (int i = 1; i <= ExamAreaGroupsGrid.Rows.Count; i++)
            //    {
            //        drCurrentRow = dt.NewRow();
            //        dt.Rows.Add();
            //        drCurrentRow["RowNumber"] = i + 1;

            //        //dt.Rows[i - 1]["GroupId"] = Convert.ToInt32(ExamAreaGroupsGrid.Rows[rowIndex].Cells[3].Text);
            //        dt.Rows[i - 1]["GroupName"] = ExamAreaGroupsGrid.Rows[rowIndex].Cells[0].Text;
            //        dt.Rows[i - 1]["QuestionCount"] = ExamAreaGroupsGrid.Rows[rowIndex].Cells[1].Text;
            //        dt.Rows[i - 1]["GroupId"] = Convert.ToInt32(ExamAreaGroupsGrid.Rows[rowIndex].Cells[2].Text);
            //        //drCurrentRow = dt.NewRow();
            //        //drCurrentRow["RowNumber"] = i + 1;
            //        rowNumber = i + 1; 
            //        rowIndex++;
            //    }

            var drCurrentRow = dtCurrentTable.NewRow();
            drCurrentRow["GroupId"] = 0;
            if (txtExamId.Text == "")
                drCurrentRow["ExamId"] = 0;
            else
                drCurrentRow["ExamId"] = Convert.ToInt32(txtExamId.Text);
            drCurrentRow["GroupName"] = txtQuestionArea.Text;
            drCurrentRow["QuestionCount"] = txtAreaQuestions.Text;
            dtCurrentTable.Rows.Add(drCurrentRow);
            //dt.Rows.Add();
            //drCurrentRow["RowNumber"] = rowNumber + 1;

            //dt.Rows[rowNumber - 1]["GroupId"] = 0;
            //dt.Rows[rowNumber - 1]["GroupName"] = txtQuestionArea.Text;
            //dt.Rows[rowNumber - 1]["QuestionCount"] = txtAreaQuestions.Text; ;

            //drCurrentRow = dt.NewRow();
            //drCurrentRow["RowNumber"] = rowNumber + 1;

            //dt.Rows.Add(drCurrentRow);
            
            Session["ExamAreaGroupsGrid"] = dtCurrentTable;

            ExamAreaGroupsGrid.DataSource = dtCurrentTable;
            ExamAreaGroupsGrid.DataBind();

            txtQuestionArea.Text = "";
            txtAreaQuestions.Text = "";


            // }
            //}
            //else
            //{
            //    Response.Write("ViewState is null");
            //}
            //SetPreviousData();

        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (ButtonAdd.Text == "New")
            {
                //Reset screen
                ButtonAdd.Text = "Submit";
                ButtonEdit.Visible = false;
                ButtonDelete.Visible = false;
                ButtonCancel.Visible = true;
                ClearControls();
                EnableDisableControls(true);
            }
            else
            {
                if (DataValidated() == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Incomplete data. Please enter compulsary data marked as *');", true);
                    return;
                }
                //Insert Record
                AddExam();
                ButtonAdd.Text = "New";
                ButtonEdit.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = false;
                EnableDisableControls(false);
                ClearControls();
                EnableDisableControls(false);
            }
        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (ButtonEdit.Text == "Edit")
            {
                if (txtExamId.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Please select a record to Edit');", true);
                    return;
                }
                ButtonEdit.Text = "Update";
                ButtonAdd.Visible = false;
                ButtonDelete.Visible = false;
                ButtonCancel.Visible = true;
                EnableDisableControls(true);
            }
            else
            {
                if (DataValidated() == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Incomplete data. Please enter compulsary data marked as *');", true);
                    return;
                }
                UpdateExam();
                ButtonEdit.Text = "Edit";
                ButtonAdd.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = false;
                ClearControls();
                EnableDisableControls(false);
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (txtExamId.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Please select a record to Delete.');", true);
                return;
            }
            else
            {
                DeleteExam();
                ClearControls();
                EnableDisableControls(false);
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            ButtonAdd.Text = "New";
            ButtonAdd.Visible = true;
            ButtonEdit.Text = "Edit";
            ButtonEdit.Visible = true;
            ButtonDelete.Visible = true;
            ButtonCancel.Visible = false;
            EnableDisableControls(false);
            ClearControls();
            EnableDisableControls(false);

        }

        protected void ExamsDetailGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            ExamsDetailGrid.CurrentPageIndex = e.NewPageIndex;
            ExamsDetailGrid.DataBind();
        }

        protected void ExamsDetailGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExamsDetailGrid.SelectedIndex = ExamsDetailGrid.SelectedItem.ItemIndex;
            if (ExamsDetailGrid.SelectedItem.Cells[1].Text != null)
                txtExamId.Text = ExamsDetailGrid.SelectedItem.Cells[1].Text;
            else
                txtExamId.Text = "";
            if (ExamsDetailGrid.SelectedItem.Cells[2].Text != null)
                txtExamCode.Text = ExamsDetailGrid.SelectedItem.Cells[2].Text;
            else
                txtExamCode.Text = "";
            if (ExamsDetailGrid.SelectedItem.Cells[3].Text != null)
                txtExamName.Text = ExamsDetailGrid.SelectedItem.Cells[3].Text;
            else
                txtExamName.Text = "";
            if (ExamsDetailGrid.SelectedItem.Cells[4].Text != null)
                txtDescription.Text = ExamsDetailGrid.SelectedItem.Cells[4].Text;
            else
                txtDescription.Text = "";
            if (ExamsDetailGrid.SelectedItem.Cells[5].Text != null)
                txtTotalQuestions.Text = ExamsDetailGrid.SelectedItem.Cells[5].Text;
            else
                txtTotalQuestions.Text = "";
            if (ExamsDetailGrid.SelectedItem.Cells[6].Text != null)
                txtPassMark.Text = ExamsDetailGrid.SelectedItem.Cells[6].Text;
            else
                txtPassMark.Text = "";
            if (ExamsDetailGrid.SelectedItem.Cells[7].Text != null && ExamsDetailGrid.SelectedItem.Cells[8].Text != null && ExamsDetailGrid.SelectedItem.Cells[9].Text != null)
            {
                TimeSpan examTimeSpan = new TimeSpan(Convert.ToInt32(ExamsDetailGrid.SelectedItem.Cells[8].Text.ToString()), Convert.ToInt32(ExamsDetailGrid.SelectedItem.Cells[9].Text.ToString()), 0);
                ddlTimeDuration.SelectedIndex = ExamDurationIndex(examTimeSpan);
            }
            else
                ddlTimeDuration.SelectedIndex = 0;


            //SqlDataSource_ExamGroups.SelectCommand = "SELECT [ExamGroupQuestionId], [ExamId], EG.[GroupId], [GroupName], [QuestionCount] FROM [dbo].[ExamGroupQuestionCount] EG LEFT OUTER JOIN [dbo].[QuestionGroups] G ON EG.GroupId = G.GroupId WHERE ExamId = " + Convert.ToInt32(txtExamId.Text.ToString()) + " ORDER BY GroupName";

            //SqlDataSource_ExamGroups.DataBind();
            //ExamGroupsGrid.DataSource = SqlDataSource_ExamGroups;
            //ExamGroupsGrid.DataBind();

            FillExamGroupsGrid();

        }

        protected void EnableDisableControls(bool enable)
        {
            if (enable == true)
            {
                txtExamCode.Enabled = true;
                txtExamName.Enabled = true;
                txtDescription.Enabled = true;
                txtTotalQuestions.Enabled = true;
                txtPassMark.Enabled = true;
                ddlTimeDuration.Enabled = true;
                txtQuestionArea.Enabled = true;
                txtAreaQuestions.Enabled = true;
                //ExamGroupsGrid.Enabled = true;
                //ExamGroupsGrid.Columns[3].Visible = true;
                ExamAreaGroupsGrid.Enabled = true;
                ExamAreaGroupsGrid.Columns[3].Visible = true;

                ExamsDetailGrid.Enabled = false;
            }
            else
            {
                txtExamCode.Enabled = false;
                txtExamName.Enabled = false;
                txtDescription.Enabled = false;
                txtTotalQuestions.Enabled = false;
                txtPassMark.Enabled = false;
                ddlTimeDuration.Enabled = false;
                txtQuestionArea.Enabled = false;
                txtAreaQuestions.Enabled = false;
                //ExamGroupsGrid.Enabled = false;
                //ExamGroupsGrid.Columns[3].Visible = false;
                ExamAreaGroupsGrid.Enabled = false;
                ExamAreaGroupsGrid.Columns[3].Visible = false;

                ExamsDetailGrid.Enabled = true;
            }

        }

        protected void ClearControls()
        {
            txtExamId.Text = "";
            txtExamCode.Text = "";
            txtExamName.Text = "";
            txtDescription.Text = "";
            txtTotalQuestions.Text = "";
            txtPassMark.Text = "";
            ddlTimeDuration.SelectedIndex = 0;
            txtAreaQuestions.Text = "";
            txtQuestionArea.Text = "";
            SqlDataSourceGroups.SelectCommand = "";
            ExamAreaGroupsGrid.DataSource = SqlDataSourceGroups;
            ExamAreaGroupsGrid.DataBind();

        }

        protected void FillExamGroupsGrid()
        {
            string sqlCommand = "";
            if (txtExamId.Text.Trim() != "")
                sqlCommand = "SELECT GroupId, EG.[ExamId] AS ExamId, [GroupName], [QuestionCount] FROM [dbo].[ExamQuestionGroups] EG LEFT OUTER JOIN [dbo].[Exams] E ON EG.ExamId = E.ExamId WHERE EG.ExamId = " + Convert.ToInt32(txtExamId.Text.ToString()) + " ORDER BY GroupName ";

            //SqlDataSource_ExamGroups.SelectCommand = sqlCommand;

            //SqlDataSource_ExamGroups.DataBind();
            //ExamGroupsGrid.DataSource = SqlDataSource_ExamGroups;
            //ExamGroupsGrid.DataBind();

            SqlDataSourceGroups.SelectCommand = sqlCommand;

            SqlDataSourceGroups.DataBind();
            DataSourceSelectArguments args = new DataSourceSelectArguments();
            DataView view = (DataView)SqlDataSourceGroups.Select(args);
            DataTable dt = view.ToTable();
            Session["ExamAreaGroupsGrid"] = dt;
            ExamAreaGroupsGrid.DataSource = dt;

            ExamAreaGroupsGrid.DataBind();
        }

        protected bool DataValidated()
        {
            bool dataValidated = true;
            if (txtExamCode.Text.Trim() == "")
            {
                dataValidated = false;
                lblExamCodeStar.Visible = true;
            }
            else
                lblExamCodeStar.Visible = false;

            if (txtExamName.Text.Trim() == "")
            {
                dataValidated = false;
                lblExamNameStar.Visible = true;
            }
            else
                lblExamNameStar.Visible = false;

            if (txtTotalQuestions.Text.Trim() == "")
            {
                dataValidated = false;
                lblQuestionsStar.Visible = true;
            }
            else
                lblQuestionsStar.Visible = false;

            if (txtPassMark.Text.Trim() == "")
            {
                dataValidated = false;
                lblPassMarkStar.Visible = true;
            }
            else
                lblPassMarkStar.Visible = false;



            //if(ExamGroupsGrid.Items.Count == 0)
            //{
            //    dataValidated = false;
            //    //lblAreaStar.Visible = true;
            //}
            //else
            //    //lblAreaStar.Visible = false;

            return dataValidated;
        }

        protected void AddExam()
        {
            int ExamId = 0;

            //string durationTime = txtDurationHours.Text.ToString() + "s:" + txtDurationMinutes.TemplateControl.ToString();
            string insertSQL = "INSERT INTO [dbo].[Exams] ([Code], [Name], [Description], [NumberOfQuestions], [PassMarkPercentage], [Duration]) VALUES (LTRIM(RTRIM('" + txtExamCode.Text + "')), LTRIM(RTRIM('" + txtExamName.Text + "')), LTRIM(RTRIM('" + txtDescription.Text + "')), " + Convert.ToInt32((txtTotalQuestions.Text).ToString()) + ", " + Convert.ToInt32((txtPassMark.Text).ToString()) + ",'" + ExamDuration() + "' )";

            SqlDataSource_Exams.InsertCommand = insertSQL;
            SqlDataSource_Exams.Insert();
            SqlDataSource_Exams.DataBind();


            string selectSQL = "SELECT DISTINCT TOP 1 ExamId FROM [dbo].[Exams] WHERE Code = '" + txtExamCode.Text + "'";

            SqlCommand cmd = new SqlCommand(selectSQL);
            SqlDataReader dr = cmd.ExecuteReader();

             
            if (dr.Read())
            {
                ExamId = Convert.ToInt32(dr[0].ToString());
            }

            if (ExamId > 0)
            {
                DataTable dtCurrentTable = (DataTable)Session["ExamAreaGroupsGrid"];

                if (dtCurrentTable.Rows.Count > 0)
                {
                    string deleteCommand = "DELETE [dbo].[ExamQuestionGroups] WHERE ExamId = " + Convert.ToInt32(ExamId) + "";
                    SqlDataSourceGroups.DeleteCommand = deleteCommand;
                    SqlDataSourceGroups.Delete();

                    string insertCommand;
                    for (int i = 0; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        insertCommand = "INSERT INTO [dbo].[ExamQuestionGroups] ([ExamId], [GroupName], [QuestionCount])  VALUES (" + Convert.ToInt32(ExamId) + ",'" + dtCurrentTable.Rows[i]["GroupName"].ToString().Trim() + "'," + Convert.ToInt32(dtCurrentTable.Rows[i]["QuestionCount"]) + ")";
                        SqlDataSourceGroups.InsertCommand = insertCommand;
                        SqlDataSourceGroups.Insert();
                    }

                }
            }

            ExamsDetailGrid.DataBind();

            //insertSQL = "INSERT INTO [dbo].[Exams] ([Code], [Name], [Description], [NumberOfQuestions], [PassMarkPercentage], [Duration]) VALUES (LTRIM(RTRIM('" + txtExamCode.Text + "')), LTRIM(RTRIM('" + txtExamName.Text + "')), LTRIM(RTRIM('" + txtDescription.Text + "')), " + Convert.ToInt32((txtTotalQuestions.Text).ToString()) + ", " + Convert.ToInt32((txtPassMark.Text).ToString()) + ",'" + ExamDuration() + "' )";

            //SqlDataSource_ExamGroups.InsertCommand = insertSQL;
            //SqlDataSource_ExamGroups.Insert();

            //SqlDataSource_ExamGroups.DataBind();
            //AreasGrid.DataBind();
        }

        protected void UpdateExam()
        {
            if (txtExamId.Text != "")
            {
                //Insert/Delete ExamQuestionGroups Table
                DataTable dtCurrentTable = (DataTable)Session["ExamAreaGroupsGrid"];

                if (dtCurrentTable.Rows.Count > 0)
                { 

                  int[] groupIDs = new int[dtCurrentTable.Rows.Count - 1];

                  string insertCommand;
                  string groupList = "";  // List of GroupIds from the AreGrouoGrid

                  for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                  {
                      if (Convert.ToInt32(dtCurrentTable.Rows[i]["GroupId"]) != 0 )
                      {
                          if (groupList == "")
                                groupList = dtCurrentTable.Rows[i]["GroupId"].ToString();
                          else
                                groupList = groupList + "," + dtCurrentTable.Rows[i]["GroupId"].ToString();                      
                      }

                  }

                  // Delete removed records from ExamQuestionGroups Table
                  string deleteCommand = "DELETE [dbo].[ExamQuestionGroups] WHERE ExamId = " + Convert.ToInt32(txtExamId.Text) + " AND GroupId NOT IN (" + groupList + ")";
                  SqlDataSourceGroups.DeleteCommand = deleteCommand;
                  SqlDataSourceGroups.Delete();

                  //Inser New records to ExamQuestionGroups Table
                  for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                  {
                      if((Convert.ToInt32(dtCurrentTable.Rows[i]["GroupId"])) == 0)
                      {
                          insertCommand = "INSERT INTO [dbo].[ExamQuestionGroups] ([ExamId], [GroupName], [QuestionCount])  VALUES (" + Convert.ToInt32(txtExamId.Text) + ",'" + dtCurrentTable.Rows[i]["GroupName"].ToString().Trim() + "'," + Convert.ToInt32(dtCurrentTable.Rows[i]["QuestionCount"]) + ")";
                          SqlDataSourceGroups.InsertCommand = insertCommand;
                          SqlDataSourceGroups.Insert();
                      }
                  }
                }

                //Update Exams table
                string updateSQL = "UPDATE [dbo].[Exams]  SET [Code] = LTRIM(RTRIM('" + txtExamCode.Text + "')), [Name] = LTRIM(RTRIM('" + txtExamName.Text + "')), [Description] = LTRIM(RTRIM('" + txtDescription.Text + "')), [NumberOfQuestions] = " + Convert.ToInt32((txtTotalQuestions.Text).ToString()) + ", [PassMarkPercentage] = " + Convert.ToInt32((txtPassMark.Text).ToString()) + ", [Duration] = '" + ExamDuration() + "' WHERE [Exams].[ExamId] = " + Convert.ToInt32((txtExamId.Text).ToString()) + "";

                SqlDataSource_Exams.UpdateCommand = updateSQL;
                SqlDataSource_Exams.Update();
                ExamsDetailGrid.DataBind();

            }
        }

        protected void DeleteExam()
        {
            if (txtExamId.Text != "")
            {
                string deleteSQL = "DELETE [dbo].[Exams] WHERE ExamId = " + Convert.ToInt32((txtExamId.Text).ToString()) + "";

                SqlDataSource_Exams.DeleteCommand = deleteSQL;
                SqlDataSource_Exams.Delete();
                SqlDataSource_Exams.DataBind();
                ExamsDetailGrid.DataBind();
            }
        }

        protected TimeSpan ExamDuration()
        {
            int examHours = 0;
            int examMinutes = 0;
            int examSeconds = 0;

            if (ddlTimeDuration.Items.Count > 0)
            {
                switch (ddlTimeDuration.SelectedIndex)
                {
                    case 0:
                        examHours = 0;
                        examMinutes = 15;
                        break;
                    case 1:
                        examHours = 0;
                        examMinutes = 30;
                        break;
                    case 2:
                        examHours = 0;
                        examMinutes = 45;
                        break;
                    case 3:
                        examHours = 1;
                        examMinutes = 0;
                        break;
                    case 4:
                        examHours = 1;
                        examMinutes = 15;
                        break;
                    case 5:
                        examHours = 1;
                        examMinutes = 30;
                        break;
                    case 6:
                        examHours = 1;
                        examMinutes = 45;
                        break;
                    case 7:
                        examHours = 2;
                        examMinutes = 0;
                        break;
                    default:
                        examHours = 0;
                        examMinutes = 15;
                        break;

                }
            }
            TimeSpan examTimeSpan = new TimeSpan(examHours, examMinutes, examSeconds);
            return examTimeSpan;
        }

        protected int ExamDurationIndex(TimeSpan examTime)
        {
            int retunIndex = 0;
            switch (examTime.ToString())
            {
                case "00:15:00":
                    retunIndex = 0;
                    break;
                case "00:30:00":
                    retunIndex = 1;
                    break;
                case "00:45:00":
                    retunIndex = 2;
                    break;
                case "01:00:00":
                    retunIndex = 3;
                    break;
                case "01:15:00":
                    retunIndex = 4;
                    break;
                case "01:30:00":
                    retunIndex = 5;
                    break;
                case "01:45:00":
                    retunIndex = 6;
                    break;
                case "02:00:00":
                    retunIndex = 7;
                    break;
                default:
                    retunIndex = 8;
                    break;
            }
            return retunIndex;
        }

        protected void Questions_TextChanged(object sender, EventArgs e)
        {
            //if(EventArgs.keyCode<48 || event.keyCode>57)
            //    event.returnValue=false;
        }

        protected void PassMark_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ExamGroupList_PreRender(object sender, EventArgs e)
        {
            //SqlDataSource_ExamGroups.SelectCommand = "SELECT [GroupId], [GroupCode], [GroupName] FROM [dbo].[QuestionGroups] ORDER BY GroupName";
            //SqlDataSource_ExamGroups.DataBind();
            //AreaList.DataBind();
        }

        //protected void ExamGroupsGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        //{
        //    //SqlDataSource_ExamGroups.SelectCommand = "SELECT [GroupId], EG.[ExamId], [GroupName], [QuestionCount] FROM [dbo].[ExamQuestionGroups] EG LEFT OUTER JOIN [dbo].[Exams] E ON EG.ExamId = E.ExamId WHERE EG.ExamId = " + Convert.ToInt32(txtExamId.Text.ToString()) + "";

        //    //SqlDataSource_ExamGroups.DataBind();
        //    //ExamGroupsGrid.DataSource = SqlDataSource_ExamGroups;
        //    //ExamGroupsGrid.DataBind();

        //    ExamGroupsGrid.CurrentPageIndex = e.NewPageIndex;
        //    ExamGroupsGrid.DataBind();
        //}

        //protected void ExamGroupsGrid_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    ExamGroupsGrid.SelectedIndex = ExamGroupsGrid.SelectedItem.ItemIndex;

        //    //if (QuestionsGrid.SelectedItem.Cells[9].Text != null)
        //    //{
        //    //    foreach (ListItem li in QuestionGroupList.Items)
        //    //    {
        //    //        if (li.Value == QuestionsGrid.SelectedItem.Cells[9].Text)
        //    //        {
        //    //            CorrectAnswerList.SelectedIndex = ((Convert.ToInt32(li.Value)) - 1);
        //    //        }
        //    //    }
        //    //}


        //}

        protected void ExamAreaGroupsGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            ExamAreaGroupsGrid.SelectedIndex = ExamAreaGroupsGrid.SelectedRow.RowIndex;

            //if (QuestionsGrid.SelectedItem.Cells[9].Text != null)
            //{
            //    foreach (ListItem li in QuestionGroupList.Items)
            //    {
            //        if (li.Value == QuestionsGrid.SelectedItem.Cells[9].Text)
            //        {
            //            CorrectAnswerList.SelectedIndex = ((Convert.ToInt32(li.Value)) - 1);
            //        }
            //    }
            //}


        }

        protected void ExamAreaGroupsGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //SqlDataSourceGroups.SelectCommand = "SELECT [GroupId], EG.[ExamId], [GroupName], [QuestionCount] FROM [dbo].[ExamQuestionGroups] EG LEFT OUTER JOIN [dbo].[Exams] E ON EG.ExamId = E.ExamId WHERE EG.ExamId = " + Convert.ToInt32(txtExamId.Text.ToString()) + "";

            //SqlDataSourceGroups.DataBind();
            //ExamAreaGroupsGrid.DataSource = SqlDataSourceGroups;
            //ExamAreaGroupsGrid.DataBind();

            //ExamAreaGroupsGrid.PageIndex = e.NewPageIndex;
            //ExamAreaGroupsGrid.DataBind();

            var dt = (DataTable)Session["ExamAreaGroupsGrid"];
            ExamAreaGroupsGrid.DataSource = dt;
            ExamAreaGroupsGrid.PageIndex = e.NewPageIndex;
            ExamAreaGroupsGrid.DataBind();
        }

        protected void ExamAreaGroupsGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            //GridViewRow row = (GridViewRow)ExamAreaGroupsGrid.Rows[e.RowIndex];

            int rowIndex = 0;
            string delGroupName = "";
            int delQuestionCount = 0;
            int delGroupId = 0;

            DataTable dtCurrentTable = (DataTable)Session["ExamAreaGroupsGrid"];
            rowIndex = e.RowIndex; 
            if (ExamAreaGroupsGrid.Rows.Count > 0)
            {
                delGroupName = ExamAreaGroupsGrid.Rows[rowIndex].Cells[0].Text;
                delQuestionCount = Convert.ToInt32(ExamAreaGroupsGrid.Rows[rowIndex].Cells[1].Text);
                delGroupId = Convert.ToInt32(ExamAreaGroupsGrid.Rows[rowIndex].Cells[2].Text);

                for (int i = 0; i < dtCurrentTable.Rows.Count; i++)
                {
                    if (dtCurrentTable.Rows[i]["GroupName"].ToString() == delGroupName.ToString())
                    {
                        DataRow dr = dtCurrentTable.Rows[i];
                        dtCurrentTable.Rows.Remove(dr);
                        dtCurrentTable.AcceptChanges();
                    }
                }

                Session["ExamAreaGroupsGrid"] = dtCurrentTable;
                ExamAreaGroupsGrid.DataSource = dtCurrentTable;
                ExamAreaGroupsGrid.DataBind();
            }
        }


    }
}