﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.DataVisualization.Charting;

namespace Examination_Portal
{
    public partial class Results : System.Web.UI.Page
    {
        private static int examID;
        private static int userID;

        protected void Page_Load(object sender, EventArgs e)
        {


            double passRate = 0;
            double candidateMarks = 0;
            DateTime dateTaken = DateTime.Now;

            examID = (Int32)(Session["examId"]);
            userID = (Int32)(Session["userId"]);

            if (!IsPostBack)
            {
                string query = "SELECT Top 1 [UserId],[ExamId],[DateTaken],[PassMarks],[TotalMarks],[Status] FROM Results WHERE UserId = " + userID + " AND ExamId = " + examID + " AND CONVERT (date, dateTaken) = CONVERT (date, '" + dateTaken.Date + "') ";

                DataTable dt = GetData(query);
                
                if (dt.Rows.Count > 0) {
                     foreach(DataRow row in dt.Rows)
                     {
                         passRate = Convert.ToDouble(row["PassMarks"]);
                         candidateMarks = Convert.ToDouble(row["TotalMarks"]);

                         if ( (row["Status"].ToString()).Trim() == "PASS")
                         {
                             Greeting.ForeColor = System.Drawing.Color.Blue;
                             Greeting.Text = "Congratulations …… !";
                             Detail.Text = "You have successfully completed the examination";
                             Result.ForeColor = System.Drawing.Color.Green;
                             Result.Text = "PASS";
                         }
                         else {
                             Greeting.ForeColor = System.Drawing.Color.Blue;
                             Greeting.Text = "Sorry......, ";
                             Detail.Text = "You did not receive the required rate to pass the examination.";
                             Result.ForeColor = System.Drawing.Color.Red;
                             Result.Text = "FAIL";
                         }
                         ExpectedScore.Text = passRate.ToString();
                         CandidateScore.Text = candidateMarks.ToString();

                     }
                  
                }

                SqlDataSource_GroupPerformance.SelectCommand = "SP_ExamQuestionsGroupPerformance";

                SqlDataSource_GroupPerformance.SelectParameters["userId"].DefaultValue = userID.ToString();
                SqlDataSource_GroupPerformance.SelectParameters["examId"].DefaultValue = examID.ToString();
                SqlDataSource_GroupPerformance.SelectParameters["dateTaken"].DefaultValue = dateTaken.Date.ToString();
                SqlDataSource_GroupPerformance.DataBind();
                //ExamsResultsGrid.DataBind();

                Chart1.Series["Score"].Points[0].YValues[0] = passRate;
                Chart1.Series["Score"].Points[1].YValues[0] = candidateMarks;

                //ExamsResultsGrid.SelectedIndex = 0;
                //ExamsResultsGrid_SelectedIndexChanged(null, null);

            }
        }

        private static DataTable GetData(string query)
        {
            DataTable dt = new DataTable();
            string constr = ConfigurationManager.ConnectionStrings["ExamPortal"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                }
                return dt;
            }
        }

        protected void ExamsResultsGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            ExamsResultsGrid.CurrentPageIndex = e.NewPageIndex;
            ExamsResultsGrid.DataBind();
        }

        protected void ExamsResultsGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        protected string GetPerformance(int questionsCount, int correctCount)
        {
            int performance = 0;

            if (questionsCount > 0 && correctCount > 0)
            {
                performance = Convert.ToInt32((Convert.ToDouble(correctCount) / Convert.ToDouble(questionsCount)) * 100);
            }

            return performance.ToString() + "%";
        }

    }
}