﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AssignExams.aspx.cs" Inherits="Examination_Portal.AssignExams" %>
<asp:Content ID="assignExamsContentPage" ContentPlaceHolderID="MainContentPageSection" runat="server">
            
    <form id="form1" runat="server" class="exams">

        <div style="padding-top: 10px; padding-bottom: 10px; float: left; width: 100%;">
            <h1>Assigning Exams to Employees</h1>
        </div>

        
        <%--<div class="tab-content" style="padding-top: 20px; padding-left: 140px; padding-bottom: 40px" >--%>
        <div class="areaLeft" style="padding-top: 20px; padding-left: 40px; padding-bottom: 40px" >
            <asp:Table ID="Table1" BorderStyle="None" runat="server">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" width="150px">
                        <asp:Label ID="lblExam" runat="server"  style="padding-top:20px; padding-right:20px; padding-bottom:20px"  Text="Exam :" Font-Size="10pt" Font-Bold="true" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                        <asp:SqlDataSource ID="SqlDataSourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT [ExamId], LTRIM(RTRIM([Name])) + ' : ' + LTRIM(RTRIM([Code]))  AS Exam FROM [dbo].[Exams] ORDER BY [Name]" ProviderName="<%$ ConnectionStrings:ExamPortal.ProviderName %>"></asp:SqlDataSource>
                        <%--<asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSourceExams" DataTextField="Exam" DataValueField="ExamId"  Height="21px"  width ="200px" OnDataBinding="ExamList_PreRender" OnDataBound="ExamList_PreRender" ></asp:DropDownList>--%>
                        <asp:DropDownList ID="ExamList" runat="server" DataSourceID="SqlDataSourceExams" DataTextField="Exam" DataValueField="ExamId"  OnDataBinding="ExamList_PreRender" OnDataBound="ExamList_PreRender" OnSelectedIndexChanged="ExamList_SelectedIndexChanged" AutoPostBack="True"   Height="21px"  width ="300px"></asp:DropDownList>
                        <asp:Label ID="lblExamStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                        <asp:TextBox ID="txtQuestionId" runat="server" Width="100px" Height="20px" Enabled="false" visible="false"></asp:TextBox>
                    </asp:TableCell>

                </asp:TableRow>

<%--                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="150px"> 
                        <asp:Label ID="lblActive" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Is Active : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                        <asp:CheckBox ID="chkActive" runat="server" />
                        <asp:Label ID="lblActiveStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>--%>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="150px"> 
                        <asp:Label ID="lblFinalDate" runat="server" style="padding-top:20px; padding-right:20px" Text="Final Date : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px"  >
                        <asp:TextBox ID="txtFinalDate" runat="server" Enabled ="false" ToolTip='Click the "..." Button to Select a Date'></asp:TextBox>
                        <asp:Button ID="ButtonFinalDate" runat="server" Text="..." OnClick="ButtonFinalDate_Click" ToolTip="Click Here to Select a Date" />
                        <asp:Label ID="lblFinalDateStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>

                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="150px"> 
                        <asp:Label ID="Label1" runat="server" Text="" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>

                    <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left" style="padding-bottom:10px"  >
                        <asp:Calendar ID="CalendarFinalDate" runat="server" BackColor="#FFFFCC" BorderColor="#FFCC66" BorderWidth="1px" DayNameFormat="Shortest" Font-Names="Verdana" Font-Size="8pt" ForeColor="#663399" Height="200px" ShowGridLines="True" Width="220px" Visible="False" OnSelectionChanged="CalendarFinalDate_SelectionChanged">
                            <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" Height="1px"></DayHeaderStyle>

                            <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC"></NextPrevStyle>

                            <OtherMonthDayStyle ForeColor="#CC9966"></OtherMonthDayStyle>

                            <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True"></SelectedDayStyle>

                            <SelectorStyle BackColor="#FFCC66"></SelectorStyle>

                            <TitleStyle BackColor="#990000" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFCC"></TitleStyle>

                            <TodayDayStyle BackColor="#FFCC66" ForeColor="White"></TodayDayStyle>
                        </asp:Calendar>
                    </asp:TableCell>

                </asp:TableRow>

            </asp:Table>

            <div style="padding-top:20px; padding-left:50px; padding-bottom:20px; float: left; width: 90%;">
                   <table style="border-style: None; border-color: inherit; border-width: medium; top: 0px;">
                        <tbody>
                            <tr>
                                <td style="width: 150px"><div style="height: 40px; width: 140px;"> <asp:Button ID="ButtonAdd" class="btn btn-default areaButton " runat="server" Text="Assign Exams" OnClick="ButtonAdd_Click" Width="120px" Height="40px"  /></div></td>
<%--                                <td style="width: 209px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonEdit" class="btn btn-default areaButton" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Width="80px" Height="40px" Visible="false" /></div></td>
                                <td style="width: 150px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonDelete" class="btn btn-default areaButton" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Width="80px" Height="40px" data-toggle="modal" data-target="#myModal" /></div></td>
                                <td style="width: 209px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonCancel" class="btn btn-default areaButton" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" Width="80px" Height="40px" Visible="false" /></div></td>--%>
                                <td style="width: 200%"></td>
                            </tr>
                        </tbody>
                   </table>      
            </div>
        </div>



        <%--<div class="tab-content" style="padding-top: 20px; padding-left: 140px; padding-bottom: 40px" >--%>
        <div class="areaRight" style="padding-top: 20px; padding-left: 10px; padding-bottom: 40px" >
            <div>
                <asp:Label ID="lblCandidates" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px; padding-left:10px" Text="Select Candidates for the Exam : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
            </div>

            <asp:Table ID="Table2" BorderStyle="None" runat="server">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" >
                        <asp:Label ID="Label7" runat="server"  Text="" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>                        
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" >
                        <asp:Label ID="Label8" runat="server"  Text="" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>                        
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" >
                            <asp:Label ID="Label5" runat="server"    Text="User Level :" Font-Size="10pt" Font-Bold="true" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                            <asp:DropDownList ID="LevelList" runat="server" OnDataBinding="LevelList_PreRender" OnDataBound="LevelList_PreRender" OnSelectedIndexChanged="LevelList_SelectedIndexChanged" AutoPostBack="True"  Height="25px" Width="70px" >
                                <asp:ListItem Text="ALL" Value="0" Selected="True"  />
                                <asp:ListItem Text="Level 1" Value="1" />
                                <asp:ListItem Text="Level 2" Value="2" />
                                <asp:ListItem Text="Level 3" Value="3" />
                                <asp:ListItem Text="Level 4" Value="4" />
                                <asp:ListItem Text="Level 5" Value="5" />
                                <asp:ListItem Text="Level 6" Value="6" />
                                <asp:ListItem Text="Level 7" Value="7" />
                                <asp:ListItem Text="Level 8" Value="8" />
                                <asp:ListItem Text="Level 9" Value="9" />   
                           </asp:DropDownList>     
                           <%--</div>--%>                                  
                     </asp:TableCell>

                    </asp:TableRow>


                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" >
                        <asp:Label ID="Label3" runat="server"  Text="Selected Users" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>                        
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" >
                        <asp:Label ID="Label4" runat="server"  Text="" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>                        
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" >
                        <asp:Label ID="Label2" runat="server"  Text="All Users" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>                                           
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="top" HorizontalAlign="Left" >
                        <asp:ListBox ID="LstSelectedUsers" runat="server" Height="200px" Width="200px" SelectionMode="Multiple"></asp:ListBox>
                        <asp:Label ID="lblSelectedUsersStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>

                    </asp:TableCell>

                    <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left" style="padding-bottom:10px; padding-left:5px; padding-right:10px" width ="50px" rowspan="3">
                        <div>
                            <asp:Button ID="ButtonSelectAll" runat="server" Text="<<" width ="40px" OnClick="ButtonSelectAll_Click" />
                        </div>
                        <div>
                            <asp:Button ID="ButtonSelectOne" runat="server" Text="<" width ="40px" OnClick="ButtonSelectOne_Click" />
                        </div>
                        <div>
                            <asp:Button ID="ButtonRemoveOne" runat="server" Text=">" width ="40px" OnClick="ButtonRemoveOne_Click" />
                        </div>
                        <div>
                            <asp:Button ID="ButtonRemoveAll" runat="server" Text=">>" width ="40px" OnClick="ButtonRemoveAll_Click" />
                        </div>
                    </asp:TableCell>

                    <asp:TableCell VerticalAlign="top" HorizontalAlign="Left" >
                        <asp:SqlDataSource ID="SqlDataSourceUsers" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT [UserId], [UserName], [Epf], [AdminUser], [SecurityLevel], [CategoryId]  FROM [dbo].[Users] ORDER BY [UserName]" ProviderName="<%$ ConnectionStrings:ExamPortal.ProviderName %>"></asp:SqlDataSource>
                        <asp:ListBox ID="LstAllUsers" runat="server" Height="200px" Width="200px" DataSourceID="SqlDataSourceUsers" DataTextField ="UserName" DataValueField="UserId" SelectionMode="Multiple" AutoPostBack="True"></asp:ListBox>
                    </asp:TableCell>
                </asp:TableRow>
<%--                <asp:TableRow>
                    <asp:TableCell VerticalAlign="top" HorizontalAlign="Left" >
                        <asp:Label ID="Label6" runat="server"  Text="Completes Users" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>                        
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="top" HorizontalAlign="Left"  >
                        <asp:ListBox ID="LstCompletedUsers" runat="server" Height="200px" Width="200px" Enabled="false"></asp:ListBox>
                    </asp:TableCell>
                </asp:TableRow>--%>

            </asp:Table>
        </div>



        <div class="tab-content" style="padding-top: 50px; padding-left: 100px; padding-bottom: 40px; padding-right:50px; float: left; width: 90%;" >
            <asp:Panel ID="Panel1" runat="server">
                <asp:SqlDataSource ID="SqlDataSource_AssignedExams" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT [UserExamId] , UE.[UserId] AS UserId, UE.[ExamId] AS ExamId, [Name], [UserName], (CASE WHEN [IsActive] = 1 THEN 'PENDING' ELSE 'COMPLETED' END) AS Status,  (CONVERT(DATE, [FinalDate])) AS FinalDate, (CONVERT(DATE, [DateTaken])) AS DateTaken, [TotalMarks] AS Marks, [Status] AS Result FROM [dbo].[UserExams] UE LEFT OUTER JOIN [dbo].[Users] U ON UE.[UserId] = u.UserId LEFT OUTER JOIN [dbo].[Exams] E ON UE.[ExamId] = E.[ExamId]  LEFT OUTER JOIN [dbo].[Results] R ON UE.[ExamId] = R.[ExamId] AND  UE.[UserId] = R.[UserId] WHERE UserName IS NOT NULL" ProviderName="<%$ ConnectionStrings:ExamPortal.ProviderName %>"></asp:SqlDataSource>

                <asp:DataGrid ID="AssignedExamsGrid" runat="server" AllowPaging="True" DataKeyField="UserExamId" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" DataSourceID="SqlDataSource_AssignedExams" HorizontalAlign="Center" OnLoad="Page_Load"
                     AllowSorting="True" OnSelectedIndexChanged="AssignedExamsGrid_SelectedIndexChanged" >
                     <%-- AutoGenerateSelectButton="True"  OnSelectedIndexChanged="AssignedExamsGrid_SelectedIndexChanged" OnPageIndexChanged="AssignedExamsGrid_PageIndexChanged" AllowSorting="True" >--%>
                <Columns>
<%--                    <asp:ButtonColumn CommandName="Select" HeaderText="Select" Text="Select" >
                    </asp:ButtonColumn>--%>
                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderText="Remove" Visible="true">
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBoxRemove" runat="server" Checked ="false"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn HeaderText="User Exam Id" DataField="UserExamId" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Exam Id" DataField="ExamId" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Exam" DataField="Name" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="User Id" DataField="UserId" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="User Name" DataField="UserName" Visible="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Final Date" DataField="FinalDate" DataFormatString = "{0:dd/MM/yyyy}" ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Status" DataField="Status"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Last Attempt Result" DataField="Result"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Marks" DataField="Marks"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Date of Last Attempt " DataField="DateTaken" DataFormatString = "{0:dd/MM/yyyy}" ></asp:BoundColumn>
                </Columns>

                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />

                <SelectedItemStyle BackColor="#FFd18b" Font-Bold="True" ForeColor="Navy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />

                <PagerStyle BackColor="#FBB040" ForeColor="#333333" HorizontalAlign="Center" Mode="NumericPages" />

                <AlternatingItemStyle BackColor="White" />

                <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />


                <HeaderStyle BackColor="#F58220" Font-Bold="True" ForeColor="White" />
                </asp:DataGrid>

            </asp:Panel>


        </div>

           <div style="padding-top:20px; padding-left:92px; padding-bottom:20px; float: left; width: 100%;">
                   <table style="border-style: None; border-color: inherit; border-width: medium; top: 0px;">
                        <tbody>
                            <tr>
                                <td style="width: 150px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonDelete" class="btn btn-default areaButton" runat="server" Text="Delete Selected" OnClick="ButtonDelete_Click" Width="120px" Height="40px" /></div></td>
                                <td style="width: 200%"></td>
                            </tr>
                        </tbody>
                   </table>      
            </div>

    </form>


</asp:Content>
