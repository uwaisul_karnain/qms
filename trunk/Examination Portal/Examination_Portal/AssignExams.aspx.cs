﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

namespace Examination_Portal
{
    public partial class AssignExams : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //code
        }

        protected void ExamList_PreRender(object sender, EventArgs e)
        {
            if (ExamList.Items.Count > 0)
            {
                ExamList.SelectedIndex = 0;
                SqlDataSourceUsers.SelectCommand = "SELECT [UserId], [UserName], [Epf], [AdminUser], [SecurityLevel], [CategoryId]  FROM [dbo].[Users] U WHERE U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[UserExams] WHERE ExamId = " + ExamList.SelectedValue + " ) AND U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[Results] WHERE ExamId = " + ExamList.SelectedValue + " ) ORDER BY [UserName]";

                SqlDataSourceUsers.DataBind();
                LstAllUsers.DataBind();

                SqlDataSource_AssignedExams.SelectCommand = "SELECT [UserExamId] , UE.[UserId] AS UserId, UE.[ExamId] AS ExamId, [Name], [UserName], (CASE WHEN [IsActive] = 1 THEN 'PENDING' ELSE 'COMPLETED' END) AS Status,  (CONVERT(DATE, [FinalDate])) AS FinalDate, (CONVERT(DATE, [DateTaken])) AS DateTaken, [TotalMarks] AS Marks, [Status] AS Result FROM [dbo].[UserExams] UE LEFT OUTER JOIN [dbo].[Users] U ON UE.[UserId] = u.UserId LEFT OUTER JOIN [dbo].[Exams] E ON UE.[ExamId] = E.[ExamId]  LEFT OUTER JOIN [dbo].[Results] R ON UE.[ExamId] = R.[ExamId] AND  UE.[UserId] = R.[UserId] WHERE UE.[ExamId] = " + ExamList.SelectedValue + " AND UserName IS NOT NULL";

                SqlDataSource_AssignedExams.DataBind();
                AssignedExamsGrid.DataBind();

            }
        }

        protected void ExamList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlDataSourceUsers.SelectCommand = "SELECT [UserId], [UserName], [Epf], [AdminUser], [SecurityLevel], [CategoryId]  FROM [dbo].[Users] U WHERE U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[UserExams] WHERE ExamId = " + ExamList.SelectedValue + " AND [IsActive] = 1 ) AND U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[Results] WHERE ExamId = " + ExamList.SelectedValue + " ) ORDER BY [UserName]";

            SqlDataSourceUsers.DataBind();
            LstAllUsers.DataBind();

            SqlDataSource_AssignedExams.SelectCommand = "SELECT [UserExamId] , UE.[UserId] AS UserId, UE.[ExamId] AS ExamId, [Name], [UserName], (CASE WHEN [IsActive] = 1 THEN 'PENDING' ELSE 'COMPLETED' END) AS Status,  (CONVERT(DATE, [FinalDate])) AS FinalDate, (CONVERT(DATE, [DateTaken])) AS DateTaken, [Status] AS Result FROM [dbo].[UserExams] UE LEFT OUTER JOIN [dbo].[Users] U ON UE.[UserId] = u.UserId LEFT OUTER JOIN [dbo].[Exams] E ON UE.[ExamId] = E.[ExamId]  LEFT OUTER JOIN [dbo].[Results] R ON UE.[ExamId] = R.[ExamId] AND  UE.[UserId] = R.[UserId] WHERE UE.[ExamId] = " + ExamList.SelectedValue + " AND UserName IS NOT NULL";

            SqlDataSource_AssignedExams.DataBind();
            AssignedExamsGrid.DataBind();

        }

        protected void ButtonFinalDate_Click(object sender, EventArgs e)
        {
            CalendarFinalDate.Visible = true;
        }

        protected void CalendarFinalDate_SelectionChanged(object sender, EventArgs e)
        {
            txtFinalDate.Text = CalendarFinalDate.SelectedDate.ToShortDateString();
            CalendarFinalDate.Visible = false;

        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {            
            if (DataValidated() == false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Incomplete data. Please enter compulsary data marked as *');", true);
                return;
            }
            //Insert Record
            AddExams();
            ClearControls();
      }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            bool itemSelected = false;

            for (int i = AssignedExamsGrid.Items.Count - 1; i >= 0; i--)
            {
                // Access the CheckBox
                CheckBox cb = (CheckBox)AssignedExamsGrid.Items[i].FindControl("CheckBoxRemove");
                if (cb != null && cb.Checked)
                {
                    itemSelected = true;
                }
            }
            if (itemSelected == false)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Please select a record to Delete.');", true);
                return;
            }
            else
            {
                DeleteAssignedEams();
                ClearControls();
            }
        }

        protected void LevelList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ExamList.Items.Count > 0)
            {
                if (LevelList.SelectedIndex == 0)
                    SqlDataSourceUsers.SelectCommand = "SELECT [UserId], [UserName], [Epf], [AdminUser], [SecurityLevel], [CategoryId]  FROM [dbo].[Users] U WHERE U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[UserExams]  WHERE ExamId = " + ExamList.SelectedValue + " AND [IsActive] = 1 ) AND U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[Results] WHERE ExamId = " + ExamList.SelectedValue + " ) ORDER BY [UserName]";
                else
                    SqlDataSourceUsers.SelectCommand = "SELECT [UserId], [UserName], [Epf], [AdminUser], [SecurityLevel], [CategoryId]  FROM [dbo].[Users] U WHERE U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[UserExams] WHERE ExamId = " + ExamList.SelectedValue + " AND [IsActive] = 1 ) AND U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[Results] WHERE ExamId = " + ExamList.SelectedValue + " ) AND [CategoryId] = " + LevelList.SelectedValue + " ORDER BY [UserName]";

            }
            else
                SqlDataSourceUsers.SelectCommand = "";


            SqlDataSourceUsers.DataBind();
            LstAllUsers.DataBind();
        }

        protected void LevelList_PreRender(object sender, EventArgs e)
        {
            if (ExamList.Items.Count > 0)
            {
                SqlDataSourceUsers.SelectCommand = "SELECT [UserId], [UserName], [Epf], [AdminUser], [SecurityLevel], [CategoryId]  FROM [dbo].[Users] U WHERE U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[UserExams] WHERE ExamId = " + ExamList.SelectedValue + " AND [IsActive] = 1 ) AND U.[UserId] NOT IN (SELECT [UserId] FROM [dbo].[Results] WHERE ExamId = " + ExamList.SelectedValue + " ) ORDER BY [UserName]";

                SqlDataSourceUsers.DataBind();
                LstAllUsers.DataBind();
            }
        }

        protected void ButtonSelectAll_Click(object sender, EventArgs e)
        {
            if (LstAllUsers.Items.Count > 0 )
            {
                for (int i = LstAllUsers.Items.Count - 1; i >= 0; i--)
                {
                    LstSelectedUsers.Items.Add(LstAllUsers.Items[i]);
                    LstSelectedUsers.ClearSelection();
                    LstAllUsers.Items.Remove(LstAllUsers.Items[i]);
                }
            }
        }

        protected void ButtonSelectOne_Click(object sender, EventArgs e)
        {
            if (LstAllUsers.Items.Count > 0 )
            {
                if (LstAllUsers.SelectedIndex > -1)
                {
                    for (int i = LstAllUsers.Items.Count - 1; i >= 0; i--)
                    {
                        if (LstAllUsers.Items[i].Selected)
                        {
                            LstSelectedUsers.Items.Add(LstAllUsers.Items[i]);
                            LstSelectedUsers.ClearSelection();
                            LstAllUsers.Items.Remove(LstAllUsers.Items[i]);
                        }
                    }
                }
                else { }

            }
        }

        protected void ButtonRemoveOne_Click(object sender, EventArgs e)
        {
            if (LstSelectedUsers.Items.Count > 0)
            {
                if (LstSelectedUsers.SelectedIndex > -1)
                {
                    for (int i = LstSelectedUsers.Items.Count - 1; i >= 0; i--)
                    {
                        if (LstSelectedUsers.Items[i].Selected)
                        {
                            LstAllUsers.Items.Add(LstSelectedUsers.Items[i]);
                            LstAllUsers.ClearSelection();
                            LstSelectedUsers.Items.Remove(LstSelectedUsers.Items[i]);
                        }
                    }
                }
                else { }

            }
        }

        protected void ButtonRemoveAll_Click(object sender, EventArgs e)
        {
            if (LstSelectedUsers.Items.Count > 0)
            {
                for (int i = LstSelectedUsers.Items.Count - 1; i >= 0; i--)
                {
                    LstAllUsers.Items.Add(LstSelectedUsers.Items[i]);
                    LstAllUsers.ClearSelection();
                    LstSelectedUsers.Items.Remove(LstSelectedUsers.Items[i]);
                }
            }
        }

        protected void ClearControls()
        {
            //if (ExamList.Items.Count > 0)
            //    ExamList.SelectedIndex = 0;
            txtFinalDate.Text = "";
            LstSelectedUsers.Items.Clear();
        }

        protected bool DataValidated()
        {
            bool dataValidated = true;

            if (ExamList.Items.Count > 0 && ExamList.Text == "")
            {
                dataValidated = false;
                lblExamStar.Visible = true;
            }
            else
                lblExamStar.Visible = false;

            if (txtFinalDate.Text == "")
            {
                dataValidated = false;
                lblFinalDateStar.Visible = true;
            }
            else
                lblFinalDateStar.Visible = false;

            if (LstSelectedUsers.Items.Count == 0)
            {
                dataValidated = false;
                lblSelectedUsersStar.Visible = true;
            }
            else
                lblSelectedUsersStar.Visible = false;

            return dataValidated;
        }

        protected void AddExams()
        {

            for (int i = LstSelectedUsers.Items.Count - 1; i >= 0; i--)
            {
                //long exid = 3;
                string insertSQL = "INSERT INTO [dbo].[UserExams] ([UserId], [ExamId], [IsActive], [FinalDate]) VALUES ( " + Convert.ToInt32(LstSelectedUsers.Items[i].Value) + ", " + ExamList.SelectedValue  + ", 1, '" +  Convert.ToDateTime(txtFinalDate.Text) + "')";

                SqlDataSource_AssignedExams.InsertCommand = insertSQL;
                SqlDataSource_AssignedExams.Insert();

            }

            SqlDataSource_AssignedExams.DataBind();
            AssignedExamsGrid.DataBind();

            //for (int i = LstSelectedUsers.Items.Count - 1; i >= 0; i--)
            //{

            //    SendMail();
            //}

        }


        //protected void SendMail()
        //{
        //    try
        //    {
        //        MailMessage mailMessage = new MailMessage();
        //        mailMessage.To.Add("gothami.peiris@attuneconsulting.com");
        //        mailMessage.From = new MailAddress("gothami.peiris@attuneconsulting.com");
        //        mailMessage.Subject = "New Exam Assigned";
        //        mailMessage.Body = "You have new exammed assigned!";
        //        SmtpClient smtpClient = new SmtpClient("smtp.your-isp.com");
        //        smtpClient.Send(mailMessage);
        //        Response.Write("E-mail sent!");
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.Write("Could not send the e-mail - error: " + ex.Message);
        //    }
        //}

        protected void DeleteAssignedEams()
        {
            for (int i = AssignedExamsGrid.Items.Count - 1; i >= 0; i--)
            {
                // Access the CheckBox
                CheckBox cb = (CheckBox)AssignedExamsGrid.Items[i].FindControl("CheckBoxRemove");
                if (cb != null && cb.Checked)
                {
                    string deleteSQL = "DELETE FROM [dbo].[UserExams] WHERE [UserExamId] = " + Convert.ToInt32(AssignedExamsGrid.Items[i].Cells[1].Text) + "";

                    SqlDataSource_AssignedExams.DeleteCommand = deleteSQL;
                    SqlDataSource_AssignedExams.Delete();
                }
            }
            SqlDataSource_AssignedExams.DataBind();
            AssignedExamsGrid.DataBind();

        }

        protected void AssignedExamsGrid_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}