﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ExamQuestions.aspx.cs" Inherits="Examination_Portal.ExamQuestions" %>

<asp:Content ID="examQuestionsContentPage" ContentPlaceHolderID="MainContentPageSection" runat="server">

    
    <form id="form1" runat="server" class="users">

        <div style="padding-top: 10px; padding-bottom: 10px; float: left; width: 100%;">
            <h1>Exam Questions Management</h1>
        </div>

        <div class="tab-content" style="padding-top: 20px; padding-left: 140px; padding-bottom: 40px; width: 100%;" >


            <asp:Table ID="tableQuestions" BorderStyle="None" runat="server" Width="100%">
                <asp:TableRow>

                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" width="150px">
                        <asp:Label ID="lblExam" runat="server"  style="padding-top:20px; padding-right:20px; padding-bottom:20px"  Text="Exam :" Font-Size="10pt" Font-Bold="true" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                        <asp:SqlDataSource ID="SqlDataSourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT [ExamId], LTRIM(RTRIM([Name])) + ' : ' + LTRIM(RTRIM([Code]))  AS Exam FROM [dbo].[Exams] ORDER BY [Name]" ProviderName="<%$ ConnectionStrings:ExamPortal.ProviderName %>"></asp:SqlDataSource>
                        <%--<asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSourceExams" DataTextField="Exam" DataValueField="ExamId"  Height="21px"  width ="200px" OnDataBinding="ExamList_PreRender" OnDataBound="ExamList_PreRender" ></asp:DropDownList>--%>
                        <asp:DropDownList ID="ExamList" runat="server" DataSourceID="SqlDataSourceExams" DataTextField="Exam" DataValueField="ExamId"  OnDataBinding="ExamList_PreRender" OnDataBound="ExamList_PreRender" OnSelectedIndexChanged="ExamList_SelectedIndexChanged" AutoPostBack="True"   Height="21px"  width ="400px"></asp:DropDownList>
                        <asp:Label ID="lblExamStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                        <asp:TextBox ID="txtQuestionId" runat="server" Width="100px" Height="20px" Enabled="false" visible="false"></asp:TextBox>
                    </asp:TableCell>

                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" width="150px">
                        <asp:Label ID="lblGroup" runat="server"  style="padding-top:20px; padding-right:20px; padding-bottom:20px"  Text="Question Group :" Font-Size="10pt" Font-Bold="true" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                        <asp:SqlDataSource ID="SqlDataSourceQuestionGroups" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="" ProviderName="<%$ ConnectionStrings:ExamPortal.ProviderName %>">
<%--                            <SelectParameters>
                                <asp:Parameter Name="examId" Type="Int32" DefaultValue=""/>
                            </SelectParameters>--%>
                        </asp:SqlDataSource>
                        <asp:DropDownList ID="QuestionGroupList" runat="server" DataSourceID="SqlDataSourceQuestionGroups" DataTextField="GroupName" DataValueField="GroupId" AutoPostBack="True" Height="21px"  width ="400px" ></asp:DropDownList>
                        <asp:Label ID="lblQuestionGroupStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="150px"> 
                        <asp:Label ID="lblQuestion" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Question : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtQuestion" runat="server" Width="750px" Height="50px" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                            <asp:Label ID="lblQuestionStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="150px"> 
                        <asp:Label ID="lblAnswer1" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Answer 1 : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtAnswer1" runat="server" Width="750px" Height="30px" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                            <asp:Label ID="lblStar1" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="150px"> 
                        <asp:Label ID="lblAnswer2" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Answer 2 : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtAnswer2" runat="server" Width="750px" Height="30px" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                            <asp:Label ID="lblStar2" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="150px"> 
                        <asp:Label ID="lblAnswer3" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Answer 3 : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtAnswer3" runat="server" Width="750px" Height="30px" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                            <asp:Label ID="lblStar3" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="150px"> 
                        <asp:Label ID="lblAnswer4" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Answer 4 : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtAnswer4" runat="server" Width="750px" Height="30px" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                            <asp:Label ID="lblStar4" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="150px">
                        <asp:Label ID="lblCorrectAnswer" runat="server"  style="padding-top:20px; padding-right:20px; padding-bottom:20px"  Text="Correct Answer :" Font-Size="10pt" Font-Bold="true" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                        <asp:DropDownList ID="CorrectAnswerList" runat="server"  Height="21px" Width="140px">
                            <asp:ListItem Text="Answer 1" Value="1" Selected="True"  />
                            <asp:ListItem Text="Answer 2" Value="2" />
                            <asp:ListItem Text="Answer 3" Value="3" />
                            <asp:ListItem Text="Answer 4" Value="4" />
                        </asp:DropDownList>
                        <asp:Label ID="lblCorrectAnswerStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>

                </asp:TableRow>

            </asp:Table>
        </div>

        <div style="padding-top:20px; padding-left:96px; padding-bottom:20px; float: left; width: 100%;">
               <table style="border-style: None; border-color: inherit; border-width: medium; top: 0px;">
                    <tbody>
                        <tr>
                            <td style="width: 150px"><div style="height: 40px; width: 140px;"> <asp:Button ID="ButtonAdd" class="btn btn-default areaButton " runat="server" Text="New" OnClick="ButtonAdd_Click" Width="80px" Height="40px"  /></div></td>
                            <td style="width: 209px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonEdit" class="btn btn-default areaButton" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Width="80px" Height="40px" /></div></td>
                            <td style="width: 150px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonDelete" class="btn btn-default areaButton" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Width="80px" Height="40px" data-toggle="modal" data-target="#myModal" /></div></td>
                            <td style="width: 209px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonCancel" class="btn btn-default areaButton" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" Width="80px" Height="40px" Visible="false" /></div></td>
                            <td style="width: 200%"></td>
                        </tr>
                    </tbody>
               </table>      
        </div>

        <div class="tab-content" style="padding-top: 50px; padding-left: 100px; padding-bottom: 40px; padding-right:50px; float: left; width: 90%;" >
            <asp:Panel ID="Panel1" runat="server">
                <asp:SqlDataSource ID="SqlDataSource_Questions" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="" ProviderName="<%$ ConnectionStrings:ExamPortal.ProviderName %>"></asp:SqlDataSource>

                <asp:DataGrid ID="QuestionsGrid" runat="server" AllowPaging="True" DataKeyField="QuestionId" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" DataSourceID="SqlDataSource_Questions" HorizontalAlign="Center" OnLoad="Page_Load" AutoGenerateSelectButton="True" AutoPostBack="True"
                     OnSelectedIndexChanged="QuestionsGrid_SelectedIndexChanged" OnPageIndexChanged="QuestionsGrid_PageIndexChanged" AllowSorting="True" >

                <Columns>
                    <asp:ButtonColumn CommandName="Select" HeaderText="Select" Text="Select" >
                    </asp:ButtonColumn>
                    <asp:BoundColumn HeaderText="Question Id" DataField="QuestionId" Visible="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Group Id" DataField="GroupId" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Question Area" DataField="GroupName" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Question" DataField="Question"  ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Answer 1" DataField="Answer1"  ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Answer 2" DataField="Answer2"  ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Answer 3" DataField="Answer3"  ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Answer 4" DataField="Answer4"  ></asp:BoundColumn> 
                    <asp:BoundColumn HeaderText="Correct Answer" DataField="CorrectAnswer" ></asp:BoundColumn>
                </Columns>

                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />

                <SelectedItemStyle BackColor="#FFd18b" Font-Bold="True" ForeColor="Navy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />

                <PagerStyle BackColor="#FBB040" ForeColor="#333333" HorizontalAlign="Center" Mode="NumericPages" />

                <AlternatingItemStyle BackColor="White" />

                <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />


                <HeaderStyle BackColor="#F58220" Font-Bold="True" ForeColor="White" />
                </asp:DataGrid>

            </asp:Panel>
        </div>




    </form>

</asp:Content>
