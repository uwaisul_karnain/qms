﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="Examination_Portal.Users" EnableEventValidation="false" %>


<asp:Content ID="usersContent" ContentPlaceHolderID="MainContentPageSection" runat="server">

    <form id="form1" runat="server" class="users">

        <div style="padding-top: 10px; padding-bottom: 10px; float: left; width: 100%;">
            <h1>User Management</h1>
        </div>

        <div class="tab-content" style="padding-top: 20px; padding-left: 140px; padding-bottom: 40px; width: 100%">


            <asp:Table ID="tblUsers" BorderStyle="None" runat="server" Width="90%">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" > 
                        <asp:Label ID="lblUserName" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="User Name : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtUserLoginID" runat="server" Width="380px" Height="20px" Enabled="false"></asp:TextBox>
                            <asp:Label ID="lblUIDStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:Label ID="lblExample" runat="server" style="padding-left:20px" Height="20px" Text="eg: Linda.Sonds@attuneconsulting.com" Font-Size="8pt" Font-Names ="Verdana"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                        <asp:Label ID="Label1" runat="server"  style="padding-top:20px; padding-right:20px; padding-bottom:20px"  Text="User Category :" Font-Size="10pt" Font-Bold="true" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                        <asp:DropDownList ID="DDListUserCategory" runat="server" Height="21px" Width="380px" DataSourceID="SqlDataSour_Categories" DataValueField="CategoryId" DataTextField="CategoryName">
                        </asp:DropDownList>
                        <asp:Label ID="lblUserCategory" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                        <asp:SqlDataSource ID="SqlDataSour_Categories" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT DISTINCT [CategoryId],[CategoryName] FROM [dbo].UserCategory"></asp:SqlDataSource>       
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                        <asp:Label ID="lblEPF" runat="server"  style="padding-top:20px; padding-right:20px; padding-bottom:20px"  Text="Employee Number : " Font-Size="10pt" Font-Bold="true" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                            <asp:TextBox ID="txtEPF" runat="server" Height="20px" Width="140px" Enabled="false"></asp:TextBox>
                            <asp:Label ID="lblEPFNo" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:Label ID="lblUserId" runat="server" Text="" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right">
                        <asp:Label ID="Status" runat="server"  style="padding-top:20px; padding-right:20px; padding-bottom:20px"  Text="User Type :" Font-Size="10pt" Font-Bold="true" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                        <asp:DropDownList ID="ddListUserType" runat="server"  Height="21px" Width="140px">
                            <asp:ListItem Selected="True">User</asp:ListItem>
                            <asp:ListItem>Admin</asp:ListItem>
                        </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>
<%--                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" >
                        <asp:Label ID="lblSecurityLevel" runat="server"  style="padding-top:20px; padding-right:20px; padding-bottom:20px"  Text="Security Level :"  Font-Size="10pt" Font-Bold="true" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                    <asp:DropDownList ID="ddListSecurityLevel" runat="server" Height="21px" Width="140px" >
                        <asp:ListItem Selected="True">Level 1</asp:ListItem>
                        <asp:ListItem>Level 2</asp:ListItem>
                        <asp:ListItem>Level 3</asp:ListItem>
                        <asp:ListItem>Level 4</asp:ListItem>
                        <asp:ListItem>Level 5</asp:ListItem>
                        <asp:ListItem>Level 6</asp:ListItem>
                        <asp:ListItem>Level 7</asp:ListItem>
                        <asp:ListItem>Level 8</asp:ListItem>
                        <asp:ListItem>Level 9</asp:ListItem>
                    </asp:DropDownList>
                    </asp:TableCell>
                </asp:TableRow>--%>

            </asp:Table>
        </div>

        <div style="padding-top:20px; padding-left:95px; padding-bottom:20px; float: left; width: 100%;">
               <table style="border-style: None; border-color: inherit; border-width: medium; top: 0px;">
                    <tbody>
                        <tr>
                            <td style="width: 150px"><div style="height: 40px; width: 140px;"> <asp:Button ID="ButtonAdd" class="btn btn-default areaButton " runat="server" Text="New" OnClick="ButtonAdd_Click" Width="80px" Height="40px"  /></div></td>
                            <td style="width: 209px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonEdit" class="btn btn-default areaButton" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Width="80px" Height="40px" /></div></td>
                            <td style="width: 150px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonDelete" class="btn btn-default areaButton" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Width="80px" Height="40px" data-toggle="modal" data-target="#myModal" /></div></td>
                            <td style="width: 209px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonCancel" class="btn btn-default areaButton" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" Width="80px" Height="40px" Visible="false" /></div></td>
                            <td style="width: 200%"></td>
                        </tr>
                    </tbody>
               </table>      
        </div>

        <div class="tab-content" style="padding-top: 50px; padding-left: 100px; padding-bottom: 40px; padding-right:50px; float: left; width: 90%;" >
            <asp:Panel ID="Panel1" runat="server">
                <asp:SqlDataSource ID="SqlDataSource_Users" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT Users.UserId, Users.UserName, Users.Epf, (SELECT CASE WHEN [AdminUser] = 1 THEN 'Admin' ELSE 'User' END AS Expr1) AS UserType, (SELECT CASE WHEN [SecurityLevel] = 1 THEN 'Level 1' WHEN [SecurityLevel] = 2 THEN 'Level 2' WHEN [SecurityLevel] = 3 THEN 'Level 3' ELSE '' END AS Expr1) AS SecurityLevel, Users.CategoryId As CategoryID, Category.CategoryName FROM Users AS Users INNER JOIN UserCategory AS Category ON Users.CategoryId = Category.CategoryId ORDER BY Users.UserName, Users.AdminUser, SecurityLevel" ProviderName="<%$ ConnectionStrings:ExamPortal.ProviderName %>"></asp:SqlDataSource>

                <asp:DataGrid ID="UserDetailGrid" runat="server" PageSize="10" AllowPaging="True" DataKeyField="UserId" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" DataSourceID="SqlDataSource_Users" HorizontalAlign="Center" OnLoad="Page_Load" AutoGenerateSelectButton="True" AutoPostBack="True"
                     OnSelectedIndexChanged="UserDetailGrid_SelectedIndexChanged" OnPageIndexChanged="UserDetailGrid_PageIndexChanged" AllowSorting="True" >
                    <%--OnPageIndexChanged="Grid_PageIndexChanged" OnCancelCommand="Grid_CancelCommand" OnDeleteCommand="Grid_DeleteCommand" OnEditCommand="Grid_EditCommand" OnUpdateCommand="Grid_UpdateCommand">--%>

                <Columns>
                    <asp:ButtonColumn CommandName="Select" HeaderText="Select" Text="Select" >
                    </asp:ButtonColumn>

                    <asp:BoundColumn HeaderText="User Id" DataField="UserId" Visible="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="User Name" DataField="UserName"  ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Employee No." DataField="EPF"  ></asp:BoundColumn>                    
                    <asp:BoundColumn HeaderText="User Type" DataField="UserType" ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Security Level" DataField="SecurityLevel" visible="false"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="CategoryId" DataField="CategoryID" visible="false" ></asp:BoundColumn>

<%--                    <asp:EditCommandColumn EditText="Edit" CancelText="Cancel" UpdateText="Update" HeaderText="Edit">
                    </asp:EditCommandColumn>
                    <asp:ButtonColumn CommandName="Delete" HeaderText="Delete" Text="Delete">
                    </asp:ButtonColumn>--%>


                </Columns>

                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />

                <SelectedItemStyle BackColor="#FFd18b" Font-Bold="True" ForeColor="Navy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />

                <PagerStyle BackColor="#FBB040" ForeColor="#333333" HorizontalAlign="Center" Mode="NumericPages" />

                <AlternatingItemStyle BackColor="White" />

                <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />

                <%--<HeaderStyle BackColor="#D70000" Font-Bold="True" ForeColor="White" />--%>
                <HeaderStyle BackColor="#F58220" Font-Bold="True" ForeColor="White" />
                </asp:DataGrid>
            </asp:Panel>
        </div>

<%--<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch demo modal
</button>--%>



    </form>

</asp:Content>

