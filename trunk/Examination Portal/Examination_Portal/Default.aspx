﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Examination_Portal._Default" %>

<asp:Content ID="DefaultContent" ContentPlaceHolderID="MainContentPageSection" runat="server">

    <form id="form1" runat="server">
<%--    <div class="jumbotron" >
        <p class="lead"></p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-large">Learn more &raquo;</a></p>
        
    </div>--%>

        <div style="padding-top: 10px; padding-bottom: 10px; float: left; width: 100%;">
                <h1>Examination Portal</h1>
        </div>
    
        <div class="areaQuestion">            
            <h2>Examinations Pending </h2>

            <p style="padding-bottom: 20px">
                <asp:Label ID="Instruction" runat="server" Font-Size="10pt" Font-Names ="Verdana" Text="Please select the Examination to continue :   " ></asp:Label>
                <asp:DropDownList ID="UserExamList" runat="server" DataSourceID="SqlDataSourceExams" DataTextField="Name" DataValueField="ExamId" width ="200px" OnDataBinding="UserExamList_PreRender" OnDataBound="UserExamList_PreRender" ></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" 
                    SelectCommand="SELECT DISTINCT UserExams.[UserExamId], Exams.[ExamId], [Code], [Name], [UserName] FROM Exams INNER JOIN UserExams ON Exams.ExamId = UserExams.ExamId INNER JOIN Users ON UserExams.UserId = Users.UserId WHERE Users.UserName = @userName AND UserExams.IsActive = 1 AND UserExams.FinalDate >= (SELECT CONVERT(DATE, GETDATE()))"
                    UpdateCommand="UPDATE UserExams SET IsActive=1 WHERE UserExamId = @UserExamId ">
                      <SelectParameters>
                        <asp:QueryStringParameter  Name="userName" QueryStringField="UserName" />
                      </SelectParameters>
                    <UpdateParameters>
                          <asp:ControlParameter Name="UserExamId" ControlId="UserExamList" PropertyName="SelectedValue"/>
                    </UpdateParameters>              
                </asp:SqlDataSource>

            </p>           

        </div>
   
        <asp:Label ID="MsgLabel" runat="server" Text="No Examinations are listed for you at the moment......" ForeColor="Red" Visible="false"></asp:Label>

        <asp:Button ID="Start" class="btn btn-default" runat="server" Text="Get Started &raquo;" OnClick="On_Click" />

        <%--<button onclick="examStartFunction()" >Get Started &raquo;</button>--%>



        <script type = "text/javascript">
            function examStartFunction()
            {
                var examWindow = window.open("Questions.aspx", "", "fullscreen:yes; resizable: no; titlebar: no; toolbars: no; menubar = no; scrollbars=yes; controllers:false ");

            }

        </script>

    </form>

</asp:Content>
