﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.DataVisualization.Charting;
using System.Timers;



using Examination_Portal.Models;namespace Examination_Portal
{
    public partial class FullScree : BasePage
    {
        # region varable declations        

            //Datatable value variables
            private static int userID;
            private static int examID;
            public string examNAME;


            private static int totalMarks = 0;
            private static int passMarks = 0;
            private static double numberOfQuestions = 0.00;
            private static DateTime examTime = new DateTime();

            //Candidate Exam variables
            private static int correctCount = 0;
            private static int unselectedCount = 0;
            private static bool answered = false;
        
            // Initialization Variable
            private static int count = 1;

            //TimeSpan variables for Timer
            private static TimeSpan baseTimeSpan = new TimeSpan();
            private static TimeSpan changeTime = new TimeSpan();

            //Alert popup variables
            private static DateTime popUpStartTime = new DateTime();
            private static DateTime popUpEndTime = new DateTime();
            private static bool popUpActivated = false;
            private static bool examTimeOut = false;

            //Database Variables
            //SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=|DataDirectory|\ExamPortal.mdf;Integrated Security=True");
            string constr = ConfigurationManager.ConnectionStrings["ExamPortal"].ConnectionString;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            MasterPage mp = new MasterPage();
            mp.ID = "MasterPage.Master";
            mp.Visible = false;

            if (!IsPostBack)
            {
                examID = (Int32)(Session["examId"]);
                userID = (Int32)(Session["userId"]);
                examNAME = (string)(Session["examName"]);

                totalMarks = 0;


                string query = "SELECT TOP 1 [ExamId], [Code], [Name], [NumberOfQuestions], [PassMarkPercentage], [Duration], GETDATE() as ExamDate FROM Exams Where ExamId = " + examID + "";
                DataTable dt = GetDataQuestions(query);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        passMarks = Convert.ToInt32(row["PassMarkPercentage"]);
                        numberOfQuestions = Convert.ToDouble(row["NumberOfQuestions"]);
                        
                        //var examTime = DateTime.Parse(row["Duration"].ToString());
                        examTime = DateTime.Parse(row["Duration"].ToString());
                        int examHours = examTime.Hour,
                            examMinutes = examTime.Minute,
                            examSeconds = examTime.Second;
                        TimeSpan examTimeSpan = new TimeSpan(examHours, examMinutes, examSeconds);
                        baseTimeSpan = examTimeSpan;
                    }

                }

                
                TimerLabel.Text = baseTimeSpan.ToString();
                count = 1;
            }
            else {

                if (popUpActivated==true)
                {

                popUpEndTime = DateTime.Now;

                TimeSpan span = popUpEndTime.Subtract(popUpStartTime);
 
                span = baseTimeSpan.Subtract(span);
                TimeSpan newTime = new TimeSpan(span.Hours, span.Minutes, span.Seconds);
                baseTimeSpan = newTime;
                count = 1;
                popUpActivated = false;

                }
            }

        }


        //SQL Server connection databinding
        private static DataTable GetDataQuestions(string query)
        {
            DataTable dt = new DataTable();
            string constr = ConfigurationManager.ConnectionStrings["ExamPortal"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                }
                return dt;
            }
        }

        protected void ButtonSubmit_Click(object sender, EventArgs e)
        {
            //long userId = 1;
            DateTime dateTaken = DateTime.Now;
            string status = "";

            examTime = examTime.Subtract(baseTimeSpan);

            int indexCounter = 0;
            //int[,] userAnsewers = new int[ListViewQuestions.Items.Count, 2];
            int[,] userAnsewers = new int[ListViewQuestions.Items.Count, 5];

            unselectedCount = 0;
            correctCount = 0;

            foreach (ListViewDataItem item in ListViewQuestions.Items)
            {

                var rb1 = (RadioButton)item.FindControl("Answer1");
                var rb2 = (RadioButton)item.FindControl("Answer2");
                var rb3 = (RadioButton)item.FindControl("Answer3");
                var rb4 = (RadioButton)item.FindControl("Answer4");

                var questionVal = (Label)item.FindControl("QuestionID");
                var questionID = questionVal.Text;

                var groupVal = (Label)item.FindControl("GroupId");
                var groupID = groupVal.Text;

                var correctVal = (Label)item.FindControl("CorrectAnswer");
                var correctAnswer = correctVal.Text;

                //var numQuesVal = (Label)item.FindControl("NumberOfQuestions");
                //numberOfQuestions = Convert.ToDouble(numQuesVal.Text);

                answered = false;

                if (rb1.Checked)
                {
                    //selectedAnswer = 1;
                    answered = true;
                    userAnsewers[item.DataItemIndex, 0] = item.DataItemIndex + 1;
                    userAnsewers[item.DataItemIndex, 1] = 1;
                    userAnsewers[item.DataItemIndex, 2] = Convert.ToInt32(questionID) ;
                    userAnsewers[item.DataItemIndex, 3] = Convert.ToInt32(groupID);
                    userAnsewers[item.DataItemIndex, 4] = Convert.ToInt32(correctAnswer);
                    if (correctAnswer == "1")
                    {
                        correctCount = correctCount + 1;
                    }
                }
                else if (rb2.Checked)
                {
                    //selectedAnswer = 2;
                    answered = true;
                    userAnsewers[item.DataItemIndex, 0] = item.DataItemIndex + 1;
                    userAnsewers[item.DataItemIndex, 1] = 2;
                    userAnsewers[item.DataItemIndex, 2] = Convert.ToInt32(questionID);
                    userAnsewers[item.DataItemIndex, 3] = Convert.ToInt32(groupID);
                    userAnsewers[item.DataItemIndex, 4] = Convert.ToInt32(correctAnswer);
                    if (correctAnswer == "2")
                    {
                        correctCount = correctCount + 1;
                    }
                }
                else if (rb3.Checked)
                {
                    //selectedAnswer = 3;
                    answered = true;
                    userAnsewers[item.DataItemIndex, 0] = item.DataItemIndex + 1;
                    userAnsewers[item.DataItemIndex, 1] = 3;
                    userAnsewers[item.DataItemIndex, 2] = Convert.ToInt32(questionID);
                    userAnsewers[item.DataItemIndex, 3] = Convert.ToInt32(groupID);
                    userAnsewers[item.DataItemIndex, 4] = Convert.ToInt32(correctAnswer);
                    if (correctAnswer == "3")
                    {
                        correctCount = correctCount + 1;
                    }
                }
                else if (rb4.Checked)
                {
                    //selectedAnswer = 4;
                    answered = true;
                    userAnsewers[item.DataItemIndex, 0] = item.DataItemIndex + 1;
                    userAnsewers[item.DataItemIndex, 1] = 4;
                    userAnsewers[item.DataItemIndex, 2] = Convert.ToInt32(questionID);
                    userAnsewers[item.DataItemIndex, 3] = Convert.ToInt32(groupID);
                    userAnsewers[item.DataItemIndex, 4] = Convert.ToInt32(correctAnswer);
                    if (correctAnswer == "4")
                    {
                        correctCount = correctCount + 1;
                    }
                }

                if (answered == false) {
                    unselectedCount += 1;
                }

            }

            // Validate for questions not answered
            if (unselectedCount != 0 && examTimeOut == false)  
            {
                popUpStartTime = DateTime.Now;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('You have not answered some of the questions. Please answer all the questions');", true);
  
                popUpActivated = true;

                return;
            }
            else { 
                if (correctCount != 0)
                {
                    totalMarks = (int)(((double)correctCount / (double)numberOfQuestions) * 100);

                    if (totalMarks >= passMarks)
                        status = "PASS";
                    else
                        status = "FAIL";
                }
                else
                    status = "FAIL";
                }

            // Write results data to Result Tale and UserExamAnswers Tables
            SqlConnection con = new SqlConnection(constr);
            con.Open();

            SqlCommand cmd = con.CreateCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = "DELETE Results ";
            cmd.ExecuteNonQuery();

            cmd.CommandText = "INSERT INTO [dbo].[Results] ([UserId], [ExamId], [DateTaken], [PassMarks], [TotalMarks], [Status], [DuratonTime]) VALUES (" + userID + ", '" + examID + "', '" + dateTaken.Date + "', '" + passMarks + "', '" + totalMarks + "', '" + status + "','" + examTime + "') ";
            cmd.ExecuteNonQuery();

            cmd.CommandText = "DELETE UserExamAnswers ";
            //cmd.CommandText = "DELETE UserExamAnswers WHERE UserId = " + userID + " AND DateTaken = '" + dateTaken.Date + "'";
            cmd.ExecuteNonQuery();

            for (int i = 0; i < userAnsewers.GetLength(0); i++)
            {
                indexCounter = i + 1;
                cmd.CommandText = "INSERT INTO [dbo].[UserExamAnswers] ([ExamId], [UserId], [GroupId], [QuestionId], [SelectedAnswer], [CorrectAnswer], [DateTaken])  VALUES (" + examID + "," + userID + ", " + userAnsewers[i, 3] + ", " + userAnsewers[i, 2] + ", " + userAnsewers[i, 1] + ", " + userAnsewers[i, 4] + ", '" + dateTaken.Date + "') ";

                cmd.ExecuteNonQuery();
            }

            con.Close();

            Response.Redirect("~/Results.aspx");

        }

        public void Timer1_Tick(object sender, EventArgs e)
        {
            if (baseTimeSpan != TimeSpan.Zero && baseTimeSpan > TimeSpan.Zero)
            {
                if (count == 1)
                {
                    changeTime = baseTimeSpan.Subtract(new TimeSpan(0, 0, 1));
                    TimerLabel.Text = changeTime.ToString();
                    count = 0;
                }
                else
                {
                    changeTime = changeTime.Subtract(new TimeSpan(0, 0, 1));
                    TimerLabel.Text = changeTime.ToString();
                    baseTimeSpan = changeTime;
                }
            }
            else {
                if (examTimeOut == false && popUpActivated == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('EXAM  TIME  OUT ...  !');", true);
                    examTimeOut = true;
                }
                else
                {
                    ButtonSubmit_Click(new object(), new EventArgs());
                }

            }
        }


    }
}