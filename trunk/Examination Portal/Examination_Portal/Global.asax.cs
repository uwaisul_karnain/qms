﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;


namespace Examination_Portal
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GoogleAuthManager.Instance.Configure(new GoogleAuthRequest()
            {

                //ClientId =
                //    "375220982546-p955eoc7egpjsf7dt8h1gmjmvfpstrdf.apps.googleusercontent.com",
                //Secret = "0UaPC5BoYSYHu4X6ICmF0_lr",
                //RedirectUrl = "https://examportal.go-attune.com/Default.aspx",

                ClientId =
                "812062822082-0vlbglp7htj8gic8r8np4omf0m8evr7i.apps.googleusercontent.com",
                Secret = "-VsmxcKMVW06AOiSyV-MmaCV",
                RedirectUrl = "http://localhost:60881/Default.aspx",

                //ClientId =
                //    "409340345776-nk9uobfvksms5t4b8pnamo8h0qdsv3g2.apps.googleusercontent.com",
                //Secret = "rVVJX245BAXLpRy8cwCWi_yr",
                //RedirectUrl = "https://examportal.go-attune.com",

                //ClientId =
                //    "138289671487-s46o9jddlq2ft5gl64gblqe9gu2r7nhe.apps.googleusercontent.com",
                //Secret = "HWBYJIJlQUAR_-PQSPf0jop1",
                //RedirectUrl = "https://examportal.go-attune.com/ExamPortal/Default.aspx",

                Scope = @"https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile"
            });

            //The log4net configuration
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Web.config")));
        }
    }
}
