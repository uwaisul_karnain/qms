﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Exams.aspx.cs" Inherits="Examination_Portal.Exams" %>

<asp:Content ID="examsPageContent" ContentPlaceHolderID="MainContentPageSection" runat="server">
        
    <form id="form1" runat="server" class="exams">

        <div style="padding-top: 10px; padding-bottom: 10px; float: left; width: 100%;">
            <h1>Exams Management</h1>
        </div>

        <%--<div class="tab-content" style="padding-top: 20px; padding-left: 140px; padding-bottom: 40px" >--%>
        <div style="padding-top: 10px; padding-bottom: 10px; float: left; width: 60%;">
            <asp:Table ID="Table1" BorderStyle="None" runat="server">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" width="300px"> 
                        <asp:Label ID="lblExamCode" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Exam Code : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtExamCode" runat="server" Width="300px" Height="20px" Enabled="false"></asp:TextBox>
                            <asp:Label ID="lblExamCodeStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                            <asp:Label ID="txtExamId" runat="server" Text="" Font-Size="10pt" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right" width="300px"> 
                        <asp:Label ID="lblExamName" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Exam Name : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtExamName" runat="server" Width="300px" Height="20px" Enabled="false"></asp:TextBox>
                            <asp:Label ID="lblExamNameStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="300px"> 
                        <asp:Label ID="lblDescription" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Description : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtDescription" runat="server" Width="300px" Height="60px" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="300px"> 
                        <asp:Label ID="lblQuestions" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Total No. of Questions : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:TextBox ID="txtTotalQuestions" runat="server" Width="100px" Height="20px" Enabled="false" CausesValidation="true" OnTextChanged="Questions_TextChanged" ToolTip="Number Value Only"></asp:TextBox>
                            <asp:Label ID="lblQuestionsStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="300px"> 
                        <asp:Label ID="lblPassMark" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Pass Mark Percentage : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                            <asp:TextBox ID="txtPassMark" runat="server" Width="100px" Height="20px" Enabled="false" CausesValidation="true" OnTextChanged="PassMark_TextChanged" ToolTip="Number Value Only"></asp:TextBox>
                            <asp:Label ID="lblPercentage" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="%" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                            <asp:Label ID="lblPassMarkStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Right"  width="300px"> 
                        <asp:Label ID="lblDuration" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text="Exam Time Duration : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px"  >
                            <asp:DropDownList ID="ddlTimeDuration" runat="server" Height="21px" Width="140px" >
                                <asp:ListItem Selected="True">00:15</asp:ListItem>
                                <asp:ListItem>00:30</asp:ListItem>
                                <asp:ListItem>00:45</asp:ListItem>
                                <asp:ListItem>01:00</asp:ListItem>
                                <asp:ListItem>01:15</asp:ListItem>
                                <asp:ListItem>01:30</asp:ListItem>
                                <asp:ListItem>01:45</asp:ListItem>
                                <asp:ListItem>02:00</asp:ListItem>
                            </asp:DropDownList>
<%--                            <asp:TextBox ID="txtDurationHours" runat="server" Width="50px" Height="20px" Enabled="false" CausesValidation="False" TextMode="Time"></asp:TextBox>
                            <asp:Label ID="lblText" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px" Text=" : " Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                            <asp:TextBox ID="txtDurationMinutes" runat="server" Width="50px" Height="20px" Enabled="false" CausesValidation="False" TextMode="Time"></asp:TextBox>--%>
                            <asp:Label ID="lblTime" runat="server" style="padding-top:20px; padding-right:20px; padding-bottom:20px; padding-left:20px" Text="[ HH : MM ]" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>
                            <asp:Label ID="lblDurationStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>

        <div style="padding-top: 10px; padding-bottom: 10px; float: right; width: 40%; height: 314px;">
            <asp:Table ID="Table2" BorderStyle="None" runat="server" width="90%">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" ColumnSpan="2">
                        <asp:Label ID="Label7" runat="server"  Text="Number of Questions for Groups" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>                        
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                            <asp:Label ID="Label3" runat="server"  Text="Question Area" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label> 
                     </asp:TableCell>
                     <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                        <asp:TextBox ID="txtQuestionArea" runat="server" Text="" Width="200px" Height="20px" Enabled="false" visible="true"></asp:TextBox>
                        <asp:Label ID="lblAreaStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                     </asp:TableCell>

                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                        <asp:Label ID="Label4" runat="server"  Text="No. of Questions for the Area" Font-Size="10pt" Font-Bold="true" ForeColor="Gray" Font-Names="Verdana"></asp:Label>                        
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left" style="padding-top:10px; padding-bottom:10px" >
                        <asp:TextBox ID="txtAreaQuestions" runat="server" Text="" Width="100px" Height="20px" Enabled="false" visible="true"></asp:TextBox>
                        <asp:Label ID="lblAreaQuestionsStar" runat="server" Text="*" Font-Size="8pt" Font-Names ="Verdana" ForeColor="Red" Visible="false"></asp:Label>
                        <asp:Button ID="ButtonAddGroup" Text="Add" runat="server"  OnClick="ButtonAddGroup_Click" style="padding-left:10px; padding-right:10px;"/>
                    </asp:TableCell>
                </asp:TableRow>

            </asp:Table>
            <asp:Panel ID="Panel3" runat="server" width="90%">
            <asp:SqlDataSource ID="SqlDataSourceGroups" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>"></asp:SqlDataSource>
            <asp:GridView ID="ExamAreaGroupsGrid" runat="server" CssClass="grid_style" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" width="90%"
                        PageSize="6" AllowPaging="True" AutoGenerateColumns="False"  HorizontalAlign="Center"  autopostback="true"
                        OnSelectedIndexChanged="ExamAreaGroupsGrid_SelectedIndexChanged" AllowSorting="True" OnPageIndexChanging="ExamAreaGroupsGrid_PageIndexChanging" ShowFooter="True" OnRowDeleting="ExamAreaGroupsGrid_RowDeleting" >

                    <Columns>
                    <%--<asp:BoundField DataField="RowNumber" HeaderText="Row Number" />--%>

                    <asp:BoundField HeaderText="Area" DataField="GroupName"></asp:BoundField>
                    <asp:BoundField HeaderText="No. of Questions" DataField="QuestionCount"></asp:BoundField> 
                    <asp:BoundField HeaderText="AreaId" DataField="GroupId"></asp:BoundField>
                    <%--<asp:BoundField CommandName="Remove" HeaderText="" Text="Remove" Visible="false"> </asp:BoundField>--%>
                    <asp:CommandField ShowDeleteButton="true" Visible="false" />
                    </Columns>  

                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                <FooterStyle BackColor="#FBB040" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#F58220" Font-Bold="True" ForeColor="White"></HeaderStyle>
                <PagerStyle BackColor="#FBB040" ForeColor="#333333" HorizontalAlign="Center"></PagerStyle>
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" ></RowStyle>
                <SelectedRowStyle BackColor="#FFd18b" Font-Bold="True" ForeColor="Navy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False"></SelectedRowStyle>
                <SortedAscendingCellStyle BackColor="#FBFBF2"></SortedAscendingCellStyle>
                <SortedAscendingHeaderStyle BackColor="#848384"></SortedAscendingHeaderStyle>
                <SortedDescendingCellStyle BackColor="#EAEAD3"></SortedDescendingCellStyle>
                <SortedDescendingHeaderStyle BackColor="#575357"></SortedDescendingHeaderStyle>

            </asp:GridView>
             </asp:Panel>

<%--            <asp:Panel ID="Panel2" runat="server" Visible ="false">
                <asp:SqlDataSource ID="SqlDataSource_ExamGroups" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>"></asp:SqlDataSource>

                <asp:DataGrid ID="ExamGroupsGrid" runat="server" PageSize="5" AllowPaging="True" DataKeyField="GroupId" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" HorizontalAlign="Center" OnLoad="Page_Load" AutoGenerateSelectButton="True"  
                        OnSelectedIndexChanged="ExamGroupsGrid_SelectedIndexChanged" OnPageIndexChanged="ExamGroupsGrid_PageIndexChanged" AllowSorting="True" >

                <Columns>
                    <asp:BoundColumn HeaderText="AreaId" DataField="GroupId" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Area" DataField="GroupName"  ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="No. of Questions" DataField="QuestionCount"  ></asp:BoundColumn> 
                    <asp:ButtonColumn CommandName="Remove" HeaderText="" Text="Remove" Visible="false"> </asp:ButtonColumn>
                </Columns>

                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <SelectedItemStyle BackColor="#FFd18b" Font-Bold="True" ForeColor="Navy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />
                <PagerStyle BackColor="#FBB040" ForeColor="#333333" HorizontalAlign="Center" Mode="NumericPages" />
                <AlternatingItemStyle BackColor="White" />
                <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />

                <HeaderStyle BackColor="#F58220" Font-Bold="True" ForeColor="White" />
                </asp:DataGrid>
            </asp:Panel>--%>

        </div>

        <div style="padding-top:20px; padding-left:106px; padding-bottom:20px; float: left; width: 100%;">
               <table style="border-style: None; border-color: inherit; border-width: medium; top: 0px;">
                    <tbody>
                        <tr>
                            <td style="width: 150px"><div style="height: 40px; width: 140px;"> <asp:Button ID="ButtonAdd" class="btn btn-default areaButton " runat="server" Text="New" OnClick="ButtonAdd_Click" Width="80px" Height="40px"  /></div></td>
                            <td style="width: 209px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonEdit" class="btn btn-default areaButton" runat="server" Text="Edit" OnClick="ButtonEdit_Click" Width="80px" Height="40px" /></div></td>
                            <td style="width: 150px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonDelete" class="btn btn-default areaButton" runat="server" Text="Delete" OnClick="ButtonDelete_Click" Width="80px" Height="40px" data-toggle="modal" data-target="#myModal" /></div></td>
                            <td style="width: 209px"><div style="height: 40px; width: 140px;"><asp:Button ID="ButtonCancel" class="btn btn-default areaButton" runat="server" Text="Cancel" OnClick="ButtonCancel_Click" Width="80px" Height="40px" Visible="false" /></div></td>
                            <td style="width: 200%"></td>
                        </tr>
                    </tbody>
               </table>      
        </div>

        <div class="tab-content" style="padding-top: 50px; padding-left: 100px; padding-bottom: 40px; padding-right:50px; float: left; width: 90%;" >
            <asp:Panel ID="Panel1" runat="server">
                <asp:SqlDataSource ID="SqlDataSource_Exams" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT [ExamId], [Code], [Name], [Description], [PassMarkPercentage], (DatePART(HOUR, [Duration])) AS TimeHours, (DATEPART(MINUTE, [Duration])) AS TimeMinutes, [NumberOfQuestions], [Duration] FROM [Exams] ORDER BY [Name]"></asp:SqlDataSource>

                <asp:DataGrid ID="ExamsDetailGrid" runat="server" AllowPaging="True" DataKeyField="ExamId" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" DataSourceID="SqlDataSource_Exams" HorizontalAlign="Center" OnLoad="Page_Load" AutoGenerateSelectButton="True" AutoPostBack="True"
                     OnSelectedIndexChanged="ExamsDetailGrid_SelectedIndexChanged" OnPageIndexChanged="ExamsDetailGrid_PageIndexChanged" AllowSorting="True" >
                    <%--OnPageIndexChanged="Grid_PageIndexChanged" OnCancelCommand="Grid_CancelCommand" OnDeleteCommand="Grid_DeleteCommand" OnEditCommand="Grid_EditCommand" OnUpdateCommand="Grid_UpdateCommand">--%>

                <Columns>
                    <asp:ButtonColumn CommandName="Select" HeaderText="Select" Text="Select" >
                    </asp:ButtonColumn>

                    <asp:BoundColumn HeaderText="Exam Id" DataField="ExamId" Visible="true"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Code" DataField="Code"  ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Name" DataField="Name"  ></asp:BoundColumn>                    
                    <asp:BoundColumn HeaderText="Description" DataField="Description" ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="No. of Questions" DataField="NumberOfQuestions"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Pass Mark" DataField="PassMarkPercentage"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Exam Duration" DataField="Duration"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Time Hours" DataField="TimeHours" Visible="false"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Time Minutes" DataField="TimeMinutes" Visible="false"></asp:BoundColumn>
                </Columns>

                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />

                <SelectedItemStyle BackColor="#FFd18b" Font-Bold="True" ForeColor="Navy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />

                <PagerStyle BackColor="#FBB040" ForeColor="#333333" HorizontalAlign="Center" Mode="NumericPages" />

                <AlternatingItemStyle BackColor="White" />

                <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />

                <%--<HeaderStyle BackColor="#D70000" Font-Bold="True" ForeColor="White" />--%>
                <HeaderStyle BackColor="#F58220" Font-Bold="True" ForeColor="White" />
                </asp:DataGrid>
            </asp:Panel>
        </div>

<%--<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch demo modal
</button>--%>



    </form>


</asp:Content>
