﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Examination_Portal.App_Start
{
    public class UserProfle
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Email { get; set; }
        public string ProfileImageUrl { get; set; }        
    }
}