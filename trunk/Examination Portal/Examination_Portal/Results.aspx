﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Results.aspx.cs" Inherits="Examination_Portal.Results" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>



<asp:Content ID="resultsContent" ContentPlaceHolderID="MainContentPageSection" runat="server">

    <form id="form1" runat="server" class="result">

       <%-- <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT Top 1 [UserId],[ExamId],[DateTaken],[PassMarks],[TotalMarks],[Status] FROM Results"></asp:SqlDataSource>--%>
        
        <h1>Your Results for the aIR 2.0 Examination</h1>

        <div>           

            <asp:Chart ID="Chart1" runat="server" Width="766px" Height="110px" ToolTip="Examination Results Sheet" BorderlineColor="Gray" style="padding-top: 50pt" visible ="false">
                <Series>

                    <asp:Series Name="Score" IsXValueIndexed ="true" IsValueShownAsLabel="true" ChartType="Bar" Legend="LegendScore">
                        <Points>
				            <asp:DataPoint AxisLabel ="Required Score" YValues="60" Color ="251, 176, 64" LegendText="Required Score"/>
                            <asp:DataPoint AxisLabel="Your Score" YValues="66" Color ="Highlight" LegendText="Your Score"/>
			            </Points>
                    </asp:Series>

                </Series>

<%--                <Legends>
                        <asp:Legend Name="Require Score" Docking="Bottom" Title="Examination Result out of 100" TableStyle="Wide" BorderDashStyle="Solid" BorderColor="#e8eaf1" TitleSeparator="Line" TitleFont="Verdana" TitleSeparatorColor="#e8eaf1" Alignment="Center">

                        </asp:Legend>
                </Legends>--%>

                <ChartAreas>
		            <asp:ChartArea Name="ChartArea1" BorderDashStyle="Solid" >
		                <AxisX>
                            <MajorGrid LineDashStyle="NotSet" />
                        </AxisX>
                        <AxisX2>
                            <MajorGrid LineDashStyle="NotSet" />
                        </AxisX2>
		            </asp:ChartArea>
                </ChartAreas>

                <Titles>
                    <asp:Title Name="Examination Results">
                    </asp:Title>
                </Titles>

            </asp:Chart>

        </div>

        <div class="tab-content" style="padding-top: 80px; padding-left: 140px; padding-bottom: 40px">
            <p><asp:Label ID="Greeting" runat="server" Font-Size="XX-Large" Font-Names ="Verdana"></asp:Label></p><br /><br />
            <p><asp:Label ID="Detail" runat="server" Font-Bold ="true" Font-Size="12pt" Font-Names ="Verdana"></asp:Label>
            </p><br /><br />
            <p><asp:Label ID="Label1" runat="server" Text="Expected Score : " Font-Bold="true" Font-Size="12pt" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                <asp:Label ID="ExpectedScore" runat="server" Font-Bold="true" Font-Size="12pt" Font-Names ="Verdana"></asp:Label>
            </p><br />
            <p><asp:Label ID="Label3" runat="server" Text="Your Score : " Font-Bold="true" Font-Size="12pt" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                <asp:Label ID="CandidateScore" runat="server" Font-Bold="true" Font-Size="12pt" Font-Names ="Verdana"></asp:Label>
            </p><br />
            <p><asp:Label ID="Status" runat="server" Text="Your Results Status : " Font-Bold="true" Font-Size="12pt" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                <asp:Label ID="Result" runat="server" Font-Bold="true" Font-Size="X-Large" Font-Names ="Verdana"></asp:Label>
            </p><br />
        </div>


        <div class="tab-content" style="padding-top: 50px; padding-left: 100px; padding-bottom: 40px; padding-right:50px; float: left; width: 90%;" >

            <asp:Panel ID="Panel1" runat="server">

                <asp:SqlDataSource ID="SqlDataSource_GroupPerformance" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommandType="StoredProcedure" SelectCommand="SP_ExamQuestionsGroupPerformance">
                <SelectParameters>
                    <asp:Parameter Name="examId" Type="Int32" DefaultValue="1"/>
                    <asp:Parameter Name="userId" Type="Int32" DefaultValue="1"/>
                    <asp:Parameter Name="dateTaken" Type="DateTime" DefaultValue="02-03-2016"/>
                </SelectParameters>
                </asp:SqlDataSource>

                <asp:DataGrid ID="ExamsResultsGrid" runat="server" AllowPaging="True" DataKeyField="ExamId" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" DataSourceID="SqlDataSource_GroupPerformance" HorizontalAlign="Center" OnLoad="Page_Load" AutoPostBack="True"
                     OnSelectedIndexChanged="ExamsResultsGrid_SelectedIndexChanged" OnPageIndexChanged="ExamsResultsGrid_PageIndexChanged" AllowSorting="True" SelectedItemStyle-Wrap="True">
                    <%--OnPageIndexChanged="Grid_PageIndexChanged" OnCancelCommand="Grid_CancelCommand" OnDeleteCommand="Grid_DeleteCommand" OnEditCommand="Grid_EditCommand" OnUpdateCommand="Grid_UpdateCommand">--%>

                <Columns>
                    <asp:ButtonColumn CommandName="Select" HeaderText="Select" Text="Select" visible ="false">
                    </asp:ButtonColumn>

                    <asp:BoundColumn HeaderText="Group Id" DataField="GroupId" Visible="false" ReadOnly="True"></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="Group Name" DataField="GroupName"  ></asp:BoundColumn>
                    <asp:BoundColumn HeaderText="No. of Questions" DataField="QuestionCount"  ></asp:BoundColumn>                    
                    <asp:BoundColumn HeaderText="Correct Answers" DataField="CorrectCount" ></asp:BoundColumn>
                    <asp:TemplateColumn HeaderText="Your Performance" ItemStyle-Width="100">
                        <ItemTemplate>
                            <asp:Label Text='<%# GetPerformance(Convert.ToInt32(Eval("QuestionCount")),Convert.ToInt32(Eval("CorrectCount")))%>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>

                    <%--<asp:BoundColumn HeaderText="Your Performance" DataField='<%# GetPerformance(Eval("QuestionCount"),Eval("CorrectCount"))%>' ></asp:BoundColumn>--%>

                </Columns>

                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />

                <%--<SelectedItemStyle BackColor="#FFd18b" Font-Bold="True" ForeColor="Navy" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" />--%>

                <%--<SelectedItemStyle  ForeColor="#333333"  />--%>


                <PagerStyle BackColor="#FBB040" ForeColor="#333333" HorizontalAlign="Center" Mode="NumericPages" />

                <AlternatingItemStyle BackColor="White" />

                <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />

                <%--<HeaderStyle BackColor="#D70000" Font-Bold="True" ForeColor="White" />--%>
                <HeaderStyle BackColor="#F58220" Font-Bold="True" ForeColor="White" />
                </asp:DataGrid>

            </asp:Panel>
        </div>

    </form>

</asp:Content>
