﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Examination_Portal.Models;

namespace Examination_Portal
{
    public class MasterPageBase : MasterPage
    {

        //Logging
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //Get User details to public variables
        public void FormSecurity(string loggedInUserName)
        {
            Session["userId"] = 0;
            Session["userName"] = "";
            Session["adminUser"] = false;
            Session["securityLevel"] = 0;


            string query = "SELECT DISTINCT TOP 1 [UserId], [UserName], [AdminUser], [SecurityLevel] FROM Users WHERE UserName = LTRIM(RTRIM('" + loggedInUserName + "'))";
            DataTable dt = GetData(query);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (!row.IsNull("UserId")) 
                        Session["userId"] = Convert.ToInt32(row["UserId"]);

                    if (!row.IsNull("UserName"))
                        Session["userName"] = (row["UserName"]).ToString();


                    if (!row.IsNull("AdminUser"))
                        Session["adminUser"] = (bool)(row["AdminUser"]);

                    if (!row.IsNull("SecurityLevel"))
                        Session["securityLevel"] = (int)(row["SecurityLevel"]);     
                    
                }

            }
        }

        //SQL Server connection and table databinding
        private static DataTable GetData(string query)
        {
            DataTable dt = new DataTable();
            string constr = ConfigurationManager.ConnectionStrings["ExamPortal"].ConnectionString;
            using (SqlConnection connection = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = connection;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                    }
                }
                return dt;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                logger.Error("Master Page Base - Page Load");
                if (!GoogleAuthManager.Instance.IsAuthenticated(Context))
                    GoogleAuthManager.Instance.ValidateRequest(Context);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw ex;
            }
        }
    }
}

