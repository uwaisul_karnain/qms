﻿CREATE TABLE [dbo].[Exams]
(
	[ExamId] INT NOT NULL PRIMARY KEY, 
    [Code] NCHAR(10) NOT NULL, 
    [Name] NVARCHAR(50) NULL, 
    [Description] NVARCHAR(200) NULL
)
