﻿using Examination_Portal.App_Start;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Examination_Portal.Models;

namespace Examination_Portal
{
    public partial class SiteMaster : MasterPageBase
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        string loggedUserName;

        public int message = 0;
        
        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;

            base.OnLoad(e);

            if (SessionProvider.LoggedUser != null)
            {
                this.spanUserName.InnerHtml = SessionProvider.LoggedUser.Name;
                this.imgProfile.Src = SessionProvider.LoggedUser.ProfileImageUrl;
                loggedUserName = SessionProvider.LoggedUser.Email;

                Session["userName"] = (string)(SessionProvider.LoggedUser.Email);
                //Session["userName"] = "Dimuthu.Chathuranga@attuneconsulting.com";
                //loggedUserName = "Dimuthu.Chathuranga@attuneconsulting.com";

            }
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }


        }

        protected override void OnLoad(EventArgs e)
        {
            //base.OnLoad(e);

            //if (SessionProvider.LoggedUser != null)
            //{
            //    this.spanUserName.InnerHtml = SessionProvider.LoggedUser.Name;                
            //    this.imgProfile.Src = SessionProvider.LoggedUser.ProfileImageUrl;
            //    loggedUserName = SessionProvider.LoggedUser.Email;

            //    Session["userName"] = (string)(SessionProvider.LoggedUser.Email);
            //    //Session["userName"] = "Dimuthu.Chathuranga@attuneconsulting.com";
            //    //loggedUserName = "Dimuthu.Chathuranga@attuneconsulting.com";

            //}

            //loggedUserName = "Dimuthu.Chathuranga@attuneconsulting.com";

            FormSecurity(loggedUserName);


            if ((bool)Session["adminUser"] == false)
            {
                this.adminMenu.Visible = false;
                this.otherInfoMenu.Visible = false;
            }
            else
            {
                this.adminMenu.Visible = true;
                this.otherInfoMenu.Visible = true;
            }
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut();


        }

        protected void ButtonDefault_Click(object sender, EventArgs e)
        {
            //Button Default (YES) Click
            message = 1;
        }
        protected void ButtonPrimary_Click(object sender, EventArgs e)
        {
            //Button Primary (NO) Click
            message = 2;
        }
    }

}