﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Results.aspx.cs" Inherits="Examination_Portal.Results" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>



<asp:Content ID="resultsContent" ContentPlaceHolderID="MainContentPageSection" runat="server">

    <form id="form1" runat="server">

       <%-- <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT Top 1 [UserId],[ExamId],[DateTaken],[PassMarks],[TotalMarks],[Status] FROM Results"></asp:SqlDataSource>--%>
        
        <div>
            <h1>Your Results for the aIR 2.0 Examination</h1>

            <asp:Chart ID="Chart1" runat="server" Width="800px" Height="200px" ToolTip="Examination Results Sheet" BorderlineColor="Gray" style="padding-top: 50pt">
                <Series>
<%--                    <asp:Series Name="RequiredScore" IsValueShownAsLabel="true" ChartType="Bar" LegendText="Required Score" Color ="251, 176, 64">
                        <Points>
				            <asp:DataPoint AxisLabel="Required Score" YValues="60" XValue="Required Score"/>
			            </Points>
                    </asp:Series>
                    <asp:Series Name="YourScore" IsValueShownAsLabel="true" ChartType="Bar" LegendText="Your Score">
                        <Points>
				            <asp:DataPoint AxisLabel="Your Score" YValues="66" XValue="Your Score"/>
			            </Points>
                    </asp:Series>--%>

                    <asp:Series Name="Score" IsXValueIndexed ="true" IsValueShownAsLabel="true" ChartType="Bar" Legend="LegendScore">
                        <Points>
				            <asp:DataPoint AxisLabel ="Required Score" YValues="60" Color ="251, 176, 64" LegendText="Required Score"/>
                            <asp:DataPoint AxisLabel="Your Score" YValues="66" Color ="Highlight" LegendText="Your Score"/>
			            </Points>
                    </asp:Series>

                </Series>

<%--                <Legends>
                        <asp:Legend Name="Require Score" Docking="Bottom" Title="Examination Result out of 100" TableStyle="Wide" BorderDashStyle="Solid" BorderColor="#e8eaf1" TitleSeparator="Line" TitleFont="Verdana" TitleSeparatorColor="#e8eaf1" Alignment="Center">

                        </asp:Legend>
                </Legends>--%>

                <ChartAreas>
		            <asp:ChartArea Name="ChartArea1" BorderDashStyle="Solid" >
		                <AxisX>
                            <MajorGrid LineDashStyle="NotSet" />
                        </AxisX>
                        <AxisX2>
                            <MajorGrid LineDashStyle="NotSet" />
                        </AxisX2>
		            </asp:ChartArea>
                </ChartAreas>

                <Titles>
                    <asp:Title Name="Examination Results">
                    </asp:Title>
                </Titles>

            </asp:Chart>

        </div>

        <div class="tab-content" style="padding-top: 40px; padding-left: 140px; padding-bottom: 40px">
            <p><asp:Label ID="Greeting" runat="server" Font-Size="XX-Large" Font-Names ="Verdana"></asp:Label><br />
               <asp:Label ID="Detail" runat="server" Font-Bold ="true" Font-Size="12pt" Font-Names ="Verdana"></asp:Label>
            </p><br /><br />
            <p><asp:Label ID="Label1" runat="server" Text="Expected Score : " Font-Bold="true" Font-Size="12pt" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                <asp:Label ID="ExpectedScore" runat="server" Font-Bold="true" Font-Size="12pt" Font-Names ="Verdana"></asp:Label>
            </p><br />
            <p><asp:Label ID="Label3" runat="server" Text="Your Score : " Font-Bold="true" Font-Size="12pt" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                <asp:Label ID="CandidateScore" runat="server" Font-Bold="true" Font-Size="12pt" Font-Names ="Verdana"></asp:Label>
            </p><br />
            <p><asp:Label ID="Status" runat="server" Text="Your Results Status : " Font-Bold="true" Font-Size="12pt" ForeColor ="Gray" Font-Names ="Verdana"></asp:Label>
                <asp:Label ID="Result" runat="server" Font-Bold="true" Font-Size="X-Large" Font-Names ="Verdana"></asp:Label>
            </p><br />
        </div>

    </form>
</asp:Content>
