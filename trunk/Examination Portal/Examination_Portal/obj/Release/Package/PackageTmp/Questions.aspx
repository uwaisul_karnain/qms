﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Questions.aspx.cs" Inherits="Examination_Portal.FullScree" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Examination Portal</title>
    <link rel="icon" href="Images/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap -->
    <link href="Css/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="Css/style_Old.css" rel="stylesheet">
    <link href="Css/global.style.css" rel="stylesheet">
    <link href="Css/fonticons.css" rel="stylesheet">
    <link href="Css/grid.animation.css" rel="stylesheet">
    <link href="Css/slidebars.min.css" rel="stylesheet">
    z
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    
    <script src="Scripts/modernizr.js" type="text/javascript"></script>
</head>


<body>
    <form id="form1" runat="server" >
<%--        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT Top 10 [ExamId], [QuestionId], [Question], [Answer1], [Answer2], [Answer3], [Answer4], [CorrectAnswer], [Marks] FROM [ExamQuestions] ORDER BY NEWID()"></asp:SqlDataSource>--%>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommandType="StoredProcedure" SelectCommand="SP_ExamQuestions">
        <SelectParameters>
            <asp:Parameter Name="examId" Type="Int32" DefaultValue="1"/>
        </SelectParameters>
        </asp:SqlDataSource>

        <div class="Qheader">

            <span class="txt">aIR 2.0 Examination</span>

           
            <div class="col-xs-6 col-md-4" style="float:right;">
                <div class="areaQuestion" style="padding-top:10px; padding-bottom:10px">
                    <asp:ScriptManager ID="ScriptManager" runat="server">
                    </asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel" runat="server" Width="75%">
                            </asp:Panel>
                            <asp:Label ID="LabelText" runat="server" Text="Time Remainng :  " Font-Size="Large" ForeColor="#919191" Font-Names="Verdana" style="padding-bottom:10px"></asp:Label>
                            <asp:Label ID="TimerLabel" runat="server" Font-Size ="X-Large" ForeColor ="#919191" Font-Names="Verdana" BorderColor="#B1B1B1" BorderStyle="Solid" BorderWidth="1px"></asp:Label><br />
                            
                            <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                            </asp:Timer>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

        <div class="Qlist_area">
            <asp:ListView runat="server" ID="ListViewQuestions" DataSourceID="SqlDataSource1" >
            <ItemTemplate>
            <tr runat="server">

              <td runat="server">
                    
                    <%--<strong>ExamId:</strong>--%><asp:Label ID="examId" runat="server" Text='<%# Eval("ExamId") %>' Visible="false"></asp:Label>
                    
                    <div class="areaQuestion" style="padding: 60px 0px 0px 80px;" >
                                       
                        <asp:Label ID="questionNumber" GroupName='<%# Eval("QuestionId") %>' Text = '<%# (((ListViewDataItem)Container).DisplayIndex + 1) + " .  " %>' runat="server" Font-Bold ="true"></asp:Label>
                        <asp:Label ID="question" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Question") %>' Font-Bold ="false"></asp:Label><br />

                        <div class="areaQuestionIn" style="padding: 20px 0px 20px 20px;" >
                            <div class="col-xs-6 col-md-6">
                                  <asp:RadioButton ID="Answer1" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer1") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton><br />
                                  <asp:RadioButton ID="Answer2" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer2") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton><br />                                 
                            </div>
                            <div class="col-xs-6 col-md-6">
                                    <asp:RadioButton ID="Answer3" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer3") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton><br />
                                    <asp:RadioButton ID="Answer4" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer4") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton>
                                    <%--<strong>Question:</strong>--%><asp:Label ID="questionID" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("QuestionId") %>' Visible ="false"></asp:Label>
                                    <%--<strong>Correct Answer:</strong>--%><asp:Label ID="CorrectAnswer" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("CorrectAnswer") %>' Visible ="false" ForeColor="SandyBrown"></asp:Label><br />                                                   
                            </div>
                        </div>
                    </div>
              </td>
   
            </tr>
          </ItemTemplate>
        </asp:ListView>

            <div class="tab-content" style="padding-top: 100px; padding-left: 200px; padding-bottom: 100px">
                <asp:Button ID="ButtonSubmit" runat="server"  Text="Submit" OnClick="ButtonSubmit_Click" />
            </div>

        </div>

        
        

    </form>
</body>
</html>
