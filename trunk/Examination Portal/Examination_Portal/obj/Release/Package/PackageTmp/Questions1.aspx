﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Questions1.aspx.cs" Inherits="Examination_Portal.Questions" %>


<asp:Content ID="questionsContent" ContentPlaceHolderID="MainContentPageSection" runat="server">
    <form id="form1" runat="server">
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT Top 10 [ExamId], [QuestionId], [Question], [Answer1], [Answer2], [Answer3], [Answer4], [CorrectAnswer], [Marks] FROM [ExamQuestions] ORDER BY NEWID()"></asp:SqlDataSource>

        <div style="padding-top: 40px; padding-left: 40px; padding-bottom: 40px">

            <h1>aIR 2.0 Examination</h1>
            <div class="col-xs-6 col-md-6">
            </div>
            <div class="col-xs-6 col-md-6">
                <div class="areaQuestion" style="padding-top:20px; padding-bottom:50px">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="Panel1" runat="server" Width="75%">
                            </asp:Panel>
                            <asp:Label ID="Label1" runat="server" Text="Time Remainng :  " Font-Size="Large" ForeColor="#919191" Font-Names="Verdana" style="padding-bottom:10px"></asp:Label>
                            <asp:Label ID="TimerLabel" runat="server" Font-Size ="X-Large" ForeColor ="#919191" Font-Names="Verdana" BorderColor="#B1B1B1" BorderStyle="Solid" BorderWidth="1px"></asp:Label><br />
                            
                            <asp:Timer ID="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                            </asp:Timer>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <asp:ListView runat="server" ID="ListViewQuestions" DataSourceID="SqlDataSource1" >
            <ItemTemplate>
            <tr runat="server">

              <td runat="server">
                    
                    <%--<strong>ExamId:</strong>--%><asp:Label ID="examId" runat="server" Text='<%# Eval("ExamId") %>' Visible="false"></asp:Label>
                    <br />
                    <br />
                    <div class="areaQuestion" style="padding: 60px 0px 0px 20px;" >
                                       
                        <asp:Label ID="questionNumber" GroupName='<%# Eval("QuestionId") %>' Text = '<%# (((ListViewDataItem)Container).DisplayIndex + 1) + " .  " %>' runat="server" Font-Bold ="true"></asp:Label>
                        <asp:Label ID="question" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Question") %>' Font-Bold ="false"></asp:Label><br />

                        <div class="areaQuestionIn" style="padding: 20px 0px 0px 20px;" >
                            <div class="col-xs-6 col-md-6">
                                  <asp:RadioButton ID="Answer1" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer1") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton><br />
                                  <asp:RadioButton ID="Answer2" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer2") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton><br />                                 
                            </div>
                            <div class="col-xs-6 col-md-6">
                                    <asp:RadioButton ID="Answer3" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer3") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton><br />
                                    <asp:RadioButton ID="Answer4" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("Answer4") %>' CssClass="Space" Font-Bold="false" Font-Size="10pt" Font-Names="Verdana"></asp:RadioButton>
                                      <%--<strong>Question:</strong>--%><asp:Label ID="questionID" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("QuestionId") %>' Visible ="false"></asp:Label>
                                    <%--<strong>Correct Answer:</strong>--%><asp:Label ID="CorrectAnswer" GroupName='<%# Eval("QuestionId") %>' runat="server" Text='<%# Eval("CorrectAnswer") %>' Visible ="false" ForeColor="SandyBrown"></asp:Label><br />                                                   
                            </div>
                        </div>
                    </div>
              </td>
   
            </tr>
          </ItemTemplate>
        </asp:ListView>
        </div>


        <div class="tab-content" style="padding-top: 40px; padding-left: 40px; padding-bottom: 40px">

            <asp:Button ID="ButtonSubmit" runat="server"  Text="Submit" OnClick="ButtonSubmit_Click" />

        </div>
        

    </form>
</asp:Content>
