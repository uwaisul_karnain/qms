﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Examination_Portal._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContentPageSection" runat="server">

    <form id="form1" runat="server">
<%--    <div class="jumbotron" >
        <p class="lead"></p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-large">Learn more &raquo;</a></p>
        
    </div>--%>

        <div style="padding-top: 40px; padding-bottom: 100px">
                <h1>Examination Portal</h1>
        </div>
    
        <div class="areaQuestion">            
            <h2>Examinations Pending </h2>

            <p style="padding-bottom: 40pt">
                <asp:Label ID="Instruction" runat="server" Font-Size="10pt" Font-Names ="Verdana" Text="Please select the Examination to continue :   " ></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSourceExams" DataTextField="Name" DataValueField="Code" width ="200px"></asp:DropDownList>
                <asp:EntityDataSource ID="EntityDataSource1" runat="server">
                </asp:EntityDataSource>
                <asp:SqlDataSource ID="SqlDataSourceExams" runat="server" ConnectionString="<%$ ConnectionStrings:ExamPortal %>" SelectCommand="SELECT DISTINCT Exams.[ExamId], [Code], [Name] FROM Exams INNER JOIN UserExams ON Exams.ExamId = UserExams.ExamId INNER JOIN Users ON UserExams.UserId = Users.UserId WHERE Users.UserName = @userid AND UserExams.IsActive = 1 AND UserExams.FinalDate >= (SELECT CONVERT(DATE, GETDATE()))"></asp:SqlDataSource>
            </p>           

        </div>
        <%--<a class="btn btn-default" href="Questions.aspx">Get Started &raquo;</a>--%>

        <%--<asp:Button ID="Start" runat="server" Text="Get Started &raquo;" OnClick="Start_Click" />--%>
   
        <%--<asp:Button ID="Start" runat="server" Text="Get Started &raquo;" OnClick="Start_Click" />--%>

        <button onclick="examStartFunction()" >Get Started &raquo;</button>

        <script>
            function examStartFunction()
            {
                var examWindow = window.open("Questions.aspx", "", "fullscreen:yes; resizable: no; titlebar: no; toolbars: no; menubar = no; scrollbars=yes; controllers:false ");
            }

        </script>


    </form>

</asp:Content>
