﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;


namespace Examination_Portal
{
    public partial class Users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { 
            UserDetailGrid.SelectedIndex = 0;
            UserDetailGrid_SelectedIndexChanged(null,null);
            }
        }

        public void UserDetailGrid_SelectedIndexChanged(Object sender, EventArgs e)
        {
            UserDetailGrid.SelectedIndex = UserDetailGrid.SelectedItem.ItemIndex;
            lblUserId.Text = UserDetailGrid.SelectedItem.Cells[1].Text;
            txtUserLoginID.Text = UserDetailGrid.SelectedItem.Cells[2].Text;
            txtEPF.Text  = UserDetailGrid.SelectedItem.Cells[3].Text;
            if( UserDetailGrid.SelectedItem.Cells[4].Text == "Admin")
                ddListUserType.SelectedIndex = 1;
            else
                ddListUserType.SelectedIndex = 0;

            //switch ((UserDetailGrid.SelectedItem.Cells[5].Text).ToString())
            //{
            //    case "Level 1":
            //        ddListSecurityLevel.SelectedIndex = 0;
            //        break;
            //    case "Level 2":
            //        ddListSecurityLevel.SelectedIndex = 1;
            //        break;
            //    case "Level 3":
            //        ddListSecurityLevel.SelectedIndex = 2;
            //        break;
            //    case "":
            //        ddListSecurityLevel.Text = "";
            //        break;
            //    default:
            //        ddListSecurityLevel.SelectedIndex = 0;
            //        break;
            //}

  
            for(int i=0; i<DDListUserCategory.Items.Count; i++)
            {
                if (UserDetailGrid.SelectedItem.Cells[6].Text.ToString() == DDListUserCategory.Items[i].Value.ToString())
                    DDListUserCategory.SelectedIndex = i;
            }

            //selectedIndex = (Convert.ToInt16) (UserDetailGrid.SelectedItem.Cells[0].Text);


        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (ButtonAdd.Text == "New"){
                //Reset screen
                ButtonAdd.Text = "Submit";
                ButtonEdit.Visible = false;
                ButtonDelete.Visible = false;
                ButtonCancel.Visible = true;
                ClearControls();
                EnableDisableControls(true);
            }
            else
            {
                if (DataValidated() == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Incomplete data. Please enter compulsary data marked as *');", true);
                    return;
                }
                //Insert Record
                AddUser();
                ButtonAdd.Text = "New";
                ButtonEdit.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = false;
                EnableDisableControls(false);
                ClearControls();
                EnableDisableControls(false);
            }

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (ButtonEdit.Text == "Edit")
            {
                if (lblUserId.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Please select a record to Edit');", true);
                    return;
                }
                ButtonEdit.Text = "Update";
                ButtonAdd.Visible = false;
                ButtonDelete.Visible = false;
                ButtonCancel.Visible = true;
                EnableDisableControls(true);
            }
            else
            {
                if (DataValidated() == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Incomplete data. Please enter compulsary data marked as *');", true);
                    return;
                }
                UpdateUser();
                ButtonEdit.Text = "Edit";
                ButtonAdd.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = false;
                ClearControls();
                EnableDisableControls(false);
            }

        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (lblUserId.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Please select a record to Delete.');", true);
                return;
            }
            else
            {
                DeleteUser();
                ClearControls();
                EnableDisableControls(false);
            }
                
        }

        protected void EnableDisableControls(bool enable)
        {
            if (enable == true) {
                txtUserLoginID.Enabled = true;
                txtEPF.Enabled = true;
                DDListUserCategory.Enabled = true;
                ddListUserType.Enabled = true;
                //ddListSecurityLevel.Enabled = true;
                UserDetailGrid.Enabled = false;
            }
            else {
                txtUserLoginID.Enabled = false;
                txtEPF.Enabled = false;
                DDListUserCategory.Enabled = false;
                ddListUserType.Enabled = false;
                //ddListSecurityLevel.Enabled = false;
                UserDetailGrid.Enabled = true;
            }

        }

        protected void ClearControls()
       {
           lblUserId.Text = "";
            txtUserLoginID.Text = "";
            txtEPF.Text = "";
            DDListUserCategory.SelectedIndex = 0;
            ddListUserType.SelectedIndex = 0;
            //ddListSecurityLevel.SelectedIndex = 0;
       }

        protected void AddUser()
       {
           string insertSQL = "INSERT INTO [dbo].Users ([UserName], [Epf], [AdminUser], [SecurityLevel], [CategoryId]) VALUES (LTRIM(RTRIM('" + txtUserLoginID.Text + "')), " + Convert.ToInt32((txtEPF.Text).ToString()) + ", '" + AdminUser() + "', " + SecurityLevel() + "," + DDListUserCategory.SelectedValue + ")";

           SqlDataSource_Users.InsertCommand = insertSQL;
           SqlDataSource_Users.Insert();
           SqlDataSource_Users.DataBind();
           UserDetailGrid.DataBind();
       }

        protected void UpdateUser()
          {
              if (lblUserId.Text != "")
              {
                  string updateSQL = "UPDATE [dbo].USERS SET [UserName] = LTRIM(RTRIM('" + txtUserLoginID.Text + "')) , [Epf] = " + Convert.ToInt32((txtEPF.Text).ToString()) + ", [AdminUser] = '" + AdminUser() + "', [SecurityLevel] = " + SecurityLevel() + ", [CategoryId] = " + DDListUserCategory.SelectedValue + " WHERE UserId = " + Convert.ToInt32((lblUserId.Text).ToString()) + "";

                  SqlDataSource_Users.UpdateCommand = updateSQL;
                  SqlDataSource_Users.Update();
                  UserDetailGrid.DataBind();
              }
          }

        protected void DeleteUser()
        {
            if (lblUserId.Text != "")
            {
                string deleteSQL = "DELETE [dbo].[Users] WHERE UserId = " + Convert.ToInt32((lblUserId.Text).ToString()) + "";

                SqlDataSource_Users.DeleteCommand = deleteSQL;
                SqlDataSource_Users.Delete();
                SqlDataSource_Users.DataBind();
                UserDetailGrid.DataBind();
            }
        }

        public bool AdminUser()
        {
         bool adminUser;
         if (ddListUserType.Text != "")
          {
              switch((ddListUserType.Text).ToString()){
                case "Admin":
                    adminUser = true;
                    break;
                case "User":
                    adminUser = false;
                    break;
                case "":
                    adminUser = false;
                    break;
                default:
                    adminUser = false;
                    break;            
              }
            }
        else
             adminUser = false;

        return adminUser;
        }

        public int SecurityLevel()
        {
         int securityLevel;
         if (lblUserId.Text != "")
          {
            switch((ddListUserType.Text).ToString()){
                case "Level 1":
                      securityLevel = 1;
                    break;
                case "Level 2":
                    securityLevel = 2;
                    break;
                case "Level 3":
                    securityLevel = 3;
                    break;
                case "":
                    securityLevel = 0;
                    break;
                default:
                    securityLevel = 0;
                    break;
              }
         }
        else
             securityLevel = 0;

        return securityLevel;
        }

        public bool DataValidated()
        {
            bool dataValidated = true;
            if (txtUserLoginID.Text.Trim() == "")
            {
                dataValidated = false;
                lblUIDStar.Visible = true;
            }
            else
                lblUIDStar.Visible = false;

            if (txtEPF.Text.Trim() == "")
            {
                dataValidated = false;
                lblEPFNo.Visible = true;
            }
            else 
                lblEPFNo.Visible = false;

            if (ddListUserType.Text.Trim() == "")
            {
                dataValidated = false;
                lblUserCategory.Visible = true;
            }
            else
                lblUserCategory.Visible = false;

            return dataValidated;
        }

        protected void UserDetailGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            UserDetailGrid.CurrentPageIndex = e.NewPageIndex;
            UserDetailGrid.DataBind();
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            ButtonAdd.Text = "New";
            ButtonAdd.Visible = true;
            ButtonEdit.Text = "Edit";
            ButtonEdit.Visible = true;
            ButtonDelete.Visible = true;
            ButtonCancel.Visible = false;
            EnableDisableControls(false);
            ClearControls();
            EnableDisableControls(false);
        }
    }
}