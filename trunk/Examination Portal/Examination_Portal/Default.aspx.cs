﻿using Examination_Portal.App_Start;
using Examination_Portal.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Examination_Portal
{
    public partial class _Default : BasePage
    {
        //string spanUserName = "";
        //string spanUserId = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //spanUserName = "Dimuthu.Chathuranga@attuneconsulting.com";

            SqlDataSourceExams.SelectParameters["userName"].DefaultValue = (string)(Session["userName"]);

        }

        protected void On_Click(object sender, EventArgs e)
        {
            if (UserExamList.Items.Count > 0)
            { 
            SqlDataSourceExams.Update();

            Session["examId"] = Convert.ToInt32(UserExamList.SelectedValue);
            Session["examName"] = Convert.ToString(UserExamList.SelectedItem.Text);

            //String x = "<script type='text/javascript'>window.opener.location.href='Default.aspx';self.close();</script>";
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", x, false);

            ScriptManager.RegisterStartupScript(this, GetType(), "examStartFunction", "examStartFunction();", true);

            //ScriptManager.RegisterStartupScript(this, GetType(), "closeWindowFunction", "closeWindowFunction();", true); 

            }
            
        }

        protected void UserExamList_PreRender(object sender, EventArgs e)
        {
            if (UserExamList.Items.Count == 0)
            {
                MsgLabel.Visible = true;
                Start.Visible = false;
            }
            else
            {
                MsgLabel.Visible = false;
                Start.Visible = true;
            }
        }

    }
}