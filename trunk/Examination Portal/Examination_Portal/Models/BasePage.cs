﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Web;
using System;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Examination_Portal.Models;
using System.Web.UI;


namespace Examination_Portal.Models
{
    public class BasePage : Page
    {

        #region base property
        public int userId { get; set; }
        public string userName { get; set; }

        public bool adminUser { get; set; }
        public int securityLevel { get; set; }

        //public int examId { get; set; }

        #endregion


    }



}