﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Examination_Portal
{
    public partial class ExamQuestions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                if (ExamList.Items.Count > 0)
                {
                    SqlDataSourceQuestionGroups.SelectCommand = "SELECT DISTINCT [GroupName], QG.[GroupId] AS GroupId, [ExamId] FROM [dbo].[ExamQuestionGroups] QG WHERE [ExamId] = " + ExamList.SelectedValue + " ORDER BY [GroupName]";

                    SqlDataSourceQuestionGroups.DataBind();
                    QuestionGroupList.DataBind();

                    SqlDataSource_Questions.SelectCommand = "SELECT [QuestionId], Q.[ExamId], [Name], Q.[GroupId], [GroupName], [Question], [Answer1], [Answer2], [Answer3], [Answer4], [CorrectAnswer], [Marks] FROM [dbo].[ExamQuestions] Q LEFT OUTER JOIN [dbo].[Exams] E ON Q.[ExamId] = E.[ExamId] LEFT OUTER JOIN [dbo].[ExamQuestionGroups] G ON Q.[GroupId] = G.[GroupId] WHERE Q.[ExamId] = " + ExamList.SelectedValue + "   ORDER BY Q.[ExamId], Q.[GroupId], QuestionId ";
                    SqlDataSource_Questions.DataBind();
                    QuestionsGrid.DataBind();
                }
            if (!IsPostBack)
            {

            }
        }

        protected void ExamList_PreRender(object sender, EventArgs e)
        {
            if (ExamList.Items.Count > 0)
            {
                ExamList.SelectedIndex = 0;
                //SqlDataSourceQuestionGroups.SelectCommand = "SELECT DISTINCT QG.[GroupId] AS GroupId, [ExamId], [GroupCode], [GroupName] FROM [dbo].[QuestionGroups] QG INNER JOIN [dbo].[ExamGroupQuestionCount]  EGQ ON QG.GroupId = EGQ.GroupId WHERE [ExamId] = " + ExamList.SelectedValue + " ORDER BY [GroupCode], [GroupName]";
                SqlDataSourceQuestionGroups.SelectCommand = "SELECT DISTINCT  [GroupId], [GroupName], [QuestionCount] FROM [dbo].[ExamQuestionGroups] WHERE [ExamId] =  " + ExamList.SelectedValue + " ORDER BY [GroupName]";

                SqlDataSourceQuestionGroups.DataBind();
                QuestionGroupList.DataBind();

                if (ExamList.Items.Count > 0 )
                {
                    SqlDataSource_Questions.SelectCommand = "SELECT [QuestionId], Q.[ExamId], [Name], Q.[GroupId], [GroupName], [Question], [Answer1], [Answer2], [Answer3], [Answer4], [CorrectAnswer], [Marks] FROM [dbo].[ExamQuestions] Q LEFT OUTER JOIN [dbo].[Exams] E ON Q.[ExamId] = E.[ExamId] LEFT OUTER JOIN [dbo].[ExamQuestionGroups] G ON Q.[GroupId] = G.GroupId  WHERE Q.[ExamId] = " + ExamList.SelectedValue + "   ORDER BY Q.[ExamId], [GroupName], QuestionId ";
                    SqlDataSource_Questions.DataBind();
                    QuestionsGrid.DataBind();
                }
            }
        }

        protected void ExamList_SelectedIndexChanged(object sender, EventArgs e)
        {

            SqlDataSourceQuestionGroups.SelectCommand = "SELECT DISTINCT QG.[GroupId] AS GroupId, [ExamId], [GroupName] FROM [dbo].[ExamQuestionGroups] QG WHERE [ExamId] = " + ExamList.SelectedValue + " ORDER BY [GroupName]";

            SqlDataSourceQuestionGroups.DataBind();
            QuestionGroupList.DataBind();

        }
   
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (ButtonAdd.Text == "New")
            {
                //Reset screen
                ButtonAdd.Text = "Submit";
                ButtonEdit.Visible = false;
                ButtonDelete.Visible = false;
                ButtonCancel.Visible = true;
                ClearControls();
                EnableDisableControls(true);
            }
            else
            {
                if (DataValidated() == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Incomplete data. Please enter compulsary data marked as *');", true);
                    return;
                }
                //Insert Record
                AddQuestion();
                ButtonAdd.Text = "New";
                ButtonEdit.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = false;
                EnableDisableControls(false);
                ClearControls();
                EnableDisableControls(false);
            }

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (ButtonEdit.Text == "Edit")
            {
                if (ExamList.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Please select a record to Edit');", true);
                    return;
                }
                ButtonEdit.Text = "Update";
                ButtonAdd.Visible = false;
                ButtonDelete.Visible = false;
                ButtonCancel.Visible = true;
                EnableDisableControls(true);
            }
            else
            {
                if (DataValidated() == false)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Incomplete data. Please enter compulsary data marked as *');", true);
                    return;
                }
                UpdateQuestion();
                ButtonEdit.Text = "Edit";
                ButtonAdd.Visible = true;
                ButtonDelete.Visible = true;
                ButtonCancel.Visible = false;
                ClearControls();
                EnableDisableControls(false);
            }

        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (txtQuestionId.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Messagebox", "alert('Please select a record to Delete.');", true);
                return;
            }
            else
            {
                DeleteQuestion();
                ClearControls();
                EnableDisableControls(false);
            }
        }

        protected void ButtonCancel_Click(object sender, EventArgs e)
        {
            ButtonAdd.Text = "New";
            ButtonAdd.Visible = true;
            ButtonEdit.Text = "Edit";
            ButtonEdit.Visible = true;
            ButtonDelete.Visible = true;
            ButtonCancel.Visible = false;
            EnableDisableControls(false);
            ClearControls();
            EnableDisableControls(false);
        }

        protected void EnableDisableControls(bool enable)
        {
            if (enable == true)
            {
                ExamList.Enabled = true;
                QuestionGroupList.Enabled = true;
                txtQuestion.Enabled = true;
                txtAnswer1.Enabled = true;
                txtAnswer2.Enabled = true;
                txtAnswer3.Enabled = true;
                txtAnswer4.Enabled = true;
                CorrectAnswerList.Enabled = true;
                QuestionsGrid.Enabled = false;
            }
            else
            {
                ExamList.Enabled = false;
                QuestionGroupList.Enabled = false;
                txtQuestion.Enabled = false;
                txtAnswer1.Enabled = false;
                txtAnswer2.Enabled = false;
                txtAnswer3.Enabled = false;
                txtAnswer4.Enabled = false;
                CorrectAnswerList.Enabled = false;
                QuestionsGrid.Enabled = true;
            }

        }

        protected void ClearControls()
        {
            if (ExamList.Items.Count > 0)
                ExamList.SelectedIndex = 0;
            if (QuestionGroupList.Items.Count > 0)
                QuestionGroupList.SelectedIndex = 0;
            txtQuestionId.Text = "";
            txtQuestion.Text = "";
            txtAnswer1.Text = "";
            txtAnswer2.Text = "";
            txtAnswer3.Text = "";
            txtAnswer4.Text = "";
            CorrectAnswerList.SelectedIndex = 0;
        }

        protected bool DataValidated()
        {
            bool dataValidated = true;

            if (ExamList.Items.Count > 0 && ExamList.Text == "")
            {
                dataValidated = false;
                lblExamStar.Visible = true;
            }
            else
                lblExamStar.Visible = false;

            if (QuestionGroupList.Items.Count > 0 && QuestionGroupList.Text == "")
            {
                dataValidated = false;
                lblQuestionGroupStar.Visible = true;
            }
            else
                lblQuestionGroupStar.Visible = false;

            if ( txtQuestion.Text.Trim() == "")
            {
                dataValidated = false;
                lblQuestionStar.Visible = true;
            }
            else
                lblQuestionStar.Visible = false;

            if (txtAnswer1.Text.Trim() == "")
            {
                dataValidated = false;
                lblStar1.Visible = true;
            }
            else
                lblStar1.Visible = false;

            if (txtAnswer2.Text.Trim() == "")
            {
                dataValidated = false;
                lblStar2.Visible = true;
            }
            else
                lblStar2.Visible = false;

            if (txtAnswer3.Text.Trim() == "")
            {
                dataValidated = false;
                lblStar3.Visible = true;
            }
            else
                lblStar3.Visible = false;

            if (txtAnswer4.Text.Trim() == "")
            {
                dataValidated = false;
                lblStar4.Visible = true;
            }
            else
                lblStar4.Visible = false;

            if (CorrectAnswerList.Items.Count > 0 && CorrectAnswerList.Text == "")
            {
                dataValidated = false;
                lblCorrectAnswerStar.Visible = true;
            }
            else
                lblCorrectAnswerStar.Visible = false;

            return dataValidated;
        }

        protected void AddQuestion()
        {
            string insertSQL = "INSERT INTO [dbo].[ExamQuestions] ([ExamId], [GroupId], [Question], [Answer1], [Answer2], [Answer3], [Answer4], [CorrectAnswer], [Marks]) VALUES (" + ExamList.SelectedValue + "," + QuestionGroupList.SelectedValue + ",LTRIM(RTRIM('" + txtQuestion.Text.ToString() + "')),LTRIM(RTRIM('" + txtAnswer1.Text.ToString() + "')),LTRIM(RTRIM('" + txtAnswer2.Text.ToString() + "')),LTRIM(RTRIM('" + txtAnswer3.Text.ToString() + "')),LTRIM(RTRIM('" + txtAnswer4.Text.ToString() + "'))," + (CorrectAnswerList.SelectedValue) + "," + 0 + ")";

            SqlDataSource_Questions.InsertCommand = insertSQL;
            SqlDataSource_Questions.Insert();
            SqlDataSource_Questions.DataBind();
            QuestionsGrid.DataBind();
        }

        protected void UpdateQuestion()
        {
            if (txtQuestionId.Text != "")
            {
                //string updateSQL = "UPDATE [dbo].[Exams]  SET [Code] = LTRIM(RTRIM('" + txtExamCode.Text + "')), [Name] = LTRIM(RTRIM('" + txtExamName.Text + "')), [Description] = LTRIM(RTRIM('" + txtDescription.Text + "')), [NumberOfQuestions] = " + Convert.ToInt32((txtQuestions.Text).ToString()) + ", [PassMarkPercentage] = " + Convert.ToInt32((txtPassMark.Text).ToString()) + ", [Duration] = '" + ExamDuration() + "' WHERE [Exams].[ExamId] = " + Convert.ToInt32((txtExamId.Text).ToString()) + "";
                string updateSQL = "UPDATE [dbo].[ExamQuestions] SET [ExamId] = " + Convert.ToInt32(ExamList.SelectedValue.ToString()) + ", [GroupId] = " + Convert.ToInt32(QuestionGroupList.SelectedValue.ToString()) + ", [Question] = LTRIM(RTRIM('" + txtQuestion.Text.ToString() + "')), [Answer1] = LTRIM(RTRIM('" + txtAnswer1.Text.ToString() + "')), [Answer2] = LTRIM(RTRIM('" + txtAnswer2.Text.ToString() + "')), [Answer3] = LTRIM(RTRIM('" + txtAnswer3.Text.ToString() + "')), [Answer4] = LTRIM(RTRIM('" + txtAnswer4.Text.ToString() + "')), [CorrectAnswer] = " + (CorrectAnswerList.SelectedIndex + 1) + ", [Marks] = 0  WHERE [QuestionId] = " + Convert.ToInt32((txtQuestionId.Text).ToString()) + "";

                SqlDataSource_Questions.UpdateCommand = updateSQL;
                SqlDataSource_Questions.Update();
                QuestionsGrid.DataBind();
            }
        }

        protected void DeleteQuestion()
        {
            if (txtQuestionId.Text != "")
            {
                string deleteSQL = "DELETE [dbo].[ExamQuestions] WHERE QuestionId = " + Convert.ToInt32((txtQuestionId.Text).ToString()) + "";

                SqlDataSource_Questions.DeleteCommand = deleteSQL;
                SqlDataSource_Questions.Delete();
                SqlDataSource_Questions.DataBind();
                QuestionsGrid.DataBind();
            }
        }

        protected void QuestionsGrid_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            QuestionsGrid.CurrentPageIndex = e.NewPageIndex;
            QuestionsGrid.DataBind();
        }

        protected void QuestionsGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            QuestionsGrid.SelectedIndex = QuestionsGrid.SelectedItem.ItemIndex;
            if (QuestionsGrid.SelectedItem.Cells[1].Text != null)
                txtQuestionId.Text = QuestionsGrid.SelectedItem.Cells[1].Text;
            else
                txtQuestionId.Text = "";
            if (QuestionsGrid.SelectedItem.Cells[2].Text != null)
            {
                foreach (ListItem li in QuestionGroupList.Items)
                {
                    if (li.Value == QuestionsGrid.SelectedItem.Cells[2].Text)
                    {
                        QuestionGroupList.SelectedIndex = (Convert.ToInt32(li.Value));
                    }
                }
            }                

            if (QuestionsGrid.SelectedItem.Cells[4].Text != null)
                txtQuestion.Text = QuestionsGrid.SelectedItem.Cells[4].Text;
            else
                txtQuestion.Text = "";
            if (QuestionsGrid.SelectedItem.Cells[5].Text != null)
                txtAnswer1.Text = QuestionsGrid.SelectedItem.Cells[5].Text;
            else
                txtAnswer1.Text = "";
            if (QuestionsGrid.SelectedItem.Cells[6].Text != null)
                txtAnswer2.Text = QuestionsGrid.SelectedItem.Cells[6].Text;
            else
                txtAnswer2.Text = "";
            if (QuestionsGrid.SelectedItem.Cells[7].Text != null)
                txtAnswer3.Text = QuestionsGrid.SelectedItem.Cells[7].Text;
            else
                txtAnswer3.Text = "";
            if (QuestionsGrid.SelectedItem.Cells[8].Text != null)
                txtAnswer4.Text = QuestionsGrid.SelectedItem.Cells[8].Text;
            else
                txtAnswer4.Text = "";
            if (QuestionsGrid.SelectedItem.Cells[9].Text != null)
            {
                foreach (ListItem li in QuestionGroupList.Items)
                {
                    if (li.Value == QuestionsGrid.SelectedItem.Cells[9].Text)
                    {
                        CorrectAnswerList.SelectedIndex = ((Convert.ToInt32(li.Value)) -1);
                    }
                }
            }   


        }

    }
}