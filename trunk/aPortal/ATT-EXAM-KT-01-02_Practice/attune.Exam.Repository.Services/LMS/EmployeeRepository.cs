﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Exam.Domain.Entities;
using attune.Exam.Repository.Interfaces;
using attune.Exam.Repository.Services.LMS.EmployeeService;

namespace attune.Exam.Repository.Services.LMS
{
    public class EmployeeRepository:IEmployeeRepository
    {
        #region Public Variabels

        public EmployeeServiceClient EmployeeService = new EmployeeServiceClient();

        #endregion

        #region IEmployeeRepository Members

        /// <summary>
        /// Get Divisions
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetDivisions(string divisionCode)
        {
            return EmployeeService.GetDivsions(divisionCode);
        }

        /// <summary>
        /// Get Sections
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetSections(string divisionCode)
        {
            return EmployeeService.GetSections(divisionCode);
        }

        /// <summary>
        /// Get Designations
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> GetDesignations()
        {
            return EmployeeService.GetDesignations();
        }

        /// <summary>
        /// Get Employee Detail
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <param name="sectionCode"></param>
        /// <param name="designationCode"></param>
        /// <returns></returns>
        public List<Employee> GetEmployeeDetail(string divisionCode, string sectionCode, string designationCode)
        {
            var result = EmployeeService.GetEmployeeDetail(divisionCode, sectionCode, designationCode).ToList();

            return result.Select(item => new Employee
                                         {
                                             FirstName = item.FirstName, LastName = item.LastName, Email = item.Email, EmpNo = item.EmpNo
                                         }).ToList();
        }

        /// <summary>
        /// Get Non EmployeeDetail
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<Employee> GetNonEmployeeDetail(DateTime startDate, DateTime endDate)
        {
            var emp = new Employee();
            var lstEmployee = new List<Employee>();

            //TODO After implement Recruitment System
            //var result = EmployeeService.GetEmployeeDetail(startDate, endDate).ToList();
            //foreach (var item in result)
            //{
            //    emp.FirstName = item.FirstName;
            //    emp.LastName = emp.LastName;
            //    emp.Email = item.Email;
            //    emp.EmpNo = item.EmpNo;
            //    lstEmployee.Add(emp);
            //}

            var emp1 = new Employee();
            var emp2 = new Employee();
            var emp3 = new Employee();

            emp1 = new Employee
                   {
                       EmpNo = 100,
                       Email = "user1@gmail.com",
                       FirstName = "fuser1",
                       LastName = "luser1"
                   };
            emp2 = new Employee()
                {
                    EmpNo = 101,
                    Email = "user2@gmail.com",
                    FirstName = "fuser2",
                    LastName = "luser2"
                };
            emp3 = new Employee()
            {
                EmpNo = 102,
                Email = "user3@gmail.com",
                FirstName = "fuser3",
                LastName = "luser3"
            };  
            
            lstEmployee.Add(emp1);
            lstEmployee.Add(emp2);
            lstEmployee.Add(emp3);
            return lstEmployee;
        }

        #endregion
    }
}
