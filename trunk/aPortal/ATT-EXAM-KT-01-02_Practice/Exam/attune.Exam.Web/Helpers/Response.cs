﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using attune.Core.Web.Mvc.Controllers;
using attune.Exam.Infrastructure.Resources;

namespace attune.Exam.Web.Helpers
{
    public class Response<T>
    {
        private bool _isSuccess;
        public ControllerBase.MessageType MessageType { get; set; }

        public string Message { get; set; }

        public bool IsSuccess
        {
            get { return _isSuccess; }
            set
            {
                _isSuccess = value;
                this.Message = value
                    ? Messages.CommonRecordSaveSuccess
                    : Messages.CommonRecordSaveFailed;
                this.MessageType = value ? ControllerBase.MessageType.Success : ControllerBase.MessageType.Error;
            }
        }

        public T Owner { get; set; }
    }
}