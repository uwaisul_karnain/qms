﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

namespace attune.Exam.Web.Helpers
{
    public sealed class Constants
    {
        

        /// <summary>
        /// The model state key errors create
        /// </summary>
        public const string MODEL_STATE_KEY_ERRORS_CREATE = "CreateException";
        /// <summary>
        /// The model state key errors update
        /// </summary>
        public const string MODEL_STATE_KEY_ERRORS_UPDATE = "UpdateError";

        /// <summary>
        /// The view data key ExamType
        /// </summary>
        public const string VIEW_DATA_KEY_EXAMTYPES = "ExamTypes";

        /// <summary>
        /// The view data key Difficult Level
        /// </summary>
        public const string VIEW_DATA_KEY_DIFFICULTLEVELS = "DifficultLevels";

        public const string VIEW_DATA_KEY_DEFAULT_LEVEL_ID = "defaultLevelId";

        /// <summary>
        /// The view data key Categories
        /// </summary>
        public const string VIEW_DATA_KEY_CATEGORIES = "Categories";

        public const string VIEW_DATA_KEY_DEFAULT_CATEGORY_ID = "defaultCategoryId";

        public const string EXCEL_NAME = "Template_QuestionUpload.xlsm";

        public const string EXCEL_PATH = "~/ExcelTemplateFile/";

        public const string EXCEL_TEMP_PATH = "~/MyFiles";

        public const int UPLOAD_MAX_LIMIT = 101;

        public const int UPLOAD_MANDATORY_COLUMN_COUNT = 6;

        public const int UPLOAD_ISPUBLISHED_COLUMN = 5;

        public const int UPLOAD_ANSWER_COLUMN_START = 7;

        public const int UPLOAD_ANSWER_COLUMN_END = 16;

        public const int UPLOAD_CORRECT_ANSWER_COLUMN_START = 17;

        public const int UPLOAD_CORRECT_ANSWER_COLUMN_END = 26;

        public const string UPLOAD_TEMPLATE_HEADER_EXAMTYPE = "ExamType";
        public const string UPLOAD_TEMPLATE_HEADER_CATEGORYTYPE = "CategoryType";
        public const string UPLOAD_TEMPLATE_HEADER_QUESTIONTYPE = "QuestionType";
        public const string UPLOAD_TEMPLATE_HEADER_DIFFICULTLEVEL = "DifficultLevel";
        public const string UPLOAD_TEMPLATE_HEADER_ISPUBLISHED = "IsPublished";
        public const string UPLOAD_TEMPLATE_HEADER_QUESTION = "Question";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER1 = "Answer1";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER2 = "Answer2";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER3 = "Answer3";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER4 = "Answer4";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER5 = "Answer5";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER6 = "Answer6";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER7 = "Answer7";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER8 = "Answer8";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER9 = "Answer9";
        public const string UPLOAD_TEMPLATE_HEADER_ANSWER10 = "Answer10";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT1 = "Correct1";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT2 = "Correct2";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT3 = "Correct3";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT4 = "Correct4";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT5 = "Correct5";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT6 = "Correct6";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT7 = "Correct7";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT8 = "Correct8";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT9 = "Correct9";
        public const string UPLOAD_TEMPLATE_HEADER_CORRECT10 = "Correct10";
        
    }
}