/****************************************************************************************
 * Copyright � attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace attune.Exam.Web.Helpers
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class MultipleSelectedIndexAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string _field, _defaultText;

        public MultipleSelectedIndexAttribute(string defaultValue, string errorMessage, string defaultText = "Multiple")
            : base(errorMessage)
        {
            _field = defaultValue;
            _defaultText = defaultText;
        }

        #region IClientValidatable Members

        /// <summary>
        /// </summary>
        /// <param name="metadata"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata,
            ControllerContext context)
        {
            var errorMessage = ErrorMessageString;
            var rule = new ModelClientValidationRule
                       {
                           ErrorMessage = errorMessage,
                           ValidationType = "multipleitemselected"
                       };
            rule.ValidationParameters.Add("field", _field);
            rule.ValidationParameters.Add("multitext", _defaultText);

            yield return rule;
        }

        #endregion

        /// <summary>
        ///     Format the error message filling in the name of the property to validate and the reference one.
        /// </summary>
        /// <param name="name">The name of the property to validate</param>
        /// <returns>The formatted error message</returns>
        public override string FormatErrorMessage(string name)
        {
            return string.Format(ErrorMessageString, name, _field);
        }
    }
}