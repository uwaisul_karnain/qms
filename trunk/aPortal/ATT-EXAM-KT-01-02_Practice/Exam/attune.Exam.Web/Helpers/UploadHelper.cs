﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using attune.Exam.Infrastructure.Resources;
using OfficeOpenXml;
using ControllerBase = attune.Core.Web.Mvc.Controllers.ControllerBase;

namespace attune.Exam.Web.Helpers
{
    public static class TemplateExtentions
    {
        public static void ConvertToBool(this DataTable dataTable, int columnIndex, string comparedTo ="1")
        {
            if (dataTable != null)
                foreach (DataRow row in dataTable.Rows)
                {
                    if (row[columnIndex].ToString() == comparedTo)
                        row[columnIndex] = true;
                    else
                        row[columnIndex] = false;
                }
        }
    }

    public class UploadHelper
    {
        private static string[] ArrHeaders = new string[]
                                             {
                                                 Constants.UPLOAD_TEMPLATE_HEADER_EXAMTYPE,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CATEGORYTYPE,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_QUESTIONTYPE,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_DIFFICULTLEVEL,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ISPUBLISHED,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_QUESTION,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER1,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER2,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER3,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER4,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER5,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER6,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER7,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER8,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER9,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_ANSWER10,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT1,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT2,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT3,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT4,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT5,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT6,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT7,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT8,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT9,
                                                 Constants.UPLOAD_TEMPLATE_HEADER_CORRECT10
                                             };

        /// <summary>
        /// Convert Excel to Data table
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="hasHeaderRow"></param>
        /// <returns></returns>
        public static DataTable ToDataTable(ExcelWorksheet ws, bool hasHeaderRow = true)
        {
            var tbl = new DataTable();
            foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                tbl.Columns.Add(hasHeaderRow
                    ? firstRowCell.Text
                    : string.Format("Column {0}", firstRowCell.Start.Column));
            var startRow = hasHeaderRow ? 2 : 1;
            for (var rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
            {
                //var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                var wsRow = ws.Cells[rowNum, 1, rowNum, tbl.Columns.Count];
                var row = tbl.NewRow();
                //foreach (var cell in wsRow) row[cell.Start.Column - 1] = cell.Text;
                //tbl.Rows.Add(row);
                if (wsRow.Count() != 0)
                {
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                    tbl.Rows.Add(row);
                }
            }
            return tbl;
        }

        /// <summary>
        /// Convert DataTable to List of objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <returns></returns>


        public static UploadResult ParsePostedFile(HttpPostedFileBase file,out ExcelWorksheet worksheet)
        {
            var message = string.Empty;
            UploadResult result = null;
            worksheet = null;
            if (file != null)
            {
                if (Path.GetExtension(file.FileName) == ".xlsx" || Path.GetExtension(file.FileName) == ".xls" ||
                    Path.GetExtension(file.FileName) == ".xlsm")
                {
                    using (var mem = new MemoryStream())
                    {
                        mem.SetLength(file.ContentLength);
                        file.InputStream.Read(mem.GetBuffer(), 0, file.ContentLength);

                        using (var pck = new ExcelPackage())
                        {
                            pck.Load(mem);
                            var sheetCount = pck.Workbook.Worksheets.Count;
                            if (sheetCount > 0)
                            {
                                var ws = pck.Workbook.Worksheets[1];
                                if (ValidateTemplateHeader(ws, out result)
                                    && ValidateMaximumUploadLimit(ws, out result)
                                    && ValidateEmptyExcelCell(ws, out result)
                                    && ValidatePublishedMode(ws, out result))
                                {
                                    result = new UploadResult {ErrorType = UploadErrorType.None};
                                    worksheet = ws;
                                }

                                return result;
                            }
                        }
                    }
                }
            }
            return null;
        }

        private static bool ValidateMaximumUploadLimit(ExcelWorksheet ws, out UploadResult result,
            bool hasHeaderRow = true)
        {
            result = new UploadResult();
            if (ws.Dimension.End.Row >= Constants.UPLOAD_MAX_LIMIT)
            {
                result.ErrorType = UploadErrorType.MaximumlimitExceeded;
                result.Message = string.Format(Messages.MasterQuestionBankUploadLimitError);
                return false;
            }
            return true;
        }

        private static bool ValidateTemplateHeader(ExcelWorksheet ws, out UploadResult result, bool hasHeaderRow = true)
        {
            result = new UploadResult();
            var header = ws.Cells[1, 1, 1, ws.Dimension.End.Column];
            var arrHeaders = ArrHeaders;
            for (var x = 1; x <= header.Count(); x++)
            {
                if (ws.Cells[1, x].Text != arrHeaders[x - 1])
                {
                    result.Message = string.Format(Messages.MasterQuestionBankUploadTemplateWrong);
                    result.ErrorType = UploadErrorType.InvalidTemplate;
                    return false;
                }
            }
            return true;
        }

        private static bool ValidateEmptyExcelCell(ExcelWorksheet ws, out UploadResult result, bool hasHeaderRow = true)
        {
            result = new UploadResult();
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            var startRow = hasHeaderRow ? 2 : 1;
            var wsCol = ws.Cells[1, 1, 1, Constants.UPLOAD_MANDATORY_COLUMN_COUNT];
            for (var rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
            {
                for (var i = 1; i <= wsCol.Count(); i++)
                {
                    var cellText = ws.Cells[rowNum, i].Text;
                    if (cellText == string.Empty || cellText == "")
                    {
                        list.Add(new KeyValuePair<string, string>(ws.Cells[1, i].Text.ToString(), rowNum.ToString()));
                    }
                }
            }
            if (list.Count > 0)
            {
                var error = new StringBuilder(Messages.MasterQuestionBankUploadErrorOccured);
                error.Append("<br/>");
                foreach (var val in list)
                {
                    error.AppendLine();
                    error.AppendFormat(Messages.MasterQuestionBankUploadErrorInRow, val.Key, val.Value);
                    error.Append("<br/>");
                }
                result.Message = error.ToString();
                result.ErrorType = UploadErrorType.RequiredField;
                return false;
            }

            return true;
        }

        private static bool ValidatePublishedMode(ExcelWorksheet ws, out UploadResult result, bool hasHeaderRow = true)
        {
            var startRow = hasHeaderRow ? 2 : 1;
            result = new UploadResult();
            for (var rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
            {
                List<string> answers = new List<string>();
                List<string> correctAnswers = new List<string>();
                //col 5 - IsPublished 
                var wsIsPublishedCol = ws.Cells[rowNum, Constants.UPLOAD_ISPUBLISHED_COLUMN].Text;
                if (wsIsPublishedCol == "1")
                {
                    //col 7 - Answer col start
                    //col 16 - Answer col end
                    for (var i = Constants.UPLOAD_ANSWER_COLUMN_START; i <= Constants.UPLOAD_ANSWER_COLUMN_END; i++)
                    {
                        var cellText = ws.Cells[rowNum, i].Text;
                        answers.Add(cellText);
                    }
                    if (answers.Count(i => string.IsNullOrEmpty(i)) == 10)
                    {
                        result.Message = Messages.MasterQuestionBankUploadAnswerCannotBeNull;
                        result.ErrorType = UploadErrorType.PublishedMode;
                        return false;
                    }
                    //col 17 - CorrectAnswer col start
                    //col 26 - CorrectAnswer col end
                    for (var i = Constants.UPLOAD_CORRECT_ANSWER_COLUMN_START; i <= Constants.UPLOAD_CORRECT_ANSWER_COLUMN_END; i++)
                    {
                        var cellText = ws.Cells[rowNum, i].Text;
                        correctAnswers.Add(cellText);
                    }
                    if (correctAnswers.Count(i => string.IsNullOrEmpty(i)) == 10)
                    {
                        result.Message = Messages.MasterQuestionBankUploadAtleastOneAnswerShouldBeCorrect;
                        result.ErrorType = UploadErrorType.PublishedMode;
                        return false;
                    }
                }
            }
            return true;
        }
    }

    public class UploadResult
    {
        public UploadErrorType ErrorType { get; set; }
        public string Message { get; set; } 
    }
}