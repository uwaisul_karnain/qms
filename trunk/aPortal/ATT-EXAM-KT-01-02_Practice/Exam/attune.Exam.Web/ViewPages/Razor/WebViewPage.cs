﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.IO;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;
using attune.Core.Helpers;
using attune.Core.Web; 
using attune.Exam.Domain; 
using attune.Exam.Infrastructure.DI;
using attune.Exam.Infrastructure.Resources;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;

namespace attune.Exam.Web.ViewPages.Razor
{
    public abstract class WebViewPage<TModel> : System.Web.Mvc.WebViewPage<TModel>
    {
        #region Delegates

        public delegate LocalizedString Localizer(string text, params object[] args);

        public delegate bool View(PrivilegeType type, SystemModule systemModule);

        #endregion

        #region Private Variables

        private readonly ResourceManager _resourceManager = new ResourceManager(typeof (Labels));
        private readonly ResourceManager _resourceManagerMessages = new ResourceManager(typeof (Messages));
        private Localizer _localizer;
        private View _view;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Get a localized resources for lables
        /// </summary>
        public Localizer L
        {
            get
            {
                _localizer = (format, args) =>
                             {
                                 var resFormat = string.Empty;
                                 if (string.IsNullOrEmpty(resFormat))
                                     resFormat = _resourceManager.GetString(format);
                                 if (string.IsNullOrEmpty(resFormat))
                                     return new LocalizedString(format);
                                 return
                                     new LocalizedString((args == null || args.Length == 0)
                                         ? resFormat
                                         : string.Format(resFormat, args));
                             };

                return _localizer;
            }
        }

        /// <summary>
        ///     Get a localized resources for messages
        /// </summary>
        public Localizer M
        {
            get
            {
                _localizer = (format, args) =>
                             {
                                 var resFormat = string.Empty;
                                 if (string.IsNullOrEmpty(resFormat))
                                     resFormat = _resourceManagerMessages.GetString(format);
                                 if (string.IsNullOrEmpty(resFormat))
                                     return new LocalizedString(format);
                                 return
                                     new LocalizedString((args == null || args.Length == 0)
                                         ? resFormat
                                         : string.Format(resFormat, args));
                             };

                return _localizer;
            }
        }

        /// <summary>
        ///     Validate the view permision
        /// </summary>
        public View V
        {
            get
            {
                _view = delegate(PrivilegeType type, SystemModule module)
                        {
                            try
                            {
                                // TODO : Implement authorization logic
                                //var privileges = UnityConfigProvider.GetConfiguredContainer()
                                //    .Resolve<IPermissionManagementService>()
                                //    .FindPrivileges( NullHandler.NullHandlerForString(HttpContext.Current.User.Identity.GetUserName(), ""),
                                //                                            (int) module
                                //                                            );
                                //return (privileges.Contains((int) type));
                                return true;
                            }
                            catch
                            {
                                // Ignore logging : This requirement is already managed by AOP
                                return false;
                            }
                        };

                return _view;
            }
        }

        /// <summary>
        /// Gets or sets the path of a layout page.
        /// </summary>
        public override string Layout
        {
            get
            {
                var layout = base.Layout;
                if (string.IsNullOrEmpty(layout)) return layout;
                var filename = Path.GetFileNameWithoutExtension(layout);
                var viewResult =
                    ViewEngines.Engines.FindView(ViewContext.Controller.ControllerContext, filename,
                        "");

                if (viewResult.View is RazorView)
                    layout = (viewResult.View as RazorView).ViewPath;

                return layout;
            }
            set { base.Layout = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Renders the wrapped section.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="wrapperHtmlAttributes">The wrapper HTML attributes.</param>
        /// <returns></returns>
        public HelperResult RenderWrappedSection(string name, object wrapperHtmlAttributes)
        {
            Action<TextWriter> action = delegate(TextWriter tw)
                                        {
                                            var htmlAttributes =
                                                HtmlHelper.AnonymousObjectToHtmlAttributes(wrapperHtmlAttributes);
                                            var tagBuilder = new TagBuilder("div");
                                            tagBuilder.MergeAttributes(htmlAttributes);

                                            var section = RenderSection(name, false);
                                            if (section != null)
                                            {
                                                tw.Write(tagBuilder.ToString(TagRenderMode.StartTag));
                                                section.WriteTo(tw);
                                                tw.Write(tagBuilder.ToString(TagRenderMode.EndTag));
                                            }
                                        };
            return new HelperResult(action);
        }

        /// <summary>
        /// Renders the section.
        /// </summary>
        /// <param name="sectionName">Name of the section.</param>
        /// <param name="defaultContent">The default content.</param>
        /// <returns></returns>
        public HelperResult RenderSection(string sectionName, Func<object, HelperResult> defaultContent)
        {
            return IsSectionDefined(sectionName) ? RenderSection(sectionName) : defaultContent(new object());
        }

        #endregion
    }

    public abstract class WebViewPage : WebViewPage<dynamic>
    {
    }
}