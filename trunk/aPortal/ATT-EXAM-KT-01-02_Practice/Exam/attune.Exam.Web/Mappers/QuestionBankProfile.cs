﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using attune.Core.Helpers;
using attune.Exam.Web.Areas.Master.Models;
using AutoMapper;
using attune.Exam.Application.Dtos;
using attune.Exam.Web.Areas.Master.Models.Exam;
using attune.Exam.Web.Areas.Master.Models.QuestionBank;

namespace attune.Exam.Web.Mappers
{
    public class QuestionBankProfile : Profile
    {
        private readonly JavaScriptSerializer _serializer = new JavaScriptSerializer();

        /// <summary>
        ///     Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        ///     Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        protected override void Configure()
        {
            base.Configure();

            Mapper.CreateMap<CreateQuestionView, QuestionBank>()
                .ForMember(x => x.ExamType.Id, m => m.MapFrom(c => NullHandler.NullHandlerForInt(c.ddlExamType, 0)))
                .ForMember(x => x.Category.Id, m => m.MapFrom(c => c.CategoryId))
                .ForMember(x => x.DifficultLevel, m => m.MapFrom(c => c.DifficultLevelId))
                .ForMember(x => x.QuestionType, m => m.MapFrom(c => c.QuestionTypeId))
                .ForMember(x => x.CreatedOn, m => m.UseValue(DateTime.UtcNow))
                .ForMember(x => x.UpdatedOn, m => m.UseValue(DateTime.UtcNow))
                .ForMember(x => x.Answers, m => m.MapFrom(c => _serializer.Deserialize<List<Answer>>(c.AnswersString)))
                ;
            Mapper.CreateMap<QuestionBank, CreateQuestionView>();


            Mapper.CreateMap<QuestionCategoryView, QuestionCategory>();
            Mapper.CreateMap<QuestionCategory, QuestionCategoryView>();

            //Mapper.CreateMap<attune.Exam.Web.Areas.Master.Models.Exam.CategoryView, Category>();
            //Mapper.CreateMap<Category, attune.Exam.Web.Areas.Master.Models.Exam.CategoryView>();
        }
    }
}