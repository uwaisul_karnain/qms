using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace attune.Exam.Web.Extentions
{
    public static class DataTableExtentions
    {
        public static List<T> ToList<T>(this DataTable dataTable) where T : new()
        {
            try
            {
                List<T> list = new List<T>();
                if (dataTable != null)
                    foreach (var row in dataTable.AsEnumerable())
                    {
                        T obj = new T();
                        foreach (var prop in obj.GetType().GetProperties())
                        {
                            try
                            {
                                PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                                propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType),
                                    null);
                            }
                            catch
                            {
                                continue;
                            }
                        }

                        list.Add(obj);
                    }
                return list;
            }
            catch
            {
                return null;
            }
        }
    }
}