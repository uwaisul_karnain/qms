/****************************************************************************************
 * Copyright � attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using attune.Core.Helpers; 
using attune.Exam.Domain; 
using attune.Exam.Infrastructure.DI;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;

namespace attune.Exam.Web.Filters
{
    /// <summary>
    ///     Requests will get validated via authorization context
    ///     This attribute can be used method in all Controllers which require authorization context
    /// </summary>
    /// <seealso cref="System.Web.Mvc.AuthorizeAttribute" />
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class ControllerAuthorizeAttribute : AuthorizeAttribute
    {
        #region Public Properties

        public SystemModule SystemModule { get; private set; }
        public PrivilegeType PrivilegeType { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ControllerAuthorizeAttribute"/> class.
        /// </summary>
        /// <param name="systemModule">The system module.</param>
        public ControllerAuthorizeAttribute(SystemModule systemModule)
        {
            SystemModule = systemModule;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ControllerAuthorizeAttribute" /> class.
        /// </summary>
        /// <param name="systemModule">The system module.</param>
        /// <param name="privilegeType">Type of the privilege.</param>
        public ControllerAuthorizeAttribute(SystemModule systemModule, PrivilegeType privilegeType)
        {
            SystemModule = systemModule;
            PrivilegeType = privilegeType;
        }

        #endregion

        #region Overriden Methods

        /// <summary>
        ///     When overridden, provides an entry point for custom authorization checks.
        ///     Called when a process requests authorization.
        ///     Define : scenario based authorization rules  which defined by UM domain
        /// </summary>
        /// <param name="httpContext">
        ///     The HTTP context, which encapsulates all HTTP-specific information about an individual HTTP
        ///     request.
        /// </param>
        /// <returns>
        ///     true if the user is authorized; otherwise, false.
        /// </returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //TODO: Implement authorization logic here

            //var service = UnityConfigProvider.GetConfiguredContainer().Resolve<IPermissionManagementService>();
            //try
            //{
            //    var privileges = service.FindPrivileges( NullHandler.NullHandlerForString(httpContext.User.Identity .GetUserName(), ""),
            //                                             (int) SystemModule  );
            //    return (privileges.Contains((int) PrivilegeType));
            //}
            //catch
            //{
            //    return false;
            //}

            return true;
        }

        /// <summary>
        ///     Processes HTTP requests that fail authorization.
        /// </summary>
        /// <param name="filterContext">
        ///     Encapsulates the information for using <see cref="T:System.Web.Mvc.AuthorizeAttribute" />.
        ///     The <paramref name="filterContext" /> object contains the controller, HTTP context, request context, action result,
        ///     and route data.
        /// </param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new {area = "Admin", controller = "Account", action = "Index"})
                    );
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new {area = "Admin", controller = "Error", action = "Unauthorized"})
                    );
            }
        }

        #endregion
    }
}