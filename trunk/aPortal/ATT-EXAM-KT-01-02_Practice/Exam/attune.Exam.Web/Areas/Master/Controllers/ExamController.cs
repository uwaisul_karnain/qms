﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using attune.Core.Adaptor;
using attune.Core.Helpers;
using attune.Core.Web.Mvc.Attributes;
using attune.Core.Web.Mvc.Controllers;
using attune.Exam.Application.Dtos;
using attune.Exam.Application.Interfaces;
using attune.Exam.Domain;
using attune.Exam.Domain.Queries;
using attune.Exam.Web.Areas.Master.Models.Exam;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using attune.Exam.Infrastructure.Resources;
using attune.Exam.Web.Extentions;
using attune.Exam.Web.Helpers;

namespace attune.Exam.Web.Areas.Master.Controllers
{
    public class ExamController : UnauthorizedControllerBase
    {
        #region Private Variables

        private readonly IQuestionPoolService _questionBankService;
        private readonly IMasterDataService _masterDataService;
        private readonly IAdaptor _adaptor;
        #endregion

        #region Public Methods

        /// <summary>
        /// Exam Controller
        /// </summary>
        /// <param name="questionBankService"></param>
        /// <param name="masterDataService"></param>
        /// <param name="adaptor"></param>
        public ExamController(IQuestionPoolService questionBankService, IMasterDataService masterDataService, IAdaptor adaptor)
        {
            _questionBankService = questionBankService;
            _masterDataService = masterDataService;
            _adaptor = adaptor;
            Populate();
        }

        // GET: Master/Exam
        public ActionResult Index()
        {
            return View(new ExamView());
        }

        #region Drop Down Load

        /// <summary>
        /// On Exam Name Read
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnExamNameRead(int examTypeId)
        {
            return Json(_masterDataService.GetExamNames(examTypeId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// On Category Read
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        public ActionResult OnCategoryRead(int examTypeId)
        {
            return Json(_masterDataService.GetCategories(examTypeId), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// On Result Format Type Read
        /// </summary>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnResultFormatTypeRead()
        {
            return Json(_masterDataService.GetResultFormatType(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Exam Grid events

        /// <summary>
        /// On Exam Manager Grid Read
        /// </summary>
        /// <param name="request"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnExamManagerGridRead([DataSourceRequest] DataSourceRequest request, ExamQuery query)
        {
            if (query != null && query.ExamTypeId != 0)
            {
                return Json(_questionBankService.FindExams(query).ToDataSourceResult(request),
               JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new JsonResult(),
               JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// On Exam De activate
        /// </summary>
        /// <param name="request"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult OnExamDeactivate([DataSourceRequest] DataSourceRequest request, string Id)
        {
            object result = null;
            try
            {
                var exam = _questionBankService.FindExam(Convert.ToInt64(Id));
                exam.IsActive = false;
                exam.UpdatedOn = DateTime.Now;
                exam.UpdatedBy = 1;//TODO:LoggedIn user Id
                if (_questionBankService.DeActivateExam(exam))
                    result =
                        new { Message = Messages.MasterExamDeActivateSuccess, Type = MessageType.Success };
                else
                    result =
                        new { Message = Messages.CommonRecordSaveFailed, Type = MessageType.Error };
            }
            catch (Exception ex)
            {
                result = new { Message = Messages.CommonRecordSaveFailed, Type = MessageType.Error };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        #endregion

        #region Exam Add

        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
          return View("Create", new CreateExamView());
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(CreateExamView model)
        {
            if (!ModelState.IsValid) return View(model);
            var response = new Response<string>();
            try
            {
                var dto = new Application.Dtos.Exam();
                dto.QuestionCategories=new List<QuestionCategory>();
                model.ExamTypeId = Convert.ToInt32(model.ddlExamType);
                model.QuestionCategories =Newtonsoft.Json.JsonConvert.DeserializeObject<List<QuestionCategoryView>>(model.QuestionCategoryString);
                AssignValues(model, dto);
                response.IsSuccess = _questionBankService.AddExam(dto);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.MessageType = MessageType.Error;
                throw;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Exam Edit
        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(string Id)
        {
            var result = _questionBankService.FindExam(Convert.ToInt64(Id));
            var examModel = new CreateExamView();
            examModel.mode = (int) ExamMode.Edit;
            examModel.ExamTypeId = (int) result.ExamType.Id;
            examModel.ddlExamType = result.ExamType.Id.ToString();
            examModel.DifficultLevel = (int)result.DifficultLevelId;
            examModel.Name = result.Name;
            examModel.Description = result.Description;
            examModel.Duration = result.Duration;
            examModel.IsManualPick = result.IsManualPick;
            examModel.ResultFormatTypeId = (int)result.ResultFormatType;
            examModel.PassScore = result.PassScore;
            examModel.Id = result.Id;
            examModel.ExamId = result.Id;
            examModel.QuestionCategories=new List<QuestionCategoryView>();
            foreach (var questionCategory in result.QuestionCategories)
            {
                var categoryView = new QuestionCategoryView()
                                   {
                                       Id = questionCategory.Id,
                                       CategoryId =
                                           new KeyValuePair<string, string>(questionCategory.Category.Id.ToString(),
                                           questionCategory.Category.Name),
                                       DifficultLevelId =
                                           new KeyValuePair<string, string>(Convert.ToInt32(questionCategory.DifficultLevel).ToString(),
                                           questionCategory.DifficultLevel.ToString()),
                                       CountValue = questionCategory.Count,
                                       Questions = questionCategory.Questions
                                   };
                examModel.QuestionCategories.Add(categoryView);
            }
            return View("Edit", examModel);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(CreateExamView model)
        {
            if (!ModelState.IsValid) return View(model);
            var response = new Response<string>();
            try
            {
                var dto = new Application.Dtos.Exam();
                dto.QuestionCategories = new List<QuestionCategory>();
                model.ExamTypeId = Convert.ToInt32(model.ddlExamType);
                model.QuestionCategories =Newtonsoft.Json.JsonConvert.DeserializeObject<List<QuestionCategoryView>>(model.QuestionCategoryString);
                dto.Id = model.ExamId;
                AssignValues(model,dto);
                response.IsSuccess = _questionBankService.UpdateExam(dto);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.MessageType = MessageType.Error;
                throw;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Question Selection

        /// <summary>
        /// Question Selection
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [CompressFilter]
        [HttpPost]
        public ActionResult QuestionSelection(QuestionSelectionQuery query)
        {
            return PartialView("_QuestionSelection", new QuestionSelectionView());
        }

        /// <summary>
        /// On Exam Category Question Read
        /// </summary>
        /// <param name="request"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnExamCategoryQuestionRead([DataSourceRequest] DataSourceRequest request,
            QuestionSelectionQuery query)
        {
            var questions = _questionBankService.FindQuestions(query);
            var index = 0;
            foreach (var question in questions)
            {
                question.Index = index;
                index++;
            }
            return Json(questions.ToDataSourceResult(request),
                JsonRequestBehavior.AllowGet);
        }


        #endregion

        #endregion

        private void Populate()
        {
            var levels = _masterDataService.GetDifficultLevels();
            ViewData[attune.Exam.Web.Helpers.Constants.VIEW_DATA_KEY_DIFFICULTLEVELS] = levels;
            ViewData[attune.Exam.Web.Helpers.Constants.VIEW_DATA_KEY_DEFAULT_LEVEL_ID] = levels.First().Key;
            var categories = new List<KeyValuePair<string,string>>();
            ViewData[attune.Exam.Web.Helpers.Constants.VIEW_DATA_KEY_CATEGORIES] = categories;
            ViewData[attune.Exam.Web.Helpers.Constants.VIEW_DATA_KEY_DEFAULT_CATEGORY_ID] = null;
            
        }

        private static void AssignValues(CreateExamView model, Application.Dtos.Exam dto)
        {
            dto.DifficultLevelId = (DifficultLevel)model.DifficultLevel;
            dto.ExamType = new ExamType()
            {
                Id = model.ExamTypeId
            };
            dto.Name = model.Name.Trim();
            if (model.Description != null) dto.Description = model.Description.Trim();
            dto.Duration = (decimal)model.Duration;
            dto.IsManualPick = (bool)model.IsManualPick;
            dto.ResultFormatType = (ResultFormatType)model.ResultFormatTypeId;
            dto.PassScore = (decimal)model.PassScore;
            foreach (var cat in model.QuestionCategories)
            {
                var category = new QuestionCategory()
                {
                    Count = cat.CountValue,
                    DifficultLevel =
                        (DifficultLevel)NullHandler.NullHandlerForInt(cat.DifficultLevelId.Key, 1),
                    Category =
                        new Category() { Id = NullHandler.NullHandlerForInt(cat.CategoryId.Key, 0) },
                    Questions = cat.Questions
                };
                dto.QuestionCategories.Add(category);
            }
            dto.CreatedOn = DateTime.Now;
            dto.UpdatedOn = DateTime.Now;
            dto.CreatedBy = 1; //TODO:Logged in user Id
            dto.UpdatedBy = 1; //TODO:Logged in user Id
            dto.IsActive = true;
        }
    }
}