﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using attune.Core.Helpers;
using attune.Core.Web.Mvc.Attributes;
using attune.Core.Web.Mvc.Controllers;
using attune.Exam.Application.Dtos;
using attune.Exam.Domain.Queries;
using attune.Exam.Web.Areas.Master.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using attune.Exam.Web.Helpers;
using attune.Exam.Infrastructure.Resources;
using attune.Exam.Web.Areas.Master.Models.QuestionBank;

namespace attune.Exam.Web.Areas.Master.Controllers
{
    public partial class QuestionBankController
    {
        /// <summary>
        /// Exam Type
        /// </summary>
        /// <returns></returns>
        public ActionResult ExamType()
        {
            var model = new ExamTypeView();
            model.ExamTypes = _questionBankService.FindExamTypes();
            return PartialView("_ExamType", model);
        }

        #region Exam Type Grid Events

        /// <summary>
        /// On Exam Type GridRead
        /// </summary>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnExamTypeGridRead([DataSourceRequest] DataSourceRequest request)
        {
            return Json(_questionBankService.FindExamTypes().ToDataSourceResult(request),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// On ExamType Grid Create
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OnExamTypeGridCreate([DataSourceRequest] DataSourceRequest request, ExamTypeView model)
        {
            if (!ModelState.IsValid) return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            try
            {
                var dto = new ExamType {Name = model.Name, Description = model.Description, IsActive = true};
                if (!_questionBankService.AddExamType(dto))
                    ModelState.AddModelError(Constants.MODEL_STATE_KEY_ERRORS_UPDATE,
                        Messages.CommonRecordSaveFailed);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(Constants.MODEL_STATE_KEY_ERRORS_CREATE, ex.Message);
                throw;
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        /// <summary>
        /// On ExamType Grid Update
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OnExamTypeGridUpdate([DataSourceRequest] DataSourceRequest request, ExamTypeView model)
        {
            if (!ModelState.IsValid) return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            try
            {
                var dto = new ExamType { Id=model.Id,Name = model.Name, Description = model.Description, IsActive = true };
                if (!_questionBankService.UpdateExamType(dto))
                    ModelState.AddModelError(Constants.MODEL_STATE_KEY_ERRORS_UPDATE,
                        Messages.CommonRecordSaveFailed);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(Constants.MODEL_STATE_KEY_ERRORS_CREATE, ex.Message);
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }
        
        /// <summary>
        /// On ExamType Grid Destroy
        /// </summary>
        /// <param name="request"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OnExamTypeGridDestroy([DataSourceRequest] DataSourceRequest request, string Id)
        {
            object result = null;
            try
            {
                var dto = _questionBankService.FindExamType(NullHandler.NullHandlerForLong(Id,0));
                dto.IsActive = false;
                if (!_questionBankService.UpdateExamType(dto))
                    result =new { Message = Messages.CommonRecordSaveFailed, Type = MessageType.Error };
                else
                    result =
                        new { Message = Messages.CommonRecordSaveSuccess, Type = MessageType.Success };
            }
            catch (Exception ex)
            {
                result = new { Message = Messages.CommonRecordSaveFailed, Type = MessageType.Error };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}