﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using attune.Core.Helpers;
using attune.Core.Web.Mvc.Attributes;
using attune.Exam.Application.Dtos;
using attune.Exam.Web.Areas.Master.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using attune.Exam.Web.Helpers;
using attune.Exam.Infrastructure.Resources;
using attune.Exam.Web.Areas.Master.Models.QuestionBank;

namespace attune.Exam.Web.Areas.Master.Controllers
{
    public partial class QuestionBankController 
    {
        /// <summary>
        /// Category
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult Category(string examTypeId)
        {
            var model = new CategoryView();
            model.Categories = _questionBankService.FindCategories(NullHandler.NullHandlerForInt(examTypeId, 0));
            var list = new List<Category>();
            foreach (var cat in model.Categories)
            {
                if (cat.ExamTypeId == null)
                    cat.IsCommon = true;
                list.Add(cat);
            }
            model.Categories = list;
            return PartialView("_Category", model);
        }

        #region Category Grid Events
        /// <summary>
        /// On Category Grid Read
        /// </summary>
        /// <param name="request"></param>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnCategoryGridRead([DataSourceRequest] DataSourceRequest request, string examTypeId)
        {
            var categories = _questionBankService.FindCategories(NullHandler.NullHandlerForInt(examTypeId, 0));
            var list = new List<Category>();
            foreach (var cat in categories)
            {
                if (cat.ExamTypeId == null)
                    cat.IsCommon = true;
                list.Add(cat);
            }
            return Json(list.ToDataSourceResult(request),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// On Category Grid Create
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OnCategoryGridCreate([DataSourceRequest] DataSourceRequest request, CategoryView model)
        {
            if (!ModelState.IsValid) return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            try
            {
                if (model.IsCommon)
                    model.ExamTypeId = null;
                else
                model.ExamType = new ExamType()
                {
                    Id = (long) model.ExamTypeId
                };

                var dto = new Category { Name = model.Name, Description = model.Description, IsActive = true ,IsCommon = model.IsCommon, ExamTypeId = model.ExamTypeId,ExamType = model.ExamType};
                if (!_questionBankService.AddCategory(dto))
                    ModelState.AddModelError(Constants.MODEL_STATE_KEY_ERRORS_UPDATE,
                        Messages.CommonRecordSaveFailed);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(Constants.MODEL_STATE_KEY_ERRORS_CREATE, ex.Message);
                throw;
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        /// <summary>
        /// On Category Grid Update
        /// </summary>
        /// <param name="request"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OnCategoryGridUpdate([DataSourceRequest] DataSourceRequest request, CategoryView model)
        {
            if (!ModelState.IsValid) return Json(new[] { model }.ToDataSourceResult(request, ModelState));
            try
            {
                if (model.IsCommon)
                    model.ExamTypeId = null;
                else
                    model.ExamType = new ExamType()
                    {
                        Id = (long)model.ExamTypeId
                    };

                var dto = new Category { Id = model.Id, Name = model.Name, Description = model.Description, IsActive = true, IsCommon = model.IsCommon, ExamTypeId = model.ExamTypeId, ExamType = model.ExamType };
                if (!_questionBankService.UpdateCategory(dto))
                    ModelState.AddModelError(Constants.MODEL_STATE_KEY_ERRORS_UPDATE,
                        Messages.CommonRecordSaveFailed);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(Constants.MODEL_STATE_KEY_ERRORS_CREATE, ex.Message);
            }
            return Json(new[] { model }.ToDataSourceResult(request, ModelState));
        }

        /// <summary>
        /// On Category Grid Destroy
        /// </summary>
        /// <param name="request"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult OnCategoryGridDestroy([DataSourceRequest] DataSourceRequest request, string Id)
        {
            object result = null;
            try
            {
                var dto = _questionBankService.FindCategory(NullHandler.NullHandlerForLong(Id, 0));
                dto.IsActive = false;
                if (!_questionBankService.UpdateCategory(dto))
                    result = new { Message = Messages.CommonRecordSaveFailed, Type = MessageType.Error };
                else
                    result = new { Message = Messages.CommonRecordSaveSuccess, Type = MessageType.Success };
            }
            catch (Exception ex)
            {
                result = new { Message = Messages.CommonRecordSaveFailed, Type = MessageType.Error };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}