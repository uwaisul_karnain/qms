﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using attune.Core.Adaptor;
using attune.Core.Helpers;
using attune.Core.Web.Mvc.Attributes;
using attune.Core.Web.Mvc.Controllers;
using attune.Exam.Application.Interfaces;
using attune.Exam.Domain.Queries;
using attune.Exam.Infrastructure.Resources;
using attune.Exam.Web.Areas.Master.Models;
using attune.Exam.Web.Helpers;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using OfficeOpenXml;
using Constants = attune.Exam.Web.Helpers.Constants;
using attune.Exam.Application.Dtos;
using attune.Exam.Domain;
using attune.Exam.Web.Areas.Master.Models.QuestionPool;
using attune.Exam.Web.Extentions;
using AutoMapper.Internal;

namespace attune.Exam.Web.Areas.Master.Controllers
{
    public class QuestionPoolController : UnauthorizedControllerBase
    {

        #region Private Variables

        private readonly IQuestionPoolService _questionPoolService;
        private readonly IMasterDataService _masterDataService;
        private readonly IAdaptor _adaptor;
        #endregion

        #region Public Methods

                /// <summary>
        /// Initializes a new instance of the <see cref="QuestionPoolController"/> class.
        /// </summary>
        /// <param name="questionPoolService">The question Bank service.</param>
        /// <param name="masterDataService"></param>
        public QuestionPoolController(IQuestionPoolService questionPoolService, IMasterDataService masterDataService, IAdaptor adaptor)
        {
            _questionPoolService = questionPoolService;
            _masterDataService = masterDataService;
            _adaptor = adaptor;

            //Populate();
        }

        // GET: Master/QuestionPool
        //public ActionResult Index(string param)
        public ActionResult Index()
        {
            return View(new QuestionPoolView());
        }

        #region Qustion bank grid events

        /// <summary>
        /// QuestionPool Grid Read
        /// </summary>
        /// <param name="request"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnQuestionPoolGridRead([DataSourceRequest] DataSourceRequest request, QuestionQuery query)
        {
            if (query != null && query.ExamTypeId != 0)
            {
                return Json(_questionPoolService.FindQuestions(query).ToDataSourceResult(request),
               JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new JsonResult(),
               JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Question Deactivate
        /// </summary>
        /// <param name="request"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult OnQuestionDeactivate([DataSourceRequest] DataSourceRequest request, string Id)
        {
            object result = null;
            try
            {
                var question = _questionPoolService.FindQuestion(Convert.ToInt64(Id));
                question.IsActive = false;
                question.UpdatedOn = DateTime.Now;
                question.UpdatedBy = 1;//TODO:LoggedIn user Id
                if (_questionPoolService.DeActivateQuestion(question))
                    result =
                        new { Message = Messages.MasterQuestionBankDeActivateSuccess, Type = MessageType.Success };
                else
                    result =
                        new { Message = Messages.CommonRecordSaveFailed, Type = MessageType.Error };
            }
            catch (Exception ex)
            {
                result = new { Message = Messages.CommonRecordSaveFailed, Type = MessageType.Error };
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Question Publish
        /// </summary>
        /// <param name="request"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult OnQuestionPublish([DataSourceRequest] DataSourceRequest request, string Id)
        {
            object result = null;
            try
            {
                var question = _questionPoolService.FindQuestion(Convert.ToInt64(Id));
                question.IsPublished = true;
                question.UpdatedOn = DateTime.Now;
                question.UpdatedBy = 1;//TODO:LoggedIn user Id
                if (_questionPoolService.UpdateQuestion(question))
                    result =
                        new { Message = Messages.MasterQuestionBankPublishSuccess, Type = MessageType.Success };
                else
                    result =
                        new { Message = Messages.CommonRecordSaveFailed, Type = MessageType.Error };
            }
            catch (Exception ex)
            {
                result = new { Message = ex.Message, Type = MessageType.Error };

            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Question DownLoad Template
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult OnQuestionDownLoadTemplate([DataSourceRequest] DataSourceRequest request)
        {
            var serverPath = System.Web.HttpContext.Current.Server.MapPath(Constants.EXCEL_PATH);
            const string fileName = Constants.EXCEL_NAME;
            var filepath = Path.Combine(serverPath, fileName);

            Response.AddHeader("content-disposition", filepath);
            Response.ContentType = "application/vnd.ms-excel";
            Response.BufferOutput = true;
            Response.ContentEncoding = Encoding.UTF8;
            Response.Charset = "UTF-8";

            return Json(filepath, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Download Excel
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpGet]
        public virtual ActionResult Download(string file)
        {
            if (file != null)
            {
                var fileName = file.Substring(file.LastIndexOf("\\", StringComparison.Ordinal) + 1).Trim();
                var fullPath = Path.Combine(Server.MapPath(Constants.EXCEL_TEMP_PATH), file);
                return File(fullPath, "application/vnd.ms-excel", fileName);
            }
            return null;
        }

        /// <summary>
        /// Question Upload
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        
        public ActionResult OnQuestionUpload(HttpPostedFileBase file)
        {
            ExcelWorksheet worksheet;
            var uploadResult = UploadHelper.ParsePostedFile(file, out worksheet);
            var result = new object { };
            if (uploadResult.ErrorType == UploadErrorType.None && worksheet != null)
            {
                DataTable dt = UploadHelper.ToDataTable(worksheet);
                dt.ConvertToBool(4);
                var list = dt.ToList<Application.Dtos.UploadQuestion>();
                if (list.Count > 0)
                {
                    var val = _questionPoolService.AddBulkQuestions(list);
                    if (val)
                        return Json(new
                        {
                            UploadResult = uploadResult,
                            Message = Messages.MasterQuestionBankUploadSuccess,
                            Type = MessageType.Success
                        }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new
                        {
                            UploadResult = uploadResult,
                            Message = Messages.MasterQuestionBankUploadErrorOccured,
                            Type = MessageType.Error
                        }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                UploadResult = uploadResult,
                Message = string.Empty,
                Type = MessageType.Error
            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Drop Down Load
        /// <summary>
        /// On ExamType Read
        /// </summary>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnExamTypeRead()
        {
            return Json(_masterDataService.GetExamTypes(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// On Category Read
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnCategoryRead(int examTypeId)
        {
            return Json(_masterDataService.GetCategories(examTypeId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// On Difficult Level Read
        /// </summary>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnDifficultLevelRead()
        {
            return Json(_masterDataService.GetDifficultLevels(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// On Question Type Read
        /// </summary>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnQuestionTypeRead()
        {
            return Json(_masterDataService.GetQuestionTypes(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Question Add
        /// <summary>
        /// Create
        /// </summary>
        /// <returns></returns>
        //public ActionResult Create()
        //{
        //    return View("Create", new CreateQuestionView()
        //    {
        //        Id = 0,
        //        Answers = new List<Answer>()
        //                                        {
        //                                            new Answer(),
        //                                            new Answer(),
        //                                            new Answer(),
        //                                            new Answer()
        //                                        }
        //    });
        //}

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        //public ActionResult Create(CreateQuestionView model)
        //{
        //    if (!ModelState.IsValid) return View(model);
        //    var response = new Response<List<Answer>>();
        //    try
        //    {
        //        if (model.PublishedMode == "1")
        //            model.IsPublished = true;

        //        model.ExamTypeId = Convert.ToInt32(model.ddlExamType);
        //        model.CategoryId = Convert.ToInt32(model.ddlCategory);
        //        model.Answers = new JavaScriptSerializer().Deserialize<List<Answer>>(model.AnswersString);
        //        model.Answers = model.Answers.Where(x => x.Response != string.Empty && x.Response != null).ToList();
        //        model.CreatedOn = DateTime.Now;
        //        model.UpdatedOn = DateTime.Now;
        //        model.CreatedBy = 1;//TODO:Logged in user Id
        //        model.IsActive = true;
        //        model.ExamType = new ExamType()
        //        {
        //            Id = model.ExamTypeId
        //        };
        //        model.Category = new Category()
        //        {
        //            Id = model.CategoryId
        //        };
        //        model.CandidateResponse = new List<Answer>();
        //        model.QuestionType = (QuestionType)model.QuestionTypeId;
        //        model.DifficultLevel = (DifficultLevel)model.DifficultLevelId;
        //        model.Question = model.Question.Trim();

        //        response.IsSuccess = _questionBankService.AddQuestion(model);
        //        response.Owner = new List<Answer>()
        //                         {
        //                             new Answer(),
        //                             new Answer(),
        //                             new Answer(),
        //                             new Answer()
        //                         };
        //    }
        //    catch (Exception ex)
        //    {
        //        response.IsSuccess = false;
        //        response.Message = ex.Message;
        //        response.MessageType = MessageType.Error;
        //        throw;
        //    }
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}

        #endregion

        #region Question Edit

        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult Edit(string Id)
        {
            var result = _questionPoolService.FindQuestion(Convert.ToInt64(Id));
            var quesModel = new CreateQuestionView
            {
                CategoryId = (int)result.Category.Id,
                ExamTypeId = (int)result.ExamType.Id,
                DifficultLevelId = (int)result.DifficultLevel,
                QuestionTypeId = (int)result.QuestionType,
                Question = result.Question,
                Answers = result.Answers,
                IsPublished = result.IsPublished
            };
            if (quesModel.Answers.Count < 4)
            {
                for (int i = quesModel.Answers.Count + 1; i <= 4; i++)
                {
                    quesModel.Answers.Add(new Answer()
                    {
                        Response = "",
                        IsCorrect = false
                    });
                }
            }
            return View("Edit", quesModel);
        }

        /// <summary>
        /// Edit
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Edit(CreateQuestionView model)
        {
            if (!ModelState.IsValid) return View(model);
            var response = new Response<List<Answer>>();
            try
            {
                if (model.PublishedMode == "1")
                    model.IsPublished = true;

                model.ExamTypeId = Convert.ToInt32(model.ddlExamType);
                model.CategoryId = Convert.ToInt32(model.ddlCategory);
                model.Answers = new JavaScriptSerializer().Deserialize<List<Answer>>(model.AnswersString);
                model.Answers = model.Answers.Where(x => x.Response != string.Empty && x.Response != null).ToList();
                model.CreatedOn = DateTime.Now;
                model.UpdatedOn = DateTime.Now;
                model.CreatedBy = 1;//TODO:Logged in user Id
                model.UpdatedBy = 1;//TODO:Logged in user Id
                model.IsActive = true;
                model.ExamType = new ExamType()
                {
                    Id = model.ExamTypeId
                };
                model.Category = new Category()
                {
                    Id = model.CategoryId
                };
                model.CandidateResponse = new List<Answer>();
                model.QuestionType = (QuestionType)model.QuestionTypeId;
                model.DifficultLevel = (DifficultLevel)model.DifficultLevelId;
                model.Question = model.Question.Trim();

                response.IsSuccess = _questionPoolService.UpdateQuestion(model);

            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.MessageType = MessageType.Error;
                throw;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #endregion

        /// <summary>
        /// Populate Drop Downs
        /// </summary>
        private void Populate()
        {
            var levels = _masterDataService.GetDifficultLevels();
            var examType = _masterDataService.GetExamTypes();
            ViewData[Constants.VIEW_DATA_KEY_CATEGORIES] =
                _masterDataService.GetCategories(NullHandler.ConvertToInt(examType.FirstOrDefault().Key));
            ViewData[Constants.VIEW_DATA_KEY_DIFFICULTLEVELS] = levels;
            ViewData[Constants.VIEW_DATA_KEY_EXAMTYPES] = examType;

        }
    }
}