﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using attune.Core.Web.Mvc.Attributes;
using attune.Exam.Domain.Entities;
using attune.Exam.Infrastructure.Resources;
using QuestionCategory = attune.Exam.Application.Dtos.QuestionCategory;

namespace attune.Exam.Web.Areas.Master.Models.Exam
{
    public class CreateExamView : Application.Dtos.Exam
    {
        public CreateExamView()
        {
            this.IsActive = true;
            
        }
        public int mode { get; set; }
        public int ExamId { get; set; }

        [Display(Name = "MasterExamLabelExamType", ResourceType = typeof (Labels))]
        [UIHint("ExamOnChangeExamType")]
        [Required]
        public int ExamTypeId { get; set; }
        
        [Display(Name = "MasterExamLabelDifficultLevel", ResourceType = typeof (Labels))]
        [Required]
        [UIHint("DifficultLevel")]
        public int DifficultLevel { get; set; }

        [Required]
        [Display(Name = "MasterExamLabelExamName", ResourceType = typeof (Labels))]
        public override string Name { get; set; }

        [Display(Name = "MasterExamLabelExamDescription", ResourceType = typeof (Labels))]
        public override string Description { get; set; }

        [Required]
        [Display(Name = "MasterExamLabelDuration", ResourceType = typeof(Labels))]
        [RegularExpression(@"[-+]?[0-9]*\.?[0-9]?[0-9]", ErrorMessageResourceName = "MasterExamIntegerValidationDuration", ErrorMessageResourceType = typeof(Messages))] 
        [Range(1, 99, ErrorMessageResourceName = "MasterExamIntegerValidationDuration", ErrorMessageResourceType = typeof(Messages))]
        public new decimal? Duration { get; set; }

        [Required]
        [UIHint("QuestionPick")]
        [Display(Name = "MasterExamLabelQuestionPickining", ResourceType = typeof (Labels))]
        public new bool? IsManualPick { get; set; }

        [Required]
        [UIHint("ResultFormatType")]
        [Display(Name = "MasterExamLabelShowResults", ResourceType = typeof (Labels))]
        public int ResultFormatTypeId { get; set; }

        [Display(Name = "MasterExamLabelPassMarks", ResourceType = typeof(Labels))]
        [RegularExpression(@"[-+]?[0-9]*\.?[0-9]?[0-9]", ErrorMessageResourceName = "MasterExamIntegerValidationPassScore", ErrorMessageResourceType = typeof(Messages))] 
        [Range(1, 100, ErrorMessageResourceName = "MasterExamIntegerValidationPassScore", ErrorMessageResourceType = typeof(Messages))]
        [Required]
        public new decimal? PassScore { get; set; }
        
        public string ddlExamType { get; set; }

        public new List<QuestionCategoryView> QuestionCategories { get; set; }

        public string QuestionCategoryString { get; set; }

    }

    public class QuestionCategoryView : Application.Dtos.QuestionCategory
    {
        [Display(Name = "MasterExamLabelDifficultLevel", ResourceType = typeof(Labels))]
        [UIHint("KDifficultLevel")]
        public KeyValuePair<string, string> DifficultLevelId { get; set; }

        [UIHint("KCategory")]
        [Display(Name = "MasterExamLabelCategory", ResourceType = typeof(Labels))]
        public KeyValuePair<string, string> CategoryId { get; set; }

        [StringLength(2, ErrorMessageResourceName = "MasterExamIntegerLengthValidationNoOfQuestion", ErrorMessageResourceType = typeof(Messages))]
        [Range(0, int.MaxValue, ErrorMessageResourceName = "MasterExamIntegerValidationNoOfQuestion", ErrorMessageResourceType = typeof(Messages))]
        [DataType(DataType.Text)]
        [Display(Name = "MasterExamNoOfQuestion", ResourceType = typeof(Labels))]
        public int? CountValue { get; set; }

        
        public List<QuestionSelectionView> QuestionSelection { get; set; }

        [ScriptIgnore]
        public new int? Count { get; set; }
    }

  }