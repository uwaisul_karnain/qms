﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System.ComponentModel.DataAnnotations;
using attune.Core.Web.Mvc.Attributes;
using attune.Exam.Infrastructure.Resources;

namespace attune.Exam.Web.Areas.Master.Models.QuestionBank
{
    public class FilterView
    {
        [Display(Name = "MasterQuestionBankLabelExamType", ResourceType = typeof(Labels))]
        [UIHint("ExamType")]
        [Required]
        public int ExamTypeId { get; set; }

        [Display(Name = "MasterQuestionBankLabelCategory", ResourceType = typeof(Labels))]
        [UIHint("Category")]
        public int? CategoryId { get; set; }

        [Display(Name = "MasterQuestionBankLabelDifficultLevel", ResourceType = typeof(Labels))]
        [UIHint("DifficultLevel")]
        public int? DifficultLevelId { get; set; }

        [Display(Name = "MasterQuestionBankLabelQuestionMode", ResourceType = typeof(Labels))]
        [UIHint("QuestionMode")]
        public bool? IsPublished { get; set; }
    }
}