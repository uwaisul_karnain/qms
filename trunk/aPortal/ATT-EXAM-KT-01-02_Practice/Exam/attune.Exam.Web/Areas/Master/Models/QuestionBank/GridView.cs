﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System.Collections.Generic;
using attune.Core.Web.Kendo.UI;
using Kendo.Mvc.UI;

namespace attune.Exam.Web.Areas.Master.Models.QuestionBank
{
    public class GridView : GridViewBase<Application.Dtos.QuestionBank>
    {
        public override List<GridColumnSettings> ColumnSettings
        {
            get
            {
                var items = new List<GridColumnSettings>
                            {
                                new GridCommandColumnSettings
                                {
                                    Commands =
                                    {
                                        new GridEditActionCommand()
                                    },
                                    Width = "auto"
                                }

                            };

                return items;
            }
        }

        
    }
}