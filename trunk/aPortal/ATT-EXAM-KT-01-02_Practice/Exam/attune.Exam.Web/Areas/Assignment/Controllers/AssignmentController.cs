﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using attune.Core.Adaptor;
using attune.Core.Helpers;
using attune.Core.Web.Mvc.Attributes;
using attune.Core.Web.Mvc.Controllers;
using attune.Exam.Application.Interfaces;
using attune.Exam.Domain;
using attune.Exam.Domain.Entities;
using attune.Exam.Domain.Queries;
using attune.Exam.Web.Areas.Assignment.Models.Assignment;
using attune.Exam.Web.Helpers;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.Owin.Security.Provider;
using Answer = attune.Exam.Application.Dtos.Answer;
using Candidate = attune.Exam.Application.Dtos.Candidate;

namespace attune.Exam.Web.Areas.Assignment.Controllers
{
    public class AssignmentController : UnauthorizedControllerBase
    {
        #region Private Variables

        private readonly IAdaptor _adaptor;
        private readonly IMasterDataService _masterDataService;
        private readonly IAssignmentService _assignmentService;
        
       
        #endregion

        #region Public Methods

        public AssignmentController(IAdaptor adaptor,IMasterDataService masterDataService,IAssignmentService assignmentService)
        {
            _adaptor = adaptor;
             _masterDataService = masterDataService;
            _assignmentService = assignmentService;
        }

        // GET: Assignment/Schedule
        public ActionResult Index()
        {
            return View(new ScheduleView());
        }

        /// <summary>
        /// Assign Exam
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AssignExam(ScheduleView model)
        {
            return PartialView("_Assignment", model);  
        }

        /// <summary>
        /// Employee Selection
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult EmployeeSelection(ScheduleView model)
        {
            model.EmployeeSelction=new EmployeeSelection();
            model.EmployeeSelction.CandidateView=new CandidateView();
            return PartialView("_EmployeeSelection", model.EmployeeSelction);
        }

        /// <summary>
        /// Non EmployeeSelection
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult NonEmployeeSelection(ScheduleView model)
        {
            model.EmployeeSelction = new EmployeeSelection();
            model.EmployeeSelction.CandidateView = new CandidateView();
            model.EmployeeSelction.FromDate = DateTime.Now;
            model.EmployeeSelction.ToDate = DateTime.Now;
            return PartialView("_NonEmployeeSelection", model.EmployeeSelction);
        }

        #region Drop Down
        /// <summary>
        /// On ExamType Read
        /// </summary>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnExamTypeRead()
        {
            return Json(_masterDataService.GetExamTypes(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// On Exam Name Read
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnExamNameRead(int examTypeId)
        {
            return Json(_masterDataService.GetExamNames(examTypeId), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// On Division Read
        /// </summary>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnDivisionRead()
        {
            return Json(_assignmentService.GetDivisions(), JsonRequestBehavior.AllowGet);
        }
        
        /// <summary>
        /// On Section Read
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <returns></returns>
        [CompressFilter]
         public ActionResult OnSectionRead(string divisionCode)
        {
            return Json(_assignmentService.GetSections(divisionCode), JsonRequestBehavior.AllowGet);
        }
        
        /// <summary>
        /// On Designation Read
        /// </summary>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnDesignationRead()
        {
            return Json(_assignmentService.GetDesignations(), JsonRequestBehavior.AllowGet);
        }

        #endregion


        /// <summary>
        /// On Employee Selection Read
        /// </summary>
        /// <param name="request"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnEmployeeSelectionRead([DataSourceRequest] DataSourceRequest request,EmployeeSelectionQuery query)
        {
            var emplyees = _assignmentService.GetEmployeeDetail(query.DivisionCode,query.SectionCode,query.Designation);
            var lstCandidates = emplyees.Select(emp => new Candidate
                                                       {
                                                           Id = emp.EmpNo, FirstName = emp.FirstName, LastName = emp.LastName, Email = emp.Email
                                                       }).ToList();
            var index = 0;
            foreach (var candidate in lstCandidates)
            {
                candidate.Index = index;
                index++;
            }
            return Json(lstCandidates.ToDataSourceResult(request),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// On Non Employee Selection Read
        /// </summary>
        /// <param name="request"></param>
        /// <param name="query"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnNonEmployeeSelectionRead([DataSourceRequest] DataSourceRequest request,EmployeeSelectionQuery query)
        {
            var emplyees = _assignmentService.GetNonEmployeeDetail(query.FromDate, query.ToDate);
            var lstCandidates = emplyees.Select(emp => new Candidate
            {
                Id = emp.EmpNo,
                FirstName = emp.FirstName,
                LastName = emp.LastName,
                Email = emp.Email
            }).ToList();

            var index = 0;
            foreach (var candidate in lstCandidates)
            {
                candidate.Index = index;
                index++;
            }
            return Json(lstCandidates.ToDataSourceResult(request),
                JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Candidate Assignment
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult CandidateAssignment(ScheduleView model)
        {
            model.EmployeeSelction = new EmployeeSelection();
            model.EmployeeSelction.CandidateView = new CandidateView();
            return PartialView("_CandidateAssignment", model.EmployeeSelction);
        }

        /// <summary>
        /// On Candidate Assignment Read
        /// </summary>
        /// <param name="request"></param>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        [CompressFilter]
        public ActionResult OnCandidateAssignmentRead([DataSourceRequest] DataSourceRequest request, string scheduleId)
        {
            if (scheduleId != null && scheduleId!="0")
            {
                var lstCandidates = _assignmentService.FindScheduleCandidates(Convert.ToInt32(scheduleId));
                return Json(lstCandidates.ToDataSourceResult(request),
                    JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new JsonResult(),
             JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Assign Exam Schedule
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AssignExamSchedule(ScheduleView model)
        {
            if (!ModelState.IsValid) return View(model);
            var response = new Response<List<Answer>>();
            try
            {
                var query = new ExamScheduleCreateOrUpdateQuery
                            {
                                Schedule = new Schedule(),
                                Assignment = new Domain.Entities.Assignment(),
                                Candidates = new List<Domain.Entities.Candidate>()
                            };
                query.Assignment.Exam=new Domain.Entities.Exam();
                query.Schedule.StartDate = model.Start;
                query.Schedule.EndDate = model.End;
                query.Assignment.Exam.Id = (long) model.ExamId;
                query.Assignment.IsAvailableAlways = (bool) model.IsExamAvailable;
                query.Assignment.AssignmentStatus = Domain.ExamStatus.Pending;
                if (model.NonEmployee)
                    query.Schedule.IsEmployee = false;
                else
                    query.Schedule.IsEmployee = true;
                query.Schedule.OnBehalfEmail = model.OnBehalfOfEmail;
                query.Schedule.IsCalendarInviteRequired = model.SendCalendarInvite;
                var dtos = new List<Candidate>();
                dtos = new JavaScriptSerializer().Deserialize<List<Application.Dtos.Candidate>>(model.CandidateString);
               
                query.Candidates = new JavaScriptSerializer().Deserialize<List<Domain.Entities.Candidate>>(model.CandidateString);
                foreach (var candidate in query.Candidates)
                {
                    candidate.IsEmployee = query.Schedule.IsEmployee;
                }
                query.Schedule.CreatedBy = 1;//TODO Logged in userId
                query.Schedule.UpdatedBy = 1;//TODO Logged in userId
                query.Schedule.CreatedOn = DateTime.Now;
                query.Schedule.UpdatedOn = DateTime.Now;
                response.IsSuccess = _assignmentService.AddExamSchedule(query);
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
                response.MessageType = MessageType.Error;
                throw;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public virtual JsonResult OnSchedularRead([DataSourceRequest] DataSourceRequest request)
        {
            var result = _assignmentService.FindSchedules();
            var lst = new List<ScheduleView>();

            foreach (var item in result)
            {
                var model = new ScheduleView();
                model.Title = item.Exam.Name;
                model.Start = item.Schedule.StartDate;
                model.End = item.Schedule.EndDate;
                model.ScheduleId = (int) item.Schedule.Id;
               lst.Add(model);
            }
            return Json(lst.ToDataSourceResult(request));
        }
        
        
        #endregion
    }
}