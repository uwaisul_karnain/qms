﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using attune.Exam.Application.Dtos;
using attune.Exam.Infrastructure.Resources;
using Kendo.Mvc.UI;

namespace attune.Exam.Web.Areas.Assignment.Models.Assignment
{
    public class ScheduleView : ISchedulerEvent
    {
        public int ScheduleId { get; set; }
        public string Title { get; set; }
        
        public string Description { get; set; }
        public bool IsAllDay { get; set; }
        [UIHint("DateTime")]
        public DateTime Start { get; set; }
        [UIHint("DateTime")]
        public DateTime End { get; set; }
        public string StartTimezone { get; set; }
        public string EndTimezone { get; set; }
        public string RecurrenceRule { get; set; }
        public string RecurrenceException { get; set; }

        [Display(Name = "MasterExamLabelExamType", ResourceType = typeof(Labels))]
        [UIHint("DDLExamType")]
        [Required]
        public int ExamTypeId { get; set; }

        [UIHint("DDLExamName")]
        [Required]
        [Display(Name = "MasterExamLabelExamName", ResourceType = typeof(Labels))]
        public int? ExamId { get; set; }

        [UIHint("ExamAvailable")]
        [Required]
        [Display(Name = "AssignmentAssignmentLabelExamAvailability", ResourceType = typeof(Labels))]
        public bool? IsExamAvailable { get; set; }

        [Display(Name = "AssignmentAssignmentLabelNonEmployee", ResourceType = typeof(Labels))]
        public bool NonEmployee { get; set; }

        [Display(Name = "AssignmentAssignmentLabelOnBehalfOfEmail", ResourceType = typeof(Labels))]
        [DataType(DataType.EmailAddress, ErrorMessageResourceName = "AssignmentAssignmentEmailValidation",ErrorMessageResourceType = typeof(Messages))]
        public string OnBehalfOfEmail { get; set; }

        [Display(Name = "AssignmentAssignmentLabelSendCalendarInvite", ResourceType = typeof(Labels))]
        public bool SendCalendarInvite { get; set; }

        public EmployeeSelection EmployeeSelction { get; set; }

        public string CandidateString { get; set; }

        public string ddlExamType { get; set; }

    }

    public class EmployeeSelection
    {
        [UIHint("Division")]
        [Required]
        [Display(Name = "AssignmentAssignmentLabelDivision", ResourceType = typeof(Labels))]
        public string DivisionCode { get; set; }

        [UIHint("Section")]
        [Display(Name = "AssignmentAssignmentLabelSection", ResourceType = typeof(Labels))]
        public string SectionCode { get; set; }

        [UIHint("Designation")]
        [Display(Name = "AssignmentAssignmentLabelDesignation", ResourceType = typeof(Labels))]
        public string Designation { get; set; }

        [UIHint("Date")]
        [Display(Name = "AssignmentAssignmentLabelFromDate", ResourceType = typeof(Labels))]
        public DateTime FromDate { get; set; }

        [UIHint("Date")]
        [Display(Name = "AssignmentAssignmentLabelToDate", ResourceType = typeof(Labels))]
        public DateTime ToDate { get; set; }

        public CandidateView CandidateView { get; set; }
    }

    
}