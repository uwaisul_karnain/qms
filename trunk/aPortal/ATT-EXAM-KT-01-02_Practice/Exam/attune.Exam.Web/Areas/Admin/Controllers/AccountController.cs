﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using attune.Core.Security;
using attune.Core.Security.Owin;
using attune.Core.Web.Mvc.Controllers;
using attune.Exam.Infrastructure.Resources;
using attune.Exam.Web.Areas.Admin.Models;
using Microsoft.Owin.Security;

namespace attune.Exam.Web.Areas.Admin.Controllers
{
    public class AccountController : AuthorizedControllerBase
    {
        #region Private Variables

        private ISecurity _identitySecurity;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the identity security.
        /// </summary>
        /// <value>
        /// The identity security.
        /// </value>
        public ISecurity IdentitySecurity
        {
            get { return _identitySecurity ?? (_identitySecurity = new SecurityManager(HttpContext.GetOwinContext())); }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Indexes this instance.
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Index()
        {
            return User.Identity.IsAuthenticated ? RedirectToLocal("") : View();
        }

        /// <summary>
        /// Indexes the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result =
                await IdentitySecurity.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, true);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    ModelState.AddModelError("", Messages.AdminAccountUserAccountIsLocked);
                    return View(model);
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("VerifyCode", new {ReturnUrl = returnUrl, rememberMe = model.RememberMe});
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", Messages.AdminAccountInvalidLoginAttempt);
                    return View(model);
            }
        }

        /// <summary>
        /// Signouts this instance.
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Signout()
        {
            await IdentitySecurity.SignOut();

            return Redirect(Url.Action("Index"));
        }

        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string returnUrl, bool rememberMe)
        {
            if (User.Identity.IsAuthenticated)
                RedirectToLocal("");
            await IdentitySecurity.SendTwoFactorCodeAsync(TwoFactorProvider.EmailCode);
            ViewBag.ReturnUrl = returnUrl;
            var model = new TwoFactorConfirmViewModel
                        {
                            RememberMe = rememberMe
                        };
            return View(model);
        }

        /// <summary>
        /// Verifies the code.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(TwoFactorConfirmViewModel viewModel, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                RedirectToLocal("");
            if (!ModelState.IsValid)
                return View(viewModel);

            var result =
                await IdentitySecurity.TwoFactorSignInAsync("EmailCode", viewModel.Code, viewModel.RememberMe, false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    ModelState.AddModelError("", Messages.AdminAccountUserAccountIsLocked);
                    return View(viewModel);
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", Messages.AdminAccountVerificationCodeIsInvalid);
                    return View(viewModel);
            }
        }


        /// <summary>
        /// Forgots the password.
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            if (User.Identity.IsAuthenticated)
            {
                RedirectToLocal("");
            }
            return View();
        }

        /// <summary>
        /// Forgots the password.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel viewModel)
        {
            if (!ModelState.IsValid)
                return View(viewModel);

            var user = await IdentitySecurity.FindByNameAsync(viewModel.UserName);
            if (user == null)
            {
                ModelState.AddModelError("", Messages.AdminAccountUserNameDoesNotExsits);
                return View(viewModel);
            }

            var token = await IdentitySecurity.GeneratePasswordResetTokenAsync(user.Id);

            var callbackUrl = Url.Action(
                "ResetPassword", "Account",
                new {userId = user.Id, token}, Request.Url.Scheme);

            await
                IdentitySecurity.SendEmailAsync(user.Id, Labels.AdminAccountForgotPasswordLink,
                    string.Format("{0}: <a href='{1}'>{2}</a>", Labels.AdminAccountPlaseClickBellowToResetYourPassword,
                        callbackUrl, Labels.AdminAccountLink));

            return View("Index");
        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ResetPassword(string userId, string token)
        {
            if (userId == null || token == null)
                return RedirectToAction("InvalidRequest", "Error");
            return View();
        }

        /// <summary>
        /// Resets the password.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="userId">The user identifier.</param>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel viewModel, string userId, string token)
        {
            if (userId == null || token == null)
                return RedirectToAction("InvalidRequest", "Error");

            if (!ModelState.IsValid)
                return View(viewModel);

            var result = await IdentitySecurity.ResetPasswordAsync(userId, token, viewModel.Password);
            if (result.Succeeded)
                return View("Index");
            AddErrors(result);
            return View();
        }


        /// <summary>
        /// Externals the login.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider,
                Url.Action("ExternalLoginCallback", "Account", new {ReturnUrl = returnUrl}));
        }

        /// <summary>
        /// Externals the login callback.
        /// </summary>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var result = await IdentitySecurity.ExternalLoginSignIn();
            switch (result.Status)
            {
                case ExternalLoginStatus.Success:
                    return RedirectToLocal(returnUrl);
                case ExternalLoginStatus.UserAccountRequired:
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = result.LoginProvider;
                    return View("ExternalLoginConfirmation",
                        new ExternalLoginConfirmationViewModel {UserName = result.DefaultUserName, Email = result.Email});
                case ExternalLoginStatus.Failed:
                default:
                    return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Externals the login confirmation.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="returnUrl">The return URL.</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model,
            string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToLocal("");

            if (ModelState.IsValid)
            {
                var user = new AppUser {UserName = model.UserName, Email = model.Email};
                var result = await IdentitySecurity.ExternalLoginConfirmation(user);

                if (result.Status == ExternalLoginStatus.Success)
                    return RedirectToLocal(returnUrl);
                AddErrorsList(result.Errors);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        /// <summary>
        /// Inner class to represent the call back request from the external login providers
        /// </summary>
        /// <seealso cref="System.Web.Mvc.HttpUnauthorizedResult" />
        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
            }

            private string LoginProvider { get; set; }
            private string RedirectUri { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties {RedirectUri = RedirectUri};

                var owin = context.HttpContext.GetOwinContext();
                owin.Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion

        #region Private Methods

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);
            return RedirectToAction("Index", "Home", new {area = ""});
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
                ModelState.AddModelError("", error);
        }

        private void AddErrorsList(IEnumerable<string> errors)
        {
            foreach (var error in errors)
                ModelState.AddModelError("", error);
        }

        #endregion
    }
}