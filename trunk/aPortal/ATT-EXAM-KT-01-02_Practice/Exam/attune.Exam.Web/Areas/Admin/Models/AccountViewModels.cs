﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.ComponentModel.DataAnnotations;
using attune.Exam.Infrastructure.Resources;

namespace attune.Exam.Web.Areas.Admin.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountUserName")]
        public string UserName { get; set; }

        [Required]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountEmail")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountUserName")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountPassword")]
        public string Password { get; set; }

        [Display(ResourceType = typeof (Labels), Name = "AdminAccountRememberMe")]
        public bool RememberMe { get; set; }
    }

    public class TwoFactorConfirmViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountVerificationCode")]
        public string Code { get; set; }

        [Display(ResourceType = typeof (Labels), Name = "AdminAccountRememberMe")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountUserName")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountPassword")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountConfirmPassword")]
        [Compare("Password", ErrorMessageResourceType = typeof (Messages), ErrorMessageResourceName = "AdminAccountConfirmPasswordNotMatched")]
        public string ConfirmPassword { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountEmail")]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(ResourceType = typeof(Labels), Name = "AdminAccountPhone")]
        public string Phone { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "AdminAccountEnableAccountLock")]
        public bool LockAccountEnable { get; set; }

        [Display(ResourceType = typeof(Labels), Name = "AdminAccountEnableTwoFactorAuthentication")]
        public bool TwoFactorAuthentication { get; set; }
    }

    public class EditUserViewModel
    {
        [Required]
        [Display(ResourceType = typeof(Labels), Name = "AdminAccountUserName")]
        public string UserName { get; set; }

        [Required]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountEmail")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        [UIHint("EmailAddress")]
        public string EmailAddress { get; set; }

        [Display(ResourceType = typeof (Labels), Name = "AdminAccountPhone")]
        [DataType(DataType.PhoneNumber)]
        [Phone]
        public string PhoneNumber { get; set; }

        [Display(ResourceType = typeof (Labels), Name = "AdminAccountEmailConfirm")]
        public bool EmailConfirmed { get; set; }

        [Display(ResourceType = typeof (Labels), Name = "AdminAccountPhoneNumberConfirmed")]
        public bool PhoneConfirmed { get; set; }

        [Display(ResourceType = typeof (Labels), Name = "AdminAccountTwoFactorAuthentication")]
        public bool TwoFactorEnabled { get; set; }

        [Display(ResourceType = typeof (Labels), Name = "AdminAccountAccountLockEnabled")]
        public bool LockoutEnabled { get; set; }

        [Display(ResourceType = typeof (Labels), Name = "AdminAccountLockoutEndDate")]
        public DateTime? LockoutEndDate { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountUserName")]
        public string UserName { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountPassword")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountConfirmPassword")]
        [Compare("Password", ErrorMessageResourceType = typeof (Messages), ErrorMessageResourceName = "AdminAccountConfirmPasswordNotMatched")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountCurrentPassword")]
        public string OldPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountNewPassword")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof (Labels), Name = "AdminAccountConfirmNewPassword")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof (Messages), ErrorMessageResourceName = "AdminAccountNewPasswordAndConfirmIsNotMatched")]
        public string ConfirmPassword { get; set; }
    }

    public class ApplicationUserViewModel
    {
        [Required]
        [Display(ResourceType = typeof(Labels), Name = "AdminAccountUserName")]
        public string UserName { get; set; }
    }
}