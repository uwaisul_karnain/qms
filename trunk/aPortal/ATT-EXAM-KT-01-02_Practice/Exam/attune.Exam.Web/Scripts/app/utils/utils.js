﻿/* 
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($, $enums, $tmpl, $module) {
    /// <summary>
    /// This file contains utility methods to assist development 
    /// </summary>
    /// <param name="$">jQuery</param>
    /// <param name="$enums">Exam enumerations</param>
    /// <param name="$tmpl"> jQuery templating Object</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    $module.showMessage = function (msg, type) {
        /// <summary>
        /// showMessage 
        /// </summary>
        /// <param name="msg">Message to display</param>
        /// <param name="$enums.messageType">Message type </param>

        $("#warMsgBody").html('');

        $("#divCommenMessage,#iCommaenimage").removeAttr("class");

        switch (type) {
            case $enums.messageType.Success:
                {
                    $("#divCommenMessage").addClass("alert alert-success");
                    $("#iCommaenimage").addClass("att att-check-square");
                    $("#spanMassageType").html("Success!");
                }
                break;
            case $enums.messageType.Error:
                {
                    $("#divCommenMessage").addClass("alert alert-danger");
                    $("#iCommaenimage").addClass("att att-ban");
                    $("#spanMassageType").html("Error!");
                }
                break;
            case $enums.messageType.Warning:
                {
                    $("#divCommenMessage").addClass("alert alert-warning alert-block");
                    $("#iCommaenimage").addClass("att att-bell");
                    $("#spanMassageType").html("Warning!");
                }
                break;
            case $.enums.messageType.Info:
                {
                    $("#divCommenMessage").addClass("alert alert-info");
                    $("#iCommaenimage").addClass("att att-info-circle");
                    $("#spanMassageType").html("Important!");
                }
                break;
        }

        $("#warMsgBody").html(msg);
        $("#divCommenMessage").show();
        setTimeout(function () { $("#divCommenMessage").hide(); }, 4000);
        $("#btnmsgc").unbind("click").bind("click", function () {
            $("#divCommenMessage").hide();
        });
    };

    $module.templateProvider = function (type, source, $to) {
        /// <summary>
        /// Build the template form the source
        /// </summary>
        /// <param name="type">$.enums.templates</param>
        /// <param name="source">Data source (object)</param>
        /// <param name="$to">Target control to bind the template</param>
        /// <returns type="Object"> Binded HTML dom object</returns>            
        return $("#" + type).$tmpl(source).appendTo($to);
    };

    $module.scrollToTop = function () {
        /// <summary>
        /// Animated page scroll to top
        /// </summary>
        $('html,body').animate({ scrollTop: 0 }, 1000);
    };

    $module.exteds = function (sub, parent) {
        /// <summary>
        /// Extend the classes
        /// </summary>
        /// <param name="sub">Instance of the sub class</param>
        /// <param name="parent">Instance of the super class</param>
        var sConstructor = parent.toString();
        var aMatch = sConstructor.match(/\s*function (.*)\(/);
        if (aMatch != null)
            sub.prototype[aMatch[1]] = parent;
        for (var m in parent.prototype) {
            if (parent.prototype.hasOwnProperty(m)) {
                sub.prototype[m] = parent.prototype[m];
            }
        }
    };

    $module.serializeToObject = function (content) {
        /// <summary>
        /// Serialize Html Content to Object Model
        /// </summary>
        /// <param name="content">pass html content id which is need to serialize as object</param>
        var o = {};
        var a = content.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $module.isFormValid = function (id) {
        /// <summary>
        /// Get the default form form the document
        /// </summary>
        return $(document.forms[((id) ? id : 0)]).data("kendoValidator").validate();
    };

    $module.getSerializedFormData = function (id) {
        /// <summary>
        /// Get the default form serialized data
        /// </summary> 
        return $(document.forms[((id) ? id : 0)]).serialize();
    };

    $module.getForm = function (id) {
        /// <summary>
        /// Get the form
        /// </summary>
        return $(document.forms[((id) ? id : 0)]);
    };

    $module.showConfirmation = function (title, message, callback) {
        /// <summary>
        /// This medowd wil show a confirmation pop-up and based on user input will send yes or no to callback function
        /// </summary>
        /// <param name="title">Title for Confirmation box</param>
        /// <param name="message">Message to show to user</param>
        /// <param name="callback">callback cunction</param>
        $("#confirmMessage").text("");
        $("#confirmMessage").text(message);
        $("#btnConfirmYes").unbind().click(function () {
            if (callback && typeof (callback) === "function")
                callback('yes');
        });
        $("#btnConfirmNo").unbind().click(function () {
            if (callback && typeof (callback) === "function")
                callback('no');
        });
        $("#confirmBox").modal("show");
    };

    $module.loadMask = function (type, name) {
        /// <summary>
        /// show a loading screen to user
        /// </summary>
        /// <param name="msg">Message to display</param>
        /// <param name="type"> 1 class , 2 id</param>
        /// <param name="name">Name of the class or id</param>
        //type : 1 class
        //type : 2 id
        if (type == 1) {
            //if class
            $('.' + name).prepend('<div class="k-loading-mask" style="width:100%;height:95%"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>');
        } else if (type == 2) {
            //if id
            $('#' + name).prepend('<div class="k-loading-mask" style="width:95%;height:95%"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>');
        }
    };

    $module.destroyMask = function (type, name) {
        /// <summary>
        /// Hide visible loading screen
        /// </summary>
        /// <param name="type"> 1 class , 2 id</param>
        /// <param name="name">Name of the class or id</param>
        if (type == 1) {
            //if class
            $('.' + name).find('.k-loading-mask:first').remove();
        } else if (type == 2) {
            //if id
            $('#' + name).find('.k-loading-mask:first').remove();
        }
    };
    return $module;

}(window.Exam.$, window.Exam.Enums, ["tmpl"], window.Exam.Utils || {}));
