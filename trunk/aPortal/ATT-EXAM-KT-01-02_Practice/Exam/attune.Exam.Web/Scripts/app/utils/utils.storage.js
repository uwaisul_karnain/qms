﻿/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($scope, $module) {
    /// <summary>
    /// Storage utilities
    /// </summary>
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$module">Optional:Current module if require the override</param>
    var $sub = $module.Storage = $module.Storage || {}, $this = {
        instance: function () {
            /// <summary>
            /// Validate and returns the local storage supporte object
            /// </summary>
            /// <returns type="Object">Browser local storage instance</returns>
            if (('localStorage' in window) && window['localStorage'] !== null)
                return window.localStorage;
            return null;
        }
    };

    $sub.set = function (key, value) {
        /// <summary>
        /// Store item in local storage
        /// </summary>
        /// <param name="key" type="string">StorageKey : Needs to deffined in each handler scope</param>
        /// <param name="value" type="Object">Object to store</param>                       
        $this.instance().setItem(key, value);
    };

    $sub.get = function (key) {
        /// <summary>
        /// Get stored item local sotrage by key
        /// </summary>
        /// <param name="key" type="string">StorageKey : Needs to deffined in each handler scope</param>
        /// <returns type="Object"></returns>
        return $this.instance().getItem(key);
    };

    return $module;

}(window.Exam, window.Exam.Utils || {}));
