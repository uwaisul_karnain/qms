﻿/*
* Author: Hasitha Liyanage
* $Date: 06-23-2016
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($scope, $, $module) {
    /// <summary>
    /// Kendo grid common behaviors
    /// </summary>
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$"> jQuery </param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>

    var $sub = $module.Kendo.Grid = $module.Kendo.Grid || {};

    $sub.onDataBound = function (e) {
        /// <summary>
        /// This will add the defualt styling behaviors for grid
        /// </summary>
        /// <param name="e"> Row event </param>
        $("button[data-mode=deactivate],a[data-mode=deactivate]").attr("title", $scope.Messages.commonDeactivateTooltip);
        $("button[data-mode=publish],a[data-mode=publish]").attr("title", $scope.Messages.commonPublishTooltip);
        $("button[data-mode=delete],a[data-mode=delete]").attr("title", $scope.Messages.commonDeleteTooltip);
        $("button[data-mode=edit],a[data-mode=edit]").attr("title", $scope.Messages.commonEditTooltip);
        $("button[data-mode=view],a[data-mode=view]").attr("title", $scope.Messages.commonViewTooltip);
        $(".k-grid-edit").attr("title", $scope.Messages.commonEditTooltip);
    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils || {}));

