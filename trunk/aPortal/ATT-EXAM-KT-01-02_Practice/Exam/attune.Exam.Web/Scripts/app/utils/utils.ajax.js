﻿/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($scope, $, $module) {
    /// <summary>
    /// Ajax request extention for utils
    /// </summary>
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$"> jQuery </param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>

    var  $sub = $module.Ajax = $module.Ajax || {};

    $sub.get = function (url, data, onSuccess, onError) {
        /// <summary>
        /// GET request
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="params">Data</param>
        /// <param name="onSuccess">Success callbak</param>
        /// <param name="onError">Error callback</param>
        /// <returns type=""></returns>
        $.ajax({
            url: url,
            type: 'GET',
            async: true,
            contentType: 'application/json',
            dataType: 'json',
            data: data,
            success: onSuccess,
            error: function (xhr, e) { 
                $.ajaxSetup().error(xhr, e);
                if ($.isFunction(onError))
                    onError();
            }
        });
    };

    $sub.post = function (url, data, onSuccess, onError) {
        /// <summary>
        /// POST request
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="params">Data</param>
        /// <param name="onSuccess">Success callbak</param>
        /// <param name="onError">Error callback</param>
        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            success: onSuccess,
            error: function (xhr, e) {
                $.ajaxSetup().error(xhr, e);
                if ($.isFunction(onError))
                    onError();
            }
        });
    };

    $sub.getHtml = function (url, data, onSuccess, onError) {
        /// <summary>
        /// Get HTML form the request
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="params">Data</param>
        /// <param name="onSuccess">Success callbak</param>
        /// <param name="onError">Error callback</param>
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: "html",
            async: true,
            success: onSuccess,
            error: function (xhr, e) {
                $.ajaxSetup().error(xhr, e);
                if ($.isFunction(onError))
                    onError();
            }
        });
    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils || {}));

