﻿/*
* Author: Hasitha Liyanage
* $Date: 06-23-2016
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($scope, $, $module) {
    /// <summary>
    /// Kendo UI controls common behaviors
    /// </summary>
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$"> jQuery </param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>

    var $sub = $module.Kendo = $module.Kendo || {};


    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils || {}));

