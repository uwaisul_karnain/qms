﻿/*
* Author: Hasitha Liyanage
* $Date: 11-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Services = (function ($scope, $, $utils, $api, $module) {
    /// <summary>
    /// User Service Handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$api">Data api</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    var $this = this, $sub = $module.User = $module.User || {};
    $sub.sample = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.sampleService, data, onSuccess || function () {

        }, onError || function () {

        });
    };


    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Configs.Apis.User, window.Exam.Services || {}));
