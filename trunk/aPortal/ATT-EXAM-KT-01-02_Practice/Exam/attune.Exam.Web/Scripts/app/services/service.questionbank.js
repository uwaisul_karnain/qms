﻿/*
* Author: Hasitha Liyanage
* $Date: 11-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Services = (function ($scope, $, $utils, $api, $module) {
    /// <summary>
    /// QuestionBank Service Handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$api">Data api</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    var $this = this, $sub = $module.QuestionBank = $module.QuestionBank || {};
    $sub.onDeactivate = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.onQuestionDeactivate, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onPublish = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.onQuestionPublish, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onDownLoadTemplate = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.get($api.OnQuestionDownLoadTemplate, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.createQuestion = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.createQuestion, data, onSuccess || function () {
           
        }, onError || function () {

        });
    };

    $sub.getExamTypes = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.getExamTypes, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.getCategories = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.getCategories, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    
    $sub.onExamTypeDelete = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.deleteExamType, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onCategoryDelete = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.deleteCategory, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Configs.Apis.QuestionBank, window.Exam.Services || {}));
