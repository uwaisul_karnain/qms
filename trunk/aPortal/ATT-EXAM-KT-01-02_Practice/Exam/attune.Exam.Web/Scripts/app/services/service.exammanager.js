﻿/*
* Author: Hasitha Liyanage
* $Date: 11-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Services = (function ($scope, $, $utils, $api, $module) {
    /// <summary>
    /// QuestionBank Service Handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$api">Data api</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    var $this = this, $sub = $module.ExamManager = $module.ExamManager || {};
    $sub.onExamDeactivate = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.onExamDeactivate, data, onSuccess || function () {

        }, onError || function () {

        });
    };

   
    $sub.createExam = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.createExam, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onCategoryDropDownRead = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.get($api.onCategoryDropDownRead, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    
    $sub.onExamQuestionCategoryView = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onExamQuestionCategoryView, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Configs.Apis.ExamManager, window.Exam.Services || {}));
