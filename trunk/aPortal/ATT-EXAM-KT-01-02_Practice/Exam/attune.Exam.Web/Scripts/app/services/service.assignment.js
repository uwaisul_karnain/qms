﻿/*
* Author: Hasitha Liyanage
* $Date: 11-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Services = (function ($scope, $, $utils, $api, $module) {
    /// <summary>
    /// QuestionBank Service Handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$api">Data api</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    var $this = this, $sub = $module.Assignment = $module.Assignment || {};
    $sub.onDeactivate = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.onQuestionDeactivate, data, onSuccess || function () {

        }, onError || function () {

        });
    };

   

    $sub.onAssignmentView = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onAssignmentView, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onNonEmployeeSelection = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onNonEmployeeSelection, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onEmployeeSelection = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onEmployeeSelection, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    
    $sub.onCandidateAssignment = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onCandidateAssignment, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Configs.Apis.Assignment, window.Exam.Services || {}));
