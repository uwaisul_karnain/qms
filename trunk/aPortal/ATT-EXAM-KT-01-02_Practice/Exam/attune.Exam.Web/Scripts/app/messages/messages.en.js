﻿/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------------------------------------------------
* $Rev: 001 $
*/

window.Exam.Messages = (function ($scope, $module) {
    /// <summary>
    /// Messages declarations
    /// </summary>    
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$module">Optional:Current module if require the override</param>
    $module.en = {
        _culture: "en",
        success: "Records successfully updated",
        failed: "Records update failed",
        fileFormatWrong: "File format is wrong",
        answerLimit: "Cannot add more than 10 answers",
        answerCannotBeNull: 'Atleast one answer is required',
        correctAnswerisRequired: 'Atleast one answer should be correct',
        moreThanOneAnswerCannotBeCorrect: 'More than one answer cannot be correct',
        noRecordsFound: "No records Found",
        selectExamType: "Please select exam type",
        selectQuestionPickingMode: "Please select question picking mode",
        addQuestionCategories: "Please add question categories",
        recordAlreadyExist: "Record already exist",
        questionCountExceeded: "Question count exceeded",
        deleteRecordsPopupTitle: "Delete Records",
        deactivateRecordsPopupTitle: "Deactivate Records",
        deactivateRecordsPopupContent: "Are you sure you want to deactivate this record",
        selectCategory: "Please select category",
        deleteRecordsCommonPopupTitle:"Delete Records",
        deleteRecordsCommonPopupContent: "Are you sure you want to delete this record?",
        publishQuestionPopupTitle: "Publish Question",
        publishQuestionPopupContent: "Are you sure you want to publish this record",
        ajax: {
            status_0: "Not connect.\n Verify Network.",
            status_400: "Bad request.",
            status_401: "Unauthorized. [401].",
            status_404: "Requested page not found. [404].",
            status_500: "Internal Server Error [500].",
            status_parsererror: "Requested JSON parse failed.",
            status_timeout: "Time out error.",
            status_abort: "Ajax request aborted.",
            status_uncaught: "Uncaught Error.\n'"
        }
    };
    return $module;

}(window.Exam, window.Exam.Messages || {}));