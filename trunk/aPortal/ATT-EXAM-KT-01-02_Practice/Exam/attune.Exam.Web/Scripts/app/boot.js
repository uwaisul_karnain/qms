﻿/*  
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------------------------------------------------
* $Rev: 001 $
*/
window.Exam = (function ($scope) {
    /// <summary>
    /// Application bootup 
    /// </summary>
    /// <param name="$scope"> Application Scope </param>    
    $scope.boot = function() {

        $scope.Configs.App.init(function () {
            /// <summary>
            /// Initialize handlers based on the controller execution
            /// </summary>
            /// <returns type=""></returns>
            if ($scope.PageMode.controller === "QuestionBank" && $scope.PageMode.action === "Index")
                $scope.Handlers.QuestionBank.init();
            if ($scope.PageMode.controller === "QuestionBank" && $scope.PageMode.action === "Create")
                $scope.Handlers.QuestionBank.init();
            if ($scope.PageMode.controller === "QuestionBank" && $scope.PageMode.action === "Edit")
                $scope.Handlers.QuestionBank.init();
            if ($scope.PageMode.controller === "Exam" && $scope.PageMode.action === "Index")
                $scope.Handlers.ExamManager.init();
            if ($scope.PageMode.controller === "Exam" && $scope.PageMode.action === "Create" )
                $scope.Handlers.ExamManager.init();
            if ($scope.PageMode.controller === "Exam" && $scope.PageMode.action === "Edit") 
                $scope.Handlers.ExamManager.init();
            if ($scope.PageMode.controller === "Assignment" && $scope.PageMode.action === "Index") {
                $scope.Handlers.Assignment.init();
                $scope.Handlers.QuestionBank.init();
            }

        });

        return true;
    }();
    
    return $scope;

}(window.Exam));

 