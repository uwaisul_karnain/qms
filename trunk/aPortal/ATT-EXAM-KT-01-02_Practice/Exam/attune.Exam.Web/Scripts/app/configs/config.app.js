﻿/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $ Added lost focus events to clear up validation tolltip for validation enable controls
*/
window.Exam.Configs.App = (function ($scope, $, $utils, $messages, $module) {
    /// <summary>
    /// Gloabal applicationa setup
    /// </summary>    
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$"> jQuery </param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$messages"> Messages </param>
    /// <param name="$module">Optional:Current module if require the override</param>
    $module.init = function(fn) {
        /// <summary>
        /// Module initilization
        /// </summary>
        $.ajaxSetup({
            error: function($xhr, exception) {
                /// <summary>
                /// Global request error. If required handle behaviors for specific erros
                /// </summary>
                /// <param name="jqXHR">HTTP request</param>
                /// <param name="exceptixon">Applicatoin exception</param>
                if ($xhr.status === 0) {
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                } else if ($xhr.status === 400)
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                else if ($xhr.status === 401) {
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else if ($xhr.status === 404)
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                else if ($xhr.status === 500)
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                else if (exception === 'parsererror')
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + exception], $scope.Enums.messageType.Error);
                else if (exception === 'timeout')
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + exception], $scope.Enums.messageType.Error);
                else if (exception === 'abort')
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + exception], $scope.Enums.messageType.Error);
                else
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + exception] + $xhr.responseText);
            }
        });


        var $form = $($utils.getForm()), $validator = $form.data("kendoValidator");
        if (!$validator) {
            $form.kendoValidator({
                rules: {
                    dategreaterthan: function(input) {
                        if (input.is("[data-val-dategreaterthan]") && input.val() !== "") {
                            var date = kendo.parseDate(input.val()),
                                otherDate = kendo.parseDate($("[name='" + input.data("valDategreaterthanField") + "']").val());
                            return otherDate == null || otherDate.getTime() < date.getTime();
                        }

                        return true;
                    },
                    itemselected: function (input) {
                        if (input.is("[data-val-itemselected]") && kendo.parseInt(input.val()) === kendo.parseInt(input.data("valItemselectedField"))) {
                            return false;
                        }
                        return true;
                    },
                    multipleitemselected: function (input) {
                        if (input.is("[data-val-multipleitemselected]")) {
                            if (kendo.parseInt(input.val()) === kendo.parseInt(input.data("valMultipleitemselectedField")))
                                return false;
                            if (!input.val().toLowerCase().indexOf(input.data("valMultipleitemselectedMultitext").toLowerCase()) > 0)
                                return false;
                        }
                        return true;
                    }
                },
                messages: {
                    dategreaterthan: function(input) {
                        return input.data("valDategreaterthan");
                    },
                    itemselected: function(input) {
                        return input.data("valItemselected");
                    },
                    multipleitemselected: function (input) {
                        return input.data("valMultipleItemselected");
                    }
                }
            });

            $validator = $form.data("kendoValidator");

            $form.find("[data-val=true]").change(function () {
                $validator.validate();
            }).keyup(function () {
                $validator.validate();
            });
        }

        $(document).ready(function() {
            $module.registerLayoutBehaviors();

            if ($.isFunction(fn))
                fn();
        });
    };

    $module.registerLayoutBehaviors = function() {
        // TODO : Configure defualt layout behaviors

        var removeClass = true;
        $("#menu").click(function() {
            setTimeout(function() {
                $(".navigate").toggleClass("slid-nav");
                removeClass = false;
            }, 200);
        });
        $(".navigate").click(function() {
            removeClass = false;
        });
        $("html").click(function() {
            if (removeClass)
                $(".navigate").removeClass("slid-nav");
            removeClass = true;
        });
        $('.employ-filters p').click(function (e) {
            

            $('.employ-filters').toggleClass("show");
        });
    };

    return $module;
}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Messages, window.Exam.Configs.App || {}));