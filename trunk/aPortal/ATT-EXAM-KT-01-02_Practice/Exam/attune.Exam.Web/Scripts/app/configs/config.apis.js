﻿/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Configs.Apis = (function($scope, $module) {
    /// <summary>
    /// APIs configurations
    /// </summary>    
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$module">Optional:Current module if require the override</param>

    $module.User = {
        sampleService: "/Manage/sampleService",
    };
    $module.QuestionBank = {
        onQuestionDeactivate: "/Master/QuestionBank/OnQuestionDeactivate",
        onQuestionPublish: "/Master/QuestionBank/OnQuestionPublish",
        OnQuestionDownLoadTemplate: "/Master/QuestionBank/OnQuestionDownLoadTemplate",
        excelTemplateDownload: "/Master/QuestionBank/Download",
        OnQuestionUpload: "/Master/QuestionBank/OnQuestionUpload",
        createQuestion: "/Master/QuestionBank/Create",
        editQuestion: "/Master/QuestionBank/Edit",
        questionList: "/Master/QuestionBank/Index",
        getExamTypes: "/Master/QuestionBank/ExamType",
        getCategories: "/Master/QuestionBank/Category",
        deleteExamType: "/Master/QuestionBank/OnExamTypeGridDestroy",
        deleteCategory: "/Master/QuestionBank/OnCategoryGridDestroy"
    };
    $module.ExamManager = {
        onExamDeactivate: "/Master/Exam/OnExamDeactivate",
        onCategoryDropDownRead: "/Master/Exam/OnCategoryRead",
        onExamQuestionCategoryView: "/Master/Exam/QuestionSelection",
        createExam: "/Master/Exam/Create",
        updateExam: "/Master/Exam/Edit",
        examList: "/Master/Exam/Index"
    };

    $module.Assignment = {
        onAssignmentView: "/Assignment/Assignment/AssignExam",
        onEmployeeSelection: "/Assignment/Assignment/EmployeeSelection",
        onNonEmployeeSelection: "/Assignment/Assignment/NonEmployeeSelection",
        onCandidateAssignment: "/Assignment/Assignment/CandidateAssignment",
        onAssignExamSchedule: "/Assignment/Assignment/AssignExamSchedule"
    };

    return $module;
}(window.Exam, window.Exam.Configs.Apis || {}));
