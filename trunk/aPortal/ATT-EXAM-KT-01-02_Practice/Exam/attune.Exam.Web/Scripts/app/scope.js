﻿/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------------------------------------------------
* $Rev: 001 $
*/
(function (w, $) {
    /// <summary>
    ///  Core application namespaces 
    /// </summary>
    /// <param name="w" type="Object">DOM window object</param>
    /// <param name="$" type="Object">jQuery object</param>
    /*
        Global scope namespaces for applicaion deffinitions 
    */
    w.Exam = w.Exam || {};
    w.Exam = {
        version: "1.0.0",
        culture: "en",
        $: $,        
        Configs: { Apis: {}, App: {} },
        Handlers: {},
        Services: {},
        Utils: {},
        Enums: {
            viewport: {
                web: 0,
                mobile: 1
            },
            messageType: {
                Warning: 1,
                Error: 2,
                Success: 3,
                Info: 4
            },
            questionType: {
                MultipleDropDown : 1,
                MultipleRadioButton : 2,
                MultipleCorrect : 3,
                YesOrNo : 4,
                TrueOrFalse : 5
            },
            action: {
                View: 1,
                Edit: 2
            },
            mode: {
                Edit: 1,
                Add: 2
            }
        },
        PageMode : {
            controller: "",
            action: ""
        }
    };
}(window, jQuery));

