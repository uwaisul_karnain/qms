﻿///#source 1 1 /Scripts/app/copyright.txt
/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
///#source 1 1 /Scripts/app/scope.js
/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------------------------------------------------
* $Rev: 001 $
*/
(function (w, $) {
    /// <summary>
    ///  Core application namespaces 
    /// </summary>
    /// <param name="w" type="Object">DOM window object</param>
    /// <param name="$" type="Object">jQuery object</param>
    /*
        Global scope namespaces for applicaion deffinitions 
    */
    w.Exam = w.Exam || {};
    w.Exam = {
        version: "1.0.0",
        culture: "en",
        $: $,        
        Configs: { Apis: {}, App: {} },
        Handlers: {},
        Services: {},
        Utils: {},
        Enums: {
            viewport: {
                web: 0,
                mobile: 1
            },
            messageType: {
                Warning: 1,
                Error: 2,
                Success: 3,
                Info: 4
            },
            questionType: {
                MultipleDropDown : 1,
                MultipleRadioButton : 2,
                MultipleCorrect : 3,
                YesOrNo : 4,
                TrueOrFalse : 5
            },
            action: {
                View: 1,
                Edit: 2
            },
            mode: {
                Edit: 1,
                Add: 2
            }
        },
        PageMode : {
            controller: "",
            action: ""
        }
    };
}(window, jQuery));


///#source 1 1 /Scripts/app/messages/messages.en.js
/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------------------------------------------------
* $Rev: 001 $
*/

window.Exam.Messages = (function ($scope, $module) {
    /// <summary>
    /// Messages declarations
    /// </summary>    
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$module">Optional:Current module if require the override</param>
    $module.en = {
        _culture: "en",
        success: "Records successfully updated",
        failed: "Records update failed",
        fileFormatWrong: "File format is wrong",
        answerLimit: "Cannot add more than 10 answers",
        answerCannotBeNull: 'Atleast one answer is required',
        correctAnswerisRequired: 'Atleast one answer should be correct',
        moreThanOneAnswerCannotBeCorrect: 'More than one answer cannot be correct',
        noRecordsFound: "No records Found",
        selectExamType: "Please select exam type",
        selectQuestionPickingMode: "Please select question picking mode",
        addQuestionCategories: "Please add question categories",
        recordAlreadyExist: "Record already exist",
        questionCountExceeded: "Question count exceeded",
        deleteRecordsPopupTitle: "Delete Records",
        deactivateRecordsPopupTitle: "Deactivate Records",
        deactivateRecordsPopupContent: "Are you sure you want to deactivate this record",
        selectCategory: "Please select category",
        deleteRecordsCommonPopupTitle:"Delete Records",
        deleteRecordsCommonPopupContent: "Are you sure you want to delete this record?",
        publishQuestionPopupTitle: "Publish Question",
        publishQuestionPopupContent: "Are you sure you want to publish this record",
        ajax: {
            status_0: "Not connect.\n Verify Network.",
            status_400: "Bad request.",
            status_401: "Unauthorized. [401].",
            status_404: "Requested page not found. [404].",
            status_500: "Internal Server Error [500].",
            status_parsererror: "Requested JSON parse failed.",
            status_timeout: "Time out error.",
            status_abort: "Ajax request aborted.",
            status_uncaught: "Uncaught Error.\n'"
        }
    };
    return $module;

}(window.Exam, window.Exam.Messages || {}));
///#source 1 1 /Scripts/app/utils/utils.js
/* 
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($, $enums, $tmpl, $module) {
    /// <summary>
    /// This file contains utility methods to assist development 
    /// </summary>
    /// <param name="$">jQuery</param>
    /// <param name="$enums">Exam enumerations</param>
    /// <param name="$tmpl"> jQuery templating Object</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    $module.showMessage = function (msg, type) {
        /// <summary>
        /// showMessage 
        /// </summary>
        /// <param name="msg">Message to display</param>
        /// <param name="$enums.messageType">Message type </param>

        $("#warMsgBody").html('');

        $("#divCommenMessage,#iCommaenimage").removeAttr("class");

        switch (type) {
            case $enums.messageType.Success:
                {
                    $("#divCommenMessage").addClass("alert alert-success");
                    $("#iCommaenimage").addClass("att att-check-square");
                    $("#spanMassageType").html("Success!");
                }
                break;
            case $enums.messageType.Error:
                {
                    $("#divCommenMessage").addClass("alert alert-danger");
                    $("#iCommaenimage").addClass("att att-ban");
                    $("#spanMassageType").html("Error!");
                }
                break;
            case $enums.messageType.Warning:
                {
                    $("#divCommenMessage").addClass("alert alert-warning alert-block");
                    $("#iCommaenimage").addClass("att att-bell");
                    $("#spanMassageType").html("Warning!");
                }
                break;
            case $.enums.messageType.Info:
                {
                    $("#divCommenMessage").addClass("alert alert-info");
                    $("#iCommaenimage").addClass("att att-info-circle");
                    $("#spanMassageType").html("Important!");
                }
                break;
        }

        $("#warMsgBody").html(msg);
        $("#divCommenMessage").show();
        setTimeout(function () { $("#divCommenMessage").hide(); }, 4000);
        $("#btnmsgc").unbind("click").bind("click", function () {
            $("#divCommenMessage").hide();
        });
    };

    $module.templateProvider = function (type, source, $to) {
        /// <summary>
        /// Build the template form the source
        /// </summary>
        /// <param name="type">$.enums.templates</param>
        /// <param name="source">Data source (object)</param>
        /// <param name="$to">Target control to bind the template</param>
        /// <returns type="Object"> Binded HTML dom object</returns>            
        return $("#" + type).$tmpl(source).appendTo($to);
    };

    $module.scrollToTop = function () {
        /// <summary>
        /// Animated page scroll to top
        /// </summary>
        $('html,body').animate({ scrollTop: 0 }, 1000);
    };

    $module.exteds = function (sub, parent) {
        /// <summary>
        /// Extend the classes
        /// </summary>
        /// <param name="sub">Instance of the sub class</param>
        /// <param name="parent">Instance of the super class</param>
        var sConstructor = parent.toString();
        var aMatch = sConstructor.match(/\s*function (.*)\(/);
        if (aMatch != null)
            sub.prototype[aMatch[1]] = parent;
        for (var m in parent.prototype) {
            if (parent.prototype.hasOwnProperty(m)) {
                sub.prototype[m] = parent.prototype[m];
            }
        }
    };

    $module.serializeToObject = function (content) {
        /// <summary>
        /// Serialize Html Content to Object Model
        /// </summary>
        /// <param name="content">pass html content id which is need to serialize as object</param>
        var o = {};
        var a = content.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    $module.isFormValid = function (id) {
        /// <summary>
        /// Get the default form form the document
        /// </summary>
        return $(document.forms[((id) ? id : 0)]).data("kendoValidator").validate();
    };

    $module.getSerializedFormData = function (id) {
        /// <summary>
        /// Get the default form serialized data
        /// </summary> 
        return $(document.forms[((id) ? id : 0)]).serialize();
    };

    $module.getForm = function (id) {
        /// <summary>
        /// Get the form
        /// </summary>
        return $(document.forms[((id) ? id : 0)]);
    };

    $module.showConfirmation = function (title, message, callback) {
        /// <summary>
        /// This medowd wil show a confirmation pop-up and based on user input will send yes or no to callback function
        /// </summary>
        /// <param name="title">Title for Confirmation box</param>
        /// <param name="message">Message to show to user</param>
        /// <param name="callback">callback cunction</param>
        $("#confirmMessage").text("");
        $("#confirmMessage").text(message);
        $("#btnConfirmYes").unbind().click(function () {
            if (callback && typeof (callback) === "function")
                callback('yes');
        });
        $("#btnConfirmNo").unbind().click(function () {
            if (callback && typeof (callback) === "function")
                callback('no');
        });
        $("#confirmBox").modal("show");
    };

    $module.loadMask = function (type, name) {
        /// <summary>
        /// show a loading screen to user
        /// </summary>
        /// <param name="msg">Message to display</param>
        /// <param name="type"> 1 class , 2 id</param>
        /// <param name="name">Name of the class or id</param>
        //type : 1 class
        //type : 2 id
        if (type == 1) {
            //if class
            $('.' + name).prepend('<div class="k-loading-mask" style="width:100%;height:95%"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>');
        } else if (type == 2) {
            //if id
            $('#' + name).prepend('<div class="k-loading-mask" style="width:95%;height:95%"><span class="k-loading-text">Loading...</span><div class="k-loading-image"><div class="k-loading-color"></div></div></div>');
        }
    };

    $module.destroyMask = function (type, name) {
        /// <summary>
        /// Hide visible loading screen
        /// </summary>
        /// <param name="type"> 1 class , 2 id</param>
        /// <param name="name">Name of the class or id</param>
        if (type == 1) {
            //if class
            $('.' + name).find('.k-loading-mask:first').remove();
        } else if (type == 2) {
            //if id
            $('#' + name).find('.k-loading-mask:first').remove();
        }
    };
    return $module;

}(window.Exam.$, window.Exam.Enums, ["tmpl"], window.Exam.Utils || {}));

///#source 1 1 /Scripts/app/utils/utils.ajax.js
/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($scope, $, $module) {
    /// <summary>
    /// Ajax request extention for utils
    /// </summary>
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$"> jQuery </param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>

    var  $sub = $module.Ajax = $module.Ajax || {};

    $sub.get = function (url, data, onSuccess, onError) {
        /// <summary>
        /// GET request
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="params">Data</param>
        /// <param name="onSuccess">Success callbak</param>
        /// <param name="onError">Error callback</param>
        /// <returns type=""></returns>
        $.ajax({
            url: url,
            type: 'GET',
            async: true,
            contentType: 'application/json',
            dataType: 'json',
            data: data,
            success: onSuccess,
            error: function (xhr, e) { 
                $.ajaxSetup().error(xhr, e);
                if ($.isFunction(onError))
                    onError();
            }
        });
    };

    $sub.post = function (url, data, onSuccess, onError) {
        /// <summary>
        /// POST request
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="params">Data</param>
        /// <param name="onSuccess">Success callbak</param>
        /// <param name="onError">Error callback</param>
        $.ajax({
            url: url,
            type: "POST",
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: 'json',
            async: true,
            success: onSuccess,
            error: function (xhr, e) {
                $.ajaxSetup().error(xhr, e);
                if ($.isFunction(onError))
                    onError();
            }
        });
    };

    $sub.getHtml = function (url, data, onSuccess, onError) {
        /// <summary>
        /// Get HTML form the request
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="params">Data</param>
        /// <param name="onSuccess">Success callbak</param>
        /// <param name="onError">Error callback</param>
        $.ajax({
            url: url,
            type: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json',
            dataType: "html",
            async: true,
            success: onSuccess,
            error: function (xhr, e) {
                $.ajaxSetup().error(xhr, e);
                if ($.isFunction(onError))
                    onError();
            }
        });
    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils || {}));


///#source 1 1 /Scripts/app/utils/utils.storage.js
/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($scope, $module) {
    /// <summary>
    /// Storage utilities
    /// </summary>
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$module">Optional:Current module if require the override</param>
    var $sub = $module.Storage = $module.Storage || {}, $this = {
        instance: function () {
            /// <summary>
            /// Validate and returns the local storage supporte object
            /// </summary>
            /// <returns type="Object">Browser local storage instance</returns>
            if (('localStorage' in window) && window['localStorage'] !== null)
                return window.localStorage;
            return null;
        }
    };

    $sub.set = function (key, value) {
        /// <summary>
        /// Store item in local storage
        /// </summary>
        /// <param name="key" type="string">StorageKey : Needs to deffined in each handler scope</param>
        /// <param name="value" type="Object">Object to store</param>                       
        $this.instance().setItem(key, value);
    };

    $sub.get = function (key) {
        /// <summary>
        /// Get stored item local sotrage by key
        /// </summary>
        /// <param name="key" type="string">StorageKey : Needs to deffined in each handler scope</param>
        /// <returns type="Object"></returns>
        return $this.instance().getItem(key);
    };

    return $module;

}(window.Exam, window.Exam.Utils || {}));

///#source 1 1 /Scripts/app/utils/utils.kendo.js
/*
* Author: Hasitha Liyanage
* $Date: 06-23-2016
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($scope, $, $module) {
    /// <summary>
    /// Kendo UI controls common behaviors
    /// </summary>
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$"> jQuery </param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>

    var $sub = $module.Kendo = $module.Kendo || {};


    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils || {}));


///#source 1 1 /Scripts/app/utils/utils.kendo.grid.js
/*
* Author: Hasitha Liyanage
* $Date: 06-23-2016
-----------------------------------------------
* $Rev: 001 $
*/

window.Exam.Utils = (function ($scope, $, $module) {
    /// <summary>
    /// Kendo grid common behaviors
    /// </summary>
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$"> jQuery </param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>

    var $sub = $module.Kendo.Grid = $module.Kendo.Grid || {};

    $sub.onDataBound = function (e) {
        /// <summary>
        /// This will add the defualt styling behaviors for grid
        /// </summary>
        /// <param name="e"> Row event </param>
        $("button[data-mode=deactivate],a[data-mode=deactivate]").attr("title", $scope.Messages.commonDeactivateTooltip);
        $("button[data-mode=publish],a[data-mode=publish]").attr("title", $scope.Messages.commonPublishTooltip);
        $("button[data-mode=delete],a[data-mode=delete]").attr("title", $scope.Messages.commonDeleteTooltip);
        $("button[data-mode=edit],a[data-mode=edit]").attr("title", $scope.Messages.commonEditTooltip);
        $("button[data-mode=view],a[data-mode=view]").attr("title", $scope.Messages.commonViewTooltip);
        $(".k-grid-edit").attr("title", $scope.Messages.commonEditTooltip);
    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils || {}));


///#source 1 1 /Scripts/app/configs/config.apis.js
/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Configs.Apis = (function($scope, $module) {
    /// <summary>
    /// APIs configurations
    /// </summary>    
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$module">Optional:Current module if require the override</param>

    $module.User = {
        sampleService: "/Manage/sampleService",
    };
    $module.QuestionBank = {
        onQuestionDeactivate: "/Master/QuestionBank/OnQuestionDeactivate",
        onQuestionPublish: "/Master/QuestionBank/OnQuestionPublish",
        OnQuestionDownLoadTemplate: "/Master/QuestionBank/OnQuestionDownLoadTemplate",
        excelTemplateDownload: "/Master/QuestionBank/Download",
        OnQuestionUpload: "/Master/QuestionBank/OnQuestionUpload",
        createQuestion: "/Master/QuestionBank/Create",
        editQuestion: "/Master/QuestionBank/Edit",
        questionList: "/Master/QuestionBank/Index",
        getExamTypes: "/Master/QuestionBank/ExamType",
        getCategories: "/Master/QuestionBank/Category",
        deleteExamType: "/Master/QuestionBank/OnExamTypeGridDestroy",
        deleteCategory: "/Master/QuestionBank/OnCategoryGridDestroy"
    };
    $module.ExamManager = {
        onExamDeactivate: "/Master/Exam/OnExamDeactivate",
        onCategoryDropDownRead: "/Master/Exam/OnCategoryRead",
        onExamQuestionCategoryView: "/Master/Exam/QuestionSelection",
        createExam: "/Master/Exam/Create",
        updateExam: "/Master/Exam/Edit",
        examList: "/Master/Exam/Index"
    };

    $module.Assignment = {
        onAssignmentView: "/Assignment/Assignment/AssignExam",
        onEmployeeSelection: "/Assignment/Assignment/EmployeeSelection",
        onNonEmployeeSelection: "/Assignment/Assignment/NonEmployeeSelection",
        onCandidateAssignment: "/Assignment/Assignment/CandidateAssignment",
        onAssignExamSchedule: "/Assignment/Assignment/AssignExamSchedule"
    };

    return $module;
}(window.Exam, window.Exam.Configs.Apis || {}));

///#source 1 1 /Scripts/app/configs/config.app.js
/*
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------
* $Rev: 001 $ Added lost focus events to clear up validation tolltip for validation enable controls
*/
window.Exam.Configs.App = (function ($scope, $, $utils, $messages, $module) {
    /// <summary>
    /// Gloabal applicationa setup
    /// </summary>    
    /// <param name="$scope"> Application Scope </param>
    /// <param name="$"> jQuery </param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$messages"> Messages </param>
    /// <param name="$module">Optional:Current module if require the override</param>
    $module.init = function(fn) {
        /// <summary>
        /// Module initilization
        /// </summary>
        $.ajaxSetup({
            error: function($xhr, exception) {
                /// <summary>
                /// Global request error. If required handle behaviors for specific erros
                /// </summary>
                /// <param name="jqXHR">HTTP request</param>
                /// <param name="exceptixon">Applicatoin exception</param>
                if ($xhr.status === 0) {
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                } else if ($xhr.status === 400)
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                else if ($xhr.status === 401) {
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                    setTimeout(function() {
                        location.reload();
                    }, 2000);
                } else if ($xhr.status === 404)
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                else if ($xhr.status === 500)
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + $xhr.status], $scope.Enums.messageType.Error);
                else if (exception === 'parsererror')
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + exception], $scope.Enums.messageType.Error);
                else if (exception === 'timeout')
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + exception], $scope.Enums.messageType.Error);
                else if (exception === 'abort')
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + exception], $scope.Enums.messageType.Error);
                else
                    $utils.showMessage($messages[$scope.culture].ajax["status_" + exception] + $xhr.responseText);
            }
        });


        var $form = $($utils.getForm()), $validator = $form.data("kendoValidator");
        if (!$validator) {
            $form.kendoValidator({
                rules: {
                    dategreaterthan: function(input) {
                        if (input.is("[data-val-dategreaterthan]") && input.val() !== "") {
                            var date = kendo.parseDate(input.val()),
                                otherDate = kendo.parseDate($("[name='" + input.data("valDategreaterthanField") + "']").val());
                            return otherDate == null || otherDate.getTime() < date.getTime();
                        }

                        return true;
                    },
                    itemselected: function (input) {
                        if (input.is("[data-val-itemselected]") && kendo.parseInt(input.val()) === kendo.parseInt(input.data("valItemselectedField"))) {
                            return false;
                        }
                        return true;
                    },
                    multipleitemselected: function (input) {
                        if (input.is("[data-val-multipleitemselected]")) {
                            if (kendo.parseInt(input.val()) === kendo.parseInt(input.data("valMultipleitemselectedField")))
                                return false;
                            if (!input.val().toLowerCase().indexOf(input.data("valMultipleitemselectedMultitext").toLowerCase()) > 0)
                                return false;
                        }
                        return true;
                    }
                },
                messages: {
                    dategreaterthan: function(input) {
                        return input.data("valDategreaterthan");
                    },
                    itemselected: function(input) {
                        return input.data("valItemselected");
                    },
                    multipleitemselected: function (input) {
                        return input.data("valMultipleItemselected");
                    }
                }
            });

            $validator = $form.data("kendoValidator");

            $form.find("[data-val=true]").change(function () {
                $validator.validate();
            }).keyup(function () {
                $validator.validate();
            });
        }

        $(document).ready(function() {
            $module.registerLayoutBehaviors();

            if ($.isFunction(fn))
                fn();
        });
    };

    $module.registerLayoutBehaviors = function() {
        // TODO : Configure defualt layout behaviors

        var removeClass = true;
        $("#menu").click(function() {
            setTimeout(function() {
                $(".navigate").toggleClass("slid-nav");
                removeClass = false;
            }, 200);
        });
        $(".navigate").click(function() {
            removeClass = false;
        });
        $("html").click(function() {
            if (removeClass)
                $(".navigate").removeClass("slid-nav");
            removeClass = true;
        });
        $('.employ-filters p').click(function (e) {
            

            $('.employ-filters').toggleClass("show");
        });
    };

    return $module;
}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Messages, window.Exam.Configs.App || {}));
///#source 1 1 /Scripts/app/services/service.user.js
/*
* Author: Hasitha Liyanage
* $Date: 11-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Services = (function ($scope, $, $utils, $api, $module) {
    /// <summary>
    /// User Service Handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$api">Data api</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    var $this = this, $sub = $module.User = $module.User || {};
    $sub.sample = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.sampleService, data, onSuccess || function () {

        }, onError || function () {

        });
    };


    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Configs.Apis.User, window.Exam.Services || {}));

///#source 1 1 /Scripts/app/services/service.questionbank.js
/*
* Author: Hasitha Liyanage
* $Date: 11-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Services = (function ($scope, $, $utils, $api, $module) {
    /// <summary>
    /// QuestionBank Service Handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$api">Data api</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    var $this = this, $sub = $module.QuestionBank = $module.QuestionBank || {};
    $sub.onDeactivate = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.onQuestionDeactivate, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onPublish = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.onQuestionPublish, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onDownLoadTemplate = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.get($api.OnQuestionDownLoadTemplate, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.createQuestion = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.createQuestion, data, onSuccess || function () {
           
        }, onError || function () {

        });
    };

    $sub.getExamTypes = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.getExamTypes, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.getCategories = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.getCategories, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    
    $sub.onExamTypeDelete = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.deleteExamType, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onCategoryDelete = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.deleteCategory, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Configs.Apis.QuestionBank, window.Exam.Services || {}));

///#source 1 1 /Scripts/app/services/service.exammanager.js
/*
* Author: Hasitha Liyanage
* $Date: 11-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Services = (function ($scope, $, $utils, $api, $module) {
    /// <summary>
    /// QuestionBank Service Handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$api">Data api</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    var $this = this, $sub = $module.ExamManager = $module.ExamManager || {};
    $sub.onExamDeactivate = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.onExamDeactivate, data, onSuccess || function () {

        }, onError || function () {

        });
    };

   
    $sub.createExam = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.createExam, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onCategoryDropDownRead = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.get($api.onCategoryDropDownRead, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    
    $sub.onExamQuestionCategoryView = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onExamQuestionCategoryView, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Configs.Apis.ExamManager, window.Exam.Services || {}));

///#source 1 1 /Scripts/app/services/service.assignment.js
/*
* Author: Hasitha Liyanage
* $Date: 11-15-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Services = (function ($scope, $, $utils, $api, $module) {
    /// <summary>
    /// QuestionBank Service Handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$api">Data api</param>
    /// <param name="$module">Optional:Current module if require to override or extend</param>
    var $this = this, $sub = $module.Assignment = $module.Assignment || {};
    $sub.onDeactivate = function (data, onSuccess, onError) {
        /// <summary>
        /// Save sample
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.post($api.onQuestionDeactivate, data, onSuccess || function () {

        }, onError || function () {

        });
    };

   

    $sub.onAssignmentView = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onAssignmentView, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onNonEmployeeSelection = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onNonEmployeeSelection, data, onSuccess || function () {

        }, onError || function () {

        });
    };

    $sub.onEmployeeSelection = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onEmployeeSelection, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    
    $sub.onCandidateAssignment = function (data, onSuccess, onError) {
        /// <summary>
        /// Get saved permissions
        /// </summary>
        /// <param name="data">Object</param>
        /// <param name="onSuccess">Callback on success</param>
        /// <param name="onError">Callback on error</param>
        $utils.Ajax.getHtml($api.onCandidateAssignment, data, onSuccess || function () {

        }, onError || function () {

        });
    };
    

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Configs.Apis.Assignment, window.Exam.Services || {}));

///#source 1 1 /Scripts/app/handlers/handler.user.js
/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.User = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;
    var isPasswordConfirmed = false;

    $this = {
        settings: {},
        bindControls: function() {
            this.settings.constants = {
                $grid: function() { return $("#grid") },
                buttons: {
                    $passwordConfirm: function () { return $("#passwordConfirm"); },
                    $addUser: function () { return $("#UserAdd"); }
                },
                textboxes: {
                    $password: function() { return $("#Password"); },
                    $confPassword: function () { return $("#confPassword"); },
                    $userName : function() { return $("#UserName"); }
                },
                modals: {
                    $confirmPassword: function () { return $("#confirmPassword"); }
                },
                checkboxes:{
                    $IsActive: function () { return $("#IsActive"); },
                    $IsAdmin: function () { return $("#IsAdmin"); }
                }
            }
        },

        bindEvents: function () {
            var constants = $this.settings.constants;
                       
        }
    };

    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */

        }, options);

        $this.bindControls();
        $this.bindEvents();


    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.User, window.Exam.Handlers.User || {}));

///#source 1 1 /Scripts/app/handlers/handler.questionbank.js
/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.QuestionBank = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;

    $this = {
        settings: {},
        bindControls: function () {
            this.settings.constants = {
                grid:
                {
                    $questionBank: function () { return $("#grdQuestionBank"); },
                    $gridAnswer: function () { return $("#gridAnswer"); },
                    $gridexamType: function () { return $("#grdExamType"); }
                },
                buttons: {
                    $btnGo: function () { return $("#btnGo"); },
                    $btnUploadWindow: function () { return $("#uploadQuestionWindow"); },
                    $btnDownLoadTemplate: function () { return $("#downLoadTemplate"); },
                    $btnSave: function () { return $("#btnSave"); },
                    $btnDraft: function () { return $("#btnDraft"); },
                    $btnEditSave: function () { return $("#btnEditSave"); },
                    $btnEditDraft: function () { return $("#btnEditDraft"); }
                },
                comboboxes: {
                    $ddlExamType: function () { return $("#ddlExamType"); },
                    $ddlCategory: function () { return $("#FilterView_CategoryId"); },
                    $ddlCascadeCategory: function () { return $("#ddlCategory"); },
                    $ddlDifficultLevel: function () { return $("#FilterView_DifficultLevelId"); },
                    $ddlQuestionMode: function () { return $("#FilterView_IsPublished"); },
                    $ddlQuestionType: function () { return $("#FilterView_QuestionTypeId"); }
                },
                placeholders: {
                    $divQsBankGrid: function () { return $("#divQsBank"); },
                    $divNoRecordsMsg: function () { return $("#divNoRecordsMsg"); },
                    $divUploadError: function () { return $("#upload-errors"); }
                },
                windows: {
                    $uploadWindow: function () { return $("#uploadWindow"); }
                },
                hidden: {
                    $answerString: function () { return $("#AnswersString"); },
                    $publishMode: function () { return $("#PublishedMode"); }
                },
                anchors: {
                    $aExamType: function () { return $("#aExamType"); },
                    $aCategory: function () { return $("#aCategory"); },
                    $aRowAdd: function () { return $("#aRowAdd"); }
                },
                textBoxes:{
                 $txtQuestion: function () { return $("#Question"); }
            }
            }
        },

        bindEvents: function () {
            var constants = $this.settings.constants,
                $kgrid = constants.grid.$questionBank().data("kendoGrid"),
                $kanswerGrid = constants.grid.$gridAnswer().data("kendoGrid"),
                isExamTypeInitialized = false;
          
            constants.buttons.$btnGo().click(function () {
                if (!$utils.isFormValid()) return;
                if ($kgrid.dataSource.page() !== 1) {
                    $kgrid.dataSource.page(1);
                }
                $kgrid.dataSource.read();
                setTimeout(function () {
                    if ($kgrid.dataSource.data().length === 0) {
                        $utils.showMessage($scope.Messages.noRecordsFound, $scope.Enums.messageType.Warning);
                        constants.placeholders.$divNoRecordsMsg().css("display", "block");
                    } else {
                        constants.placeholders.$divNoRecordsMsg().css("display", "none");
                    }
                }, 500);
            });
            constants.buttons.$btnUploadWindow().click(function (e) {
                e.preventDefault();
                var window = constants.windows.$uploadWindow().data("kendoWindow");
                window.center();
                window.open();
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            });
            constants.buttons.$btnDownLoadTemplate().click(function (e) {
                e.preventDefault();
                $service.onDownLoadTemplate({
                }, function (result) {
                    window.location = $scope.Configs.Apis.QuestionBank.excelTemplateDownload + "?file=" + result;
                });
            });
            constants.buttons.$btnSave().click(function (e) {
                e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $kanswerGrid.dataSource.data(),
                    $chks = $($kanswerGrid.element).find("input[type=checkbox]");
                var arr = [];
                for (var i = 0, x = $gdata.length; i < x; i++) {
                    $gdata[i]["IsCorrect"] = $($chks[i]).is(":checked");
                    if ($($chks[i]).is(":checked")) {
                        arr.push($($chks[i]).is(":checked"));
                    }
                }
                constants.hidden.$publishMode().val("1");
                constants.hidden.$answerString().val(JSON.stringify($gdata));
               if ($validator.validate()) {
                    if ($("#QuestionTypeId").val() === $scope.Enums.questionType.MultipleCorrect.toString())
                        $module.createQuestionPublish($scope.Configs.Apis.QuestionBank.createQuestion, $scope.Enums.mode.Add);
                    else {
                        if (arr.length > 1)
                            $utils.showMessage($scope.Messages.moreThanOneAnswerCannotBeCorrect, $scope.Enums.messageType.Error);
                        else
                            $module.createQuestionPublish($scope.Configs.Apis.QuestionBank.createQuestion, $scope.Enums.mode.Add);
                    }
                }
            });
            constants.buttons.$btnDraft().click(function (e) {
                e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $kanswerGrid.dataSource.data(),
                    $chks = $($kanswerGrid.element).find("input[type=checkbox]");
                var arr = [];
                for (var i = 0, x = $gdata.length; i < x; i++) {
                    $gdata[i]["IsCorrect"] = $($chks[i]).is(":checked");
                    if ($($chks[i]).is(":checked")) {
                        arr.push($($chks[i]).is(":checked"));
                    }
                }
                constants.hidden.$publishMode().val("0");
                constants.hidden.$answerString().val(JSON.stringify($gdata));
                if ($validator.validate()) {
                    if ($("#QuestionTypeId").val() === $scope.Enums.questionType.MultipleCorrect.toString())
                        $module.createQuestionDraft($scope.Configs.Apis.QuestionBank.createQuestion, $scope.Enums.mode.Add);
                    else {
                        if (arr.length > 1)
                            $utils.showMessage($scope.Messages.moreThanOneAnswerCannotBeCorrect, $scope.Enums.messageType.Error);
                        else
                            $module.createQuestionDraft($scope.Configs.Apis.QuestionBank.createQuestion, $scope.Enums.mode.Add);
                    }
                }
            });
            constants.buttons.$btnEditSave().click(function (e) {
                 e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $kanswerGrid.dataSource.data(),
                    $chks = $($kanswerGrid.element).find("input[type=checkbox]");
                var arr = [];
                for (var i = 0, x = $gdata.length; i < x; i++) {
                    $gdata[i]["IsCorrect"] = $($chks[i]).is(":checked");
                    if ($($chks[i]).is(":checked")) {
                        arr.push($($chks[i]).is(":checked"));
                    }
                }
                constants.hidden.$publishMode().val("1");
                constants.hidden.$answerString().val(JSON.stringify($gdata));
                if ($validator.validate())
                    if ($("#QuestionTypeId").val() === $scope.Enums.questionType.MultipleCorrect.toString())
                        $module.createQuestionPublish($scope.Configs.Apis.QuestionBank.editQuestion, $scope.Enums.mode.Edit);
                    else {
                        if (arr.length > 1)
                            $utils.showMessage($scope.Messages.moreThanOneAnswerCannotBeCorrect, $scope.Enums.messageType.Error);
                        else
                            $module.createQuestionPublish($scope.Configs.Apis.QuestionBank.editQuestion, $scope.Enums.mode.Edit);
                    }
            });
            constants.buttons.$btnEditDraft().click(function (e) {
               
                e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $kanswerGrid.dataSource.data(),
                    $chks = $($kanswerGrid.element).find("input[type=checkbox]");
                var arr = [];
                for (var i = 0, x = $gdata.length; i < x; i++) {
                    $gdata[i]["IsCorrect"] = $($chks[i]).is(":checked");
                    if ($($chks[i]).is(":checked")) {
                        arr.push($($chks[i]).is(":checked"));
                    }
                }
                constants.hidden.$publishMode().val("0");
                constants.hidden.$answerString().val(JSON.stringify($gdata));
                if ($validator.validate()) {
                    if ($("#QuestionTypeId").val() === $scope.Enums.questionType.MultipleCorrect.toString())
                        $module.createQuestionDraft($scope.Configs.Apis.QuestionBank.editQuestion,$scope.Enums.mode.Edit);
                    else {
                        if (arr.length > 1)
                            $utils.showMessage($scope.Messages.moreThanOneAnswerCannotBeCorrect, $scope.Enums.messageType.Error);
                        else
                            $module.createQuestionDraft($scope.Configs.Apis.QuestionBank.editQuestion, $scope.Enums.mode.Edit);
                    }
                }
            });
            constants.anchors.$aExamType().click(function(e) {
                e.preventDefault();
                if (!isExamTypeInitialized)
                    isExamTypeInitialized = $module.ExamType.init({
                        onExamTypeUpsert: function () {
                            // constants.comboboxes.$ddlExamType().data("kendoDropDownList").dataSource.read();
                        }
                    });
                else
                    $module.ExamType.toggle();
            });
            constants.anchors.$aCategory().click(function (e) {
                e.preventDefault();
                $module.Category.toggle();
            });
            constants.anchors.$aRowAdd().click(function (e) {
                e.preventDefault();
                if ($kanswerGrid.dataSource.data().length > 10) { 
                    $kanswerGrid.cancelRow();
                    $utils.showMessage($scope.Messages.answerLimit, $scope.Enums.messageType.Warning);
                } else $kanswerGrid.dataSource.add();
            });
            
            $module.clearData();
            $module.kendoFileUpload();
        }
    };
    
    $module.createQuestionPublish = function (url, mode) {
        var constants = $this.settings.constants;
        $.ajax({
            url: url,
            type: "POST",
            data: $(document.forms[0]).serialize(),
            success: function (result) {
                if (result.IsSuccess) {
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Success);
                   if (mode === $scope.Enums.mode.Edit) {
                        window.location = $scope.Configs.Apis.QuestionBank.questionList;
                    } else {
                        constants.grid.$gridAnswer().data("kendoGrid").dataSource.data(result.Owner),
                            constants.textBoxes.$txtQuestion().val("");
                    }
                } else
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Error);
            },
            error: function (result) {
                if (result.responseText.indexOf($scope.Messages.answerCannotBeNull) !== -1)
                    $utils.showMessage($scope.Messages.answerCannotBeNull, $scope.Enums.messageType.Warning);
                if (result.responseText.indexOf($scope.Messages.correctAnswerisRequired) !== -1)
                    $utils.showMessage($scope.Messages.correctAnswerisRequired, $scope.Enums.messageType.Warning);
               
            }
        });
    };
    $module.createQuestionDraft = function (url,mode) {
        var constants = $this.settings.constants;
        $.ajax({
            url: url,
            type: "POST",
            data: $(document.forms[0]).serialize(),
            success: function (result) {
                if(result.IsSuccess) {
                $utils.showMessage(result.Message, $scope.Enums.messageType.Success);
                    if (mode === $scope.Enums.mode.Edit) {
                        window.location = $scope.Configs.Apis.QuestionBank.questionList;
                    } else {
                        constants.grid.$gridAnswer().data("kendoGrid").dataSource.data(result.Owner),
                            constants.textBoxes.$txtQuestion().val("");
                    }
                } else
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Error);
            },
            error: function (result) {
                $utils.showMessage($scope.Messages.failed, $scope.Enums.messageType.Error);
            }
        });
    };

    function onUpload(e) {
         var files = e.files;
        $.each(files, function () {
            if (this.extension.toLowerCase() !== (".xlsx") && this.extension.toLowerCase() !== (".xls") && this.extension.toLowerCase() !== (".xlsm")) {
                $utils.showMessage($scope.Messages.fileFormatWrong, $scope.Enums.messageType.Error);
                e.preventDefault();
            }
        });
    };
    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */

        }, options);

        $this.bindControls();
        $this.bindEvents();
    };
    $module.getCascadeValues = function () {
        return {
            examTypeId: $("#ddlExamType").val()
        }
    };

    $module.onDataBound = function (e) {
        $utils.Kendo.Grid.onDataBound(e);
        var constants = $this.settings.constants,
        grid = constants.grid.$questionBank().data("kendoGrid");
        var trs = this.wrapper.data("kendoGrid").content.find('tr');
        if (grid.dataSource.data().length > 0) {
            trs.each(function () {
                var item = grid.dataItem($(this));
                if (item.IsPublished) {
                    $(this).find('td:nth-child(3)')[0].innerHTML = "";
                }
            });
        }
        constants.grid.$questionBank().find("button[data-mode=deactivate]").click(function (e) {
            e.preventDefault();
            var $row = $(this);
            $utils.showConfirmation($scope.Messages.deactivateRecordsPopupTitle, $scope.Messages.deactivateRecordsPopupContent, function (response) {
                if (response === "yes") {
                    var rowId = $row.attr("rowid");
                    var QuestionId = rowId;
                    $service.onDeactivate({
                            Id: QuestionId
                    }, function (response) {
                            if (response.Type === $scope.Enums.messageType.Success) {
                                $utils.showMessage(response.Message, $scope.Enums.messageType.Success);
                                if (grid.dataSource.page() !== 1) {
                                    grid.dataSource.page(1);
                                }
                                grid.dataSource.read();
                            }
                            else
                                $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        },
                        function(response) {
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        });
                }
            });
        });
        constants.grid.$questionBank().find("button[data-mode=publish]").click(function (e) {
            e.preventDefault();
            var $row = $(this);
            $utils.showConfirmation($scope.Messages.publishQuestionPopupTitle, $scope.Messages.publishQuestionPopupContent, function (response) {
                if (response === "yes") {
                    var rowId = $row.attr("rowid");
                    var QuestionId = rowId;
                    $service.onPublish({
                            Id: QuestionId
                    }, function (response) {
                        if (response.Type === $scope.Enums.messageType.Success) {
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Success);
                            if (grid.dataSource.page() !== 1) {
                                grid.dataSource.page(1);
                            }
                            grid.dataSource.read();
                        }
                        else
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        },
                        function(response) {
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Error);

                        });
                }
            });
        });
    };
    $module.getGridParameters = function () {
        return {
            query: {
                ExamTypeId: $("#ddlExamType").val(),
                CategoryId: $("#FilterView_CategoryId").val(),
                DifficultLevelId: $("#FilterView_DifficultLevelId").val(),
                Mode: $("#FilterView_IsPublished").val()
            }
        }
    };
    $module.clearData = function () {
        $("#ddlExamType").val("");
        $("#FilterView_CategoryId").val("");
        $("#FilterView_DifficultLevelId").val("");
        $("#FilterView_IsPublished").val("");
    };
    $module.kendoFileUpload = function () {
        var constants = $this.settings.constants;
        var window = constants.windows.$uploadWindow().data("kendoWindow");
        var $kgrid = constants.grid.$questionBank().data("kendoGrid");
        $("#file").kendoUpload({
            async: {
                multiple: false,
                saveUrl: $scope.Configs.Apis.QuestionBank.OnQuestionUpload,
                autoUpload: false
            },
            upload: onUpload,
            success: function (result) {
                if (result.response.Type === $scope.Enums.messageType.Success) {
                    $utils.showMessage(result.response.Message, $scope.Enums.messageType.Success);
                    window.close();
                    if ($kgrid.dataSource.page() !== 1) {
                        $kgrid.dataSource.page(1);
                    }
                    $kgrid.dataSource.read();
                } else if (result.response.Type === $scope.Enums.messageType.Error)
                    constants.placeholders.$divUploadError().html(result.response.UploadResult.Message).show();
                
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            }
        });
    };
    $module.onOpenUploadPopUp = function () {
        $(".k-upload-files").remove();
        $(".k-upload-status").remove();
        $("#upload-errors").empty().hide();
    };
    $module.ddlExamTypeOnChange = function () {
        var constants = $this.settings.constants;
        if (constants.comboboxes.$ddlExamType().val() !== "") {
             //constants.anchors.$aCategory().show();
            constants.anchors.$aCategory().prop('disabled', false);
            $module.Category.init({

            });
        } else {
            //constants.anchors.$aCategory().hide();
            constants.anchors.$aCategory().prop('disabled', true);
        }
    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.QuestionBank, window.Exam.Handlers.QuestionBank || {}));

///#source 1 1 /Scripts/app/handlers/handler.questionbank.examtype.js
/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.QuestionBank.ExamType = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;

    $this = {
        settings: {},
        bindControls: function() {
            this.settings.constants = {
                grid:
                {
                    $gridexamType: function() { return $("#grdExamType"); }
                },
                placeholders: {
                    $examType: function() { return $("#partialExamType"); },
                    $category: function() { return $("#partialCategory"); }
                },
                comboboxes: {
                    $ddlExamType: function() { return $("#ddlExamType"); }
                },
                anchors: {
                    $aRowExamTypeAdd: function() { return $("#aRowExamTypeAdd"); }

                }

            }
        },

        bindEvents: function() {
            var constants = $this.settings.constants,
                $kgrid = constants.grid.$gridexamType().data("kendoGrid"),
                $partialView = constants.placeholders.$examType();

            if ($partialView.html().length === 0) {
                $service.getExamTypes({

                }, function(response) {
                    $partialView.html(response).show();
                });
            } else {
                if ($kgrid) $kgrid.dataSource.read();
                $partialView.hide();
            }

    //Custom row add function remove and kendo default toolbar create function enabled
        //    setTimeout(function() {
        //        constants.anchors.$aRowExamTypeAdd().click(function(e) {

        //            var constants = $this.settings.constants,
        //                $kgrid = constants.grid.$gridexamType().data("kendoGrid");
        //            e.preventDefault();
        //            if ($kgrid.dataSource.page() !== 1) {
        //                $kgrid.dataSource.page(1);
        //            }
        //           // $this.isAdd = true;
        //           // $kgrid.dataSource.add();
        //        });
        //    }, 2000);
        }
        //idAdd: false
    };

   
    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */
            callbacks : {
                onExamTypeUpsert:null
            }
        }, options);

        $this.bindControls();
        $this.bindEvents();

        return true;
    };
    $module.toggle = function (e) {
        if ($this.settings.constants.placeholders.$category().is(":visible"))
            $this.settings.constants.placeholders.$category().hide();
        if ($this.settings.constants.placeholders.$examType().is(":visible"))
            $this.settings.constants.placeholders.$examType().hide();
        else
            $this.settings.constants.placeholders.$examType().show();
    };

    $module.requestEnd = function (e) {
        var constants = $this.settings.constants;
        if (e.response != undefined) {
            if (e.response.Errors == null) {
                if (e.type === "update" || e.type === "create" || e.type === "destroy") {
                    e.sender.read();
                    constants.comboboxes.$ddlExamType().data("kendoDropDownList").dataSource.read();
                    //if ($.isFunction($this.settings.callbacks.onExamTypeUpsert))
                    //    $this.settings.callbacks.onExamTypeUpsert();
                }
            } else {
                if (e.type === "update" || e.type === "create") {
                    e.sender.read();
                }
            }
        }
        };

   
    $module.onDataBoundExamType = function (e) {
        var constants = $this.settings.constants;
         $utils.Kendo.Grid.onDataBound(e);
      /*  if ($this.isAdd) {
            this.wrapper.find(".k-grid-edit:first").click();
            $this.isAdd = false;
        }*/

        constants.grid.$gridexamType().find("button[data-mode=delete]").click(function (e) {
            var item = constants.grid.$gridexamType().data("kendoGrid").dataItem($(this).closest("tr"));
            e.preventDefault();
            $utils.showConfirmation($scope.Messages.deleteRecordsCommonPopupTitle, $scope.Messages.deleteRecordsCommonPopupContent, function (response) {
                if (response === "yes") {
                    if (item.Id !== 0) {
                        $service.onExamTypeDelete({
                            Id: item.Id
                        }, function(response) {
                            if (response.Type === $scope.Enums.messageType.Success) {
                                constants.comboboxes.$ddlExamType().data("kendoDropDownList").dataSource.read();
                                constants.grid.$gridexamType().data("kendoGrid").dataSource.read();
                            }
                        });
                    } else {
                        var dataSource = constants.grid.$gridexamType().data("kendoGrid").dataSource;
                        dataSource.remove(item);
                    }
                }
            });
        });
    };
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.QuestionBank, window.Exam.Handlers.QuestionBank.ExamType || {}));

///#source 1 1 /Scripts/app/handlers/handler.questionbank.category.js
/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.QuestionBank.Category = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;

    $this = {
        settings: {},
        bindControls: function () {
            this.settings.constants = {
                grid:
                {
                    $gridCategory: function () { return $("#grdCategory"); }
                },
                placeholders: {
                    $category: function () { return $("#partialCategory"); },
                    $examType: function () { return $("#partialExamType"); }
                },
                comboboxes: {
                    $ddlCascadeCategory: function () { return $("#ddlCategory"); },
                    $ddlExamType: function () { return $("#ddlExamType"); }
                },
                anchors: {
                    $aRowCategoryAdd: function () { return $("#aRowCategoryAdd"); }
                   
                }
                
            }
        },

        bindEvents: function() {
            var constants = $this.settings.constants,
                $partialView = constants.placeholders.$category();
            $service.getCategories({
                examTypeId: constants.comboboxes.$ddlExamType().val()
            }, function(response) {
                $partialView.html(response).hide();
            });

   //Custom row add function remove and kendo default toolbar create function enabled
            //setTimeout(function(){
            //    constants.anchors.$aRowCategoryAdd().click(function (e) {
            //    var constants = $this.settings.constants,
            //    $kgrid = constants.grid.$gridCategory().data("kendoGrid");
            //    e.preventDefault();
            //    if ($kgrid.dataSource.page() !== 1) {
            //        $kgrid.dataSource.page(1);
            //    }
            //    $kgrid.dataSource.add();
                
            //    });
            //}, 2000);
        }
    };


    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */
            callbacks: {
                onExamTypeUpsert: null
            }
        }, options);

        $this.bindControls();
        $this.bindEvents();

        return true;
    };
    $module.toggle = function (e) {
        if ($this.settings.constants.placeholders.$examType().is(":visible"))
            $this.settings.constants.placeholders.$examType().hide();
        if ($this.settings.constants.placeholders.$category().is(":visible"))
            $this.settings.constants.placeholders.$category().hide();
        else
            $this.settings.constants.placeholders.$category().show();
    };

    $module.getGridParameters = function () {
        return {
                examTypeId: $("#ddlExamType").val()
        }
    };

    $module.onGridSave = function (e) {
            var constants = $this.settings.constants;
            e.model.ExamTypeId = constants.comboboxes.$ddlExamType().val();
            e.model.ExamType = null;
    };

   $module.onGridRemove = function (e) {
      e.model.ExamType = null;
   };

   $module.requestEnd = function (e) {
        var constants = $this.settings.constants;
        if (e.response != undefined) {
            if (e.response.Errors == null) {
                if (e.type === "update" || e.type === "create" || e.type === "destroy") {
                    e.sender.read();
                    constants.comboboxes.$ddlCascadeCategory().data("kendoDropDownList").dataSource.read();
                }
            } else {
                if (e.type === "update" || e.type === "create") {
                    e.sender.read();
                }
            }
        }
    };

   $module.onDataBoundCategory = function (e) {
       var constants = $this.settings.constants;
       $utils.Kendo.Grid.onDataBound(e);

       constants.grid.$gridCategory().find("button[data-mode=delete]").click(function (e) {
           var item = constants.grid.$gridCategory().data("kendoGrid").dataItem($(this).closest("tr"));
           e.preventDefault();
           $utils.showConfirmation($scope.Messages.deleteRecordsCommonPopupTitle, $scope.Messages.deleteRecordsCommonPopupContent, function (response) {
               if (response === "yes") {
                   if (item.Id !== 0) {
                       $service.onCategoryDelete({
                           Id: item.Id
                       }, function (response) {
                           if (response.Type === $scope.Enums.messageType.Success) {
                               constants.comboboxes.$ddlCascadeCategory().data("kendoDropDownList").dataSource.read();
                               constants.grid.$gridCategory().data("kendoGrid").dataSource.read();
                           }
                       });
                   } else {
                       var dataSource = constants.grid.$gridCategory().data("kendoGrid").dataSource;
                       dataSource.remove(item);
                   }
               }
           });
       });
   };
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.QuestionBank, window.Exam.Handlers.QuestionBank.Category || {}));

///#source 1 1 /Scripts/app/handlers/handler.exammanager.js
/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.ExamManager = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;
    var oldExamTypeValue, oldQuestionPickingMode;
    $this = {
        settings: {},
        bindControls: function () {
            this.settings.constants = {
                grid:
                {
                    $examManager: function () { return $("#grdExamManager"); },
                    $gridQuestionCategory: function () { return $("#gridQuestionCategory"); }
                },
                buttons: {
                    $btnGo: function () { return $("#btnGo"); },
                    $btnCreate: function () { return $("#btnCreate"); },
                    $btnUpdate: function () { return $("#btnUpdate"); },
                    $btnEditSave: function () { return $("#btnEditSave"); }
                },
                comboboxes: {
                    $ddlExamType: function () { return $("#ddlExamType"); },
                    $ddlExamId: function () { return $("#FilterView_ExamId"); },
                    $ddlQuestionPick: function () { return $("#IsManualPick"); }
                    
                },
                placeholders: {
                    $divQsBankGrid: function () { return $("#divQsBank"); }
                },
                hidden: {
                    $questionCategoryString: function () { return $("#QuestionCategoryString"); },
                    $hdnmode: function () { return $("#mode"); },
                    $hdnExamId: function () { return $("#ExamId"); },
                    
                },
                anchors: {
                    $aRowAdd: function () { return $("#aRowAdd"); }
                },
                textBox: {
                    $duration: function () { return $("#Duration"); },
                    $passScore: function () { return $("#PassScore"); }
                }
            }
        },

        bindEvents: function () {
            var constants = $this.settings.constants,
                $kgrid = constants.grid.$examManager().data("kendoGrid"),
                $categoryGrid = constants.grid.$gridQuestionCategory().data("kendoGrid");
               
            constants.buttons.$btnGo().click(function () {
                if (!$utils.isFormValid()) return;
                if ($kgrid.dataSource.page() !== 1) {
                    $kgrid.dataSource.page(1);
                }
                $kgrid.dataSource.read();
                setTimeout(function () {
                    if ($kgrid.dataSource.data().length === 0) {
                        $utils.showMessage($scope.Messages.noRecordsFound, $scope.Enums.messageType.Warning);
                    } 
                }, 500);
            });
            constants.buttons.$btnCreate().click(function (e) {
                e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $categoryGrid.dataSource.data(),
                    error = false;
                constants.hidden.$questionCategoryString().val(JSON.stringify($gdata));
                if ($validator.validate()) {
                    if ($gdata.length > 0) {
                        for (var a = 0; a < $gdata.length; a++) {
                            if ($gdata[a].CountValue == null || $gdata[a].CountValue===0) {
                                error = true;
                            }
                        }
                        if (!error) {
                            $module.SaveExam($scope.Configs.Apis.ExamManager.createExam);
                        } else {
                            $utils.showMessage($scope.Messages.addQuestionCategories, $scope.Enums.messageType.Warning);
                        }
                    } else {
                        $utils.showMessage($scope.Messages.addQuestionCategories, $scope.Enums.messageType.Warning);
                    }
                }
            });
            constants.buttons.$btnUpdate().click(function (e) {
                e.preventDefault();
                $categoryGrid = constants.grid.$gridQuestionCategory().data("kendoGrid");
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $categoryGrid.dataSource.data(),
                    error = false;
                constants.hidden.$questionCategoryString().val(JSON.stringify($gdata));
                if ($validator.validate()) {
                    if ($gdata.length > 0) {
                        for (var a = 0; a < $gdata.length; a++) {
                            if ($gdata[a].CountValue == null || $gdata[a].CountValue === 0) {
                                error = true;
                            }
                        }
                        if (!error) {
                            $module.SaveExam($scope.Configs.Apis.ExamManager.updateExam);
                        } else {
                            $utils.showMessage($scope.Messages.addQuestionCategories, $scope.Enums.messageType.Warning);
                        }
                    } else {
                        $utils.showMessage($scope.Messages.addQuestionCategories, $scope.Enums.messageType.Warning);
                    }
                }
            });
            constants.anchors.$aRowAdd().click(function (e) {
                e.preventDefault();
                var isValid = true;
                if (constants.comboboxes.$ddlExamType().val() === "") {
                    $categoryGrid.cancelRow();
                    $utils.showMessage($scope.Messages.selectExamType, $scope.Enums.messageType.Warning);
                    isValid = false;
                }
                else if (constants.comboboxes.$ddlQuestionPick().val() === "") {
                    $categoryGrid.cancelRow();
                    $utils.showMessage($scope.Messages.selectQuestionPickingMode, $scope.Enums.messageType.Warning);
                    isValid = false;
                }
                if (isValid && constants.comboboxes.$ddlQuestionPick().val() !== "") {
                    $categoryGrid.dataSource.add();
                }
            });
        }
    };

    $module.SaveExam = function (url) {
        $.ajax({
            url: url,
            type: "POST",
            data: $(document.forms[0]).serialize(),
            success: function (result) {
                if (result.IsSuccess) {
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Success);
                    window.location = $scope.Configs.Apis.ExamManager.examList;
                } else
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Error);
            },
            error: function (result) {
                if (result.responseText.indexOf($scope.Messages.recordAlreadyExist) !== -1)
                    $utils.showMessage($scope.Messages.recordAlreadyExist, $scope.Enums.messageType.Warning);
                else if (result.responseText.indexOf($scope.Messages.cannotAddDuplicateQuestionCategories) !== -1)
                    $utils.showMessage($scope.Messages.cannotAddDuplicateQuestionCategories, $scope.Enums.messageType.Warning);
                else if (result.responseText.indexOf($scope.Messages.questionCountExceeded) !== -1)
                    $utils.showMessage($scope.Messages.questionCountExceeded, $scope.Enums.messageType.Warning);
                else
                    $utils.showMessage($scope.Messages.failed, $scope.Enums.messageType.Error);
            }
        });
    };

    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */

        }, options);

        $this.bindControls();
        $this.bindEvents();

        $module.clearData();
        if (parseInt($("#mode").val()) === $scope.Enums.mode.Edit) {
            $("#ddlExamType").data("kendoDropDownList").readonly();
            $("#IsManualPick").data("kendoDropDownList").readonly();
        }
    };

    $module.getCascadeValues = function () {
        return {
            examTypeId: $("#ddlExamType").val()
        }
    };

    $module.onDataBound = function (e) {
        var constants = $this.settings.constants,
             grid = constants.grid.$examManager().data("kendoGrid");
        $utils.Kendo.Grid.onDataBound(e);
        constants.grid.$examManager().find("button[data-mode=deactivate]").click(function (e) {
            e.preventDefault();
            var $row = $(this);
            $utils.showConfirmation($scope.Messages.deactivateRecordsPopupTitle, $scope.Messages.deactivateRecordsPopupContent, function (response) {
                if (response === "yes") {
                    var rowId = $row.attr("rowid");
                    var ExamId = rowId;
                    $service.onExamDeactivate({
                        Id: ExamId
                        }, function(response) {
                            if (response.Type === $scope.Enums.messageType.Success) {
                                $utils.showMessage(response.Message, $scope.Enums.messageType.Success);
                                if (grid.dataSource.page() !== 1) {
                                    grid.dataSource.page(1);
                                }
                                grid.dataSource.read();
                            } else
                                $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        },
                        function(response) {
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        });
                }
            });
        });
    };

    $module.getGridParameters = function () {
        return {
            query: {
                ExamTypeId: $("#ddlExamType").val(),
                ExamId: $("#FilterView_ExamId").val()
            }
        }
    };

    $module.clearData = function () {
        $("#ddlExamType").val("");
        $("#FilterView_ExamId").val("");
    };

    $module.ddlExamTypeOnChange = function (e) {
        var constants = $this.settings.constants,
        categoryGrid = constants.grid.$gridQuestionCategory().data("kendoGrid");
        if (constants.comboboxes.$ddlExamType().val() !== "") {
            if (categoryGrid.dataSource.data().length === 0) {
                oldExamTypeValue = constants.comboboxes.$ddlExamType().val();
                $service.onCategoryDropDownRead({
                    examTypeId: constants.comboboxes.$ddlExamType().val()
                }, function(response) {
                });
            } else {
                if (constants.comboboxes.$ddlExamType().prop("readonly") === false) {
                    $utils.showConfirmation($scope.Messages.deleteRecordsPopupTitle, $scope.Messages.deleteRecordsPopupContent, function(response) {
                        if (response === "yes") {
                            categoryGrid.dataSource.data([]);
                            $service.onCategoryDropDownRead({
                                examTypeId: constants.comboboxes.$ddlExamType().val()
                            }, function(response) {
                            });
                        } else {
                            constants.comboboxes.$ddlExamType().data("kendoDropDownList").value(oldExamTypeValue);
                        }
                    });
                }
            }
        }
    };
    
    $module.ddlQuestionPickOnChange = function () {
        var constants = $this.settings.constants,
        categoryGrid = constants.grid.$gridQuestionCategory().data("kendoGrid");
        if (constants.comboboxes.$ddlQuestionPick().val() !== "") {
            if (categoryGrid.dataSource.data().length === 0) {
                oldQuestionPickingMode = constants.comboboxes.$ddlQuestionPick().val();
            } else {
                if (constants.comboboxes.$ddlQuestionPick().prop("readonly") === false) {
                    $utils.showConfirmation($scope.Messages.deleteRecordsPopupTitle, $scope.Messages.deleteRecordsPopupContent, function (response) {
                        if (response === "yes") {
                            categoryGrid.dataSource.data([]);
                            oldQuestionPickingMode = constants.comboboxes.$ddlQuestionPick().val();
                        } else {
                            constants.comboboxes.$ddlQuestionPick().data("kendoDropDownList").value(oldQuestionPickingMode);
                        }
                    });
                }
            }
        }
    };

    $module.onEditCategory = function (e) {
        e.preventDefault();
        var constants = $this.settings.constants;
        if (constants.comboboxes.$ddlQuestionPick().val() !== "") {
            if (constants.comboboxes.$ddlQuestionPick().val() === "True") {
                e.container.find("input[id=CountValue]").prop('disabled', true);
               } else {
                e.container.find("input[id=CountValue]").prop('disabled', false);
            }
        }
    };

    $module.onDataBoundCategory = function (e) {
        $utils.Kendo.Grid.onDataBound(e);
        setTimeout(function() {
            var constants = $this.settings.constants, $kgrid = constants.grid.$gridQuestionCategory().data("kendoGrid");

            if (constants.comboboxes.$ddlQuestionPick().val() !== "") {
                if (constants.comboboxes.$ddlQuestionPick().val() === "True") {
                    $kgrid.element.find("button[id=editCategory]").show();
                    $kgrid.element.find("button[id=viewCategory]").show();
                } else {
                    $kgrid.element.find("button[id=editCategory]").hide();
                    $kgrid.element.find("button[id=viewCategory]").hide();
                }
            }
 
            constants.grid.$gridQuestionCategory().find("button[data-mode=edit]").each(function(i) {
                var mode = $(this).attr("data-mode");
                $(this).attr("data-index", i).click(function (e) {
                    e.preventDefault();
                    var index = parseInt($(this).attr("data-index")),
                        $gdata = $kgrid.dataSource.data();
                    if ($gdata[index].CategoryId.Key !== "" && $gdata[index].DifficultLevelId.Key !== "") {

                        $module.Questions.init({
                            index: index,
                            query: {
                                QuestionCategoryId: $gdata[index].Id,
                                CategoryId: $gdata[index].CategoryId.Key,
                                DifficultLevelId: $gdata[index].DifficultLevelId.Key,
                                SelectedIndexes: $gdata[index].SelectedIndexes,
                                Action: mode === "edit" ? $scope.Enums.action.Edit : $scope.Enums.action.View, //edit action-2, view action-1
                                Mode: $("#mode").val() // Edit mode-1 , Add mode-2
                            },
                            onQuestionSelected: function (o, sIndexes, cIndex) {
                                $gdata[cIndex].Questions = o;
                                $gdata[cIndex].SelectedIndexes = sIndexes;
                                $gdata[cIndex].CountValue = sIndexes.length;

                                $kgrid.element.find("tbody tr:nth(" + cIndex + ") td:nth(3)").html(sIndexes.length);
                            }
                        });
                    } else {
                        $utils.showMessage($scope.Messages.selectCategory, $scope.Enums.messageType.Warning);
                    }
                });
            });

            constants.grid.$gridQuestionCategory().find("button[data-mode=view]").each(function (i) {
                var mode = $(this).attr("data-mode");
                $(this).attr("data-index", i).click(function(e) {
                    e.preventDefault();
                    var index = parseInt($(this).attr("data-index")),
                        $gdata = $kgrid.dataSource.data();
                    if ($gdata[index].CategoryId.Key !== "" && $gdata[index].DifficultLevelId.Key !== "") {

                        $module.Questions.init({
                            index: index,
                            query: {
                                QuestionCategoryId: $gdata[index].Id,
                                CategoryId: $gdata[index].CategoryId.Key,
                                DifficultLevelId: $gdata[index].DifficultLevelId.Key,
                                SelectedIndexes: $gdata[index].SelectedIndexes,
                                Action: $scope.Enums.action.View, //edit action-2, view action-1
                                Mode: $("#mode").val() // Edit mode-1 , Add mode-2
                            },
                            onQuestionSelected: function(o, sIndexes, cIndex) {
                                $gdata[cIndex].Questions = o;
                                $gdata[cIndex].SelectedIndexes = sIndexes;
                                $gdata[cIndex].CountValue = sIndexes.length;

                                $kgrid.element.find("tbody tr:nth(" + cIndex + ") td:nth(3)").html(sIndexes.length);
                            }
                        });
                    } else {
                        $utils.showMessage("Please select category", $scope.Enums.messageType.Warning);
                    }

                });
            });
            
            constants.grid.$gridQuestionCategory().find("button[data-mode=delete]").click(function (e) {
               var item = constants.grid.$gridQuestionCategory().data("kendoGrid").dataItem($(this).closest("tr"));
               e.preventDefault();
               $utils.showConfirmation($scope.Messages.deleteRecordsCommonPopupTitle, $scope.Messages.deleteRecordsCommonPopupContent, function (response) {
                    if (response === "yes") {
                        var dataSource = constants.grid.$gridQuestionCategory().data("kendoGrid").dataSource;
                        dataSource.remove(item);
                        dataSource.sync();
                    }
                });
            });
        }, 500);
    };
   
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.ExamManager, window.Exam.Handlers.ExamManager || {}));

///#source 1 1 /Scripts/app/handlers/handler.exammanager.questions.js
/*
* Author: Hasitha Liyanage
* $Date: 06-23-2016
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.ExamManager.Questions = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// Exam question selection handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this; 
    $this = {
        settings: {},
        bindControls: function() {
            this.settings.constants = {
                grid: {
                    $questionSelectionGrid: function() { return $("#questionSelectionGrid"); }
                },
                checkboxes : {
                    $chkAll: function () { return $("#chkAll"); }
                },
                modals: {
                    $questionSelectionPopUp: function() { return $("#questionSelectionPopup"); }
                },
                buttons: {
                    $assign: function() { return $("#btnAssign"); }
                }
            }
        },

        getGridDataSource : function() {
            return $this.settings.constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource;
        },

        bindEvents: function() {
            var constants = $this.settings.constants,
                query = $this.settings.query,
                $dataSource = $this.getGridDataSource();

            constants.buttons.$assign().click(function(e) {
                e.preventDefault();
                var arrSelected = [],
                    arrSelectedIndexes = [];
                   
                if ($.isFunction($this.settings.onQuestionSelected)) {
                    for (var j = 0, i = $dataSource.data().length; j < i; j++) {
                        var dataItem = $dataSource.data()[j];
                        if (dataItem.IsSelected) {
                            arrSelected.push(dataItem);
                            arrSelectedIndexes.push(dataItem.Index);
                        }
                    }
                    $this.settings.onQuestionSelected(arrSelected, arrSelectedIndexes, $this.settings.index);
                    $module.close();
                }
            });

            var arr = [];
            for (var z = 0, r = $dataSource.data().length; z < r; z++) {
                arr.push(z);
            }

            if (parseInt(query.Mode) === $scope.Enums.mode.Edit) {
                if (query.Action === $scope.Enums.action.View) {
                    constants.buttons.$assign().hide();
                    constants.grid.$questionSelectionGrid().find('.k-grid-header').find('tr').find('th')[0].innerHTML = "";
                    //setTimeout(function() {
                    
                    if (query.SelectedIndexes.length > 0) {
                        for (var v = $dataSource.data().length - 1; v >= 0; v--) {
                            if (query.SelectedIndexes.indexOf(v) === -1) {
                                var dataGrd = constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.at(v);
                                constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.remove(dataGrd);
                            }
                        }
                    } else {
                        for (var a = $dataSource.data().length - 1; a >= 0; a--) {
                            var itemSelected = constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.at(a);
                            if (itemSelected.IsSelected === false) {
                                constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.remove(itemSelected);
                            }
                        }
                    }
                    // }, 500);
                }
                else {
                    $(query.SelectedIndexes).each(function (i) {
                        if (arr.length !== query.SelectedIndexes.length) {
                            var notSelected = $(arr).not(query.SelectedIndexes).get();
                            $(notSelected).each(function(v) {
                                var index = notSelected[v];
                                $dataSource.data()[index].IsSelected = false;
                                constants.grid.$questionSelectionGrid().find(".chkList[data-index=" + index + "]").prop("checked", false);
                            });
                        }
                        var idx = query.SelectedIndexes[i];
                        $dataSource.data()[idx].IsSelected = true;
                        constants.grid.$questionSelectionGrid().find(".chkList[data-index=" + idx + "]").prop("checked", true);
                    });
                }
            }
            else {
                if (query.Action === $scope.Enums.action.View) {
                    constants.buttons.$assign().hide();
                    constants.grid.$questionSelectionGrid().find('.k-grid-header').find('tr').find('th')[0].innerHTML = "";
                    if (query.SelectedIndexes.length > 0) {
                        for (var x = $dataSource.data().length - 1; x >= 0; x--) {
                            if (query.SelectedIndexes.indexOf(x) === -1) {
                                var grd = constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.at(x);
                                constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.remove(grd);
                            }
                        }
                    } else {
                        for (var i = $dataSource.data().length - 1; i >= 0; i--) {
                            var item = constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.at(i);
                            constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.remove(item);
                        }
                    }
                }
                else {
                    $(query.SelectedIndexes).each(function (i) {
                        if (arr.length !== query.SelectedIndexes.length) {
                            var notSelected = $(arr).not(query.SelectedIndexes).get();
                            $(notSelected).each(function (v) {
                                var index = notSelected[v];
                                $dataSource.data()[index].IsSelected = false;
                                constants.grid.$questionSelectionGrid().find(".chkList[data-index=" + index + "]").prop("checked", false);
                            });
                        }
                        var idx = query.SelectedIndexes[i];
                        $dataSource.data()[idx].IsSelected = true;
                        constants.grid.$questionSelectionGrid().find(".chkList[data-index=" + idx + "]").prop("checked", true);
                    });
                }
            }

            $this.bindCheckChangeEvent();
        },

        bindCheckChangeEvent: function () {
            var constants = $this.settings.constants,
                $kgrid = $this.settings.constants.grid.$questionSelectionGrid().data("kendoGrid"),
                $chks = $kgrid.wrapper.find(".chkList");

            function bindAllCheckBoxState() {
                constants.checkboxes.$chkAll().prop("checked", ($kgrid.wrapper.find(".chkList:checked").length === $chks.length));
            };

            function bindTableCheckBoxstate() {
                $kgrid.dataSource.data()[$(this).attr("data-index")].IsSelected = $(this).prop("checked");

                bindAllCheckBoxState();
            };
           
            constants.checkboxes.$chkAll().unbind("click").click(function () {
                var isChecked = $(this).prop("checked");
                $chks.each(function () {
                    $kgrid.dataSource.data()[$(this).attr("data-index")].IsSelected = isChecked;
                    $(this).prop("checked", isChecked);
                }).click(bindTableCheckBoxstate);
            });

            $chks.click(bindTableCheckBoxstate);
            bindAllCheckBoxState();
        } 
    };


    
    $module.init = function(options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */
            query: {
                QuestionCategoryId: 0,
                CategoryId: 0,
                DifficultLevelId: 0,
                SelectedIndexes: [],
                Action: 0, //Edit action-2, View action-1
                Mode: 0 // Edit mode-1 , Add mode-2
            },
            index: 0,
            onQuestionSelected: null
        }, options);

        $this.bindControls();

        $module.loadQuestions($this.settings.query);
    };

    $module.loadQuestions = function (query) {
        /// <summary>
        /// Loading the question related to category,diffeculty level and category
        /// </summary>
        /// <param name="query"></param> 
        $service.onExamQuestionCategoryView(query, function (response) {
            $module.open(response);
            
       });
    };

    $module.getQuestionSelectionGridParameters = function () {
         return {
            query: $this.settings.query
        }
    };

    $module.open= function (response) {
        var constants = $this.settings.constants;
        var $window = constants.modals.$questionSelectionPopUp().kendoWindow({
            modal: true,
            activate: function () {
                //setTimeout(function(){
                $this.bindEvents();
                //}, 500);
            }
        }).data("kendoWindow");
        
        $window.content(response);
        $window.center();
        $window.open();
    };

    $module.close = function () {
        $this.settings.constants.grid.$questionSelectionGrid().find(".k-grid-content").empty();
        $this.settings.constants.modals.$questionSelectionPopUp().data("kendoWindow").close();
    };

    $module.onDataBound = function () {
        var constants = $this.settings.constants,
                query = $this.settings.query,
                $dataSource = $this.getGridDataSource();

        $this.bindCheckChangeEvent();
       
            if (query.Action === $scope.Enums.action.View) {
                if ($dataSource.data().length > 0) {
                    var trs = constants.grid.$questionSelectionGrid().find('.k-grid-content').find('tr');
                    trs.each(function() {
                        $(this).find("input[type=checkbox]").hide();
                    });
                }
        }
    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.ExamManager, window.Exam.Handlers.ExamManager.Questions || {}));

///#source 1 1 /Scripts/app/handlers/handler.assignment.js
/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.Assignment = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;

    $this = {
        settings: {},
        bindControls: function () {
            this.settings.constants = {
                grid:
                {
                    $grdEmployeeSelection: function () { return $("#grdEmployeeSelection"); },
                    $grdCandidateAssignment: function () { return $("#grdCandidateAssignment"); }
                },
                buttons: {
                    $btnEmpSearch: function () { return $("#btnEmpSearch"); }
                    
                },
                comboboxes: {
                    $ddlExamType: function () { return $("#ddlExamType"); },
                    $ddlIsExamAvailable: function () { return $("#IsExamAvailable"); },
                    $ddlDivision: function () { return $("#ddlDivision"); },
                    $ddlSectionCode: function () { return $("#SectionCode"); },
                    $ddlDesignation: function () { return $("#Designation"); }
                },
               windows: {
                	$assignmentWindow: function () { return $("#assignmentWindow"); }
                },
                hidden: {
                    $candidateString: function () { return $("#CandidateString"); }
                    
                },
                textBoxes:{
                    $txtQuestion: function () { return $("#Question"); },
                    $dtFromDate: function () { return $("#FromDate"); },
                    $dtToDate: function () { return $("#ToDate"); }
                    
                },
                datePicker: {
                    $dateStart: function () { return $("#Start"); },
                    $dateEnd:function () { return $("#End"); }
                },
                checkboxes: {
                    $nonEmployee: function () { return $("#NonEmployee"); },
                    $chkAll: function () { return $("#chkAll"); }
                },
                divs: {
                    $sendCalendarInvite: function () { return $("#calendarInvite"); },
                    $txtbehalfEmail: function () { return $("#behalfEmail"); }
                }
                
            }
        },

        bindEvents: function () {
            var constants = $this.settings.constants;
            //var $chks = $kgrid.wrapper.find(".chkList");
            //constants.buttons.$btnEmpSearch().click(function (e) {
            //    //e.preventDefault();
            //    debugger;
            //    if (!$utils.isFormValid()) return;
            //    if ($kgrid.dataSource.page() !== 1) {
            //        $kgrid.dataSource.page(1);
            //    }
            //    $kgrid.dataSource.read();
            //    setTimeout(function () {
            //        if ($kgrid.dataSource.data().length === 0) {
            //            $utils.showMessage($scope.Messages.noRecordsFound, $scope.Enums.messageType.Warning);
            //            constants.placeholders.$divNoRecordsMsg().css("display", "block");
            //        } else {
            //            constants.placeholders.$divNoRecordsMsg().css("display", "none");
            //        }
            //    }, 500);
            //});
        }
    };

    $module.onChangeSchedular = function (e) {
        var constants = $this.settings.constants;

    	e.preventDefault();
    	var start = e.start; //selection start date
    	var end = e.end; //selection end date
    	var slots = e.slots; //list of selected slots
    	var events = e.events; //list of selected Scheduler events
        
    	$service.onAssignmentView({
    		model: {
    			Start: start,
    			End: end,
    			ScheduleId: e.scheduleId
		    }
    	}, function (response) {
    		var window = constants.windows.$assignmentWindow().data("kendoWindow");
    		window.center();
    		window.content(response);
    		window.open();
    		var arrSelected = [];
    		var form = $("#frmAssignment").show();
	        var $form = $(document.forms[0]);
	         //   $validator = $form.data("kendoValidator");
	        
	        form.steps({
	            headerTag: "h3",
	            bodyTag: "fieldset",
	            transitionEffect: "slideLeft",
	            onStepChanging: function(event, currentIndex, newIndex) {
	                //if ($validator.validate()) { //TODO form validation

	                //}
	                // Allways allow previous action even if the current form is not valid!
	                if (currentIndex > newIndex) {
	                    return true;
	                }
	                if (currentIndex === 1 ) {
	                    var items = $("#grdEmployeeSelection").data("kendoGrid").dataSource.data();
	                    for (var j = 0, i = items.length; j < i; j++) {
	                        var dataItem = items[j];
	                        if (dataItem.IsSelected) {
	                            arrSelected.push(dataItem);
	                        }
	                    }
	                }
	                // Needed in some cases if the user went back (clean up)
	                if (currentIndex < newIndex) {
	                    // To remove error styles
	                    form.find(".body:eq(" + newIndex + ") label.error").remove();
	                    form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
	                }
	                form.validate().settings.ignore = ":disabled,:hidden";
	                return form.valid();
	            },
	            onStepChanged: function(event, currentIndex, priorIndex) {
	                if (currentIndex === 1 && priorIndex === 0) {
	                    if ($("#NonEmployee").is(":checked")) {
	                        $service.onNonEmployeeSelection({
	                            model: {
	                                NonEmployee: $("#NonEmployee").is(":checked"),
	                                ScheduleId: e.scheduleId
	                            }
	                        }, function(response) {
	                            $("#empSelection").html(response);
	                            if (currentIndex === 1) {
	                                $module.checkedChangeEvent();
	                            }
	                        });
	                    }
	                    else {
	                        $service.onEmployeeSelection({
	                            model: {
	                                NonEmployee: $("#NonEmployee").is(":checked"),
	                                ScheduleId: e.scheduleId
	                            }
	                        }, function(response) {
	                            $("#empSelection").html(response);
	                            if (currentIndex === 1) {
	                                $module.checkedChangeEvent();
	                            }
	                        });
	                    }
	                }
	                if (currentIndex === 2 && priorIndex === 1) {
	                    $service.onCandidateAssignment({
	                        model: {
	                            ScheduleId: e.scheduleId
	                        }
	                    }, function (response) {
	                        $("#candidateAssign").html(response);

	                        setTimeout(function () {
	                        var grd = $("#grdCandidateAssignment").data("kendoGrid");
	                        for (var i = 0; i < arrSelected.length; i++) {
	                           grd.dataSource.add({
	                                Id: arrSelected[i].Id,
	                                FirstName: arrSelected[i].FirstName,
	                                LastName: arrSelected[i].LastName,
	                                Email: arrSelected[i].Email,
	                                Index: arrSelected[i].Index,
	                                ShowResults: true
	                            });
	                        }
	                        arrSelected = [];
	                        }, 1000);
	                    });
	                }
	            },
	            onFinishing: function(event, currentIndex) {
	                form.validate().settings.ignore = ":disabled";
	                return form.valid();
	            },
	            onFinished: function (event, currentIndex) {
	                var grd = $("#grdCandidateAssignment").data("kendoGrid").dataSource.data();
	                constants.hidden.$candidateString().val(JSON.stringify(grd));

	                $.ajax({
	                    url: $scope.Configs.Apis.Assignment.onAssignExamSchedule,
	                    type: "POST",
	                    data: $(document.forms[0]).serialize(),
	                    success: function (result) {
	                        if (result.IsSuccess) {
	                            $utils.showMessage(result.Message, $scope.Enums.messageType.Success);
	                            window.close();
	                            debugger;
	                            $("#scheduler").data("kendoScheduler").dataSource.read();
	                            //var editedEventUID = $(".k-scheduler-edit-form").data("uid");
	                            //var event = scheduler.occurrenceByUid(editedEventUID);
	                            //event.set("title", "UPDATED TITLE");
	                        } else
	                            $utils.showMessage(result.Message, $scope.Enums.messageType.Error);
	                    },
	                    error: function (result) {
	                        //if (result.responseText.indexOf($scope.Messages.answerCannotBeNull) !== -1)
	                        //    $utils.showMessage($scope.Messages.answerCannotBeNull, $scope.Enums.messageType.Warning);
	                        //if (result.responseText.indexOf($scope.Messages.correctAnswerisRequired) !== -1)
	                        //    $utils.showMessage($scope.Messages.correctAnswerisRequired, $scope.Enums.messageType.Warning);

	                    }
	                });
	                
	            }
	        });

    		constants.datePicker.$dateStart().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
    		constants.datePicker.$dateEnd().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();

    		constants.checkboxes.$nonEmployee().click(function () {
    		    if ($(this).is(':checked')) {
    		        constants.divs.$txtbehalfEmail().show();
    		        constants.divs.$sendCalendarInvite().hide();
    		    }
    		    else {
    		        constants.divs.$txtbehalfEmail().hide();
    		        if (constants.comboboxes.$ddlIsExamAvailable().val() !== "") {
		                if (constants.comboboxes.$ddlIsExamAvailable().val() === "False") {
		                    constants.divs.$sendCalendarInvite().show();
		                } else {
		                    constants.divs.$sendCalendarInvite().hide();
		                }
		            }
		        }
    		});
    	});
    };


    $module.checkedChangeEvent = function () {
        var constants = $this.settings.constants;
        var $kgrid = $("#grdEmployeeSelection").data("kendoGrid");
        var $chks = $kgrid.wrapper.find(".chkList");

        function bindAllCheckBoxState() {
            constants.checkboxes.$chkAll().prop("checked", ($kgrid.wrapper.find(".chkList:checked").length === $chks.length));
        };

        function bindTableCheckBoxstate() {
            $kgrid.dataSource.data()[$(this).attr("data-index")].IsSelected = $(this).prop("checked");
            bindAllCheckBoxState();
        };

        constants.checkboxes.$chkAll().unbind("click").click(function() {
            var isChecked = $(this).prop("checked");
            $kgrid.wrapper.find(".chkList").each(function() {
                $kgrid.dataSource.data()[$(this).attr("data-index")].IsSelected = isChecked;
                $(this).prop("checked", isChecked);
            }).click(bindTableCheckBoxstate);
        });

        $kgrid.wrapper.find(".chkList").click(bindTableCheckBoxstate);
        bindAllCheckBoxState();
    };

    $module.onEmployeeSelectionDataBound = function () {
        $module.checkedChangeEvent();
    };
   
    $module.getCascadeValues = function () {
    	return {
    		examTypeId: $("#ddlExamType").val()
    	}
    };

    $module.getSectionCascadeValues = function() {
        return {
            divisionCode: $("#ddlDivision").val()
        }
    };

    $module.ddlDivisionOnChange = function () {
        var constants = $this.settings.constants,
               $kgrid = constants.grid.$grdEmployeeSelection().data("kendoGrid");
            if ($kgrid.dataSource.page() !== 1) {
                $kgrid.dataSource.page(1);
            }
            $kgrid.dataSource.read();
    };

    $module.ddlSectionOnChange = function () {
        var constants = $this.settings.constants,
               $kgrid = constants.grid.$grdEmployeeSelection().data("kendoGrid");
        if ($kgrid.dataSource.page() !== 1) {
            $kgrid.dataSource.page(1);
        }
        $kgrid.dataSource.read();
    };

    $module.ddlDesignationOnChange = function () {
        var constants = $this.settings.constants,
               $kgrid = constants.grid.$grdEmployeeSelection().data("kendoGrid");
        if ($kgrid.dataSource.page() !== 1) {
            $kgrid.dataSource.page(1);
        }
        $kgrid.dataSource.read();
    };

    $module.getEmployeeSelectionGridParameters = function() {
        return {
            query: {
                DivisionCode: $("#ddlDivision").val(),
                SectionCode: $("#SectionCode").val(),
                Designation: $("#Designation").val()
            }
        }
    };

    $module.getNonEmployeeSelectionGridParameters = function () {
        return {
            query: {
                FromDate: $("#FromDate").val(),
                ToDate: $("#EndDate").val()
            }
        }
    };

    $module.getCandidateAssignmentGridParameters = function() {
        return {
                scheduleId: 0
        }
    };
    
    $module.ddlExamAvailableOnChange = function () {
        var constants = $this.settings.constants;
        if (constants.comboboxes.$ddlIsExamAvailable().val() !== "") {
            if (constants.comboboxes.$ddlIsExamAvailable().val() === "True") {
                constants.datePicker.$dateStart().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
                constants.datePicker.$dateEnd().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
                    constants.divs.$sendCalendarInvite().hide();
            } else {
                constants.datePicker.$dateStart().offsetParent().find(".k-select").find(".k-icon.k-i-clock").show();
                constants.datePicker.$dateEnd().offsetParent().find(".k-select").find(".k-icon.k-i-clock").show();
                if (constants.checkboxes.$nonEmployee().is(':checked')) {
                    constants.divs.$sendCalendarInvite().hide();
                } else {
                    constants.divs.$sendCalendarInvite().show();
                }
            }
        } else {
            constants.datePicker.$dateStart().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
            constants.datePicker.$dateEnd().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
            if (constants.checkboxes.$nonEmployee().is(':checked')) {
                constants.divs.$sendCalendarInvite().hide();
            } else {
                constants.divs.$sendCalendarInvite().show();
            }
        }
    };
    
    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */

        }, options);

        $this.bindControls();
        $this.bindEvents();
        
    };
  
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.Assignment, window.Exam.Handlers.Assignment || {}));

///#source 1 1 /Scripts/app/boot.js
/*  
* Author: Hasitha Liyanage
* $Date: 10-15-2015
-----------------------------------------------------------------------------------------
* $Rev: 001 $
*/
window.Exam = (function ($scope) {
    /// <summary>
    /// Application bootup 
    /// </summary>
    /// <param name="$scope"> Application Scope </param>    
    $scope.boot = function() {

        $scope.Configs.App.init(function () {
            /// <summary>
            /// Initialize handlers based on the controller execution
            /// </summary>
            /// <returns type=""></returns>
            if ($scope.PageMode.controller === "QuestionBank" && $scope.PageMode.action === "Index")
                $scope.Handlers.QuestionBank.init();
            if ($scope.PageMode.controller === "QuestionBank" && $scope.PageMode.action === "Create")
                $scope.Handlers.QuestionBank.init();
            if ($scope.PageMode.controller === "QuestionBank" && $scope.PageMode.action === "Edit")
                $scope.Handlers.QuestionBank.init();
            if ($scope.PageMode.controller === "Exam" && $scope.PageMode.action === "Index")
                $scope.Handlers.ExamManager.init();
            if ($scope.PageMode.controller === "Exam" && $scope.PageMode.action === "Create" )
                $scope.Handlers.ExamManager.init();
            if ($scope.PageMode.controller === "Exam" && $scope.PageMode.action === "Edit") 
                $scope.Handlers.ExamManager.init();
            if ($scope.PageMode.controller === "Assignment" && $scope.PageMode.action === "Index") {
                $scope.Handlers.Assignment.init();
                $scope.Handlers.QuestionBank.init();
            }

        });

        return true;
    }();
    
    return $scope;

}(window.Exam));

 
