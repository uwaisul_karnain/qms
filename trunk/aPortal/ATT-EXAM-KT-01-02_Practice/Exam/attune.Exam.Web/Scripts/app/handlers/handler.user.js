﻿/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.User = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;
    var isPasswordConfirmed = false;

    $this = {
        settings: {},
        bindControls: function() {
            this.settings.constants = {
                $grid: function() { return $("#grid") },
                buttons: {
                    $passwordConfirm: function () { return $("#passwordConfirm"); },
                    $addUser: function () { return $("#UserAdd"); }
                },
                textboxes: {
                    $password: function() { return $("#Password"); },
                    $confPassword: function () { return $("#confPassword"); },
                    $userName : function() { return $("#UserName"); }
                },
                modals: {
                    $confirmPassword: function () { return $("#confirmPassword"); }
                },
                checkboxes:{
                    $IsActive: function () { return $("#IsActive"); },
                    $IsAdmin: function () { return $("#IsAdmin"); }
                }
            }
        },

        bindEvents: function () {
            var constants = $this.settings.constants;
                       
        }
    };

    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */

        }, options);

        $this.bindControls();
        $this.bindEvents();


    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.User, window.Exam.Handlers.User || {}));
