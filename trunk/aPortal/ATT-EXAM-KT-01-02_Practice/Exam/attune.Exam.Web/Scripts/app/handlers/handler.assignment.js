﻿/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.Assignment = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;

    $this = {
        settings: {},
        bindControls: function () {
            this.settings.constants = {
                grid:
                {
                    $grdEmployeeSelection: function () { return $("#grdEmployeeSelection"); },
                    $grdCandidateAssignment: function () { return $("#grdCandidateAssignment"); }
                },
                buttons: {
                    $btnEmpSearch: function () { return $("#btnEmpSearch"); }
                    
                },
                comboboxes: {
                    $ddlExamType: function () { return $("#ddlExamType"); },
                    $ddlIsExamAvailable: function () { return $("#IsExamAvailable"); },
                    $ddlDivision: function () { return $("#ddlDivision"); },
                    $ddlSectionCode: function () { return $("#SectionCode"); },
                    $ddlDesignation: function () { return $("#Designation"); }
                },
               windows: {
                	$assignmentWindow: function () { return $("#assignmentWindow"); }
                },
                hidden: {
                    $candidateString: function () { return $("#CandidateString"); }
                    
                },
                textBoxes:{
                    $txtQuestion: function () { return $("#Question"); },
                    $dtFromDate: function () { return $("#FromDate"); },
                    $dtToDate: function () { return $("#ToDate"); }
                    
                },
                datePicker: {
                    $dateStart: function () { return $("#Start"); },
                    $dateEnd:function () { return $("#End"); }
                },
                checkboxes: {
                    $nonEmployee: function () { return $("#NonEmployee"); },
                    $chkAll: function () { return $("#chkAll"); }
                },
                divs: {
                    $sendCalendarInvite: function () { return $("#calendarInvite"); },
                    $txtbehalfEmail: function () { return $("#behalfEmail"); }
                }
                
            }
        },

        bindEvents: function () {
            var constants = $this.settings.constants;
            //var $chks = $kgrid.wrapper.find(".chkList");
            //constants.buttons.$btnEmpSearch().click(function (e) {
            //    //e.preventDefault();
            //    debugger;
            //    if (!$utils.isFormValid()) return;
            //    if ($kgrid.dataSource.page() !== 1) {
            //        $kgrid.dataSource.page(1);
            //    }
            //    $kgrid.dataSource.read();
            //    setTimeout(function () {
            //        if ($kgrid.dataSource.data().length === 0) {
            //            $utils.showMessage($scope.Messages.noRecordsFound, $scope.Enums.messageType.Warning);
            //            constants.placeholders.$divNoRecordsMsg().css("display", "block");
            //        } else {
            //            constants.placeholders.$divNoRecordsMsg().css("display", "none");
            //        }
            //    }, 500);
            //});
        }
    };

    $module.onChangeSchedular = function (e) {
        var constants = $this.settings.constants;

    	e.preventDefault();
    	var start = e.start; //selection start date
    	var end = e.end; //selection end date
    	var slots = e.slots; //list of selected slots
    	var events = e.events; //list of selected Scheduler events
        
    	$service.onAssignmentView({
    		model: {
    			Start: start,
    			End: end,
    			ScheduleId: e.scheduleId
		    }
    	}, function (response) {
    		var window = constants.windows.$assignmentWindow().data("kendoWindow");
    		window.center();
    		window.content(response);
    		window.open();
    		var arrSelected = [];
    		var form = $("#frmAssignment").show();
	        var $form = $(document.forms[0]);
	         //   $validator = $form.data("kendoValidator");
	        
	        form.steps({
	            headerTag: "h3",
	            bodyTag: "fieldset",
	            transitionEffect: "slideLeft",
	            onStepChanging: function(event, currentIndex, newIndex) {
	                //if ($validator.validate()) { //TODO form validation

	                //}
	                // Allways allow previous action even if the current form is not valid!
	                if (currentIndex > newIndex) {
	                    return true;
	                }
	                if (currentIndex === 1 ) {
	                    var items = $("#grdEmployeeSelection").data("kendoGrid").dataSource.data();
	                    for (var j = 0, i = items.length; j < i; j++) {
	                        var dataItem = items[j];
	                        if (dataItem.IsSelected) {
	                            arrSelected.push(dataItem);
	                        }
	                    }
	                }
	                // Needed in some cases if the user went back (clean up)
	                if (currentIndex < newIndex) {
	                    // To remove error styles
	                    form.find(".body:eq(" + newIndex + ") label.error").remove();
	                    form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
	                }
	                form.validate().settings.ignore = ":disabled,:hidden";
	                return form.valid();
	            },
	            onStepChanged: function(event, currentIndex, priorIndex) {
	                if (currentIndex === 1 && priorIndex === 0) {
	                    if ($("#NonEmployee").is(":checked")) {
	                        $service.onNonEmployeeSelection({
	                            model: {
	                                NonEmployee: $("#NonEmployee").is(":checked"),
	                                ScheduleId: e.scheduleId
	                            }
	                        }, function(response) {
	                            $("#empSelection").html(response);
	                            if (currentIndex === 1) {
	                                $module.checkedChangeEvent();
	                            }
	                        });
	                    }
	                    else {
	                        $service.onEmployeeSelection({
	                            model: {
	                                NonEmployee: $("#NonEmployee").is(":checked"),
	                                ScheduleId: e.scheduleId
	                            }
	                        }, function(response) {
	                            $("#empSelection").html(response);
	                            if (currentIndex === 1) {
	                                $module.checkedChangeEvent();
	                            }
	                        });
	                    }
	                }
	                if (currentIndex === 2 && priorIndex === 1) {
	                    $service.onCandidateAssignment({
	                        model: {
	                            ScheduleId: e.scheduleId
	                        }
	                    }, function (response) {
	                        $("#candidateAssign").html(response);

	                        setTimeout(function () {
	                        var grd = $("#grdCandidateAssignment").data("kendoGrid");
	                        for (var i = 0; i < arrSelected.length; i++) {
	                           grd.dataSource.add({
	                                Id: arrSelected[i].Id,
	                                FirstName: arrSelected[i].FirstName,
	                                LastName: arrSelected[i].LastName,
	                                Email: arrSelected[i].Email,
	                                Index: arrSelected[i].Index,
	                                ShowResults: true
	                            });
	                        }
	                        arrSelected = [];
	                        }, 1000);
	                    });
	                }
	            },
	            onFinishing: function(event, currentIndex) {
	                form.validate().settings.ignore = ":disabled";
	                return form.valid();
	            },
	            onFinished: function (event, currentIndex) {
	                var grd = $("#grdCandidateAssignment").data("kendoGrid").dataSource.data();
	                constants.hidden.$candidateString().val(JSON.stringify(grd));

	                $.ajax({
	                    url: $scope.Configs.Apis.Assignment.onAssignExamSchedule,
	                    type: "POST",
	                    data: $(document.forms[0]).serialize(),
	                    success: function (result) {
	                        if (result.IsSuccess) {
	                            $utils.showMessage(result.Message, $scope.Enums.messageType.Success);
	                            window.close();
	                            debugger;
	                            $("#scheduler").data("kendoScheduler").dataSource.read();
	                            //var editedEventUID = $(".k-scheduler-edit-form").data("uid");
	                            //var event = scheduler.occurrenceByUid(editedEventUID);
	                            //event.set("title", "UPDATED TITLE");
	                        } else
	                            $utils.showMessage(result.Message, $scope.Enums.messageType.Error);
	                    },
	                    error: function (result) {
	                        //if (result.responseText.indexOf($scope.Messages.answerCannotBeNull) !== -1)
	                        //    $utils.showMessage($scope.Messages.answerCannotBeNull, $scope.Enums.messageType.Warning);
	                        //if (result.responseText.indexOf($scope.Messages.correctAnswerisRequired) !== -1)
	                        //    $utils.showMessage($scope.Messages.correctAnswerisRequired, $scope.Enums.messageType.Warning);

	                    }
	                });
	                
	            }
	        });

    		constants.datePicker.$dateStart().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
    		constants.datePicker.$dateEnd().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();

    		constants.checkboxes.$nonEmployee().click(function () {
    		    if ($(this).is(':checked')) {
    		        constants.divs.$txtbehalfEmail().show();
    		        constants.divs.$sendCalendarInvite().hide();
    		    }
    		    else {
    		        constants.divs.$txtbehalfEmail().hide();
    		        if (constants.comboboxes.$ddlIsExamAvailable().val() !== "") {
		                if (constants.comboboxes.$ddlIsExamAvailable().val() === "False") {
		                    constants.divs.$sendCalendarInvite().show();
		                } else {
		                    constants.divs.$sendCalendarInvite().hide();
		                }
		            }
		        }
    		});
    	});
    };


    $module.checkedChangeEvent = function () {
        var constants = $this.settings.constants;
        var $kgrid = $("#grdEmployeeSelection").data("kendoGrid");
        var $chks = $kgrid.wrapper.find(".chkList");

        function bindAllCheckBoxState() {
            constants.checkboxes.$chkAll().prop("checked", ($kgrid.wrapper.find(".chkList:checked").length === $chks.length));
        };

        function bindTableCheckBoxstate() {
            $kgrid.dataSource.data()[$(this).attr("data-index")].IsSelected = $(this).prop("checked");
            bindAllCheckBoxState();
        };

        constants.checkboxes.$chkAll().unbind("click").click(function() {
            var isChecked = $(this).prop("checked");
            $kgrid.wrapper.find(".chkList").each(function() {
                $kgrid.dataSource.data()[$(this).attr("data-index")].IsSelected = isChecked;
                $(this).prop("checked", isChecked);
            }).click(bindTableCheckBoxstate);
        });

        $kgrid.wrapper.find(".chkList").click(bindTableCheckBoxstate);
        bindAllCheckBoxState();
    };

    $module.onEmployeeSelectionDataBound = function () {
        $module.checkedChangeEvent();
    };
   
    $module.getCascadeValues = function () {
    	return {
    		examTypeId: $("#ddlExamType").val()
    	}
    };

    $module.getSectionCascadeValues = function() {
        return {
            divisionCode: $("#ddlDivision").val()
        }
    };

    $module.ddlDivisionOnChange = function () {
        var constants = $this.settings.constants,
               $kgrid = constants.grid.$grdEmployeeSelection().data("kendoGrid");
            if ($kgrid.dataSource.page() !== 1) {
                $kgrid.dataSource.page(1);
            }
            $kgrid.dataSource.read();
    };

    $module.ddlSectionOnChange = function () {
        var constants = $this.settings.constants,
               $kgrid = constants.grid.$grdEmployeeSelection().data("kendoGrid");
        if ($kgrid.dataSource.page() !== 1) {
            $kgrid.dataSource.page(1);
        }
        $kgrid.dataSource.read();
    };

    $module.ddlDesignationOnChange = function () {
        var constants = $this.settings.constants,
               $kgrid = constants.grid.$grdEmployeeSelection().data("kendoGrid");
        if ($kgrid.dataSource.page() !== 1) {
            $kgrid.dataSource.page(1);
        }
        $kgrid.dataSource.read();
    };

    $module.getEmployeeSelectionGridParameters = function() {
        return {
            query: {
                DivisionCode: $("#ddlDivision").val(),
                SectionCode: $("#SectionCode").val(),
                Designation: $("#Designation").val()
            }
        }
    };

    $module.getNonEmployeeSelectionGridParameters = function () {
        return {
            query: {
                FromDate: $("#FromDate").val(),
                ToDate: $("#EndDate").val()
            }
        }
    };

    $module.getCandidateAssignmentGridParameters = function() {
        return {
                scheduleId: 0
        }
    };
    
    $module.ddlExamAvailableOnChange = function () {
        var constants = $this.settings.constants;
        if (constants.comboboxes.$ddlIsExamAvailable().val() !== "") {
            if (constants.comboboxes.$ddlIsExamAvailable().val() === "True") {
                constants.datePicker.$dateStart().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
                constants.datePicker.$dateEnd().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
                    constants.divs.$sendCalendarInvite().hide();
            } else {
                constants.datePicker.$dateStart().offsetParent().find(".k-select").find(".k-icon.k-i-clock").show();
                constants.datePicker.$dateEnd().offsetParent().find(".k-select").find(".k-icon.k-i-clock").show();
                if (constants.checkboxes.$nonEmployee().is(':checked')) {
                    constants.divs.$sendCalendarInvite().hide();
                } else {
                    constants.divs.$sendCalendarInvite().show();
                }
            }
        } else {
            constants.datePicker.$dateStart().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
            constants.datePicker.$dateEnd().offsetParent().find(".k-select").find(".k-icon.k-i-clock").hide();
            if (constants.checkboxes.$nonEmployee().is(':checked')) {
                constants.divs.$sendCalendarInvite().hide();
            } else {
                constants.divs.$sendCalendarInvite().show();
            }
        }
    };
    
    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */

        }, options);

        $this.bindControls();
        $this.bindEvents();
        
    };
  
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.Assignment, window.Exam.Handlers.Assignment || {}));
