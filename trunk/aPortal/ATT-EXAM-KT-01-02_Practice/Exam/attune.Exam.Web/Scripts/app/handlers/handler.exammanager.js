﻿/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.ExamManager = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;
    var oldExamTypeValue, oldQuestionPickingMode;
    $this = {
        settings: {},
        bindControls: function () {
            this.settings.constants = {
                grid:
                {
                    $examManager: function () { return $("#grdExamManager"); },
                    $gridQuestionCategory: function () { return $("#gridQuestionCategory"); }
                },
                buttons: {
                    $btnGo: function () { return $("#btnGo"); },
                    $btnCreate: function () { return $("#btnCreate"); },
                    $btnUpdate: function () { return $("#btnUpdate"); },
                    $btnEditSave: function () { return $("#btnEditSave"); }
                },
                comboboxes: {
                    $ddlExamType: function () { return $("#ddlExamType"); },
                    $ddlExamId: function () { return $("#FilterView_ExamId"); },
                    $ddlQuestionPick: function () { return $("#IsManualPick"); }
                    
                },
                placeholders: {
                    $divQsBankGrid: function () { return $("#divQsBank"); }
                },
                hidden: {
                    $questionCategoryString: function () { return $("#QuestionCategoryString"); },
                    $hdnmode: function () { return $("#mode"); },
                    $hdnExamId: function () { return $("#ExamId"); },
                    
                },
                anchors: {
                    $aRowAdd: function () { return $("#aRowAdd"); }
                },
                textBox: {
                    $duration: function () { return $("#Duration"); },
                    $passScore: function () { return $("#PassScore"); }
                }
            }
        },

        bindEvents: function () {
            var constants = $this.settings.constants,
                $kgrid = constants.grid.$examManager().data("kendoGrid"),
                $categoryGrid = constants.grid.$gridQuestionCategory().data("kendoGrid");
               
            constants.buttons.$btnGo().click(function () {
                if (!$utils.isFormValid()) return;
                if ($kgrid.dataSource.page() !== 1) {
                    $kgrid.dataSource.page(1);
                }
                $kgrid.dataSource.read();
                setTimeout(function () {
                    if ($kgrid.dataSource.data().length === 0) {
                        $utils.showMessage($scope.Messages.noRecordsFound, $scope.Enums.messageType.Warning);
                    } 
                }, 500);
            });
            constants.buttons.$btnCreate().click(function (e) {
                e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $categoryGrid.dataSource.data(),
                    error = false;
                constants.hidden.$questionCategoryString().val(JSON.stringify($gdata));
                if ($validator.validate()) {
                    if ($gdata.length > 0) {
                        for (var a = 0; a < $gdata.length; a++) {
                            if ($gdata[a].CountValue == null || $gdata[a].CountValue===0) {
                                error = true;
                            }
                        }
                        if (!error) {
                            $module.SaveExam($scope.Configs.Apis.ExamManager.createExam);
                        } else {
                            $utils.showMessage($scope.Messages.addQuestionCategories, $scope.Enums.messageType.Warning);
                        }
                    } else {
                        $utils.showMessage($scope.Messages.addQuestionCategories, $scope.Enums.messageType.Warning);
                    }
                }
            });
            constants.buttons.$btnUpdate().click(function (e) {
                e.preventDefault();
                $categoryGrid = constants.grid.$gridQuestionCategory().data("kendoGrid");
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $categoryGrid.dataSource.data(),
                    error = false;
                constants.hidden.$questionCategoryString().val(JSON.stringify($gdata));
                if ($validator.validate()) {
                    if ($gdata.length > 0) {
                        for (var a = 0; a < $gdata.length; a++) {
                            if ($gdata[a].CountValue == null || $gdata[a].CountValue === 0) {
                                error = true;
                            }
                        }
                        if (!error) {
                            $module.SaveExam($scope.Configs.Apis.ExamManager.updateExam);
                        } else {
                            $utils.showMessage($scope.Messages.addQuestionCategories, $scope.Enums.messageType.Warning);
                        }
                    } else {
                        $utils.showMessage($scope.Messages.addQuestionCategories, $scope.Enums.messageType.Warning);
                    }
                }
            });
            constants.anchors.$aRowAdd().click(function (e) {
                e.preventDefault();
                var isValid = true;
                if (constants.comboboxes.$ddlExamType().val() === "") {
                    $categoryGrid.cancelRow();
                    $utils.showMessage($scope.Messages.selectExamType, $scope.Enums.messageType.Warning);
                    isValid = false;
                }
                else if (constants.comboboxes.$ddlQuestionPick().val() === "") {
                    $categoryGrid.cancelRow();
                    $utils.showMessage($scope.Messages.selectQuestionPickingMode, $scope.Enums.messageType.Warning);
                    isValid = false;
                }
                if (isValid && constants.comboboxes.$ddlQuestionPick().val() !== "") {
                    $categoryGrid.dataSource.add();
                }
            });
        }
    };

    $module.SaveExam = function (url) {
        $.ajax({
            url: url,
            type: "POST",
            data: $(document.forms[0]).serialize(),
            success: function (result) {
                if (result.IsSuccess) {
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Success);
                    window.location = $scope.Configs.Apis.ExamManager.examList;
                } else
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Error);
            },
            error: function (result) {
                if (result.responseText.indexOf($scope.Messages.recordAlreadyExist) !== -1)
                    $utils.showMessage($scope.Messages.recordAlreadyExist, $scope.Enums.messageType.Warning);
                else if (result.responseText.indexOf($scope.Messages.cannotAddDuplicateQuestionCategories) !== -1)
                    $utils.showMessage($scope.Messages.cannotAddDuplicateQuestionCategories, $scope.Enums.messageType.Warning);
                else if (result.responseText.indexOf($scope.Messages.questionCountExceeded) !== -1)
                    $utils.showMessage($scope.Messages.questionCountExceeded, $scope.Enums.messageType.Warning);
                else
                    $utils.showMessage($scope.Messages.failed, $scope.Enums.messageType.Error);
            }
        });
    };

    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */

        }, options);

        $this.bindControls();
        $this.bindEvents();

        $module.clearData();
        if (parseInt($("#mode").val()) === $scope.Enums.mode.Edit) {
            $("#ddlExamType").data("kendoDropDownList").readonly();
            $("#IsManualPick").data("kendoDropDownList").readonly();
        }
    };

    $module.getCascadeValues = function () {
        return {
            examTypeId: $("#ddlExamType").val()
        }
    };

    $module.onDataBound = function (e) {
        var constants = $this.settings.constants,
             grid = constants.grid.$examManager().data("kendoGrid");
        $utils.Kendo.Grid.onDataBound(e);
        constants.grid.$examManager().find("button[data-mode=deactivate]").click(function (e) {
            e.preventDefault();
            var $row = $(this);
            $utils.showConfirmation($scope.Messages.deactivateRecordsPopupTitle, $scope.Messages.deactivateRecordsPopupContent, function (response) {
                if (response === "yes") {
                    var rowId = $row.attr("rowid");
                    var ExamId = rowId;
                    $service.onExamDeactivate({
                        Id: ExamId
                        }, function(response) {
                            if (response.Type === $scope.Enums.messageType.Success) {
                                $utils.showMessage(response.Message, $scope.Enums.messageType.Success);
                                if (grid.dataSource.page() !== 1) {
                                    grid.dataSource.page(1);
                                }
                                grid.dataSource.read();
                            } else
                                $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        },
                        function(response) {
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        });
                }
            });
        });
    };

    $module.getGridParameters = function () {
        return {
            query: {
                ExamTypeId: $("#ddlExamType").val(),
                ExamId: $("#FilterView_ExamId").val()
            }
        }
    };

    $module.clearData = function () {
        $("#ddlExamType").val("");
        $("#FilterView_ExamId").val("");
    };

    $module.ddlExamTypeOnChange = function (e) {
        var constants = $this.settings.constants,
        categoryGrid = constants.grid.$gridQuestionCategory().data("kendoGrid");
        if (constants.comboboxes.$ddlExamType().val() !== "") {
            if (categoryGrid.dataSource.data().length === 0) {
                oldExamTypeValue = constants.comboboxes.$ddlExamType().val();
                $service.onCategoryDropDownRead({
                    examTypeId: constants.comboboxes.$ddlExamType().val()
                }, function(response) {
                });
            } else {
                if (constants.comboboxes.$ddlExamType().prop("readonly") === false) {
                    $utils.showConfirmation($scope.Messages.deleteRecordsPopupTitle, $scope.Messages.deleteRecordsPopupContent, function(response) {
                        if (response === "yes") {
                            categoryGrid.dataSource.data([]);
                            $service.onCategoryDropDownRead({
                                examTypeId: constants.comboboxes.$ddlExamType().val()
                            }, function(response) {
                            });
                        } else {
                            constants.comboboxes.$ddlExamType().data("kendoDropDownList").value(oldExamTypeValue);
                        }
                    });
                }
            }
        }
    };
    
    $module.ddlQuestionPickOnChange = function () {
        var constants = $this.settings.constants,
        categoryGrid = constants.grid.$gridQuestionCategory().data("kendoGrid");
        if (constants.comboboxes.$ddlQuestionPick().val() !== "") {
            if (categoryGrid.dataSource.data().length === 0) {
                oldQuestionPickingMode = constants.comboboxes.$ddlQuestionPick().val();
            } else {
                if (constants.comboboxes.$ddlQuestionPick().prop("readonly") === false) {
                    $utils.showConfirmation($scope.Messages.deleteRecordsPopupTitle, $scope.Messages.deleteRecordsPopupContent, function (response) {
                        if (response === "yes") {
                            categoryGrid.dataSource.data([]);
                            oldQuestionPickingMode = constants.comboboxes.$ddlQuestionPick().val();
                        } else {
                            constants.comboboxes.$ddlQuestionPick().data("kendoDropDownList").value(oldQuestionPickingMode);
                        }
                    });
                }
            }
        }
    };

    $module.onEditCategory = function (e) {
        e.preventDefault();
        var constants = $this.settings.constants;
        if (constants.comboboxes.$ddlQuestionPick().val() !== "") {
            if (constants.comboboxes.$ddlQuestionPick().val() === "True") {
                e.container.find("input[id=CountValue]").prop('disabled', true);
               } else {
                e.container.find("input[id=CountValue]").prop('disabled', false);
            }
        }
    };

    $module.onDataBoundCategory = function (e) {
        $utils.Kendo.Grid.onDataBound(e);
        setTimeout(function() {
            var constants = $this.settings.constants, $kgrid = constants.grid.$gridQuestionCategory().data("kendoGrid");

            if (constants.comboboxes.$ddlQuestionPick().val() !== "") {
                if (constants.comboboxes.$ddlQuestionPick().val() === "True") {
                    $kgrid.element.find("button[id=editCategory]").show();
                    $kgrid.element.find("button[id=viewCategory]").show();
                } else {
                    $kgrid.element.find("button[id=editCategory]").hide();
                    $kgrid.element.find("button[id=viewCategory]").hide();
                }
            }
 
            constants.grid.$gridQuestionCategory().find("button[data-mode=edit]").each(function(i) {
                var mode = $(this).attr("data-mode");
                $(this).attr("data-index", i).click(function (e) {
                    e.preventDefault();
                    var index = parseInt($(this).attr("data-index")),
                        $gdata = $kgrid.dataSource.data();
                    if ($gdata[index].CategoryId.Key !== "" && $gdata[index].DifficultLevelId.Key !== "") {

                        $module.Questions.init({
                            index: index,
                            query: {
                                QuestionCategoryId: $gdata[index].Id,
                                CategoryId: $gdata[index].CategoryId.Key,
                                DifficultLevelId: $gdata[index].DifficultLevelId.Key,
                                SelectedIndexes: $gdata[index].SelectedIndexes,
                                Action: mode === "edit" ? $scope.Enums.action.Edit : $scope.Enums.action.View, //edit action-2, view action-1
                                Mode: $("#mode").val() // Edit mode-1 , Add mode-2
                            },
                            onQuestionSelected: function (o, sIndexes, cIndex) {
                                $gdata[cIndex].Questions = o;
                                $gdata[cIndex].SelectedIndexes = sIndexes;
                                $gdata[cIndex].CountValue = sIndexes.length;

                                $kgrid.element.find("tbody tr:nth(" + cIndex + ") td:nth(3)").html(sIndexes.length);
                            }
                        });
                    } else {
                        $utils.showMessage($scope.Messages.selectCategory, $scope.Enums.messageType.Warning);
                    }
                });
            });

            constants.grid.$gridQuestionCategory().find("button[data-mode=view]").each(function (i) {
                var mode = $(this).attr("data-mode");
                $(this).attr("data-index", i).click(function(e) {
                    e.preventDefault();
                    var index = parseInt($(this).attr("data-index")),
                        $gdata = $kgrid.dataSource.data();
                    if ($gdata[index].CategoryId.Key !== "" && $gdata[index].DifficultLevelId.Key !== "") {

                        $module.Questions.init({
                            index: index,
                            query: {
                                QuestionCategoryId: $gdata[index].Id,
                                CategoryId: $gdata[index].CategoryId.Key,
                                DifficultLevelId: $gdata[index].DifficultLevelId.Key,
                                SelectedIndexes: $gdata[index].SelectedIndexes,
                                Action: $scope.Enums.action.View, //edit action-2, view action-1
                                Mode: $("#mode").val() // Edit mode-1 , Add mode-2
                            },
                            onQuestionSelected: function(o, sIndexes, cIndex) {
                                $gdata[cIndex].Questions = o;
                                $gdata[cIndex].SelectedIndexes = sIndexes;
                                $gdata[cIndex].CountValue = sIndexes.length;

                                $kgrid.element.find("tbody tr:nth(" + cIndex + ") td:nth(3)").html(sIndexes.length);
                            }
                        });
                    } else {
                        $utils.showMessage("Please select category", $scope.Enums.messageType.Warning);
                    }

                });
            });
            
            constants.grid.$gridQuestionCategory().find("button[data-mode=delete]").click(function (e) {
               var item = constants.grid.$gridQuestionCategory().data("kendoGrid").dataItem($(this).closest("tr"));
               e.preventDefault();
               $utils.showConfirmation($scope.Messages.deleteRecordsCommonPopupTitle, $scope.Messages.deleteRecordsCommonPopupContent, function (response) {
                    if (response === "yes") {
                        var dataSource = constants.grid.$gridQuestionCategory().data("kendoGrid").dataSource;
                        dataSource.remove(item);
                        dataSource.sync();
                    }
                });
            });
        }, 500);
    };
   
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.ExamManager, window.Exam.Handlers.ExamManager || {}));
