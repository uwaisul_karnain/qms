﻿/*
* Author: Hasitha Liyanage
* $Date: 06-23-2016
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.ExamManager.Questions = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// Exam question selection handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this; 
    $this = {
        settings: {},
        bindControls: function() {
            this.settings.constants = {
                grid: {
                    $questionSelectionGrid: function() { return $("#questionSelectionGrid"); }
                },
                checkboxes : {
                    $chkAll: function () { return $("#chkAll"); }
                },
                modals: {
                    $questionSelectionPopUp: function() { return $("#questionSelectionPopup"); }
                },
                buttons: {
                    $assign: function() { return $("#btnAssign"); }
                }
            }
        },

        getGridDataSource : function() {
            return $this.settings.constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource;
        },

        bindEvents: function() {
            var constants = $this.settings.constants,
                query = $this.settings.query,
                $dataSource = $this.getGridDataSource();

            constants.buttons.$assign().click(function(e) {
                e.preventDefault();
                var arrSelected = [],
                    arrSelectedIndexes = [];
                   
                if ($.isFunction($this.settings.onQuestionSelected)) {
                    for (var j = 0, i = $dataSource.data().length; j < i; j++) {
                        var dataItem = $dataSource.data()[j];
                        if (dataItem.IsSelected) {
                            arrSelected.push(dataItem);
                            arrSelectedIndexes.push(dataItem.Index);
                        }
                    }
                    $this.settings.onQuestionSelected(arrSelected, arrSelectedIndexes, $this.settings.index);
                    $module.close();
                }
            });

            var arr = [];
            for (var z = 0, r = $dataSource.data().length; z < r; z++) {
                arr.push(z);
            }

            if (parseInt(query.Mode) === $scope.Enums.mode.Edit) {
                if (query.Action === $scope.Enums.action.View) {
                    constants.buttons.$assign().hide();
                    constants.grid.$questionSelectionGrid().find('.k-grid-header').find('tr').find('th')[0].innerHTML = "";
                    //setTimeout(function() {
                    
                    if (query.SelectedIndexes.length > 0) {
                        for (var v = $dataSource.data().length - 1; v >= 0; v--) {
                            if (query.SelectedIndexes.indexOf(v) === -1) {
                                var dataGrd = constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.at(v);
                                constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.remove(dataGrd);
                            }
                        }
                    } else {
                        for (var a = $dataSource.data().length - 1; a >= 0; a--) {
                            var itemSelected = constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.at(a);
                            if (itemSelected.IsSelected === false) {
                                constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.remove(itemSelected);
                            }
                        }
                    }
                    // }, 500);
                }
                else {
                    $(query.SelectedIndexes).each(function (i) {
                        if (arr.length !== query.SelectedIndexes.length) {
                            var notSelected = $(arr).not(query.SelectedIndexes).get();
                            $(notSelected).each(function(v) {
                                var index = notSelected[v];
                                $dataSource.data()[index].IsSelected = false;
                                constants.grid.$questionSelectionGrid().find(".chkList[data-index=" + index + "]").prop("checked", false);
                            });
                        }
                        var idx = query.SelectedIndexes[i];
                        $dataSource.data()[idx].IsSelected = true;
                        constants.grid.$questionSelectionGrid().find(".chkList[data-index=" + idx + "]").prop("checked", true);
                    });
                }
            }
            else {
                if (query.Action === $scope.Enums.action.View) {
                    constants.buttons.$assign().hide();
                    constants.grid.$questionSelectionGrid().find('.k-grid-header').find('tr').find('th')[0].innerHTML = "";
                    if (query.SelectedIndexes.length > 0) {
                        for (var x = $dataSource.data().length - 1; x >= 0; x--) {
                            if (query.SelectedIndexes.indexOf(x) === -1) {
                                var grd = constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.at(x);
                                constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.remove(grd);
                            }
                        }
                    } else {
                        for (var i = $dataSource.data().length - 1; i >= 0; i--) {
                            var item = constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.at(i);
                            constants.grid.$questionSelectionGrid().data("kendoGrid").dataSource.remove(item);
                        }
                    }
                }
                else {
                    $(query.SelectedIndexes).each(function (i) {
                        if (arr.length !== query.SelectedIndexes.length) {
                            var notSelected = $(arr).not(query.SelectedIndexes).get();
                            $(notSelected).each(function (v) {
                                var index = notSelected[v];
                                $dataSource.data()[index].IsSelected = false;
                                constants.grid.$questionSelectionGrid().find(".chkList[data-index=" + index + "]").prop("checked", false);
                            });
                        }
                        var idx = query.SelectedIndexes[i];
                        $dataSource.data()[idx].IsSelected = true;
                        constants.grid.$questionSelectionGrid().find(".chkList[data-index=" + idx + "]").prop("checked", true);
                    });
                }
            }

            $this.bindCheckChangeEvent();
        },

        bindCheckChangeEvent: function () {
            var constants = $this.settings.constants,
                $kgrid = $this.settings.constants.grid.$questionSelectionGrid().data("kendoGrid"),
                $chks = $kgrid.wrapper.find(".chkList");

            function bindAllCheckBoxState() {
                constants.checkboxes.$chkAll().prop("checked", ($kgrid.wrapper.find(".chkList:checked").length === $chks.length));
            };

            function bindTableCheckBoxstate() {
                $kgrid.dataSource.data()[$(this).attr("data-index")].IsSelected = $(this).prop("checked");

                bindAllCheckBoxState();
            };
           
            constants.checkboxes.$chkAll().unbind("click").click(function () {
                var isChecked = $(this).prop("checked");
                $chks.each(function () {
                    $kgrid.dataSource.data()[$(this).attr("data-index")].IsSelected = isChecked;
                    $(this).prop("checked", isChecked);
                }).click(bindTableCheckBoxstate);
            });

            $chks.click(bindTableCheckBoxstate);
            bindAllCheckBoxState();
        } 
    };


    
    $module.init = function(options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */
            query: {
                QuestionCategoryId: 0,
                CategoryId: 0,
                DifficultLevelId: 0,
                SelectedIndexes: [],
                Action: 0, //Edit action-2, View action-1
                Mode: 0 // Edit mode-1 , Add mode-2
            },
            index: 0,
            onQuestionSelected: null
        }, options);

        $this.bindControls();

        $module.loadQuestions($this.settings.query);
    };

    $module.loadQuestions = function (query) {
        /// <summary>
        /// Loading the question related to category,diffeculty level and category
        /// </summary>
        /// <param name="query"></param> 
        $service.onExamQuestionCategoryView(query, function (response) {
            $module.open(response);
            
       });
    };

    $module.getQuestionSelectionGridParameters = function () {
         return {
            query: $this.settings.query
        }
    };

    $module.open= function (response) {
        var constants = $this.settings.constants;
        var $window = constants.modals.$questionSelectionPopUp().kendoWindow({
            modal: true,
            activate: function () {
                //setTimeout(function(){
                $this.bindEvents();
                //}, 500);
            }
        }).data("kendoWindow");
        
        $window.content(response);
        $window.center();
        $window.open();
    };

    $module.close = function () {
        $this.settings.constants.grid.$questionSelectionGrid().find(".k-grid-content").empty();
        $this.settings.constants.modals.$questionSelectionPopUp().data("kendoWindow").close();
    };

    $module.onDataBound = function () {
        var constants = $this.settings.constants,
                query = $this.settings.query,
                $dataSource = $this.getGridDataSource();

        $this.bindCheckChangeEvent();
       
            if (query.Action === $scope.Enums.action.View) {
                if ($dataSource.data().length > 0) {
                    var trs = constants.grid.$questionSelectionGrid().find('.k-grid-content').find('tr');
                    trs.each(function() {
                        $(this).find("input[type=checkbox]").hide();
                    });
                }
        }
    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.ExamManager, window.Exam.Handlers.ExamManager.Questions || {}));
