﻿/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.QuestionBank.ExamType = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;

    $this = {
        settings: {},
        bindControls: function() {
            this.settings.constants = {
                grid:
                {
                    $gridexamType: function() { return $("#grdExamType"); }
                },
                placeholders: {
                    $examType: function() { return $("#partialExamType"); },
                    $category: function() { return $("#partialCategory"); }
                },
                comboboxes: {
                    $ddlExamType: function() { return $("#ddlExamType"); }
                },
                anchors: {
                    $aRowExamTypeAdd: function() { return $("#aRowExamTypeAdd"); }

                }

            }
        },

        bindEvents: function() {
            var constants = $this.settings.constants,
                $kgrid = constants.grid.$gridexamType().data("kendoGrid"),
                $partialView = constants.placeholders.$examType();

            if ($partialView.html().length === 0) {
                $service.getExamTypes({

                }, function(response) {
                    $partialView.html(response).show();
                });
            } else {
                if ($kgrid) $kgrid.dataSource.read();
                $partialView.hide();
            }

    //Custom row add function remove and kendo default toolbar create function enabled
        //    setTimeout(function() {
        //        constants.anchors.$aRowExamTypeAdd().click(function(e) {

        //            var constants = $this.settings.constants,
        //                $kgrid = constants.grid.$gridexamType().data("kendoGrid");
        //            e.preventDefault();
        //            if ($kgrid.dataSource.page() !== 1) {
        //                $kgrid.dataSource.page(1);
        //            }
        //           // $this.isAdd = true;
        //           // $kgrid.dataSource.add();
        //        });
        //    }, 2000);
        }
        //idAdd: false
    };

   
    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */
            callbacks : {
                onExamTypeUpsert:null
            }
        }, options);

        $this.bindControls();
        $this.bindEvents();

        return true;
    };
    $module.toggle = function (e) {
        if ($this.settings.constants.placeholders.$category().is(":visible"))
            $this.settings.constants.placeholders.$category().hide();
        if ($this.settings.constants.placeholders.$examType().is(":visible"))
            $this.settings.constants.placeholders.$examType().hide();
        else
            $this.settings.constants.placeholders.$examType().show();
    };

    $module.requestEnd = function (e) {
        var constants = $this.settings.constants;
        if (e.response != undefined) {
            if (e.response.Errors == null) {
                if (e.type === "update" || e.type === "create" || e.type === "destroy") {
                    e.sender.read();
                    constants.comboboxes.$ddlExamType().data("kendoDropDownList").dataSource.read();
                    //if ($.isFunction($this.settings.callbacks.onExamTypeUpsert))
                    //    $this.settings.callbacks.onExamTypeUpsert();
                }
            } else {
                if (e.type === "update" || e.type === "create") {
                    e.sender.read();
                }
            }
        }
        };

   
    $module.onDataBoundExamType = function (e) {
        var constants = $this.settings.constants;
         $utils.Kendo.Grid.onDataBound(e);
      /*  if ($this.isAdd) {
            this.wrapper.find(".k-grid-edit:first").click();
            $this.isAdd = false;
        }*/

        constants.grid.$gridexamType().find("button[data-mode=delete]").click(function (e) {
            var item = constants.grid.$gridexamType().data("kendoGrid").dataItem($(this).closest("tr"));
            e.preventDefault();
            $utils.showConfirmation($scope.Messages.deleteRecordsCommonPopupTitle, $scope.Messages.deleteRecordsCommonPopupContent, function (response) {
                if (response === "yes") {
                    if (item.Id !== 0) {
                        $service.onExamTypeDelete({
                            Id: item.Id
                        }, function(response) {
                            if (response.Type === $scope.Enums.messageType.Success) {
                                constants.comboboxes.$ddlExamType().data("kendoDropDownList").dataSource.read();
                                constants.grid.$gridexamType().data("kendoGrid").dataSource.read();
                            }
                        });
                    } else {
                        var dataSource = constants.grid.$gridexamType().data("kendoGrid").dataSource;
                        dataSource.remove(item);
                    }
                }
            });
        });
    };
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.QuestionBank, window.Exam.Handlers.QuestionBank.ExamType || {}));
