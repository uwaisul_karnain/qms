﻿/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.QuestionBank.Category = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;

    $this = {
        settings: {},
        bindControls: function () {
            this.settings.constants = {
                grid:
                {
                    $gridCategory: function () { return $("#grdCategory"); }
                },
                placeholders: {
                    $category: function () { return $("#partialCategory"); },
                    $examType: function () { return $("#partialExamType"); }
                },
                comboboxes: {
                    $ddlCascadeCategory: function () { return $("#ddlCategory"); },
                    $ddlExamType: function () { return $("#ddlExamType"); }
                },
                anchors: {
                    $aRowCategoryAdd: function () { return $("#aRowCategoryAdd"); }
                   
                }
                
            }
        },

        bindEvents: function() {
            var constants = $this.settings.constants,
                $partialView = constants.placeholders.$category();
            $service.getCategories({
                examTypeId: constants.comboboxes.$ddlExamType().val()
            }, function(response) {
                $partialView.html(response).hide();
            });

   //Custom row add function remove and kendo default toolbar create function enabled
            //setTimeout(function(){
            //    constants.anchors.$aRowCategoryAdd().click(function (e) {
            //    var constants = $this.settings.constants,
            //    $kgrid = constants.grid.$gridCategory().data("kendoGrid");
            //    e.preventDefault();
            //    if ($kgrid.dataSource.page() !== 1) {
            //        $kgrid.dataSource.page(1);
            //    }
            //    $kgrid.dataSource.add();
                
            //    });
            //}, 2000);
        }
    };


    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */
            callbacks: {
                onExamTypeUpsert: null
            }
        }, options);

        $this.bindControls();
        $this.bindEvents();

        return true;
    };
    $module.toggle = function (e) {
        if ($this.settings.constants.placeholders.$examType().is(":visible"))
            $this.settings.constants.placeholders.$examType().hide();
        if ($this.settings.constants.placeholders.$category().is(":visible"))
            $this.settings.constants.placeholders.$category().hide();
        else
            $this.settings.constants.placeholders.$category().show();
    };

    $module.getGridParameters = function () {
        return {
                examTypeId: $("#ddlExamType").val()
        }
    };

    $module.onGridSave = function (e) {
            var constants = $this.settings.constants;
            e.model.ExamTypeId = constants.comboboxes.$ddlExamType().val();
            e.model.ExamType = null;
    };

   $module.onGridRemove = function (e) {
      e.model.ExamType = null;
   };

   $module.requestEnd = function (e) {
        var constants = $this.settings.constants;
        if (e.response != undefined) {
            if (e.response.Errors == null) {
                if (e.type === "update" || e.type === "create" || e.type === "destroy") {
                    e.sender.read();
                    constants.comboboxes.$ddlCascadeCategory().data("kendoDropDownList").dataSource.read();
                }
            } else {
                if (e.type === "update" || e.type === "create") {
                    e.sender.read();
                }
            }
        }
    };

   $module.onDataBoundCategory = function (e) {
       var constants = $this.settings.constants;
       $utils.Kendo.Grid.onDataBound(e);

       constants.grid.$gridCategory().find("button[data-mode=delete]").click(function (e) {
           var item = constants.grid.$gridCategory().data("kendoGrid").dataItem($(this).closest("tr"));
           e.preventDefault();
           $utils.showConfirmation($scope.Messages.deleteRecordsCommonPopupTitle, $scope.Messages.deleteRecordsCommonPopupContent, function (response) {
               if (response === "yes") {
                   if (item.Id !== 0) {
                       $service.onCategoryDelete({
                           Id: item.Id
                       }, function (response) {
                           if (response.Type === $scope.Enums.messageType.Success) {
                               constants.comboboxes.$ddlCascadeCategory().data("kendoDropDownList").dataSource.read();
                               constants.grid.$gridCategory().data("kendoGrid").dataSource.read();
                           }
                       });
                   } else {
                       var dataSource = constants.grid.$gridCategory().data("kendoGrid").dataSource;
                       dataSource.remove(item);
                   }
               }
           });
       });
   };
    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.QuestionBank, window.Exam.Handlers.QuestionBank.Category || {}));
