﻿/*
* Author: Hasitha Liyanage
* $Date: 12-21-2015
-----------------------------------------------
* $Rev: 001 $
*/
window.Exam.Handlers.QuestionBank = (function ($scope, $, $utils, $service, $module) {
    /// <summary>
    /// User handler
    /// </summary>
    /// <param name="$scope">Application Scope </param>
    /// <param name="$">jQuery</param>
    /// <param name="$utils"> Utilities </param>
    /// <param name="$service">User apis</param>
    /// <param name="$module">Current module if require to override or extend</param>
    var $this = this;

    $this = {
        settings: {},
        bindControls: function () {
            this.settings.constants = {
                grid:
                {
                    $questionBank: function () { return $("#grdQuestionBank"); },
                    $gridAnswer: function () { return $("#gridAnswer"); },
                    $gridexamType: function () { return $("#grdExamType"); }
                },
                buttons: {
                    $btnGo: function () { return $("#btnGo"); },
                    $btnUploadWindow: function () { return $("#uploadQuestionWindow"); },
                    $btnDownLoadTemplate: function () { return $("#downLoadTemplate"); },
                    $btnSave: function () { return $("#btnSave"); },
                    $btnDraft: function () { return $("#btnDraft"); },
                    $btnEditSave: function () { return $("#btnEditSave"); },
                    $btnEditDraft: function () { return $("#btnEditDraft"); }
                },
                comboboxes: {
                    $ddlExamType: function () { return $("#ddlExamType"); },
                    $ddlCategory: function () { return $("#FilterView_CategoryId"); },
                    $ddlCascadeCategory: function () { return $("#ddlCategory"); },
                    $ddlDifficultLevel: function () { return $("#FilterView_DifficultLevelId"); },
                    $ddlQuestionMode: function () { return $("#FilterView_IsPublished"); },
                    $ddlQuestionType: function () { return $("#FilterView_QuestionTypeId"); }
                },
                placeholders: {
                    $divQsBankGrid: function () { return $("#divQsBank"); },
                    $divNoRecordsMsg: function () { return $("#divNoRecordsMsg"); },
                    $divUploadError: function () { return $("#upload-errors"); }
                },
                windows: {
                    $uploadWindow: function () { return $("#uploadWindow"); }
                },
                hidden: {
                    $answerString: function () { return $("#AnswersString"); },
                    $publishMode: function () { return $("#PublishedMode"); }
                },
                anchors: {
                    $aExamType: function () { return $("#aExamType"); },
                    $aCategory: function () { return $("#aCategory"); },
                    $aRowAdd: function () { return $("#aRowAdd"); }
                },
                textBoxes:{
                 $txtQuestion: function () { return $("#Question"); }
            }
            }
        },

        bindEvents: function () {
            var constants = $this.settings.constants,
                $kgrid = constants.grid.$questionBank().data("kendoGrid"),
                $kanswerGrid = constants.grid.$gridAnswer().data("kendoGrid"),
                isExamTypeInitialized = false;
          
            constants.buttons.$btnGo().click(function () {
                if (!$utils.isFormValid()) return;
                if ($kgrid.dataSource.page() !== 1) {
                    $kgrid.dataSource.page(1);
                }
                $kgrid.dataSource.read();
                setTimeout(function () {
                    if ($kgrid.dataSource.data().length === 0) {
                        $utils.showMessage($scope.Messages.noRecordsFound, $scope.Enums.messageType.Warning);
                        constants.placeholders.$divNoRecordsMsg().css("display", "block");
                    } else {
                        constants.placeholders.$divNoRecordsMsg().css("display", "none");
                    }
                }, 500);
            });
            constants.buttons.$btnUploadWindow().click(function (e) {
                e.preventDefault();
                var window = constants.windows.$uploadWindow().data("kendoWindow");
                window.center();
                window.open();
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            });
            constants.buttons.$btnDownLoadTemplate().click(function (e) {
                e.preventDefault();
                $service.onDownLoadTemplate({
                }, function (result) {
                    window.location = $scope.Configs.Apis.QuestionBank.excelTemplateDownload + "?file=" + result;
                });
            });
            constants.buttons.$btnSave().click(function (e) {
                e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $kanswerGrid.dataSource.data(),
                    $chks = $($kanswerGrid.element).find("input[type=checkbox]");
                var arr = [];
                for (var i = 0, x = $gdata.length; i < x; i++) {
                    $gdata[i]["IsCorrect"] = $($chks[i]).is(":checked");
                    if ($($chks[i]).is(":checked")) {
                        arr.push($($chks[i]).is(":checked"));
                    }
                }
                constants.hidden.$publishMode().val("1");
                constants.hidden.$answerString().val(JSON.stringify($gdata));
               if ($validator.validate()) {
                    if ($("#QuestionTypeId").val() === $scope.Enums.questionType.MultipleCorrect.toString())
                        $module.createQuestionPublish($scope.Configs.Apis.QuestionBank.createQuestion, $scope.Enums.mode.Add);
                    else {
                        if (arr.length > 1)
                            $utils.showMessage($scope.Messages.moreThanOneAnswerCannotBeCorrect, $scope.Enums.messageType.Error);
                        else
                            $module.createQuestionPublish($scope.Configs.Apis.QuestionBank.createQuestion, $scope.Enums.mode.Add);
                    }
                }
            });
            constants.buttons.$btnDraft().click(function (e) {
                e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $kanswerGrid.dataSource.data(),
                    $chks = $($kanswerGrid.element).find("input[type=checkbox]");
                var arr = [];
                for (var i = 0, x = $gdata.length; i < x; i++) {
                    $gdata[i]["IsCorrect"] = $($chks[i]).is(":checked");
                    if ($($chks[i]).is(":checked")) {
                        arr.push($($chks[i]).is(":checked"));
                    }
                }
                constants.hidden.$publishMode().val("0");
                constants.hidden.$answerString().val(JSON.stringify($gdata));
                if ($validator.validate()) {
                    if ($("#QuestionTypeId").val() === $scope.Enums.questionType.MultipleCorrect.toString())
                        $module.createQuestionDraft($scope.Configs.Apis.QuestionBank.createQuestion, $scope.Enums.mode.Add);
                    else {
                        if (arr.length > 1)
                            $utils.showMessage($scope.Messages.moreThanOneAnswerCannotBeCorrect, $scope.Enums.messageType.Error);
                        else
                            $module.createQuestionDraft($scope.Configs.Apis.QuestionBank.createQuestion, $scope.Enums.mode.Add);
                    }
                }
            });
            constants.buttons.$btnEditSave().click(function (e) {
                 e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $kanswerGrid.dataSource.data(),
                    $chks = $($kanswerGrid.element).find("input[type=checkbox]");
                var arr = [];
                for (var i = 0, x = $gdata.length; i < x; i++) {
                    $gdata[i]["IsCorrect"] = $($chks[i]).is(":checked");
                    if ($($chks[i]).is(":checked")) {
                        arr.push($($chks[i]).is(":checked"));
                    }
                }
                constants.hidden.$publishMode().val("1");
                constants.hidden.$answerString().val(JSON.stringify($gdata));
                if ($validator.validate())
                    if ($("#QuestionTypeId").val() === $scope.Enums.questionType.MultipleCorrect.toString())
                        $module.createQuestionPublish($scope.Configs.Apis.QuestionBank.editQuestion, $scope.Enums.mode.Edit);
                    else {
                        if (arr.length > 1)
                            $utils.showMessage($scope.Messages.moreThanOneAnswerCannotBeCorrect, $scope.Enums.messageType.Error);
                        else
                            $module.createQuestionPublish($scope.Configs.Apis.QuestionBank.editQuestion, $scope.Enums.mode.Edit);
                    }
            });
            constants.buttons.$btnEditDraft().click(function (e) {
               
                e.preventDefault();
                var $form = $(document.forms[0]),
                    $validator = $form.data("kendoValidator"),
                    $gdata = $kanswerGrid.dataSource.data(),
                    $chks = $($kanswerGrid.element).find("input[type=checkbox]");
                var arr = [];
                for (var i = 0, x = $gdata.length; i < x; i++) {
                    $gdata[i]["IsCorrect"] = $($chks[i]).is(":checked");
                    if ($($chks[i]).is(":checked")) {
                        arr.push($($chks[i]).is(":checked"));
                    }
                }
                constants.hidden.$publishMode().val("0");
                constants.hidden.$answerString().val(JSON.stringify($gdata));
                if ($validator.validate()) {
                    if ($("#QuestionTypeId").val() === $scope.Enums.questionType.MultipleCorrect.toString())
                        $module.createQuestionDraft($scope.Configs.Apis.QuestionBank.editQuestion,$scope.Enums.mode.Edit);
                    else {
                        if (arr.length > 1)
                            $utils.showMessage($scope.Messages.moreThanOneAnswerCannotBeCorrect, $scope.Enums.messageType.Error);
                        else
                            $module.createQuestionDraft($scope.Configs.Apis.QuestionBank.editQuestion, $scope.Enums.mode.Edit);
                    }
                }
            });
            constants.anchors.$aExamType().click(function(e) {
                e.preventDefault();
                if (!isExamTypeInitialized)
                    isExamTypeInitialized = $module.ExamType.init({
                        onExamTypeUpsert: function () {
                            // constants.comboboxes.$ddlExamType().data("kendoDropDownList").dataSource.read();
                        }
                    });
                else
                    $module.ExamType.toggle();
            });
            constants.anchors.$aCategory().click(function (e) {
                e.preventDefault();
                $module.Category.toggle();
            });
            constants.anchors.$aRowAdd().click(function (e) {
                e.preventDefault();
                if ($kanswerGrid.dataSource.data().length > 10) { 
                    $kanswerGrid.cancelRow();
                    $utils.showMessage($scope.Messages.answerLimit, $scope.Enums.messageType.Warning);
                } else $kanswerGrid.dataSource.add();
            });
            
            $module.clearData();
            $module.kendoFileUpload();
        }
    };
    
    $module.createQuestionPublish = function (url, mode) {
        var constants = $this.settings.constants;
        $.ajax({
            url: url,
            type: "POST",
            data: $(document.forms[0]).serialize(),
            success: function (result) {
                if (result.IsSuccess) {
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Success);
                   if (mode === $scope.Enums.mode.Edit) {
                        window.location = $scope.Configs.Apis.QuestionBank.questionList;
                    } else {
                        constants.grid.$gridAnswer().data("kendoGrid").dataSource.data(result.Owner),
                            constants.textBoxes.$txtQuestion().val("");
                    }
                } else
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Error);
            },
            error: function (result) {
                if (result.responseText.indexOf($scope.Messages.answerCannotBeNull) !== -1)
                    $utils.showMessage($scope.Messages.answerCannotBeNull, $scope.Enums.messageType.Warning);
                if (result.responseText.indexOf($scope.Messages.correctAnswerisRequired) !== -1)
                    $utils.showMessage($scope.Messages.correctAnswerisRequired, $scope.Enums.messageType.Warning);
               
            }
        });
    };
    $module.createQuestionDraft = function (url,mode) {
        var constants = $this.settings.constants;
        $.ajax({
            url: url,
            type: "POST",
            data: $(document.forms[0]).serialize(),
            success: function (result) {
                if(result.IsSuccess) {
                $utils.showMessage(result.Message, $scope.Enums.messageType.Success);
                    if (mode === $scope.Enums.mode.Edit) {
                        window.location = $scope.Configs.Apis.QuestionBank.questionList;
                    } else {
                        constants.grid.$gridAnswer().data("kendoGrid").dataSource.data(result.Owner),
                            constants.textBoxes.$txtQuestion().val("");
                    }
                } else
                    $utils.showMessage(result.Message, $scope.Enums.messageType.Error);
            },
            error: function (result) {
                $utils.showMessage($scope.Messages.failed, $scope.Enums.messageType.Error);
            }
        });
    };

    function onUpload(e) {
         var files = e.files;
        $.each(files, function () {
            if (this.extension.toLowerCase() !== (".xlsx") && this.extension.toLowerCase() !== (".xls") && this.extension.toLowerCase() !== (".xlsm")) {
                $utils.showMessage($scope.Messages.fileFormatWrong, $scope.Enums.messageType.Error);
                e.preventDefault();
            }
        });
    };
    $module.init = function (options) {
        /// <summary>
        /// Module initilization
        /// </summary>
        /// <param name="options"> Module configuratoin oprtions </param>
        /// <returns type="Object"></returns>
        $this.settings = $scope.$.extend(true, {
            /* 
                List default configurations for this module 
             */

        }, options);

        $this.bindControls();
        $this.bindEvents();
    };
    $module.getCascadeValues = function () {
        return {
            examTypeId: $("#ddlExamType").val()
        }
    };

    $module.onDataBound = function (e) {
        $utils.Kendo.Grid.onDataBound(e);
        var constants = $this.settings.constants,
        grid = constants.grid.$questionBank().data("kendoGrid");
        var trs = this.wrapper.data("kendoGrid").content.find('tr');
        if (grid.dataSource.data().length > 0) {
            trs.each(function () {
                var item = grid.dataItem($(this));
                if (item.IsPublished) {
                    $(this).find('td:nth-child(3)')[0].innerHTML = "";
                }
            });
        }
        constants.grid.$questionBank().find("button[data-mode=deactivate]").click(function (e) {
            e.preventDefault();
            var $row = $(this);
            $utils.showConfirmation($scope.Messages.deactivateRecordsPopupTitle, $scope.Messages.deactivateRecordsPopupContent, function (response) {
                if (response === "yes") {
                    var rowId = $row.attr("rowid");
                    var QuestionId = rowId;
                    $service.onDeactivate({
                            Id: QuestionId
                    }, function (response) {
                            if (response.Type === $scope.Enums.messageType.Success) {
                                $utils.showMessage(response.Message, $scope.Enums.messageType.Success);
                                if (grid.dataSource.page() !== 1) {
                                    grid.dataSource.page(1);
                                }
                                grid.dataSource.read();
                            }
                            else
                                $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        },
                        function(response) {
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        });
                }
            });
        });
        constants.grid.$questionBank().find("button[data-mode=publish]").click(function (e) {
            e.preventDefault();
            var $row = $(this);
            $utils.showConfirmation($scope.Messages.publishQuestionPopupTitle, $scope.Messages.publishQuestionPopupContent, function (response) {
                if (response === "yes") {
                    var rowId = $row.attr("rowid");
                    var QuestionId = rowId;
                    $service.onPublish({
                            Id: QuestionId
                    }, function (response) {
                        if (response.Type === $scope.Enums.messageType.Success) {
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Success);
                            if (grid.dataSource.page() !== 1) {
                                grid.dataSource.page(1);
                            }
                            grid.dataSource.read();
                        }
                        else
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Error);
                        },
                        function(response) {
                            $utils.showMessage(response.Message, $scope.Enums.messageType.Error);

                        });
                }
            });
        });
    };
    $module.getGridParameters = function () {
        return {
            query: {
                ExamTypeId: $("#ddlExamType").val(),
                CategoryId: $("#FilterView_CategoryId").val(),
                DifficultLevelId: $("#FilterView_DifficultLevelId").val(),
                Mode: $("#FilterView_IsPublished").val()
            }
        }
    };
    $module.clearData = function () {
        $("#ddlExamType").val("");
        $("#FilterView_CategoryId").val("");
        $("#FilterView_DifficultLevelId").val("");
        $("#FilterView_IsPublished").val("");
    };
    $module.kendoFileUpload = function () {
        var constants = $this.settings.constants;
        var window = constants.windows.$uploadWindow().data("kendoWindow");
        var $kgrid = constants.grid.$questionBank().data("kendoGrid");
        $("#file").kendoUpload({
            async: {
                multiple: false,
                saveUrl: $scope.Configs.Apis.QuestionBank.OnQuestionUpload,
                autoUpload: false
            },
            upload: onUpload,
            success: function (result) {
                if (result.response.Type === $scope.Enums.messageType.Success) {
                    $utils.showMessage(result.response.Message, $scope.Enums.messageType.Success);
                    window.close();
                    if ($kgrid.dataSource.page() !== 1) {
                        $kgrid.dataSource.page(1);
                    }
                    $kgrid.dataSource.read();
                } else if (result.response.Type === $scope.Enums.messageType.Error)
                    constants.placeholders.$divUploadError().html(result.response.UploadResult.Message).show();
                
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            }
        });
    };
    $module.onOpenUploadPopUp = function () {
        $(".k-upload-files").remove();
        $(".k-upload-status").remove();
        $("#upload-errors").empty().hide();
    };
    $module.ddlExamTypeOnChange = function () {
        var constants = $this.settings.constants;
        if (constants.comboboxes.$ddlExamType().val() !== "") {
             //constants.anchors.$aCategory().show();
            constants.anchors.$aCategory().prop('disabled', false);
            $module.Category.init({

            });
        } else {
            //constants.anchors.$aCategory().hide();
            constants.anchors.$aCategory().prop('disabled', true);
        }
    };

    return $module;

}(window.Exam, window.Exam.$, window.Exam.Utils, window.Exam.Services.QuestionBank, window.Exam.Handlers.QuestionBank || {}));
