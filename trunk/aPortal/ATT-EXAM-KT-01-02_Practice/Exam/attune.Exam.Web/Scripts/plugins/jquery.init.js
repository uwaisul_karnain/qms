/* ========================================================================= */
/* BE SURE TO COMMENT CODE/IDENTIFY PER PLUGIN CALL */
/* ========================================================================= */

jQuery(function ($) {

    $('.employ-filters p').on('click', function (e) {
        $('.employ-filters').toggleClass("show");
    });
    //bring to top for working filter show option//

    //Datepicker Script//
    $('#sandbox-container input').datepicker({
        autoclose: true,
        daysOfWeekDisabled: [0, 6],
        todayHighlight: true,
    });
    $('#sandbox-container input').on('show', function(e) {
        console.debug('show', e.date, $(this).data('stickyDate'));

        if (e.date) {
            $(this).data('stickyDate', e.date);
        } else {
            $(this).data('stickyDate', null);
        }
    });
    $('#sandbox-container input').on('hide', function(e) {
        console.debug('hide', e.date, $(this).data('stickyDate'));
        var stickyDate = $(this).data('stickyDate');
        if (!e.date && stickyDate) {
            console.debug('restore stickyDate', stickyDate);
            $(this).datepicker('setDate', stickyDate);
            $(this).data('stickyDate', null);
        }
    });

    //Accordian Script//
    function toggleChevron(e) {
        $(e.target)
            .prev('.panel-heading')
            .find("i.indicator")
            .toggleClass('att-angle-down att-angle-up');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);

    $(document.body).on('click', '.dropdown-menu li', function(event) {

        var $target = $(event.currentTarget);

        $target.closest('.btn-group')
            .find('[data-bind="label"]').text($target.text())
            .end()
            .children('.dropdown-toggle').dropdown('toggle');

        return false;

    });

    $("#valForm").validate({

        showErrors: function(errorMap, errorList) {

            // Clean up any tooltips for valid elements
            $.each(this.validElements(), function(index, element) {
                var $element = $(element);

                $element.data("title", "") // Clear the title - there is no error associated anymore
                    .removeClass("error")
                    .tooltip("destroy");
            });

            // Create new tooltips for invalid elements
            $.each(errorList, function(index, error) {
                var $element = $(error.element);

                $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                    .data("title", error.message)
                    .addClass("error")
                    .tooltip(); // Create a new tooltip based on the error messsage we just set in the title
            });
        },

        submitHandler: function(form) {
            //alert("This is a valid form!");
        }
    });

    $('#reset').click(function() {
        var validator = $("#valForm").validate();
        validator.resetForm();
    });

    $('#example').DataTable();

    var removeClass = true;
    $(".menu").click(function() {
        $(".navigate").toggleClass('slid-nav');
        removeClass = false;
    });
    $(".navigate").click(function() {
        removeClass = false;
    });
    $("html").click(function() {
        if (removeClass) {
            $(".navigate").removeClass('slid-nav');
        }
        removeClass = true;
    });

    $('.arv').click(function() {
        alert('Approved');
    });
    $('.rej').click(function() {
        alert('Reject');
    });
    //$('.action a').click(function() {
    //    alert('Delete');
    //});
    $('.remind').click(function() {
        alert('Remind Send');
    });

    $('[data-toggle="tooltip"]').tooltip();

    //$('.each-col').click(function(e) {
    //    e.preventDefault();
    //    $('.each-col').removeClass('active');
    //    $(this).addClass('active');
    //});

    $('.each-col .detail').on('click', function(e) {
        $('.each-col').removeClass('active');
        $(this).parent().addClass('active');
        //$('.each-col').toggleClass("active");
    });

    $('#calendar').fullCalendar({
        header: {
            left: 'prev, title, next, today',
            center: '',
            right: ''
        },

        //defaultDate: '2015-10-12',
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [{
            title: '2',
            start: '2015-10-01'
        }, {
            title: '8',
            url: 'leaveapprovelist.html',
            start: '2015-11-30',
            className: ["leaved"],
            rendering: 'background',
            backgroundColor: '#eba5a5',
        }, {
            title: '22',
            url: 'leaveapprovelist.html',
            start: '2015-12-14',
            className: ["leaved"],
            rendering: 'background',
            backgroundColor: '#eba5a5',
        }, {
            title: '2',
            start: '2015-11-20'
        }, {
            title: '8',
            url: 'leaveapprovelist.html',
            start: '2015-11-03',
            className: ["leaved"],
            rendering: 'background',
            backgroundColor: '#eba5a5',
            color: '#FFF',
        }, {
            title: '8',
            url: 'leaveapprovelist.html',
            start: '2015-11-10',
            className: ["leaved"],
            rendering: 'background',
            backgroundColor: '#eba5a5',
            color: '#FFF',
        },{
            title: '8',
            url: 'leaveapprovelist.html',
            start: '2015-11-11',
            className: ["leaved"],
            rendering: 'background',
            backgroundColor: '#eba5a5',
            color: '#FFF',
        },  {
            title: '22',
            url: 'leaveapprovelist.html',
            start: '2015-11-25',
            className: ["poyaday"],
            rendering: 'background',
            backgroundColor: 'rgba(155,89,182, 0.6)',
        }, {
            title: '2',
            start: '2015-12-01'
        }, {
            title: '8',
            url: 'leaveapprovelist.html',
            start: '2015-12-28'
        }, {
            title: '22',
            url: 'leaveapprovelist.html',
            start: '2015-12-24'
        }],


    });


    $('.multiselect').multiselect({
        enableCaseInsensitiveFiltering: true,
        maxHeight: 200,
    });
    $("#calendar td").click(function() {
        $('html,body').animate({
                scrollTop: $(".up-lvl").offset().top
            },
            'slow');
    });


    var win = $(window),
        fxel = $('#sticky'),
        //extra = $('.today-list'),
        eloffset = fxel.offset().top;

    win.scroll(function() {
        if (eloffset < win.scrollTop()) {
            fxel.addClass("fixed-scroll");
            extra.addClass("paddingmr");
        } else {
            fxel.removeClass("fixed-scroll");
            extra.removeClass("paddingmr");
        }
    });

});
