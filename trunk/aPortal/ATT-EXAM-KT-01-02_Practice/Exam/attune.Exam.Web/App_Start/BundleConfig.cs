﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System.Web;
using System.Web.Optimization;

namespace attune.Exam.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        /// <summary>
        /// Registers the bundles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            RegisterScriptBundles(bundles);

            RegisterStyleBundles(bundles);
        }

        /// <summary>
        /// Registers the script bundles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        private static void RegisterScriptBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                                    "~/Scripts/jquery.validate*",
                                    "~/Scripts/jquery.steps.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
          "~/Scripts/bootstrap.js",
          "~/Scripts/respond.js"));

            #region Kendo

            bundles.Add(new ScriptBundle("~/bundles/kendo-web").Include(
                "~/Scripts/kendo/Current/kendo.web.min.js",
                "~/Scripts/kendo/Current/kendo.aspnetmvc.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo-Jszip").Include(
                "~/Scripts/kendo/Current/jszip.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo-angular").Include(
                "~/Scripts/kendo/Current/angular.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo-dataviz").Include(
                "~/Scripts/kendo/Current/kendo.all.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendo-mobile").Include(
                "~/Scripts/kendo/Current/kendo.mobile.min.js"));

            #endregion

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/app/exam-{version}.js"));
        }

        /// <summary>
        /// Registers the style bundles.
        /// </summary>
        /// <param name="bundles">The bundles.</param>
        private static void RegisterStyleBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/bootstrap").Include(
                          "~/Content/themes/common/bootstrap/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include( 
                      "~/Content/themes/base/style.css",
                      "~/Content/themes/common/plugins/reset.css",
                      "~/Content/themes/base/kendo_overwrite.css",
                      "~/Content/themes/base/jquery.steps.css"));

            bundles.Add(new StyleBundle("~/Content/kendo/web/css").Include(
                "~/Content/kendo/web/kendo.common.min.css",
                "~/Content/kendo/web/kendo.default.min.css"
                ));

            bundles.Add(new StyleBundle(
                "~/Fonts/google/css",
                "https://fonts.googleapis.com/css?family=Alegreya+Sans:400,300,500,700"));
        }
    }
}
