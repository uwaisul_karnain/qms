﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using attune.Core.Security;
using attune.Core.Security.Owin;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Owin;

namespace attune.Exam.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        /// <summary>
        /// Configures the authentication.
        /// </summary>
        /// <param name="app">The application.</param>
        public void ConfigureAuth(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888

            var securityConfig = new SecurityConfigHandler(app);
            securityConfig.ExternalAuthenticationHandle += SecurityConfigOnExternalAuthenticationHandle;

            securityConfig.Configure(new ConfigOptions { Modes = Modes.All });
        }

        /// <summary>
        /// Securities the configuration on external authentication handle.
        /// </summary>
        /// <param name="app">The application.</param>
        private void SecurityConfigOnExternalAuthenticationHandle(IAppBuilder app)
        {
            var facebookAuthenticationOptions = new FacebookAuthenticationOptions()
            {
                AppId = "1594114920827911",
                AppSecret = "61c31940dac418c8a80912a550c82643",

            };
            facebookAuthenticationOptions.Scope.Add("email");
            app.UseFacebookAuthentication(facebookAuthenticationOptions);

            var googleAuthencticationOption = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "850462593313-2tmb5nqa8p2uu08nls5thqqfdbjtga70.apps.googleusercontent.com",
                ClientSecret = "lVLD-SeFOQu9bf6iWQ4Jj4Nf",

            };
            app.UseGoogleAuthentication(googleAuthencticationOption);
        }
    }
}