﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using attune.Core.Adaptor;
using attune.Core.Adaptor.AutoMapper;
using attune.Core.Caching;
using attune.Core.Caching.System;
using attune.Core.Ioc.Unity;
using attune.Core.Logging;
using attune.Core.Logging.Log4NetV2;
using attune.Core.Logging.TraceLog;
using attune.Core.Repository.EF6;
using attune.Core.UnitOfWork;
using attune.Core.Validation;
using attune.Core.Validators;
using Microsoft.Practices.Unity;
using attune.Exam.Application.Interfaces;
using attune.Exam.Application;
using attune.Exam.Repository.EF;
using attune.Exam.Repository.EF.Model;
using attune.Exam.Repository.Interfaces;
using attune.Exam.Repository.Services.LMS;

namespace attune.Exam.Infrastructure.DI
{
    /// <summary>
    ///     This container will be the actualt DI context will be used in the application
    /// </summary>
    /// <seealso cref="IUnityTypeRegister" />
    public sealed class LiveContainer : IUnityTypeRegister
    {
        #region IUnityTypeRegister Members

        /// <summary>
        ///     Registers the applicaions.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="members">The members.</param>
        public void RegisterApplicaions(IUnityContainer container, List<InjectionMember> members)
        {
            container.RegisterType<IQuestionBankService, QuestionBankService>(members.ToArray());
            container.RegisterType<IMasterDataService, MasterDataService>(members.ToArray());
            container.RegisterType<IAssignmentService, AssignmentService>(members.ToArray());
            container.RegisterType<IQuestionPoolService, QuestionPoolService>(members.ToArray());
        }

        /// <summary>
        ///     Registers the domains.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="members">The members.</param>
        public void RegisterDomains(IUnityContainer container, List<InjectionMember> members)
        {
            container.RegisterType<Business.Interfaces.IQuestionBankService, Business.QuestionBankService>(
               members.ToArray());
            container.RegisterType<Business.Interfaces.IAssignmentService, Business.AssignmentService>(
               members.ToArray());
            container.RegisterType<Business.Interfaces.IEmployeeService, Business.EmployeeService>(
               members.ToArray());
            container.RegisterType<Business.Interfaces.IQuestionPoolService, Business.QuestionPoolService>(
               members.ToArray());
        }

        /// <summary>
        ///     Registers the repositories.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="members">The members.</param>
        public void RegisterRepositories(IUnityContainer container, List<InjectionMember> members)
        {
            container.RegisterType<IQuestionBankRepository, QuestionBankRepository>(members.ToArray());
            container.RegisterType<IAssignmentRepository, AssignmentRepository>(members.ToArray());
            container.RegisterType<IEmployeeRepository, EmployeeRepository>(members.ToArray());
            container.RegisterType<IQuestionPoolRepository, QuestionPoolRepository>(members.ToArray());           
        }

        /// <summary>
        ///     Registers the types.
        /// </summary>
        /// <param name="container">The container.</param>
        /// <param name="members">The members.</param>
        public void RegisterTypes(IUnityContainer container, List<InjectionMember> members)
        {
            container.RegisterType<IUnitOfWork<DbContext>, UnitOfWork>(new TransientLifetimeManager(),
                new InjectionConstructor(
                    new aPortalEntities()));
                    //new DbContext(ConfigurationManager.ConnectionStrings["aPortalEntities"].ConnectionString)
                    //{
                    //    Configuration = { LazyLoadingEnabled = false }
                    //}));


            container.RegisterType<IAdaptor, AutoMapperAdaptor>(new ContainerControlledLifetimeManager());
            container.RegisterType<ILogger, Log4NetLogger>(new ContainerControlledLifetimeManager(), new InjectionConstructor());
            container.RegisterType<ITraceLogger, TraceLogger>(new ContainerControlledLifetimeManager(), new InjectionConstructor());
            container.RegisterType<IEntityValidator, EntityValidator>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICache, CacheManager>(new ContainerControlledLifetimeManager());
        }

        #endregion
    }
}