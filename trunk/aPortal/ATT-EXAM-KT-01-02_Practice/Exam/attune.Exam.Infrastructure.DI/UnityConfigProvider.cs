﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System.Collections.Generic;
using attune.Core.Ioc.Unity;
using attune.Core.Ioc.Unity.AOP.Caching;
using attune.Core.Ioc.Unity.AOP.Logging;
using attune.Core.Ioc.Unity.AOP.Validation;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;

namespace attune.Exam.Infrastructure.DI
{
    public sealed class UnityConfigProvider
    {
        #region Private Variables

        private static IUnityContainer _container;

        #endregion

        #region Public Methods

        /// <summary>
        ///     Gets the configuration.
        /// </summary>
        /// <returns></returns>
        public static IUnityContainer GetConfiguredContainer()
        {
            if (_container != null) return _container;

            _container = new UnityContainer();

            #region Global Interceptors

            var injectionMembers = new List<InjectionMember>
                                   {
                                       new Interceptor<InterfaceInterceptor>(),
                                       new InterceptionBehavior<ValidationInterceptionBehavior>(),
                                       new InterceptionBehavior<LoggingInterceptionBehavior>(),
                                       new InterceptionBehavior<TraceInterceptionBehavior>(),
                                       new InterceptionBehavior<CachingInterceptionBehavior>()
                                   };


            _container.AddNewExtension<Interception>();

            #endregion

            //TODO : Add config setting to switch between different containers if require

            IUnityTypeRegister live = new LiveContainer();

            live.RegisterApplicaions(_container, injectionMembers);
            live.RegisterDomains(_container, injectionMembers);
            live.RegisterRepositories(_container, injectionMembers);
            live.RegisterTypes(_container, injectionMembers);

            InstanceFactory.SetContainer(_container);

            return _container;
        }

        #endregion
    }
}