﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

namespace attune.Exam.Domain
{

    /// <summary>
    ///     List all the Exam Status that required for Assginment
    ///     NOTE : Enum value must be identical to DB values in the [exam.ExamStatus] table
    /// </summary>
    public enum ExamStatus
    {
        Pending = 1,
        Completed = 2,
        Rescheduled = 3
    }

    /// <summary>
    ///     List all the Difficult Level that required for Exam and Question
    ///     NOTE : Enum value must be identical to DB values in the [exam.DifficultLevel] table
    /// </summary>
    public enum DifficultLevel
    {
        Beginner = 1,
        Intermediate = 2,
        Expert = 3
    }


    /// <summary>
    ///     List all the Question that required for Question creation
    ///     NOTE : Enum value must be identical to DB values in the [exam.QuestionType] table
    /// </summary>
    public enum QuestionType
    {
        MultipleDropDown = 1,
        MultipleRadioButton = 2,
        MultipleCorrect = 3,
        YesOrNo = 4,
        TrueOrFalse = 5
    }

    /// <summary>
    ///     List all the Result Type that required for Assignment
    ///     NOTE : Enum value must be identical to DB values in the [exam.ResultType] table
    /// </summary>
    public enum ResultType
    {
        Pass = 1,
        Fail = 2
    }

    /// <summary>
    ///     List all the Result Format Type that required for Assignment
    ///     NOTE : Enum value must be identical to DB values in the [exam.ResultFormatType] table
    /// </summary>
    public enum ResultFormatType
    {
        Percentage = 1,
        Point = 2
    }

    public enum ExamMode
    {
        Edit = 1,
        Add = 2
    }

    public enum QuestionPicking
    {
        Manual = 1,
        Auto = 2
    }
   
    /// <summary>
    ///     List all System modules that requires privilege manamgnet
    ///     NOTE : Enum value must be identical to DB values in the [um.SystemModule] table
    /// </summary>
    public enum SystemModule
    {
        UserManagement = 15
    }

    /// <summary>
    ///     List all privilege type that requires privilege manamgnet
    ///     NOTE : Enum value must be identical to DB values in the [um.Privilege] table
    /// </summary>
    public enum PrivilegeType
    {
        ViewUserManagement = 49
    }
}