﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using attune.Core.Validation.Attributes;

namespace attune.Exam.Domain.Entities
{
    public class Exam : AggregatedRoot<long>
    {
        #region Public Properties
        [NotNull]
        public ExamType ExamType { get; set; }
        [NotNullOrEmpty]
        public string Name { get; set; }
        public string Description { get; set; }
        public DifficultLevel DifficultLevelId { get; set; }
        public decimal? Duration { get; set; }
        public bool IsManualPick { get; set; }
        public ResultFormatType ResultFormatType { get; set; }
        public decimal PassScore { get; set; }
        [CollectionNotEmpty]
        public List<QuestionCategory> QuestionCategories { get; set; }       

        #endregion
    }
}
