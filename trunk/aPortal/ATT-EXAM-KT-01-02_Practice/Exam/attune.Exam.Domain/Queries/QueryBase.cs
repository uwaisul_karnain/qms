﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System.Collections.Generic;
using attune.Core;

namespace attune.Exam.Domain.Queries
{
    public abstract class QueryBase : IQuery
    {
        #region Constructor

        protected QueryBase()
        {
            this.IsActive = true;
        }

        #endregion

        #region IQuery Members

        public virtual List<string> OrderBy { get; set; }
        public virtual List<string> Includes { get; set; }
        public virtual bool Ascending { get; set; }
        public virtual int? PageIndex { get; set; }
        public virtual int? PageSize { get; set; }

        #endregion

        public bool IsActive { get; set; }
    }
}