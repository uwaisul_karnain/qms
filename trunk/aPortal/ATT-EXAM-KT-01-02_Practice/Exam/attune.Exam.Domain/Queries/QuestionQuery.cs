﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System.Collections.Generic;
using attune.Core.Validation.Attributes;

namespace attune.Exam.Domain.Queries
{
    public sealed class QuestionQuery : QueryBase
    {
        public QuestionQuery()
        {
            this.Includes = new List<string>()
                            {
                                "ExamType",
                                "Category",
                                "DifficultLevel"
                            };
        }

        private bool? _isPublished;

        [GreaterThan(0)]
        public int ExamTypeId { get; set; }

        public int DifficultLevelId { get; set; }

        public int CategoryId { get; set; }

        public string Mode { get; set; }

        public bool? IsPublished
        {
            get
            {
                _isPublished = (string.IsNullOrEmpty(Mode) ? (bool?) null : Mode.ToLower() == "true");
                return _isPublished;
            }
            set { _isPublished = value; }
        }
    }
}