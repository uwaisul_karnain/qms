﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Threading.Tasks;
using attune.Core.Validation.Attributes;
using attune.Exam.Domain;
using attune.Exam.Domain.Entities;
using attune.Exam.Domain.Queries;

namespace attune.Exam.Repository.Interfaces
{
    public interface IQuestionPoolRepository
    {
        #region Common
        /// <summary>
        /// Get Exam Types
        /// </summary>
        /// <returns></returns>
        List<ExamType> GetExamTypes();

        /// <summary>
        /// Get Categories
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        List<Category> GetCategories(int examTypeId);

        /// <summary>
        /// Get Exam Names
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        List<Domain.Entities.Exam> GetExamNames(int examTypeId);

        #endregion

        #region Question Pool
        /// <summary>
        /// Retrieve Question Pool Details
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        List<QuestionPool> FindQuestions([NotNull] QuestionQuery query);

        /// <summary>
        /// Bulk upload questions and answers by passing list of question Pool objects
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        bool AddBulkQuestions([CollectionNotEmpty] List<QuestionPool> query);

        /// <summary>
        /// Bulk Upload Question
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        bool BulkUploadQuestion(DataTable table);

        /// <summary>
        /// Find
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        QuestionPool FindQuestion(long questionId);

        /// <summary>
        /// Update Question
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool UpdateQuestion([NotNull]QuestionPool entity);

        /// <summary>
        /// Add Question
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool AddQuestion([NotNull] QuestionPool entity);

        /// <summary>
        /// De Activate Question
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool DeActivateQuestion(QuestionPool entity);

        #endregion

        #region Exam type
        /// <summary>
        /// Find Exam Types
        /// </summary>
        /// <returns></returns>
        List<ExamType> FindExamTypes();

        /// <summary>
        /// Add Exam Type
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool AddExamType([NotNull]ExamType entity);

        /// <summary>
        /// Update Exam Type
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool UpdateExamType([NotNull]ExamType entity);

        /// <summary>
        /// Is ExamType Name Exist
        /// </summary>
        /// <param name="examTypeName"></param>
        /// <returns></returns>
        bool IsExamTypeNameExist(string examTypeName);

        /// <summary>
        /// Find Exam Type
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ExamType FindExamType(long id);

        #endregion

        #region Category
        /// <summary>
        /// Find Categories
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        List<Category> FindCategories(int examTypeId);

        /// <summary>
        /// Is Category Name Exist
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        bool IsCategoryNameExist(string categoryName);

        /// <summary>
        /// Add Category
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool AddCategory([NotNull]Category entity);

        /// <summary>
        /// Update Category
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool UpdateCategory([NotNull]Category entity);

        /// <summary>
        /// Find Category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Category FindCategory(long id);

        #endregion

        #region Exam

        /// <summary>
        /// Find Exams
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        List<Domain.Entities.Exam> FindExams([NotNull] ExamQuery query);

        /// <summary>
        ///  Find Exam
        /// </summary>
        /// <param name="examId"></param>
        /// <returns></returns>
        Domain.Entities.Exam FindExam(long examId);

        /// <summary>
        /// De Activate Exam
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool DeActivateExam(Domain.Entities.Exam entity);

        /// <summary>
        /// Add Exam
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool AddExam([NotNull]Domain.Entities.Exam entity);

        /// <summary>
        /// Update Exam
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool UpdateExam([NotNull] Domain.Entities.Exam entity);

        /// <summary>
        /// Find Questions
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        List<QuestionPool> FindQuestions(QuestionSelectionQuery query);

        /// <summary>
        /// Is Exam Name Exist
        /// </summary>
        /// <param name="examName"></param>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        bool IsExamNameExist(string examName, int examTypeId);

        /// <summary>
        /// Find Exam Question Category
        /// </summary>
        /// <param name="questionCategoryId"></param>
        /// <returns></returns>
        long FindExamQuestionCategory(long questionCategoryId);

        /// <summary>
        /// Is Questions Available
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool IsQuestionsAvailable(Domain.Entities.Exam entity);

        #endregion

        
    }
}
