﻿
/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Validation.Attributes;
using attune.Exam.Domain.Entities;
using attune.Exam.Domain.Queries;

namespace attune.Exam.Repository.Interfaces
{
    public interface IAssignmentRepository
    {
        /// <summary>
        /// Add Exam Schedule
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        bool AddExamSchedule([NotNull]ExamScheduleCreateOrUpdateQuery query);

        /// <summary>
        /// Update Exam Schedule
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        bool UpdateExamSchedule([NotNull]ExamScheduleCreateOrUpdateQuery query);

        /// <summary>
        /// Find Schedule Candidates
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        List<Candidate> FindScheduleCandidates(int scheduleId);

        List<Assignment> FindSchedules();
    }
}
