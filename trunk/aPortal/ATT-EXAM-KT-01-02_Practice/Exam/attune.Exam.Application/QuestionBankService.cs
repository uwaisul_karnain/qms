﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core;
using attune.Core.Adaptor;
using attune.Core.Application;
using attune.Exam.Application.Dtos;
using attune.Exam.Application.Interfaces;
using attune.Exam.Domain;
using attune.Exam.Domain.Queries;
using attune.Exam.Repository.Interfaces;
using Answer = attune.Exam.Domain.Entities.Answer; 
using QuestionBank = attune.Exam.Application.Dtos.QuestionBank;

namespace attune.Exam.Application
{
    public class QuestionBankService : AppServiceBase, IQuestionBankService
    {
        #region Private Variables

        private readonly IQuestionBankRepository _questionBankRepository;
        private readonly Business.Interfaces.IQuestionBankService _questionBankService;
        private readonly IAdaptor _adaptor;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="QuestionBankService" /> class.
        /// </summary>
        /// <param name="questionBankRepository">The question bank repositry.</param>
        /// <param name="adaptor">The adaptor.</param>
        /// <param name="questionBankService"></param>
        public QuestionBankService(IQuestionBankRepository questionBankRepository, IAdaptor adaptor, Business.Interfaces.IQuestionBankService questionBankService)
        {
            _questionBankRepository = questionBankRepository;
            _adaptor = adaptor;
            _questionBankService = questionBankService;
        }

        #endregion

        #region IQuestionBank Service Members

        #region Question Bank
        /// <summary>
        /// Find Questions
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<QuestionBank> FindQuestions(QuestionQuery query)
        {
            return _adaptor.Adapt<List<Domain.Entities.QuestionBank>, List<QuestionBank>>(
                _questionBankRepository.FindQuestions(query));
        }

        /// <summary>
        /// Find
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public QuestionBank FindQuestion(long questionId)
        {
            return _adaptor.Adapt<Domain.Entities.QuestionBank, QuestionBank>(
                _questionBankRepository.FindQuestion(questionId));
        }
        
        /// <summary>
        ///  Update Question
        /// </summary>
        /// <param name="questionBank"></param>
        /// <returns></returns>
        public bool UpdateQuestion(QuestionBank questionBank)
        {
            if (questionBank.IsPublished)
            {
                return !_questionBankService.IsHavingAnswer(_adaptor.Adapt<List<Application.Dtos.Answer>, List<Domain.Entities.Answer>>(questionBank.Answers)) ||
                 _questionBankRepository.UpdateQuestion(
                            _adaptor.Adapt<Application.Dtos.QuestionBank, Domain.Entities.QuestionBank>(questionBank));
            }
            else
            {
                return
                    _questionBankRepository.UpdateQuestion(
                        _adaptor.Adapt<Application.Dtos.QuestionBank, Domain.Entities.QuestionBank>(questionBank));
            }
        }

        /// <summary>
        ///  Add Bulk Question to upload
        /// </summary>
        /// <param name="uploadQuestions"></param>
        /// <returns></returns>
        public bool AddBulkQuestions(List<UploadQuestion> uploadQuestions)
        {
            var questionbank = new List<Domain.Entities.QuestionBank>();
            foreach (var uploadQuestion in uploadQuestions)
            {
                var listAnswers = new List<Answer>();
                SetAnswers(uploadQuestion, listAnswers);
                questionbank.Add(new Domain.Entities.QuestionBank
                                 {
                                     ExamType = new Domain.Entities.ExamType() {Id = uploadQuestion.ExamType},
                                     Category = new Domain.Entities.Category() {Id = uploadQuestion.CategoryType},
                                     QuestionType = (QuestionType) uploadQuestion.QuestionType,
                                     DifficultLevel = (DifficultLevel) uploadQuestion.DifficultLevel,
                                     Question = uploadQuestion.Question,
                                     IsPublished = uploadQuestion.IsPublished,
                                     Answers = listAnswers,
                                     IsActive = true,
                                     CreatedBy = 1,
                                     CreatedOn = DateTime.Now,
                                     UpdatedOn = DateTime.Now

                                 });
                
            }
            if (questionbank.Count > 0)
            {
                var result = _questionBankRepository.AddBulkQuestions(questionbank);
                return result;
            }
            return false;
        }

        /// <summary>
        /// Add Question
        /// </summary>
        /// <param name="questionBank"></param>
        /// <returns></returns>
        public bool AddQuestion(Application.Dtos.QuestionBank questionBank)
        {
            if (questionBank.IsPublished)
            {
                return !_questionBankService.IsHavingAnswer( _adaptor.Adapt<List<Application.Dtos.Answer>, List<Domain.Entities.Answer>>(questionBank.Answers))||
                 _questionBankRepository.AddQuestion(
                            _adaptor.Adapt<Application.Dtos.QuestionBank, Domain.Entities.QuestionBank>(questionBank));
            }
            else
            {
                return
                    _questionBankRepository.AddQuestion(
                        _adaptor.Adapt<Application.Dtos.QuestionBank, Domain.Entities.QuestionBank>(questionBank));
            }
        }

        /// <summary>
        /// De Activate Question
        /// </summary>
        /// <param name="questionBank"></param>
        /// <returns></returns>
        public bool DeActivateQuestion(QuestionBank questionBank)
        {
            return _questionBankRepository.DeActivateQuestion(
                        _adaptor.Adapt<Application.Dtos.QuestionBank, Domain.Entities.QuestionBank>(questionBank));
        }

        #endregion

        #region Exam Type
        /// <summary>
        /// Find Exam Types
        /// </summary>
        /// <returns></returns>
        public List<ExamType> FindExamTypes()
        {
            return _adaptor.Adapt<List<Domain.Entities.ExamType>, List<ExamType>>(
                _questionBankRepository.FindExamTypes());
        }

        /// <summary>
        /// Add Exam Type
        /// </summary>
        /// <param name="examType"></param>
        /// <returns></returns>
        public bool AddExamType(ExamType examType)
        {
            return !_questionBankService.IsExamTypeNameExist(examType.Name) ||
                  _questionBankRepository.AddExamType(_adaptor.Adapt<Application.Dtos.ExamType, Domain.Entities.ExamType>(examType));
            
        }

        /// <summary>
        /// Update ExamType
        /// </summary>
        /// <param name="examType"></param>
        /// <returns></returns>
        public bool UpdateExamType(ExamType examType)
        {
            return
                    _questionBankRepository.UpdateExamType(
                        _adaptor.Adapt<Application.Dtos.ExamType, Domain.Entities.ExamType>(examType));
        }

        /// <summary>
        /// Find Exam Type
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExamType FindExamType(long id)
        {
            return _adaptor.Adapt<Domain.Entities.ExamType, ExamType>(
                _questionBankRepository.FindExamType(id));
        }

        #endregion

        #region Category
        /// <summary>
        /// Find Categories
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        public List<Category> FindCategories(int examTypeId)
        {
            return _adaptor.Adapt<List<Domain.Entities.Category>, List<Category>>(
                _questionBankRepository.FindCategories(examTypeId));
        }

        /// <summary>
        /// Add Category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public bool AddCategory(Category category)
        {
            return !_questionBankService.IsCategoryNameExist(category.Name) ||
                  _questionBankRepository.AddCategory(_adaptor.Adapt<Application.Dtos.Category, Domain.Entities.Category>(category));
            
        }

        /// <summary>
        /// Update Category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public bool UpdateCategory(Category category)
        {
            return _questionBankRepository.UpdateCategory(
                           _adaptor.Adapt<Application.Dtos.Category, Domain.Entities.Category>(category));
        }

        /// <summary>
        /// Find Category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Category FindCategory(long id)
        {
            return _adaptor.Adapt<Domain.Entities.Category, Category>(
                _questionBankRepository.FindCategory(id));
        }

        #endregion

        #region Exam
        
        /// <summary>
        /// Find Exams
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<Dtos.Exam> FindExams(ExamQuery query)
        {
            return _adaptor.Adapt<List<Domain.Entities.Exam>, List<Application.Dtos.Exam>>(
               _questionBankRepository.FindExams(query));
        }

        /// <summary>
        /// Find Exam
        /// </summary>
        /// <param name="examId"></param>
        /// <returns></returns>
        public Dtos.Exam FindExam(long examId)
        {
           return _adaptor.Adapt<Domain.Entities.Exam, Dtos.Exam>(_questionBankRepository.FindExam(examId));
            
        }

        /// <summary>
        /// De Activate Exam
        /// </summary>
        /// <param name="exam"></param>
        /// <returns></returns>
        public bool DeActivateExam(Dtos.Exam exam)
        {
            return _questionBankRepository.DeActivateExam(
                       _adaptor.Adapt<Application.Dtos.Exam, Domain.Entities.Exam>(exam));
        }

        /// <summary>
        /// Add Exam
        /// </summary>
        /// <param name="exam"></param>
        /// <returns></returns>
        public bool AddExam(Dtos.Exam exam)
        {
            return !_questionBankService.IsExamNameExist(exam.Name, (int)exam.ExamType.Id) || 
                    !_questionBankService.IsQuestionCategoryDuplicated(_adaptor.Adapt<List<Application.Dtos.QuestionCategory>, List<Domain.Entities.QuestionCategory>>(exam.QuestionCategories)) ||
                    !_questionBankService.IsQuestionsAvailable(_adaptor.Adapt<Application.Dtos.Exam,Domain.Entities.Exam>(exam))||
                    _questionBankRepository.AddExam(_adaptor.Adapt<Application.Dtos.Exam, Domain.Entities.Exam>(exam));
            
        }

        /// <summary>
        /// Find Questions
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<QuestionBank> FindQuestions(QuestionSelectionQuery query)
        {
            if (query.Mode == (int) Domain.ExamMode.Edit && query.QuestionCategoryId!=0)
            {
                var examId = _questionBankRepository.FindExamQuestionCategory(query.QuestionCategoryId);
                var dto = _adaptor.Adapt<Domain.Entities.Exam, Dtos.Exam>(_questionBankRepository.FindExam(examId));
                var questions = _adaptor.Adapt<List<Domain.Entities.QuestionBank>, List<QuestionBank>>(_questionBankRepository.FindQuestions(query));
                foreach (var cat in dto.QuestionCategories)
                {
                    var examQuestions = cat.Questions;
                    foreach (var question in questions)
                    {
                        if (examQuestions.Select(x => x.Id).ToList().Contains((int) question.Id))
                        {
                            question.IsSelected = true;
                        }
                    }
                }
                return questions;
            }
            return _adaptor.Adapt<List<Domain.Entities.QuestionBank>, List<QuestionBank>>(
                _questionBankRepository.FindQuestions(query));
        }

        /// <summary>
        /// Update Exam
        /// </summary>
        /// <param name="exam"></param>
        /// <returns></returns>
        public bool UpdateExam(Dtos.Exam exam)
        {
            return !_questionBankService.IsQuestionCategoryDuplicated(_adaptor.Adapt<List<Application.Dtos.QuestionCategory>, List<Domain.Entities.QuestionCategory>>(exam.QuestionCategories)) ||
                  !_questionBankService.IsQuestionsAvailable(_adaptor.Adapt<Application.Dtos.Exam, Domain.Entities.Exam>(exam)) ||
                  _questionBankRepository.UpdateExam(_adaptor.Adapt<Application.Dtos.Exam, Domain.Entities.Exam>(exam));
            
        }

        #endregion

        #endregion

        /// <summary>
        /// Bind answers to entity
        /// </summary>
        /// <param name="uploadQuestion"></param>
        /// <param name="listAnswers"></param>
       
        private static void SetAnswers(UploadQuestion uploadQuestion, List<Answer> listAnswers)
        {
            if (uploadQuestion.Answer1 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 1 || uploadQuestion.Correct2 == 1
                    || uploadQuestion.Correct3 == 1 || uploadQuestion.Correct4 == 1
                    || uploadQuestion.Correct5 == 1 || uploadQuestion.Correct6 == 1
                    || uploadQuestion.Correct7 == 1 || uploadQuestion.Correct8 == 1
                    || uploadQuestion.Correct9 == 1 || uploadQuestion.Correct10 == 1)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer1,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer1,
                                        IsCorrect = false
                                    });
                }
            }
            if (uploadQuestion.Answer2 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 2 || uploadQuestion.Correct2 == 2
                    || uploadQuestion.Correct3 == 2 || uploadQuestion.Correct4 == 2
                    || uploadQuestion.Correct5 == 2 || uploadQuestion.Correct6 == 2
                    || uploadQuestion.Correct7 == 2 || uploadQuestion.Correct8 == 2
                    || uploadQuestion.Correct9 == 2 || uploadQuestion.Correct10 == 2)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer2,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer2,
                                        IsCorrect = false
                                    });
                }
            }
            if (uploadQuestion.Answer3 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 3 || uploadQuestion.Correct2 == 3
                    || uploadQuestion.Correct3 == 3 || uploadQuestion.Correct4 == 3
                    || uploadQuestion.Correct5 == 3 || uploadQuestion.Correct6 == 3
                    || uploadQuestion.Correct7 == 3 || uploadQuestion.Correct8 == 3
                    || uploadQuestion.Correct9 == 3 || uploadQuestion.Correct10 == 3)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer3,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer3,
                                        IsCorrect = false
                                    });
                }
            }
            if (uploadQuestion.Answer4 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 4 || uploadQuestion.Correct2 == 4
                    || uploadQuestion.Correct3 == 4 || uploadQuestion.Correct4 == 4
                    || uploadQuestion.Correct5 == 4 || uploadQuestion.Correct6 == 4
                    || uploadQuestion.Correct7 == 4 || uploadQuestion.Correct8 == 4
                    || uploadQuestion.Correct9 == 4 || uploadQuestion.Correct10 == 4)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer4,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer4,
                                        IsCorrect = false
                                    });
                }
            }
            if (uploadQuestion.Answer5 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 5 || uploadQuestion.Correct2 == 5
                    || uploadQuestion.Correct3 == 5 || uploadQuestion.Correct4 == 5
                    || uploadQuestion.Correct5 == 5 || uploadQuestion.Correct6 == 5
                    || uploadQuestion.Correct7 == 5 || uploadQuestion.Correct8 == 5
                    || uploadQuestion.Correct9 == 5 || uploadQuestion.Correct10 == 5)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer5,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer5,
                                        IsCorrect = false
                                    });
                }
            }
            if (uploadQuestion.Answer6 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 6 || uploadQuestion.Correct2 == 6
                    || uploadQuestion.Correct3 == 6 || uploadQuestion.Correct4 == 6
                    || uploadQuestion.Correct5 == 6 || uploadQuestion.Correct6 == 6
                    || uploadQuestion.Correct7 == 6 || uploadQuestion.Correct8 == 6
                    || uploadQuestion.Correct9 == 6 || uploadQuestion.Correct10 == 6)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer6,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer6,
                                        IsCorrect = false
                                    });
                }
            }
            if (uploadQuestion.Answer7 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 7 || uploadQuestion.Correct2 == 7
                    || uploadQuestion.Correct3 == 7 || uploadQuestion.Correct4 == 7
                    || uploadQuestion.Correct5 == 7 || uploadQuestion.Correct6 == 7
                    || uploadQuestion.Correct7 == 7 || uploadQuestion.Correct8 == 7
                    || uploadQuestion.Correct9 == 7 || uploadQuestion.Correct10 == 7)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer7,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer7,
                                        IsCorrect = false
                                    });
                }
            }
            if (uploadQuestion.Answer8 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 8 || uploadQuestion.Correct2 == 8
                    || uploadQuestion.Correct3 == 8 || uploadQuestion.Correct4 == 8
                    || uploadQuestion.Correct5 == 8 || uploadQuestion.Correct6 == 8
                    || uploadQuestion.Correct7 == 8 || uploadQuestion.Correct8 == 8
                    || uploadQuestion.Correct9 == 8 || uploadQuestion.Correct10 == 8)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer8,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer8,
                                        IsCorrect = false
                                    });
                }
            }
            if (uploadQuestion.Answer9 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 9 || uploadQuestion.Correct2 == 9
                    || uploadQuestion.Correct3 == 9 || uploadQuestion.Correct4 == 9
                    || uploadQuestion.Correct5 == 9 || uploadQuestion.Correct6 == 9
                    || uploadQuestion.Correct7 == 9 || uploadQuestion.Correct8 == 9
                    || uploadQuestion.Correct9 == 9 || uploadQuestion.Correct10 == 9)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer9,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer9,
                                        IsCorrect = false
                                    });
                }
            }

            if (uploadQuestion.Answer10 != string.Empty)
            {
                if (uploadQuestion.Correct1 == 10 || uploadQuestion.Correct2 == 10
                    || uploadQuestion.Correct3 == 10 || uploadQuestion.Correct4 == 10
                    || uploadQuestion.Correct5 == 10 || uploadQuestion.Correct6 == 10
                    || uploadQuestion.Correct7 == 10 || uploadQuestion.Correct8 == 10
                    || uploadQuestion.Correct9 == 10 || uploadQuestion.Correct10 == 10)
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer10,
                                        IsCorrect = true
                                    });
                }
                else
                {
                    listAnswers.Add(new Answer()
                                    {
                                        Response = uploadQuestion.Answer10,
                                        IsCorrect = false
                                    });
                }
            }
        }
     }
}
