﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Validation.Attributes;
using attune.Exam.Application.Dtos;
using attune.Exam.Domain;
using attune.Exam.Domain.Entities;
using attune.Exam.Domain.Queries;
using attune.Exam.Infrastructure.Attributes.Caching;
using Category = attune.Exam.Application.Dtos.Category;
using ExamType = attune.Exam.Application.Dtos.ExamType;
using QuestionBank = attune.Exam.Application.Dtos.QuestionBank;

namespace attune.Exam.Application.Interfaces
{
    public interface IQuestionBankService
    {
        #region Question Bank
        /// <summary>
        /// Find Questions
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        List<QuestionBank> FindQuestions(QuestionQuery query);

        /// <summary>
        /// Find
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        QuestionBank FindQuestion(long questionId);

        /// <summary>
        /// Update Question
        /// </summary>
        /// <param name="questionBank"></param>
        /// <returns></returns>
        bool UpdateQuestion([NotNull]QuestionBank questionBank);

        /// <summary>
        /// Add Bulk Questions
        /// </summary>
        /// <param name="uploadQuestions"></param>
        /// <returns></returns>
        bool AddBulkQuestions(List<UploadQuestion> uploadQuestions);

        /// <summary>
        /// Add Question
        /// </summary>
        /// <param name="questionBank"></param>
        /// <returns></returns>
        bool AddQuestion([NotNull] QuestionBank questionBank);

        /// <summary>
        /// De Activate Question
        /// </summary>
        /// <param name="questionBank"></param>
        /// <returns></returns>
        bool DeActivateQuestion([NotNull] QuestionBank questionBank);

        #endregion 

        #region Exam Type
        /// <summary>
        /// Find Exam Types
        /// </summary>
        /// <returns></returns>
        List<ExamType> FindExamTypes();

        /// <summary>
        /// Add Exam Type
        /// </summary>
        /// <param name="examType"></param>
        /// <returns></returns>
        [AppCache(Key = Constants.CACHE_KEY_ALL_EXAM_TYPES,Clear = true)]
        bool AddExamType([NotNull]ExamType examType);

        /// <summary>
        /// Update Exam Type
        /// </summary>
        /// <param name="examType"></param>
        /// <returns></returns>
        [AppCache(Key = Constants.CACHE_KEY_ALL_EXAM_TYPES, Clear = true)]
        bool UpdateExamType([NotNull]ExamType examType);

        /// <summary>
        /// Find ExamType
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ExamType FindExamType(long id);

        #endregion

        #region Category
        /// <summary>
        /// Find Categories
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        List<Category> FindCategories(int examTypeId);

        /// <summary>
        /// Add Category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        bool AddCategory([NotNull]Category category);

        /// <summary>
        /// Update Category
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        bool UpdateCategory([NotNull]Category category);

        /// <summary>
        /// Find Category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Category FindCategory(long id);

        #endregion

        #region Exam

        /// <summary>
        /// Find Exams
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        List<Application.Dtos.Exam> FindExams(ExamQuery query);

        /// <summary>
        /// Find Exam
        /// </summary>
        /// <param name="examId"></param>
        /// <returns></returns>
        Dtos.Exam FindExam(long examId);

        /// <summary>
        /// De Activate Exam
        /// </summary>
        /// <param name="exam"></param>
        /// <returns></returns>
        bool DeActivateExam(Dtos.Exam exam);

        /// <summary>
        /// Add Exam
        /// </summary>
        /// <param name="exam"></param>
        /// <returns></returns>
        bool AddExam(Dtos.Exam exam);

        /// <summary>
        /// Find Questions
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        List<QuestionBank> FindQuestions(QuestionSelectionQuery query);

        /// <summary>
        /// Update Exam
        /// </summary>
        /// <param name="exam"></param>
        /// <returns></returns>
        bool UpdateExam(Dtos.Exam exam);

        #endregion
       
    }
}
