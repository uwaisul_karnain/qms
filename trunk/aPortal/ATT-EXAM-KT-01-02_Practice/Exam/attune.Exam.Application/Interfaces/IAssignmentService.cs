﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using attune.Exam.Application.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Exam.Domain.Queries;
using attune.Exam.Infrastructure.Attributes.Caching;

namespace attune.Exam.Application.Interfaces
{
    public interface IAssignmentService
    {
        /// <summary>
        /// Get Divisions
        /// </summary>
        /// <returns></returns>
        [AppCache(Key = Constants.CACHE_KEY_ALL_DIVISIONS)]
        List<KeyValuePair<string, string>> GetDivisions();

        /// <summary>
        /// Get Sections
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetSections(string divisionCode);
        
        /// <summary>
        /// Get Designations
        /// </summary>
        /// <returns></returns>
        [AppCache(Key = Constants.CACHE_KEY_ALL_DESIGNATIONS)]
        List<KeyValuePair<string, string>> GetDesignations();

        /// <summary>
        /// Get Employee Detail
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <param name="sectionCode"></param>
        /// <param name="designationCode"></param>
        /// <returns></returns>
        List<Employee> GetEmployeeDetail(string divisionCode, string sectionCode, string designationCode);

        /// <summary>
        /// Get Non EmployeeDetail
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        List<Employee> GetNonEmployeeDetail(DateTime startDate, DateTime endDate);

        /// <summary>
        /// FindSchedule Candidates
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        List<Candidate> FindScheduleCandidates(int scheduleId);
        
        /// <summary>
        /// Add Exam Schedule
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        bool AddExamSchedule(ExamScheduleCreateOrUpdateQuery query);

        List<Assignment> FindSchedules();
    }
}
