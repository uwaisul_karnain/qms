﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Caching;
using attune.Exam.Domain;
using attune.Exam.Domain.Entities;
using attune.Exam.Domain.Queries;
using attune.Exam.Infrastructure.Attributes.Caching;
using QuestionBank = attune.Exam.Application.Dtos.QuestionBank;

namespace attune.Exam.Application.Interfaces
{
    public interface IMasterDataService
    {
        /// <summary>
        /// Get Exam Types
        /// </summary>
        /// <returns></returns>
        [AppCache(Key = Constants.CACHE_KEY_ALL_EXAM_TYPES)]
        List<KeyValuePair<string, string>> GetExamTypes();

        /// <summary>
        /// Get Categories
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetCategories(int examTypeId);

        /// <summary>
        /// Get Difficult Levels
        /// </summary>
        /// <returns></returns>
        [AppCache(Key = Constants.CACHE_KEY_ALL_DIFFICULTY_LEVELS)]
        List<KeyValuePair<string, string>> GetDifficultLevels();

        /// <summary>
        /// Get Question Types
        /// </summary>
        /// <returns></returns>
        [AppCache(Key = Constants.CACHE_KEY_ALL_QUESTION_TYPES)]
        List<KeyValuePair<string, string>> GetQuestionTypes();

        /// <summary>
        /// Get Exam Names
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetExamNames(int examTypeId);

        /// <summary>
        /// Get Result Format Type
        /// </summary>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetResultFormatType();

        
    }
}
