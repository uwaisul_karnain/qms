﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using attune.Core;
using attune.Core.Adaptor;
using attune.Core.Application;
using attune.Exam.Application.Dtos;
using attune.Exam.Application.Interfaces;
using attune.Exam.Domain.Queries;
using attune.Exam.Repository.Interfaces;
using Category = attune.Exam.Domain.Entities.Category;
using DifficultLevel = attune.Exam.Domain.DifficultLevel;

namespace attune.Exam.Application
{
    public class MasterDataService : AppServiceBase, IMasterDataService
    {
        #region Private Variables
        private readonly IQuestionBankRepository _questionBankRepository;
        private readonly IAdaptor _adaptor;
        #endregion
        
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="MasterDataService" /> class.
        /// </summary>
        /// <param name="questionBankRepository">The question bank repositry.</param>
        /// <param name="adaptor">The adaptor.</param>
        public MasterDataService(IQuestionBankRepository questionBankRepository, IAdaptor adaptor)
        {
            _questionBankRepository = questionBankRepository;
            _adaptor = adaptor;
        }

        #endregion

        /// <summary>
        /// Get ExamTypes
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<string,string>> GetExamTypes()
        {
            return _questionBankRepository.GetExamTypes().Select(item => new KeyValuePair<string, string>(item.Id.ToString(), item.Name)).ToList();
        }

        /// <summary>
        /// Get Categories
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetCategories(int examTypeId)
        {
            return
                _questionBankRepository.GetCategories(examTypeId)
                    .Select(item => new KeyValuePair<string, string>(item.Id.ToString(), item.Name))
                    .ToList();
        }

        /// <summary>
        /// Get DifficultLevels
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetDifficultLevels()
        {
            //TODO : Localize the the enum values
            var list = new List<KeyValuePair<string, string>>();
            list.Add(new KeyValuePair<string, string>(((int)Domain.DifficultLevel.Beginner).ToString(),Domain.DifficultLevel.Beginner.ToString()));
            list.Add(new KeyValuePair<string, string>(((int)Domain.DifficultLevel.Intermediate).ToString(),Domain.DifficultLevel.Intermediate.ToString()));
            list.Add(new KeyValuePair<string, string>(((int)Domain.DifficultLevel.Expert).ToString(),Domain.DifficultLevel.Expert.ToString()));

            return list;
        }

        /// <summary>
        /// Get Question Types
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetQuestionTypes()
        {
            var list = new List<KeyValuePair<string, string>>();
            list.Add(new KeyValuePair<string, string>(((int)Domain.QuestionType.MultipleDropDown).ToString(), Domain.QuestionType.MultipleDropDown.ToString()));
            list.Add(new KeyValuePair<string, string>(((int)Domain.QuestionType.MultipleRadioButton).ToString(), Domain.QuestionType.MultipleRadioButton.ToString()));
            list.Add(new KeyValuePair<string, string>(((int)Domain.QuestionType.MultipleCorrect).ToString(), Domain.QuestionType.MultipleCorrect.ToString()));
            list.Add(new KeyValuePair<string, string>(((int)Domain.QuestionType.YesOrNo).ToString(), Domain.QuestionType.YesOrNo.ToString()));
            list.Add(new KeyValuePair<string, string>(((int)Domain.QuestionType.TrueOrFalse).ToString(), Domain.QuestionType.TrueOrFalse.ToString()));

            return list.OrderBy(x=>x.Value).ToList();
        }

        /// <summary>
        /// Get Exam Names
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetExamNames(int examTypeId)
        {
            return _questionBankRepository.GetExamNames(examTypeId).Select(item => new KeyValuePair<string, string>(item.Id.ToString(), item.Name)).ToList();
        }

        public List<KeyValuePair<string, string>> GetResultFormatType()
        {
            //TODO : Localize the the enum values
            var list = new List<KeyValuePair<string, string>>();
            list.Add(new KeyValuePair<string, string>(((int)Domain.ResultFormatType.Percentage).ToString(), Domain.ResultFormatType.Percentage.ToString()));
            list.Add(new KeyValuePair<string, string>(((int)Domain.ResultFormatType.Point).ToString(), Domain.ResultFormatType.Point.ToString()));
            return list;
        }

        
    }
}
