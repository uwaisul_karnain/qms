﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Exam.Domain;

namespace attune.Exam.Application.Dtos
{
    public class Exam : DtoBase<int>
    {
        #region Public Properties

        public virtual ExamType ExamType { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual DifficultLevel DifficultLevelId { get; set; }
        public virtual decimal Duration { get; set; }
        public virtual bool IsManualPick { get; set; }
        public virtual ResultFormatType ResultFormatType { get; set; }
        public virtual decimal PassScore { get; set; }
        public virtual List<QuestionCategory> QuestionCategories { get; set; }       

        #endregion
    }
}
