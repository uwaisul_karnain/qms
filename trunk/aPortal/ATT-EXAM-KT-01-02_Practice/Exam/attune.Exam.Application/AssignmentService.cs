﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Adaptor;
using attune.Core.Application;
using attune.Exam.Application.Interfaces;
using attune.Exam.Repository.Interfaces;
using attune.Exam.Application.Dtos;
using attune.Exam.Domain.Queries;

namespace attune.Exam.Application
{
    public class AssignmentService : AppServiceBase, IAssignmentService
    {
        #region Private Variables

        private readonly IAssignmentRepository _assignmentRepository;
        private readonly Business.Interfaces.IAssignmentService _assignmentService;
        private readonly Business.Interfaces.IEmployeeService _employeeService;
        private readonly IAdaptor _adaptor;

        #endregion

        #region Constructor


        public AssignmentService(IAssignmentRepository assignmentRepository, IAdaptor adaptor, Business.Interfaces.IAssignmentService assignmentService,
            Business.Interfaces.IEmployeeService employeeService)
        {
            _assignmentRepository = assignmentRepository;
            _adaptor = adaptor;
            _assignmentService = assignmentService;
            _employeeService = employeeService;
        }

        #endregion

        #region IAssignmentService Methods
        /// <summary>
        /// Get Divisions
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetDivisions()
        {
            return _employeeService.GetDivisions(null);
        }

        /// <summary>
        /// Get Sections
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetSections(string divisionCode)
        {
            return _employeeService.GetSections(divisionCode);
        }

        /// <summary>
        /// Get Designations
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetDesignations()
        {
            return _employeeService.GetDesignations();
        }

        /// <summary>
        ///  Get Employee Detail
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <param name="sectionCode"></param>
        /// <param name="designationCode"></param>
        /// <returns></returns>
        public List<Employee> GetEmployeeDetail(string divisionCode, string sectionCode,string designationCode)
        {
            return _adaptor.Adapt<List<Domain.Entities.Employee>, List<Employee>>(
                _employeeService.GetEmployeeDetail(divisionCode, sectionCode, designationCode));
        }

        /// <summary>
        /// Get Non Employee Detail
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<Employee> GetNonEmployeeDetail(DateTime startDate, DateTime endDate)
        {
            return _adaptor.Adapt<List<Domain.Entities.Employee>, List<Employee>>(
                _employeeService.GetNonEmployeeDetail(startDate, endDate));
        }

        /// <summary>
        /// Find Schedule Candidates
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        public List<Candidate> FindScheduleCandidates(int scheduleId)
        {
            return _adaptor.Adapt<List<Domain.Entities.Candidate>, List<Candidate>>(
                _assignmentRepository.FindScheduleCandidates(scheduleId));
        }

        /// <summary>
        /// Add Exam Schedule
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public bool AddExamSchedule(ExamScheduleCreateOrUpdateQuery query)
        {
            return _assignmentRepository.AddExamSchedule(query);
        }

        public List<Assignment> FindSchedules()
        {
            return _adaptor.Adapt<List<Domain.Entities.Assignment>, List<Assignment>>(
                _assignmentRepository.FindSchedules());
        }

        #endregion
    }
}
