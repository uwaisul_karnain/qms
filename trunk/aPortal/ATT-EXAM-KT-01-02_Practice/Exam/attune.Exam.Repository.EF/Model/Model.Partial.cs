﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace attune.Exam.Repository.EF.Model
{
    public partial class aPortalEntities
    {
        public override int SaveChanges()
        {
            foreach (var change in ChangeTracker.Entries())
            {
                var values = change.State == System.Data.Entity.EntityState.Deleted ? change.OriginalValues : change.CurrentValues;
                foreach (var name in values.PropertyNames)
                {
                    var value = values[name];
                    if (value is DateTime)
                    {
                        var date = (DateTime)value;
                        if (date < SqlDateTime.MinValue.Value)
                            values[name] = SqlDateTime.MinValue.Value;
                        else if (date > SqlDateTime.MaxValue.Value)
                            values[name] = SqlDateTime.MaxValue.Value;
                    }
                }
            }
            return base.SaveChanges();
        }
    }
}
