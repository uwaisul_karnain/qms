﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace attune.Exam.Repository.EF.Adaptors.Profiles
{
    public class ExaminationProfile:Profile
    {
        /// <summary>
        ///     Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        ///     Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        protected override void Configure()
        {
            base.Configure();

            Mapper.CreateMap<Domain.Entities.Exam, Model.Examination>()
                .ForMember(x => x.ResultFormatTypeId, m => m.MapFrom(c => c.ResultFormatType))
                .ForMember(x=>x.IsQuestionManualPick,m=>m.MapFrom(c=>c.IsManualPick))
                .ForMember(x => x.ResultFormatType, m => m.Ignore())
                .ForMember(x => x.ExamType, m => m.Ignore());
            Mapper.CreateMap<Model.Examination, Domain.Entities.Exam>()
                .ForMember(x=>x.ResultFormatType,m=>m.MapFrom(c=>c.ResultFormatTypeId))
                .ForMember(x => x.IsManualPick, m => m.MapFrom(c => c.IsQuestionManualPick));
        }
    }
}
