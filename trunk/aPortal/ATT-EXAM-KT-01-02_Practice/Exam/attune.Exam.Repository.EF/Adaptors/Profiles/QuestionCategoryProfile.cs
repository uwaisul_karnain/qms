﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Exam.Domain.Entities;
using AutoMapper;

namespace attune.Exam.Repository.EF.Adaptors.Profiles
{
    public class QuestionCategoryProfile:Profile
    {
        /// <summary>
        ///     Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        ///     Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        protected override void Configure()
        {
            base.Configure();

            Mapper.CreateMap<QuestionCategory, Model.ExamQuestionCategory>()
                .ForMember(x => x.DifficultLevelId, m => m.MapFrom(c => c.DifficultLevel))
                .ForMember(x => x.CategoryId, m => m.MapFrom(c => c.Category.Id))
                .ForMember(x => x.Count, m => m.MapFrom(c => c.Count))
                .ForMember(x => x.DifficultLevel, m => m.Ignore())
                .ForMember(x => x.Category, m => m.Ignore())
                .ForMember(x => x.Examination, m => m.Ignore()); 

            Mapper.CreateMap<Model.ExamQuestionCategory, QuestionCategory>()
                .ForMember(x=>x.DifficultLevel,m=>m.MapFrom(c=>c.DifficultLevelId))
                .ForMember(x=>x.Category.Id,m=>m.MapFrom(c=>c.CategoryId))
                .ForMember(x => x.Count, m => m.MapFrom(c => c.Count));
        }
    }
}
