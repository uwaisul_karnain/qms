﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq; 
using attune.Exam.Domain.Queries;
using attune.Exam.Repository.EF.Model; 
using Schedule = attune.Exam.Repository.EF.Model.Schedule;

namespace attune.Exam.Repository.EF.Adaptors.Profiles.Resolvers
{
    public class ExamScheduleCreateOrUpdateQueryToScheduleResolver : AutoMapper.TypeConverter<ExamScheduleCreateOrUpdateQuery, attune.Exam.Repository.EF.Model.Schedule>
    {
        protected override Schedule ConvertCore(ExamScheduleCreateOrUpdateQuery source)
        {
            var schedule = new Schedule();

            schedule.Id = source.Schedule.Id;
            schedule.CandidateExamQuestions = new List<CandidateExamQuestion>();
            schedule.CreatedBy = source.Schedule.CreatedBy;
            schedule.CreatedDate = source.Schedule.CreatedOn;
            schedule.EndDateTime = source.Schedule.EndDate;
            schedule.IsEmployee = source.Schedule.IsEmployee;
            schedule.IsInviteRequired = source.Schedule.IsCalendarInviteRequired;
            schedule.OnBehalfEmail = source.Schedule.OnBehalfEmail;
            schedule.StartDateTime = source.Schedule.StartDate;
            schedule.TimeZone = null;
            schedule.UpdatedBy = source.Schedule.UpdatedBy;
            schedule.UpdatedDate = source.Schedule.UpdatedOn;

            schedule.ExaminationId = source.Assignment.Exam.Id;
            schedule.IsExamAvailableAlways = source.Assignment.IsAvailableAlways;

            schedule.ScheduledCandidates = new List<ScheduledCandidate>();
            foreach (var candidate in source.Candidates)
            {
                schedule.ScheduledCandidates.Add(new ScheduledCandidate()
                {
                    ExamStatusId = (int)source.Assignment.AssignmentStatus,
                    ResultTypeId = (int?)source.Assignment.ResultType,
                    IsResultDisplay = source.Assignment.IsResultDisplay,
                    Score = (decimal?)source.Assignment.Score,
                    Comment = source.Assignment.Feedback,
                    Email = candidate.Email,
                    FirstName = candidate.FirstName,
                    IsEmployee = candidate.IsEmployee,
                    LastName = candidate.LastName,
                    CandidateId = (int)candidate.Id,
                    ScheduleId = schedule.Id
                });
            }

            return schedule;
        }
    }
}