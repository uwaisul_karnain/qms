﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using attune.Exam.Domain.Entities;
using AutoMapper;

namespace attune.Exam.Repository.EF.Adaptors.Profiles
{
    public class QuestionBankProfile : Profile
    {
        /// <summary>
        ///     Override this method in a derived class and call the CreateMap method to associate that map with this profile.
        ///     Avoid calling the <see cref="T:AutoMapper.Mapper" /> class from this method.
        /// </summary>
        protected override void Configure()
        {
            base.Configure();

            Mapper.CreateMap<QuestionBank, Model.QuestionBank>()
                .ForMember(x => x.QuestionTypeId, m => m.MapFrom(c => c.QuestionType))
                .ForMember(x => x.DifficultLevelId, m => m.MapFrom(c => c.DifficultLevel))
                .ForMember(x => x.DifficultLevel, m => m.Ignore())
                .ForMember(x=>x.Category,m=>m.Ignore())
                .ForMember(x=>x.ExamType,m=>m.Ignore())
                .ForMember(x => x.QuestionType, m => m.Ignore())
                  .ForMember(x => x.CreatedDate, m => m.MapFrom(c => c.CreatedOn))
                    .ForMember(x => x.UpdatedDate, m => m.MapFrom(c => c.UpdatedOn));  

            Mapper.CreateMap<Model.QuestionBank, QuestionBank>()
                .ForMember(x => x.QuestionType, m => m.MapFrom(c => c.QuestionTypeId))
                .ForMember(x => x.DifficultLevel, m => m.MapFrom(c => c.DifficultLevelId))
                .ForMember(x => x.CreatedOn, m => m.MapFrom(c => c.CreatedDate))
                .ForMember(x => x.UpdatedOn, m => m.MapFrom(c => c.UpdatedDate));
        }
    }
}