﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Adaptor;
using attune.Core.Domain;
using attune.Core.Repository;
using attune.Core.Repository.EF6;
using attune.Core.UnitOfWork;
using attune.Exam.Domain;
using attune.Exam.Domain.Entities;
using attune.Exam.Domain.Queries;
using attune.Exam.Repository.EF.Model;
using attune.Exam.Repository.Interfaces;
using Answer = attune.Exam.Domain.Entities.Answer;
using Category = attune.Exam.Domain.Entities.Category;
using ExamType = attune.Exam.Domain.Entities.ExamType;
using QuestionBank = attune.Exam.Domain.Entities.QuestionPool;

namespace attune.Exam.Repository.EF
{
    public class QuestionPoolRepository : RepositoryBase<DbContext>, IQuestionPoolRepository
    {
        #region private members

        private readonly IAdaptor _adaptor;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="QuestionBankRepository" /> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="adaptor"></param>
        public QuestionPoolRepository(IUnitOfWork<DbContext> unitOfWork, IAdaptor adaptor)
            : base(unitOfWork)
        {
            _adaptor = adaptor;
        }

        #endregion

        #region IQuestionPoolRepository Methods

        #region Common
        /// <summary>
        /// Get Exam Types
        /// </summary>
        /// <returns></returns>
        public List<ExamType> GetExamTypes()
        {
            var repository = RepositoryProvider.GetRepository<Model.ExamType>(UnitOfWork);
            var examTypes = (from d in repository.FindAsQueryable().Where(x => x.IsActive)
                             select d).OrderBy(x => x.Name).ToList();
            return _adaptor.Adapt<List<Model.ExamType>, List<ExamType>>(examTypes);
        }

        /// <summary>
        /// Get Categories
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        public List<Category> GetCategories(int examTypeId)
        {
            var repository = RepositoryProvider.GetRepository<Model.Category>(UnitOfWork);
            var categories = (from d in repository.FindAsQueryable().Where(x => x.IsActive && (x.ExamTypeId == examTypeId || x.ExamTypeId == null))
                              select d).OrderBy(x => x.Name).ToList();
            return _adaptor.Adapt<List<Model.Category>, List<Category>>(categories);
        }

        /// <summary>
        /// Get Exam Names
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        public List<Domain.Entities.Exam> GetExamNames(int examTypeId)
        {
            var repository = RepositoryProvider.GetRepository<Model.Examination>(UnitOfWork);
            var examNames = (from d in repository.FindAsQueryable().Where(x => x.IsActive && x.ExamTypeId == examTypeId).Distinct()
                             select d).OrderBy(x => x.Name).ToList();
            return _adaptor.Adapt<List<Model.Examination>, List<Domain.Entities.Exam>>(examNames);
        }

        #endregion

        #region Question Pool
        /// <summary>
        /// Retrieve Question Bank Details
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<QuestionPool> FindQuestions(QuestionQuery query)
        {
            var dbQuery = RepositoryProvider.GetRepository<Model.QuestionBank>(this.UnitOfWork).FindAsQueryable();

            if (query.Includes != null && query.Includes.Any())
                query.Includes.Where(i => i != null).ToList().ForEach(i => { dbQuery = dbQuery.Include(i); });

            if (query.OrderBy != null && query.OrderBy.Any())
                query.OrderBy.Where(i => i != null).ToList().ForEach(
                    s => dbQuery = QueryHelper.CallOrderBy(dbQuery, s, query.Ascending));

            dbQuery = dbQuery.Where(x => x.ExamTypeId == query.ExamTypeId && x.IsActive == query.IsActive);

            if (query.DifficultLevelId > 0)
                dbQuery = dbQuery.Where(x => x.DifficultLevelId == query.DifficultLevelId);

            if (query.CategoryId > 0)
                dbQuery = dbQuery.Where(x => x.CategoryId == query.CategoryId);

            if (query.IsPublished != null)
                dbQuery = dbQuery.Where(x => x.IsPublished == query.IsPublished);

            var list = dbQuery.OrderByDescending(x => x.Id).ToList();

            return
                _adaptor.Adapt<List<Model.QuestionBank>, List<QuestionPool>>(list);
        }

        /// <summary>
        /// Bulk upload questions and answers by passing list of question bank objects
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public bool AddBulkQuestions(List<QuestionPool> query)
        {
            var questionRepository = RepositoryProvider.GetRepository<Model.QuestionBank>(UnitOfWork);
            foreach (
                var mapped in
                    query.Select(question => _adaptor.Adapt<QuestionPool, Model.QuestionBank>(question))
                )
            {
                mapped.Category = null;
                mapped.ExamType = null;
                mapped.CreatedDate = DateTime.Now;
                mapped.UpdatedDate = DateTime.Now;
                questionRepository.Add(mapped);

            }

            return UnitOfWork.Commit() > 0;
        }

        /// <summary>
        /// Bulk Upload Question
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public bool BulkUploadQuestion(DataTable table)
        {
            var warnings = new SqlParameter("questions", SqlDbType.Structured)
            {
                Value = table,
                TypeName = "[exam].[udt_QuestionBank]"
            };
            var d = this.UnitOfWork.DbContext.Database.ExecuteSqlCommand("EXEC [exam].[sp_UploadQuestion]", warnings) >
                    0;

            return d;
        }

        /// <summary>
        /// Find
        /// </summary>
        /// <param name="questionId"></param>
        /// <returns></returns>
        public QuestionPool FindQuestion(long questionId)
        {
            return
                _adaptor.Adapt<Model.QuestionBank, QuestionPool>(
                    RepositoryProvider.GetRepository<Model.QuestionBank>(UnitOfWork).FindAsQueryable().Where(x => x.Id == questionId && x.IsActive).Include(x => x.Answers).FirstOrDefault());
        }

        /// <summary>
        /// Update Question
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool UpdateQuestion(QuestionBank entity)
        {
            var dbo = RepositoryProvider.GetRepository<Model.QuestionBank>(UnitOfWork).Find(entity.Id);
            var questionRepository = RepositoryProvider.GetRepository<Model.QuestionBank>(UnitOfWork);
            var answerRepository = RepositoryProvider.GetRepository<Model.Answer>(UnitOfWork);
            var answers = answerRepository.FindAsQueryable().Where(x => x.QuestionId == entity.Id).ToList();
            if (answers.Count > 0)
            {
                foreach (var item in answers)
                {
                    if (!entity.Answers.Select(x => x.Id).Contains(item.Id))
                    {
                        answerRepository.Remove(item);
                    }
                }
                foreach (var ans in entity.Answers)
                {
                    if (answers.Select(x => x.Id).ToList().Contains(ans.Id))
                    {
                        answerRepository.Update(_adaptor.Adapt<Answer, Model.Answer>(ans));
                    }
                    else
                    {
                        ans.QuestionId = (int)entity.Id;
                        answerRepository.Add(_adaptor.Adapt<Answer, Model.Answer>(ans));
                    }
                }
            }
            else
            {
                foreach (var ans in entity.Answers)
                {
                    ans.QuestionId = (int)entity.Id;
                    answerRepository.Add(_adaptor.Adapt<Answer, Model.Answer>(ans));
                }
            }

            dbo.ExamTypeId = (int)entity.ExamType.Id;
            dbo.CategoryId = (int)entity.Category.Id;
            dbo.DifficultLevelId = (int)entity.DifficultLevel;
            dbo.QuestionTypeId = (int)entity.QuestionType;
            dbo.Question = entity.Question;
            dbo.IsPublished = entity.IsPublished;
            dbo.CreatedBy = entity.CreatedBy;
            dbo.UpdatedBy = entity.UpdatedBy;
            dbo.UpdatedDate = entity.UpdatedOn;
            dbo.CreatedDate = entity.CreatedOn;
            dbo.IsActive = entity.IsActive;
            questionRepository.Update(dbo);


            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;

        }

        /// <summary>
        /// Add Question
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool AddQuestion(QuestionPool entity)
        {
            var provider = RepositoryProvider.GetRepository<Model.QuestionBank>(UnitOfWork);
            provider.Add(_adaptor.Adapt<QuestionPool, Model.QuestionBank>(entity));
            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// De Activate Question
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool DeActivateQuestion(QuestionPool entity)
        {
            var dbo = RepositoryProvider.GetRepository<Model.QuestionBank>(UnitOfWork).Find(entity.Id);
            var questionRepository = RepositoryProvider.GetRepository<Model.QuestionBank>(UnitOfWork);

            dbo.UpdatedBy = entity.UpdatedBy;
            dbo.UpdatedDate = entity.UpdatedOn;
            dbo.IsActive = entity.IsActive;
            questionRepository.Update(dbo);
            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        #endregion

        #region Exam Type
        /// <summary>
        /// Find Exam Types
        /// </summary>
        /// <returns></returns>
        public List<ExamType> FindExamTypes()
        {
            var dbQuery = RepositoryProvider.GetRepository<Model.ExamType>(UnitOfWork).
                FindAsQueryable().Where(x => x.IsActive).OrderBy(x => x.Name).ToList();
            return
                _adaptor.Adapt<List<Model.ExamType>, List<ExamType>>(dbQuery);
        }

        /// <summary>
        /// Add Exam Type
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool AddExamType(ExamType entity)
        {
            var provider = RepositoryProvider.GetRepository<Model.ExamType>(UnitOfWork);
            provider.Add(_adaptor.Adapt<ExamType, Model.ExamType>(entity));
            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// Update Exam Type
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool UpdateExamType(ExamType entity)
        {
            var provider = RepositoryProvider.GetRepository<Model.ExamType>(UnitOfWork);

            Model.ExamType dbu = provider.Find(entity.Id);
            dbu.Name = entity.Name;
            dbu.Description = entity.Description;
            dbu.IsActive = entity.IsActive;

            provider.Update(dbu);
            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// Is ExamType Name Exist
        /// </summary>
        /// <param name="examTypeName"></param>
        /// <returns></returns>
        public bool IsExamTypeNameExist(string examTypeName)
        {
            var r =
                RepositoryProvider.GetRepository<Model.ExamType>(UnitOfWork)
                    .FindAsQueryable()
                    .Where(x => x.Name.Trim().ToLower() == examTypeName.Trim().ToLower() && x.IsActive == true).ToList();

            return r.Count > 0;
        }

        /// <summary>
        /// Find Exam Type
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ExamType FindExamType(long id)
        {
            var dbo = RepositoryProvider.GetRepository<Model.ExamType>(UnitOfWork).Find(id);
            return _adaptor.Adapt<Model.ExamType, ExamType>(dbo);
        }

        #endregion

        #region Category
        /// <summary>
        /// Find Categories
        /// </summary>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        public List<Category> FindCategories(int examTypeId)
        {
            var dbQuery = RepositoryProvider.GetRepository<Model.Category>(UnitOfWork).
               FindAsQueryable().Where(x => x.IsActive && (x.ExamTypeId == examTypeId || x.ExamTypeId == null)).OrderBy(x => x.Name).ToList();
            return
                _adaptor.Adapt<List<Model.Category>, List<Category>>(dbQuery);
        }

        /// <summary>
        /// Is Category Name Exist
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public bool IsCategoryNameExist(string categoryName)
        {
            var r =
                RepositoryProvider.GetRepository<Model.Category>(UnitOfWork)
                    .FindAsQueryable()
                    .Where(x => x.Name.Trim().ToLower() == categoryName.Trim().ToLower() && x.IsActive == true).ToList();

            return r.Count > 0;
        }

        /// <summary>
        /// Add Category
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool AddCategory(Category entity)
        {
            var provider = RepositoryProvider.GetRepository<Model.Category>(UnitOfWork);
            provider.Add(_adaptor.Adapt<Category, Model.Category>(entity));
            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// Update Category
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool UpdateCategory(Category entity)
        {
            var provider = RepositoryProvider.GetRepository<Model.Category>(UnitOfWork);

            Model.Category dbu = provider.Find(entity.Id);
            dbu.Name = entity.Name;
            dbu.Description = entity.Description;
            dbu.IsActive = entity.IsActive;
            if (entity.ExamType != null)
                dbu.ExamTypeId = (int?)entity.ExamType.Id;
            else
                dbu.ExamTypeId = null;

            provider.Update(dbu);
            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// Find Category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Category FindCategory(long id)
        {
            var dbo = RepositoryProvider.GetRepository<Model.Category>(UnitOfWork).Find(id);
            return _adaptor.Adapt<Model.Category, Category>(dbo);

        }

        #endregion

        #region Exam

        /// <summary>
        /// Find Exams
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<Domain.Entities.Exam> FindExams(ExamQuery query)
        {
            var dbQuery = RepositoryProvider.GetRepository<Model.Examination>(this.UnitOfWork).FindAsQueryable();

            if (query.Includes != null && query.Includes.Any())
                query.Includes.Where(i => i != null).ToList().ForEach(i => { dbQuery = dbQuery.Include(i); });

            if (query.OrderBy != null && query.OrderBy.Any())
                query.OrderBy.Where(i => i != null).ToList().ForEach(
                    s => dbQuery = QueryHelper.CallOrderBy(dbQuery, s, query.Ascending));

            dbQuery = dbQuery.Where(x => x.ExamTypeId == query.ExamTypeId && x.IsActive == query.IsActive);

            if (query.ExamId > 0)
                dbQuery = dbQuery.Where(x => x.Id == query.ExamId);

            var list = dbQuery.OrderByDescending(x => x.Id).ToList();

            return
                _adaptor.Adapt<List<Model.Examination>, List<Domain.Entities.Exam>>(list);
        }

        /// <summary>
        /// Find Exam
        /// </summary>
        /// <param name="examId"></param>
        /// <returns></returns>
        public Domain.Entities.Exam FindExam(long examId)
        {
            var questionCategoryEntities = new List<QuestionCategory>();
            var exam = RepositoryProvider.GetRepository<Examination>(UnitOfWork).FindAsQueryable().Include(x => x.ExamQuestionCategories).Include(x => x.ExamQuestions).FirstOrDefault(x => x.Id == examId && x.IsActive);
            var examEntity = _adaptor.Adapt<Model.Examination, Domain.Entities.Exam>(exam);
            if (exam != null)
            {
                questionCategoryEntities =
                    _adaptor.Adapt<List<Model.ExamQuestionCategory>, List<Domain.Entities.QuestionCategory>>(
                        exam.ExamQuestionCategories.ToList());

                examEntity.QuestionCategories = questionCategoryEntities;
                //foreach (var category in questionCategoryEntities)
                //{
                //    category.Questions = new List<QuestionPool>();
                //    var examQuestions = exam.ExamQuestions.Where(
                //        x => x.DifficultLevelId == (int)category.DifficultLevel && x.QuestionCategoryId == category.Category.Id && x.ExamId == (int)examId).ToList();
                //    category.Questions.AddRange(_adaptor.Adapt<List<Model.ExamQuestion>, List<Domain.Entities.QuestionBank>>(examQuestions.ToList()));
                //}
            }
            return examEntity;
        }

        /// <summary>
        /// De Activate Exam
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool DeActivateExam(Domain.Entities.Exam entity)
        {
            var dbo = RepositoryProvider.GetRepository<Model.Examination>(UnitOfWork).Find(entity.Id);
            var questionRepository = RepositoryProvider.GetRepository<Model.Examination>(UnitOfWork);

            dbo.UpdatedBy = entity.UpdatedBy;
            dbo.UpdatedOn = entity.UpdatedOn;
            dbo.IsActive = entity.IsActive;
            questionRepository.Update(dbo);
            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// Add Exam
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool AddExam(Domain.Entities.Exam entity)
        {
            var examProvider = RepositoryProvider.GetRepository<Model.Examination>(UnitOfWork);
            var questionCategoryProvider = RepositoryProvider.GetRepository<Model.ExamQuestionCategory>(UnitOfWork);
            var examQuestionProvider = RepositoryProvider.GetRepository<Model.ExamQuestion>(UnitOfWork);
            var examQuestionAnswerProvider = RepositoryProvider.GetRepository<Model.ExamQuestionAnswer>(UnitOfWork);
            var answerRepository = RepositoryProvider.GetRepository<Model.Answer>(UnitOfWork);

            examProvider.Add(_adaptor.Adapt<Domain.Entities.Exam, Model.Examination>(entity));
            foreach (var quesCategory in entity.QuestionCategories)
            {
                questionCategoryProvider.Add(
                    _adaptor.Adapt<Domain.Entities.QuestionCategory, Model.ExamQuestionCategory>(quesCategory));
                if (quesCategory.Questions != null)
                {
                    foreach (var question in quesCategory.Questions)
                    {
                        var exQuestion = _adaptor.Adapt<Domain.Entities.QuestionBank, Model.ExamQuestion>(question);
                        examQuestionProvider.Add(exQuestion);

                        var answers =
                            answerRepository.FindAsQueryable().Where(x => x.QuestionId == question.Id).ToList();
                        question.Answers = _adaptor.Adapt<List<Model.Answer>, List<Domain.Entities.Answer>>(answers);
                        foreach (var answer in question.Answers)
                        {
                            var ans = _adaptor.Adapt<Domain.Entities.Answer, Model.ExamQuestionAnswer>(answer);
                            ans.ExamQuestion = exQuestion;
                            examQuestionAnswerProvider.Add(ans);
                        }
                    }
                }
            }
            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// Update Exam
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool UpdateExam(Domain.Entities.Exam entity)
        {
            var dbo =
                RepositoryProvider.GetRepository<Examination>(UnitOfWork)
                    .FindAsQueryable().FirstOrDefault(x => x.Id == entity.Id);
            var examRepository = RepositoryProvider.GetRepository<Model.Examination>(UnitOfWork);
            var questionCategoryRepository = RepositoryProvider.GetRepository<Model.ExamQuestionCategory>(UnitOfWork);
            var examQuestionRepository = RepositoryProvider.GetRepository<Model.ExamQuestion>(UnitOfWork);
            var examQuestionAnswerRepository = RepositoryProvider.GetRepository<Model.ExamQuestionAnswer>(UnitOfWork);
            var categories = questionCategoryRepository.FindAsQueryable().Where(x => x.ExamId == dbo.Id).ToList();
            var answerRepository = RepositoryProvider.GetRepository<Model.Answer>(UnitOfWork);

            foreach (var cat in categories)
            {
                var examQuestions = examQuestionRepository.FindAsQueryable().Where(x => x.ExamId == dbo.Id).ToList();
                foreach (var exQues in examQuestions)
                {
                    var examQuestionAnswers = examQuestionAnswerRepository.FindAsQueryable().Where(x => x.ExamQuestionId == exQues.Id)
                            .ToList();
                    foreach (var exQuesAns in examQuestionAnswers)
                    {
                        examQuestionAnswerRepository.Remove(exQuesAns);
                    }
                    examQuestionRepository.Remove(exQues);
                }
                questionCategoryRepository.Remove(cat);
            }

            if (dbo != null)
            {
                dbo.ExamTypeId = (int)entity.ExamType.Id;
                dbo.Name = entity.Name;
                dbo.Description = entity.Description;
                dbo.DifficultLevelId = (int)entity.DifficultLevelId;
                dbo.Duration = entity.Duration;
                dbo.IsQuestionManualPick = entity.IsManualPick;
                dbo.ResultFormatTypeId = (int)entity.ResultFormatType;
                dbo.PassScore = entity.PassScore;
                dbo.CreatedBy = entity.CreatedBy;
                dbo.CreatedOn = entity.CreatedOn;
                dbo.UpdatedOn = entity.UpdatedOn;
                dbo.UpdatedBy = entity.UpdatedBy;
                dbo.IsActive = entity.IsActive;
                examRepository.Update(dbo);

                foreach (var quesCategory in entity.QuestionCategories)
                {
                    var exCat = _adaptor.Adapt<Domain.Entities.QuestionCategory, Model.ExamQuestionCategory>(quesCategory);
                    exCat.ExamId = dbo.Id;
                    questionCategoryRepository.Add(exCat);
                    if (quesCategory.Questions != null)
                    {
                        foreach (var question in quesCategory.Questions)
                        {
                            var exQuestion = _adaptor.Adapt<Domain.Entities.QuestionBank, Model.ExamQuestion>(question);
                            exQuestion.ExamId = dbo.Id;
                            examQuestionRepository.Add(exQuestion);

                            var answers =
                                answerRepository.FindAsQueryable().Where(x => x.QuestionId == question.Id).ToList();
                            question.Answers = _adaptor.Adapt<List<Model.Answer>, List<Domain.Entities.Answer>>(answers);
                            foreach (var answer in question.Answers)
                            {
                                var ans = _adaptor.Adapt<Domain.Entities.Answer, Model.ExamQuestionAnswer>(answer);
                                ans.ExamQuestion = exQuestion;
                                examQuestionAnswerRepository.Add(ans);
                            }
                        }
                    }
                }
            }
            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// Find Questions
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<QuestionBank> FindQuestions(QuestionSelectionQuery query)
        {
            var dbQuery = RepositoryProvider.GetRepository<Model.QuestionBank>(this.UnitOfWork).FindAsQueryable();

            if (query.Includes != null && query.Includes.Any())
                query.Includes.Where(i => i != null).ToList().ForEach(i => { dbQuery = dbQuery.Include(i); });

            if (query.OrderBy != null && query.OrderBy.Any())
                query.OrderBy.Where(i => i != null).ToList().ForEach(
                    s => dbQuery = QueryHelper.CallOrderBy(dbQuery, s, query.Ascending));

            dbQuery = dbQuery.Where(x => x.IsActive == query.IsActive && x.IsPublished);

            if (query.DifficultLevelId > 0)
                dbQuery = dbQuery.Where(x => x.DifficultLevelId == query.DifficultLevelId);

            if (query.CategoryId > 0)
                dbQuery = dbQuery.Where(x => x.CategoryId == query.CategoryId);

            var list = dbQuery.OrderBy(x => x.Id).ToList();

            return
                _adaptor.Adapt<List<Model.QuestionBank>, List<QuestionBank>>(list);
        }

        /// <summary>
        /// Is Exam Name Exist
        /// </summary>
        /// <param name="examName"></param>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        public bool IsExamNameExist(string examName, int examTypeId)
        {
            var r =
               RepositoryProvider.GetRepository<Model.Examination>(UnitOfWork)
                   .FindAsQueryable()
                   .Where(x => x.Name.Trim().ToLower() == examName.Trim().ToLower() && x.IsActive == true && x.ExamTypeId == examTypeId).ToList();

            return r.Count > 0;
        }

        /// <summary>
        /// Find Exam Question Category
        /// </summary>
        /// <param name="questionCategoryId"></param>
        /// <returns></returns>
        public long FindExamQuestionCategory(long questionCategoryId)
        {
            var entity = RepositoryProvider.GetRepository<Model.ExamQuestionCategory>(UnitOfWork).Find(questionCategoryId);
            if (entity != null) return entity.ExamId;
            return 0;
        }

        /// <summary>
        /// Is Questions Available
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool IsQuestionsAvailable(Domain.Entities.Exam entity)
        {
            foreach (var category in entity.QuestionCategories)
            {
                var count = RepositoryProvider.GetRepository<Model.QuestionBank>(UnitOfWork).FindAsQueryable()
                    .Where(
                        x => x.DifficultLevelId == (int)category.DifficultLevel && x.CategoryId == category.Category.Id && x.IsActive && x.IsPublished)
                    .ToList()
                    .Count;
                if (category.Count > count)
                {
                    return true;

                }
            }
            return false;
        }

        #endregion

        #endregion

    }
}
