﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Adaptor;
using attune.Core.Repository;
using attune.Core.Repository.EF6;
using attune.Core.UnitOfWork;
using attune.Exam.Domain.Entities;
using attune.Exam.Domain.Queries;
using attune.Exam.Repository.EF.Model;
using attune.Exam.Repository.Interfaces;
using Schedule = attune.Exam.Domain.Entities.Schedule;

namespace attune.Exam.Repository.EF
{
    public class AssignmentRepository : RepositoryBase<DbContext>, IAssignmentRepository
    {
        #region private members

        private readonly IAdaptor _adaptor;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="AssignmentRepository" /> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="adaptor"></param>
        public AssignmentRepository(IUnitOfWork<DbContext> unitOfWork, IAdaptor adaptor)
            : base(unitOfWork)
        {
            _adaptor = adaptor;
        }

        #endregion

        #region IAssignmentRepository Methods

        /// <summary>
        /// Add Exam Schedule
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public bool AddExamSchedule(ExamScheduleCreateOrUpdateQuery query)
        {
            var scheduleProvider = RepositoryProvider.GetRepository<Model.Schedule>(UnitOfWork);
            var dSchedule = _adaptor.Adapt<ExamScheduleCreateOrUpdateQuery, Model.Schedule>(query);

            scheduleProvider.Add(dSchedule);

            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// Update Exam Schedule
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public bool UpdateExamSchedule(ExamScheduleCreateOrUpdateQuery query)
        {
            var scheduleProvider = RepositoryProvider.GetRepository<Model.Schedule>(UnitOfWork);
            var scheduleCandidateProvider = RepositoryProvider.GetRepository<Model.ScheduledCandidate>(UnitOfWork);
            var currentSchedule = scheduleProvider.FindAsQueryable().Where(x => x.Id == query.Schedule.Id).Include("ScheduledCandidates").FirstOrDefault();
            if (currentSchedule == null) throw new NullReferenceException("Schedule is not available");

            foreach (var candidate in currentSchedule.ScheduledCandidates.ToList())
                scheduleCandidateProvider.Remove(candidate);

            currentSchedule = _adaptor.Adapt<ExamScheduleCreateOrUpdateQuery, Model.Schedule>(query);

            foreach (var candidate in currentSchedule.ScheduledCandidates)
                scheduleCandidateProvider.Add(candidate);

            scheduleProvider.Update(currentSchedule);

            return UnitOfWork.Commit() > Constants.SUCCESS_COMMIT;
        }

        /// <summary>
        /// Find Schedule Candidates
        /// </summary>
        /// <param name="scheduleId"></param>
        /// <returns></returns>
        public List<Candidate> FindScheduleCandidates(int scheduleId)
        {
            var rep =
                RepositoryProvider.GetRepository<Model.ScheduledCandidate>(UnitOfWork)
                    .FindAsQueryable()
                    .Where(x => x.ScheduleId == scheduleId)
                    .ToList();
            return
                _adaptor.Adapt<List<Model.ScheduledCandidate>, List<Candidate>>(rep);

        }

        public List<Assignment> FindSchedules()
        {
            var rep =
                RepositoryProvider.GetRepository<Model.Schedule>(UnitOfWork)
                    .FindAsQueryable()
                    .ToList();
            return
                _adaptor.Adapt<List<Model.Schedule>, List<Assignment>>(rep);
        }

        #endregion
    }
}
