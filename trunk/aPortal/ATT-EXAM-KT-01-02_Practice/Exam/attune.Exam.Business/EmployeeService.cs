﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Adaptor;
using attune.Core.Domain;
using attune.Exam.Business.Interfaces;
using attune.Exam.Domain.Entities;
using attune.Exam.Repository.Interfaces;

namespace attune.Exam.Business
{
    public class EmployeeService : DomServiceBase, IEmployeeService
    {
        #region Private Variables

        private readonly IEmployeeRepository _employeeRepository;
        private readonly IAdaptor _adaptor;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeService" /> class.
        /// </summary>
        /// <param name="employeeRepository">The authorization repository.</param>
        /// <param name="adaptor">The adaptor.</param>
        public EmployeeService(IEmployeeRepository employeeRepository, IAdaptor adaptor)
        {
            _employeeRepository = employeeRepository;
            _adaptor = adaptor;
        }

        #endregion

        #region IemployeeService Members
        /// <summary>
        /// Get Divisions
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetDivisions(string divisionCode)
        {
            return
                _employeeRepository.GetDivisions(divisionCode).Select(item => new KeyValuePair<string, string>(item.Key.ToString(), item.Value.ToString()))
                    .ToList();
        }

        /// <summary>
        /// Get Sections
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetSections(string divisionCode)
        {
            return
                _employeeRepository.GetSections(divisionCode).Select(item => new KeyValuePair<string, string>(item.Key.ToString(), item.Value.ToString()))
                    .ToList();
        }

        /// <summary>
        ///  Get Designations
        /// </summary>
        /// <returns></returns>
        public List<KeyValuePair<string, string>> GetDesignations()
        {
            return
                _employeeRepository.GetDesignations().Select(item => new KeyValuePair<string, string>(item.Key.ToString(), item.Value.ToString()))
                    .ToList();
        }

        /// <summary>
        /// Get Employee Detail
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <param name="sectionCode"></param>
        /// <param name="designationCode"></param>
        /// <returns></returns>
        public List<Employee> GetEmployeeDetail(string divisionCode, string sectionCode, string designationCode)
        {
            return _employeeRepository.GetEmployeeDetail(divisionCode, sectionCode,designationCode);
        }

        /// <summary>
        /// Get Non EmployeeDetail
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<Employee> GetNonEmployeeDetail(DateTime startDate, DateTime endDate)
        {
            return _employeeRepository.GetNonEmployeeDetail(startDate, endDate);
        }

        #endregion
    }
}
