﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;

namespace attune.Exam.Business.Exceptions
{
    [Serializable]
    public abstract class ExceptionBase : ApplicationException
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionBase" /> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="message">The message.</param>
        protected ExceptionBase(Exception exception, string message)
            : base(message, exception)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionBase" /> class.
        /// </summary>
        /// <param name="message">A message that describes the error.</param>
        protected ExceptionBase(string message)
            : base(message, new Exception(message))
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="ExceptionBase" /> class.
        /// </summary>
        protected ExceptionBase()
        {
            ExceptionType = ExceptionType.None;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets the unique key.
        /// </summary>
        /// <value>
        ///     The unique key.
        /// </value>
        public abstract string UniqueKey { get; }

        /// <summary>
        ///     Gets or sets the type of the exception.
        /// </summary>
        /// <value>
        ///     The type of the exception.
        /// </value>
        public ExceptionType ExceptionType { get; set; }

        #endregion
    }
}