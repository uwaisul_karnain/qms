﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;

namespace attune.Exam.Business.Exceptions
{
    public class EntityValidationErrorsException : ExceptionBase
    {
        #region Overrides of ExceptionBase

        public override string UniqueKey
        {
            get { return Constants.EX_CODE_ENTITY_VALIDATOR; }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="EntityValidationErrorsException" /> class.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="message">The message.</param>
        public EntityValidationErrorsException(Exception exception, string message)
            : base(exception, message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EntityValidationErrorsException" /> class.
        /// </summary>
        /// <param name="message">The message.</param>
        public EntityValidationErrorsException(string message)
            : base(message)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="EntityValidationErrorsException" /> class.
        /// </summary>
        public EntityValidationErrorsException()
        {
            ExceptionType = ExceptionType.EntityValdation;
        }

        #endregion
    }
}