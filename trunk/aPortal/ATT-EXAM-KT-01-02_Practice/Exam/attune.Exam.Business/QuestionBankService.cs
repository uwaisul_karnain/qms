﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Adaptor;
using attune.Core.Domain;
using attune.Exam.Business.Exceptions;
using attune.Exam.Business.Interfaces;
using attune.Exam.Domain.Entities;
using attune.Exam.Infrastructure.Resources;
using attune.Exam.Repository.Interfaces;

namespace attune.Exam.Business
{
    public class QuestionBankService : DomServiceBase, IQuestionBankService
    {
        #region Private Variables

        private readonly IQuestionBankRepository _questionBankRepository;
        private readonly IAdaptor _adaptor;
        #endregion

         #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="QuestionBankService" /> class.
        /// </summary>
        /// <param name="questionBankRepository">The user managment repository.</param>
        /// <param name="adaptor">The adaptor.</param>
        public QuestionBankService(IQuestionBankRepository questionBankRepository, IAdaptor adaptor)
        {
            _questionBankRepository = questionBankRepository;
            _adaptor = adaptor;
        }

        #endregion

        /// <summary>
        /// Is Having Answer
        /// </summary>
        /// <param name="answers"></param>
        /// <returns></returns>
        public bool IsHavingAnswer(List<Answer> answers)
        {
            if (answers.Count == 0)
            {
                throw new AnswerNullException(BusinessExceptions.AnswerRequired);
            }
            else
            {
                if (!answers.Any(x => x.IsCorrect))
                {
                    throw new AnswerNullException(BusinessExceptions.CorrectAnswerRequired);
                }
            }
            return true;
        }

        /// <summary>
        /// Is ExamType Name Exist
        /// </summary>
        /// <param name="examTypeName"></param>
        /// <returns></returns>
        public bool IsExamTypeNameExist(string examTypeName)
        {
            if (_questionBankRepository.IsExamTypeNameExist(examTypeName))
                throw new RecordExistException(BusinessExceptions.RecordAlreadyExist);
            return true;
        }

        /// <summary>
        /// Is Category Name Exist
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public bool IsCategoryNameExist(string categoryName)
        {
            if (_questionBankRepository.IsCategoryNameExist(categoryName))
                throw new RecordExistException(BusinessExceptions.RecordAlreadyExist);
            return true;
        }

        /// <summary>
        /// Is Exam Name Exist
        /// </summary>
        /// <param name="examName"></param>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        public bool IsExamNameExist(string examName, int examTypeId)
        {
            if (_questionBankRepository.IsExamNameExist(examName,examTypeId))
                throw new RecordExistException(BusinessExceptions.RecordAlreadyExist);
            return true;
        }

        /// <summary>
        /// Is Question Category Duplicated
        /// </summary>
        /// <param name="questionCategories"></param>
        /// <returns></returns>
        public bool IsQuestionCategoryDuplicated(List<QuestionCategory>questionCategories )
        {
            var list = new List<QuestionCategory>();
            foreach (var ques in questionCategories)
            {
                if (list.Any(x => x.Category.Id == ques.Category.Id && x.DifficultLevel == ques.DifficultLevel))
                {
                    throw new RecordExistException(BusinessExceptions.CannotAddDuplicateQuestionCategories);
                }
                list.Add(ques);
            }
            return true;
        }

        /// <summary>
        /// Is Questions Available
        /// </summary>
        /// <param name="exam"></param>
        /// <returns></returns>
        public bool IsQuestionsAvailable(Domain.Entities.Exam exam)
        {
            if (_questionBankRepository.IsQuestionsAvailable(exam))
                throw new RecordExistException(BusinessExceptions.QuestionCountExceeded);
            return true;
        }
    }

}
