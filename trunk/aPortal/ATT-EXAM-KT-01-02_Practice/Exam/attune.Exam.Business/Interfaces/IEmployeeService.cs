﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Exam.Domain.Entities;

namespace attune.Exam.Business.Interfaces
{
    public interface IEmployeeService
    {
        /// <summary>
        /// Get Divisions
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetDivisions(string divisionCode);

        /// <summary>
        /// Get Sections
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetSections(string divisionCode);

        /// <summary>
        /// Get Designations
        /// </summary>
        /// <returns></returns>
        List<KeyValuePair<string, string>> GetDesignations();

        /// <summary>
        /// Get Employee Detail
        /// </summary>
        /// <param name="divisionCode"></param>
        /// <param name="sectionCode"></param>
        /// <param name="designationCode"></param>
        /// <returns></returns>
        List<Employee> GetEmployeeDetail(string divisionCode, string sectionCode,string designationCode);

        /// <summary>
        /// Get Non EmployeeDetail
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        List<Employee> GetNonEmployeeDetail(DateTime startDate, DateTime endDate);
    }
}
