﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Exam.Domain.Entities;

namespace attune.Exam.Business.Interfaces
{
    public interface IQuestionPoolService
    {
        /// <summary>
        /// Is Having Answer
        /// </summary>
        /// <param name="answers"></param>
        /// <returns></returns>
        bool IsHavingAnswer(List<Answer> answers);

        /// <summary>
        /// Is ExamType Name Exist
        /// </summary>
        /// <param name="examTypeName"></param>
        /// <returns></returns>
        bool IsExamTypeNameExist(string examTypeName);

        /// <summary>
        /// Is Category Name Exist
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        bool IsCategoryNameExist(string categoryName);

        /// <summary>
        /// Is Exam Name Exist
        /// </summary>
        /// <param name="examName"></param>
        /// <param name="examTypeId"></param>
        /// <returns></returns>
        bool IsExamNameExist(string examName, int examTypeId);

        /// <summary>
        /// Is Question Category Duplicated
        /// </summary>
        /// <param name="questionCategories"></param>
        /// <returns></returns>
        bool IsQuestionCategoryDuplicated(List<QuestionCategory> questionCategories);

        /// <summary>
        /// Is Questions Available
        /// </summary>
        /// <param name="exam"></param>
        /// <returns></returns>
        bool IsQuestionsAvailable(Domain.Entities.Exam exam);
    }
}
