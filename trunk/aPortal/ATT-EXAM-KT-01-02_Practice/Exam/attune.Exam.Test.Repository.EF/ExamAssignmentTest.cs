﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Text;
using System.Collections.Generic;
using attune.Exam.Domain.Entities;
using attune.Exam.Domain.Queries;
using attune.Exam.Infrastructure.DI;
using attune.Exam.Test.Repository.EF.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace attune.Exam.Test.Repository.EF
{
    /// <summary>
    /// This Test will perfome scenarios of add exam,question category,question and answer details.
    ///
    /// ASSUMPTIONS :
    ///     Master data is used in the data perpration is considered as valid and will not be tested in the logical path of the
    ///     test to validate for exsistance.
    /// SCENARIOS:
    ///     1. Exam  is required for adding schedule, if not provided error message is required to prompt
    ///     2. Schedule is required for adding schedule candidate, if not provided error message is required to prompt
    ///     

    /// </summary>
    [TestClass]
    public class ExamAssignmentTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize()]
        public static void Init(TestContext testContext)
        {
             
        }

        /// <summary>
        /// To run code after all tests in a class have run
        /// </summary>
        [ClassCleanup()]
        public static void Cleanup()
        {

        }

        #endregion

        #region Positive Scenarios

        /// <summary>
        /// This performs to add with all required fields
        /// </summary>
        [TestMethod]
        public void PT_Exam_Assignment_With_RequiredFields()
        {
            try
            {
                var result =
                    ExamAssignmentRepositoryMock.ArrangeMockRepository().AddExamSchedule(MockDataHelper.GetFakeScheduleCreateData());
                Assert.IsTrue(result);
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }

        /// <summary>
        /// This performs to add with all required fields
        /// </summary>
        [TestMethod]
        public void PT_Exam_Assignment_Update_With_RequiredFields()
        {
            try
            {
                var created = MockDataHelper.GetFakeScheduleCreateData();
                created.Schedule.Id = 1;
                var result =
                    ExamAssignmentRepositoryMock.ArrangeMockRepository().UpdateExamSchedule(created);
                Assert.IsTrue(result);
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }

        #endregion


        #region Negative Scenarios
        [TestMethod]
        public void NT_Exam_Assignment_Query_CannotBeNull()
        {
            try
            {
                var result =
                   ExamAssignmentRepositoryMock.ArrangeMockRepository().AddExamSchedule(null);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message, string.Format("query {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
                return;
            }
            Assert.Fail(Constants.DEFUALT_ERROR_ON_TEST_FAILED);
        }
        #endregion
    }
}
