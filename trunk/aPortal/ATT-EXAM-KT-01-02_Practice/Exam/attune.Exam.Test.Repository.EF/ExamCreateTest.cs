﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using attune.Exam.Test.Repository.EF.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using attune.Exam.Domain.Entities;
using attune.Exam.Repository.EF;

namespace attune.Exam.Test.Repository.EF
{
    /// <summary>
    /// This Test will perfome scenarios of add exam,question category,question and answer details.
    ///
    /// ASSUMPTIONS :
    ///     Master data is used in the data perpration is considered as valid and will not be tested in the logical path of the
    ///     test to validate for exsistance.
    /// SCENARIOS:
    ///     1. ExamType is required for adding exam, if not provided error message is required to prompt
    ///     2. Exam Name is required for adding exam, if not provided error message is required to prompt
    ///     3. Question Category is required for adding exam, if not provided error message is required to prompt
    ///     4. Updating Exam with question categories, exam questions and answers
    
    /// </summary>
    [TestClass]
    public class ExamCreateTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize()]
        public static void Init(TestContext testContext)
        {

        }

        /// <summary>
        /// To run code after all tests in a class have run
        /// </summary>
        [ClassCleanup()]
        public static void Cleanup()
        {

        }

        #endregion

        #region Positive Scenarios
        /// <summary>
        /// This performs to add with all required fields
        /// </summary>
        [TestMethod]
        public void PT_Exam_Question_Add_With_RequiredFields()
        {
            var exam = GetExamTestData();
            try
            {
                var result = ExamRepositoryMock.ArrangeMockRepository().AddExam(exam);
                Assert.IsTrue(result);
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }

        /// <summary>
        /// This performs to update fields
        /// </summary>
        [TestMethod]
        public void PT_Exam_Question_Update_Fields()
        {
            var exam = GetExamTestData();
            exam.Id = 1;
            try
            {
                var result = ExamRepositoryMock.ArrangeMockRepository().UpdateExam(exam);
                Assert.IsTrue(result);
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }
        #endregion


        #region Negative Scenarios
        /// <summary>
        /// This performs to validate Object can not null
        /// </summary>
        [TestMethod]
        public void NT_Exam_Question_Add_Object_CannotBeNull()
        {
            try
            {
                var exams = ExamRepositoryMock.ArrangeMockRepository().AddExam(null);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message, string.Format("entity {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
                return;
            }
            Assert.Fail(Constants.DEFUALT_ERROR_ON_TEST_FAILED);
        }

        /// <summary>
        /// This performs to validate Exam Type is required
        /// </summary>
        [TestMethod]
        public void NT_Exam_ExamType_Is_RequiredTo_Add()
        {
            var exams = GetExamTestData();
            exams.ExamType = null;
            try
            {
                var res = ExamRepositoryMock.ArrangeMockRepository().AddExam(exams);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message,
                    string.Format("ExamType {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
            }
        }

        /// <summary>
        /// This performs to validate Exam Name is required
        /// </summary>
        [TestMethod]
        public void NT_Exam_ExamName_Is_RequiredTo_Add()
        {
            var exam = GetExamTestData();
            exam.Name = null;
            try
            {
                var res = ExamRepositoryMock.ArrangeMockRepository().AddExam(exam);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message,
                    string.Format("Name {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
            }
        }


        /// <summary>
        /// This performs to validate Question category is required
        /// </summary>
        [TestMethod]
        public void NT_Exam_Question_Category_Is_RequiredTo_Add()
        {
            var exam = GetExamTestData();
            exam.QuestionCategories = null;
            try
            {
                var res = ExamRepositoryMock.ArrangeMockRepository().AddExam(exam);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message,
                    string.Format("QuestionCategories {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
            }
        }
        
        #endregion

        private static Domain.Entities.Exam GetExamTestData()
        {
            var exam = new Domain.Entities.Exam
            {
                
                ExamType = new ExamType() { Id = 1 },
                DifficultLevelId = Domain.DifficultLevel.Beginner,
                Name = "J2EE",
                Duration=(decimal?) 2.30,
                IsManualPick=false,
                ResultFormatType=Domain.ResultFormatType.Percentage,
                PassScore=30,
                IsActive = true,
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                QuestionCategories = new List<QuestionCategory>()
                                          {
                                              new QuestionCategory()
                                              {
                                                  Category = new Category(){Id=1},Count = 2,DifficultLevel = Domain.DifficultLevel.Beginner,
                                                  Questions = new List<QuestionBank>()
                                                  {
                                                      new QuestionBank()
                                                      {
                                                          Category = new Category() { Id = 1 },
                                                          ExamType = new ExamType() { Id = 1 },
                                                          DifficultLevel = Domain.DifficultLevel.Beginner,
                                                          QuestionType = Domain.QuestionType.MultipleDropDown,
                                                          Question = "Question 1",
                                                          IsActive = true,
                                                          IsPublished = true,
                                                          CreatedBy = 1,
                                                          CreatedOn = DateTime.Now,
                                                          Answers = new List<Answer>()
                                                                {
                                                                    new Answer() {Response = "Answer 1", IsCorrect = false},
                                                                    new Answer() {Response = "Answer 2", IsCorrect = false},
                                                                    new Answer() {Response = "Answer 3", IsCorrect = false},
                                                                    new Answer() {Response = "Answer 4", IsCorrect = true}
                                                                }
                                                      }
                                                  }
                                                  
                                              },
                                              new QuestionCategory()
                                              {
                                                  Category = new Category(){Id=2},Count = 3,DifficultLevel = Domain.DifficultLevel.Intermediate
                                                  
                                              },
                                              new QuestionCategory()
                                              {
                                                  Category = new Category(){Id=3},Count = 5,DifficultLevel = Domain.DifficultLevel.Expert
                                                  
                                              },
                                          }

            };
            return exam;
        }

       
    }
}