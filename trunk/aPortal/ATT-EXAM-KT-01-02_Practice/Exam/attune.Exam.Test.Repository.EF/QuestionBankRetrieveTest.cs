﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Linq;
using System.Runtime.CompilerServices;
using attune.Exam.Domain.Queries;
using attune.Exam.Repository.Interfaces;
using attune.Exam.Test.Repository.EF.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace attune.Exam.Test.Repository.EF
{
    /// <summary>
    /// This Test will perfome scenarios of data filteration of question bank details.
    /// Questions can be filtered use of ExamType, Category and Deficulty level.
    ///
    /// ASSUMPTIONS :
    ///     Master data is used in the data perpration is considered as valid and will not be tested in the logical path of the
    ///     test to validate for exsistance.
    /// SCENARIOS:
    ///     1. ExamType is required for all filterations, if not provided error message is required to prompt
    ///     2. Record set can be filtered either form Category or Dificulty level is considered an optional.
    /// </summary>
    [TestClass]
    public class QuestionBankRetrieveTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize()]
        public static void Init(TestContext testContext)
        {

        }

        /// <summary>
        /// To run code after all tests in a class have run
        /// </summary>
        [ClassCleanup()]
        public static void Cleanup()
        {

        }

        #endregion

        #region Positive Scenarios

        /// <summary>
        /// This performs to Filter By All parameters
        /// </summary>
        [TestMethod]
        public void PT_FilterBy_All()
        {
            var query = new QuestionQuery {ExamTypeId = 1, CategoryId = 1, DifficultLevelId = 1};
            try
            {
                var questions = QuestionBankRepositoryMock.ArrangeMockRepository().FindQuestions(query);

                Assert.IsTrue(questions.Count > 0, Constants.DEFUALT_NO_RECORDS_FOUND);
                Assert.IsTrue(questions.Select(x => x.ExamType.Id).Contains(query.ExamTypeId),
                    string.Format("{0} Exam Type", Constants.DEFUALT_INVALID));
                Assert.IsTrue(questions.Select(x => x.Category.Id).Contains(query.CategoryId),
                    string.Format("{0} Category", Constants.DEFUALT_INVALID));
                Assert.IsTrue(questions.Select(x => (int) x.DifficultLevel).Contains(query.DifficultLevelId),
                    string.Format("{0} Difficult Level", Constants.DEFUALT_INVALID));
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }

        /// <summary>
        /// This performs to Filter by Exam Type Only
        /// </summary>
        [TestMethod]
        public void PT_FilterBy_ExamType_Only()
        {
            var query = new QuestionQuery {ExamTypeId = 1};
            try
            {
                var questions = QuestionBankRepositoryMock.ArrangeMockRepository().FindQuestions(query);

                Assert.IsTrue(questions.Count > 0, Constants.DEFUALT_NO_RECORDS_FOUND);
                Assert.IsTrue(questions.Select(x => x.ExamType.Id).Contains(query.ExamTypeId),
                    string.Format("{0} Exam Type", Constants.DEFUALT_INVALID));
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }

        /// <summary>
        /// This performs to filter Exam Type and Category only
        /// </summary>
        [TestMethod]
        public void PT_FilterBy_ExamTypeAndCategory_Only()
        {
            var query = new QuestionQuery {ExamTypeId = 1, CategoryId = 1};
            try
            {
                var questions = QuestionBankRepositoryMock.ArrangeMockRepository().FindQuestions(query);

                Assert.IsTrue(questions.Count > 0, Constants.DEFUALT_NO_RECORDS_FOUND);
                Assert.IsTrue(questions.Select(x => x.ExamType.Id).Contains(query.ExamTypeId),
                    string.Format("{0} Exam Type", Constants.DEFUALT_INVALID));
                Assert.IsTrue(questions.Select(x => x.Category.Id).Contains(query.CategoryId),
                    string.Format("{0} Category", Constants.DEFUALT_INVALID));
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }

        #endregion

        #region Negative Scenarios

        /// <summary>
        /// This performs to validate Query can not null
        /// </summary>
        [TestMethod]
        public void NT_FilterBy_QueryCannotBeNull()
        {
            QuestionQuery query = null;
            try
            {
                var questions = QuestionBankRepositoryMock.ArrangeMockRepository().FindQuestions(query);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message, string.Format("query {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
                return;
            }
            Assert.Fail(Constants.DEFUALT_ERROR_ON_TEST_FAILED);
        }

        /// <summary>
        /// This performs to validate Exam Type is required
        /// </summary>
        [TestMethod]
        public void NT_ExamTypeId_Is_RequiredTo_Filter()
        {
            var query = new QuestionQuery() {CategoryId = 1, DifficultLevelId = 1};
            try
            {
                var questions = QuestionBankRepositoryMock.ArrangeMockRepository().FindQuestions(query);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message,
                    string.Format("ExamTypeId {0}", Constants.DEFUALT_ERROR_PATTERN_GREATER_THAN_ZERO));
                return;
            }
            Assert.Fail(Constants.DEFUALT_ERROR_ON_TEST_FAILED);
        }


        #endregion
    }
}