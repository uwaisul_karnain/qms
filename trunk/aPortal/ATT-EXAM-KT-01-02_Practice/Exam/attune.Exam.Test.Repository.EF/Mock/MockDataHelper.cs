﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Exam.Repository.EF.Model;

namespace attune.Exam.Test.Repository.EF.Mock
{
    public class MockDataHelper
    {
        public static IQueryable<QuestionBank> GetFakeQuestionBankAsQueryable()
        {
            return new List<QuestionBank>()
                   {
                       new QuestionBank()
                       {
                           Id = 1,
                           CategoryId = 1,
                           ExamTypeId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Category = GetFakeCategory(1,1),
                           ExamType = GetFakeExamType(1),
                           DifficultLevel = GetFakeDifficultLevel((int) Domain.DifficultLevel.Beginner),
                           Question = "Question 1",
                           IsActive = true,
                           IsPublished = true,
                           CreatedBy = 1,
                           CreatedDate = new DateTime(),
                           UpdatedBy = 1,
                           UpdatedDate = new DateTime()
                       },
                       new QuestionBank()
                       {
                           Id = 2,
                           CategoryId = 1,
                           ExamTypeId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Category = GetFakeCategory(1,1),
                           ExamType = GetFakeExamType(1),
                           DifficultLevel = GetFakeDifficultLevel((int) Domain.DifficultLevel.Beginner),
                           Question = "Question 2",
                           IsActive = true,
                           IsPublished = true,
                           CreatedBy = 1,
                           CreatedDate = new DateTime(),
                           UpdatedBy = 1,
                           UpdatedDate = new DateTime()
                       },
                       new QuestionBank()
                       {
                           Id = 3,
                           CategoryId = 1,
                           ExamTypeId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Category = GetFakeCategory(1,1),
                           ExamType = GetFakeExamType(1),
                           DifficultLevel = GetFakeDifficultLevel((int) Domain.DifficultLevel.Beginner),
                           Question = "Question 3",
                           IsActive = true,
                           IsPublished = true,
                           CreatedBy = 1,
                           CreatedDate = new DateTime(),
                           UpdatedBy = 1,
                           UpdatedDate = new DateTime()
                       }
                   }.AsQueryable();
        } 

        private static Category GetFakeCategory(int id,int examTypeId)
        {
            return new Category { Id = id, ExamType = GetFakeExamType(examTypeId), IsActive = true, ExamTypeId = examTypeId };
        }

        private static ExamType GetFakeExamType(int id)
        {
            return new ExamType {Id = id, Name = string.Format("Exam {0}", id), IsActive = true};
        }

        private static DifficultLevel GetFakeDifficultLevel(int id)
        {
            return new DifficultLevel {Id = id, Name = string.Format("DifficultLevel {0}", id), IsActive = true};
        }

        public static IQueryable<Examination> GetFakeExamAsQueryable()
        {
            return new List<Examination>()
                   {
                       new Examination()
                       {
                           Id = 1,
                           Name = "Asp.Net",
                           ExamTypeId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Description = "Asp.Net 1",
                           ExamType = GetFakeExamType(1),
                           DifficultLevel = GetFakeDifficultLevel((int) Domain.DifficultLevel.Beginner),
                           Duration = (decimal?) 2.30,
                           IsQuestionManualPick=true,
                           ResultFormatTypeId=(int) Domain.ResultFormatType.Percentage,
                           PassScore= 45,
                           IsActive = true,
                           CreatedBy = 1,
                           CreatedOn = new DateTime(),
                           UpdatedBy = 1,
                           UpdatedOn = new DateTime()
                       },
                       new Examination()
                       {
                           Id = 2,
                           Name = "JAVA",
                           ExamTypeId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Description = "JAVA 1",
                           ExamType = GetFakeExamType(1),
                           DifficultLevel = GetFakeDifficultLevel((int) Domain.DifficultLevel.Beginner),
                           Duration = (decimal?) 3.00,
                           IsQuestionManualPick=true,
                           ResultFormatTypeId=(int) Domain.ResultFormatType.Percentage,
                           PassScore= 50,
                           IsActive = true,
                           CreatedBy = 1,
                           CreatedOn = new DateTime(),
                           UpdatedBy = 1,
                           UpdatedOn = new DateTime()
                       },
                       new Examination()
                       {
                           Id = 3,
                           Name = "VB",
                           ExamTypeId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Description = "VB 1",
                           ExamType = GetFakeExamType(1),
                           DifficultLevel = GetFakeDifficultLevel((int) Domain.DifficultLevel.Beginner),
                           Duration = (decimal?) 3.30,
                           IsQuestionManualPick=true,
                           ResultFormatTypeId=(int) Domain.ResultFormatType.Point,
                           PassScore= 6,
                           IsActive = true,
                           CreatedBy = 1,
                           CreatedOn = new DateTime(),
                           UpdatedBy = 1,
                           UpdatedOn = new DateTime()
                       }
                   }.AsQueryable();
        }

        public static IQueryable<ExamQuestionCategory> GetFakeExamQuestionCategoryAsQueryable()
        {
            return new List<ExamQuestionCategory>()
                   {
                       new ExamQuestionCategory()
                       {
                           Id = 1,
                           ExamId = 1,
                           CategoryId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Count = 2
                       },
                       new ExamQuestionCategory()
                       {
                           Id = 2,
                           ExamId = 2,
                           CategoryId = 1,
                           DifficultLevelId  = (int) Domain.DifficultLevel.Expert,
                           Count = 3
                       }
                       
                   }.AsQueryable();
        }

        public static IQueryable<ExamQuestion> GetFakeExamQuestionAsQueryable()
        {
            return new List<ExamQuestion>()
                   {
                       new ExamQuestion()
                       {
                           Id = 1,
                           ExamId = 1,
                           QuestionId = 1,
                           QuestionCategoryId = 1,
                           QuestionTypeId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Question = "Q1"
                       },
                       new ExamQuestion()
                       {
                           Id = 2,
                           ExamId = 2,
                           QuestionId = 3,
                           QuestionCategoryId = 1,
                           QuestionTypeId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Question = "Q2"
                       },
                       new ExamQuestion()
                       {
                           Id = 3,
                           ExamId = 2,
                           QuestionId = 2,
                           QuestionCategoryId = 1,
                           QuestionTypeId = 1,
                           DifficultLevelId = (int) Domain.DifficultLevel.Beginner,
                           Question = "Q3"
                       }
                   }.AsQueryable();
        }

        public static IQueryable<ExamQuestionAnswer> GetFakeExamQuestionAnswerAsQueryable()
        {
            return new List<ExamQuestionAnswer>()
                   {
                       new ExamQuestionAnswer()
                       {
                           Id = 1,
                           ExamQuestionId = 1,
                           Answer = "Answer 1",
                           IsCorrect = true
                           
                       },
                       new ExamQuestionAnswer()
                       {
                           Id = 2,
                           ExamQuestionId = 1,
                           Answer = "Answer 2",
                           IsCorrect = false
                           
                       },
                       new ExamQuestionAnswer()
                       {
                           Id = 3,
                           ExamQuestionId = 1,
                           Answer = "Answer 3",
                           IsCorrect = false
                           
                       },
                   }.AsQueryable();
        }

        #region Fake for Exam schedule
        public static IQueryable<Schedule> GetFakeExamFakeScheduleAsQueryable()
        {
            return new List<Schedule>()
            {
                new Schedule()
                {
                    Id = 1,
                    StartDateTime = DateTime.Now,
                    EndDateTime = DateTime.Now,
                    IsInviteRequired = true,
                    IsEmployee = true,
                    OnBehalfEmail = null,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now,
                    ScheduledCandidates = GetFakeExamFakeScheduleCandidates()
                }
            }.AsQueryable();
        }

        public static List<ScheduledCandidate> GetFakeExamFakeScheduleCandidates()
        {
            return new List<ScheduledCandidate>()
            {
                new ScheduledCandidate(){
                    Id =1
                },
                new ScheduledCandidate(){
                    Id =2
                }
            };
        }

        public static Domain.Queries.ExamScheduleCreateOrUpdateQuery GetFakeScheduleCreateData()
        {
            return new Domain.Queries.ExamScheduleCreateOrUpdateQuery()
            {
                Schedule = new attune.Exam.Domain.Entities.Schedule()
                {
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now,
                    IsCalendarInviteRequired = true,
                    IsEmployee = true,
                    OnBehalfEmail = null,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now
                },
                Assignment = new Domain.Entities.Assignment()
                {
                    IsAvailableAlways = true,
                    Exam = new Domain.Entities.Exam() { Id = 64 },
                    AssignmentStatus = Domain.ExamStatus.Pending,
                    IsResultDisplay = true
                },
                Candidates = new List<Domain.Entities.Candidate>()
                                                                                                          {
                                                                                                              new Domain.Entities.Candidate()
                                                                                                              {
                                                                                                                  FirstName = "User1",
                                                                                                                  LastName = "User2",
                                                                                                                  Email = "user@gmail.com",
                                                                                                                  IsEmployee = true
                                                                                                              },
                                                                                                              new Domain.Entities.Candidate()
                                                                                                              {
                                                                                                                  FirstName = "User1",
                                                                                                                  LastName = "User2",
                                                                                                                  Email = "user@gmail.com",
                                                                                                                  IsEmployee = true
                                                                                                              }
                                                                                                          }
            };
        } 
        #endregion
    }
}
