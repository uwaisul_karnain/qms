﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Adaptor;
using attune.Core.Adaptor.AutoMapper;
using attune.Core.Ioc.Unity.AOP.Validation;
using attune.Core.Repository.EF6;
using attune.Core.UnitOfWork;
using attune.Exam.Repository.EF;
using attune.Exam.Repository.EF.Model;
using attune.Exam.Repository.Interfaces;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Telerik.JustMock;
using Telerik.JustMock.Unity;

namespace attune.Exam.Test.Repository.EF.Mock
{
    public class ExamRepositoryMock
    {
        public static IQuestionBankRepository ArrangeMockRepository()
        {
            #region Prepare Unity container

            var container = new UnityContainer();
            container.AddNewExtension<Interception>();
            container.EnableMocking();

            #endregion

            #region Registering Types

            var injectionMembers = new List<InjectionMember>
                                   {
                                       new Interceptor<InterfaceInterceptor>(),
                                       new InterceptionBehavior<ValidationInterceptionBehavior>()
                                   };

            container.RegisterType<InterfaceInterceptor>();
            container.RegisterType<ValidationInterceptionBehavior>();

            container.RegisterType<IQuestionBankRepository, QuestionBankRepository>(injectionMembers.ToArray());
            container.RegisterType<IUnitOfWork<DbContext>, UnitOfWork>(new TransientLifetimeManager());
            container.RegisterType<IAdaptor, AutoMapperAdaptor>(new ContainerControlledLifetimeManager());

            #endregion

            #region Arrange Mocks

            var repository = container.Resolve<IQuestionBankRepository>();
            var uow = container.Resolve<IUnitOfWork<DbContext>>();


            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<Examination>(uow).FindAsQueryable()).Returns(MockDataHelper.GetFakeExamAsQueryable());
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<Examination>(uow).Add(Arg.IsAny<Examination>())).DoNothing();
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<Examination>(uow).Update(Arg.IsAny<Examination>())).DoNothing();
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<Examination>(uow).Find(MockDataHelper.GetFakeExamAsQueryable().Where(x => x.Id == 1))).Returns(MockDataHelper.GetFakeExamAsQueryable().FirstOrDefault());
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<Examination>(uow).ExecuteSqlCommand(Arg.IsAny<string>())).DoNothing();
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<ExamQuestionCategory>(uow).FindAsQueryable()).Returns(MockDataHelper.GetFakeExamQuestionCategoryAsQueryable);
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<ExamQuestionCategory>(uow).Add(Arg.IsAny<ExamQuestionCategory>())).DoNothing();
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<ExamQuestionCategory>(uow).Remove(Arg.IsAny<ExamQuestionCategory>())).DoNothing();
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<ExamQuestion>(uow).FindAsQueryable()).Returns(MockDataHelper.GetFakeExamQuestionAsQueryable());
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<ExamQuestion>(uow).Remove(Arg.IsAny<ExamQuestion>())).DoNothing();
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<ExamQuestion>(uow).Add(Arg.IsAny<ExamQuestion>())).DoNothing();
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<ExamQuestionAnswer>(uow).FindAsQueryable()).Returns(MockDataHelper.GetFakeExamQuestionAnswerAsQueryable());
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<ExamQuestionAnswer>(uow).Remove(Arg.IsAny<ExamQuestionAnswer>())).DoNothing();
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<ExamQuestionAnswer>(uow).Add(Arg.IsAny<ExamQuestionAnswer>())).DoNothing();
            Telerik.JustMock.Mock.Arrange(() => uow.Commit()).Returns(1);

            #endregion

            return repository;
        }
    }
}
