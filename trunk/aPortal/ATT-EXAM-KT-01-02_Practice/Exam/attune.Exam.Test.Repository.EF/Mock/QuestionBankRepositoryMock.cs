﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using attune.Core.Adaptor;
using attune.Core.Adaptor.AutoMapper;
using attune.Core.Ioc.Unity.AOP.Validation;
using attune.Core.Repository.EF6;
using attune.Core.UnitOfWork;
using attune.Exam.Repository.EF;
using attune.Exam.Repository.EF.Model;
using attune.Exam.Repository.Interfaces;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Telerik.JustMock;
using Telerik.JustMock.Helpers;
using Telerik.JustMock.Unity;

namespace attune.Exam.Test.Repository.EF.Mock
{
    /// <summary>
    /// This mock class will prepare the unity container, required dependancies and the required data set to handel question bank test scenarios
    /// </summary>
    public class QuestionBankRepositoryMock
    {
        public static IQuestionBankRepository ArrangeMockRepository()
        {
            #region Prepare Unity container

            var container = new UnityContainer();
            container.AddNewExtension<Interception>();
            container.EnableMocking();

            #endregion

            #region Registering Types

            var injectionMembers = new List<InjectionMember>
                                   {
                                       new Interceptor<InterfaceInterceptor>(),
                                       new InterceptionBehavior<ValidationInterceptionBehavior>()
                                   };

            container.RegisterType<InterfaceInterceptor>();
            container.RegisterType<ValidationInterceptionBehavior>();

            container.RegisterType<IQuestionBankRepository, QuestionBankRepository>(injectionMembers.ToArray());
            container.RegisterType<IUnitOfWork<DbContext>, UnitOfWork>(new TransientLifetimeManager());
            container.RegisterType<IAdaptor, AutoMapperAdaptor>(new ContainerControlledLifetimeManager());

            #endregion

            #region Arrange Mocks

            var repository = container.Resolve<IQuestionBankRepository>();
            var uow = container.Resolve<IUnitOfWork<DbContext>>();


            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<QuestionBank>(uow).FindAsQueryable()).Returns(MockDataHelper.GetFakeQuestionBankAsQueryable);
            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<QuestionBank>(uow).Add(Arg.IsAny<QuestionBank>())).DoNothing();

            Telerik.JustMock.Mock.Arrange(() => RepositoryProvider.GetRepository<QuestionBank>(uow).ExecuteSqlCommand(Arg.IsAny<string>())).DoNothing();

            Telerik.JustMock.Mock.Arrange(() => uow.Commit()).Returns(1);  

            #endregion

            return repository;
        }
    }
}
