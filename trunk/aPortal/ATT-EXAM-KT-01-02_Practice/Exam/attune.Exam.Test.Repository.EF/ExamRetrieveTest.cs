﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using attune.Exam.Domain.Queries;
using attune.Exam.Test.Repository.EF.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace attune.Exam.Test.Repository.EF
{
    /// <summary>
    /// This Test will perfome scenarios of data filteration of exam details.
    /// Exam can be filtered use of ExamType, and Exam Name.
    ///
    /// ASSUMPTIONS :
    ///     Master data is used in the data perpration is considered as valid and will not be tested in the logical path of the
    ///     test to validate for exsistance.
    /// SCENARIOS:
    ///     1. ExamType is required for all filterations, if not provided error message is required to prompt
    ///     2. Record set can be filtered from Exam Name is considered an optional.
    /// </summary>
    [TestClass]
    public class ExamRetrieveTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize()]
        public static void Init(TestContext testContext)
        {

        }

        /// <summary>
        /// To run code after all tests in a class have run
        /// </summary>
        [ClassCleanup()]
        public static void Cleanup()
        {

        }

        #endregion

        #region Positive Scenarios
        /// <summary>
        /// This performs to Filter By All parameters
        /// </summary>
        [TestMethod]
        public void PT_Exam_FilterBy_All()
        {
            var query = new ExamQuery { ExamTypeId = 1, ExamId = 1};
            try
            {
                var exams = ExamRepositoryMock.ArrangeMockRepository().FindExams(query);

                Assert.IsTrue(exams.Count > 0, Constants.DEFUALT_NO_RECORDS_FOUND);
                Assert.IsTrue(exams.Select(x => x.ExamType.Id).Contains(query.ExamTypeId),
                    string.Format("{0} Exam Type", Constants.DEFUALT_INVALID));
                Assert.IsTrue(exams.Select(x => x.Id).Contains(query.ExamId),
                    string.Format("{0} Exam Id", Constants.DEFUALT_INVALID));
                
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }

        /// <summary>
        /// This performs to Filter by Exam Type Only
        /// </summary>
        [TestMethod]
        public void PT_Exam_FilterBy_ExamType_Only()
        {
            var query = new ExamQuery { ExamTypeId = 1 };
            try
            {
                var exams = ExamRepositoryMock.ArrangeMockRepository().FindExams(query);

                Assert.IsTrue(exams.Count > 0, Constants.DEFUALT_NO_RECORDS_FOUND);
                Assert.IsTrue(exams.Select(x => x.ExamType.Id).Contains(query.ExamTypeId),
                    string.Format("{0} Exam Type", Constants.DEFUALT_INVALID));
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }
        #endregion

        #region Negative Scenarios
        /// <summary>
        /// This performs to validate Query can not null
        /// </summary>
        [TestMethod]
        public void NT_Exam_FilterBy_QueryCannotBeNull()
        {
            try
            {
                var exams = ExamRepositoryMock.ArrangeMockRepository().FindExams(null);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message, string.Format("query {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
                return;
            }
            Assert.Fail(Constants.DEFUALT_ERROR_ON_TEST_FAILED);
        }

        /// <summary>
        /// This performs to validate Exam Type is required
        /// </summary>
        [TestMethod]
        public void NT_Exam_ExamTypeId_Is_RequiredTo_Filter()
        {
            var query = new ExamQuery();
            try
            {
                var questions = ExamRepositoryMock.ArrangeMockRepository().FindExams(query);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message,
                    string.Format("ExamTypeId {0}", Constants.DEFUALT_ERROR_PATTERN_GREATER_THAN_ZERO));
                return;
            }
            Assert.Fail(Constants.DEFUALT_ERROR_ON_TEST_FAILED);
        }

        #endregion
    }
}
