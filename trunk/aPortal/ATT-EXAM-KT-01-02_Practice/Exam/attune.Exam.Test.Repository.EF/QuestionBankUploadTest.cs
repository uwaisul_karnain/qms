﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using attune.Exam.Domain.Queries;
using attune.Exam.Test.Repository.EF.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using attune.Exam.Domain.Entities;
using System.Threading.Tasks;

namespace attune.Exam.Test.Repository.EF
{
    /// <summary>
    /// This Test will perfome scenarios of bulk upload of question and answer details.   
    /// ASSUMPTIONS :
    ///     Master data is used in the data perpration is considered as valid and will not be tested in the logical path of the
    ///     test to validate for exsistance.
    /// SCENARIOS:
    ///     1. Passing list of question objects to be persist as a bulk operation    
    ///     2. Passing empty Collection to bulk operation   
    /// </summary>
    [TestClass]
    public class QuestionBankUploadTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize()]
        public static void Init(TestContext testContext)
        {

        }

        /// <summary>
        /// To run code after all tests in a class have run
        /// </summary>
        [ClassCleanup()]
        public static void Cleanup()
        {

        }

        #endregion

        #region Positive Scenarios

        /// <summary>
        /// This performs to upload bulk questions
        /// </summary>
        [TestMethod]
        public void PT_Question_BulkUpload()
        {
            var query = new List<QuestionBank>()
                        {
                            new QuestionBank()
                            {
                                Category = new Category() {Id = 1},
                                ExamType = new ExamType() {Id = 1},
                                DifficultLevel = Domain.DifficultLevel.Beginner,
                                Question = "Question 1",
                                IsActive = true,
                                IsPublished = true,
                                Answers = new List<Answer>()
                                          {
                                              new Answer() {Response = "Answer 1", IsCorrect = false},
                                              new Answer() {Response = "Answer 2", IsCorrect = false},
                                              new Answer() {Response = "Answer 3", IsCorrect = false},
                                              new Answer() {Response = "Answer 4", IsCorrect = true}
                                          }
                            },
                            new QuestionBank()
                            {
                                Category = new Category() {Id = 1},
                                ExamType = new ExamType() {Id = 1},
                                DifficultLevel = Domain.DifficultLevel.Beginner,
                                Question = "Question 2",
                                IsActive = true,
                                IsPublished = true,
                                Answers = new List<Answer>()
                                          {
                                              new Answer() {Response = "Answer 1", IsCorrect = true},
                                              new Answer() {Response = "Answer 2", IsCorrect = false},
                                              new Answer() {Response = "Answer 3", IsCorrect = false},
                                              new Answer() {Response = "Answer 4", IsCorrect = false}
                                          }
                            }
                        };
            try
            {

                var result = QuestionBankRepositoryMock.ArrangeMockRepository().AddBulkQuestions(query);
                Assert.IsTrue(result);
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }

        [TestMethod]
        public void PT_Question_BulkUpload_2()
        {
            var dt = new DataTable();
            dt.Columns.Add("Id");
            dt.Columns.Add("ExamTypeId");
            dt.Columns.Add("CategoryId");
            dt.Columns.Add("QuestionTypeId");
            dt.Columns.Add("DifficultLevelId");
            dt.Columns.Add("Question");
            dt.Columns.Add("IsPublished");
            dt.Columns.Add("Answer");
            dt.Columns.Add("Correct");

            dt.Rows.Add(1, 1, 1, 1, 1, "Q7", 1, "Ans1", false);
            dt.Rows.Add(2, 1, 2, 2, 1, "Q7", 1, "Ans1", false);
            dt.Rows.Add(3, 1, 1, 1, 1, "Q7", 1, "Ans12", false);
            dt.Rows.Add(4, 1, 1, 1, 1, "Q7", 1, "Ans1", false);
            try
            {

                var result = QuestionBankRepositoryMock.ArrangeMockRepository().BulkUploadQuestion(dt);
                Assert.IsTrue(result);
            }
            catch (AssertFailedException e)
            {
                Assert.Fail(e.Message);
            }
        }

        #endregion

        #region Negative Scenarios

        /// <summary>
        /// This performs to validate Collection cannot be Empty
        /// </summary>
        [TestMethod]
        public void NT_Question_BulkUpload_Collection_CannotBeEmpty()
        {
            List<QuestionBank> query = null;

            try
            {
                var result = QuestionBankRepositoryMock.ArrangeMockRepository().AddBulkQuestions(query);
                Assert.IsTrue(result);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message, string.Format("query {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
                return;
            }
            Assert.Fail(Constants.DEFUALT_ERROR_ON_TEST_FAILED);
        }

        #endregion
    }
}
