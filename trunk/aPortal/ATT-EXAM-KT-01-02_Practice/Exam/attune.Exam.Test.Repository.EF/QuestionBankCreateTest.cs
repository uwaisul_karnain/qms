﻿/****************************************************************************************
 * Copyright © attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using attune.Exam.Domain.Entities;
using attune.Exam.Domain.Queries;
using attune.Exam.Test.Repository.EF.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace attune.Exam.Test.Repository.EF
{
    /// <summary>
    /// This Test will perfome scenarios of add question and answer details.
    ///
    /// ASSUMPTIONS :
    ///     Master data is used in the data perpration is considered as valid and will not be tested in the logical path of the
    ///     test to validate for exsistance.
    /// SCENARIOS:
    ///     1. ExamType is required for adding questions, if not provided error message is required to prompt
    ///     2. Category is required for adding questions, if not provided error message is required to prompt
    /// </summary>
    [TestClass]
    public class QuestionBankCreateTest
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        /// <summary>
        /// Use TestInitialize to run code before running each test
        /// </summary>
        /// <param name="testContext"></param>
        [ClassInitialize()]
        public static void Init(TestContext testContext)
        {

        }

        /// <summary>
        /// To run code after all tests in a class have run
        /// </summary>
        [ClassCleanup()]
        public static void Cleanup()
        {

        }

         #endregion

        #region Positive Scenarios

        /// <summary>
        /// This performs to add with all required fields
        /// </summary>
        [TestMethod]
        public void PT_Question_Add_With_RequiredFields()
        {
            var question = GetQuestionBankTestData();
            try
            {
                var result = QuestionBankRepositoryMock.ArrangeMockRepository().AddQuestion(question);
                Assert.IsTrue(result);
            }
            catch (AssertFailedException e)
            {
                 Assert.Fail(e.Message);
            }
        }

        #endregion

        #region Negative Scenarios

        /// <summary>
        /// This performs to validate Object can not null
        /// </summary>
        [TestMethod]
        public void NT_Question_Add_Object_CannotBeNull()
        {
            try
            {
                var questions = QuestionBankRepositoryMock.ArrangeMockRepository().AddQuestion(null);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message, string.Format("entity {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
                return;
            }
            Assert.Fail(Constants.DEFUALT_ERROR_ON_TEST_FAILED);
        }

        /// <summary>
        /// This performs to validate Exam Type is required
        /// </summary>
        [TestMethod]
        public void NT_ExamType_Is_RequiredTo_Add()
        {
            var question = GetQuestionBankTestData();
            question.ExamType = null;
            try
            {
                var questions = QuestionBankRepositoryMock.ArrangeMockRepository().AddQuestion(question);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message,
                    string.Format("ExamType {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
            }
        }

        /// <summary>
        /// This performs to validate Category is required
        /// </summary>
        [TestMethod]
        public void NT_Category_Is_RequiredTo_Add()
        {
            var question = GetQuestionBankTestData();
            question.Category = null;
            try
            {
                var questions = QuestionBankRepositoryMock.ArrangeMockRepository().AddQuestion(question);
            }
            catch (ArgumentNullException ex)
            {
                StringAssert.Contains(ex.Message,
                    string.Format("Category {0}", Constants.DEFUALT_ERROR_PATTERN_NULL));
            }
        }

        #endregion

        private static QuestionBank GetQuestionBankTestData()
        {
            var question = new QuestionBank
            {
                Category = new Category() { Id = 1 },
                ExamType = new ExamType() { Id = 1 },
                DifficultLevel = Domain.DifficultLevel.Beginner,
                QuestionType = Domain.QuestionType.MultipleDropDown,
                Question = "Question 1",
                IsActive = true,
                IsPublished = true,
                CreatedBy = 1,
                CreatedOn = DateTime.Now,
                Answers = new List<Answer>()
                                          {
                                              new Answer() {Response = "Answer 1", IsCorrect = false},
                                              new Answer() {Response = "Answer 2", IsCorrect = false},
                                              new Answer() {Response = "Answer 3", IsCorrect = false},
                                              new Answer() {Response = "Answer 4", IsCorrect = true}
                                          }

            };
            return question;
        }

    }
}
