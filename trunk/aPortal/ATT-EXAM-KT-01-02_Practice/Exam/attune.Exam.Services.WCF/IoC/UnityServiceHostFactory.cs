/****************************************************************************************
 * Copyright � attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace attune.Exam.Services.WCF.Ioc
{
    public class UnityServiceHostFactory : ServiceHostFactory
    {
        /// <summary>
        ///     Creates a <see cref="ServiceHost" /> for a specified type of service with a specific base address.
        /// </summary>
        /// <returns>
        ///     A <see cref="UnityServiceHost" /> for the type of service specified with a specific base address.
        /// </returns>
        /// <param name="serviceType">
        ///     Specifies the type of service to host.
        /// </param>
        /// <param name="baseAddresses">
        ///     The <see cref="T:System.Array" /> of type <see cref="T:System.Uri" /> that contains the base addresses for the
        ///     service hosted.
        /// </param>
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            /*
            var container = UnityConfigProvider.GetConfiguredContainer(); // TODO : Configure the projects UI contatiner by reference the attune.<Project>.Infrastructure.DI 

            UnityDependencyFactory.Set(container);

            return new UnityServiceHost(serviceType, baseAddresses);
             */
            return null;
        }
    }
}