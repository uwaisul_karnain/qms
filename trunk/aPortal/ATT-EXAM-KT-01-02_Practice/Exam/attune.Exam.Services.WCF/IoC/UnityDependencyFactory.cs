/****************************************************************************************
 * Copyright � attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using Microsoft.Practices.Unity;

namespace attune.Exam.Services.WCF.Ioc
{
    public class UnityDependencyFactory
    {
        #region Constructor

        static UnityDependencyFactory()
        {
            Container = new UnityContainer();
        }

        #endregion

        #region Private Variables

        #endregion

        #region Public Methods

        /// <summary>
        ///     Gets the container.
        /// </summary>
        /// <value>
        ///     The container.
        /// </value>
        public static IUnityContainer Container { get; private set; }

        /// <summary>
        ///     Sets the specified container.
        /// </summary>
        /// <param name="container">The container.</param>
        public static void Set(IUnityContainer container)
        {
            Container = container;
        }

        /// <summary>
        ///     Resolves the specified service type.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <returns></returns>
        public static object Resolve(Type serviceType)
        {
            return Container.Resolve(serviceType);
        }

        /// <summary>
        ///     Resolves this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T Resolve<T>()
        {
            var ret = default(T);

            if (Container.IsRegistered(typeof(T)))
            {
                try
                {
                    ret = Container.Resolve<T>();
                }
                catch (Exception)
                {
                }
            }

            return ret;
        }

        #endregion
    }
}