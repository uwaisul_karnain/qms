/****************************************************************************************
 * Copyright � attune Lanka (Private) Limited. All rights reserved.                     *
 * No part of this publication may be reproduced, transmitted, transcribed,             *
 * stored in a retrieval system, or translated into any language,                       *
 * in any form or by any means, electronic, mechanical, photocopying,                   *
 * recording, or otherwise, without prior written permission from attune.               *
 *                                                                                      *
 * All copyright, confidential information, patents, design rights and                  *
 * all other intellectual property rights of whatsoever nature in and                   *
 * to any source code contained herein (including any header files and                  *
 * demonstration code that may be included), are and shall remain the sole and          *
 * exclusive property of attune.                                                        *
 *                                                                                      *
 * The information furnished herein is believed to be accurate and reliable.            *
 * However, no responsibility is assumed by attune for its use, or for any              *
 * infringements of patents or other rights of third parties resulting from its use.    *
 *                                                                                      *
 ***************************************************************************************/

using System;
using System.ServiceModel;

namespace attune.Exam.Services.WCF.Ioc
{
    /// <summary>
    ///     This service host is used to set up the service behavior that replaces the instance provider to use dependency
    ///     injection.
    /// </summary>
    public class UnityServiceHost : ServiceHost
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="UnityServiceHost" /> class.
        /// </summary>
        /// <param name="serviceType">Type of the service.</param>
        /// <param name="baseAddresses">The base addresses.</param>
        public UnityServiceHost(Type serviceType, Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
        }

        /// <summary>
        ///     Opens the channel dispatchers.
        /// </summary>
        /// <param name="timeout">
        ///     The <see cref="T:System.Timespan" /> that specifies how long the on-open operation has to
        ///     complete before timing out.
        /// </param>
        protected override void OnOpen(TimeSpan timeout)
        {
            Description.Behaviors.Add(new UnityServiceBehavior());
            base.OnOpen(timeout);
        }
    }
}