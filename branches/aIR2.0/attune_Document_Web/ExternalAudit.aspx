﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ExternalAudit.aspx.cs" Inherits="attune_Document_Web.ExternalAudit" %>

<asp:Content ID="externalAuditPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>External Audit Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The objective of external reviews / Audits is to get the third eye view and validate attune processes and procedures follow the accurate industrial standards.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>As of now, only attune Lanka office located in Colombo is being validated for the standard certification. However same best practices and process are implementing across the company.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>ISO  - </strong>International Standards Organization</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>CMMI - </strong>Capability Maturity Model Integration</p>                                                        
                          </div>
                        </div>

                        <div class="data">
                            <h3>Our standards   </h3>

                            <h4>ISO 9001-2008</h4>    

                            <p>attune is ISO  Certified organization and periodical audits are been conducted to maintain the internal standards align to industrial standard level.</p>

                            <p class="tab-content" style="padding-top: 20px"><strong>Surveillance Audit  - </strong> Take place yearly</p>
                            <p><strong>Re-Certification Audit  -  </strong> Take place in every 3 years.</p>

                            <h4>CMMI</h4>    
                            <p>attune name listed in Software Engineering Institute (SEI) of Carnegie Mellon University  US (<a href="https://sas.cmmiinstitute.com"> https://sas.cmmiinstitute.com </a>) in July 2015 as CMMI level 3 company.</p>                         
                            <p class="tab-content" style="padding-top: 20px">Next assessment take place in 3 years.</p>
                            <p></p>
                        </div>

<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>

