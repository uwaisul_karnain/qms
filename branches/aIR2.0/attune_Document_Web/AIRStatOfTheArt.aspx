﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AIRStatOfTheArt.aspx.cs" Inherits="attune_Document_Web.AIRStatOfTheArt" %>


<asp:Content ID="aIRDiagramPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

        <div class="rightSectiontop">
            <h2>FRS/Agile attune Implementation Roadmap</h2>
            <p>The Advanced aIR Implementation Roadmap is attune’s global methodology implementing Fashion specific SAP solutions by creating a high-level design built into a Future Reference System based on proven industry practices and the attune Fashion Suite. Thereafter, the Roadmap follows our own hybrid agile approach for building, testing, and implementing the solution. 
</p> 
        </div> 
<%--    <img src="Images/aIRDiagram.jpg">

    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
    <br><br>
    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

    <br><br>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
    <br><br>
    </p>--%>


          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <div class="disc">
                            <div class="diagram">
                                <div class="state-of-art-wrap">
                                    <img src="Images/air-image.png" />


                                    <%--<span class="col-movuse-over over-1"></span>
                                    <span class="col-movuse-over over-2"></span>
                                    <span class="col-movuse-over over-3"></span>
                                    <span class="col-movuse-over over-4"></span>
                                    <span class="col-movuse-over over-5"></span>
                                    <span class="col-movuse-over over-6"></span>--%>


                                </div>
                            </div>
                        </div>
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->

</asp:Content>

