﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="attune_Document_Web._Default" %>

<asp:Content ID="HLDPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">
    
    <div class="rightSectiontop">
        <h2>aIR Methodology</h2>
        <p> The vision of attune's aIR methodology is to institutionalize within attune a consistent, global approach to customer projects that is understood by every employee; which results in optimal services delivery efficiency and engagements that always meet or exceed customer expectations.  </p>
        <p> Within this hub, you will find step-by-step guidelines for project implementations and a comprehensive database of supporting templates and process guidelines. </p>
 
    </div> 
    <div class="rightBackground">
        <div class="section group">
            <div class="col-box-1 col span_1_of_2">
                <div class="pic pic1" onclick="">
                    <img src="Images/img1.png" class="pic-image" alt="Pic"/>
                    
                    <span class="pic-caption">
                        <%--<h1 class="pic-title"><a href="AIRMethodDiagram.aspx">Implementation Methodology</a></h1>--%>
                        <h1 class="pic-title"><a href="AIRMethodDiagram.aspx" style="color:#fff; ">Implementation Methodology</a></h1>
                        <p class="colbox-cont" style="padding-top: 30px"><a href="ProjectPreparation.aspx">Project Preparation</a></p>
                        <p class="colbox-cont"><a href="BusinessBluePrinting.aspx">Business Blueprinting</a></p>
                        <p class="colbox-cont"><a href="RealizationBuild.aspx">Realization - Build</a></p>
                        <p class="colbox-cont"><a href="RealizationTesting.aspx">Realization - Testing</a></p>
                        <p class="colbox-cont"><a href="FinalPreparationAndCutover.aspx">Final Preparation and Cutover</a></p>
                        <p class="colbox-cont"><a href="PostGoLive.aspx">Post Go-Live Support</a></p>
                    </span>
                </div>
            </div>
                
            <div class="col-box-2 col span_1_of_2">
                <div class="pic pic2" onclick="">
                    <img src="Images/img3.png" class="pic-image" alt="Pic"/>
                    <span class="pic-caption">
                        <h1 class="pic-title">Governing Principles</h1>
                        <p class="colbox-cont" style="padding-top: 30px"><a href="ProcessTailoring.aspx">Process Tailoring</a></p>
                        <%--<p class="colbox-cont"><a href="RiskManagement.aspx">Risk Management</a></p>--%>
                        <p class="colbox-cont"><a href="ProjectPlanning.aspx">Project Planning</a></p>
                        <p class="colbox-cont"><a href="ProjectMonitoringAndControl.aspx">Project Monitoring & Control</a></p>
                        <p class="colbox-cont"><a href="MeasurementandAnalysis.aspx">Measurements and Analysis</a></p>
                        <%--<p class="colbox-cont"><a href="DecisionAnalysisResolution.aspx">Decision Analysis Resolution</a></p>--%>
                        <%--<p class="colbox-cont"><a href="ConfigurationManagement.aspx">Configuration Management</a></p>--%>
                        <p class="colbox-cont"><a href="ProjectClosure.aspx">Project Closure</a></p>
                        <%--<p class="colbox-cont"><a href="TrainingProcess.aspx">Training Process</a></p>--%>
                        <%--<p class="colbox-cont"><a href="Policy.aspx">Organization Policy</a></p>--%>
                        <%--<p class="colbox-cont"><a href="ProcessManagement.aspx">Process Management</a></p>--%>
                        <p class="colbox-cont"><a href="ProcessImprovement.aspx">Process Improvements</a></p>
                        <%--<p class="colbox-cont"><a href="InternalAudit.aspx">Internal Auditing</a></p>--%>
                        <%--<p class="colbox-cont"><a href="ExternalAudit.aspx">External Auditing</a></p>--%>
                        <%--<p class="colbox-cont"><a href="SEPGPlan.aspx">SEPG</a></p>--%>
                        <p class="colbox-cont"><a href="ConfigurationManagementProcess.aspx">Configuration Management Process</a></p>
                        <p class="colbox-cont"><a href="DecisionAnalysisAndResolutionProcess.aspx">Decision Analysis and Resolution  Process</a></p>
                    </span>
                </div>
            </div>
                
            <div style="display:none" class="col-box-3 col span_1_of_2">
                <div class="pic pic3" onclick="">
                    <img src="Images/img2.png" class="pic-image" alt="Pic"/>
                    <span class="pic-caption">
                        <%--<h1 class="pic-title"><a href="AgileMethodDiagram.aspx">Development Methodology</a></h1>--%>
                        <h1 class="pic-title"><a href="AgileMethodDiagram.aspx" style="color:#fff; ">Development Methodology</a></h1>
                        <p class="colbox-cont" style="padding-top: 30px"><a href="RequirementAnalysis.aspx">Requirement Analysis</a></p>
                        <p class="colbox-cont"><a href="RequirementManagement.aspx">Requirement Development</a></p>
                        <p class="colbox-cont"><a href="ArchitectureDesign.aspx">Architecture & Design</a></p>
                        <p class="colbox-cont"><a href="CodingUnitTestingIntegration.aspx">Coding, Unit Testing and Integration</a></p>
                        <p class="colbox-cont"><a href="QualityAssuranceAndControl.aspx">Quality Assurance and Control</a></p>
                        <p class="colbox-cont"><a href="WorkProductReview.aspx">Work Product Review</a></p>
                        <p class="colbox-cont"><a href="ManagementReview.aspx">Management Review Process</a></p>
                    </span>
                </div>
            </div>
            <%--added agile from air3.0--%>
            <!--<div class="col-box-3 col span_1_of_2">
                <div class="pic pic3" onclick="">
                    <img src="Images/agile-image.png" class="pic-image" alt="Pic"/>
                    <span class="pic-caption">
                        <%--<h1 class="pic-title"><a href="AgileMethodDiagram.aspx">Development Methodology</a></h1>--%>
                        <h1 class="pic-title"><a href="<%--AgileMethodDiagram.aspx--%> AIRStatOfTheArt.aspx" style="color:#fff; ">FRS/Agile attune Implementation Roadmap</a></h1>
                        <p class="colbox-cont" style="padding-top: 30px"><a href="AgileProjectPreparation.aspx">Project Preparation</a></p>
                        <p class="colbox-cont"><a href="AgileBuildingFutureReferenceSolution.aspx">Building the Future Reference Solution</a></p>
                        <p class="colbox-cont"><a href="AgileBuildPhase.aspx">Agile Build and Unit Test</a></p>
                        <p class="colbox-cont"><a href="AgileTesting.aspx">Testing</a></p>
                        <p class="colbox-cont"><a href="AgileFinalPreparationAndCutover.aspx">Final Preparation and Cutover</a></p>
                        <p class="colbox-cont"><a href="AgilePostGoLiveSupport.aspx">Post Go-Live Support</a></p>
                    </span>
                </div>
            </div>-->
                
            <div class="col-box-4 col span_1_of_2">
<%--                <div class="pic pic-3d">
                    <img src="Images/img4.png" class="pic-image" alt="Pic"/>
                    <span class="pic-caption open-right">
                        <h1 class="pic-title">Solutioning Consulting Methodology</h1>
                        <p class="colbox-cont"><a href="#">Project Preparation</a></p>
                        <p class="colbox-cont"><a href="#">Business Blueprinting</a></p>
                        <p class="colbox-cont"><a href="#">Realization - Build</a></p>
                        <p class="colbox-cont"><a href="#">Realization - Testing</a></p>
                        <p class="colbox-cont"><a href="#">Final Preparation and Cutover</a></p>
                        <p class="colbox-cont"><a href="#">Post Go-Live Support</a></p>

                    </span>
                </div>--%>
            </div>

        </div>
    </div>

</asp:Content>
