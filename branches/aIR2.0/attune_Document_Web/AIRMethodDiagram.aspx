﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AIRMethodDiagram.aspx.cs" Inherits="attune_Document_Web.AIRMethodDiagram" %>


<asp:Content ID="aIRDiagramPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

        <div class="rightSectiontop">
            <h2>attune Implementation Roadmap</h2>
            <p>aIR is our global methodology for Designing, Building, Implementing and supporting SAP solutions. aIR has been tailor made to support the implementation of fashion specific SAP solutions. aIR is based on proven frameworks of PMI, ASAP and ASAP Focus methodologies. 
</p> 
        </div> 
<%--    <img src="Images/aIRDiagram.jpg">

    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
    <br><br>
    It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

    <br><br>
    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. 
    <br><br>
    </p>--%>


          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <div class="disc">
                            <div class="diagram">
                                <div class="litetooltip-hotspot-wrapper" style="max-width: 1020px">
                                    <div class="litetooltip-hotspot-container" style="padding-bottom: 75%">

                                        <%--<img src="Images/airDiagram.jpg" alt="" usemap="#Map" />--%>
                                        
                                        <%--<img src="Images/airDiagram.jpg" alt="" usemap="#Map" />
                                 <map name="Map" id="Map">
                                <area alt="" title="" href="ProjectPreparation.aspx" shape="poly" coords="57,56,23,92,176,95,213,58,177,23,24,20" />
    <area alt="" title="" href="BusinessBluePrinting.aspx" shape="poly" coords="231,23,228,93,319,92,313,25" />
    <area alt="" title="" href="RealizationBuild.aspx" shape="poly" coords="374,20,374,95,493,95,491,21" />
    <area alt="" title="" href="RealizationTesting.aspx" shape="poly" coords="530,21,535,93,647,93,644,24" />
    <area alt="" title="" href="FinalPreparationAndCutover.aspx" shape="poly" coords="693,20,695,95,797,95,791,23" />
    <area alt="" title="" href="PostGoLive.aspx" shape="poly" coords="841,21,843,94,951,94,951,25" />
    <area alt="" title="" href="ProjectPreparation.aspx" shape="poly" coords="92,138,89,157,136,187,245,159,210,114,101,122" />
    <area alt="" title="" href="BusinessBluePrinting.aspx" shape="poly" coords="275,135,236,196,371,212,399,168,349,126" />
    <area alt="" title="" href="RealizationBuild.aspx" shape="poly" coords="427,130,446,188,580,177,581,132,526,109,467,112" />
    <area alt="" title="" href="RealizationTesting.aspx" shape="poly" coords="606,158,610,210,676,203,754,193,738,146,691,125,631,131" />
    <area alt="" title="" href="FinalPreparationAndCutover.aspx" shape="poly" coords="756,133,776,187,863,172,902,148,876,116,776,109" />
    <area alt="" title="" href="PostGoLive.aspx" shape="poly" coords="863,202,875,253,980,258,1005,223,980,181" />
                            </map>--%>

                                       <div class="diagram-mapper">
                    <div class="sixteen columns">
                        <div id="applicationStatus">
                            <ul class="applicationStatus">
                                <li class="colour-1">
                                  <a href="ProjectPreparation.aspx">
                                    <i class=""></i>
                                    <span>Project <br />Preparation</span>
                                    <span class="shdow"></span>
                                  </a>
                                </li>
                                <li class="colour-2">
                                  <a href="BusinessBluePrinting.aspx">
                                    <i class=""></i>
                                    <span>Business <br />BluePrint</span>
                                    <span class="shdow"></span>
                                  </a>
                                </li>
                                <li class="colour-3">
                                  <a href="RealizationBuild.aspx">
                                    <i class=""></i>
                                    <span>Realization-<br />Build</span>
                                    <span class="shdow"></span>
                                  </a>
                                </li>
                                <li class="colour-4">
                                  <a href="RealizationTesting.aspx">
                                    <i class=""></i>
                                    <span>Realization-<br />Test</span>
                                    <span class="shdow"></span>
                                  </a>
                                </li>
                                <li class="colour-5">
                                  <a href="FinalPreparationAndCutover.aspx">
                                    <i class=""></i>
                                    <span>Final Prep & <br />cutover</span>
                                    <span class="shdow"></span>
                                  </a>
                                </li>
                                <li class="colour-6">
                                  <a href="PostGoLive.aspx">
                                    <i class=""></i>
                                    <span>Post go Live <br />Support</span>
                                    <span class="shdow"></span>
                                  </a>
                                </li>
                                <li class="colour-7">
                                  &nbsp;
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="map-links">
                        <div class="link-1">
                            <a href="ProjectPreparation.aspx">
                                <img src="Images/link-1.png" class="img-responsive" alt="Image">
                            </a>
                        </div>
                        <div class="link-2">
                            <a href="BusinessBluePrinting.aspx">
                                <img src="Images/link-2.png" class="img-responsive" alt="Image">
                            </a>
                        </div>
                        <div class="link-3">
                            <a href="RealizationBuild.aspx">
                                <img src="Images/link-3.png" class="img-responsive" alt="Image">
                            </a>
                        </div>
                        <div class="link-4">
                            <a href="RealizationTesting.aspx">
                                <img src="Images/link-4.png" class="img-responsive" alt="Image">
                            </a>
                        </div>
                        <div class="link-5">
                            <a href="FinalPreparationAndCutover.aspx">
                                <img src="Images/link-5.png" class="img-responsive" alt="Image">
                            </a>
                        </div>
                        <div class="link-6">
                            <a href="PostGoLive.aspx">
                                <img src="Images/link-6.png" class="img-responsive" alt="Image">
                            </a>
                        </div>
                    </div>
                </div>



                                    </div>
                                </div>
                            </div>
                        </div>
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->

</asp:Content>

