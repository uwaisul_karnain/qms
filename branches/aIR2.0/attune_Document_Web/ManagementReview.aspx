﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ManagementReview.aspx.cs" Inherits="attune_Document_Web.ManagementReview" %>

<asp:Content ID="managementReviewPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Management  Review  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The Main objective of the Management Review process is that the senior management obtains visibility into the processes being followed within the current ongoing projects of the organization.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>For visibility into projects, senior management performs reviews for each project in weekly basis scheduling an “Operations Meeting” with all the PMs. Here, senior management reviews the processes in the project (in terms of the activities, status and results), identifies issues and resolves them.</p>
                            <p>For visibility into the overall operations of the organization, senior management performs reviews for the operations of the various groups and resolves the issues identified.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>VPS - </strong>Vice President Services</p>
                                <p><strong>GM  - </strong>Group Manager</p>
                                <p><strong>PC  - </strong>Process Consultant</p>                                
                          </div>
                          <div class="col-xs-6 col-md-4">                                 
                                <p><strong>PM  - </strong>Project Manager</p>  
                                <p><strong>PL  - </strong>Project Lead</p>                           
                                <p><strong>TL - </strong>Technical Lead</p>                       
                          </div>
                          <div class="col-xs-6 col-md-4">                                 
                                <p><strong>PSTR  - </strong>Project Status Tracking Report</p>
                                <p><strong>PSTS  - </strong>Project Status Tracking Sheet</p>
                                <p><strong>aPTR  - </strong>attune Process Tailoring Repository</p>
                          </div>
                        </div>

                        <div class="data">
                            <h3>Process and Steps</h3>     

                            <p>Senior management, other than the managers directly involved in activities, needs to periodically review the activities, status and results of processes in order to identify issues and resolve them. </p>
                            <p>The management review activities for each project are handled through weekly held Operations Meeting. Weekly project status tracking report is prepared by the PL for each project. The VPS and the VP Services are invitees to this meeting. The operations meeting is coordinated by the PC. In this meeting, the progress of the project and processes used in the project are reviewed. These processes include requirements management, software engineering, work-product reviews, project management, risk management, configuration management, decision analysis and resolution, quality assurance, measurement & analysis. Issues and corrective actions to resolve them are identified and these are tracked to closure. </p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td rowspan="4"><strong>Prepare</strong></td>
                                            <td><p>Prepare and send the Project Status Tracking Report to PC and VPS one day prior to the operations meeting.</p>  
                                                <p>(Template : Project Status Tracking Report)</p></td>
                                            <td>PM</td>
                                            <td>PSTR</td>                                    
                                            <td>aPTR, PSTS</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Follow up with project members and the new PSTR for status of action points frVPS the previous Status Tracking Report and record the status of the action points</p></td>
                                            <td rowspan="3">PC, VPS</td>
                                            <td>Action Item Status</td>
                                            <td rowspan="9">Previous PSTR AIs, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare the agenda for the operations meeting. </p>
                                                <p>The agenda includes;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Presentations by the PM/PL or other persons required to provide the information to be reviewed.</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Submission and discussion of statuses, metrics, risks, client complaints and Project Status Summary in the Status Tracking reports</p>
                                                <p class="tab-content" style="padding-left: 20px">•	List down  the Action Items escalated by the senior management review team for each project </p></td>
                                            <%--<td></td>--%>
                                            <td>Agenda</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Schedule the date, time and venue of the next operations meeting  and circulates the agenda to all senior management and the PMs/PLs.</p></td>
                                            <%--<td></td>--%>
                                            <td>Meeting Request</td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="4"><strong>Execute</strong></td>
                                            <td><p>The operations meeting is convened as per schedule.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The VPS is the chairperson of the operation meeting.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The PC is responsible for recording the minutes of the meeting.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Each PM/PL is responsible for recording actions items for their respective projects</p></td>
                                            <td rowspan="3">VPS, PC, PM</td>
                                            <td rowspan="4">PSTR</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>List of action item status frVPS the previous operations meeting. Each action item is discussed and marked as one of the following:/p>
                                                <p class="tab-content" style="padding-left: 20px">•	Implemented and closed/p>
                                                <p class="tab-content" style="padding-left: 20px">•	Carried forward/p>
                                                <p class="tab-content" style="padding-left: 20px">•	Irrelevant and closed</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>All carried forward action points are assigned new dates.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Present information required for insight into the processes being used for the project and the problems that need to be resolved. </p>
                                                <p>SVPSe of the focus areas of the presentations include:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Performance of the project with respect to the planned schedule, Effort</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Risks and Issues</p>
                                                <p class="tab-content" style="padding-left: 20px">•	People Issues</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Variances in Measurement</p></td>
                                            <td>PM/PL</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="2"><strong>Closure</strong></td>
                                            <td><p>Point out the list of carried forward and new action points, along with the responsibilities and dates, for agreement frVPS the attendees of the operations meeting. </p></td>
                                            <td rowspan="2">PC</td>
                                            <td rowspan="2">List of AIs for Each Project</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Circulate the Action Item list for each project to all participants of the operations meeting.  </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                    </tbody>


                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>

                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Once a week for each project </p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Next Project Status Tracking Update meeting due date</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Project Status Tracking Report</p>                                            
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Collection of Project Status Reports with action items frVPS all projects </p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort for Review per project</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p></p>
                                        </div>
                                    </td>
                                    </tr>         
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Management Review Process.pdf">Download Management Review Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>                                                                                          
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\Weekly Project Status Report - Template Sample and Instructions.xlsm">1. Project Status Report</a></p>                                          
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>       
                                    <p><a href="References\MOD.xlsx">1. Metric Objective Description (attune MOD)</a></p>  
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>    

<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>





