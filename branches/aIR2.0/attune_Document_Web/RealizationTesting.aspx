﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="RealizationTesting.aspx.cs" Inherits="attune_Document_Web.IntegrationTestingAndUAT" %>

<asp:Content ID="realizationTestingPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Realization – Testing  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Testing phase of Realization of a SAP implementation project lifecycle.  It covers the activities that need to be performed within this phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The expected deliverables</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>

                            <p>The activities related to the Realization - Testing phase that are defined here derive the process followed during this phase of the project.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>BBP - </strong>Business Blueprint Document</p>
                                <p><strong>BODS - </strong>Business Objects Data Services</p>
                                <p><strong>BP - </strong>Business Process</p>
                                <p><strong>BPE - </strong>Business Process Experts</p>
                                <p><strong>BPO - </strong>Business Process Owner</p>
                                <p><strong>BTS - </strong>Business Transformation Services</p>
                                <p><strong>CR - </strong>Change Requests</p>
                                <p><strong>DM - </strong>Data Migration</p>
                                <p><strong>EUS - </strong>End-User Showcase</p>
                                <p><strong>ETL - </strong>Extract Transform Load</p>
                                <p><strong>FC - </strong>Functional Consultant</p>
                                <p><strong>FICO - </strong>Finance and Controlling</p>
                                <p><strong>FL - </strong>Functional Lead</p>
                                <p><strong>HPALM - </strong>Hewlett Packard Application Lifecycle Management</p>                                
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>FS - </strong>Functional Specification</p>
                                <p><strong>HPQC - </strong>Hewlett Packard Quality Center</p>
                                <p><strong>IST - </strong>Integration Scenario Test</p>
                                <p><strong>MM - </strong>Materials Management</p>
                                <p><strong>PC - </strong>Project Charter</p>
                                <p><strong>PGM - </strong>Program Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PMO - </strong>Project Management Office</p>
                                <p><strong>PP - </strong>Planning and Production</p>
                                <p><strong>PPL - </strong>Project Plan</p>                             
                                <p><strong>RM - </strong>Resource Manager</p>
                                <p><strong>SA - </strong>Solution Architect</p>
                                <p><strong>SC - </strong>Steering Committee</p>
                                <p><strong>RICEF - </strong>Reports, Interfaces, Custom Enhancements and Forms</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SD - </strong>Sales and Distribution</p>
                                <p><strong>SME - </strong>Subject Matter Expert</p>
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>TM - </strong>Training Manager</p>
                                <p><strong>TR - </strong>Transport Request</p>
                                <p><strong>TS - </strong>Technical Specification</p>
                                <p><strong>TSC - </strong>Test Coordinator</p>
                                <p><strong>VPS - </strong>Vice President Services</p>
                                <p><strong>WBS - </strong>Work Breakdown Structure</p>
                                <p><strong>WP - </strong>Work Package</p>
                          </div>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h3>Process and Steps</h3>     
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived using the SAP ASAP Focus methodology. Within the aIR methodology, the Realization of the solution follows the Business Blueprint phase. The Realization is broken further into two, namely Build and Testing. The Build part of the Realization covers the activities performed to configure and develop the solution designed in the Business Blueprint Document. The process for that Phase is covered in the Realization - Build Phase Process document.  The Testing part of Realization covers the activities performed to thoroughly test the solution developed during the Realization - Build Phase.  This document covers those testing activities as well as the activities carried out in parallel to get ready for the Final Preparation and Cutover.</p>
                            <p>Within the Realization - Test phase, the activities start with the testing kick off where all the relevant stakeholders from the customer, the customer’s core project team, the attune project team and the confirmed testing team come together and are made aware of the testing that is required to be completed. The Testers’ training timeline, the Testers’ training schedule, the testing timelines, the testing schedule, and the test scripts are shared with everyone. </p>
                            <p>The training of the integration testing team that is assigned to IST 1 commences thereafter, and everyone in the testing team are taken through the system navigation, the business process scenarios to be covered, the transactions to be used, and all of the steps required to successfully complete the testing.  Meanwhile, the consultants perform end-to-end process testing in the IST 1 quality environment before the system is turned over to the business users to begin testing.  </p>
                            <p>The Integration testing mainly involves the customer’s core project team with extended team members and the attune project team supported them. The Integration testing takes place in the quality environment and all of the end to end business scenarios maintained in the test scripts are executed within the system. During Integration testing, all transactions including the interfaces to third party tools are covered. Test results are updated into the Test Scripts.   A log is maintained to track the defects encountered, and the fixes done to resolve the defects.   Completed test scripts with the actual results are archived for later review by Internal Audit. </p> 
                            <p>There are two cycles of integration testing.  During each cycle, all steps of all test scripts are executed.  The target is to pass each step during each test cycle.  Defects are resolved and fixed as quickly as possible.  Successful retesting of the defect is required before the step in the test script can be passed.    Two cycles of integration testing are done in order to give the customer confidence that all defects have been found and fixed before going live.  </p>
                            <p>During the first cycle, the QA client that was prepared during the Mock 1 cutover is used.  This client is connected to the external systems’ test environments.  The Mock 1 data that was migrated to the quality box during the Build Phase is used.  Security access is generally unrestricted so that users can easily do their testing without being stopped.  Also, job schedules are not turned on.  Transactions that will be run automatically once in production, are run manually to keep testing progressing without delays.  After all test scripts have been run by the testers, an additional few days will be scheduled at the end of the cycle to fix and retest any remaining defects.</p>
                            <p>At the end of IST 1, a stage-gate review is done with the PMO and the Steering Committee.  The status of the exit criteria for the cycle is formally reviewed and approval to move into the next cycle is requested.  In case, any conditions are mentioned for the approval, these are captured and documented within the Project Status Tracking Sheet for future references and actions. </p>
                            <p>The second cycle of integration testing is carried out by key users of the future system.  It is the cycle whereby the customer decides to accept the system.  The testing team for IST 2 is expanded to include key users and business process experts.  New users are trained prior to the start of IST 2 in a similar manner as the IST 1 team.</p>
                            <p>IST 2 is done in a fresh quality client that is prepared during the Mock 2 cutover and reconnected to the external systems’ test environments.  Using a fresh quality client allows the Project Team to reference the original test environment to resolve defects, if necessary.  The same test scripts that were run during IST 1 are executed a second time.  Mock 2 migrated data is used.  Production-like security roles and job schedules are set up and activated in the fresh quality client before testing begins so that they are used throughout this cycle of testing as they will be in production.   IST 2 follows the same process as IST 1, whereby the test results are documented using the test scripts. Defects encountered are logged together with the fixes.   After all test scripts have been run by the testers, an additional few days will be scheduled at the end of the cycle to fix and retest any remaining defects.</p>
                            <p>Once IST 2 is completed for all the end to end business scenarios as per the test scripts, a stage-gate review is done with the PMO and the Steering Committee.  Test results are shared with all relevant stakeholders from the customer and the decision is made to provide the sign off of the integration testing, thereby, accepting the To-Be solution. In case, any conditions are mentioned for the approval, these are captured and documented within the Project Status Tracking Sheet for future references and actions. </p>
                            <p>Performance/Stress Testing is performed by the Technical Infrastructure Team with the support of the attune Functional and Technical Consultants and with input from the Customer’s Core Project Team. Performance/Stress testing is done in a new /separate QA client in parallel with IST 2.  A tool, such as HP LoadRunner, is used to simulate expected peak volumes of key/critical transactions and measure actual performance against expected response times.  Peak numbers of end-users using the same transaction at the same time in the system are also simulated and response times measured.  Network connections in all locations that the project impacts are also stress-tested at this time.  Unsatisfactory results are analyzed and resolved.  Once all performance test results are satisfactory, the results are shared with the BPO’s and the customer’s Team Leads for their sign-off on the expected performance of the system.  </p>
                            <p>Regression testing is performed by existing businesses that are currently operating in the production environment that the project is being implemented in.  Regression testing is not applicable if the implementation is the first one in the production environment.  The purpose of regression testing is to ensure that functionality being used by businesses already operating in the production environment is not broken by the configuration and new development done for the project implementation.  Regression testing is performed in the quality environment using test scripts that cover the end-to-end business processes used by existing production businesses.  Generally, these test scripts were developed when these business processes originally went live.  Regression testing is done at the end of the Realization – Test Phase in parallel with the End User Showcase.</p>  
                            <p>The End-User Showcase is conducted in the quality environment after IST 2 is signed off by the Steering Committee. The customer’s Core Project Team, BPO’s, and BPE’s with the support of the attune Project Team perform the End-User Showcase. The target audience are the users that are impacted by the system implementation. The End-User Showcase is generally a roadshow that is done face-to-face. The objective of the End-User Showcase is to give the users an opportunity to see a demo of the system and gain an understanding of the business process changes that are being implemented with it prior to being trained on the system. It is a key step in the adoption of the system. </p>
                            <p>The Project Status Tracking Sheet will carry all the discussed Issues, CRs, Assumptions, Risks, Decisions, and Action Items encountered in this phase. </p>
                            <p>During the Realization – Test Phase, a number of activities are carried out to prepare for proceeding to the next phase, which is Final Preparation and Cutover.  The Training schedule is prepared to train all of the business users (i.e. end users). The development of training materials continues from the Realization – Build Phase and is completed in the Realization – Testing Phase.  Assessments are done to gauge the readiness of the Organization to proceed with the Business Go Live and organizational communications are done to create awareness of the impending Go Live. The Organizational assessment and communications are done by the customer’s Management team in conjunction with the project Business Transformation Team. The production system is set up and released to the DM Team.</p>
                            <p>Also during the Realization – Test Phase, the attune project team together with the customer’s core project team will do detailed Live cutover planning with the Customer’s business team. How the business will ramp down so that the legacy system can be shut off is discussed and decided.  How the ramp up of the business on the new system will be done is also discussed and decided.  The PMO will develop a Live Cutover Schedule.  The Mock Cutovers (i.e. Dry Run) that are done as part of testing also serve to prepare and plan for the Go Live by migrating data from legacy systems to the quality environment.  The mock cutovers (i.e. Mock 1 and Mock 2) will have all the activities to be performed during the Go Live in the right sequence. This is done to help gauge the time required for the completion of the Go Live and also deal with any unexpected issues related to data conversion whereby the issues can be avoided during the actual Go Live.  Similar to how Mock 1 migrated data was used for Integration Test 1, Mock 2 migrated data is used for Integration Test 2.   The customer’s core project team will prepare scripts for the Controlled resumption.</p>
                            <p>The PMO prepares the Support Plan for Post-go-Live Support and a Business Continuity Plan for the implementation. The Business Continuity Plan describes the strategy and approach for continuing business in the unlikely event that the implementation goes so badly that it must be stopped.  Actions are taken to ensure that the mitigation approach is ready to be executed, if required.</p>
                            <p>With the completion of all of the activities mentioned above, the teams then focus on moving to the next phase, Final Preparation and Cutover. </p>
                            <p>The table below highlights each of the activities mentioned above in detail. It provides information on the activities, the responsible person or team, the documents created in each activity (i.e. artifacts) and the reference documents used. </p>


                            <h4>Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="3"><Strong>Realization – Test  -  Integration Testing </Strong></td>
                                            <td><p><Strong>1.	Conduct Integration Testing Kick off</Strong></p>
                                                <p>In this activity, the Integration Testing is initiated. </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Conduct the Testing Kick off meeting with the relevant stakeholders from customer, the customer’s core project team, the attune project team, the customer’s testing team.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Provide QA system access to the customer’s core project team, attune project team, the testing team with the required authorizations.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Confirm that all released transports are in the quality environment using the Transport Tracking sheet.  </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Put automated Transport Management Procedures in place for testing. </p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Share the testing schedule, testing timeline, the test scripts to be used for testing with all relevant stakeholders from the customer, the customer’s core project team, attune project team and testing team in order to create the awareness and start the testing</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Confirm that the testing room(s), infrastructure, and equipment are ready</p></td>
                                            <td>PM / SA / FL / TL / FC / TC / SAP Security / Customer’s core project team</td>
                                            <td>Testing Kick-Off Presentation / Updated Testing Schedule / Updated Detailed Project Plan</td>                                    
                                            <td>Detailed Project Plan / Test Scripts / Testing Schedule / Transport Tracking Sheet</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>2.	Conduct Training for Integration Cycle 1 Testers</Strong></p>
                                                <p>This activity is owned by the project’s Training Manager and is completed by the Training Team with support from the Customer’s core project team and the attune project team.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The list of Integration Testers are identified for each business process listed under each SAP module (as per the As-Is and To-Be process documents completed during the Realization – Build Phase. </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	During the Realization – Build Phase, the Training Manager and the Testing Lead created the Training Schedule and sent out the meeting invites to the Integration Testers requiring training.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The training team member responsible for each module takes the identified Integration Testers through the training.  The training covers:    </p>
                                                <p class="tab-content" style="padding-left: 40px">i.	SAP Navigation</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	The business processes covered within each SAP module (focus is on the To-Be business processes documents with the process flows as per the Business Blueprint Document)</p>
                                                <p class="tab-content" style="padding-left: 40px">iii.	The system features and functions within each business process</p>
                                                <p class="tab-content" style="padding-left: 40px">iv.	The concept of master data, how it is created or maintained and how it is used</p>
                                                <p class="tab-content" style="padding-left: 40px">v.	The concept of the transactional data and how transactions are entered in SAP for each step in the To-be process flow together with the main SAP transactions</p>
                                                <p class="tab-content" style="padding-left: 40px">vi.	Hands on training with access to SAP system (the quality environment that will be used for IST 1 is used for this purpose)</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	A customer core team member and an attune project team member from each module is present to support the Trainer and to help answer any questions or provide clarifications.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The Training manuals are updated by the Training team in order to fine tune the documents to cover all the To-Be business processes</p>
                                            <td>TM / Training Team / Customer’s Core project team / FC / SAP Security</td>
                                            <td>Integration Testers’ Training Schedule / End User Training Materials</td>
                                            <td>Training Schedule used for testing team and core team training / Training Materials used for testing team and core team training / Business Blueprint Document / To-Be Business Process Documents with process flow diagrams</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>3.	Conduct Preliminary testing of End-to-End Processes</Strong></p>
                                                <p>While the IST 1 testers are being trained, the attune consultants perform end-to-end testing in the QA environment to ensure that the system is ready for the customer to begin their testing.</p>
                                                <p>In this activity, the following takes place:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Project team identifies certain key scenarios (from the Business Process Documents) </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Conduct the testing for the identified scenarios in the IST 1 quality environment</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Log and resolve the issues faced</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Provide the “Go” decision for Integration Testing</p>
                                            <td>SA / FL / TL / FC / TC / SAP Security</td>
                                            <td>End-to-End Testing Issue Log</td>
                                            <td>Business Process Documents / TR Tracking Sheet / Build Tracker</td>
                                        </tr>
                                        <tr>
                                            <td><Strong>Realization – Test  -  Integration Testing Cycle 1</Strong></td>
                                            <td><p><Strong>4.	Conduct IST 1 Testing and Manage Defects & CR’s</Strong></p>
                                                <p>This activity covers the performance of the first cycle of Integration testing. Activities for Integration testing are:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Integration testing is done by the customer’s core project team  and selected extended team members with the support of attune’s project team.   </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The already defined test scripts covering the end to end business scenarios are used for the testing</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The test scripts are updated with the test results.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Defects found are recorded in the Defect Log together with a description of the fix made to resolve the defect.  The appropriate step(s) in the test script are marked as “Failed”.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	After the fix is in the quality environment, the tester retests.  If the defect is resolved, the step in the test script is marked as “Passed”.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Any CRs requested must follow the Change Request Management process.  If approved, the relevant stakeholders from the customer determine when the CR will be accommodated based on business criticality (i.e. during Realization or after Go-live).</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	For those CRs that are approved, the Build Tracker is updated.  The project Development Process is followed for each CR.  The development is scheduled by the PM according to the implementation decision made.  </p>
                                                <p class="tab-content" style="padding-left: 20px">h.	All the end to end business scenarios documented in the test scripts are tested together with the interfaces to other third party systems.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	Once all of the steps in the test scripts have been run, a few additional days are added to resolve all remaining defects that have not yet been fixed and retested during the Integration Test cycle.  As described above most defects will be resolved throughout the test cycle.  This activity allows time to clean up any defects that are still outstanding before the next integration test cycle begins.   </p>
                                                <p class="tab-content" style="padding-left: 20px">j.	Once all of the steps in all of the test scripts are passed with no further defects, the results are reviewed with the relevant stakeholders of the customer.  </p>
                                                <p class="tab-content" style="padding-left: 20px">k.	A stage gate review of the exit criteria for IST 1 and the entrance criteria for IST 2 is done by the PMO with the Steering Committee.  The SC makes the decision to exit IST 1.  If the decision is made pending the completion of any open testing, the open items are tracked by the PMO to completion.</p>
                                                <p class="tab-content" style="padding-left: 20px">l.	The completed test scripts with results attached are archived for access and review by Internal Audit.</p>
                                                <p class="tab-content" style="padding-left: 20px">m.	The Business Blueprint documents and the To-Be Business Process documents with the process flows, the FS/TS, the Transport Tracking sheet, the test scenarios, and the test scripts must all be updated for approved CR’s and for defect resolutions, where required.  </p></td>
                                            <td>TSC / Customer’s Core Project team / BPO’s / BPE’s / All relevant stakeholders of the customer / PM / SA / TA / FL / TL / FC / TC /  SAP Security /  SC</td>
                                            <td>Updated Test Scripts with test results / Defect Log / Test Status Reports / Updated or new FS/TS Document for Development Objects / Updated Build Tracker / Updated Project Status Tracking Sheet / Updated CR Log /  Updated Business Blueprint documents</td>                                    
                                            <td>Updated Detailed Project Plan / Updated Testing Schedule / Test Scripts / Training Schedule / Training Materials </td>
                                        </tr>
                                        <tr>
                                            <td><Strong>Realization – Test  -  Integration Testing Cycle 2</Strong></td>
                                            <td><p><Strong>5.	Mock 2 Cutover (Dry Run)</Strong></p>
                                                <p>This involves a trial run of the activities to be performed during Go Live. The data is migrated from the legacy system(s) to a fresh quality test environment.  100% of the data is loaded and validated.  Exceptions are approved by the PMO.</p>
                                                <p>The test environment that is set up in this dry run will be used for IST 2.  </p>
                                                <p>The following activities are completed here:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The PM prepares the Mock 2 Cutover Schedule which consists of all the activities to be performed during Go Live in the right sequence</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The PM kicks off each activity in the Mock Cutover Schedule in an email and tracks the completion of the activity.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The IST 2 quality client is built by the Basis Team and released to the DM Team.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The FC and TC perform required set up activities in the IST 2 quality client.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The relevant stakeholders from the customer confirm that data in the legacy production system that is to be migrated is cleansed.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	A copy of the legacy production system is taken and installed on a test database with limited access (i.e. frozen database)</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The data to be migrated is extracted from the frozen database  or manually prepared in the formats required for the Data Conversion Tools (the same tools used during Mock Cutover 1 during the Build phase) </p>
                                                <p class="tab-content" style="padding-left: 20px">h.	The extracted data is reviewed and validated by the relevant stakeholders from the customer.  Issues reported are tracked and resolved.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	The TRs listed in the Transport Request Tracking sheet are moved into the IST 2 quality environment</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	With the above step, all of the objects both configuration and development, are available in the IST 2 quality environment</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	The validated extract data and the manually prepared data are loaded into the IST 2 quality environment starting with the master data and then moving to the transactional data.</p>
                                                <p class="tab-content" style="padding-left: 20px">l.	The migrated data is reviewed and validated in the IST 2 quality environment by the relevant stakeholders from the customer.   Issues reported are tracked and resolved.   The goal is to accurately load 100% of the legacy data.  Exceptions are approved by the PMO.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                <p class="tab-content" style="padding-left: 20px">m.	The FC and TC perform any required post data migration set up activities in the IST 2 quality client.</p>
                                                <p class="tab-content" style="padding-left: 20px">n.	The middleware and 3rd party system cutover activities are performed and validated.</p>
                                                <p class="tab-content" style="padding-left: 20px">o.	Once it is confirmed that everything is in order, the Dry Run or Mock Cutover 2 is considered complete and the IST 2 quality environment ready for conducting IST 2.</p>
                                                <p class="tab-content" style="padding-left: 20px">p.	The status and the results of the Dry Run are shared daily with all relevant stakeholders of the customer, the customer’s core project team and, if required, the customer’s and attune management team.</p>
                                                <p class="tab-content" style="padding-left: 20px">q.	The actual time taken to complete the Dry Run is captured.  Based on this information, the PMO determines if enough time has been allotted for the Live Cutover in the Detailed Project Schedule. If not,  corrective action is recommended to the PMO and SC.</p></td>
                                            <td>PM  / DM   Team / Basis / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Mock 2 Cutover Schedule / Updated Project Status Tracking Sheet / Updated Detailed Project Plan / Meeting Minutes of discussions taken place to discuss and overcome issues faced during the Dry Run</td>                                    
                                            <td>Mock 1 Cutover Schedule / Detailed Project Plan / Project Status Tracking Sheet / Project Charter </td>
                                        </tr>
                                        <tr>
                                            <td><Strong>Realization – Test  -  Integration Testing </Strong></td>
                                            <td><p><Strong>6.	Conduct Training for New Integration Cycle 2 Testers</Strong></p>
                                                <p>This activity is owned by the project’s Training Manager and is completed by the Training Team with support from the Customer’s core project team and the attune project team.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The list of new Integration Testers are identified for each business process listed under each SAP module.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	During the IST 1 testing the Training Manager and the Testing Lead create the Training Schedule and send out the meeting invites to the new Integration Testers requiring training.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The training team member responsible for each module takes the identified new Integration Testers through the training.  The training covers the same topics that were covered for IST 1 testers.   </p>  
                                                <p class="tab-content" style="padding-left: 20px">d.	A customer core team member and an attune project team member from each module is present to support the Trainer and to help answer any questions or provide clarifications.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The Training manuals are updated by the Training team in order to fine tune the documents to cover all the To-Be business processes</p></td>
                                            <td>TM / Training Team / Customer’s Core project team / FC / SAP Security</td>
                                            <td>Integration Testers’ Training Schedule / End User Training Materials</td>                                    
                                            <td>Training Schedule used for testing team and core team training / Training Materials used for testing team and core team training / Business Blueprint Document / To-Be Business Process Documents with process flow diagrams</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2"><Strong>Realization – Test  -  Integration Testing Cycle 2</Strong></td>
                                            <td><p><Strong>7.	Conduct IST 2 Testing and Manage Defects & CR’s</Strong></p>
                                                <p>This activity covers the performance of the second cycle of Integration testing. Activities for Integration testing are:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Integration testing is done by the customer’s core project team and selected extended team members with the support of attune’s project team.   </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The same set of test scripts that were used in IST 1, updated for CR’s approved for implementation at Go-Live, are used for IST 2.  A new set of master data should be used.  The goal is to find any remaining defects and resolve them.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The testing schedule used in IST 1 is updated for IST 2.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Data migrated in Mock 2 is used for IST 2.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The testing team is given access to the IST 2 quality environment with the required authorizations that they will have in production.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Job schedules are set up and activated in the IST 2 environment to simulate how they will run in production. </p>
                                                <p class="tab-content" style="padding-left: 20px">g.	As IST 2 is performed, the test scripts are updated with the test results.</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Defects found are recorded in the Defect Log together with a description of the fix made to resolve the defect.  The appropriate step(s) in the test script are marked as “Failed”.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	After the fix is in the quality environment, the tester retests.  If the defect is resolved, the step in the test script is marked as “Passed”.</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	Any CRs requested must follow the Change Request Management process. If approved, the relevant stakeholders from the customer determine when the CR will be accommodated based on business criticality (i.e. during Realization or after Go-live.  CR’s should generally not be approved for Go-live at this stage because there is not enough testing time left to test them adequately.</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	For those CRs that are approved, the Build Tracker is updated.  The project Development Process is followed for each CR.  The development is scheduled by the PM according to the implementation decision made.  </p>
                                                <p class="tab-content" style="padding-left: 20px">l.	All the end to end business scenarios documented in the test scripts are tested together with the interfaces to other third party systems.</p>
                                                <p class="tab-content" style="padding-left: 20px">m.	Once all of the steps in the test scripts have been run, a few additional days are added to resolve all remaining defects that have not yet been fixed and retested during the Integration Test cycle.  As described above most defects will be resolved throughout the test cycle.  This activity allows time to clean up any defects that are still outstanding before the stage gate review.</p>
                                                <p class="tab-content" style="padding-left: 20px">n.	Once all of the steps in all of the test scripts are passed with no further defects, the results are reviewed with the relevant stakeholders of the customer.  </p>
                                                <p class="tab-content" style="padding-left: 20px">o.	A stage gate review of the exit criteria for IST 2 and the entrance criteria for the End-User Showcase is done by the PMO with the Steering Committee.  The SC makes the decision to exit IST 2.  If the decision is made pending the completion of any open testing, the open items are tracked by the PMO to completion.</p>
                                                <p class="tab-content" style="padding-left: 20px">p.	The Business Blueprint documents and the To-Be Business Process documents with the process flows, the FS/TS, the Transport Tracking sheet, the test scenarios, and the test scripts must all be updated for approved CR’s and for defect resolutions, where required.  </p></td>
                                            <td>TSC / Customer’s Core Project team / BPO’s / BPE’s / All relevant stakeholders of the customer / PM / SA / TA / FL / TL / FC / TC /  SAP Security /  SC</td>
                                            <td>Updated Test Scripts with test results / Defect Log / Test Status Reports / Updated or new FS/TS Document for Development Objects / Updated Build Tracker / Updated Project Status Tracking Sheet / Updated CR Log /  Updated Business Blueprint documents</td>                                    
                                            <td>Updated Detailed Project Plan / Updated Testing Schedule / Test Scripts / Training Schedule / Training Materials </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>8.	Conduct Performance/ Stress Testing</Strong></p>
                                                <p>This test is led by the Project’s Technical Architect and conducted by the Infrastructure Team in a fresh/separate quality environment with a good set of master data loaded.   This is a copy of the quality environment used for IST 2 taken after Mock 2 cutover is complete.  A tool such a HP LoadRunner is used to simulate peak volumes.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Planning Performance/ Stress Testing:</p>
                                                <p class="tab-content" style="padding-left: 40px">i.	For automated transactions, the TA and the SA analyze the legacy production system to determine the highest volume (i.e. peak) days for key/critical /time-sensitive transactions.  Historical peak volumes are increased by a factor agreed to with the customer that reflects expected future peak volumes.  The expected future peak volumes are simulated in the stress test.</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	For key/critical manual transactions, the TA and the SA work with the customer’s BPO’s to determine the maximum number of end-users expected to be in the system at the same time.  The peak number of users is simulated in the test and response times measured. </p>
                                                <p class="tab-content" style="padding-left: 40px">iii.	The customer’s BPO’s determine the acceptable response times per transaction, automated and manual, that must be achieved to pass the performance tests.</p>
                                                <p class="tab-content" style="padding-left: 40px">iv.	Network connections in all locations are tested by simulating  peak volumes</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Conducting Performance/ Stress Tests and managing issues and defects:</p>
                                                <p class="tab-content" style="padding-left: 40px">i.	Performance / stress tests for each transaction and network connection identified during planning are done by simulating the volumes or the number of end-users in the system at one time.  Actual response times are recorded and compared to the “acceptable” response times identified during planning.</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	Defects found are recorded in the Defect Log together with a description of the fix made to resolve the defect.  The appropriate step(s) in the test script are marked as “Failed”.</p>
                                                <p class="tab-content" style="padding-left: 40px">iii.	The TA, SA, TL, and the FL troubleshoot and resolve all performance areas that have failed.</p>
                                                <p class="tab-content" style="padding-left: 40px">iv.	Retest resolved defects.  When the actual test results are equal to or better than the acceptable results, the test results are deemed satisfactory.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Once all performance test results are satisfactory, results are shared with the customer’s BPO’s and Team Leads. The BPO’s and Team Leads are asked to sign off on the performance of the system.  </p>
                                            <td>TA / Basis Team / SA / FL / TL / Customer’s Team Leads</td>
                                            <td>Performance/ Stress Test Scripts with test results / Updated Defect Log / Test Status Reports / Updated FS/TS documents / Updated Project Status Tracking Sheet / Updated Business Blueprint documents</td>
                                            <td>N/A</td>
                                        </tr>
                                        <tr>
                                            <td><Strong>Realization – Test  -  Regression Test</Strong></td>
                                            <td><p><Strong>9.	Conduct Regression Testing and Manage Defects</Strong></p>
                                                <p>This activity covers the performance of regression testing.  It will be carried out after IST 2 is signed off by the Steering Committee and at the same time as the End-User Showcase.  The purpose of regression testing is to ensure that functionality currently in production is not broken when the new functionality is added.  Activities for regression testing are:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The regression testing is done by representatives from the customer’s businesses that are already operating in the production environment.  They are supported by the customer’s core project team and attune’s project team.   </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The same set of test scripts are used that were written when the business processes originally went live.  Test scripts cover key and critical end-to-end business processes as well as those that are impacted by the new functionality.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The copy of production that is used to build the quality environment will contain the master data that will be used. </p> 
                                                <p class="tab-content" style="padding-left: 20px">d.	The TSC will create the testing schedule with the customer’s business representatives.</p> 
                                                <p class="tab-content" style="padding-left: 20px">e.	The testing team is given access to the IST 2 quality environment with the same authorizations that they have in production.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Job schedules are set up and activated in the IST 2 environment to simulate how they run in production. </p>
                                                <p class="tab-content" style="padding-left: 20px">g.	As regression testing is performed, the test scripts are updated with the test results.</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Defects found are recorded in the Defect Log together with a description of the fix made to resolve the defect.  The appropriate step(s) in the test script are marked as “Failed”.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	After the fix is in the quality environment, the tester retests.  If the defect is resolved, the step in the test script is marked as “Passed”.</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	All the end to end business scenarios documented in the regression test scripts are tested together with the interfaces to other third party systems.</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	Once all of the steps in all of the test scripts are passed with no further defects, the results are reviewed with the relevant stakeholders of the customer.  </p>
                                                <p class="tab-content" style="padding-left: 20px">l.	The Business Blueprint documents and the To-Be Business Process documents with the process flows, the FS/TS, the Transport Tracking sheet, the test scenarios, and the test scripts must all be updated for defect resolutions, where required.  </p></td>
                                            <td>Customer’s Testers representing businesses already in production / TSC / </td>
                                            <td>Updated Regression Test Scripts with test results / Defect Log / Test Status Reports / Updated or new FS/TS Document for Development Objects / Updated Build Tracker / Updated Project Status Tracking Sheet / Updated CR Log /  Updated Business Blueprint documents</td>                                    
                                            <td>Regression test scripts / Updated Detailed Project Plan / Updated Testing Schedule / Test Scripts / Training Schedule / Training Materials</td>
                                        </tr>                                    
                                        <tr>
                                            <td><Strong>Realization – Test  -  End-User Showcase </Strong></td>
                                            <td><p><Strong>10.	Conduct End-User Showcase </Strong></p>
                                                <p>The EUS is done by the BPO’s, BPE’s, and the customer’s core project team with support from the attune project team for the end users impacted by the implementation.  It is done face-to-face with the users.  It may take multiple days to complete.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The scripts used for the EUS system demo are selected from the Integration Test Scripts and cover the end to end business processes relevant to the users.  </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Business Process documents are selected to align with the system demo.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The EUS schedule is prepared to cover all locations impacted.  Those conducting the EUS are assigned and make travel plans.  Invitations are sent to the participants.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The IST 2 QA environment is used for the EUS.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The EUS team is given access to the IST 2 quality environment with the required authorizations </p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Job schedules are already activated in the IST 2 environment to simulate how they will run in production. </p>
                                                <p class="tab-content" style="padding-left: 20px">g.	As EUS sessions are performed, a parking lot is maintained with questions raised by the participants.</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Parking lot items are reviewed by the customer’s core project team and the attune project team and responses provided.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	Any CRs requested must follow the Change Request Management process.  If approved, the relevant stakeholders from the customer determine when the CR will be accommodated based on business criticality after Go-live.  CR’s should generally not be approved for Go-live at this stage because there is not enough testing time left to test them adequately.</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	For those CRs that are approved, the Build Tracker is updated.  The project Development Process is followed for each CR.  The development is scheduled by the PM according to the implementation decision made.</p>  
                                                <p class="tab-content" style="padding-left: 20px">k.	All the end to end business scenarios documented in the EUS scripts are performed together with the interfaces to other third party systems.</p>
                                                <p class="tab-content" style="padding-left: 20px">l.	Once all of the EUS sessions are completed, the results are reviewed with the relevant stakeholders of the customer.  </p>
                                                <p class="tab-content" style="padding-left: 20px">m.	A stage gate review of the exit criteria for EUS and the entrance criteria for Final Prep and Go-Live is done by the PMO with the Steering Committee.  The SC makes the decision to exit the Realization Test Phase.  If the decision is made pending the completion of any open testing, the open items are tracked by the PMO to completion.</p>
                                                <p class="tab-content" style="padding-left: 20px">n.	The Business Blueprint documents and the To-Be Business Process documents with the process flows, the FS/TS, the Transport Tracking sheet, the test scenarios, and the test scripts must all be updated for approved CR’s and for defect resolutions, where required.  </p></td>
                                            <td>TSC / Customer’s Core Project team / BPO’s / BPE’s / All relevant stakeholders of the customer / PM / SA / TA / FL / TL / FC / TC /  SAP Security /  SC</td>
                                            <td>End-User Showcase materials / Updated Project Status Tracking Sheet / Updated CR Log /  Updated Business Blueprint documents</td>                                    
                                            <td>Updated Detailed Project Plan / Updated Testing Schedule / Test Scripts / Training Schedule / Training Materials </td>
                                        </tr>  
                                        <tr>
                                            <td rowspan="9"><Strong>Realization – Test  -  Prepare for Live Cutover</Strong></td>
                                            <td><p><Strong>11.	Prepare Production System</Strong></p>
                                                <p>Within this activity the following takes place: </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Final Transport Tracking sheet is confirmed with the TRs in the right sequence</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The list of user IDs to be created with the relevant authorizations and accesses are prepared for the business users</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The Production system is built and set up for Go Live</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	All connections to third party systems are set up from the Production system</p></td>
                                            <td>PM / FL / TL / SAP Security / Customer’s core project team </td>
                                            <td>Authorization Matrix and User ID List</td>                                    
                                            <td>Not available</td>
                                        </tr>  
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>12.	Live Cutover Planning</Strong></p>
                                                <p>The attune PMO and Team Leads together with the customer’s core project team,  will do detailed Live cutover planning with the Customer’s BPO’s and BPE’s and the Customer’s IT Team that is  responsible for 3rd party systems and the legacy systems.      </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Discuss and decide how each aspect of the business impacted by the implementation will ramp down so that the legacy system can be shut off for 3-5 days with minimal impact to the business and its partners.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Determine the cut-off dates for creating or changing master data and get BPO approval.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Determine the cut-off dates for live transactions and get BPO approval. </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Discuss and decide how the ramp up of the business on the new system will be done.  This planning includes a day of Controlled Resumption followed by several days/weeks ramping up while in the Post Go-live Support period.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The PMO will develop a Live Cutover Schedule that covers the detailed Business Shutdown activities, legacy and 3rd party systems shutdown activities, Data Migration activities, legacy and 3rd party systems conversion and ramp-up activities, and Business Ramp-Up activities.  Scheduled times are based on actual times to perform each activity during Mock Cutover.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The BPE’s will create Controlled Resumption scripts.  The BPO’s approve them.</p></td>
                                            <td>PGM /PM / PMO / BPO / BPE / Customer IT / TA / SA / FL /TL</td>
                                            <td>Live Cutover Schedule / Business Shutdown and Business Ramp-up approach</td>                                    
                                            <td>Mock Cutover Schedules</td>
                                        </tr>  
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>13.	Prepare Job Schedule</Strong></p>
                                                <p>The batch job schedule needs to be prepared, programmed and tested during the test phase. As much as possible, batch jobs and the schedule should be tested during integration test cycle 2.</p></td>
                                            <td>FL / FC / TL</td>
                                            <td>Final Job Schedule</td>                                    
                                            <td>Batch job worksheets</td>
                                        </tr> 
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>14.	Prepare for End User Training</Strong></p>
                                                <p>This activity is primarily performed by the training team in consultation with the project teams. </p>
                                                <p class="tab-content" style="padding-left: 20px"></p></td>
                                            <td>TM / Training Team / Core Project team / Customer Extended Team </td>
                                            <td>Final End User Training Manuals (Material)/ End User Training Schedule / Training Evaluations</td>                                    
                                            <td>Training Needs Assessment/ End User Training Manuals used for IST Training / Updated schedule/ Training evaluations</td>
                                        </tr> 
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>15.	Assess Organizational readiness</Strong></p>
                                                <p>This activity is performed by the Business Transformation Team together with the project PMO. This involves: </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Finalizing the Readiness indicators for each work stream</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Each work stream rating  themselves against a set of given readiness indicators</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Work stream ratings are collected and analyzed to produce the Go-Live Readiness Assessment</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Based on any gaps, mitigation strategy is implemented before or after go-live as appropriate </p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Sustainment Strategy is formulated</p></td>
                                            <td>PGM / PM / Customer Core Project team / The Business Transformation Team  / Steering Committee</td>
                                            <td>Go-Live Readiness  Assessment document  / Sustainment Strategy  </td>                                    
                                            <td>Updated Detailed Project Plan / Project Charter / Project Kick Off presentation / Updated Project Status Tracking Sheet/Communication Plan </td>
                                        </tr> 
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>16.	Prepare Organization Communication </Strong></p>
                                                <p>In this activity, the communications are prepared and shared in order to pass the message to everyone in the organization during the decided timeline on the impending Go Live and the support of the whole organization is garnered in order to ensure the Go Live is a success.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Finalize Go-Live communication plan</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Templates are created and communication is ready to be executed</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Go-Live communication plan is executed</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Feedback is actively solicited from Change Agents to ascertain state of business readiness</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Address any ad-hoc communication requirements.</p></td>
                                            <td>Business Transformation Team / Project Steering Committee</td>
                                            <td>Communication Templates – various channel and tools</td>                                    
                                            <td>Stakeholder Map /Communication Plan/Go-Live Readiness Assessment /Help Desk Procedures – Escalation process</td>
                                        </tr> 
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>17.	Prepare Post-Go-Live Support Plan and Procedures</Strong></p>
                                                <p>The post-go-live support plan outlines how continuous support will be provided to the business users immediately after Go-Live.  It is prepared by the PM working closely with the attune Leads and Customer Leads.  The plan covers the procedures for post-go-live support, the resources required, the knowledge transfer from Project Team to Support Team, where needed, the locations that will be supported onsite vs remotely, the tools for recording and tracking support issues reported by end users, and the status reporting.  </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The plan and procedures are discussed with all of the relevant stakeholders from the customer and attune, and the plan is approved by the customer. </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The PM begins to line up the required resources and prepare for the necessary knowledge transfer.</p></td>
                                            <td>PM / FL /  TL / Customer PM / BPO’s / BPE’s</td>
                                            <td>Post-Go-Live Support Plan / Post-Go-Live Support Procedures</td>                                    
                                            <td>Project Charter (i.e. Support Strategy))</td>
                                        </tr> 
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>18.	Prepare Business Continuity Plan</Strong></p>
                                                <p>The PMO prepares a Business Continuity Plan for the implementation.  </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Business Continuity Plan describes the strategy and approach for continuing business in the unlikely event that the implementation goes badly and must be stopped.  </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The Business Continuity Plan and proposed mitigation procedures are discussed with all of the relevant stakeholders from the customer and attune, and the plan is approved by the customer. </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Actions are taken to ensure that the mitigation approach is ready to be executed, if required.</p></td>
                                            <td>PMO / Customer Stakeholders</td>
                                            <td>Business Continuity Plan</td>                                    
                                            <td>Project Charter</td>
                                        </tr> 
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><Strong>19.	Store all documents created and updated by attune during the Phase on ONEattune in the Project workspace</Strong></p>
                                                <p>It is important to retain attune intellectual property on ONE attune.  This activity applies to every member of the team.  It includes documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </p></td>
                                            <td>All attune project team members</td>
                                            <td>Updated ONEattune Project work space</td>                                    
                                            <td>Documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </td>
                                        </tr>                                
                                                                                                                                                                                                                                                                                                                   
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Complete, updated and approved Functional and Technical Specification documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Unit tests passed for all configured and developed objects.   Defects fixed and retested.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed QA System with all configurations and developments, including SAP, middleware, EDI, and 3rd party system changes, if required</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed and passed string testing of all interfaces</p>
                                            <p class="tab-content" style="padding-left: 20px">•	100% of Mock 0 data is converted, exceptions approved by the PMO </p>
                                            <p class="tab-content" style="padding-left: 20px">•	100% of Mock 1 data migrated and all cutover tasks completed, exceptions approved by the PMO</p>
                                            <p class="tab-content" style="padding-left: 20px">•	QA environment ready for Integration Testing</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Integration Testing Schedule and Scripts</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All required integration testers assigned and booked per the testing schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Integration Testing Team Training Materials</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Testing tool set up and defect tracking tool in place</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Preliminary end-to-end process testing executed and all steps passed </p>
                                            <p class="tab-content" style="padding-left: 20px">•	All integration test scripts executed and all steps passed in 2 cycles of Integration Testing.  Exceptions approved by the Steering Committee.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All End-User Showcase sessions conducted and feedback given for questions raised. </p>
                                            <p class="tab-content" style="padding-left: 20px">•	System Acceptance sign-off by BPOs.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All regression test scripts executed and all steps passed.  Exceptions approved by the Steering Committee.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All Performance/Stress Testing scripts executed and passed. Exceptions approved by the Steering Committee.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Production environment ready for Live Cutover</p>
                                            <p class="tab-content" style="padding-left: 20px">•	100% of Mock 2 Cutover completed. Exceptions approved by the Steering Committee.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Live Cutover business shutdown and ramp-up plans approved</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Controlled Resumption scripts</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Ready for End User Training</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Go-Live Readiness Assessments completed and published</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Organization Communications prepared and shared</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Post-Go-Live Support Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Post-Go-Live Support Procedures</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Business Continuity Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All Business Blueprint documents, System Landscape Diagram, and Functional/Technical Specification documents updated with changes related to approved CR’s and refinement of the solution during the Test Phase</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All Critical and High severity defects resolved and successfully retested.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All Medium severity defects logged and either resolved and retested or workarounds documented.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All Low severity defects logged.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Exit Stage Gate Review with all items passed and exceptions approved by Steering Committee</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	RICEF Object List</p>
                                            <p class="tab-content" style="padding-left: 20px">•	List of Objects to be configured</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated System Landscape Diagram</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Functional and Technical Specification Documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Baseline Project  Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Build Tracker </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Transport Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Communication effectiveness survey</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integrated communication plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized Testing Schedule for Integration testing</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized Testing Status Report</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized Test Scripts</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Defect Management Process Document</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized Testing Team Training Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Testing Team Training Materials</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Project Status Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	End User Training Curriculum, Course Outlines, ILT Templates and development tracker</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Role Mapping Document (including Business Roles mapped to Security Roles)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Testing Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated or New Functional and Technical Specification documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Business Blueprint documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated System Landscape Diagram</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Master Project Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Mock Cutover schedules</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Defect Log</p>
                                            <p class="tab-content" style="padding-left: 20px">•	End User Training Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Test Scripts with test results</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Performance/Stress Test scripts with results</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Project Status Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All required End User Training Materials</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Live Cutover Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Live Cutover Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Controlled Resumption Scripts</p>
                                            <p class="tab-content" style="padding-left: 20px">•	User ID List and Authorization Matrix</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Go-Live Readiness Assessment</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Communication Templates – Mixed </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Post-Go-Live Support Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Post-Go-Live Support Procedures</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Business Continuity Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Communications Effectiveness Survey</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Train-the-Trainer Program</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Tester Training</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Training IDs</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Final Job Schedule</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Realization - Testing Phase profitability</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Coverage of Integration Testing (covering all To-Be business processes)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Coverage of Performance/Stress Testing (covering all key/critical transactions)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Percentage of test scripts successfully passed in each cycle (target 100%)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Percentage of data loaded for each Mock Cutover (target 100%)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Number of defects reported </p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	The project may decide to use client defined templates and standards for the deliverables from within the project phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Certain activities of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Certain activities of this process may be delivered in another phase of the project, as per the contractual requirements.  This tailoring is documented in the Project’s Process</p>
                                            <p class="tab-content" style="padding-left: 20px">•	In case the client or another team representing the client is performing some parts of the activities and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>
                                        </div>
                                    </td>
                                    </tr> 

                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Realization - Testing  Phase Process.pdf">Download Realization - Testing  Phase Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr> 
                                                                                                                                  
                                </tbody>
                                </table>
                            </div>
                        </div>

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <h3>Document Templates</h3>                       
                                <p><a href="References\attune BT Communication Effectiveness Survey.xlsx">1. attune BT Communication Effectiveness Survey</a></p>
                                <p><a href="References\attune BT End User Training Curriculum With Calendar Option.xlsx">2. attune BT End User Training Curriculum With Calendar Option</a></p>
                                <p><a href="References\attune BT Train the Trainer Part 1 Program Overview.docx">3. attune BT Train the Trainer Part 1 Program Overview</a></p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Guidelines</h3>                                                     
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Samples</h3>
                          </div>
                        </div>   --%>   
    
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <%--<h3>Functional & Technical</h3>--%>     
                                <h4>Templates</h4>
                                <p><a href="References\Batch Schedule.xlsx">1. Batch Job Schedule</a></p> 
                                <p><a href="References\Business Continuity Plan.docx">2. Business Continuity Plan</a></p>
                                <p><a href="References\Communication Effectiveness Survey.xlsx">3. Communication Effectiveness Survey</a></p>
                                <p><a href="References\Controlled Resumption Scripts.xlsx">4. Controlled Resumption Scripts</a></p>
                                <p><a href="References\End User Training Curriculum With Calendar Option.xlsx">5. End User Training Curriculum With Calendar Option</a></p>                
                                <p><a href="References\End User Training Course Template.docx">6. End User Training Course Template</a></p>
                                <p><a href="References\End User Training Schedule.xlsx">7. End User Training Schedule</a></p>                    
                                <p><a href="References\Job Schedule.xlsx">8. Job Schedule</a></p>                             
                                <p><a href="References\Live Cutover Plan.docx">9. Live Cutover Plan</a></p>
                                <p><a href="References\Live Cutover Schedule - Template.mpp">10. Live Cutover Schedule - Template</a></p>
                                <p><a href="References\Meeting Minutes.docx">11. Meeting Minutes</a></p>
                                <p><a href="References\Milestone Report.xlsx">12. Milestone Report</a></p>
                                <p><a href="References\Post Go Live Support Phase Process.docx">13. Post Go Live Support Phase Process</a></p>
                                <p><a href="References\Post Go Live Support Plan - Template.docx">14. Post Go Live Support Plan - Template</a></p>
                                <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">15. Services Methodology Operational Review Evaluation</a></p>
                                <p><a href="References\Stage Gate Review.pptx">16. Stage Gate Review</a></p>                              
                                <p><a href="References\Training Development and Standards.docx">17. Training Development and Standards</a></p>  
                                <p><a href="References\Train the Trainer Part 1 Program Overview.docx">18. Train the Trainer Part 1 Program Overview</a></p>
                                <p><a href="References\User Auth Role Maintenance.xlsx">19. User Auth Role Maintenance</a></p>
                                                          
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>PMO & Testing</h3>--%>   
                                <h4>Guidelines</h4> 
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>Business Transformation </h3>--%>
                                <h4>Samples</h4> 


                          </div>
                        </div>                        

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                        </div> 

                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                        </div>     --%>   
    
                                  
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>
