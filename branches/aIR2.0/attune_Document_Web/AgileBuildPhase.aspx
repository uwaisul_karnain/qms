﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AgileBuildPhase.aspx.cs" Inherits="attune_Document_Web.AgileBuildPhase" %>

<asp:Content ID="projectPreparationPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">--%>
                <%--<div class="row">
                    <div class="col-lg-12">

                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Preparation  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed and the activities to be performed in the Agile Build and Unit Test phase of an SAP implementation project lifecycle.</p>
                            <p>During the Agile Build and Unit Test Phase, Agile teams formed during the FRS Phase further define and build the final solution based upon the FRS Phase and Workshop results. The final system is configured, approved RICEFW objects are built, and unit testing of both configuration and objects is completed.  The Business Process Owners review the results of each Sprint and approve the solution to be released for integration testing.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depend upon the expected deliverables and the contractual requirements.  The activities related to the Agile Build and Unit Test Phase that are defined here derive the process followed during this phase of the project.</p> 
                        </div>
                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        
                        <p>The following is a representative list of Agile Methodology definitions and descriptions used in the Agile Build and Unit Test Phase Process.  A complete list of Acronyms, Abbreviations, and Definitions can be found in the document entitled “aIR Terminology”, found in the Guidelines section of the aIR Methodology portal.</p>
	                    <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Agile Terminology</th>
                                        <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Agile Software Development</td>                                    
                                            <td>Agile software development refers to methodologies based upon iterative development, where requirements and solutions evolve through collaboration between cross-functional teams.</td>
                                        </tr>
                                        <tr>
                                            <td>Agile Build and Unit Test Phase</td>
                                            <td>Project Phase in which the solution is configured, developed, unit tested, approved by the product owner, and documented.</td>
                                        </tr>
                                         <tr>
                                            <td>Burn Down Chart</td>
                                            <td>A Burn Down Chart is a graphical representation of work left to do versus time.  The outstanding work (or backlog) is on the vertical axis, with the time dimension along the horizontal axis.  A Burn Down Chart is created for each of the Sprints to monitor completion and to predict when the work in the Sprint will be completed.</td>
                                        </tr>
                                         <tr>
                                            <td>Product Backlog</td>
                                            <td>The Product Backlog is a prioritized features list, containing short descriptions of all functionality desired in the system.  The Product Backlog contains all User Stories associated with the configuration and development of needed for the system.</td>
                                        </tr>
                                         <tr>
                                            <td>Product Backlog Grooming</td>
                                            <td>Backlog Grooming is when the Business Process Owner and some, or all, of the Sprint team review the items on the Backlog to ensure it contains the appropriate items, that they are prioritized, and that the items at the top of the Backlog are ready for delivery.  Product Backlog Grooming occurs on an ongoing basis during the Sprint, and is an essential activity during the daily and weekly team meetings.</td>
                                        </tr>
                                         <tr>
                                            <td>Product Owner</td>
                                            <td>Customer's global Product Owner of the functional area (i.e. OTC, PTP, RTR)
                                                <p class="tab-content" style="padding-left: 20px">• Partners with attune Functional Lead for the functional area</p>
                                                <p class="tab-content" style="padding-left: 20px">• Nominates business subject matter experts based on work shop schedule</p>
                                                <p class="tab-content" style="padding-left: 20px">• Attends blueprint workshops</p>
                                                <p class="tab-content" style="padding-left: 20px">• Signs off on all business blueprint documents for the functional area</p>
                                                <p class="tab-content" style="padding-left: 20px">• Attends and signs off on solution walk through</p>
                                                <p class="tab-content" style="padding-left: 20px">• Provides oversight during all project phases to ensure that the system solution and business processes meet the  global business process area requirements</p>
                                                <p class="tab-content" style="padding-left: 20px">• Staffs the functional roles of the project with SME's and extended project team members to cover all functional activities related to specifications, testing, data migration, training, and live cutover to meet the project schedule</p>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>Scrum</td>
                                            <td>Scrum is a process framework for agile development.  The Scrum process framework requires the use of development cycles called Sprints.</td>
                                        </tr>
                                         <tr>
                                            <td>Scrum Master</td>
                                            <td>A scrum master is the facilitator for an agile development team.  The scrum master manages the process for how information is exchanged.</td>
                                        </tr>
                                         <tr>
                                            <td>Sprint</td>
                                            <td>A set period of time during the Agile Build and Unit Testing Phase during which specific work by an identified team has to be completed and made ready for review.  There are typically 4-6 Sprints during the Agile Build and Testing Phase.  Each Sprint is typically 20 to 30 days in duration.</td>
                                        </tr>
                                         <tr>
                                            <td>Sprint Plan</td>
                                            <td>Plan of specific work to be done during the Agile Build and Unit Testing Phase.  </td>
                                        </tr>
                                         <tr>
                                            <td>Sprint Retrospective</td>
                                            <td>The Sprint Retrospective is the inspect-and-adapt activity performed at the end of each Sprint during the Agile Build and Unit Test Phase.  The Sprint retrospective is a continuous improvement opportunity for a Sprint team to review its processes and identify improvements.</td>
                                        </tr>
                                         <tr>
                                            <td>Story Points</td>
                                            <td>Size of product backlog items, taking into account complexity and physical size of the backlog item.  Story points are estimated by Sprint team members.</td>
                                        </tr>
                                         <tr>
                                            <td>User Stories</td>
                                            <td>User Stories are short, simple descriptions of a feature told from the perspective of the person who desires the new capability; usually a user or customer of the system.  A user story expresses the business value for the Product Backlog items.  The User Story is equivalent to a Work Package aIR Methodology.  A User Story must be a Work Package that can be unit tested.</td>
                                        </tr>
                                         <tr>
                                            <td>Work Package</td>
                                            <td>A unit of work assigned to a consultant in the Agile Build and Test Phase of a project.  A Work Package is a RICEFW object, configuration package, or some other work that must be done to build the solution.  Can be configuration or development-related.  </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h3>Process and Steps</h3>     
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), derived from Agile principles, and PMI, SAP ASAP Focus, and SAP Activate methodologies. The aIR Methodology includes elements of Agile principles for business requirements gathering and development, while maintaining tried and true traditional approaches to ERP implementations.  The Agile Build and Unit Test Phase follows the FRS phase and includes all activities necessary to realize the proposed solution as described in the FRS Business Process Reference Documents and presented in the FRS Walkthrough. </p>
                            <p>Agile teams, formed in the FRS Phase, execute work in Sprints.  Collaboratively, the members of the Agile teams refine the requirements and the detailed solution design.  The Agile teams also perform configuration and development, unit testing, and documentation of design, specifications, and unit test results. The required specification documents for the User Stories (work packages) are prepared by the Agile team resources.</p>
                            <p>During the Agile Build phase, scope is managed by the Agile teams within each Sprint.  Any newly discovered business requirement and/or technical requirement not found in the baseline scope or any change/modification to a previously identified business requirement that has not been identified, evaluated, estimated, or approved will be considered as new scope.  The Agile teams will allocate 5% contingency for each Sprint for changes/modifications.  The Chief Product Owner/PMO, or the Product Owner of a team may decide to replace an existing user story with a new user story, or to add it to the backlog for future Sprints. </p>
                            <p>The development is carried out by the Agile teams as per the given standards and guidelines. Development and configuration are tracked by the Leads using Burn-Down Charts, and by the Project Manager using the Master Project Schedule to ensure that the progress is as per the plan. The unit testing is completed and the results are documented for the configured and developed objects, one by one, in order to ensure that these objects function per the requirements and are fit for the purpose. Finally, string testing of all interfaces is done.  Development of a User Story is considered complete once it passes unit and string testing and is signed by the Product Owner.</p>
                            <p>During the FRS Phase, the Development Environment is set up and put in place as defined in the Customer Ecosystem Architecture and the System Landscape Diagram.  During the Agile Build  Phase, the Quality Environment is set up.</p>
                            <p>When the development is completed for data migration user stories, the Data Migration Team will complete a Mock 0 migration in the Development environment. After Mock 0, the Mock 1 Cutover is done in the QA environment.</p>
                            <p>The ground work required to start the Integration Testing (Pilot) Phase is done in the Agile Build phase as well. The end to end business scenarios which are to be used for the Integration testing are defined. The test scripts are prepared as per the defined scenarios and validated by the relevant stakeholders from the customer. The Master Project Schedule is updated to define the timeline for the testing. A schedule is prepared for Integration testing and is shared with the relevant stakeholders from the customer.</p>
                            <p>In parallel, a training schedule is defined in order to train the users who are to be part of the testing team confirmed by the customer. The detailed project schedule also includes the timelines for the training. The required training materials are created as well.</p>
                            <p>The Project Status Tracking Sheet is used to document all Issues, CRs, Assumptions, Risks, Decisions, and Action Items highlighted and discussed during this phase.</p>


                             <h4>Sub-Processes within Agile Build and Unit Test Phase</h4>
                               <p>The Agile Build and Unit Test Phase consists of a series of overlapping Sub-Processes.  Activities are executed within each Sub-Process as the Phase progresses.  The Sub-Processes are:  Phase Level Execution, Sprint Level Execution, Mock Data Migration Execution, Business Transformation Execution, and Phase Exit Execution. Activities are shown in the diagram below and depict the overlapping nature of the Phase Sub-Processes. </p>
                                <img src="Images/sub-processes-image.png">

                            <h4>Phase Level Execution</h4>
                            <p>Phase Level Execution includes all high-level activities required during the Agile Build and Unit Test Phase.</p>

                            <h4>Phase Level Execution Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activities</th>
                                        <th>Responsible / Participants</th>
                                        <th>Artifacts</th>
                                        <th>Reference Documents</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="1"><strong>Phase Level Execution</strong></td>
                                            <td><p><strong>1.	Conduct the Agile Build Kick-Off Presentation. </strong></p>
                                                <p>Activities in the Agile Build and Unit Test Phase begin immediately after sign-off of the FRS Phase.  This activity is conducted at the beginning of the Agile Build and Unit Test Phase.  Attendees include all Steering Committee Members, as well as all Project Team Members.</p>
                                            </td>
                                            <td><strong>PM</strong> / All project team members</td>
                                            <td>Agile Build Kick-Off Presentation</td>                                    
                                            <td>FRS Business Process Reference Documents / Product Backlog / Sprint Plan / Master Project Schedule / Resource Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Phase Level Execution</strong></td>
                                            <td><p><strong>2.	Update Project Status Tracking Sheet. </strong></p>
                                                <p>The Project Status Tracking Sheet is updated to document all issues, change requests, assumptions, risks, and decisions.  Action Items are highlighted and discussed during weekly team meetings and Steering Committee Meetings.</p>
                                            </td>
                                            <td><strong>PM</strong></td>
                                            <td>Updated Project Status Tracking Sheet </td>                                    
                                            <td>Minutes from Sprint Scrum of Scrum Meetings / Product Backlog / Sprint Plan / Release Burn-down </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Phase Level Execution</strong></td>
                                            <td><p><strong>3.	Update Master Project Schedule.</strong></p>
                                                <p>The Master Project Schedule is updated throughout the Phase.  Any deviations to the Schedule are reported to the Steering Committee.</p>
                                            </td>
                                            <td><strong>PM</strong></td>
                                            <td>Updated Master Project Schedule</td>                                    
                                            <td>Minutes from Sprint Scrum of Scrum Meetings / Product Backlog / Sprint Plan / Release Burn-down</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Phase Level Execution</strong></td>
                                            <td><p><strong>4.	Conduct Team Training for Agile Build and Unit Testing Phase.</strong></p>
                                                <p>The Agile Coach and Scrum Masters conduct team training on Agile principles used in the Agile Build and Unit Test Phase.  The purpose is to prepare team members for all Agile Build and Unit Test activities.</p>
                                            </td>
                                            <td><strong>Agile Coach</strong> / Scrum Masters / Agile Team members</td>
                                            <td>Agile Build and Unit Test Phase Team Training Materials</td>                                    
                                            <td>Introduction to Agile principles / Introduction to aIR Methodology</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Phase Level Execution</strong></td>
                                            <td><p><strong>5.	Monthly Steering Committee Meetings</strong></p>
                                                <p>Each month the Steering Committee meets to review status of the program/project, address open decisions, and to resolve conflict. </p>
                                            </td>
                                            <td><strong>PM</strong> / Steering Committee Members</td>
                                            <td>Steering Committee Meeting Presentation</td>                                    
                                            <td>Updated Master Project Schedule / Updated Project Status Tracking Sheet / Minutes of weekly Agile Team meetings</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Phase Level Execution Entry Criteria</h3>
                                        <div class="data">
                                                <p>•	Acceptance of proposed solution and sign off of the FRS Business Process Reference Documents </p>
                                                <p>•	Completed FRS Walkthrough Presentation</p>
                                                <p>•	Personalized Reference System Complete </p>
                                                <p>•	Finalized Project Scope</p>
                                                <p>•	Master Project Schedule updated for Agile Build Phase  and resources leveled</p>
                                                <p>•	Completed FRS Phase Exit Stage Gate Review with all items passed and exceptions approved by Steering Committee</p>
                                        </div>
                                    </td>
                                        <td>
                                    <h3>Phase Level Execution  Input Documents</h3>
                                        <div class="data">

                                            <p>•	To-Be Business Process Documents with process flow diagrams (for reference)</p>
                                            <p>•	FRS Business Process Reference Documents</p>
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	System Landscape Diagram</p>
                                            <p>•	Master Project Plan updated with Agile Build and Unit Test details</p>

                                        </div>
                                    </td>
                                    <%--<td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                                <p>•	Business is fully operational on the new system and using the new business processes</p>
                                                <p>•	Production system is stable and performing according to metrics agreed with the customer</p>
                                                <p>•	Permanent Support Team and Help Desk tool in place for long-term Incident management</p>
                                                <p>•	Agreed Post Go Live Support period for Project Team completed</p>
                                                <p>•	Knowledge Transfer to Permanent Support team completed</p>
                                                <p>•	All Business Blueprint documents, Integration Landscape Diagram, and Functional/Technical Specification documents updated with changes related to approved CR’s and refinement of the solution during the Hyper-care period</p>
                                                <p>•	Lessons Learned published</p>
                                                <p>•	Completed Exit Stage Gate Review with all items passed. Exceptions approved by Steering Committee.</p>
                                        </div>
                                    </td>--%>
                                    </tr>
                                    <%--<tr class="loop">
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">

                                                <p>•	Updated Post-Go Live Support Coverage Schedule</p>
                                                <p>•	Communication of Project Closedown decision</p>
                                                <p>•	Daily Post-Go Live Support Status Communications</p>
                                                <p>•	Knowledge Transfer Schedule and tracker</p>
                                                <p>•	Lessons Learned Report</p>
                                                <p>•	Post Go Live Stage Gate Review Decision</p>
                                                <p>•	Updated Business Blueprint documents</p>
                                                <p>•	Updated End User Training Materials</p>
                                                <p>•	Updated Go Live Readiness  Assessment document</p>
                                                <p>•	Updated incident log and resolutions</p>
                                                <p>•	Updated or New Functional and Technical Specification documents</p>
                                                <p>•	Updated Project Status Tracking Sheet</p>
                                                <p>•	Updated Sustainment Strategy </p>
                                        </div>
                                    </td>
                                    </tr> --%> 
                                                                                              
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <h4>Phase Level Execution</h4>
                        <p>Sprint Level Execution includes all activities related to refining the business requirements and the detailed solution design.  Sprint Planning for the entire project, including all rollouts, is completed during the FRS Phase</p>
                        <p>Duration of the Sprints is fixed:</p>
                            <p class="tab-content" style="padding-left: 20px">•	Sprint 1 will be 6 weeks/30 days</p>
                            <p class="tab-content" style="padding-left: 20px">•	Sprint 2 through Sprint X will be 4 weeks/20 days.</p>
                        <p>Validation of the Sprint Plan will be done in the first 1-2 days prior to each of Sprints 2 through X.  During the Sprint Execution, Agile team members collaboratively perform configuration, development, unit testing, and complete design documentation, Functional / Technical Specifications, and unit testing results.  Agile team members attend a Daily Scrum (stand up) meeting to share knowledge, and identify risks and issues.  A weekly Scrum of Scrum Meeting is attended by Agile team leaders to discuss cross-team issues.  Product Backlog grooming occurs during these meetings. </p>
                        <p>A Sprint Review and Demo is held at the end of each Sprint.  A Sprint Retrospective, attended by Agile team members, is also held at the end of each Sprint with feedback used as input to the next Sprint.  Scope is managed by the Agile teams within each Sprint.</p>

                        <h4>Sprint Level Execution Steps</h4>
                        <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activities</th>
                                        <th>Responsible / Participants</th>
                                        <th>Artifacts</th>
                                        <th>Reference Documents</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>1.	Conduct the Sprint activities according to the Sprint Plan.  </strong></p>
                                                <p>These activities include refining the requirements, completing the detailed solution design, unit testing, documenting test results, design documentation, completion of functional and technical specifications (begun in the FRS Phase), and approvals.  These activities are described in individual activities below.</p>
                                            </td>
                                            <td><strong>SM</strong>, All Agile Team Members</td>
                                            <td>Described in individual activities below.</td>                                    
                                            <td>Product Backlog / Sprint Plan / Master Project Schedule / Resource Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>2.	Track progress of the Sprint Plan:  Sprint and Release Burn-Down; Product Backlog Grooming.  </strong></p>
                                                <p>(NOTE:  Jira software is used to track and monitor the progress.) Development and configuration activities and completions are tracked by the Agile Coach and the Scrum Masters in each Agile team, using Jira, to ensure that the team progresses as per the Sprint Plan</p>
                                            </td>
                                            <td><strong>SM</strong> / Agile Coach / PO / SA / PM </td>
                                            <td>Updated Product Backlog / Updated Sprint Plan / Updated Release Burn-down / Sprint Retrospective </td>                                    
                                            <td>Product Backlog / Sprint Plan / Release Burn-down</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>3.	Hold Daily Scrum (Stand up) Meetings. </strong></p>
                                                <p>Agile team members participate in a daily Scrum (stand-up) meeting to identify risks, share knowledge, and review tasks in progress</p>
                                            </td>
                                            <td><strong>Agile Teams </strong></td>
                                            <td>Updated Product Backlog / Updated Sprint Plan / Updated Release Burn-down</td>                                    
                                            <td>Product Backlog / Sprint Plan / Release Burn-down</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>4.	Hold Weekly Scrum of Scrum Meetings.  </strong></p>
                                                <p>All Agile team members participate in a weekly Scrum of Scrum Meeting to discuss cross-team issues and solutions. </p>
                                            </td>
                                            <td><strong>Scrum Masters</strong>, PO / Agile Coach / Functional Leads / Lead Developers / PM</td>
                                            <td>Updated Product Backlog / Updated Sprint Plan / Updated Release Burn-down</td>                                    
                                            <td>Product Backlog / Sprint Plan / Release Burn-down</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>5.	Prepare / Update Functional / Technical Specification Documents for Development Objects</strong></p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Preparation of the Functional / Technical Specifications based on the Business Process Reference Documents, the Product Backlog, and the collaborative work during the Sprints.  The specification is for Development Objects with the details of the purpose and use of the object with applicable test cases, together with the logic to be used to develop the object</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The review and the approval of the Functional / Technical Specification is done during the Sprint as the Agile teams are collaboratively defining and building the solution.</p>
                                            </td>
                                            <td><strong>Functional Leads / Technical Leads / SA / TA</strong> / Scrum Masters / FC / TC / BPE / PO</td>
                                            <td>Functional Specification Document for Development Objects (Conversion, Enhancement, Interface, Reports and Forms) / Updated FRS Business Process Reference Documents</td>                                    
                                            <td>Product Backlog / FRS Business Process Reference Documents / Functional Unit Test Results / Minutes of FRS Workshops and Deep Dive Sessions / FS Approval Procedure </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>6.	Complete the Required Configuration</strong></p>
                                                <p>This activity is related to configuration of the required objects:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Configurations start with the Organizational Structure</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	With the Organizational structure in place, the other objects within each module are configured</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The configuration objects are defined based on the Business Process Reference Documents related to each module</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The Product Owner and Functional Leads create and use the Product Backlog, Sprint Plan, and Jira system Release Burn-Down Charts to monitor the progress and to provide updates to the PM.   </p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The PM updates the detailed Master Project Schedule with the status of the configuration work packages that is given to them by the FL’s. The TRs created as a result of Configurations are updated into the TR Tracking Sheet</p>
                                            </td>
                                            <td><strong>Functional Leads / Technical Leads / SA / TA</strong>FL / FC / SM / PO </td>
                                            <td>Updated Product Backlog / Updated Sprint Plan / TR Tracking Sheet / Updated Business Process Reference Documents / Updated Sprint Plan / Updated Product Backlog / Updated Jira system Burn Down Charts</td>                                    
                                            <td>FRS Business Process Reference Documents </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>7.	Build Development Objects </strong></p>
                                                <p>Within this activity the following takes place: </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Once the Agile Team collaboratively agrees upon the Functional Specification, the TS is prepared (this is only for development objects)</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The TS is then reviewed by the TL</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	With the review, certain updates may take place in the TS</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	With the FS and TS complete, the development takes place in the development client in the Development box for all the reports, interfaces, conversion tools, Enhancements and Forms</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The Jira system is used to track the progress of the developments</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	All relevant Stakeholders are updated on the progression of the Agile Build process</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The Transport Requests generated during the build are captured using the Transport Tracking Sheet</p>
                                            </td>
                                            <td><strong>TL</strong>, PO, PM / SA / FL / FC / TC / All relevant stakeholders from Customer </td>
                                            <td>Functional and Technical Specification Documents / Updated Product Backlog / Updated Sprint Plan,  / TR Tracking Sheet / Project Status Tracking Sheet / Updated Business Process Reference Documents </td>                                    
                                            <td>Product Backlog, Sprint Plan,  / Business Process Reference Documents </td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>8.	Identify the Batch Jobs</strong></p>
                                                <p>Batch jobs are identified during the Sprints and are added to the Batch Schedule.  All Batch Jobs identified are unit tested during Sprint Execution.</p>
                                            </td>
                                            <td><strong>PO</strong> / FL / TL / FC / TC</td>
                                            <td>Batch Job Schedule</td>                                    
                                            <td>Master Business Processes / End-to-End Business Scenarios / User Stories / Product Backlog / IST Sprint Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>9.	Perform Functional Unit Testing and User Story Testing </strong></p>
                                                <p>With this activity, the project team would:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Complete the Unit testing for all User Stories (the completed configuration work packages and the development objects) following the test scripts written in the Functional/Technical Specifications </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The Unit Testing client in the development box is used for this purpose</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Any issues identified are tracked using an Issue Log and are rectified</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Screen shots and other results information for each work package is documented in the Functional Unit Test Results and User Story Acceptance Test Results documents</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Once the testing completes with no issues, the Unit testing is considered complete</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Update all relevant stakeholders on the progression of Unit testing</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	FL and TL release the related TRs to QA after verification that the Business Process Reference Documents  and Functional/Technical Specifications are updated with all CR’s and refinements of the solution during the Agile Build Phase</p>
                                            </td>
                                            <td><strong>PO</strong>, Scrum Masters, FL / TL / FC / TC / BT / PO / BPE’s, IT Lead from Customer</td>
                                            <td>User Story Acceptance Test Results /  Functional Unit Test Results / Unit Testing Issue Log </td>                                    
                                            <td>Functional and Technical Specification Documents / Product Backlog / Sprint Plan </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>10.	Perform String Testing for all interface objects</strong></p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The string testing is done in the Unit testing client in the development system.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Screen shots and other results information for each interface is documented in the Functional Unit Test Results and User Story Acceptance Test Results documents</p>
                                            </td>
                                            <td><strong>PO</strong> / SM / FL / TL / FC / TC / BT</td>
                                            <td>Unit Testing Issue Log / Unit Test Results document</td>                                    
                                            <td>Approved Functional and Technical Specification Documents / Product Backlog / Sprint Plan </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>11.	End of Sprint Review Meeting, Demo, and Sign-Off</strong></p>
                                                <p>The End of Sprint Review Meeting is held at the close of the Sprint. This includes the Sprint demo of the work completed during the Sprint. A complete review of the Sprint is conducted, along with approval and formal sign-off by the Product Owner.  The results of the End of Sprint Review are reported to the Steering Committee and project stakeholders.</p>
                                            </td>
                                            <td><strong>PO</strong> / All Agile Build Team Members</td>
                                            <td>Minutes of End of Sprint Review Meeting / Sprint Retrospective Document</td>                                    
                                            <td>Product Backlog / Sprint Plan / User Story Acceptance Test Results / Functional Unit Testing Results</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>12.	Validate Sprint Plans for Sprints 2 – X</strong></p>
                                                <p>After each Sprint, the Sprint Plan for the next Sprint is validated and updated based upon the activities completed and validated during the previous Sprint.</p>
                                            </td>
                                            <td><strong>SM</strong> / PO / PM / BPEs / FL / TL </td>
                                            <td>Updated Sprint Plan</td>                                    
                                            <td>Sprint Plan / Product Backlog</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Sprint Level Execution</strong></td>
                                            <td><p><strong>13.	End of Sprint Retrospective </strong></p>
                                                <p>The Sprint Retrospective is an opportunity for the Agile teams perform a self-review of the Sprint, and create a plan for improvements to be made during the next Sprint. The Sprint Retrospective occurs after the Sprint Review and Demo and prior to the next Sprint.  The Scrum Master ensures that the event takes place and that attendees understand the purpose.  All members of the Agile teams should be in attendance.</p>
                                            </td>
                                            <td><strong>SM</strong>, Agile Team Members</td>
                                            <td>Sprint Retrospective Document</td>                                    
                                            <td>Sprint Plan / Product Backlog</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Sprint Level Execution Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	User Stories Complete</p>
                                            <p>•	Gap List and Development List Complete (for reference)</p>
                                            <p>•	Completed / Groomed Product Backlog  (User Stories (work packages), Configuration objects, Development Objects)</p>
                                            <p>•	Sprint Planning for entire project completed, including all rollouts.</p>
                                            <p>•	Sprint 1 Preparation completed during the FRS Phase </p>
                                            <p>•	Sprint Monitoring and Reporting software platform selected, installed, and configured (i.e., Jira, etc.).</p>
                                            <p>•	Agile Teams Onboarding and Training for Agile Build and Unit Test completed</p>
                                            <p>•	Development Environment is ready for configuration, development, and unit testing</p>
                                        </div>
                                            
                                    </td>
                                        <td>
                                    <h3>Sprint Level Execution Exit Criteria</h3>
                                        <div class="data">
                                                <p>•	All configuration and development complete and unit tested/accepted by users – interfaces string tested.  Defects fixed and retested.</p>
                                                <p>•	FRS Business Process Reference documents updated with detailed design.</p>
                                                <p>•	Complete, updated, and approved Functional and Technical Specification Documents</p>
                                                <p>•	Sprint Retrospectives (used as input to each of Sprint 2 – X)</p>
                                                <p>•	End of Sprint Review Meetings; any User Stories not completed in a Sprint are identified and included in the Product Backlog for future planning</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Sprint Level Execution Input Documents</h3>
                                        <div class="data">
                                                <p>•	Product Backlog </p>
                                                <p>•	FRS Business Process Reference Documents</p>
                                                <p>•	attune Development Standards & Guidelines documents</p>
                                                <p>•	Sprint Plan (at the beginning of Sprint 1)</p>
                                                <p>•	Sprint Retrospectives (created at the end of each Sprint, and input to Sprint 2 through Sprint X)</p>
                                        </div>
                                    </td>
                                        <td>
                                    <h3>Sprint Level Execution Artifacts Created and Updated</h3>
                                        <div class="data">

                                            <p>•	Updated Functional / Technical Specification documents</p>
                                            <p>•	Functional Unit Test Results</p>
                                            <p>•	Updated FRS Business Process Reference Documents </p>
                                            <p>•	Updated Sprint Plan</p>
                                            <p>•	Completed User Stories</p>
                                            <p>•	User Story Acceptance Test Results</p>
                                            <p>•	Minutes of End of Sprint Review Meetings</p>
                                            <p>•	Sprint Retrospective Documents</p>

                                        </div>
                                    </td>
                                    </tr>
                                                                                              
                                </tbody>
                                </table>
                            </div>
                           
                        </div>
                        
                        <h4>Mock Data Migration Execution</h4>
                        <p>Mock Data Migration Execution includes all activities related to data conversion activities performed in the Agile Build and Unit Test Phase.  Once development is complete for the data migration User Stories, the Data Migration team will begin activities related to Mock 0 Migration in the Development environment.  Once Mock 0 is complete, the Mock 1 Cutover is executed in the QA environment.</p>

                        <h4>Mock Data Migration Execution Steps</h4>

                        <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activities</th>
                                        <th>Responsible / Participants</th>
                                        <th>Artifacts</th>
                                        <th>Reference Documents</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="1"><strong>Mock Data Migration  Execution  </strong></td>
                                            <td><p><strong>1.	Conduct Mock 0</strong></p>
                                                <p>The Mock 0 is the unit/string test of data migration programs.  It is a full data migration in the development unit test box.  Extract programs, load programs, automated validation routines, data cleansing processes and data validation processes are fully tested.  The extracts and loads are repeated for data that does not load until 100% of data is migrated or there is agreement with the customer and the PMO that it will not be loaded during the Mock 0.  Mock 1 cutover uses the same programs and data to save time.</p>
                                                <p>This step covers:</p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	The PM prepares the Mock 0 Schedule which consists of all the activities to be performed during the Mock 0</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	The PM kicks off each activity in the Mock 0 Schedule in an email and tracks the completion of the activity.</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	The Mock 0 is done in the Unit testing client in the Development box to test the Data Migration programs developed as part of the Product Backlog </p>
                                                    <p class="tab-content" style="padding-left: 20px">d.	A full set of legacy system production data as of the end of a fiscal month is used for this purpose.  A copy of the legacy production system is taken and installed on a test database with limited access (i.e. frozen database).  This frozen database is used exclusively to extract data for the Mock 0 and the subsequent Mock 1 Cutover.</p>
                                                    <p class="tab-content" style="padding-left: 20px">e.	The data to be migrated is extracted from the frozen database  or manually prepared in the formats required for the Data Conversion Tools (the same tools used during Mock Cutover 1 during the Build phase) </p>
                                                    <p class="tab-content" style="padding-left: 20px">f.	The extracted data is reviewed and validated by the relevant stakeholders from the customer.  Issues reported are tracked and resolved.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                    <p class="tab-content" style="padding-left: 20px">g.	The data is then uploaded into the unit test client starting with the master data and then moving to the transactional data.</p>
                                                    <p class="tab-content" style="padding-left: 20px">h.	The uploaded data is validated by the relevant customer stakeholders using the Data Validation Procedures.</p>
                                                    <p class="tab-content" style="padding-left: 20px">i.	The process is repeated for data that cannot be loaded until all of the data has either been loaded or exceptions are approved by the PMO.</p>
                                                    <p class="tab-content" style="padding-left: 20px">j.	Any issues faced are captured and resolved.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                            </td>
                                            <td><strong>PM</strong> /SA / FL / TL / FC / TC</td>
                                            <td>Data Migration Templates  / Unit Testing Issue Log </td>                                    
                                            <td>Functional and Technical Specification Documents / Product Backlog / Sprint Plan </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Mock Data Migration Execution  </strong></td>
                                            <td><p><strong>2. Set up the Quality Environment</strong></p>
                                                <p>During the Agile Build Phase, the quality environment is prepared according to the approved Client Eco System Architecture.</p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	With the exception of unit testing, all testing takes place in the quality environment.   Separate clients are set up for each cycle of Integration Testing.  Regression testing and Performance/Stress Testing shares the INT 2 client.</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	3rd party test systems and middleware are connected to the SAP test clients.</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	The INT 1 test client is set up first.  It is used during the Build Phase for Mock 1 cutover.  All other clients are required for the Test Phase.</p>
                                            </td>
                                            <td><strong>PM</strong> / Basis Consultant / SAP Security </td>
                                            <td>Updated System Landscape Diagram / Updated Customer Ecosystem Architecture</td>                                    
                                            <td>Project Charter / Project Kick Off Document, Transport Management Procedures / Customer Ecosystem Architecture / System Landscape Diagram</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Mock Data Migration Execution  </strong></td>
                                            <td><p><strong>3. Conduct Mock 1 Cutover</strong></p>
                                                <p>The Mock 1 Cutover tests the set-up of the system environment with migrated data.  Mock 1 cutover uses the same programs and data used in the Mock 0.  It is a full data migration to the Quality environment.  100% of the test data is loaded.  Exceptions are approved by the PMO.   Migrated data is used for INT 1.  Time estimated to do Mock 1 Cutover is approximately half of the Mock 0 duration.</p>
                                                <p>In this step:</p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	The PM prepares the schedule to conduct the mock cutover with all the required activities maintained</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	The PM kicks off each activity in the Mock Cutover Schedule in an email and tracks the completion of the activity.</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	The INT 1 quality client is built by the Basis Team and released to the DM Team.</p>
                                                    <p class="tab-content" style="padding-left: 20px">d.	All released TRs are moved to the INT 1 Quality system so that all required configurations and developments are moved</p>
                                                    <p class="tab-content" style="padding-left: 20px">e.	The FC and TC perform required set up activities in the INT 1 quality client.</p>
                                                    <p class="tab-content" style="padding-left: 20px">f.	The selected Legacy system data (used for Mock 0) which is in the required template/format is used once again to load the data into the INT 1 client starting with the master data and then moving to the transactional data.</p>
                                                    <p class="tab-content" style="padding-left: 20px">g.	The migrated data is reviewed and validated in the INT 1 quality environment by the relevant stakeholders from the customer.   Issues reported are tracked and resolved.   The goal is to accurately load 100% of the legacy data.  Exceptions are approved by the PMO.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                    <p class="tab-content" style="padding-left: 20px">h.	The FC and TC perform any required post data migration set up activities in the INT 1 quality client.</p>
                                                    <p class="tab-content" style="padding-left: 20px">i.	The middleware and 3rd party system cutover activities are performed and validated.</p>
                                                    <p class="tab-content" style="padding-left: 20px">j.	Once it is confirmed that everything is in order, the Dry Run or Mock Cutover 1 is considered complete and the INT 1 quality environment ready for conducting the preliminary end-to-end process testing performed at the beginning of the Realization - Test Phase.</p>
                                                    <p class="tab-content" style="padding-left: 20px">k.	The status and the results of the Mock 1 cutover are shared daily with all relevant stakeholders of the customer, the customer’s core project team and, if required, the customer’s and attune management team.</p>
                                                    <p class="tab-content" style="padding-left: 20px">l.	The actual time taken to complete Mock 1 cutover activities is captured.  The Mock 2 schedule will be based on these times.</p>
                                            </td>
                                            <td><strong>PM</strong> /FL / TL / FC / TC</td>
                                            <td>Mock Cutover Schedule </td>                                    
                                            <td>Date Migration Templates</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Mock Data Migration Execution Entry Criteria </h3>
                                        <div class="data">
                                            <p>•	All configuration and development complete for data migration User Stories</p>
                                            <p>•	Development for all User Stories related to Data Migration completed and unit tested</p>
                                            <p>•	QA Environment is complete for Mock 1</p>
                                        </div>
                                            
                                    </td>
                                        <td>
                                    <h3>Mock Data Migration Exit Criteria</h3>
                                        <div class="data">
                                              <p>•	Mock 1 Data Migration Complete</p>
                                              <p>•	100% of Mock 0 data is converted, exceptions approved by PMO</p>
                                              <p>•	100% of Mock 1 Cutover completed, exceptions approved by PMO</p>  
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Mock Data Migration Execution Input Documents</h3>
                                        <div class="data">
                                            <p>•	Functional / Technical Specification Documents for Data Migration</p>
                                            <p>•	Updated Product Backlog</p>
                                            <p>•	Transport Management Procedures</p>
                                            <p>•	Updated Integrated System Landscape Diagram</p>
                                            <p>•	Data Migration Templates</p>
                                            <p>•	Mock Cutover Schedules</p>
                                        </div>
                                    </td>
                                        <td>
                                    <h3>Mock Data Migration Execution Artifacts Created and Updated</h3>
                                        <div class="data">

                                            <p>•	Mock 0 Schedule</p>
                                            <p>•	Mock 1 Cutover Schedule</p>
                                            <p>•	Validated Mock 1 Data Migration</p>
                                            <p>•	Data Cleansing and Validation Plan</p>
                                            <p>•	Updated Project Status Tracking Sheet</p>

                                        </div>
                                    </td>
                                    </tr>
                                                                                              
                                </tbody>
                                </table>
                            </div>
                           
                        </div>

                        <h4>Business Transformation Execution</h4>
                        <p>Foundational activities for Business Transformation are performed in the Agile Build and Unit Test Phase.  These activities include those related to Change Management, Stakeholder Analysis, IST Phase planning, Testing Coordination, and End User Training preparation.</p>
                        
                        <h4>Business Transformation Execution Steps</h4>

                        <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activities</th>
                                        <th>Responsible / Participants</th>
                                        <th>Artifacts</th>
                                        <th>Reference Documents</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>1.	Capture additional change impacts. </strong></p>
                                                <p>BPE’s and the PO collaborate with functional and technical consultants during Agile Build and Test Phase.  Any changes to the design, or addition or deletion of user stories should be noted in the change impact assessment.</p>
                                            </td>
                                            <td><strong>BT</strong> / BPE’s / PO</td>
                                            <td>Updated Change Impact Assessment</td>                                    
                                            <td>Change Impact Assessment / FRS Business Process Reference Documents</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>2.	Prepare end to end Business Scenarios required for Integration Testing</strong></p>
                                                <p>BPE’s and the PO collaborate with functional and technical consultants during Agile Build and Test Phase.  Any changes to the design, or addition or deletion of user stories should be noted in the change impact assessment.</p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	The end to end scenarios to be used for Integration Testing are defined</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	End-to-End Business scenarios are validated and confirmed by the relevant stakeholders from the customer</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	The scenarios are documented.</p>
                                            </td>
                                            <td><strong>BT</strong> / BPE’s / PO</td>
                                            <td>Integration Test Scenarios </td>                                    
                                            <td>Business Process Reference Document / Product Backlog / Sprint Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>3.	Prepare Integration Testing Script Development Tracker</strong></p>
                                                <p>This tracker is used to list out the business scenarios and variants for which scripts are to be created. The tracker lists the owner of the script and target dates for completing each step in the development and approval process.</p>
                                            </td>
                                            <td><strong>PO</strong> / SM / FL / TL / FC / TC / BT</td>
                                            <td>Integration Test Script Development Tracker</td>                                    
                                            <td>Integration Test Scenarios / Integration Test Scripts / Business Process Reference Document, Product Backlog, Sprint Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>4. Prepare IST User Stories and Test Scripts</strong></p>
                                                <p>End-to-End Business Scenarios are used to test that the solution is working as per the Agile Build phase. All business processes, reports, and forms are tested.  All Interfaces are tested.</p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	For IST, a User Story (from Agile Build) is a series of steps within an End-to-End Business scenario that belongs to one Product Owner.</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	User stories within the End-to-End Business Scenarios are identified</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	Test scripts are developed to execute the user stories</p>
                                                    <p class="tab-content" style="padding-left: 20px">d.	All User Stories are organized for testing efficiency and for minimizing repetition of steps that have already passed.</p>
                                                    <p class="tab-content" style="padding-left: 20px">e.	User Stories are sequenced so that output from one User Story will be used as inputs to one or more successor User Stories</p>
                                                    <p class="tab-content" style="padding-left: 20px">f.	User Stories are reviewed to ensure that all testing requirements and build objects are covered in at least one User Story</p>
                                            </td>
                                            <td><strong>TSC</strong> / PM  / FL / TL / FC / TC / BTS / Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>IST User Stories / Integration Test Scripts / Updated Integration Test Script Development Tracker</td>                                    
                                            <td>End-to-End Business Scenarios / User Stories from Agile Build </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>5. Prepare Integration Testing Backlog and Sprint Plan</strong></p>
                                                <p>The following activities are completed here:</p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	Test Scripts are created that cover each business scenario and all User Stories from the Product Backlog</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	The required Master data to be used for the completion of the test scripts is identified</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	The testing team is identified and confirmed from the customer for Integration Testing</p>
                                                    <p class="tab-content" style="padding-left: 20px">d.	All End-to-End Business Scenarios and User Stories will be planned into 1 week Sprints.  The Sprint Plan identifies dependencies between user stories to ensure proper sequence of execution.</p>
                                                    <p class="tab-content" style="padding-left: 20px">e.	User Stories are assigned to IST teams for execution.</p>
                                                    <p class="tab-content" style="padding-left: 20px">f.	Prepare the detailed testing schedule (i.e. by day, by script, by resource).  Adjust resources to fit within the testing timeline or get PMO approval to adjust the testing timeline.</p>
                                                    <p class="tab-content" style="padding-left: 20px">g.	Update the Baseline Project Schedule with the high-level testing timeline.</p>
                                            </td>
                                            <td><strong>TSC</strong> / PM  / FL / TL / FC / TC / BTS / Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Finalized Integration Testing Sprint Plan /  Finalized Integration Test Scripts / Updated Baseline Project Schedule/ Integration Testing Preparations Status Report</td>                                    
                                            <td>End-to-End Business Scenarios / FRS Process List / FRS Business Process Reference Documents  / To-Be Business  Documents with process flow diagrams / Updated Product Backlog</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>6. Finalize Defect Management Process</strong></p>
                                                <p>The Testing Defect Management process is reviewed with the customer, tailored, if required, and finalized.</p>
                                            </td>
                                            <td><strong>TSC</strong> / BTS / Customer’s Core Project team </td>
                                            <td>Defect Management Process document</td>                                    
                                            <td>Defect Management Process template</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>7. Finalize Testing Status Report format</strong></p>
                                                <p>The daily integration testing status report format is tailored and finalized. </p>
                                            </td>
                                            <td><strong>TSC/BTS </strong></td>
                                            <td>Testing Status Report</td>                                    
                                            <td>Testing Strategy Document</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td><p><strong>8. Prepare End User Training Development Process, Standards, and Templatest</strong></p>
                                                <p>During the Agile Build and Unit Test phase, preparation for developing End User training begins. Preparation includes the documenting of the development process and standards for the training materials. Customization of the templates for the various documents is completed.</p>
                                            </td>
                                            <td><strong>TC / BTS </strong></td>
                                            <td>End User Training Development Processes, Standards, and Templates</td>                                    
                                            <td>FRS Process List / FRS Business Process Reference Documents / End-to-End Business Scenarios / User Stories / Gap List </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>9. Prepare Tester Training Schedule</strong></p>
                                                <p>With the testing team from the customer identified and confirmed, the project team needs to work together with the customer core project team in order to train the testing team so that they will be able to follow the script and complete the testing. Therefore, both teams work together and prepare a training schedule which is later ratified and agreed upon with the relevant stakeholders from the customer.</p>
                                                <p>Training topics will be adjusted based upon the carryover of Agile team members from the Agile Build and Unit Test phase to the IST Phase.  If the Agile teams move into the IST Phase, training may be reduced.  The planned training would cover: </p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	SAP Navigation</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	The business processes covered within each SAP module (focusing mainly on the end to end business scenarios identified for Testing)</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	The features and functions within each business process</p>
                                                    <p class="tab-content" style="padding-left: 20px">d.	The concept of master data and how it is used</p>
                                                    <p class="tab-content" style="padding-left: 20px">e.	The concept of the transactional data and how transactions are entered in SAP for each process together with the main SAP transaction needed for the completion of the testing</p>
                                                    <p class="tab-content" style="padding-left: 20px">f.	Hands on training with access to SAP system (In general it is the Unit Testing client that is used for this purpose)</p>
                                                <p>Once the training schedule is in place, the detailed project schedule is updated to derive the timelines required to complete the training</p>
                                            </td>
                                            <td><strong>PM</strong>  / FL / TL / FC / TC / BTS / Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Finalized Training Schedule / Updated Detailed Project Schedule</td>                                    
                                            <td>Testing Schedule /  Test Scripts / To-Be Business Documents with process flow diagrams / FRS  Business Process Reference Documents </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>10. Prepare Tester Training Materials</strong></p>
                                                <p>This activity is involved in preparing the training manuals required for the training of the testing team to be involved in Integration Testing. The training materials can consists of MS Power Point presentation slides and/or MS Word/PDF documents with screen shots displaying </p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	To-be business processes</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	SAP Navigation</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	The different transactions used to maintain Master data required for an end to end business scenario</p>
                                                    <p class="tab-content" style="padding-left: 20px">d.	The different transactions used to maintain transactional data for an end to end business scenario</p>
                                                    <p class="tab-content" style="padding-left: 20px">e.	Simulations and Business Process Procedures can also be used if they are available.</p>
                                                    <p class="tab-content" style="padding-left: 20px">f.	The process and tools used for executing integration testing are also presented.</p>
                                            </td>
                                            <td><strong>FL / FC / BTS</strong> / Customer Core Project team</td>
                                            <td>All required Training Materials</td>                                    
                                            <td>Training Schedule / Test Scripts / To-Be Business Documents with process flow diagrams / FRS  Business Process Reference Documents</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>11. Prepare End User Training Curriculum and Course Outlines</strong></p>
                                                <p>The End User Training curriculum lists the courses that will be taught, the owner for each course and the business roles that will take each course. The curriculum list will become the basis for the development tracker. A course outline is written for each course listed in the curriculum.</p>
                                            </td>
                                            <td><strong>TM</strong> / Customer Core Project Team</td>
                                            <td>End User Training Curriculum</td>                                    
                                            <td>Business Process Reference Documents / BPML / Change Impact assessment</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>12. Prepare the End User Training development tracker and course material templates</strong></p>
                                                <p>Based on the curriculum list, a development tracker is created to track the status of developing the required end user training materials for each course</p>
                                                <p>Templates are created based on the specific project standards and customer logos and presentation standards.</p>
                                            </td>
                                            <td><strong>TM</strong></td>
                                            <td>Training Development tracker and course material templates</td>                                    
                                            <td>Training development standards and templates</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>13. Communication Effectiveness Check</strong></p>
                                                <p>The Business Transformation Team will develop a set of key questions to ascertain the effectiveness of the communication channels. Based on feedback, additional adjustment will be made to the integrated communication plan</p>
                                            </td>
                                            <td><strong>BTS</strong> / Customer’s Core Project Team</td>
                                            <td>Communication Effectiveness Survey</td>                                    
                                            <td>Integrated Communication Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>14.	Complete Scheduled communication events according to the Integrated Communications Plan</strong></p>
                                            </td>
                                            <td><strong>BTS</strong></td>
                                            <td>Updated Integrated Communications Plan</td>                                    
                                            <td>Integrated Communications Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Business Transformation Execution </strong></td>
                                            <td><p><strong>15. Prepare Business Role descriptions and mapping to security roles</strong></p>
                                                <p>BTS will work with Business Process Owners to define both existing and new business roles. Once the business roles are defined and approved, the security team will define security roles and the authorizations schema. BTS may facilitate the discussion between the security team and the business process owners.</p>
                                            </td>
                                            <td><strong>BTS</strong> / BPO / Security</td>
                                            <td>Business Role Mapping Document</td>                                    
                                            <td>BPML / Business Process Reference Documents / Change Impact Assessment</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Business Transformation Execution Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Completed Change Impact Assessment and Heat Map</p>
                                            <p>•	Updated Project Status Tracking Sheet</p>
                                        </div>
                                            
                                    </td>
                                        <td>
                                    <h3>Business Transformation Execution Exit Criteria</h3>
                                        <div class="data">
                                             <p>•	Test preparation complete</p>
                                             <p>•	Approved security roles required for integration testing ready for IST2</p>
                                             <p>•	Approved batch schedule ready for testing in IST2</p>
                                             <p>•	Completed Integrated Testing Sprint Plan and Scripts</p>
                                             <p>•	Completed Integration Testing Training Materials</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Business Transformation Execution Entry Documents</h3>
                                        <div class="data">
                                            <p>•	Training Development Process, Standards and Templates</p>
                                            <p>•	Change Impact Assessment</p>
                                            <p>•	Integrated Communication Plan</p>
                                        </div>
                                    </td>
                                        <td>
                                    <h3>Business Transformation Execution Artifacts Created and Updated</h3>
                                        <div class="data">
                                            <p>•	Integration Testing Training Materials</p>
                                            <p>•	Communication effectiveness survey</p>
                                            <p>•	End User ILT Course Presentation</p>
                                            <p>•	End User Training Curriculum with Calendar Option</p>
                                            <p>•	End User Training Development Process, Standards, and Templates</p>
                                            <p>•	End User Training Course Outlines</p>
                                            <p>•	End User Training Curriculum</p>
                                            <p>•	Integration Testing Training Schedule</p>
                                            <p>•	Integration Test Scenarios</p>
                                            <p>•	Integration Test Scripts</p>
                                            <p>•	Integration Testing Script Development Tracker</p>
                                            <p>•	Integration Testing Schedule</p>
                                            <p>•	Business Role Mapping Design</p>
                                            <p>•	Updated Integrated communication plan</p>

                                        </div>
                                    </td>
                                    </tr>
                                                                                              
                                </tbody>
                                </table>
                            </div>
                           
                        </div>

                        <h4>Phase Exit Execution</h4>
                        <p>Phase Exit Execution includes all high-level activities required to complete the Agile Build and Unit Test Phase and proceed to the IST Phase (Pilot).  </p>

                        <h4>Phase Exit Steps</h4>

                        <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activities</th>
                                        <th>Responsible / Participants</th>
                                        <th>Artifacts</th>
                                        <th>Reference Documents</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="1"><strong>Phase Exit Execution</strong></td>
                                            <td><p><strong>1.	Update Project Status Tracking Sheet.</strong></p>
                                                <p>The Project Status Tracking Sheet is updated to document all issues, change requests, assumptions, risks, and decisions through the end of the Phase. </p>
                                            </td>
                                            <td><strong>PM</strong></td>
                                            <td>Updated Project Status Tracking Sheet </td>                                    
                                            <td>Minutes from Sprint Scrum of Scrum Meetings / Product Backlog / Sprint Plan / Release Burn-down </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Phase Exit Execution</strong></td>
                                            <td><p><strong>2.	Update Master Project Schedule.</strong></p>
                                                <p>The Master Project Schedule is updated through the end of the Phase.  Any deviations to the Schedule are reported to the Steering Committee.</p>
                                            </td>
                                            <td><strong>PM</strong></td>
                                            <td>Updated Master Project Schedule</td>                                    
                                            <td>Minutes from Sprint Scrum of Scrum Meetings / Product Backlog / Sprint Plan / Release Burn-down</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Phase Exit Execution</strong></td>
                                            <td><p><strong>3.	Conduct Exit Stage Gate Review.</strong></p>
                                                <p>This is the review with the Steering Committee of the Agile Build and Unit Test Phase exit criteria and entrance criteria for the next Phase.  During this review the PMO is seeking approval to move to the IST Phase. </p>
                                            </td>
                                            <td><strong>PM</strong> / all Project Team Members / Steering Committee Members</td>
                                            <td>Exit Stage Gate Review Presentation</td>                                    
                                            <td>Updated Master Project Schedule / Updated Project Status Tracking Sheet / Minutes of weekly Agile Team meetings</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Phase Exit Execution</strong></td>
                                            <td><p><strong>4.	Store all documents created and updated by attune during the Phase on ONEattune in the Project workspace</strong></p>
                                                <p>It is important to retain attune intellectual property on ONE attune.  This activity applies to every member of the team.  It includes documents used for an activity (e.g. FRS Workshop presentations) as well as deliverable artifacts.  </p>
                                            </td>
                                            <td>All attune Project Team Members</td>
                                            <td>Updated ONEattune Project Work Space</td>                                    
                                            <td>All documents used for an activity (e.g. FRS Workshop presentations) as well as deliverable artifacts.  </td>
                                        </tr>
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Phase Exit Criteria </h3>
                                        <div class="data">
                                            <p>•	Updated Integration Landscape Diagram with changes related to approved Change Requests and refinement of the solution during the Build Phase</p>
                                            <p>•	QA environment ready for Integration Testing with all configurations and developments, including SAP, middleware, EDI, and Third Party changes, if required.</p>
                                            <p>•	Completed Exit Stage Gate Review with all items passed and exceptions approved by the Steering Committee</p>
                                        </div>
                                            
                                    </td>
                                        <td>
                                    <h3>Phase Exit Artifacts Created and Updated</h3>
                                        <div class="data">
                                             <p>•	End User Solution Showcase schedule</p>
                                             <p>•	Updated Master Project  Schedule</p>
                                             <p>•	Updated Project Status Tracking Sheet</p>
                                             <p>•	Updated System Landscape Diagram</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <h3>Measurement of Improvement</h3>
                                            <div class="data">
                                                <p>•	Realization - Build Phase profitability</p>
                                                <p>•	Percentage of the approved FS documents (100% as a target)</p>
                                                <p>•	Percentage of Completeness of the Unit testing </p>
                                                <p>•	Effort for Peer Review VS Number of Findings</p>
                                                <p>•	Percentage of data migrated for Integration Testing</p>
                                                <p>•	Effort Variance in completion of activities in Build versus Plan</p>
                                                <p>•	Percentage of testers attending training (100% as a target)</p>
                                                <p>•	Level of training effectiveness for testers</p>
                                                <p>•	Implementation of improvements per the Sprint Retrospectives</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <h3>Tailoring</h3>
                                            <div class="data">
                                                <p>•	The project may decide to use client defined templates and standards for the deliverables from within the Agile Build phase. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>
                                                <p>•	Certain activities of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>
                                                <p>•	Certain activities of this process may be delivered in another phase of the project, as per the contractual requirements.  This tailoring is documented in the Project’s Process</p>
                                                <p>•	In case the client or another team representing the client is performing some parts of the activities and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>
                                            </div>
                                        </td>
                                    </tr>
                                                                                              
                                </tbody>
                                </table>
                            </div>
                           
                        </div>                    

                        <%--<h2>Document Templates</h2>--%>                       
                        

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <%--<h3>Functional & Technical</h3>--%>     
                                <h4>Templates</h4>
                                <p><a href="#">1.  Agile Build Kick-Off Presentation</a></p> 
                                <p><a href="References\Business_Role_Mapping.docx">2. Business Role Mapping Design</a></p> 
                                <p><a href="References\Communication Effectiveness Survey.xlsx">3. Communication effectiveness survey</a></p> 
                                <p><a href="#">4. Completed User Stories</a></p> 
                                <p><a href="References\Data Cleansing and Validation Plan Template.docx">5. Data Cleansing and Validation Plan</a></p> 
                                <p><a href="References\End User ILT Course.pptx">6.  End User ILT Course Presentation</a></p>    
                                <p><a href="#">7.  End User Solution Showcase schedule</a></p>  
                                <p><a href="References\End User Training Course Outline.docx">8. End User Training Course Outlines</a></p> 
                                <p><a href="References\End User Training Curriculum.xlsx">9. End User Training Curriculum</a></p> 
                                <p><a href="References\End User Training Curriculum With Calendar Option.xlsx">10. End User Training Curriculum with Calendar Option</a></p>
                                <p><a href="#">11. End User Training Development Process, Standards, and Templates</a></p>
                                <p><a href="#">12. Functional / Technical Specification documents</a></p>
                                <p><a href="#">13. Functional Unit Test Results</a></p> 
                                <p><a href="References\Test Scenario Description.docx">14. Integration Test Scenarios</a></p>    
                                <p><a href="References\Integration Test Script.xlsx">15. Integration Test Scripts</a></p>  
                                <p><a href="References\Testing Daily Schedule_Single Instance per Test.xlsx">16. Integration Testing Schedule</a></p>
                                <p><a href="References\Integration Test Script Development Tracker.xlsx">17. Integration Testing Script Development Tracker</a></p>
                                <p><a href="References\A Day in the Life of a Tester.pptx">18. Integration Testing Training Materials</a></p>
                                <p><a href="References\Testing Daily Schedule_Single Instance per Test.xlsx">19. Integration Testing Training Schedule</a></p>
                                <p><a href="#">20. Minutes of End of Sprint Review Meetings</a></p>
                                <p><a href="#">21. Mock 0 Schedule</a></p> 
                                <p><a href="References\Template - Mock Cutover Schedule.mpp">22.  Mock 1 Cutover Schedule</a></p>
                                <p><a href="#">23.  QA Systems and test lab ready for IST1</a></p>
                                <p><a href="#">24.  Sprint Retrospective Document</a></p>
                                <p><a href="References\Stage Gate Review.pptx">25.  Stage Gate Review Dashboard</a></p>
                                <p><a href="#">26. Testing Status Report</a></p>
                                <p><a href="#">27.  Updated Business Process Reference Documents</a></p>
                                <p><a href="References\Integrated Communication Plan.xlsx">28.  Updated Integrated communication plan</a></p>
                                <p><a href="References\Master Project Schedule.mpp">29.  Updated Master Project  Schedule</a></p>
                                <p><a href="References\Project Status Tracking Sheet.xlsm">30. Updated Project Status Tracking Sheet</a></p>
                                <p><a href="#">31. Updated Sprint Plan </a></p>
                                <p><a href="#">32. Updated System Landscape Diagram</a></p>
                                <p><a href="#">33. User Story Acceptance Test Results</a></p> 
                                <p><a href="#">34. Validated Mock 1 Data Migration</a></p>


                          </div>
<%--                          <div class="col-xs-6 col-md-4"> 
                                <h3>PMO & Testing</h3>   
                                <p><a href="https://life.oneattune.com/mlink/file/MzQ2MzY" target="_blank">1.  attune PMO Project Governance</a></p> 
                                <p><a href="https://life.oneattune.com/mlink/file/NDE4NTc" target="_blank">2.  attune ABAP development Guidelines</a></p>  
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTA">3.  attune Coding Standards Java</a></p>    
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTE">4.  attune Coding Standards Microsoft.Net</a></p> 
                          </div> --%>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>Business Transformation</h3>--%>
                                <h4>Guidelines</h4>
                                <p><a href="References\Branded aIR Diagram.pptx">1.  Branded aIR Diagram</a></p> 
                                <p><a href="References\Phase Process Summary.docx">2. Phase Process Summary</a></p> 

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>   
                                <p><a href="References\Onboarding Guide Sample.docx">1.  Onboarding Guide Sample</a></p> 
                                <p><a href="References\Project Organizational Chart Sample.pptx">2.  Project Organizational Chart Sample</a></p> 
                          </div>
                        </div>                        

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                        </div> 

                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                        </div> --%>


<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->

</asp:Content>
