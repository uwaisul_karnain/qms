﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AgileBuildingFutureReferenceSolution.aspx.cs" Inherits="attune_Document_Web.AgileBuildingFutureReferenceSolution" %>

<asp:Content ID="projectPreparationPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">--%>
                <%--<div class="row">
                    <div class="col-lg-12">

                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Preparation  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Building the Future Reference System (FRS) phase of the SAP implementation project lifecycle.  It covers the activities that need to be performed within this phase. </p>
                            <p>During the FRS phase, the existing business processes are understood, the requirements are elicited, the business impacts are evaluated, and the reference solution is demonstrated and documented.  The FRS Business Process Reference Documents are signed-off and the FRS Walkthrough Presentation is completed.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depend upon the expected deliverables and the contractual requirements.  The activities related to the FRS Phase that are defined here derive the process followed during this phase of the project.</p>
                               
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <p>A complete list of Acronyms, Abbreviations, and Definitions can be found in the document entitled “aIR Terminology”, found in the Guidelines section of the aIR Methodology portal.</p>
	

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h3>Process and Steps</h3>     
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived from PMI, SAP ASAP and SAP Activate methodologies.  aIR methodology includes elements of Agile principles for requirements gathering, development, and testing, while maintaining tried and true traditional approaches to ERP implementations.</p>
                            <p>Within the aIR methodology, the Future Reference System (FRS) phase follows the Project Preparation Phase. The activities begin immediately after the completion of the Project Preparation Phase Stage Gate sign-off.  This is the phase within which the existing business practices are understood, the requirements are elicited and business impacts are evaluated.  In addition, the Future Reference Solution is built, demonstrated and documented, the Business Process Reference documents are approved and signed off, and the FRS walkthrough is completed.</p>
                            <p>The project team (both attune and customer team members) are aligned by functional area (e.g. Order-To-Cash, Procure-to-Pay, Retail Operations, Finance). The business processes covered in each area are listed, and meetings are held as per the FRS Workshop Schedule prepared during the Project Preparation Phase.  FRS Workshops are held where the topic covers multiple modules, such as the organizational structure.  All relevant stakeholders, business process owners, heads of departments are invited to these workshops in order to review the attune Fashion Suite proven industry practices, the Future Reference System personalized to the Customer’s business processes, and to identify deviations from industry practices. . The requirements and business practices form the basis to derive the proposed solution which is then explained in detail in the Business Process Reference Documents. Deep Dive Discussions are held to identify details associated with business processes.   One of the key activities to gain approval of the proposed solution is a High Level Design Walkthrough conducted by the consultants at the end of the FRS Phase.  Other key activities are preparation of user stories and estimates of effort, creation of the product backlog, and sprint planning.</p>
                            <p>The Project Status Tracking Sheet is used to document and track the Gaps, Issues, Changes, Risks, Assumptions, Decisions, and Action items in this phase as well as in the subsequent phases that follow.</p>
                            <p>Preparation activities for the Agile Build Phase, such as the creation of the development environment and alignment on development standards and methodology, are carried out by the technical team while the FRS Workshops are carried out.  It is in this phase that Agile teams are formed, comprised of the Scrum Master (SM), Product Owner (PO), Functional Lead and Functional Consultants, Developers, EDI translator, data migration technical consultants, Business Process Experts, and Third Party Integrators.  The Product Backlog, User Stories, Sprint Plan, and Business Process References Documents created in this phase form the basis for the subsequent activities to be completed in the Agile Build phase in Realization. </p>
                            <p>During the FRS phase, BTS team members participate in these workshops to provide guidance and input in identifying the change impacts associated with the proposed solution per functional/organizational areas.  Typically, a BTS team member may cover 2 functional areas and work closely with customer team members to capture the relevant change impacts ultimately leading to the creation of the Change Impact Assessment document.</p>
                            <p>The Change Impact Assessment documents any changes resulting from a deviation from the “AS-IS” to the “TO-BE” process and is typically categorized into People, Process and or Technology.  This document becomes the foundational document that feeds into the creation of all Business Transformation activities.  </p>

                            <h4>Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activities</th>
                                        <th>Responsible / Participants</th>
                                        <th>Artifacts</th>
                                        <th>Reference Documents</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="1"><strong>FRS Preparation</strong></td>
                                            <td><p><strong>1.	Business Process Owners collect current process information</strong></p></td>
                                            <td><strong>BPO</strong> / All relevant stakeholders</td>
                                            <td>As-Is Business  Process Documents with process flow diagrams (prepared by Customer)</td>                                    
                                            <td></td>
                                        </tr>
                                       
                                         <tr>
                                            <td rowspan="1"><strong>FRS Preparation</strong></td>
                                            <td><p><strong>2.	Perform Stakeholder and Project Team Training on attune aIR Methodology for activities in the FRS Phase</strong></p>
                                            <p>This training is specific to Agile principles to be used in the FRS Phase.  Principles to be covered in this training include processes required for preparation and estimation of User stories, creation of the Product Backlog and estimating, Sprint Planning, and training on tools to facilitate Sprint monitoring (Jira, etc.)</p></td>
                                            <td><strong>Agile Coach</strong> / All Stakeholders and Project Team Members</td>
                                            <td>Introduction to Agile principles used during the FRS Phase</td>                                    
                                            <td>Phase Process Documents / aIR Methodology Templates / aIR Guidelines / aIR Samples</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="17"><strong>FRS Preparation</strong></td>
                                            <td><p><strong>3.	Update FRS Workshop presentations with customer-specific details to make the workshops relevant to the customer.</strong></p></td>
                                            <td><strong>PM</strong> / SA / FL / TL / FC / TC / Customer Project team members / All relevant stakeholders/ BTS</td>
                                            <td>Updated FRS Workshop Presentations / Project Status Tracking Sheet </td>                                    
                                            <td>As-Is Business  Process Documents with process flow diagrams /  FRS  Workshop Schedule / FRS Workshop Tracker / FRS Workshop Presentations</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>4.	Personalize the baseline solution</strong></p>
                                            <p class="tab-content" style="padding-left: 20px">a.	Personalize the baseline system with customer data and organization structure</p></td>
                                            <td><strong>PM</strong>/ SA / FL / TL / FC / TC </td>
                                            <td>Personalized Future Reference System</td>                                    
                                            <td></td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>5.	Conduct FRS Workshops</strong></p>
                                            <p>The sessions are held to review the current business practices and to obtain the requirements from the customer. The workshops should aim to focus on the following:</p>
                                            <p class="tab-content" style="padding-left: 20px">a.	Reminders sent to participants prior to each FRS Workshop.</p>
                                            <p class="tab-content" style="padding-left: 20px">b.	Showcase proven industry practices referring the baseline solution
Review customer’s  business processes and practices (together with a visual representation of the flow within each process), identifying the pain points and areas of improvements 
</p></td>
                                            <td><strong>PM  / SA / FL</strong> / TL / FC / TC / Customer Project team members / All relevant stakeholders/ BTS/BPO</td>
                                            <td>Meeting Minutes of FRS Workshops / Updated FRS Workshop Presentations</td>                                    
                                            <td></td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>6.	Identify the Gaps / Deviations</strong></p>
                                            <p>The project team identifies which processes/requirements can be mapped using SAP Standard and which processes/requirements are considered as Gaps/Deviations. 
The Gap/Deviation identification should cover:
</p>
                                            <p class="tab-content" style="padding-left: 20px">a.	Identify and document the processes /requirements that can be addressed using SAP standard functionality</p>
                                            <p class="tab-content" style="padding-left: 20px">b.	Identify the possible Gaps/ Deviations</p>
                                            <p class="tab-content" style="padding-left: 20px">c.	Discuss and agree on the possible solution options to map the processes / requirements, including integration with external systems</p>
                                            <p class="tab-content" style="padding-left: 20px">d.	Use attune Fashion Suite  demonstrations when necessary in order to further elaborate on the possible solutions (optional)</p>
                                            <p class="tab-content" style="padding-left: 20px">e.	Discuss and agree if some of these Gaps can be met with a business process change rather than doing customizing in the system and document implications of such decision from a BT angle (people, process and technology)</p>
                                            <p class="tab-content" style="padding-left: 20px">f.	Document all agreed solutions in the related FRS Business Process Reference Documents   </p></td>
                                            <td><strong>PM</strong>  / SA / FL / TL / FC / TC / BTS /  All relevant stakeholders from Customer </td>
                                            <td>GAP List / Meeting Minutes of FRS Workshops / Project Status Tracking Sheet / FRS Business Process Reference Documents </td>                                    
                                            <td>As-Is Business  Process Documents with process flow diagrams / Meeting Minutes of Workshops / Personalized Reference System</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>7.	Schedule and hold Deep Dive Detailed Design discussions to close all open design gaps and issues</strong></p>
                                            <p>Deep Dive sessions are scheduled only for the purpose of gathering further details regarding the FRS Business Processes.  Deep Dive sessions are conducted only when needed.</p></td>
                                            <td><strong>PM</strong>  / SA / FL / TL / FC / TC / Customer Project team members / All relevant stakeholders/ BTS</td>
                                            <td>FRS Business Process Reference Documents / Updated Personalized Reference System / FS Template Conversion / FS Template Enhancement / FS Template Interface / FS Template Reports & Forms </td>                                    
                                            <td>Personalized Reference System / FRS Minutes of Workshops</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>8.	Design the To-Be Solution</strong></p>
                                            <p>This is the designing of the overall integrated solution done by the Solution Architect along with the Functional and Technical Leads from attune and the customer.  The Systems Landscape Design is finalized.</p></td>
                                            <td><strong>PM</strong>/ SA / FL / TL / FC / TC</td>
                                            <td>Updated System Landscape Design</td>                                    
                                            <td>As-Is Business  Process Documents with process flow diagrams / Meeting Minutes of Workshops</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>9.	Finalize and Sign-Off Reference Solution and related documentation </strong></p>
                                            <p>The project team documents the To-Be solution for each process with a visual flow diagram
The team completes the FRS Business Process Reference Documents 
The customer reviews and provides the sign off on the FRS Business Process Reference Documents. 
</p></td>
                                            <td><strong>PM</strong>  / SA / FL / TL / FC / TC</td>
                                            <td>FRS Business Process Reference Documents / Updated Personalized Reference System /  </td>                                    
                                            <td>As-Is Business  Process Documents with process flow diagrams</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>10.	Prepare User Stories and Estimate related effort</strong></p>
                                            <p>User Stories are equivalent to the “work package”; i.e. a RICEF object, configuration package, or some other work to be done to build the solution. 
</p></td>
                                            <td><strong>PO</strong>, SA, Agile Coach, PM, PM  / SA / FL / TL / FC / TC / Offshore Development Manager</td>
                                            <td>User Stories</td>                                    
                                            <td>Signed off To-Be Business  Process Documents with process flow diagrams</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>11.	Complete the Product Backlog and Estimations</strong></p>
                                            <p class="tab-content" style="padding-left: 20px">a.	Define the Configuration and Development Objects.  Objects to include:</p>
                                            <p class="tab-content" style="padding-left: 40px">i.	Data Extractions Tools and Formats</p>
                                            <p class="tab-content" style="padding-left: 40px">ii.	Data Conversion tools and Formats</p>
                                            <p class="tab-content" style="padding-left: 40px">iii.	All required development objects (interface, EDI objects, reports, enhancement programs, outputs and forms)</p>
                                            <p class="tab-content" style="padding-left: 40px">iv.	All required objects to be configured</p>
                                            <p class="tab-content" style="padding-left: 20px">b.	Identify a naming convention for the objects.  This naming convention is then shared with the customer teams and agreed upon.  All the objects are to be named and referred as per the decided convention.</p>
                                            <p class="tab-content" style="padding-left: 20px">c.	Estimate each configuration and development object</p>
                                            <p class="tab-content" style="padding-left: 20px">d.	Incorporate User Stories and Related Effort Estimates into the Product Backlog. </p></td>
                                            <td><strong>PM</strong>  / SA / FL / TL / FC / TC / BTS /  All relevant stakeholders from Customer </td>
                                            <td>GAP List / Meeting Minutes of FRS Workshops / Project Status Tracking Sheet / FRS Business Process Reference Documents </td>                                    
                                            <td>As-Is Business  Process Documents with process flow diagrams / Meeting Minutes of Workshops / Personalized Reference System</td>
                                        </tr>

                                        <tr>
                                            
                                            <td><p><strong>12.	Update the Master Project Schedule and ratify.</strong></p>
                                            <p class="tab-content" style="padding-left: 20px">a.	The PM updates the Master Project Schedule utilizing the estimated effort from the User Stories and Product Backlog, the timelines, and the resource plan.</p>
                                            <p class="tab-content" style="padding-left: 20px">b.	The SA, PO, and Team leads collaborate during the ratification process</p>
                                            <p class="tab-content" style="padding-left: 20px">c.	The Master Project Schedule contains all systems impacted, including modifications by external parties to their systems, if these modifications are part of the overall solution</p>
                                            <td><strong>PO</strong>, SA/ PM  / FL / TL / </td>
                                            <td>Updated Master Project Schedule</td>                                    
                                            <td>User Stories (RICEFW Object List / List of Objects to be configured)  / Master Project Schedule /  Resource Plan/ Product Backlog</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>13.	Plan the Agile Build Sprints</strong></p>
                                            <p class="tab-content" style="padding-left: 20px">a.	Sprint Planning will cover all the work for the entire Agile Build Phase</p>
                                            <p class="tab-content" style="padding-left: 20px">b.	Sprint durations are fixed (Sprint 1 = 6 weeks; Sprint 2 through Sprint X = 4 weeks)</p>
                                            <p class="tab-content" style="padding-left: 20px">c.	The Team estimates the user stories (work packages) effort in Story Points based upon priority and decides how many user stories will be included in the upcoming Sprints.</p>
                                            <td><strong>PO</strong> / PM / SA / FL / TL/ Offshore Development Manager/ Agile Coach </td>
                                            <td>Sprint Plan</td>                                    
                                            <td>Product Backlog (User Stories / Work Packages), Gap List</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>14.	Complete Change Impact Assessment</strong></p>
                                            <p>The BTS team captures any Change Impacts from the blueprint sessions and completes the Change Impact Assessment.  The Change Impact Assessment  documents key changes arising from the proposed solution </p></td>
                                            <td><strong>BTS</strong> / Customer Project Team / SMEs </td>
                                            <td>Change Impact Assessment</td>                                    
                                            <td>FRS Business Process Reference Documents</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>15.	Hold FRS Walkthrough Presentation </strong></p>
                                            <p>The team conducts a 2-3 day walkthrough of the Solution design for the customer’s Project Team and all relevant stakeholders.  The walkthrough is done using Power Point and system demos in the FRS system.  End-to-end business processes are covered.  The customer accepts the proposed solution presented in the Design Walkthrough.</p></td>
                                            <td><strong>SA</strong>/ PM  / FL / TL / FC / TC / All relevant stakeholders</td>
                                            <td>FRS Walkthrough presentation</td>                                    
                                            <td>System Landscape Design, FRS Business Process Reference documents, Product Backlog, Sprint Plan</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>16.	Complete Integrated Communication Plan</strong></p>
                                            <p>Based on the Change Impact Assessment and the Stakeholder Assessment, a detailed integrated communication plan is developed. </p></td>
                                            <td><strong>BTS</strong> / Customer Project Team / HR / Customer Communications Team / Key Stakeholders </td>
                                            <td>Integrated Communication Plan</td>                                    
                                            <td>Stakeholder Assessment Change Impact Assessment</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>17.	Prepare Key Leadership Action Plans and Key Performance Indicators</strong></p>
                                            <p>Key leadership action plans are developed to help coach and drive key leader’s continued support and involvement. The Key Performance Indicators can be developed in conjunction with this activity. </p></td>
                                            <td><strong>BTS</strong> / Customer Project Team /  HR / Customer Communications Team /Key Stakeholders</td>
                                            <td>Leadership Action Plans KPIs</td>                                    
                                            <td>Stakeholder Assessment Change Impact Assessment Integrated Communication Plan</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>18.	Prepare Change Agent Strategy</strong></p>
                                            <p>The Knowledge transfer plan describes how the knowledge transfer strategy will be implemented and the checklist tracks to completion of listed knowledge transfer events.</p></td>
                                            <td><strong>BTS</strong> / PM / FL /TL/SA </td>
                                            <td>Knowledge Transfer Tracking</td>                                    
                                            <td>Knowledge Transfer Strategy</td>
                                        </tr>

                                         <tr>   
                                            <td><p><strong>19.	Prepare Knowledge Transfer Plan and Checklist</strong></p>
                                            <p>Key stakeholders known as Change Agents are identified to help drive the business adoption of the new solution.  These individuals have either formal or informal influence/credibility with the organization.  The Change Agent Strategy document provides a guide and reference on how these Change Agents will carry out their roles.</p></td>
                                            <td><strong>BTS</strong> / Customer Project Team/ HR / Customer Communications Team / Key Stakeholders</td>
                                            <td>Change Champion Change Agent Strategy </td>                                    
                                            <td>Stakeholder Assessment Change Impact Assessment Integrated Communication Plan</td>
                                        </tr>


                                        <tr>
                                            <td rowspan="1"><strong>Prepare for the Agile Build and Test Phase</strong></td>
                                            <td><p><strong>20.	Build and set up the development environment</strong></p>
                                            <p>During the FRS Phase the development environment is prepared according to the approved Client Eco System Architecture.  It must be ready to begin development at the beginning of the Agile Build Phase.</p>
                                            <p class="tab-content" style="padding-left: 20px">a.	The setup of the Development box with the separate clients clearly identified for:</p>
                                            <p class="tab-content" style="padding-left: 40px">i.	Configuration only client: Also known as the Golden client. This is the system purely for configuration changes.</p>
                                            <p class="tab-content" style="padding-left: 40px">ii.	Development only client: This is the system where all developments are carried out.</p>
                                            <p class="tab-content" style="padding-left: 40px">iii.	Unit Testing Client: This system would consist of both Configuration changes and Development changes in order to perform Unit Testing.  Mock 0 will be done in this client.</p></td>
                                            <td><strong>TA</strong> / TL / TC / SA / FL</td>
                                            <td>Development environment ready for the Build</td>                                    
                                            <td>Client Eco System Architecture </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Prepare for the Agile Build and Test Phase</strong></td>
                                            <td><p><strong>21.	Confirm Development Procedures, Development Standards, the Offshore Model, and Transport Management Procedures</strong></p>
                                            <p>During the FRS  Phase, the following activities are done as part of this activity:</p>
                                            <p class="tab-content" style="padding-left: 20px">a.	the attune and the Customer’s Technical Teams agree on the development standards that will be followed</p>
                                            <p class="tab-content" style="padding-left: 20px">b.	The offshore vs onsite development model is confirmed and communicated</p>
                                            <p class="tab-content" style="padding-left: 20px">c.	Technical procedures are agreed to such as Functional/Technical spec approval processes, transport management procedures, system freeze procedures, etc.</p></td>
                                            <td><strong>TA</strong> / TL / TC / SA / FL</td>
                                            <td>Development Procedures document</td>                                    
                                            <td>Development Standards / Development models / Transport Management Procedures</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"></td>
                                            <td><p><strong>22.	Prepare meeting presentation templates</strong></p>
                                            <p>Presentation templates for the Team Leads, PMO and Steering Committee meetings are prepared, reviewed and approved.</p></td>
                                            <td><strong>PGM</strong> / PM</td>
                                            <td>Presentation templates</td>                                    
                                            <td>Project communication and PowerPoint standards</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2"><strong>Prepare for the Agile Build and Test Phase</strong></td>
                                            <td><p><strong>23.	Onboard the On Site and Off Shore Development and Functional Team Members</strong></p>
                                            <p>PM works with the TA and TL and Regional Professional Services VP’s to identify the offshore and onsite developers, and offshore and onsite functional team members that will be added to the project team for the Build Phase.</p></td>
                                            <td><strong>PM</strong> / TA / TL </td>
                                            <td></td>                                    
                                            <td>Resource Plan</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>24.	Store all documents created and updated by attune during the Phase on ONEattune in the Project workspace</strong></p>
                                            <p>It is important to retain attune intellectual property on ONE attune.  This activity applies to every member of the team.  It includes documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </p></td>
                                            <td><strong>PM</strong> / All attune project team members</td>
                                            <td>Updated ONEattune Project work space</td>                                    
                                            <td>Documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </td>
                                        </tr>


                                        

                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Signed off Project Charter by Customer (all project strategies are included)</p>
                                            <p>•	attune Project Team Onboarding Completed</p>
                                            <p>•	Target Operating Model / Future Vision  direction specified by the Business Process Owners and Project Sponsors (Question to Leadership – How do we ensure this is complete prior to FRS?  In what format? Need to make sure the terminology is consistent between Project Prep and FRS)</p>
                                            <p>•	Completed Project Transformation Vision Statement</p>
                                            <p>•	Business Process and IT Solution Owners Designated and Dedicated to the Project (have a follow up with Bianca on how to identify this level of commitment.)</p>
                                            <p>•	Applicable Business Processes (Master Process List) Identified</p>
                                            <p>•	Guiding Principles Established by Sponsors</p>
                                            <p>•	Completed FRS Workshop Schedule and invitations sent to all participants</p>
                                            <p>•	Customer Resources committed to attend scheduled FRS Workshops </p>
                                            <p>•	FRS Workshop Tracker developed</p>
                                            <p>•	Completed Project Kick Off Meeting </p>
                                            <p>•	Completed Introduction to the aIR Methodology training for Project Team Members</p>
                                            <p>•	Completed Customer Training for FRS Workshop methodology</p>
                                            <p>•	Updated Master Project Schedule for FRS phase </p>
                                            <p>•	Completed Initial Business Transformation Readiness Assessment Report</p>
                                            <p>•	FRS environment set up and attune Fashion Suite installed and available for personalization</p>
                                            <p>•	SAP Sandbox environment in place for system demos</p>
                                            <p>•	Customer Project Team Training completed (optional, if purchased)</p>
                                            <p>•	Completed Fundamentals of Change Management for Key Stakeholders Workshop</p>
                                            <p>•	Completed Leading Change for Leaders Workshop</p>
                                            <p>•	Completed Project Preparation Exit Stage Gate Review with all items passed and/or exceptions approved by Steering Committee</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Completed Future Reference System Personalized for the Customer</p>
                                            <p>•	Global High-Level Design</p>
                                            <p>•	Successful completion of the FRS Workshops</p>
                                            <p>•	Completed FRS Business Process Reference Documents</p>
                                            <p>•	Completed Gap List and Development List</p>
                                            <p>•	Completed Product Backlog and User Stories</p>
                                            <p>•	Completed Sprint Planning</p>
                                            <p>•	Completed FRS Walkthrough Presentation</p>
                                            <p>•	Completed Change Impact Assessment</p>
                                            <p>•	Completed Key Performance Indicators for project reporting</p>
                                            <p>•	Acceptance and sign-off of the FRS document</p>
                                            <p>•	Completed Exit Stage Gate Review with all items passed and exceptions approved by Steering Committee</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Signed off Statement of Work/Contract </p>
                                            <p>•	Approved Project Charter, includes all strategy documents</p>
                                            <p>•	High Level Timeline</p>
                                            <p>•	Initial 30 Day Communication Plan</p>
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	Final Resource Plan</p>
                                            <p>•	Master Project Schedule</p>

                                            <p>•	Training Materials for FRS Phase training for the Project Team</p>
                                            <p>•	FRS Workshop Schedule</p>
                                            <p>•	FRS Workshop Tracker</p>
                                            <p>•	SAP System Landscape Strategy</p>
                                            <p>•	Integration Landscape Diagram</p>
                                            <p>•	As-Is Business Process Documents (Optional)</p>
                                            <p>•	Initial Business Transformation Readiness Assessment and Approach</p>
                                            <p>•	Initial Business Transformation Readiness Report</p>
                                            <p>•	Executive Alignment Workshop and Workshop Materials</p>
                                            <p>•	Fundamentals of Change Management for Key Stakeholders Workshop Materials</p>
                                            <p>•	Stakeholder Identification with Grid Mapping</p>
                                            <p>•	Leading Change for Leaders Workshop Materials</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">

                                            <p>•	Approved High Level Design</p>
                                            <p>•	Change Champion / Change Agent Strategy</p>
                                            <p>•	Change Impact Assessment</p>
                                            <p>•	Approved Development Procedures Document/p>
                                            <p>•	FRS Business Process Reference Documents</p>
                                            <p>•	FRS Walkthrough Presentation</p>
                                            <p>•	Integrated Communication Plan</p>
                                            <p>•	Key Stakeholder Leadership Action Plan</p>
                                            <p>•	Knowledge Transfer Plan and Tracker</p>
                                            <p>•	Gap List</p>
                                            <p>•	Meeting Minutes of FRS Workshops</p>
                                            <p>•	User Stories</p>
                                            <p>•	Product Backlog</p>

                                            <p>•	Personalized Reference System</p>
                                            <p>•	Sprint Plan</p>
                                            <p>•	Stage Gate Review Dashboard</p>
                                            <p>•	Steering Committee Meeting Update</p>
                                            <p>•	Updated FRS Workshop Schedule</p>
                                            <p>•	Updated Integration Landscape Diagram</p>
                                            <p>•	Updated Project Status Tracking Sheet</p>
                                            <p>•	Updated Master Project Schedule</p
                                            <p>•	Updated System Landscape Strategy</p
                                            <p>•	Updated Strategy Documents and Project Charter</p
                                            <p>•	Weekly PMO Meeting Update</p
                                            <p>•	Weekly Team Leads Meeting Update</p

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	FRS Phase profitability</p>
                                            <p>•	Schedule and effort variance for FRS Phase</p>
                                            <p>•	Review Efficiency</p>
                                            <p>•	Quality of requirements captured and how well solution fits – measured by number of subsequent change requests</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The project may decide to use client defined templates and standards for the deliverables from within the FRS phase. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>

                                            <p>•	Certain activities of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>

                                            <p>•	Certain activities of this process may be delivered in another phase of the project, as per the contractual requirements.  This tailoring is documented in the Project’s Process</p>

                                            <p>•	In case the client or another team representing the client is performing some parts of the activities and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. 

</p>

                                        </div>
                                    </td>
                                    </tr>     
                                    
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Project Preparation Phase Process.pdf">Download Project Preparation Phase Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>  
                                                                                              
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <%--<h2>Document Templates</h2>--%>                       
                        

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <%--<h3>Functional & Technical</h3>--%>     
                                <h4>Templates</h4>
                                <p><a href="#">1.  Approved Development Procedures Documents</a></p> 
                                <p><a href="#">2. Approved High Level Design</a></p> 
                                <p><a href="References\Change_Champion_Change_Agent_Strategy.docx">3. Change Champion / Change Agent Strategy</a></p> 
                                <p><a href="References\Change_Impact_Assessment.docx">4. Change Impact Assessment</a></p> 
                                <p><a href="References\attune BPD Template.docx">5. FRS Business Process Reference Documents</a></p> 
                                <p><a href="#">6.  FRS Walkthrough Presentation</a></p>    
                                <p><a href="References\GAP List.xlsx">7.  Gap List</a></p>  
                                <p><a href="References\Integrated Communication Plan.xlsx">8. Integrated Communication Plan</a></p> 
                                <p><a href="References\Key Stakeholder Leadership Action Plan.docx">9. Key Stakeholder Leadership Action Plans</a></p> 
                                <p><a href="References\Knowledge Transfer Tracking.xlsx">10. Knowledge Transfer Plan and Tracker</a></p>
                                <p><a href="References\Meeting Minutes.docx">11. Meeting Minutes of FRS Workshops</a></p>
                                <p><a href="#">12. Personalized Reference System</a></p>
                                <p><a href="References\Product Backlog.xls">13. Product Backlog</a></p> 
                                <p><a href="#">14. Sprint Plan</a></p>    
                                <p><a href="References\Stage Gate Review.pptx">15. Stage Gate Review Dashboard</a></p>  
                                <p><a href="#">16. Steering Committee Meeting Update</a></p>
                                <p><a href="References\Business Blueprint Workshop Schedule.xlsx">17. Updated FRS Workshop Schedule</a></p>
                                <p><a href="References\System Integration Landscape.vsd">18. Updated Integration Landscape Diagram</a></p>
                                <p><a href="#">19. Updated Master Project Schedule</a></p>
                                <p><a href="References\Project Status Tracking Sheet.xlsm">20. Updated Project Status Tracking Sheet</a></p>
                                <p><a href="References\Project Charter.docx">21. Updated Strategy Documents and Project Charter</a></p> 
                                <p><a href="References\SAP System Landscape Stratergy.docx">22.  Updated System Landscape Strategy</a></p>
                                <p><a href="#">23.  Weekly PMO Meeting Update</a></p>
                                <p><a href="#">24.  Weekly Team Leads Meeting Update</a></p>


                          </div>
<%--                          <div class="col-xs-6 col-md-4"> 
                                <h3>PMO & Testing</h3>   
                                <p><a href="https://life.oneattune.com/mlink/file/MzQ2MzY" target="_blank">1.  attune PMO Project Governance</a></p> 
                                <p><a href="https://life.oneattune.com/mlink/file/NDE4NTc" target="_blank">2.  attune ABAP development Guidelines</a></p>  
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTA">3.  attune Coding Standards Java</a></p>    
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTE">4.  attune Coding Standards Microsoft.Net</a></p> 
                          </div> --%>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>Business Transformation</h3>--%>
                                <h4>Guidelines</h4>
                                <p><a href="References\Branded aIR Diagram.pptx">1.  Branded aIR Diagram</a></p> 
                                <p><a href="References\Phase Process Summary.docx">2. Phase Process Summary</a></p> 

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>   
                                <p><a href="References\Onboarding Guide Sample.docx">1.  Onboarding Guide Sample</a></p> 
                                <p><a href="References\Project Organizational Chart Sample.pptx">2.  Project Organizational Chart Sample</a></p> 
                          </div>
                        </div>                        

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                        </div> 

                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                        </div> --%>


<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->

</asp:Content>
