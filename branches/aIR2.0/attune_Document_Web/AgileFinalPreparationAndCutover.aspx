﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AgileFinalPreparationAndCutover.aspx.cs" Inherits="attune_Document_Web.AgileFinalPreparationAndCutover" %>

<asp:Content ID="projectPreparationPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">--%>
                <%--<div class="row">
                    <div class="col-lg-12">

                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Preparation  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Final Preparation and Cutover phase of a SAP implementation project lifecycle.  It covers the activities that need to be performed within this phase.</p>
                            <p>During the Final Prep and Cutover Phase the project team prepares for go-live.  The primary focus is on executing live cutover, conducting end user training, and preparing for the Post Go-Live Support Phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on the expected deliverables and the contractual requirements.  The activities related to the Final Preparation and Cutover phase that are defined here derive the process followed during this phase of the project.</p> 
                        </div>
                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        
                        <p>A complete list of Acronyms, Abbreviations, and Definitions can be found in the document entitled “aIR Terminology”, found in the Guidelines section of the aIR Methodology portal.  </p>
	

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h3>Process and Steps</h3>     
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), derived from Agile principles, and PMI, SAP ASAP Focus, and SAP Activate methodologies. The aIR Methodology includes elements of Agile principles for business requirements gathering and development, while maintaining tried and true traditional approaches to ERP implementations.  Within the aIR methodology, the Final Preparation and Cutover phase follows the IST Phase (Integration Testing for Pilot 1).</p>
                            <p>Within this phase, all of the activities are focused on going live.  The major activities carried out are end user training and the live cutover of the systems.  The organizational readiness assessments begun in the IST Phase continue right up until go-live.  Organizational communication continues throughout the phase.  The post-go-live support preparations are finished.</p>
                            <p>The activities start with the kick off of End User training.  The training continues until all of the business users involved in all of the different business processes are trained and have been given the knowledge required to continue their day to day activities in the new SAP system after Go Live.</p>
                            <p>While the training is being conducted, the Live Cutover Schedule prepared during the IST Phase is executed in the production environment.  This begins with a formal Go/No-Go decision by the Steering Committee to move all of the transports for the implementation to production and migrate master data from the legacy systems to the new production system.  This activity is carried out over a period of a few weeks while the business continues to operate in the legacy system.  At this time, the business is also ramping down per the plan put in place in the previous phase so that the legacy system can be shut down for 3-5 days with minimal impact to the customer and its partners.</p>
                            <p>When master data has been loaded in the new production system, validated by the customer, and signed off by the BPO’s, the second formal Go/No-Go decision is made by the Steering Committee to shut down the legacy system and migrate open transactions and inventory (i.e. transaction data) to the new production system.  All ancillary systems perform their cutover activities, as well.  The migration of transaction data and the cutover of ancillary systems is generally done over a 3-5 day period and must be at the end of a fiscal month.  Because there is no system for the business to operate in, the migration continues 24 hours a day until all transaction data is loaded, validated by the customer, and signed off by the BPO’s.  During the downtime, security access set up is also completed for all users and job schedules are set up and activated in the new production environment.  All post-load data activities are done.  The production environment is now ready for live transactions.</p>
                            <p>At this time, the Steering Committee is asked to make their 3rd formal Go/No-Go decision.  This decision is to begin Controlled Resumption.  Controlled Resumption takes place in all locations impacted by the project implementation.  Over the course of 1 day, a handful of pre-defined end-to-end live transactions are processed by end users in the production environment using the Controlled Resumption scripts.  The purpose of Controlled Resumption is to ramp up very slowly while monitoring the system for small bugs.  These bugs are corrected immediately before proceeding to the next step in the script.  This approach minimizes delays later when large volumes of live transactions are being processed in the system. At this time, the organizational readiness assessments are discontinued.</p>
                            <p>Once all of the Controlled Resumption scripts are successfully completed and all bugs are fixed, the Steering Committee makes its 4th and final Go/No-Go Decision.  This decision is the stage gate to exit the Final Preparation and Cutover Phase and begin the Post-Go-Live Support Phase.</p>
                            <p>While the master data migration is underway, all preparations for post-go-live support are being carried out per the plan developed and approved in the Realization – Test Phase. Procedures for post-go-live support are finalized. The support team is identified and assigned and their locations for post-go-live support are mapped out. A post-go-live support schedule is prepared. Tools for recording and tracking support issues reported by end users are put in place. Status reporting templates are completed. Post-go-live support kick-off meetings with the support team and the end users are scheduled and conducted. </p>
                            <p>Also, while the master data is being migrated, the PMO prepares a Business Continuity Plan for the implementation. The Business Continuity Plan describes the strategy and approach for continuing business in the unlikely event that the implementation goes so badly that it must be stopped.  Actions are taken to ensure that the mitigation approach is ready to be executed, if required.</p>
                            <p>Throughout the entire phase, the Project Status Tracking Sheet is updated with any issues, risks, decisions, assumptions, and action items taken.</p>
                            <p>The table below highlights each of the activities mentioned above in detail. It provides information on the activities, the responsible person or team, the documents created in each activity (i.e. artifacts) and the reference documents used. </p>



                            <h4>Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activities</th>
                                        <th>Responsible / Participants</th>
                                        <th>Artifacts</th>
                                        <th>Reference Documents</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="10"><strong>Final Prep and Cutover Execution</strong></td>
                                            <td><p><strong>1.	End User Training</strong></p>
                                                <p>This activity is owned by the project’s Training Manager and is conducted by the Training Team with support from the Customer’s core project team and the attune project team.  This training occurs in parallel with the live cutover activities.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Using the End User Training checklist, confirm that all preparations for end user training are complete.  These should have been completed prior to the end of the Realization – Test Phase.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The Training team member responsible for each module takes the class through the training.  The training covers:</p>
                                                    <p class="tab-content" style="padding-left: 40px">SAP Navigation</p>
                                                    <p class="tab-content" style="padding-left: 40px">ii.	The business processes covered within each SAP module (focus is on the To-Be business processes documents with the process flows as per the Business Blueprint Document)</p>
                                                    <p class="tab-content" style="padding-left: 40px">iii.	The system features and functions within each business process</p>
                                                    <p class="tab-content" style="padding-left: 40px">iv.	The concept of master data, how it is created or maintained and how it is used</p>
                                                    <p class="tab-content" style="padding-left: 40px">v.	The concept of the transactional data and how transactions are entered in SAP for each step in the To-be process flow together with the main SAP transactions</p>
                                                    <p class="tab-content" style="padding-left: 40px">vi.	Hands on training with access to SAP system (the Training environment or a quality environment will be used for this purpose)</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	A customer core team member and an attune project team member from each module is present to support the Trainer and to help answer any questions or provide clarifications.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	A training evaluation form is filled out by each trainee. </p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Feedback on how well the class understood the training is given to the customer’s upper management by the Trainer.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The Training materials are updated by the Training team in order to fine tune the documents to cover all the To-Be business processes</p>
                                            </td>
                                            <td><strong>TM</strong> / Training Team / Customer’s Core project team / FC / SAP Security </td>
                                            <td>Updated End User Training Schedule / Updated End User Training Materials</td>                                    
                                            <td>End User Training Schedule / End User Training invitations / End User Training Materials / Training System readiness checklist / Training Logistics Readiness Checklist / FRS Business Process Documents / To-Be Business Process Documents with process flow diagrams</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>2.	Execute Live Cutover – Master Data Migration</strong></p>
                                                <p>Live cutover is performed in the production environment following the cutover schedule prepared during the IST Phase.  The cutover schedule contains the detailed activities for the entire cutover. The PM kicks off each activity in the Live Cutover Schedule in an email and tracks the completion of the activities.  The status and the results of the Live Cutover are shared daily with all relevant stakeholders of the customer, the customer’s core project team and the customer’s and attune’s management team.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Steering Committee Go/No-Go Decision #1 to begin the live cutover</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The TRs listed in the Transport Request Tracking sheet are moved into the production environment following the Transport Management Procedures for production. With the above step, all of the objects both configuration and development, are available in the production environment.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The FC and TC perform required set up activities in the production client.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The relevant stakeholders from the customer confirm that data in the legacy production system that is to be migrated is cleansed.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Master data creation and changes are cut-off in the legacy production system according to the schedule prepared and agreed to in advance with the BPO’s.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The master data to be migrated is extracted from the legacy production system or manually prepared in the formats required for the Data Conversion Tools (the same tools used during Mock Cutover 1 during the Build phase) </p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The master data extracted is reviewed and validated by the relevant stakeholders from the customer.  Issues reported are tracked and resolved.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	The validated extract master data and the manually prepared master data are loaded into the production environment.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	The migrated data in the production environment is validated and signed off by the relevant stakeholders from the customer.   100% of the legacy data must be accurately loaded.  Issues reported are tracked and resolved.   These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	The FC and TC perform required post master data migration set up activities in the production client.</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	The production client is ready to receive open transactions from the legacy system.</p>
                                            </td>
                                            <td><strong>PM</strong>  / DM   Team / Basis / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Updated Live Cutover Schedule / Updated Project Status Tracking Sheet / Updated Detailed Project Plan / Meeting Minutes of discussions taken place to discuss and overcome issues faced during the Dry Run</td>                                    
                                            <td>Live Cutover Schedule / Transport Management Procedures / Detailed Project Plan / Project Status Tracking Sheet / Project Charter</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>3.	Execute Live Cutover – Transaction Data Migration</strong></p>
                                                <p>As agreed to during the Live Cutover Planning activities, the business operations are stopped and the legacy system is shutdown.   This is at the end of a fiscal month. Live cutover is continued in the production environment following the cutover schedule. The PM continues to kick off each activity in the Live Cutover Schedule in an email and track the completion of the activities. Transaction data migration activities continue 24 hours a day until done, usually 3-4 days.  The status and the results of the Live Cutover are shared daily with all relevant stakeholders of the customer, the customer’s core project team and the customer’s and attune’s management team.</p>
                                                
						                        <p class="tab-content" style="padding-left: 20px">a.	Steering Committee Go/No-Go Decision #2 to shut down the legacy production system and migrate transaction data</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Business transactions are stopped in the legacy production system according to the schedule prepared and agreed to in advance with the BPO’s.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The transaction data to be migrated is extracted from the legacy production system or manually prepared in the formats required for the Data Conversion Tools (the same tools used during Mock Cutover 1 during the Build phase) </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The transaction data extracted is reviewed and validated by the relevant stakeholders from the customer.  Issues reported are tracked and resolved.  These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The validated extracted transaction data and the manually prepared transaction data are loaded into the production environment.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The migrated data in the production environment is validated and signed off by the relevant stakeholders from the customer.   100% of the legacy data must be accurately loaded.  Issues reported are tracked and resolved.   These issues and actions taken are documented in the Project Status Tracking Sheet.</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The FC and TC perform all required post transaction data migration set up activities in the production client.</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	The middleware and 3rd party system cutover activities are performed and validated.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	The production environment is ready for live transactions</p>
                                            </td>
                                            <td><strong>PM</strong>  / DM   Team / Basis / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Updated Live Cutover Schedule / Updated Project Status Tracking Sheet / Updated Detailed Project Plan / Meeting Minutes of discussions taken place to discuss and overcome issues faced during the Dry Run</td>                                    
                                            <td>Live Cutover Schedule / Transport Management Procedures / Detailed Project Plan / Project Status Tracking Sheet / Project Charter </td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>4.	Conduct Controlled Resumption</strong></p>
                                                <p>When all data is migrated and signed off, the first live transactions will be processed in the production environment by BPE’s. The activities for this activity are:</p>
						                        
						                        <p class="tab-content" style="padding-left: 20px">a.	Once the transaction data is migrated and validated with the sign off, Go/No-Go Decision #3 to begin Controlled Resumption is taken by the Steering Committee.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The BPE’s Conduct Controlled Resumption following the scripts prepared in the Realization – Test Phase.  Key processes are covered end-to-end.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The core project team monitors the transactions very closely.  Bugs are fixed as they are found before going to the next step.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	When all scripts have been successfully completed and passed, the production environment is ready for full processing.</p>
                                            </td>
                                            <td><strong>PM</strong>  / DM   Team / Basis / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Updated Controlled Resumption Scripts / Updated Project Status Tracking Sheet / Updated Detailed Project Plan / Updated Defects Log with defects identified during Controlled Resumption</td>                                    
                                            <td>Controlled Resumption Scripts / Live Cutover Schedule / Transport Management Procedures / Detailed Project Plan / Project Status Tracking Sheet / Project Charter </td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>5.	Begin Full Processing</strong></p>
                                                <p>At the completion of Controlled Resumption, the Steering Committee makes its 4th and final Go/No-Go Decision. </p>
						                        
						                        <p class="tab-content" style="padding-left: 20px">a.	Steering Committee Go/No-Go Decision #4  to begin full transaction processing</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Full transaction processing commences and the post-go-live support Phase begins.</p>
                                            </td>
                                            <td><strong>PM</strong>  / DM   Team / Basis / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Final Preparation and Live Cutover Stage Gate Review Decision </td>                                    
                                            <td>Updated Controlled Resumption Scripts / Updated Live Cutover Schedule / Transport Management Procedures / Detailed Project Plan / Project Status Tracking Sheet / Project Charter </td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>6.	Assess Organizational readiness</strong></p>
                                                <p>This activity is continued from the IST Phase. It is performed by the Business Transformation Team together with the project PMO. This involves: </p>
						                        
						                        <p class="tab-content" style="padding-left: 20px">a.	Finalizing the Readiness indicators for each work stream</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Each work stream rating  themselves against a set of given readiness indicators</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Work stream ratings are collected and analyzed to produce the Go-Live Readiness Assessment</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Based on any gaps, mitigation strategy is implemented before or after go-live as appropriate </p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Sustainment Strategy is formulated</p>
                                            </td>
                                            <td><strong>PGM</strong> / PM / Customer Core Project team / The Business Transformation Team  / Steering Committee</td>
                                            <td>Go-Live Readiness  Assessment document  / Sustainment Strategy  </td>                                    
                                            <td>Updated Detailed Project Plan / Project Charter / Project Kick Off presentation / Updated Project Status Tracking Sheet/Communication Plan </td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>7.	Prepare Organization Communication </strong></p>
                                                <p>Communications continue to be shared in order to pass the message to everyone in the organization during the decided timeline on the impending Go Live and the support of the whole organization is garnered in order to ensure the Go Live is a success.</p>
						                        
						                        <p class="tab-content" style="padding-left: 20px">a.	Go-Live communication plan is executed</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Feedback is actively solicited from Change Agents to ascertain state of business readiness</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Address any ad-hoc communication requirements</p>
                                            </td>
                                            <td><strong>Business Transformation Team</strong> / Project Steering Committee</td>
                                            <td>Communication Templates – various channel and tools</td>                                    
                                            <td>Stakeholder Map /Communication Plan/Go-Live Readiness Assessment /Help Desk Procedures – Escalation process</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>8.	Post-Go-Live Support Readiness</strong></p>
                                                <p>This involves making all of the preparations for Post-Go-Live Support that were laid out in the plan prepared and approved in the Realization – Test Phase. </p>
						                        <p>The following activities are done to ensure that the business users will be able to  continue their day to day activities in the new system:</p>
						                        <p class="tab-content" style="padding-left: 20px">a.	Finalize the standard operating procedures </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The Support team is identified and assigned</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The Knowledge Transfer schedule is prepared and knowledge transfer is conducted. </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The assigned locations for post-go-live support are mapped out.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The Post-Go-Live Support Schedule is prepared.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The tool to be used to log issues and CRs is set up</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	All of the Business users, attune project team, customer’s core project team and the identified support team members are all trained on the new Tool</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Status reporting templates are completed. </p>
                                                <p class="tab-content" style="padding-left: 20px">i.	Post-go-live support Kick-Off meetings with the support team and the end users are scheduled and conducted.  </p>
                                                <p class="tab-content" style="padding-left: 20px">j.	Post-go-live support is ready to be conducted as soon as Controlled Resumption is completed</p>
                                            </td>
                                            <td><strong>Business Transformation Team</strong> / Project Steering Committee</td>
                                            <td>Communication Templates – various channel and tools</td>                                    
                                            <td>Stakeholder Map /Communication Plan/Go-Live Readiness Assessment /Help Desk Procedures – Escalation process</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>9.	Prepare Business Continuity Plan</strong></p>
                                                <p>While the master data is being migrated to production, the PMO prepares a Business Continuity Plan for the implementation.</p>
						                        <p class="tab-content" style="padding-left: 20px">a.	The Business Continuity Plan describes the strategy and approach for continuing business in the unlikely event that the implementation goes badly and must be stopped.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The Business Continuity Plan and proposed mitigation procedures are discussed with all of the relevant stakeholders from the customer and attune, and the plan is approved by the customer. </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Actions are taken to ensure that the mitigation approach is ready to be executed, if required.</p>
                                            </td>
                                            <td><strong>PMO</strong> / Customer Stakeholders</td>
                                            <td>Business Continuity Plan</td>                                    
                                            <td>Project Charter</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>10.	Store all documents created and updated by attune during the Phase on ONEattune in the Project workspace</strong></p>
                                                <p>It is important to retain attune intellectual property on ONE attune.  This activity applies to every member of the team.  It includes documents used for an activity (e.g. FRS Workshop presentations) as well as deliverable artifacts.</p>
                                            </td>
                                            <td><strong>PM</strong> / All attune project team members</td>
                                            <td>Updated ONEattune Project work space</td>                                    
                                            <td>Documents used for an activity (e.g. FRS Workshop presentations) as well as deliverable artifacts. </td>
                                        </tr>
                                        

                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	All integration test scripts executed and all steps passed in 2 cycles of Integration Testing.  Exceptions approved by the Steering Committee.</p>
                                            <p>•	All End-User Showcase sessions conducted and feedback given for questions raised. </p>
                                            <p>•	System acceptance Sign Off by the BPO’s.</p>
                                            <p>•	All regression test scripts executed and all steps passed. Exceptions approved by the Steering Committee.</p>
                                            <p>•	All Performance/Stress Testing scripts executed and passed.</p>
                                            <p>•	Production environment ready for Live Cutover</p>
                                            <p>•	Live Cutover dry runs complete, 100% of the data loaded, and the data validated by the customer.  Exceptions approved by the Steering Committee.</p>
                                            <p>•	Live Cutover business shutdown and ramp-up plans approved</p>
                                            <p>•	Approved Controlled Resumption scripts</p>
                                            <p>•	Ready for End User Training</p>
                                            <p>•	Go-Live Readiness Assessments completed and published</p>
                                            <p>•	Organization Communication prepared and shared</p>
                                            <p>•	Approved Post-Go-Live Support Plan</p>
                                            <p>•	Approved Post-Go-Live Support Procedures</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	All end users trained</p>
                                            <p>•	Master data migrated to new system and signed off by the customer</p>
                                            <p>•	All open transactions and inventory migrated to new system and signed off by the customer</p>
                                            <p>•	All cutover activities done</p>
                                            <p>•	Controlled Resumption scripts executed and all bugs resolved</p>
                                            <p>•	Released Production System to the end-users</p>
                                            <p>•	Go Decision by the Steering Committee to go to full business processing</p>
                                            <p>•	Post-Go-Live Support ready to be executed</p>
                                            <p>•	All Business Blueprint documents, Integration Landscape Diagram, and Functional/Technical Specification documents updated with changes related to approved CR’s and refinement of the solution during the Final Preparation and Cutover Phase</p>
                                            <p>•	Completed Exit Stage Gate Review with all items passed and exceptions approved by Steering Committee</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">


                                            <p>•	End User Training Schedule</p>
                                            <p>•	All required End User Training Materials</p>
                                            <p>•	Live Cutover Plan</p>
                                            <p>•	Live Cutover Schedule</p>
                                            <p>•	Approved Controlled Resumption Scripts</p>
                                            <p>•	Go-Live Readiness Assessment</p>
                                            <p>•	Communication Templates – Mixed </p>
                                            <p>•	User ID List and Authorization Matrix</p>
                                            <p>•	Post-Go-Live Support Plan</p>
                                            <p>•	Post-Go-Live Support Procedures</p>
                                            <p>•	Updated or New Functional and Technical Specification Documents</p>
                                            <p>•	Updated Business Blueprint Document</p>
                                            <p>•	Updated Master Project Schedule</p>
                                            <p>•	Updated Project Status Tracking Sheet</p>
                                            <p>•	Updated Defect Log</p>
                                            <p>•	Updated Test Scripts with actual results</p>


                                            
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">

                                            <p>•	Updated Business Continuity Plan</p>
                                            <p>•	Communication Templates</p>
                                            <p>•	Cutover Stage Gate Reviews</p>
                                            <p>•	End User ID List and Authorization Matrix</p>
                                            <p>•	Final Preparation and Live Cutover Stage Gate Review Decision and sign-off</p>
                                            <p>•	Go / No-Go Decision Dashboard</p>
                                            <p>•	Go-Live Readiness Assessment document</p>
                                            <p>•	Post Go-live Support Coverage Schedule</p>
                                            <p>•	Sustainment Strategy (discuss with Jeff)</p>
                                            <p>•	Training Evaluation</p>
                                            <p>•	Updated Business Blueprint documents</p>
                                            <p>•	Updated Controlled Resumption Scripts</p>
                                            <p>•	Updated Defects Log</p>
                                            <p>•	Updated End User Training Materials</p>
                                            <p>•	Updated End User Training Schedule</p>
                                            <p>•	Updated Knowledge Transfer Schedule and Tracker</p>
                                            <p>•	Updated Live Cutover Schedule</p>
                                            <p>•	Updated Master Project Schedule</p>
                                            <p>•	Updated or New Functional and Technical Specification documents</p>
                                            <p>•	Updated Project Status Tracking Sheet</p>
                                            <p>•	Updated Integration Landscape Diagram</p>
                                            <p>•	Updated Transport Tracking Sheet</p>

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Final Preparation and Cutover Phase profitability</p>
                                            <p>•	Release of complete Solution as per the Business Blueprint Document</p>
                                            <p>•	Percentage of end users able to do their daily jobs in the new system at Go-Live</p>
                                            <p>•	Cutover schedule achieved (i.e. no additional downtime required)</p>

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The project may decide to use client defined templates and standards for the deliverables from within the Final Preparation and Cutover phase. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>
                                            <p>•	Certain activities of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>
                                            <p>•	Certain activities of this process may be delivered in another phase of the project, as per the contractual requirements.  This tailoring is documented in the Project’s Process</p>
                                            <p>•	In case the client or another team representing the client is performing some parts of the activities and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>

                                        </div>
                                    </td>
                                    </tr>     
                                    
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Project Preparation Phase Process.pdf">Download Project Preparation Phase Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>  
                                                                                              
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <%--<h2>Document Templates</h2>--%>                       
                        

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <%--<h3>Functional & Technical</h3>--%>     
                                <h4>Templates</h4>
                                <p><a href="#">1.  Communication Templates</a></p> 
                                <p><a href="References\Stage Gate Review.pptx">2. Cutover Stage Gate Reviews</a></p> 
                                <p><a href="References\User Auth  SAP ABAP Projects.xlsx">3. End User ID List and Authorization Matrix</a></p> 
                                <p><a href="#">4. Final Preparation and Live Cutover Stage Gate Review Decision and sign-off</a></p> 
                                <p><a href="#">5. Go / No-Go Dashboard</a></p> 
                                <p><a href="#">6.  Go-Live Readiness Assessment document</a></p>    
                                <p><a href="References\Post Go Live Support Plan - Template.docx">7.  Post Go-live Support Schedule</a></p>  
                                <p><a href="#">8. Sustainment Strategy</a></p> 
                                <p><a href="#">9. Updated Business Blueprint documents</a></p> 
                                <p><a href="References\Business Continuity Plan.docx">10. Updated Business Continuity Plan</a></p>
                                <p><a href="References\Controlled Resumption Scripts.xlsx">11. Updated Controlled Resumption Scripts</a></p>
                                <p><a href="#">12. Updated Defects Log</a></p>
                                <p><a href="References\A Day in the Life of a Tester.pptx">13. Updated End User Training Materials</a></p> 
                                <p><a href="References\End User Training Schedule.xlsx">14. Updated End User Training Schedule</a></p>    
                                <p><a href="References\Knowledge Transfer Schedule and Tracker.xlsx">15. Updated Knowledge Transfer Schedule and Tracker</a></p>  
                                <p><a href="References\Live Cutover Schedule - Template.mpp">16. Updated Live Cutover Schedule</a></p>
                                <p><a href="References\Master Project Schedule.mpp">17. Updated Master Project Schedule</a></p>
                                <p><a href="References\Project Status Tracking Sheet.xlsm">18. Updated Project Status Tracking Sheet</a></p>
                                <p><a href="#">19. Updated System Landscape Diagram</a></p>
                                <p><a href="#">20. Updated Transport Tracking Sheet</a></p>
                                <p><a href="#">21. Updated or New Functional and Technical Specification documents</a></p> 


                          </div>
<%--                          <div class="col-xs-6 col-md-4"> 
                                <h3>PMO & Testing</h3>   
                                <p><a href="https://life.oneattune.com/mlink/file/MzQ2MzY" target="_blank">1.  attune PMO Project Governance</a></p> 
                                <p><a href="https://life.oneattune.com/mlink/file/NDE4NTc" target="_blank">2.  attune ABAP development Guidelines</a></p>  
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTA">3.  attune Coding Standards Java</a></p>    
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTE">4.  attune Coding Standards Microsoft.Net</a></p> 
                          </div> --%>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>Business Transformation</h3>--%>
                                <h4>Guidelines</h4>
                                <p><a href="References\Branded aIR Diagram.pptx">1.  Branded aIR Diagram</a></p> 
                                <p><a href="References\Phase Process Summary.docx">2. Phase Process Summary</a></p> 

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>   
                                <p><a href="References\Onboarding Guide Sample.docx">1.  Onboarding Guide Sample</a></p> 
                                <p><a href="References\Project Organizational Chart Sample.pptx">2.  Project Organizational Chart Sample</a></p> 
                          </div>
                        </div>                        

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                        </div> 

                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                        </div> --%>


<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->

</asp:Content>
