﻿using attune_Document_Web.App_Start;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Helpers;
//using System.Web.Helpers;

namespace attune_Document_Web
{
    public class GoogleAuthManager
    {
        private static GoogleAuthManager _instance;
        private static readonly object Lock = new object();
        private bool _isConfigured;
        private GoogleAuthRequest _request;

        private GoogleAuthManager()
        {
        }

        public static GoogleAuthManager Instance
        {
            get
            {
                lock (Lock)
                {
                    return _instance ?? (_instance = new GoogleAuthManager());
                }
            }
        }

        public void Configure(GoogleAuthRequest authRequest)
        {
            _isConfigured = true;
            _request = authRequest;
        }

        public void ValidateRequest(HttpContext context)
        {
            if (!_isConfigured) throw new ArgumentNullException("Configuration is null");

            if (string.IsNullOrEmpty(context.Request.QueryString["code"]))
                context.Response.Redirect(_request.FormattedAuthUrl());
            else if (!string.IsNullOrEmpty(context.Request.QueryString["code"]))
            {
                RequestForAuthorization(context);

                RequestForUserInfomation();

                if (SessionProvider.LoggedUser != null
                    && !string.IsNullOrWhiteSpace(SessionProvider.LoggedUser.Domain)
                    && SessionProvider.LoggedUser.Domain == "attuneconsulting.com")
                    context.Response.Redirect(_request.RedirectUrl);
                else
                    context.Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private void RequestForUserInfomation()
        {
            dynamic data;
            using (var client = new WebClient())
            {
                var response = client.DownloadData(string.Format(_request.UserInfo, _request.Token));
                var result = Encoding.UTF8.GetString(response);
                data = Json.Decode(result);
            }

            SessionProvider.LoggedUser = new UserProfle()
            {
                Name = data.displayName,
                ProfileImageUrl = data.image.url,
                Domain = data.domain
            };
        }

        private void RequestForAuthorization(HttpContext context)
        {
            dynamic data;
            var values =
                new NameValueCollection
                    {
                        {"client_id", _request.ClientId},
                        {"client_secret", _request.Secret},
                        {"redirect_uri", _request.RedirectUrl},
                        {"code", context.Request.QueryString["code"]},
                        {"grant_type", "authorization_code"}
                    };

            // Post Request
            using (var client = new WebClient())
            {
                var response = client.UploadValues(_request.TokenUrl, values);
                var result = Encoding.UTF8.GetString(response);
                data = Json.Decode(result);

                var aCookie = new HttpCookie("gAuthUser");
                aCookie.Values["access_token"] = data.access_token;
                aCookie.Expires = DateTime.Now.AddSeconds(data.expires_in);
                //aCookie.Expires = DateTime.Now.AddSeconds(3);
                context.Response.Cookies.Add(aCookie);
                _request.Token = data.access_token;
            }
        }

        public bool IsAuthenticated(HttpContext context)
        {
            var coo = context.Request.Cookies["gAuthUser"];
            if (coo == null) return false;

            try
            {
                var arr = coo.Values.GetValues("access_token");
                if (arr.Length > 0)
                {
                    _request.Token = arr[0];
                    RequestForUserInfomation();
                }
            }
            catch (Exception e)
            { 
            }

            return true;
        }
    }


    public abstract class AuthBase
    {
        public string RedirectUrl { get; set; }
        public string ClientId { get; set; }
        public string Secret { get; set; }
        public abstract string AuthUrl { get; }
        public abstract string TokenUrl { get; }
        public string Token { get; set; }
        public string ResponseType { get; set; }
        public string Scope { get; set; }
        public string AccessType { get; set; }
        public abstract string FormattedAuthUrl();
    }

    public sealed class GoogleAuthRequest : AuthBase
    {
        public GoogleAuthRequest()
        {
            AccessType = "online";
            ResponseType = "code";
        }

        public override string AuthUrl
        {
            get { return "https://accounts.google.com/o/oauth2/auth"; }
        }

        public override string TokenUrl
        {
            get { return "https://accounts.google.com/o/oauth2/token"; }
        }

        public string UserInfo
        {
            get { return "https://www.googleapis.com/plus/v1/people/me?access_token={0}"; }
        }

        public override string FormattedAuthUrl()
        {
            return
                string.Format(
                    "{0}?client_id={1}&redirect_uri={2}&scope={3}&response_type={4}&access_type={5}",
                    AuthUrl, ClientId, RedirectUrl, Scope, ResponseType, AccessType);
        }
    }
}