﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AgilePostGoLiveSupport.aspx.cs" Inherits="attune_Document_Web.AgilePostGoLiveSupport" %>

<asp:Content ID="projectPreparationPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">--%>
                <%--<div class="row">
                    <div class="col-lg-12">

                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Preparation  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Post Go Live Support phase of the SAP implementation project lifecycle.  It covers the activities that need to be performed within this phase.</p>
                            <p>During Post Go Live Support, the focus is on providing support to the business users, stabilizing the production system performance, preparing to officially hand over support to the permanent support team, and getting ready to close the project.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on the expected deliverables and the contractual requirements.  The activities related to the Post Go Live Support phase that are defined here derive the process followed during this phase of the project.</p> 
                        </div>
                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        
                        <p>A complete list of Acronyms, Abbreviations, and Definitions can be found in the document entitled “aIR Terminology”, found in the Guidelines section of the aIR Methodology portal. </p>
	

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h3>Process and Steps</h3>     
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), derived from Agile principles, and PMI, SAP ASAP Focus, and SAP Activate methodologies. The aIR Methodology includes elements of Agile principles for business requirements gathering and development, while maintaining tried and true traditional approaches to ERP implementations.  Within the aIR methodology, the Post Go Live Support phase follows the Final Preparation and Cutover phase. </p>
                            <p>Within the Post Go Live Support phase, all of the activities are focused on providing support to the business users, stabilizing the production system performance, preparing to officially hand over support to the permanent support team, and get ready to close the project.  </p>
                            <p>The Post-Go Live Support phase begins immediately after the Steering Committee makes Decision # 4 to begin processing transactions at full volume.  This decision was made in the Final Preparation and Cutover Phase. </p>
                            <p>With the Go Live successfully completed, the communication is sent out to all relevant stakeholders that the organization is now live in the new production system.  The point of contact for support of each business process is shared as part of the communication. The support team members are organized based on the business processes (i.e. OTC, PTP, Finance, Retail, etc.).</p>
                            <p>End users log into the system, and start their day-to-day work. The accesses and the operations are monitored as part of the Post Go Live Support and any issues faced are captured using the Help Desk Tool defined as part of the Post Go Live support plan that was prepared in the Final Preparation and Cutover Phase. The issues are raised as Incidents which are then assigned to the relevant member from the customer’s project team who is providing the level 1 support. If the member from the customer project team cannot resolve the issue, it is then assigned to the attune project team member who provides the level 2 support. Hands-on “How To” support is provided onsite by both the customer’s project team and the attune consultants.  This face-to-face support continues until the end-users are comfortable using the system.</p>
                            <p>Post-Go Live support procedures that were covered in the kick-off meetings held during the Final Prep and Live Cutover Phase with the Support Team members and the end users are followed.  The defined SOP has the guidelines and steps to be followed when an issue is encountered in the Production System. It gives details as to how an Incident should be raised, what information needs to be maintained when creating an Incident, how to evaluate the severity of the issue, how the communication is sent out to the relevant people when an issue is raised, the details of the team responsible to provide support, and what steps are followed by the relevant team members (both customer and attune) when following up on the incidents raised.</p>
                            <p>The incidents raised can be either break/fixes or change requests. The customer project team and attune project team members analyze the issue and determine if the issue is a break/fix or a CR. For break/fixes, the efforts are captured in the Incident raised in the Help Desk Tool and the solution is provided as quickly as possible based on the level of severity to the business. If it is a CR, the project change request process is followed.  The project team (both customer and attune) members provide a work around until the issue or the CR is resolved. In the case of a CR which involves a new development or a change to an existing development, either a new functional specification is prepared or the existing functional specification is updated with the change. Additionally, the related training materials are also amended accordingly.</p>
                            <p>During the post Go Live support period, daily status meetings are held with the participation of the project team members (customer and attune) and the relevant stakeholders from the customer.  The status of the incidents raised are reviewed, the action plan is discussed, and dates are maintained to close these Incidents.  As the number of incidents reported and outstanding declines, the status meetings may be held less frequently, but not less than once a week.</p>
                            <p>A daily status report is sent out by the project Communications group to all project team members and stakeholders.  This report communicates how well the business ramp-up is progressing in comparison to metrics that were established prior to the cutover (e.g. orders processed, units shipped, inventory received, etc.).  The report also communicates the stability of the system in terms of the number of incidents reported and their severity as well as the status of outstanding issues.</p>
                            <p>It is critical that the IT team stabilize the production system performance as quickly as possible after go live to ensure that transaction volumes are being processed in the most optimal manner.  Job schedules are carefully monitored and tweaked as required.  EDI transactions are continuously scrutinized to ensure that they are successfully processed.  Interfaces are constantly watched to make certain that transactions are flowing smoothly between systems.  The system performance is monitored carefully by the infrastructure team and adjustments are made as required.  This includes servers, internet connections, printers, etc.</p>
                            <p>While the post Go Live support takes place, the attune project team conducts Knowledge Transfer sessions to the permanent support team (attune or customer) that is identified as per the continuous support terms agreed in the Post Go Live Support Plan. The identification of the team for permanent support is done during the Final Preparation and Cutover Phase as part of the Post Go Live Support Plan. A schedule is prepared for the KT with the dates and times together with the person responsible to conduct the training, and the schedule is shared with the permanent support team members. The permanent support team is broken down by SAP module, with a Lead assigned to every module. The KT is done covering all the business processes within each module as per the implemented solution described in the Business Blueprint documents, Functional/Technical specs, and Training materials. Along with the KT, the permanent support team gets access to customer’s systems and the Help Desk Tool.  They are trained on using the Help Desk tool as well. </p>
                            <p>With the KT done and the permanent support team having the required access to the Help Desk Tool, a controlled run takes place where the attune project team who performs the Level 2 support assigns certain incidents or CRs to the members in the permanent support staff. The attune project team members follow up on these incidents and CRs, and provide support to the permanent support team whenever required. The permanent support team members are invited to the Daily Status meetings and they provide feedback on the status of the tickets assigned to them. </p>
                            <p>The controlled run is then changed to a shadow run where the permanent support staff would take on the lead role, and manage the Support process with minimal involvement from the attune project team. In this run, the attune project team members would be more involved in a support role where they would shadow the permanent support team members and step in only when required such as when resolving certain complex issues or CRs. </p>
                            <p>The shadow run would then give way to where the permanent support team would completely take over the support, which results in the complete handover of support from the attune project team to the permanent support team. With the complete handover done, the attune project team then prepares for the official project closure. </p>
                            <p>The project closure activities involve conducting the lessons learned and documenting them. The documented lessons learned are then shared with all the relevant stakeholders from attune as well as the customer. These lessons learned are added to a central repository on ONE attune so that they can be referred to in the future when planning and executing similar projects. The lessons learned covers all the phases executed in the project. </p>
                            <p>With the lessons learned completed and shared with all relevant stakeholders, the next activity is to gauge the status of the production support in order to determine if the production system is stable and if the customer is able to continue either on their own or with the permanent support team (as agreed in the Post Go Live Support Plan). This would result in the decision to officially close the project and roll off the project team members. This decision is made at the Steering Committee and PMO level. </p>
                            <p>The details of the current support status are presented and discussed. The feedback from all the relevant stakeholders is obtained regarding the current status. If the relevant stakeholders from the customer agree that the production system is stable and that they are in position to either continue by themselves or with the permanent support team from attune, then the decision to close the project is taken. This decision is communicated to all concerned, and the project team is rolled off. The permanent support team continues to be engaged with the customer and provide the support as agreed in the Post Go Live Support plan.</p>
                            <p>If the decision is made to continue post go live support with the project team because the production system is not yet stable, the PMO and Steering Committee would agree on an extended support plan.  The required steps are taken for the continuous engagement until such time that the customer agrees that the project can be closed.</p>
                            <p>To prepare for project closure the Project Team makes all final adjustments to project documentation and places them in the customer’s documentation repository and in the project ONE attune workspace.  The attune PM finalizes the billings with the customer.  After the Post Go Live Support phase is officially closed with the customer, attune will complete the internal attune project closure activities as specified in the Project Closure process.  These activities include inactivating the related resource assignments in PSA, closing the project in PSA once all invoices have been produced, creating reusable components, doing project performance reviews, updating consultants’ skills and CV’s, etc.</p>
                            <p>Throughout the entire phase, the Project Status Tracking Sheet is updated with any issues, risks, decisions, assumptions, and action items taken.</p>
                            <p>The table below highlights each of the activities mentioned above in detail. It provides information on the activities, the responsible person or team, the documents created in each activity (i.e. artifacts) and the reference documents used. </p>


                            <h4>Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activities</th>
                                        <th>Responsible / Participants</th>
                                        <th>Artifacts</th>
                                        <th>Reference Documents</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="1"><strong>Post Go Live Support of Business Users</strong></td>
                                            <td><p><strong>1.	Support End Users</strong></p>
                                                <p>This task involves providing hands-on support for business users.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Decision #4 is made by the Steering Committee to resume full transaction processing. </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Communication is sent out to all relevant stakeholders that the organization is live in the production system.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The attune and customer project team, assigned per the support schedule to the respective business processes, are stationed across the different departments or functional areas such as Customer Service, Sourcing, Warehouse, Finance etc.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	End users in the respective areas log in to the system and carry out their daily activities using the new business processes that they have been trained on, such as order entry, creation of Purchase Orders etc.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	“How to” questions from business users are answered immediately by the attune and customer project team members assigned to their area.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The user activities are monitored and issues identified are logged into the Help Desk Tool by the Level 1 support team.</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The issues are assigned initially to the Customer’s project team members (Level 1 Support) and then assigned to the attune project team members (Level 2)</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Workarounds are defined for complex issues until a resolution is implemented.</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	In the first few days, most of the issues faced can be related to accesses and authorizations</p>
                                            </td>
                                            <td>PMO / Customer’s Core project team / FL / TL / FC / TC / SAP Security / BTS</td>
                                            <td>Updated End User Training Materials / SOP document for Production Support / Updated Go Live Readiness  Assessment document / Updated Business Continuity Plan </td>                                    
                                            <td>End User Training Materials / FRS Business Process Documents / To-Be Business Process Documents with process flow diagrams / Functional/ Technical specs / Post Go Live Support Plan / Controlled Resumption Scripts / Communication Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Post Go Live Support of Business Users</strong></td>
                                            <td><p><strong>2.	Resolve issues reported by business users</strong></p>
                                                <p>This task covers the resolution of issues reported by end users (Level 1 and 2) after Go Live.  The issues will fall into several categories such as data issues, business process issues, system break/fix issues, and change requests.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The issues are captured in the Help Desk Tool by the Level 1 support team member assigned </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Every issue is documented with an Incident number, the incident category, priority, module/area within which the Incident was identified, as well as the description of the issue and the relevant supporting documents (for example: Screen shots, emails etc.) are attached.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Incidents that are determined to be change requests follow the change request process for the project before they are assigned to anyone to resolve.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	All other Incidents are initially assigned to the team providing Level 1 support to analyze and resolve</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	If Level 1 cannot resolve the issue, it is assigned to Level 2</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Where a configuration or development is required to resolve the issue, thorough testing must be carried out and transport management procedures must be followed.</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The Incident is then followed up by the person responsible and closed once the end user confirms that it is resolved in production. It may be re-opened if the issue recurs</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	If the Incident is complex and requires more analysis, then a workaround is defined so that end users can continue their operations without any interruptions until a permanent fix is in place</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	The above steps are followed for each and every issue raised by the end users</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	All resolved Incidents are stored in the Help Desk Tool with all the relevant details such as the workaround used, the fix that was implemented and whether the fix was a development or configuration together with screen shots related to the workaround or fix.</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	Level 1 and Level 2 support team members can use these resolved Incidents as a knowledge base when providing support since similar issues maybe raised</p>
                                            </td>
                                            <td>Basis / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Updated Project Status Tracking Sheet / Updated Post Go Live Support Plan / Meeting Minutes of discussions taken place to discuss and overcome issues faced  / Approved Support Schedule / Updated Sustainment Strategy  / SOP document for Support</td>                                    
                                            <td>Transport Management Procedures / Detailed Project Plan / Project Status Tracking Sheet / Project Charter </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Post Go Live Support of Business Users</strong></td>
                                            <td><p><strong>3.	Manage Incidents</strong></p>
                                                <p>This task involves following up on the reported Incidents until closure. </p>
                                                <p>Open issues are constantly monitored by the Support Leads and the PMO to ensure that they are being resolved as quickly as possible and according to the turn-around times in the Support Plan for the level of severity of the issue.</p>
                                                <p>Daily status meetings are held by the PMO with the support team members and all of the relevant stakeholders participating.</p>
                                                <p>The current status of the Incident, the analysis done, the root cause for the issues, the resolution to be implemented, the target date by when the issues is expected to be closed are all discussed and agreed upon.  The Incidents per Module are reviewed one by one in the meeting.</p>
                                            </td>
                                            <td>PM  / DM   Team / Basis / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Updated Project Status Tracking Sheet / Meeting Minutes of discussions taken place to discuss and overcome issues faced / Incidents raised in the Help Desk Tool</td>                                    
                                            <td>Post Go Live Support Plan / Project Status Tracking Sheet</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Post Go Live Support of Business Users</strong></td>
                                            <td><p><strong>4.	Gather Change Requests</strong></p>
                                                <p>The Incidents raised by end users can also involve CRs.  The project’s Change Request process is followed.</p>
                                                <p>This step highlights how the CRs are handled. </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	When Incidents are raised, it is assigned to a member from the support team (either Level 1 or 2)</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The assigned person will complete an analysis of the incident in order to determine if this Incidents is a break fix or CR</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	If it is a CR, then the details on why the Incident was termed as a CR is  communicated (The communication maybe via email or via a discussion during the Daily or Weekly status meeting) </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The  details on why the Incident was termed as a CR is evaluated and agreement is reached to categorize the Incident as a CR</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	With the agreement reached, the impact analysis is performed to check the impact of implementing the CR in the existing solution</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The effort involved and the cost is also captured</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The effort, cost and impact analysis are shared with the relevant stakeholders from the customer for approval</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Once approved, the CR is developed or configured, tested and implemented</p>
                                            </td>
                                            <td>PM  / DM   Team / Basis / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Updated Project Status Tracking Sheet / Meeting Minutes of discussions taken place to discuss the CR  / Incidents raised for the CR in the Help Desk Tool / Updated Functional and Technical Specification / New Functional and Technical Specification / Emails with approval to proceed with the CR</td>                                    
                                            <td>Post Go Live Support Plan / Project Status Tracking Sheet</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Post Go Live Support of Business Users</strong></td>
                                            <td><p><strong>5.	Stabilizing the production system performance - IT Monitoring</strong></p>
                                                <p>This task involves proactive monitoring of the production environment by the customer’s IT department and attune Project Team members to ensure that the system is performing at optimal levels.  Adjustments are made to maximize performance as required.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Decision #4 is made by the Steering Committee to resume full transaction processing. </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Communication is sent out to all relevant stakeholders that the organization is live in the production system.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	All inbound and outbound EDI transactions are checked.  Failures are resolved immediately.  </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Interfaces are constantly watched to ensure that the data is flowing smoothly between systems.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Job schedules are monitored and tweaked, as needed.</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The infrastructure team monitors system performance including servers, printers, internet connections, etc. and makes adjustments where needed to enhance processing.</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	All system issues are reported, tracked, and fixed following the support procedures laid out in the Support plan and described in the above sections</p>
                                            </td>
                                            <td>Basis / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer / BTS</td>
                                            <td>Updated Project Status Tracking Sheet / Updated Post Go Live Support Plan / Meeting Minutes of discussions taken place to discuss and overcome issues faced  / Approved Support Schedule / Updated Sustainment Strategy  / SOP document for Support / Updated Job Schedule</td>                                    
                                            <td>Transport Management Procedures / Detailed Project Plan / Project Status Tracking Sheet / Project Charter / Job Schedule / Communication Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Post Go Live Support of Business Users</strong></td>
                                            <td><p><strong>6.	Publish Daily status communication</strong></p>
                                                <p>Daily status of post Go Live support is published using the related template format.</p>
                                                <p>Daily status includes updates on issues logged, issues fixed, open issues and current status of production related KPIs. Production KPIs can include DC processing, sales order processed, purchase orders processes, etc.</p>
                                            </td>
                                            <td>BTS / PM</td>
                                            <td>Daily status e-mails</td>                                    
                                            <td>Daily communication template, daily status meetings, incident reports</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Prepare to Transition Support to Permanent Support Team</strong></td>
                                            <td><p><strong>7.	Conduct KT sessions</strong></p>
                                                <p>The KT sessions to the permanent support team are carried out by the attune consultants.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The KT schedule is created with the dates and time and the person responsible to conduct the KT</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The meeting invites are sent out to all support team members</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The relevant documents required for the KT are updated and shared with all relevant members (for example: PPT decks, updated Functional and Technical Specs, Business Process documents and the Business Blueprint documents)</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The KTs cover each business process within each SAP module and the solution implemented to cover the business process in the system</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The KTs are conducted and transfer of knowledge is done</p>
                                            </td>
                                            <td>PM / SA / FL / TL / FC / TC / Basis / Permanent Support Team members </td>
                                            <td>KT Schedule / Updated Functional and Technical Specifications / Updated FRS Business Process Documents / Updated To-Be business process documents with process flow diagrams / Updated SOP document for Support  / Recordings of the KT sessions / Updated End User Training materials</td>                                    
                                            <td>Post Go Live Support Plan / SOP document for Support</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Prepare to Transition Support to Permanent Support Team</strong></td>
                                            <td><p><strong>8.	Train Permanent Support Team on Help Desk Tool</strong></p>
                                                <p>After KT is completed, the customer and/or attune consultants will train the permanent support team on the Help Desk Tool and procedures that will be used.  Access to the Help Desk tool is given to each permanent support team member.</p>
                                            </td>
                                            <td>PM / Basis / Permanent Support Team members</td>
                                            <td></td>                                    
                                            <td>Post Go Live Support Plan / SOP document for Support / Help Desk Tool Training documentation</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Prepare to Transition Support to Permanent Support Team</strong></td>
                                            <td><p><strong>9.	Controlled Support Run</strong></p>
                                                <p>The permanent support team begins to fix some issues under the guidance of the project team to get some real-life exposure to the system.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Development system and test system access is given to the permanent support team members after their KT is completed.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The Level 2 support team assigns a few incidents to the permanent support team.  The Level 2 project team members oversee the resolution of the incident and provide guidance.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Permanent support team members provide status on the issues assigned to them.</p>
                                            </td>
                                            <td>SA / FL / TL / FC / TC / Permanent Support Team members</td>
                                            <td>Updated Help Desk tickets</td>                                    
                                            <td>Updated Functional and Technical Specifications / Updated FRS Business Process Documents / Updated To-Be business process documents with process flow diagrams / Updated SOP document for Support  / Recordings of the KT sessions / Updated End User Training materials</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Prepare to Transition Support to Permanent Support Team</strong></td>
                                            <td><p><strong>10.	Shadow Support Run </strong></p>
                                                <p>When the controlled support run is deemed successful, the final step in the preparation of the permanent support team is to assume responsibility for the Level 2 support with minimal involvement from the attune project team members.  This will be done towards the end of the Post Go Live Support phase and continues until the phase ends and support is permanently transitioned.</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The permanent support team is assigned any new break/fix tickets as well as the newly approved CR’s. </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The attune Project Team members will shadow the permanent support team members and step in only when required.</p>
                                            </td>
                                            <td>SA / FL / TL / FC / TC Permanent Support Team members</td>
                                            <td>Updated Help Desk tickets</td>                                    
                                            <td>Updated Functional and Technical Specifications / Updated FRS Business Process Documents / Updated To-Be business process documents with process flow diagrams / Updated SOP document for Support  / Recordings of the KT sessions / Updated End User Training materials</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Prepare for Project Closure</strong></td>
                                            <td><p><strong>11.	Gather Lessons Learned</strong></p>
                                                <p>The project teams (attune and customer’s) document the lessons learned during the execution of the project. The lessons learned cover all of the issues, risks faced as well as what was done right and what was done wrong during every phase of the project.  Conclusions are categorized as “Stop, Start, and Continue” actions.</p>
                                                <p>Ideally, the lessons learned are captured at the conclusion of each project phase.  If this cannot be done, all phases are included in the final Lessons Learned session at the end of the project. </p>
                                                <p>The teams gather the lessons learned during:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Project Prep: Issues and risks faced and the lessons learned with regards to the:</p>
                                                    <p class="tab-content" style="padding-left: 40px">i.	High level effort estimations</p>
                                                    <p class="tab-content" style="padding-left: 40px">ii.	Costing</p>
                                                    <p class="tab-content" style="padding-left: 40px">iii.	Sizing and Scoping</p>
                                                    <p class="tab-content" style="padding-left: 40px">iv.	Resource planning and allocation</p>
                                                    <p class="tab-content" style="padding-left: 40px">v.	On-boarding of the resources</p>
                                                    <p class="tab-content" style="padding-left: 40px">vi.	Logistics such as organizing the rooms for the project teams, organizing the meetings etc.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	FRS Phase: Issues and risks faced and the lessons learned with regards to the:</p>
                                                    <p class="tab-content" style="padding-left: 40px">i.	FRS workshop scheduling</p>
                                                    <p class="tab-content" style="padding-left: 40px">ii.	Execution of the FRS workshops</p>
                                                    <p class="tab-content" style="padding-left: 40px">iii.	Making sure all the relevant stakeholders were involved on a full time basis</p>
                                                    <p class="tab-content" style="padding-left: 40px">iv.	Documentation of the related as-is business processes and making sure all the required details are maintained</p>
                                                    <p class="tab-content" style="padding-left: 40px">v.	Capturing and storing all of the emails, meeting minutes centrally for reference</p>
                                                    <p class="tab-content" style="padding-left: 40px">vi.	Solution designing based on the as-is processes</p>
                                                    <p class="tab-content" style="padding-left: 40px">vii.	Documentation of the To-Be processes and storage of these documents centrally for future reference</p>
                                                    <p class="tab-content" style="padding-left: 40px">viii.	Design Walkthrough and the involvement of all of the relevant stakeholders</p>
                                                    <p class="tab-content" style="padding-left: 40px">ix.	Creating the User Stories and Estimations</p>
                                                    <p class="tab-content" style="padding-left: 40px">x.	Creating the Product Backlog and estimations</p>
                                                    <p class="tab-content" style="padding-left: 40px">xi.	Planning the Sprints</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Agile Build and Test Phase, and IST Phase: Issues and Risks faced and the lessons learned with regards to the </p>
                                                    <p class="tab-content" style="padding-left: 40px">i.	Effort estimation of the User Stories </p>
                                                    <p class="tab-content" style="padding-left: 40px">ii.	The resource assignments to the Product Backlog </p>
                                                    <p class="tab-content" style="padding-left: 40px">iii.	The completion of the Functional and Technical specifications</p>
                                                    <p class="tab-content" style="padding-left: 40px">iv.	The review and approval process of the Functional Specifications with the involvement of the Agile teams</p>
                                                    <p class="tab-content" style="padding-left: 40px">v.	Collaboration between the Business Owners, BPEs, Functional and Technical Consultants during the Sprints</p>
                                                    <p class="tab-content" style="padding-left: 40px">vi.	The development of the different objects </p>
                                                    <p class="tab-content" style="padding-left: 40px">vii.	The monitoring of the developments</p>
                                                    <p class="tab-content" style="padding-left: 40px">viii.	The preparation for both cycles of integration testing</p>
                                                    <p class="tab-content" style="padding-left: 40px">ix.	The mock cutovers performed</p>
                                                    <p class="tab-content" style="padding-left: 40px">x.	The testing completed (unit and integration) and the tools used for testing</p>
                                                    <p class="tab-content" style="padding-left: 40px">xi.	The End-User Showcase that was done</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Final Prep and Cutover: Issues and Risks faced and the lessons learned with regards to the </p>
                                                    <p class="tab-content" style="padding-left: 40px">i.	The involvement of the relevant stakeholders during the final prep</p>
                                                    <p class="tab-content" style="padding-left: 40px">ii.	Extraction of data from the Legacy system</p>
                                                    <p class="tab-content" style="padding-left: 40px">iii.	Preparing the data in the required templates and formats</p>
                                                    <p class="tab-content" style="padding-left: 40px">iv.	The validation and approval of the data to be uploaded</p>
                                                    <p class="tab-content" style="padding-left: 40px">v.	The tools used for the upload</p>
                                                    <p class="tab-content" style="padding-left: 40px">vi.	The preparation of the production system</p>
                                                    <p class="tab-content" style="padding-left: 40px">vii.	The training plans</p>
                                                    <p class="tab-content" style="padding-left: 40px">viii.	The trainings conducted and the involvement of all end users in the training</p>
                                                    <p class="tab-content" style="padding-left: 40px">ix.	The communications related to the Cutover and Go Live</p>
                                                    <p class="tab-content" style="padding-left: 40px">x.	The involvement of all the relevant stakeholders during the Final prep and cutover</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Post Go Live Support: Issues and Risks faced and the lessons learned with regards to the:</p>
                                                    <p class="tab-content" style="padding-left: 40px">i.	End user support provided</p>
                                                    <p class="tab-content" style="padding-left: 40px">ii.	System stability</p>
                                                    <p class="tab-content" style="padding-left: 40px">iii.	The KT done to the Permanent support team</p>
                                                    <p class="tab-content" style="padding-left: 40px">iv.	The transition to the permanent support team with the controlled run and the shadow run of support</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	BTS activities done throughout the project for organizational change management, communication, and training:  Issues and Risks faced and the lessons learned.</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The lessons learned are documented in a “Start, Stop, and Continue” format and shared with all the relevant stakeholders from the customer and attune</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	The documents are then stored in the ONE attune Re-use repository for future reference</p>
                                            </td>
                                            <td>PGM / PM / Customer Core Project team / The Business Transformation Team  / attune project team</td>
                                            <td>Lessons Learned Documents  </td>                                    
                                            <td>Updated Detailed Project Plan / Project Charter / Project Kick Off presentation / Updated Project Status Tracking Sheet</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Prepare for Project Closure</strong></td>
                                            <td><p><strong>12.	Decision to End Post-Go Live and Transition to Permanent Support</strong></p>
                                                <p>In preparation for ending the customer project, the final Stage Gate Review takes place:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The review of the current project and support status with the Steering Committee</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The details are presented and the final decision taken to close the project</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The continuation of the production support is revisited and confirmed, as per the agreed terms and conditions</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The communication is shared with all relevant stakeholders regarding the closedown</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The attune project team is rolled off of the project</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The final payments are discussed and terms are agreed to raise the invoice and obtain the payments</p>
                                            </td>
                                            <td>Business Transformation Team / Project Steering Committee / PMO</td>
                                            <td>Communication Templates for Project Closedown / Post Go Live Stage Gate Review Decision</td>                                    
                                            <td>Updated Detailed Project Plan / Post Go Live Support Plan / Updated Project Status Tracking Sheet</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Prepare for Project Closure</strong></td>
                                            <td><p><strong>13.	Store all documents created and updated by attune during the Phase on ONEattune in the Project workspace</strong></p>
                                                <p>It is important to retain attune intellectual property on ONE attune.  This activity applies to every member of the team.  It includes documents used for an activity (e.g. FRS Workshop presentations) as well as deliverable artifacts.</p>
                                            </td>
                                            <td>All attune project team members</td>
                                            <td>Updated ONEattune Project work space</td>                                    
                                            <td>Documents used for an activity (e.g. FRS Workshop presentations) as well as deliverable artifacts. </td>
                                        </tr>
                                        
                                        

                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                                <p>•	All end users trained</p>
                                                <p>•	Master data migrated to new system and signed off by the customer</p>
                                                <p>•	All open transactions and inventory migrated to new system and signed off by the customer</p>
                                                <p>•	All cutover activities completed</p>
                                                <p>•	Controlled Resumption scripts executed and all defects resolved</p>
                                                <p>•	Released Production System to the end-users</p>
                                                <p>•	Go Decision by the Steering Committee to go to full business processing</p>
                                                <p>•	Post-Go Live Support ready to be executed</p>
                                                <p>•	All FRS Business Process Documents, System Landscape Diagram, and Functional/Technical Specification documents updated with changes related to approved CR’s and refinement of the solution during the Final Preparation and Cutover Phase</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                                <p>•	Business is fully operational on the new system and using the new business processes</p>
                                                <p>•	Production system is stable and performing according to metrics agreed with the customer</p>
                                                <p>•	Permanent Support Team and Help Desk tool in place for long-term Incident management</p>
                                                <p>•	Agreed Post Go Live Support period for Project Team completed</p>
                                                <p>•	Knowledge Transfer to Permanent Support team completed</p>
                                                <p>•	All Business Blueprint documents, Integration Landscape Diagram, and Functional/Technical Specification documents updated with changes related to approved CR’s and refinement of the solution during the Hyper-care period</p>
                                                <p>•	Lessons Learned published</p>
                                                <p>•	Completed Exit Stage Gate Review with all items passed. Exceptions approved by Steering Committee.</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">


                                                <p>•	Post Go Live Support Plan</p>
                                                <p>•	Post Go Live Support Procedures</p>
                                                <p>•	Approved support schedule</p>
                                                <p>•	Sustainment strategy</p>
                                                <p>•	Approved Controlled Resumption scripts</p>
                                                <p>•	Go Live Readiness Assessments completed and published</p>
                                                <p>•	Updated or New Functional and Technical Specification Documents</p>
                                                <p>•	Updated FRS Business Process Documents</p>
                                                <p>•	Updated End User Training Materials</p>
                                                <p>•	Updated Change Request Log</p>
                                                <p>•	Updated Project Status Tracking Sheet</p>
                                                <p>•	Knowledge Transfer Strategy</p>
                                                <p>•	Knowledge Transfer Tracking Sheet</p>
                                                <p>•	Daily Go Live Communication template</p> 
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">

                                                <p>•	Updated Post-Go Live Support Coverage Schedule</p>
                                                <p>•	Communication of Project Closedown decision</p>
                                                <p>•	Daily Post-Go Live Support Status Communications</p>
                                                <p>•	Knowledge Transfer Schedule and tracker</p>
                                                <p>•	Lessons Learned Report</p>
                                                <p>•	Post Go Live Stage Gate Review Decision</p>
                                                <p>•	Updated Business Blueprint documents</p>
                                                <p>•	Updated End User Training Materials</p>
                                                <p>•	Updated Go Live Readiness  Assessment document</p>
                                                <p>•	Updated incident log and resolutions</p>
                                                <p>•	Updated or New Functional and Technical Specification documents</p>
                                                <p>•	Updated Project Status Tracking Sheet</p>
                                                <p>•	Updated Sustainment Strategy </p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                                <p>•	Post Go Live Phase profitability</p>
                                                <p>•	Percentage of end users able to do their daily jobs in the new system after Go Live</p>
                                                <p>•	Effort variance for Post Go Live Support Phase</p>
                                                <p>•	Defects slippage</p>
                                                <p>•	CR Rate</p></div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                                <p>•	The project may decide to use client defined templates and standards for the deliverables from within the Final Preparation and Cutover phase. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>
                                                <p>•	Certain tasks of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>
                                                <p>•	Certain activities of this process may be delivered in another phase of the project, as per the contractual requirements.  This tailoring is documented in the Project’s Process</p>
                                                <p>•	In case the client or another team representing the client is performing some parts of the tasks and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>
                                         </div>
                                    </td>
                                    </tr>     
                                    
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Project Preparation Phase Process.pdf">Download Project Preparation Phase Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>  
                                                                                              
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <%--<h2>Document Templates</h2>--%>                       
                        

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <%--<h3>Functional & Technical</h3>--%>     
                                <h4>Templates</h4>
                                <p><a href="#">1.  Approved Support Schedule</a></p> 
                                <p><a href="#">2. Communication of Project Closedown decision</a></p> 
                                <p><a href="References\Stage Gate Review.pptx">3. Completed Exit Stage Gate Review with all items passed. Exceptions approved by Steering Committee.</a></p> 
                                <p><a href="#">4. Daily Post Go-live Status Communications</a></p> 
                                <p><a href="References\Knowledge Transfer Schedule and Tracker.xlsx">5. Knowledge Transfer Schedule and tracker</a></p> 
                                <p><a href="References\Lessons Learned User Focus Group.docx">6.  Lessons Learned Report</a></p>    
                                <p><a href="References\Stage Gate Review.pptx">7.  Post Go Live Stage Gate Review Decision</a></p>  
                                <p><a href="#">8. Updated Business Blueprint documents</a></p> 
                                <p><a href="References\A Day in the Life of a Tester.pptx">9. Updated End User Training Materials</a></p> 
                                <p><a href="#">10. Updated Go-Live Readiness  Assessment document</a></p>
                                <p><a href="References\Project Status Tracking Sheet.xlsm">11. Updated Project Status Tracking Sheet</a></p>
                                <p><a href="#">12. Updated Sustainment Strategy</a></p>
                                <p><a href="#">13. Updated incident log and resolutions</a></p> 
                                <p><a href="#">14. Updated or New Functional and Technical Specification documents</a></p>
                                

                          </div>
<%--                          <div class="col-xs-6 col-md-4"> 
                                <h3>PMO & Testing</h3>   
                                <p><a href="https://life.oneattune.com/mlink/file/MzQ2MzY" target="_blank">1.  attune PMO Project Governance</a></p> 
                                <p><a href="https://life.oneattune.com/mlink/file/NDE4NTc" target="_blank">2.  attune ABAP development Guidelines</a></p>  
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTA">3.  attune Coding Standards Java</a></p>    
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTE">4.  attune Coding Standards Microsoft.Net</a></p> 
                          </div> --%>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>Business Transformation</h3>--%>
                                <h4>Guidelines</h4>
                                <p><a href="References\Branded aIR Diagram.pptx">1.  Branded aIR Diagram</a></p> 
                                <p><a href="References\Phase Process Summary.docx">2. Phase Process Summary</a></p> 

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>   
                                <p><a href="References\Onboarding Guide Sample.docx">1.  Onboarding Guide Sample</a></p> 
                                <p><a href="References\Project Organizational Chart Sample.pptx">2.  Project Organizational Chart Sample</a></p> 
                          </div>
                        </div>                        

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                        </div> 

                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                        </div> --%>


<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->

</asp:Content>
