﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="MeasurementandAnalysis.aspx.cs" Inherits="attune_Document_Web.MeasurementandAnalysis" %>

<asp:Content ID="measurementAndAnalysisPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Measurement and Analysis Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The objective of Measurement and Analysis Process is to effectively monitor the projects, support groups and the implementation of process needs. All measures are mapped to organization business objectives and align to attune policies. Appropriate data to be collected to support information needs. Fulfill the data needs to be managed and analyzed for informed decision making and corrective actions. </p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The scope of the Measurement and Analysis process focuses on planning and implementing data collection and analysis. It addresses identification of organization business objectives, data needs, defining procedures to gather, store and analyze data, collecting data, analyzing it, and making the results available. These results should help leadership team on decision making process. Measures should be focus prevention and detection aspects in project level as well as other project support sub unites.  SAP or Software Development (SWD) project execute in attune should defined respective metrics from organization defined list with the project startup. In the time of project execution PM / PL should collect data, analyze those respective metrics and if there is a deviation corrective actions should be implement.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>TL  - </strong>Technical Lead</p>                                
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>QA  - </strong>Quality Assurance</p>

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PC - </strong>Process Consultant</p>
                                <p><strong>MAA  - </strong>Measurement And Analysi</p>
                                <p><strong>SOW - </strong>Statement of Work</p>
                          </div>
                          <div class="col-xs-6 col-md-4">  
                                <p><strong>PAD - </strong>Project Approach Document</p>
                                <p><strong>SWD  - </strong>Software Development</p>
                                <p><strong>MSA - </strong>Master Service Agreement</p>
                          </div>

                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>During the early stages of the project, the PM/TL, with the support of the Process Consultant, creates the Project Measurement Plan. The plan addresses the measurement and analysis related activities needed to support the information needs of the project.</p>
                            <p>During the execution of the project, the Project Measurement Plan is executed to collect identified data, analyze data and provide results of data collection and analysis to relevant stakeholders. The PM is responsible for monitoring respective measures and take necessary actions on deviations. </p>

                            <h4>2.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="8"><strong>Plan </strong></td>
                                            <td><p>Organization leadership team define respective metrics for the organization based on organization business objectives. This is communicate to the all project managers and project leads.</p> </td>
                                            <td>PMO team</td>
                                            <td>attune MOD</td>                                    
                                            <td>Business objective </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM / PL receives project go-ahead from attune senior management & or customer. </p></td>
                                            <td rowspan="7">PM / PL, TL</td>
                                            <td rowspan="7">Project Charter / PAD</td>
                                            <td rowspan="7">SOW,  Client Project charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM / PL with help from the team identifies the measures to be used in the projects.  PM goes through the Organizational Level Metrics Definition Sheet and selects the compulsory metrics and decides whether any of the optional measures are applicable for the project based on client provided SOW or charter document. If there are any project specific metric available and team should track, required information will be documented in the Project Approach Document under measurement plan.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>For each information need, the PM defines the measurement objectives. Measures required to address each measurement objective are identified. The guidelines available are used to identify typical measures. Traceability between the specific measures, the measurement objectives and the information needs they pertain to, is maintained. </p>
                                                <p>The Process Consultant supports the PM in these activities.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>For each identified measure, the PM, with the help of the team, defines what the measure is and how it will be gathered and stored.  Required information’s are  defined in Project approach document under measurement plan section.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM, with the help of the team, defines the analysis to be performed on the collected data. This definition addresses the following:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The analyses to be performed (aimed to meet the measurement objectives, and information needs and objectives)</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The initial analysis to be performed, criteria for deciding whether further detailed analysis is needed, and the detailed analysis that may be needed</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	The data analysis methods and tools to be used for the analyses</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The reporting of the analysis done, including the frequency of reporting, the content and format of the report and the recipients</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Method to provide any assistance required by the recipients of the analyses reports to understand the reports</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM revalidates the measures and measurement objectives already identified based on the detailed definition of measures, the procedures of their collection, storage, retrieval, and the analyses and reporting. </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The Project Manager, respective development manager ( SWD / SAP) review the Project approach document.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                       <tr>
                                            <td rowspan="7"><strong>Execute </strong></td>
                                            <td><p>As specified in the PAD, the Project Manager orient the project members on the role, responsibilities, authority and value of project measurement activities as and when project members join the project. They ensure that team members are aware of the importance of measures and analysis to support informed decision making and corrective actions. </p></td>
                                            <td rowspan="7">PM, TL,  PC</td>
                                            <td rowspan="7"></td>                                    
                                            <td rowspan="7">aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM ensures that data is collected and checked for completeness and integrity as specified in the PAD.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM ensures that data analysis and reporting are performed. </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM ensures that measurement data and analyses results are stored in one attune.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The Director of Delivery periodically reviews the execution of the PAD with the PM and respective managers.</p> 
                                                <p>The review includes progress on the defined measurement and analysis activities and their suitability in meeting the information needs and objectives of the project. Problems faced in obtaining measures, performing the analyses and using the analyses reports to take decisions are also discussed. </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Based on the above activities, the Project Manager changes the PAD, if required and baselines the changed plan. </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM / PL share project data with respective  prepares a report of Project MAA activities for the Group Manager </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>

                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Project high level schedule and delivery model</p> 
                                            <p>•	Proposed contract (SOW)</p> 
                                            <p>•	Customer charter presentation / document</p> 
                                            <p>•	Project created in PSA</p> 
                                            <p>•	PM or Project Lead, Functional and Technical Leads are assigned to the project</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Project Closure </p> 
                                            <p>•	SOW direction of client won the project execution</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Proposed contract (SOW)</p> 
                                            <p>•	Working papers (including the estimation sheets) accompanying the proposal/ contract</p> 
                                            <p>•	Customer charter presentation / document</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	PAD </p> 
                                            <p>•	Review records of PAD </p> 
                                            <p>•	Weekly Project Status Tracking Report</p> 
                                            <p>•	Weekly operations status report</p> 
                                            <p>•	Project closure report</p> 
                                            <p>•	Attune MOD </p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Cumulative % of metric deviations ( per week) </p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>Measurement analysis is a mandatory for all types of projects.</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Project should give high priority on any client defined metrics.</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Any project running in pure customer location and under customer project management responsibility, respective measure will decided by relevant project manager.</p> 
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>                       
                                    <p><a href="References\attune MOD.xlsx">1. Metric Objective Description</a></p>                    
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>                                                     
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                    
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>
