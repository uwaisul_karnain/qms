﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="FinalPreparationAndCutover.aspx.cs" Inherits="attune_Document_Web.FinalPreparationAndCutover" %>


<asp:Content ID="finalPreparationPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Final  Preparation  &  Cutover  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Final Preparation and Cutover phase of a SAP implementation project lifecycle.  It covers the activities that need to be performed within this phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p>•	The expected deliverables</p>
                            <p>•	The contractual requirements</p>
                            <p>•	The methodology imposed by the client</p>


                            <p>The activities related to the Final Preparation and Cutover phase that are defined here derive the process followed during this phase of the project.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>BBP - </strong>Business Blueprint Document</p>
                                <p><strong>BP - </strong>Business Process</p>
                                <p><strong>BPO - </strong>Business Process Owner</p>
                                <p><strong>CR - </strong>Change Requests</p>
                                <p><strong>FC - </strong>Functional Consultant</p>                 
                                <p><strong>FICO - </strong>Finance and Controlling</p>    
                                <p><strong>FL - </strong>Functional Lead</p>
                                <p><strong>FS - </strong>Functional Specification</p>                 
                                <p><strong>MM - </strong>Materials Management</p> 
                                <p><strong>PC - </strong>Project Charter</p>     
                          </div>
                          <div class="col-xs-6 col-md-4">                                
                                <p><strong>PGM - </strong>Program Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PMO - </strong>Project Management Office</p>                 
                                <p><strong>PP - </strong>Planning and Production</p>    
                                <p><strong>PPL - </strong>Project Plan</p>
                                <p><strong>PSD - </strong>Professional Services Director</p>                 
                                <p><strong>RICEF - </strong>Reports, Interfaces, Custom Enhancements and Forms</p> 
                                <p><strong>RM - </strong>Resource Manager</p> 
                                <p><strong>SA - </strong>Solution Architect</p>
                                <p><strong>SD - </strong>Sales and Distribution</p>
                          </div>
                          <div class="col-xs-6 col-md-4">                                
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TC - </strong>Technical Consultant</p>                 
                                <p><strong>TL - </strong>Technical Lead</p>    
                                <p><strong>TR - </strong>Transport Request</p>
                                <p><strong>TS - </strong>Technical Specification</p>                 
                                <p><strong>UAT - </strong>User Acceptance Testing</p> 
                                <p><strong>WBS - </strong>Work Breakdown Structure</p> 
                                <p><strong>WP - </strong>Work Package</p>
                          </div>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h2>Process and Steps</h2>    
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived using the SAP ASAP Focus methodology. Within the aIR methodology, the Final Preparation and Cutover phase follows the Realization phase. </p>
                            <p>Within this phase, the activities start with the continuation and completion of the End User training, which was kicked off during the Integration Testing and UAT in the Realization Phase. The training continues until all of the business users involved in all of the different business processes are trained and have been given the knowledge required to continue their day to day tasks in the new SAP system after Go Live</p>
                            <p>While the training is being conducted, the discussions to confirm the Cutover plan takes place.  The Cutover Plan prepared during the Integration Testing and UAT phase which was used for the mock cutover 2 (i.e. Dry Run) is further reviewed, fine-tuned and validated based on the results achieved during the mock cutover. The fine tuning can be on the sequence of the steps to be followed, the data load involved in migration, the number of resources and tools involved, the timing of the steps to be completed etc. Once the Cutover Plan is reviewed and fine-tuned, it is then shared with all the relevant stakeholders from the customer by the project team (both attune and customer’s core team). The plan is validated and confirmed by all and is agreed upon. Once agreed, the project team would then be using the confirmed plan to execute the Cutover.</p>
                            <p>With the Cutover plan in place, the Go Live readiness of the organization is assessed. Here, the project teams (both attune and Customer’s core team) work together with the business users, business process leads, business process owners, Heads of departments and all relevant stakeholders from the customer to identify and collect the information that needs to be maintained or loaded into the new system as part of the Cutover (i.e. data conversion). These data can be in terms Master data such as the customer information, vendor information, bank account information and the balances, product or services information, pricing information etc. The data can also be the transactional data such as open Customer Orders, open Purchase Orders to be delivered from vendors, Accounts Payables, Accounts Receivables etc.  The information is collected and documented. The documentation is done using the formats and templates required for the defined conversion tools. The data documented is validated and confirmed by all the relevant stakeholders from the customer in order to confirm that the data to be loaded into the new system. The confirmation can be with a sign off, if required. </p>
                            <p>Apart from the data gathering for Cutover, the assessment would also cover the tasks where the project Steering Committee together with the customer’s Management team would create the organizational awareness on the impending Go Live. The communications made, the readiness of the business users, the change management (i.e. business transformation) activities etc. are all looked into and the necessary actions are taken to ensure the whole organization is ready. </p>
                            <p>With the assessment done, the Cutover is then executed. The production system already prepared during the Integration Testing and UAT phase of Realization is used to move the TRs so that the To-Be solution is in place, the validated data is uploaded as part of data migration into the production system, the business users’ log-in credentials are created with the required accesses and authorizations, validate the migrated data, and finally, the decision to Go or No-Go is made. If the decision is a Go, then the completed production system is released to the business users and the organization is Live on the new SAP system. </p>
                            <p>If in case an issue occurs, these issues are resolved in order to complete the Cutover. These issues, discussions, actions taken are all updated in to the Project Status Tracking Sheet. If in case the decision is a No-Go, then the next steps are discussed, and the required actions are taken to overcome the showstoppers that led to the No-Go decision. The Project Status Tracking Sheet is updated with the all issues, risks, dependencies, assumptions, and action items.</p> 
                            <p>With the Go decision, the teams then focus on the next steps which is to put in place a support plan or the business continuity plan. The plans is a roadmap on how the customer is to be supported in their operations, post implementation. With the execution of the plan, the day to day issues the business users may face when performing their tasks in the new system or CRs that are required to ensure the implemented solution is fine tuned to meet the needs of the organization are managed. The plan is discussed with all the relevant stakeholders from the customer and attune, and the final agreement is reached, and the organization moves towards the execution of the plan, which is covered in the Post Go Live Support Phase and Project Closure. </p>
                            <p>The table below highlights each of the activities mentioned above in detail. It provides information on the activities, the responsible person or team, the documents created in each activity (i.e. artifacts) and the reference documents used. </p>


                            <h4>1.	Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="5">Final Prep and Cutover Execution</td>
                                            <td><p><strong>1.	Complete End User Training</strong></p>
                                                <p>This task, which is continued from the Integration Testing & UAT from the Realization Phase, is completed within this phase. The training is conducted by the Customer’s core project team with the support of the attune project team.</p>
                                                <p>For more details on how the training is conducted, please read task number “4 Conduct End User Training” on page 13 of the process document for “Realization – Integration Testing & UAT”. </p></td>
                                            <td>Customer’s Core project team / FC / SAP Security </td>
                                            <td>Updated End User Training Schedule / Updated End User Training Materials</td>                                    
                                            <td>Training Schedule used for testing team and core team training / Training Materials used for testing team and core team training / Business Blueprint Document / To-Be Business Process Documents with process flow diagrams</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>2.	Determine Go Live Readiness</strong></p>
                                                <p>The tasks performed in this step include:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	All the data to be moved from the Legacy system to the new SAP system is identified and prepared.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The data is extracted from the Legacy system and maintained the formats and templates required so that these can be migrated using the already defined Data Conversion Tools that are in place</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The prepared data are shared with all the relevant stakeholders from the customer</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The data is reviewed and validated   </p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Any required changes are done to confirm the accuracy of the data</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The confirmed data are signed off by the relevant stakeholder from the customer</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The signed off data are shared with the attune project team in order to load the data into the new system as part of the migration</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Additionally, the project Steering Committee and the PMO ensure that the organization is ready for the Go Live by ensuring:</p>
                                                <p class="tab-content" style="padding-left: 40px">i.	The communications prepared during the Integration Testing and UAT phase is shared across the organization</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	The required Change Management (Business Transformation) activities are executed to ensure that the organization is ready for the impending Go Live. </p></td>
                                            <td>PGM / PM / Customer’s Core Project team / SA / FL / TL / FC / TC / The customer’s Management team / Steering Committee / All relevant stakeholders from Customer</td>
                                            <td>Confirmed and Signed off Data sheets with the extracted data from the Legacy System to be migrated to new SAP System / Updated Visual and Email Communications of Go Live awareness</td>
                                            <td>Visual and Email communications prepared during the Integration Testing & UAT Phase</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>3.	Execute Cutover</strong></p>
                                                <p>Within this activity the following takes place: </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Final Transport Tracking sheet is confirmed with the TRs in the right sequence</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The TRs are moved the already prepared Production System in order to move the Configured and Developed Objects into the new system</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	In case any issues arise during the TR movement, the issues are documented, discussed and the necessary actions are taken to resolve these issues</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The moved objects are checked and validated in order to confirm that the complete To-Be solution is in place in the new Production System</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The confirmed and signed off Data sheets with the data from the Legacy System which needs to be migrated are loaded into the new production system</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The list of user IDs  are created with the relevant authorizations and accesses for the business users</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	All connections/interfaces to third party systems are set up from the Production system</p></p></td>
                                            <td>PM / SA / FL / TL / FC / TC /  SAP Security / Customer’s core project team </td>
                                            <td><p>Updated Project Status Tracking Sheet / Updated Cutover Plan</p>
                                                <p>Updated Authorization Matrix and User ID List </p></td>
                                            <td>Cutover Plan / TR Tracking Sheet / RICEF Object List / List of Configuration Objects / Detailed Project Plan / Project Status Tracking Sheet / Authorization Matrix and User ID List / Confirmed and Signed off Data sheets with the extracted data from the Legacy System to be migrated to new SAP System / Business Blueprint Document</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>4.	Validate Converted Data, Make “Go / No-Go” Decision and Release Production System</strong></p>
                                                <p>With the data converted and migrated, this task is review, validate and confirm that the production system is ready to be released. The following takes place in this step:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The relevant stakeholders from the customer, business process owners, Heads of Departments, business users review the data that has been migrated and confirm that the data in the new system is accurate</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Once the validation is done, the decision is made whether to release the production system (i.e. the Go / No-Go decision) </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	If it is a No-Go, then the issues are discussed with all relevant stakeholders from the customer, and the required actions are taken to resolve the issues and proceed</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	If required, SAP’s help may be requested to rectify the showstopper</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	All the issues and actions taken are updated into the Project Status Tracking Sheet</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Once all issues are rectified, the review is done again, and the Go / No-Go Decision is made</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	If it’s a Go, then the production system is released for the business users</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	If it’s still a No-Go, then the next steps are discussed with all the relevant stakeholders from the customer, Steering Committee, and Project PMO</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	The discussions and decisions made are updated into the Project Status Tracking Sheet</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	It may be possible that the release of the Production System may be put on hold until every issue is sorted</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	It is possible that the Legacy System may be used until the release of the new SAP production system finally takes place</p></td>
                                            <td>Customer’s Core project team / PM / SA / FL / TL / FC / TC / SAP Security / Steering Committee / All relevant Stakeholders from the Customer / Others (SAP Support)</td>
                                            <td>Updated Project Status Tracking Sheet / Released Production System</td>
                                            <td>Updated Project Status Tracking Sheet / Updated Cutover Plan / Confirmed and Signed off Data sheets with the extracted data from the Legacy System to be migrated to new SAP System</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>5.	Prepare Support Plan and Execute Business Continuity</strong></p>
                                                <p>This involves preparing the Support Plan and ensuring the business continuity in the new SAP System. The following activities are completed here:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Support Plan to provide continuous support to the business users is drawn</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The standard operating procedures are discussed and prepared</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The team to provide continuous support is identified and assigned</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The Knowledge Transfer is planned from the project team to the support teams</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The Tool to be used to log issues and CRs is identified and implemented</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	All of the Business users, attune project team, customer’s core project team and the identified support team members are all trained on the new Tool</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	Business Users log the defects encountered into the tool</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	CRs required to further fine tune the solution is logged so that the CRs can be delivered</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	Missing authorization and accesses are provided</p>
                                                <p class="tab-content" style="padding-left: 20px">All of the above tasks ensure that the business users can continue their day to day tasks in the new system. Thereby, ensuring the business continuity</p></td>
                                            <td>PGM / PM  / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer / Steering Committee</td>
                                            <td>Approved Support Plan / Knowledge Transfer Schedule</td>
                                            <td>Updated Project Status Tracking Sheet / Business Blueprint Document</td>
                                        </tr>

                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Completed Integration Testing</p> 
                                            <p>•	Completed UAT</p>
                                            <p>•	UAT Sign Off</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Completed Cutover</p>
                                            <p>•	Validation and Sign Off on the Converted Data</p>
                                            <p>•	Go Decision</p>
                                            <p>•	Released Production System</p>
                                            <p>•	Approved Support Plan</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Business Blueprint Document</p>
                                            <p>•	Detailed Project Plan</p>
                                            <p>•	Transport Request Tracking Sheet</p>
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	All required End User Training Materials</p>
                                            <p>•	End User Training Schedule</p>
                                            <p>•	Cutover Plan</p>
                                            <p>•	User ID List and Authorization Matrix</p>
                                            <p>•	Visual / Email Communications (Optional)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Updated Detailed Project Plan</p>
                                            <p>•	Updated Transport Tracking Sheet</p>
                                            <p>•	Updated Project Status Tracking Sheet</p>
                                            <p>•	Updated End User Training Schedule</p>
                                            <p>•	All required and updated End User Training Materials</p>
                                            <p>•	Cutover Plan</p>
                                            <p>•	Confirmed Data sheets for Migration</p>
                                            <p>•	User ID List and Authorization Matrix</p>
                                            <p>•	Meeting Minutes of discussions held</p>
                                            <p>•	Visual / Email Communications (Optional)</p>
                                            <p>•	Approved Support Plan</p>
                                            <p>•	Knowledge Transfer Schedule</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Release of complete Solution as per the Business Blueprint Document</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The project may decide to use client defined templates and standards for the deliverables from within the Blueprinting phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>

                                            <p>•	Certain tasks of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>

                                            <p>•	In case the client or another team representing the client is performing some parts of the tasks and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document</p>

                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <h3>Document Templates</h3>                       
                                <p><%--<a href="References\attune System Requirement Specification.docx">1. attune System Requirement Specification</a>--%></p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Guidelines</h3>                                                     
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Samples</h3>
                          </div>
                        </div>                                                           
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>



