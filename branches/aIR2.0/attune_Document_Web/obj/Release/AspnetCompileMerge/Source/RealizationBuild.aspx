﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="RealizationBuild.aspx.cs" Inherits="attune_Document_Web.RealizationBuild" %>

<asp:Content ID="RealizationBuildPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Realization - Build  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Build phase of Realization of a SAP implementation project lifecycle.  It covers the activities that need to be performed within the Build phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The expected deliverables</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>
                            <p class="tab-content" style="padding-left: 20px">•	The methodology imposed by the client</p>

                            <p>The activities related to the Build phase that are defined here derive the process followed during this phase of the project.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>BBP - </strong>Business Blueprint Document</p>
                                <p><strong>BP - </strong>Business Process</p>
                                <p><strong>BPO - </strong>Business Process Owner</p>
                                <p><strong>CR - </strong>Change Requests</p>
                                <p><strong>FC - </strong>Functional Consultant</p>
                                <p><strong>FICO - </strong>Finance and Controlling</p>
                                <p><strong>FL - </strong>Functional Lead</p>
                                <p><strong>FS - </strong>Functional Specification</p>
                                <p><strong>MM - </strong>Materials Management</p>
                                <p><strong>PC - </strong>Project Charter</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PGM - </strong>Program Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PMO - </strong>Project Management Office</p>
                                <p><strong>PP - </strong>Planning and Production</p>
                                <p><strong>PPL - </strong>Project Plan</p>
                                <p><strong>PSD - </strong>Professional Services Director</p>
                                <p><strong>RICEF - </strong>Reports, Interfaces, Custom Enhancements and Forms</p>
                                <p><strong>RM - </strong>Resource Manager</p>
                                <p><strong>SA - </strong>Solution Architect</p>
                                <p><strong>SD - </strong>Sales and Distribution</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>TR - </strong>Transport Request</p>
                                <p><strong>TS - </strong>Technical Specification</p>
                                <p><strong>UAT - </strong>User Acceptance Testing</p>
                                <p><strong>WBS - </strong>Work Breakdown Structure</p>
                                <p><strong>WP - </strong>Work Package</p>
                          </div>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h2>Process and Steps</h2>    
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived using the SAP ASAP Focus methodology. Within the aIR methodology, the Realization phase follows the Business Blueprint phase. The Realization phase is broken further into two, namely Build and Integration Testing & UAT. The Build part of the Realization covers the activities performed to realize the proposed solution mentioned in detail in the Business Blueprint Document.</p> 
                            <p>It is in this phase that the project team defines the WBS or the WP, as per the finalized RICEF and Configuration list for every business process, with the agreed naming conventions. The resources from the project team (the FCs and TCs) are then allocated to each WBS or WP. The required specification documents for the RICEFs and Configurations are prepared by the resources, which are then reviewed together with the relevant stakeholders from the customer, and the necessary approvals are obtained prior to starting the development or configuration. The developments are carried out as per the given standards and guidelines. Developments and configuration are tracked using the tracking sheet in order to ensure that the progress is as per the plan. The unit testing is completed for the configured and developed objects, one by one, in order to ensure that these objects are as per the requirements and are fit for the purpose, before it is provided to the business users to test.</p> 
                            <p>Prior to the activities mentioned above, the systems are put in place and the system landscape defined in the Project Charter is set up.  </p>
                            <p>Additionally, the ground work required to start the Integration testing and UAT is done in this phase as well. The end to end business scenarios which are to be used for the Integration testing and UAT are defined. The test scripts are prepared as per the defined scenarios and validated by the relevant stakeholders from the customer. The detailed project plan is updated to define the timeline for the testing. A schedule is prepared on the Integration testing and UAT and is shared with the relevant stakeholders from the customer.</p>
                            <p>In parallel, a training schedule is defined in order to train the users who are to be part of the testing team confirmed by the customer. The detailed project plan would also include the timelines for the training. The required training materials are created as well. </p>
                            <p>The Project Status Tracking Sheet is used to document all Issues, CRs, Assumptions, Risks, Action Items highlighted and discussed during this phase.</p>
                            <p>Therefore, the table below highlights each of the activities mentioned above in detail. It provides information on the activities, the responsible person or team, the documents created in each activity (i.e. artifacts) and the reference documents used. </p>


                            <h4>1.	Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="7">Realization - Execution of Build</td>
                                            <td><p>1.	Configure the system</p>
                                                <p>The system landscape is defined and set up as per the Landscape details provided in the Project Charter. The system landscape would include:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The setup of the Development box with the separate clients clearly identified for:</p>
                                                <p class="tab-content" style="padding-left: 40px">i.	Configuration only client: Also known as the Golden client. This is the system purely for configuration changes.</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	Development only client: This is the system where all developments are carried out.</p>
                                                <p class="tab-content" style="padding-left: 40px">iii.	Unit Testing Client: This system would consist of both Configuration changes and Development changes in order to perform Unit Testing.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The setup of the Quality box where all the Integration Testing and UAT takes place</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The details explaining the procedures on how a configuration and development change done in the respective systems are moved across into the Unit testing and Quality system (with an approval process if applicable). These procedures are mentioned are mentioned in the Project Charter Document, and re-iterated during this phase</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The details on  continuation of the Sandbox system which was setup during the Business Blueprint phase (Optional)</p></td>
                                            <td>PM / SAP Security</td>
                                            <td>System Landscape Diagram</td>                                    
                                            <td>Project Charter / Project Kick Off DocumentSOW, RFP,  Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>2.	Prepare Specification Documents</p>
                                                <p>This task covers the activities that finally lead to the review and approval of the Functional Specifications. The list of activities include:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The preparation of the WBS or WP to be assigned to the project resources in order to complete the identified RICEF and Configuration objects. </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Allocation of the WBS or WP to the project resources, both functional and technical</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Update of the detailed project plan with the WBS or WP information together with the responsible person </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Derive the timeline for completion of the FS, review and approval of the FS, completion of the TS, and completion of Unit testing of the objects that were configured or developed</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Share the timeline with the expected completion dates to the project team and all relevant stakeholders in order to create the awareness</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Preparation of the FS based on the WP or WBS assigned. This specifications may be for:</p>
                                                <p class="tab-content" style="padding-left: 40px">i.	Configuration objects: The details on the configuration nodes in SAP followed to define the configuration object. Can include screen shots of the configuration (It is optional to prepare a FS document for configuration objects)</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	Development Objects: the details on the purpose and use of the object with applicable test cases, together with the logic to be used to develop the object</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	Prepare the Build Tracker for the WP or WBS in order to track the progress of the configurations and developments.</p></td>
                                            <td>PM / SA / FL / TL / FC / TC </td>
                                            <td>Functional Specification Document for Development or Configuration Objects / Build Tracker / Updated Detailed Project Plan</td>                                   
                                            <td>RICEF List and Configuration List / Business Blueprint Document</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>3.	Build Objects</p> 
                                                <p>Within this activity the following takes place:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The review and the approval of the Functional Specification by the relevant stakeholder from the Customer</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Once the approval is obtained, the TS is prepared (this is only for development objects)</p>  
                                                <p class="tab-content" style="padding-left: 20px">c.	The TS is then reviewed by the TL</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	With the review, certain update may take place in the TS</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	With the FS and TS complete, the development or configuration takes place in the respective system in the Development box</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The already prepared Build Tracker is used to track the progress of the configurations and developments</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	Update all relevant stakeholders on the progression of the Build</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	The Transport Requests generated during the build is captured using the Transport Tracking Sheet</p></td>
                                            <td>PM / SA / FL / TL / FC / TC / All relevant stakeholders from Customer </td>
                                            <td>Approved Functional and Technical Specification Documents / Build Tracker / Transport Tracking Sheet / Project Status Tracking Sheet</td>                                    
                                            <td>RICEF List and Configuration List / Business Blueprint Document</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>4.	Perform Unit Tests
                                                <p>With this task, the project team would:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Complete the Unit testing for the completed configuration or the development object</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Any issues identified are tracked using an Issue Log and are rectified</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Once the testing completes with no issues, the Unit testing is considered complete</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Update all relevant stakeholders on the progression of Unit testing</p></td>
                                            <td>PM / FL / TL / FC / TC </td>
                                            <td>Unit Testing Issue Log</td>                                    
                                            <td>Approved Functional and Technical Specification Documents / Build Tracker</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>5.	Build Testing Schedule and Scripts</p>
                                                <p>The following activities are completed here:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The end to end scenarios to be used for Integration Testing and UAT are defined</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	These scenarios are validated and confirmed by the relevant stakeholders from the customer</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The scenarios are documented and the Test Scripts are created</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The required Master data to be used for the completion of these scenarios are identified</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The tools to create or load the master data are identified and developed (as per the Data Conversion Strategy defined in the Project Charter)</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The master data is then created or loaded into the Quality Box as part of Mock Cutover 1 using the tools developed</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The testing team is identified and confirmed from the customer for UAT</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Update the detailed Project Plan with the testing schedule in order to derive the timelines required for Testing</p></td>
                                            <td>PM  / FL / TL / FC / TC / Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Finalized Testing Schedule /  Finalized Test Scripts / Updated Detailed Project Plan</td>                                    
                                            <td>Business Process List / To-Be Business  Documents with process flow diagrams / Business Blueprint Document</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>6.	Build Training Schedule</p>
                                                <p>With the testing team from the customer identified and confirmed, the project team needs to work together with the customer core project team in order to train the testing team so that they will be able to follow the script and complete the testing. Therefore, both teams work together and prepare a training schedule which is later ratified and agreed upon with the relevant stakeholders from the customer.</p>
                                                <p>The planned training would cover: </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	SAP Navigation</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The business processes covered within each SAP module (focusing mainly on the end to end business scenarios identified for Testing)</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The features and functions within each business process</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The concept of master data and how it is used</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The concept of the transactional data and how transactions are entered in SAP for each process together with the main SAP transaction needed for the completion of the testing</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Hands on training with access to SAP system (In general it is the Unit Testing client that is used for this purpose)</p>
                                                <p>Once the training schedule is in place, the detailed project plan is updated to derive the timelines required to complete the training</p></td>
                                            <td>PM  / FL / TL / FC / TC / Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Finalized Training Schedule / Updated Detailed Project Plan</td>                                    
                                            <td>Testing Schedule /  Test Scripts / To-Be Business Documents with process flow diagrams / Business Blueprint Document</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>7.	Build Training Materials</p>
                                                <p>This tasks is involved in preparing the training manuals required for the training of the testing team to be involved in the UAT.</p>
                                                <p>The training materials can consists of MS Power Point presentation slides and/or MS Word/PDF documents with screen shots displaying </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	SAP Navigation</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The different transactions used to maintain Master data required for an end to end business scenario</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The different transactions used to maintain transactional data for an end to end business scenario</p></td>
                                            <td>FL / FC / Customer Core Project team</td>
                                            <td>All required Training Materials</td>                                    
                                            <td>Training Schedule / Test Scripts / To-Be Business Documents with process flow diagrams / Business Blueprint Document</td>
                                        </tr>
                                                                                                                                                                                                                                                                                      
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Complete, updated and approved Functional and Technical Specification documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completely Unit Tested configured and developed objects</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed System with all configurations and developments</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>Successful completion of Project Kick Off, Requirement Gathering workshops, and sign-off of Business Blueprint</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	To-Be Business Process Documents with process flow diagrams</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Document</p>
                                            <p class="tab-content" style="padding-left: 20px">•	RICEF Object List</p>
                                            <p class="tab-content" style="padding-left: 20px">•	List of Objects to be configured</p>
                                            <p class="tab-content" style="padding-left: 20px">•	attune Development Standards & Guidelines document</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Project Status Tracking Sheet</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	System Landscape Diagram</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Functional and Technical Specification Documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Detailed Project Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Build Tracker </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Transport Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Unit Testing Issue Log</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized Testing Schedule (for Integration testing and UAT)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized Test Scripts</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized Training Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All required Training Materials</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Project Status Tracking Sheet</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Completeness of the FS/TS documents as per the Build Tracker</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completeness of the Unit testing performed on the configured and developed objects as per the FS</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	The project may decide to use client defined templates and standards for the deliverables from within the Blueprinting phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Certain tasks of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	In case the client or another team representing the client is performing some parts of the tasks and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <h3>Document Templates</h3>                       
                                <p><%--<a href="References\attune System Requirement Specification.docx">1. attune System Requirement Specification</a>--%></p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Guidelines</h3>                                                     
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h3>Samples</h3>
                          </div>
                        </div>                              
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>

