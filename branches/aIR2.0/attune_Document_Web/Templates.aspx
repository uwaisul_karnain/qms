﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Templates.aspx.cs" Inherits="attune_Document_Web.Templates" %>

<asp:Content ID="templateContent" ContentPlaceHolderID="MainContentPageSection" runat="server">
    
    <!-- Page Content -->
        <h1>Templates</h1>

        <div class="row">
                <div class="col-xs-6 col-md-4">
                    <h3>Implementation Methodology</h3>    

                            <p><a href="References\Weekly Project Status Report - Template Sample and Instructions.xlsm">Project Status Report</a></p>

                    <h2>Project Preparation</h2>   

                            <p><a href="References\ABAP Development Guidelines.docx">1.  ABAP Development Guidelines</a></p> 
                            <p><a href="References\Blueprint Tracker.xls">2. Blueprint Tracker</a></p> 
                            <p><a href="References\Business Blueprint Workshop Schedule.xlsx">3. Business Blueprint Workshop Schedule</a></p> 
                            <p><a href="References\Business Transformation Strategy.docx">4. Business Transformation Strategy</a></p> 
                            <p><a href="References\Change Management Strategy.docx">5. Change Management Strategy</a></p> 
                            <p><a href="References\Coding Standards Java.doc">6.  Coding Standards Java</a></p>    
                            <p><a href="References\Coding Standards Microsoft.Net.docx">7.  Coding Standards Microsoft.Net</a></p>  
                            <p><a href="References\Communication Strategy.docx">8. Communication Strategy</a></p> 
                            <p><a href="References\End User Training Strategy.docx">9. End User Training Strategy</a></p> 
                            <p><a href="References\Executive_Alignment_Strategy.docx">10. Executive Alignment Strategy</a></p>
                            <p><a href="References\Executive Alignment Workshop.pptx">11. Executive Alignment Workshop</a></p>
                            <p><a href="References\Fundamentals of Change Mgmt for Key Stakeholders.pptx">12. Fundamentals of Change Management for Key Stakeholders</a></p>
                            <p><a href="References\Initial 30 Day Communication Plan.docx">13. Initial 30 Day Communication Plan</a></p> 
                            <p><a href="References\Initial Business Transformation Readiness Assessment Approach.docx">14. Initial Business Transformation Readiness Assessment Approach</a></p>    
                            <p><a href="References\Initial Business Transformation Readiness Report.pptx">15. Initial Business Transformation Readiness Report</a></p>  
                            <p><a href="References\System Integration Landscape.vsd">16. Integration Landscape Diagram</a></p>
                            <p><a href="References\Issue Management Process.docx">17. Issue Management Process</a></p>
                            <p><a href="References\Master Project Schedule.mpp">18. Master Project Schedule</a></p>
                            <p><a href="References\Meeting Minutes.docx">19. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">20. Milestone Report</a></p>
                            <p><a href="References\Onboarding Guide.docx">21. Onboarding Guide</a></p> 
                            <p><a href="References\Project Charter.docx">22.  Project Charter</a></p>
                            <p><a href="References\Project Governance.pptx">23.  Project Governance</a></p>
                            <p><a href="References\Project Organizational Chart.pptx">24.  Project Organizational Chart</a></p>
                            <p><a href="References\Project Status Tracking Sheet.xlsm">25.  Project Status Tracking Sheet</a></p>
                            <p><a href="References\Project Team Training Strategy.docx">26. Project Team Training Strategy</a></p>
                            <p><a href="References\Risk Management Plan.docx">27.  Risk Management Plan</a></p>
                            <p><a href="References\SAP System Landscape Stratergy.docx">28.  SAP System Landscape Stratergy</a></p>
                            <p><a href="References\Scope Management & Change Request Process.docx">29.  Scope Management & Change Request Process</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">30. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Size Estimation Sheet.xlsx">31. Size Estimation Sheet </a></p>
                            <p><a href="References\Stage Gate Review.pptx">32. Stage Gate Review</a></p>
                            <p><a href="References\Stakeholder Identification with Grid Mapping.xlsx">33. Stakeholder Identification with Grid Mapping</a></p> 
                            <p><a href="References\Testing Strategy.docx">34. Testing Strategy</a></p>
   
                    
                    <h2>Business Blueprinting</h2>    

                            <p><a href="References\Business Blueprint Workshop Schedule.xlsx">1. Business Blueprint Workshop Schedule</a></p>
                            <p><a href="References\attune BPD Template.docx">2.  Business Process Document</a></p> 
                            <p><a href="References\Change_Champion_Change_Agent_Strategy.docx">3. Change Champion Change Agent Strategy</a></p> 
                            <p><a href="References\Change_Impact_Assessment.docx">4. Change Impact Assessment</a></p> 
                            <p><a href="References\Change Impact Assessment Strategy.docx">5. Change Impact Assessment Strategy</a></p> 
                            <p><a href="References\Change Impact Assessment Workbook.xlsx">6. Change Impact Assessment Workbook</a></p> 
                            <p><a href="References\Defect Management Process.docx">7. Defect Management Process</a></p>
                            <p><a href="References\Detailed Build Schedule.mpp">8. Detailed Build Schedule</a></p>
                            <p><a href="References\Detailed Build Schedule Guidelines.docx">9. Detailed Build Schedule Guidelines</a></p>
                            <p><a href="References\End User Training Curriculum With Calendar Option.xlsx">10. End User Training Curriculum With Calendar Option</a></p> 
                            <p><a href="References\GAP List.xlsx">11. GAP List</a></p>
                            <p><a href="References\Integrated Communication Plan.xlsx">12. Integrated Communication Plan</a></p> 
                            <p><a href="References\Integration Test Script Development Tracker.xlsx">13. Integration Test Script Development Tracker</a></p>
                            <p><a href="References\Key Stakeholder Leadership Action Plan.docx">14. Key Stakeholder Leadership Action Plan</a></p> 
                            <p><a href="References\Knowledge Transfer Tracking.xlsx">15. Knowledge Transfer Tracking</a></p> 
                            <p><a href="References\Meeting Minutes.docx">16. Meeting Minutes of Business Blueprint Workshop</a></p>
                            <p><a href="References\Milestone Report.xlsx">17. Milestone Report</a></p>
                            <p><a href="References\RICEFW List.xlsx">18. RICEFW Objects List</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">19. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">20. Stage Gate Review</a></p>
                            <p><a href="References\Training Development and Standards.docx">21. Training Development and Standards</a></p>
                              

                    <h2>Realization-Build</h2> 

                            <p><a href="References\A Day in the Life of a Tester.pptx">1. A Day in the Life of a Tester</a></p>
                            <p><a href="References\Business_Role_Mapping.docx">2. Business Role Mapping</a></p>
                            <p><a href="References\Communication Effectiveness Survey.xlsx">3. Communication Effectiveness Survey</a></p>
                            <p><a href="References\Detailed Build Schedule.mpp">4. Detailed Build Schedule</a></p>
                            <p><a href="References\Detailed Build Schedule Guidelines.docx">5. Detailed Build Schedule Guidelines</a></p>
                            <p><a href="References\Data Cleansing and Validation Plan Template.docx">6. Data Cleansing and Validation Plan Template</a></p>
                            <p><a href="References\End User ILT Course.pptx">7. End User ILT Course</a></p> 
                            <p><a href="References\End User Training Course Outline.docx">8. End User Training Course Outline</a></p>
                            <p><a href="References\End User Training Curriculum.xlsx">9. End User Training Curriculum</a></p> 
                            <p><a href="References\FS template Conversion.docx">10. FS template Conversion</a></p> 
                            <p><a href="References\FS template Enhancements.docx">11. FS template Enhancements</a></p>
                            <p><a href="References\FS template Interface.docx">12. FS template Interface</a></p> 
                            <p><a href="References\FS template Reports & Forms.docx">13. FS template Reports & Forms</a></p>    
                            <p><a href="References\Test Scenario Description.docx">14. Integration Test Scenarios</a></p>
                            <p><a href="References\Integration Test Script.xlsx">15. Integration Test Script</a></p>
                            <p><a href="References\Testing Daily Schedule_Single Instance per Test.xlsx">16. Integration Testing Schedule</a></p>
                            <p><a href="References\A Day in the Life of a Tester.pptx">17. Integration Testing Training Materials</a></p>
                            <p><a href="References\Meeting Minutes.docx">18. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">19. Milestone Report</a></p>
                            <p><a href="References\Project documentation update guidelines.docx">20. Project documentation update guidelines</a></p>
                            <p><a href="References\Phase Kick-off Agenda.pptx">21. Phase Kick-off Agenda</a></p>
                            <p><a href="References\Phase Kick off Meeting Guidelines.docx">22. Phase Kick off Meeting Guidelines</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">23. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">24. Stage Gate Review</a></p>
                            <p><a href="References\Template - Mock Cutover Schedule.mpp">25. Template - Mock Cutover Schedule</a></p>
                            <p><a href="References\Tester Training Strategy.docx">26. Tester Training Strategy</a></p>
                            

                    <h2>Realization-Testing</h2> 

                            <p><a href="References\Batch Schedule.xlsx">1. Batch Job Schedule</a></p>
                            <p><a href="References\Business Continuity Plan.docx">2. Business Continuity Plan</a></p>
                            <p><a href="References\Communication Effectiveness Survey.xlsx">3. Communication Effectiveness Survey</a></p>
                            <p><a href="References\Controlled Resumption Scripts.xlsx">4. Controlled Resumption Scripts</a></p>
                            <p><a href="References\End User Training Curriculum With Calendar Option.xlsx">5. End User Training Curriculum With Calendar Option</a></p>
                            <p><a href="References\End User Training Course Template.docx">6. End User Training Course Template</a></p>
                            <p><a href="References\End User Training Schedule.xlsx">7. End User Training Schedule</a></p>
                            <p><a href="References\Job Schedule.xlsx">8. Job Schedule</a></p>
                            <p><a href="References\Live Cutover Plan.docx">9. Live Cutover Plan</a></p>
                            <p><a href="References\Live Cutover Schedule - Template.mpp">10. Live Cutover Schedule - Template</a></p>
                            <p><a href="References\Meeting Minutes.docx">11. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">12. Milestone Report</a></p>                 
                            <p><a href="References\Post Go Live Support Phase Process.docx">13. Post Go Live Support Phase Process</a></p>
                            <p><a href="References\Post Go Live Support Plan - Template.docx">14. Post Go Live Support Plan - Template</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">15. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">16. Stage Gate Review</a></p>
                            <p><a href="References\Training Development and Standards.docx">17. Training Development and Standards</a></p>
                            <p><a href="References\Train the Trainer Part 1 Program Overview.docx">18. Train the Trainer Part 1 Program Overview</a></p>
                            <p><a href="References\User Auth Role Maintenance.xlsx">19. User Auth Role Maintenance</a></p>                         

                    <h2>Final Preparation</h2> 

                            <p><a href="References\Controlled Resumption Scripts.xlsx">1. Controlled Resumption Scripts</a></p>
                            <p><a href="References\Go-live Readiness Checklist.xlsx">2. Go-live Readiness Checklist</a></p>
                            <p><a href="References\Go Live Readiness Scorecard.pptx">3. Go Live Readiness Scorecard</a></p>
                            <p><a href="References\Knowledge Transfer Plan Template.docx">4. Knowledge Transfer Plan Template</a></p>
                            <p><a href="References\Knowledge Transfer Schedule and Tracker.xlsx">5. Knowledge Transfer Schedule and Tracker</a></p>
                            <p><a href="References\Meeting Minutes.docx">6. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">7. Milestone Report</a></p>              
                            <p><a href="References\Post Go Live Support Phase Process.docx">8. Post Go Live Support Phase Process</a></p>
                            <p><a href="References\Post Go Live Support Plan - Template.docx">9. Post Go Live Support Plan - Template</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">10. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">11. Stage Gate Review</a></p>
                            <p><a href="References\Training Evaluation.xlsx">12. Training Evaluation</a></p>
                            <p><a href="References\User Auth  SAP ABAP Projects.xlsx">13. User ID List and Authorization Matrix</a></p> 

                    <h2>Post-Go Live</h2> 

                            <p><a href="References\Lessons Learned User Focus Group.docx">1. Lessons Learned User Focus Group</a></p>
                            <p><a href="References\Meeting Minutes.docx">2. Meeting Minutes</a></p>
                            <p><a href="References\Milestone Report.xlsx">3. Milestone Report</a></p>
                            <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">4. Services Methodology Operational Review Evaluation</a></p>
                            <p><a href="References\Stage Gate Review.pptx">5. Stage Gate Review</a></p>
                                                                         
                </div>
                <div class="col-xs-6 col-md-4"> 
                    <h3>Development Methodology</h3>    

                    <h2>Requirement Analysis</h2> 
                            <p><a href="References\Acceptance Test Plan.docx">1. Acceptance Test Plan</a></p>  
                            <p><a href="References\Product Backlog.xls">2. Product Backlog</a></p>
                            <p><a href="References\Requirement Clarification Tracking Sheet.xls">3. Requirement Clarification Tracking Sheet</a></p>
                            <p><a href="References\RTM.xlsx">4. Requirement Traceability Matrix</a></p>                                                                     
                            <p><a href="References\System Requirement Specification.docx">5. System Requirement Specification</a></p>

                    <h2>Requirement Development</h2> 
                            <p><a href="References\Change Request.docx">1. Change Request</a></p>             
                            <p><a href="References\Product Backlog.xls">2. Product Backlog</a></p>
                            <p><a href="References\Project Effort Estimation Sheet - 3 Point Estimation.xls">3. Project Effort Estimation Sheet - 3 Point Estimation.xls</a></p>
                            <p><a href="References\Project Status Tracking Sheet.xlsm">4. Project Status Tracking Sheet</a></p>                                                                
                            <p><a href="References\Requirement Clarification Tracking Sheet.xls">5. Requirement Clarification Tracking Sheet</a></p>   
                            <p><a href="References\RTM.xlsx">6. Requirement Traceability Matrix</a></p>       
                            <p><a href="References\System Requirement Specification.docx">7. System Requirement Specification</a></p>

                    <h2>Architecture & Design</h2> 
                            <p><a href="References\Architecture Design.docx">1. Architecture Design</a></p>
                            <p><a href="References\Detail Design.docx">2. Detail Design</a></p>
                            <p><a href="References\Integration Plan.docx">3. Integration Plan</a></p> 

                    <h2>Coding, Unit Testing and Integration</h2> 
                            <p><a href="References\Change Request.docx">1. Change Request</a></p>  
                            <p><a href="References\Deployment Guide.docx">2. Deployment Guide</a></p>
                            <p><a href="References\Integration Plan.docx">3. Integration Plan</a></p>
                            <p><a href="References\Peer Review Form.xlsx">4. Peer Review Form</a></p>
                            <p><a href="References\Project Review Log.xlsx">5. Project Review Log</a></p>
                            <p><a href="References\Release Note.docx">6. Release Note</a></p>   
                            <p><a href="References\RTM.xlsx">7. Requirement Traceability Matrix</a></p> 
                            <p><a href="References\User And Operations Manual.docx">8. User And Operations Manual</a></p>   

                    <h2>Quality Assurance and Control</h2> 
                            <p><a href="References\Release Note.docx">1. Release Note</a></p>
                            <p><a href="References\Test Cases.xlsx">2. Test Cases</a></p>
                            <p><a href="References\Test Plan.docx">3. Test Plan</a></p> 

                    <h2>Work Product Review</h2> 
                            <p><a href="References\Peer Review Form.xlsx">1. Peer Review Form</a></p>
                            <p><a href="References\Project Review Log.xlsx">2. Project Review Log</a></p>                                                

                    <h2>Management Review Process</h2> 
                            <%--<p><a href="References\Project Status Report.pptx">1. Project Status Report</a></p>--%> 
                            <p><a href="References\Weekly Project Status Report - Template Sample and Instructions.xlsm">1. Project Status Report</a></p> 
                    

                </div>
                <div class="col-xs-6 col-md-4"> 
                    <h3>Governing Principles</h3>

                    <h2>Process Tailoring</h2>

                            <p><a href="References\Process Tailoring Repository.xlsx">1. Process Tailoring Record</a></p>                                                                                              

                    <h2>Project Planning</h2>
  
                            <p><a href="References\Project Effort Estimation Sheet - 3 Point Estimation.xls">1. Effort Estimate for Software Developments</a></p>
                            <p><a href="References\Project Approach Document.docx">2. Project Approach Document</a></p>
                            <p><a href="References\Project Charter.docx">3. Project Charter</a></p>
                            <p><a href="References\Project Status Tracking Sheet.xlsm">4. Project Status Tracking Sheet</a></p>
                            <p><a href="References\Process Tailoring Repository.xlsx">5. Process Tailoring Record</a></p>
                            <p><a href="References\Project Skills Tracker.xlsx">6. Skill Tracker</a></p> 

                    <h2>Project Monitoring & Control</h2>

                            <p><a href="References\Project Skills Tracker.xlsx">1. Project Skill Tracker</a></p> 
                            <p><a href="References\Project Status Tracking Sheet.xlsm">2. Project Status Tracking Sheet</a></p>  
                       
                    <h2>Measurements and Analysis</h2>

                            <p><a href="References\MOD.xlsx">1. Metric Objective Description</a></p> 

                    <%--<h2>Project Closure</h2>--%>

                    <h2>Process Improvements</h2>

                            <p><a href="References\Process Improvement Request Form.docx">1. Process Improvement Request Form.docx</a></p>                                                                                            
                            <p><a href="References\Process Improvement Request Log.xlsx">2. Process Improvement Request Log</a></p>                                                                                            
                        


                </div>
        </div>     
    <!-- /#page-content-wrapper -->
</asp:Content>
