﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="TrainingSales.aspx.cs" Inherits="attune_Document_Web.TrainingSales" %>
<asp:Content ID="TrainingSalesPageContent" ContentPlaceHolderID="MainContentPageSection" runat="server">

          <!-- Page Content -->

                        <h1>aIR 2.0 Introduction for Sales</h1>

                        <h3>Presentations</h3>
                        <div class="data">
                                <p class="tab-content" style="padding-left: 20px"><a href="References\aIR 2.0 Introduction for Regional SVP's and Sales.pptx">1.  aIR 2.0 Introduction for Regional SVP's and Sales</a></p>  
                        </div>

                        <h3>Videos</h3>
                        <div class="data">
                                <p class="tab-content" style="padding-left: 20px"><a href="https://life.oneattune.com/mlink/file/NTMxNTk" target="_blank">1.  aIR 2.0 Introduction for Sales - Recording</a></p>  

                        </div>

        <!-- /#page-content-wrapper -->

</asp:Content>
