﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Samples.aspx.cs" Inherits="attune_Document_Web.Samples" %>


<asp:Content ID="sampleContent" ContentPlaceHolderID="MainContentPageSection" runat="server">
    
    <!-- Page Content -->
        <h1>Samples</h1>

        <div class="row">
                <div class="col-xs-6 col-md-4">
                    <h3>Implementation Methodology</h3>    

                    <h2>Project Preparation</h2>   

                            <p><a href="References\Onboarding Guide Sample.docx">1.  Onboarding Guide Sample</a></p> 
                            <p><a href="References\Project Organizational Chart Sample.pptx">2.  Project Organizational Chart Sample</a></p> 
                    
                    <h2>Business Blueprinting</h2>   

                            <p><a href="References\FRSBusinessProcessReferenceDocumentS.docx">1.   FRS Business Process Reference Document</a></p>

                    <h2>Realization-Build</h2> 

                            <p><a href="References\Sample FS Conversions.docx">1. Sample FS Conversions</a></p> 
                            <p><a href="References\Sample FS Enhancements.docx">2. Sample FS Enhancements</a></p>
                            <p><a href="References\Sample FS Interface.docx">3. Sample FS Interface</a></p> 
                            <p><a href="References\Sample FS Reports.docx">4. Sample FS Reports</a></p>
                            <p><a href="References\Sample RICEFW Development Process.pptx">5. Sample RICEFW Development Process</a></p>        

                    <h2>Final Preparation</h2> 

                            <p><a href="References\Live Cutover Calendar Views Sample.pptx">1. Live Cutover Calendar Views Sample</a></p>
                                                                         
                </div>

        </div>     
    <!-- /#page-content-wrapper -->
</asp:Content>

