﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Guidelines.aspx.cs" Inherits="attune_Document_Web.Guidelines" %>
<asp:Content ID="guidelinesContentPage" ContentPlaceHolderID="MainContentPageSection" runat="server">
        
    <!-- Page Content -->
        <h1>Guidelines</h1>

        <div class="row">
                <div class="col-xs-6 col-md-4">
                    <h3>Implementation Methodology</h3>    
                            <p><a href="References\aIRTerminologyG.xlsx">1.  aIR Terminology (Acronyms, Abbreviations, and Definitions)</a></p>
                            <p><a href="References\TemplateandCustomerDeliveredDocumentNamingandStorageConventionG.docx">2.  aIR Methodology Template and Customer Delivered Document Naming and Storage Convention</a></p>
                            <p><a href="References\TemplateCreationStandardsandGuidelinesG.docx">3.  aIR Methodology Template Creation Standards and Guidelines</a></p>  
                    <h2>Project Preparation</h2>   
                            <p><a href="References\Branded aIR Diagram.pptx">1.  Branded aIR Diagram</a></p> 
                            <p><a href="References\Phase Process Summary.docx">2. Phase Process Summary</a></p>
                    <h2>Project Preparation</h2>  
                    
                    <h2>Business Blueprinting</h2>   
                            <p><a href="References\ProcessFlowForBPDs.vsdx">1.  BPD Process Flow</a></p>  
                    
                    <%--<h2>Business Blueprinting</h2>--%>    

                    <h2>Realization-Build</h2> 
                            <p><a href="References\ABAP Development Guidelines.docx">1.  ABAP development Guidelines</a></p>  
                            <p><a href="References\Coding Standards Java.doc">2.  Coding Standards Java</a></p>    
                            <p><a href="References\Coding Standards Microsoft.Net.docx">3.  Coding Standards Microsoft.Net</a></p>
                            <p><a href="References\Data Cleansing and Migration Guidelines.docx">4.  Data Cleansing and Migration Guidelines</a></p>
                            <p><a href="References\Detailed Mock Cutover Guidelines.docx">5.  Detailed Mock Cutover Guidelines</a></p>
                            <p><a href="References\Test Scenario Development Guidelines.docx">6.  Test Scenario Development Guidelines</a></p>  

                    <%--<h2>Realization-Testing</h2>--%> 
                       

                    <%--<h2>Final Preparation</h2>--%> 


                    <%--<h2>Post-Go Live</h2>--%> 
                                                                         
                </div>
                <div class="col-xs-6 col-md-4"> 
                    <h3>Development Methodology</h3>    

                    <%--<h2>Requirement Analysis</h2>--%> 

                    <%--<h2>Requirement Development</h2>--%> 

                    <%--<h2>Architecture & Design</h2>--%> 

                    <%--<h2>Coding, Unit Testing and Integration</h2>--%> 

                    <%--<h2>Quality Assurance and Control</h2>--%> 

                    <h2>Work Product Review</h2> 
                            <p><a href="References\Development Standard Document APEX.doc">1. <strong>Force.com - </strong>Development Standard Document APEX</a></p>                                                                                                                               
                            <p><a href="References\Coding Standards Java.doc">2. <strong>Java - </strong>Java Coding Standards</a></p>  
                            <p><a href="References\Coding Standards Guide.docx">3. <strong>Microsoft.Net - </strong> Coding Standards Guide</a></p>                                                                     
                            <p><a href="References\Jquery.pptx">4. <strong>Microsoft.Net - </strong>Jquery</a></p>    
                            <p><a href="References\NET Portal Development Framework.docx">5. <strong>Microsoft.Net - </strong>.NET Portal Development Framework</a></p>
                            <p><a href="References\Coding standards-Infrastructure and security Development Guide.docx">6. Fiori Cloud App Development</a></p>
                            <p><a href="References\Cloud App Configuration Guide.docx">7. Cloud App Configuration Guide</a></p>
                                             
                    <h2>Management Review Process</h2> 
                            <p><a href="References\MOD.xlsx">1. Metric Objective Description (attune MOD)</a></p>
                             

                </div>
                <div class="col-xs-6 col-md-4"> 
                    <h3>Governing Principles</h3>

                    <h2>Process Tailoring</h2>
                            <p><a href="References\Agile Project Process.pptx">1. Agile Project Process</a></p>
                            <p><a href="References\Life Cycle Definitions.docx">2. Life Cycle Definitions</a></p>       
                    <h2>Project Planning</h2>
                            <p><a href="References\MOD.xlsx">1. Metric Objective Description (attune MOD)</a></p>  

                    <%--<h2>Project Monitoring & Control</h2>--%>
                       
                    <%--<h2>Measurements and Analysis</h2>--%>

                    <%--<h2>Project Closure</h2>--%>

                    <%--<h2>Process Improvements</h2>--%>



                </div>
        </div>     
    <!-- /#page-content-wrapper -->
</asp:Content>
