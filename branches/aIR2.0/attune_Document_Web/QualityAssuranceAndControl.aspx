﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="QualityAssuranceAndControl.aspx.cs" Inherits="attune_Document_Web.QualityAssuranceAndControl" %>

<asp:Content ID="qualityAssuranceAndControlPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Quality  Assurance  and  Control  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This objective of this process is to describe the QAC activities in software engineering lifecycle that need to be carried out in software development and maintenance projects.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>Projects are executed in a phased manner and the applicability of the various phases described in this process will depend on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The stage at which the project starts</p>
                            <p class="tab-content" style="padding-left: 20px">•	The expected deliverables</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>
                            <p class="tab-content" style="padding-left: 20px">•	The methodology imposed by the client</p>
                            <p>The QAC activities defined here, along with the above, are used to derive the Project's Process at the beginning of the project (the Define Project's Process procedure in the Integrated Project Planning process)</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PC  - </strong>Process Consultant</p>
                                <p><strong>GM  - </strong>Group Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PL  - </strong>Project Lead</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>BA - </strong>Business Analyst</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>QA  - </strong>Quality Assurance</p>                                
                                <p><strong>QAC  - </strong>Quality Assurance & Control</p>
                                <p><strong>CR - </strong>Change Request</p>
                                <p><strong>RM - </strong>Requirement Management</p>
                                <p><strong>CM  - </strong>Configuration Management</p>                                
                                <p><strong>UAT  - </strong>User Acceptance Testing</p>                                                          
                          </div>
                          <div class="col-xs-6 col-md-4">   
                                <p><strong>ATP  - </strong>Acceptance Test Plan</p>                             
                                <p><strong>SRS  - </strong>System Requirement Specification</p>
                                <p><strong>RCS - </strong>Requirement Clarification Sheet</p>
                                <p><strong>RTM  - </strong>Requirement Traceability Matrix</p>
                                <p><strong>PAD  - </strong>Project Approach Document</p>
                                <p><strong>aPTR - </strong>attune Process Tailoring Repository</p>                             
                          </div>
                        </div>

                        <div class="data">
                            <h3>Process and Steps</h3>     

                            <p>The software development lifecycles followed by attune are agile and the iterative method.</p>
                            <p>Each Sprint, Sub-system or System testing is performed using the System Testing procedure. The focus is to verify and validate the functioning of the sprint, sub-systems and system. The focus is also to perform the system test identified earlier at design time. At the end of these, the system is tested and ready for acceptance.</p>
                            <p>The Deployment, User Acceptance Testing and Changeover procedure describes how the system is to be deployed at client site and how the client performs the agreed upon acceptance testing to accept the system and puts the accepted system in operation. This completes the validation of the system developed.</p>
                            <p>All the procedures incorporate work-product reviews required for verification, and the CM activities for version control and establishing and maintaining baselines. Requirements are managed using the RM process (including maintenance of bi-directional traceability).</p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        <tr>
                                            <td rowspan="6"><strong>Perform Quality Assurance</strong></td>
                                            <td><p>The PM identifies the team for system QA. </p></td>
                                            <td>PM</td>
                                            <td rowspan="2">Test Plan</td>                                    
                                            <td rowspan="2">Unit Tested Sub-System/System, SRS, Product Backlog, RCS, PAD, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The System Test Plan is prepared and this may be done jointly with the client.</p> 
                                                <p>1.	Products for system testing are selected</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Determine the user requirements and functionality to be tested during system testing</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Determine the sub-systems and units to be tested during system testing</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Select the testing methodology</p>

                                                <p>2.	System testing environment is defined and established</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify system test environment requirements</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify client/ user supplied components to be used for or integrated into the system testing</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify test equipment and tools</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify resources required for system testing</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Set up testing environment</p>

                                                <p>3.	Operational scenarios, procedures, inputs, outputs and criteria for system testing are defined.</p>

                                                <p>4.	Review the above with the user and client representatives through presentations and discussions. </p>

                                                <p>5.	Prepare System Test Plan document based on the analysis done. (Template : Test Plan)</p>

                                                <p>6.	Update the RTM for traceability with respect to the System Test Plan.</p></td>
                                            <td>BA/QA</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>System Test Plan document is reviewed, review comments are incorporated and verified</p></td>
                                            <td>PM, TL</td>
                                            <td rowspan="2">Review Comments</td>
                                            <td rowspan="3">Test Plan, SRS, Product Backlog, RCS</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>If required, external review by client or client representative are initiated. Sign-off is taken after the external review comments are incorporated.</p></td>
                                            <td>PM, External QA</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare Test Cases for the system, sub-system or integrated system/sub-system.</p>
                                                <p>•	Identify test cases</p>
                                                <p>•	Generate the required test data and construct the required test drivers and stubs.</p> 
                                                <p>The use of test data generators and other testing tools for this activity is done using the identified tools.</p> 
                                                <p>(Template: Test Cases)</p></td>
                                            <td>QA</td>
                                            <td>Test Cases</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Baseline Test Plan and Test Cases as soon as they are reviewed OK.</p></td>
                                            <td>PM/PL</td>
                                            <td>Test Plan, Test Cases</td>
                                            <td>aPTR</td>
                                        </tr>

                                        <tr>
                                            <td><p><strong>Perform Quality Control</strong></p>
                                                            <p></p>
                                                            <p><strong>(Test Execution)</strong></p>

                                            <td><p>System/sub-system testing is done as per the Test Plan and Test Cases.</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	The test results are logged in a similar format document renamed to a different name, which represent the date of execution and build version.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identified changes and defects in the units are logged in the Bugtracker Database</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Changes are tracked and the system /sub-system is provided for re-testing using the CM process.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Successfully Tested sub-system/system is moved on for UAT</p> 
                                                <p>(Template: Test Cases)</p></td>
                                            <td>BA, QA, SE</td>
                                            <td>Test Result Sheet, Buck Tracker Records</td>                                    
                                            <td>Test Plan, Test Cases, SRS</td>
                                        </tr>

                                        <tr>
                                            <td rowspan="9"><strong>Perform Deployment, UAT and Change Over</strong></td>
                                            <td><p>Ensure that the client has the targeted hardware, environment, resources and acceptance test data ready for UAT.</p></td>
                                            <td>PM, Team</td>
                                            <td>UAT Environment</td>                                    
                                            <td>ATP, User Manuals</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>If required, changes are incorporated to the available ATP and the modified ATP is signed off by both the PM and the Client representative.</p></td>
                                            <td>QA</td>
                                            <td>ATP</td>
                                            <td>CR Log, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Deploy the system at the client site’s UAT environment.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Problems faced are resolved and closed, and the designated team logs these problems and the steps taken for closing them.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Update Deployment Document if necessary</p></td>
                                            <td>QA, SE</td>
                                            <td>Issue Log and the action taken</td>
                                            <td>Deployment Manual</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Execute acceptance testing activities.</p>
                                                <p>Suitable training and support is provided, If UAT requires training as per ATP</p></td>
                                            <td>Client, PM, QA</td>
                                            <td></td>
                                            <td rowspan="3">Deployed Sub-System/System, User & Operations Manuals</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Problems found during UAT are logged in the client-defined format or the Test Log (Bugtracker). Each problem is tracked to closure.</p></td>
                                            <td>Client UAT Team</td>
                                            <td>Bug Tracker / Test Log</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>When problems found during UAT, changes may be required to system code and documents;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Analyze the impact of the changes.</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Decision taken to incorporate the change with current sprint or in a future sprit and re-schedule the future sprints accordingly</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Initiate the changes and ensure that proper CM procedure is followed</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Incorporate, review and release the changes for re-testing.</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Bi-directional traceability is maintained.</p></td>
                                            <td>PM/PL</td>
                                            <td>RTM, Schedule</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>At successful completion of UAT, obtains a sign-off from the client.</p></td>
                                            <td>PM</td>
                                            <td>Sign-off mail for release or sprint, UAT approved Sub-System/System</td>
                                            <td>Deployed System, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Hand Over the Tested Sub-system / System</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Deploy the system in client site real environment and make necessary arrangements for the system to go live.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	User training is provided for the client, if agreed in the contract by the maintenance and support team.</p></td>
                                            <td>PM, Team, Maintenance and Support team</td>
                                            <td>Deployed Sub-System/System</td>
                                            <td>Deployment Guide</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>After successful changeover, requests the client for a formal letter recording the completion of changeover and marking the system going into production.</p>
                                                <p>If the client deems the system unsuitable and no agreement can be reached to make it acceptable, or there are some contractual problems, or if the changeover is not achieved successfully, the system may be scrapped and deemed as not installable.  In such a case, the reasons for such decisions are documented as part of the Project Monitoring and Control process.</p></td>
                                            <td>PM</td>
                                            <td>Confirmation Mail</td>
                                            <td>Deployed System</td>
                                        </tr>

                                    </tbody>


                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>






                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Successful completion of internal reviews, external reviews and sign-offs of SRS and the Acceptance Test Plan</p> 
                                            <p>•	The required Sprint, sub-system or system is integrated</p>
                                            <p>•	Successfully reviewed System / Sub-system  Test Plans</p> 
                                            <p>•	Deployment and User Manuals are available</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	All outputs of this phase are successfully reviewed internally and, if applicable, external reviews and sign-off are completed.</p>
                                            <p>•	System / Sub-System testing completed and reviewed, product support documents (such as User Documentation and Installation and Changeover Manual) are tested and reviewed.</p>
                                            <p>•	Applicable external reviews and sign-offs are completed.</p>
                                            <p>•	System goes ‘live’ or a decision is taken to scrap the system</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Test Plan</p>
                                            <p>•	Test Cases</p>
                                            <p>•	Acceptance Test Plan</p>
                                            <p>•	System Requirement Specification</p>
                                            <p>•	Architecture Design</p>
                                            <p>•	Detail Design</p>
                                            <p>•	Integrated System / Sub-System</p>
                                            <p>•	System / Sub-System Tested Source Code</p>
                                            <p>•	Deployment and User Manuals</p>                                            
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Test Plan</p> 
                                            <p>•	Test Cases</p> 
                                            <p>•	Copy of Test Cases updated with Test Result</p> 
                                            <p>•	Modified product support documents</p>
                                            <p class="tab-content" style="padding-left: 50px">o	Deployment Guide</p>
                                            <p class="tab-content" style="padding-left: 50px">o	User and Operations Manual</p>
                                            <p>•	Confirmation letter / mail of completion from client</p>  
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort for Quality Assurance</p>
                                            <p>•	Effort for User Acceptance Testing process</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>For Projects where Clients Specifically Mention that they will perform the QA activities, PM of the project coordinates with GM and PCc and can decide to limit the scope of QA activities at attune</p>
                                        </div>
                                    </td>
                                    </tr>  
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Quality Assurance and Control Process.pdf">Download Quality Assurance and Control Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>                                                                                             
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\Release Note.docx">1. Release Note</a></p>
                                    <p><a href="References\Test Cases.xlsx">2. Test Cases</a></p>
                                    <p><a href="References\Test Plan.docx">3. Test Plan</a></p>                                                
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>                                                     
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div> 

<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>


