﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ProcessImprovement.aspx.cs" Inherits="attune_Document_Web.ProcessImprovement" %>

<asp:Content ID="processImprovementDiagramPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Process Improvement Requests (PIR) Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document explains the methodology to manage the Process Improvement Requests (PIRs) for the attune Implementation Roadmap (aIR) Methodology. </p>

                            <p>The aIR methodology provides step-by-step guidelines for project implementations, development, and support with a comprehensive database of supporting templates, guidelines, and checklists. </p>

                            <p>The Process Improvement Requests process covers the development or modification of process assets like:</p>
                            <p class="tab-content" style="padding-left: 20px">•	Phase  Processes</p>
                            <p class="tab-content" style="padding-left: 20px">•	Definitions </p>
                            <p class="tab-content" style="padding-left: 20px">•	Templates </p>
                            <p class="tab-content" style="padding-left: 20px">•	Guidelines and Checklists</p>
                            <p class="tab-content" style="padding-left: 20px">•	aIR Training </p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>All aIR users can request improvements to the process. Mainly consider defined and published content, templates and guidelines as the basis for improvement.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>aIR  - </strong>Implementation Roadmap</p>       
                                <p><strong>PI - </strong>Process Improvement</p>                         
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PIR  - </strong>Process Improvement Request</p> 
                                <p><strong>PIRL  - </strong>Process Improvement Request Log</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>VP SM - </strong>VP Services Methodology</p>
                          </div>
                        </div>

                          <div class="data">
                            <h3>Procedure</h3>   
                                    <p class="tab-content" style="padding-left: 20px">attune Implementation Roadmap (aIR) methodology, templates and guidelines are defined and published in the aIR portal.  Any aIR user that identifies improvements, suggestions or new requirements to the aIR methodology, should raise the request following the process Improvement Process.</p>
                            </div>

<%--                        <div class="disc">
                            <div class="diagram"  style="padding-top: 60px">
                                <div class="litetooltip-hotspot-wrapper" style="max-width: 1020px">
                                    <div class="litetooltip-hotspot-container" style="padding-bottom: 50%">
                                    <img src="images/ProcessImprovement.jpg" class="img-responsive" data-width="1020" data-height="665" />

                                    <div class="hotspot" style="top: 5.359477124183006%; left: 2.892156862745098%; width: 4.215686274509804%; height: 17.647058823529413%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748164896" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="2.892156862745098" data-hotspot-y="5.359477124183006" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    <div class="hotspot" style="top: 23.398692810457515%; left: 2.696078431372549%; width: 4.6078431372549025%; height: 4.836601307189543%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748173260" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="2.696078431372549" data-hotspot-y="23.398692810457515" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    <div class="hotspot" style="top: 29.673202614379086%; left: 2.696078431372549%; width: 4.313725490196078%; height: 31.372549019607842%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748179444" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="2.696078431372549" data-hotspot-y="29.673202614379086" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    <div class="hotspot" style="top: 63.39869281045751%; left: 2.401960784313726%; width: 4.509803921568627%; height: 30.84967320261438%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748184700" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="2.401960784313726" data-hotspot-y="63.39869281045751" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    <div class="hotspot" style="top: 14.37908496732026%; left: 7.990196078431372%; width: 10.392156862745098%; height: 6.405228758169934%; background: none; border: 0; border-radius: 0; opacity: 0.8" id="hotspot_1426748192323" data-location="undefined" data-template="" data-templatename="" data-opacity="undefined" data-backcolor="undefined" data-textcolor="undefined" data-textalign="undefined" data-margin="undefined" data-padding="undefined" data-width="undefined" data-delay="undefined" data-trigger="undefined" data-issticky="undefined" data-hotspot-x="7.990196078431372" data-hotspot-y="14.37908496732026" data-hotspot-blink="undefined" data-hotspot-bgcolor="undefined" data-hotspot-bordercolor="undefined" data-hotspot-borderradius="undefined">
                                    <div class="data-container"></div>
                                    </div>

                                    </div>
                                    </div>
                            </div>
                        </div>--%>

                            <h4>Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="6"><strong>N/A</strong></td>
                                            <td><p>1.	Define process improvement </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The need for a modification to aIR is recognized</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Champion of improvement idea fills out a Process Improvement form</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Submit Process Improvement form to the VP Services Methodology </p></td>
                                            <td>aIR user</td>
                                            <td>PIR form</td>                                    
                                            <td>Supporting documentation of benefits, impacts, and existing pain points</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>2.	PIR Screening</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Services Methodology Team will fill out PIR Log when the PIR is received.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	VP SM will review PIR for completeness and clarity. </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	VP SM will ask for more information from the requestor, if necessary.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	New PIR’s will be added to the agenda of the weekly Services Methodology meeting to get input from the rest of the team.</p></td>
                                            <td>VP Services Methodology & the Team, PIR requestor</td>
                                            <td>PIR Log, PIR form</td>
                                            <td>PIR log</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>3.	Advisory Board Review</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Once a month the VP SM will bring the screened PIR’s to the Practice Heads meeting.  This is also the Advisory Board.</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The requestor will attend to present the request along with the benefits and impacts.</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The Advisory Board will decide to:</p>
                                                <p class="tab-content" style="padding-left: 40px">i.	Approve</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	Reject</p>
                                                <p class="tab-content" style="padding-left: 40px">iii.	Defer</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	PIR’s may be deferred pending more information from the requestor or until a pre-defined situation has occurred (i.e. PSA system changes in place)</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Team records the Advisory Board decision in the PIRL </p></td>
                                            <td>aIR Advisory Board, PIR requestor</td>
                                            <td>PIR Decision, Updated PIRL</td>
                                            <td>PIR, PIR log</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>4.	Process Improvement Implementation</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Approved PIR is assigned to a Practice Head by the COO to   build and implement the change</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The Practice Head will analyze all aspects in the PIR details, future requirements, industry trends and impacting process </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Practice head will build the PIR requirement and share with the Services Methodology Team for review</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Services Methodology Team assign the required subject matter experts, practice heads and Services Methodology Team to provide feedbacks.</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Respective practice head will incorporate review feedbacks and share with the Services Methodology Team for confirmation</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Progress towards implementation is monitored in the weekly Practice Heads meetings.</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	With the Services Methodology Practice Head’s approval, PIR’s ready to implement are published in the aIR portal and communicated to aIR users via ONEattune / aIR notification</p></td>
                                            <td>Practice Head , Services Methodology Team, Services Methodology Practice Head</td>
                                            <td>Respective process, template  or guideline, Meeting minutes , PIRL, Review feedbacks</td>
                                            <td>PIRL, PIR process</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>5.	Implemented change will be accessed by respective practice head within a project environment for effectiveness and completeness </p></td>
                                            <td>Practice Heads, PM</td>
                                            <td>Respective process, Template or guideline</td>
                                            <td>aIR methodology</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>6.	Review PIP and PIRL</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Once a year validate the overall process and PIRL.</p></td>
                                            <td>aIR Advisory Board</td>
                                            <td>Meeting minutes</td>
                                            <td></td>
                                        </tr>
                                    </tbody>

                                    </table>
                                </div>
                            </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Defined and published aIR methodology, templates and guidelines </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Process Improvement Request Process</p>
                                            <p class="tab-content" style="padding-left: 20px">•	aIR methodology Advisory Board defined</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Process improvement request implemented and communicated to end user via aIR portal and / or ONEattune</p>
                                            <p class="tab-content" style="padding-left: 20px">•	PIR rejected and / or deferred.  Update the log and communicate to the requester.</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Process Improvement Request Form</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Organization direction for process improvement or definition</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Process Improvement Request form</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Process Improvement Request Log</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Number of process improvements received from the users and tracked to closure</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Time spent on process improvements </p>

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">Process Improvement request process is applicable for all aIR processes, templates and guidelines.</p> 
                                        </div>
                                    </td>
                                    </tr>   
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\aIR Process Improvement Process.pdf">Download aIR Process Improvement Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>                                                                                              
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\Process Improvement Request Form.docx">1. Process Improvement Request Form.docx</a></p>                                                                                            
                                    <p><a href="References\Process Improvement Request Log.xlsx">2. Process Improvement Request Log</a></p>                                                                                            

                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>    
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>     
                    
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->

</asp:Content>



