﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ProjectPreparation.aspx.cs" Inherits="attune_Document_Web.ProjectPreparation" %>

<asp:Content ID="projectPreparationPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">--%>
                <%--<div class="row">
                    <div class="col-lg-12">

                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Preparation  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Project Preparation phase of the SAP implementation project lifecycle.  It covers the activities that need to be performed as well as the deliverables within the Project Preparation phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p>•	The contractual requirements</p>
                            <p>•	The methodology imposed by the client</p>

                            <p>The activities to be fulfilled in this phase are mentioned in detail under the Process Overview of this document.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>BTS - </strong>Business Transformation Services</p>
                                <p><strong>FL - </strong>Functional Lead</p>
                                <p><strong>FC - </strong>Functional Consultant</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PGM - </strong>Program Manager</p>
                                <p><strong>SVP - </strong>Senior Vice President</p>     
                          </div>
                          <div class="col-xs-6 col-md-4">    
                                <p><strong>VPS - </strong>Vice President Services</p>
                                <p><strong>PMO - </strong>Project Management Office</p>                                                 
                                <p><strong>RM - </strong>Resource Manager</p>                                                         
                                <p><strong>SA - </strong>Solution Architect</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                          </div>
                          <div class="col-xs-6 col-md-4">     
                                <p><strong>TC - </strong>Technical Consultant</p>                                                         
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>PC - </strong>Project Charter</p>
                                <p><strong>PPL - </strong>Project Plan</p>                                
                                <p><strong>pp - </strong>Project Plan</p>
                                <p><strong>SOW - </strong>Statement of Work</p> 
                          </div>
                        </div>
	

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h3>Process and Steps</h3>     
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived from PMI and SAP ASAP Focus methodologies.  The Project Preparation is the first phase within the aIR methodology. During the Project Preparation Phase, the project scope, the project plan, the project resources, and project approach are agreed upon. In addition, the required project standards and procedures are agreed upon. These standards and procedures cover the topics such as scope management, change control, issue management, escalation and resolution, documentation, and risk management. The Project Status Tracking Sheet is prepared and put in place in order to track the Issues, Changes, Risks, Assumptions, Decisions, and Action items in this phase as well as in the subsequent phases that follow. The Project strategies are defined, documented, and agreed to. These are included in the Project Charter.</p>
                            <p>During the Project Preparation Phase, Business Transformation begins with the Initial Business Transformation Readiness Assessment and subsequent report presenting the results of the assessment along with recommended actions. An Initial 30 Day Communication Plan is written and shared with the customer. This initial plan is very simple and is used to get some quick communications out to members of the organization to inform them that the project is starting as well as the transformation vision and purpose. Additional Business Transformation activities include conducting the Executive Alignment Workshop and the Fundamentals of Change Management for Key Stakeholders Workshop.</p>
                            <p>Therefore, the steps or procedures that are followed within the project preparation phase are described in the section below. The Project Charter that is generated in this phase forms the basis for the implementation approach of the project. </p>
                            <p>It must be highlighted that in case the customer or another team representing the customer is performing some parts of the activities or deliverables in this phase, these activities or deliverables are reviewed for completeness by the relevant stakeholders. Handling of customer completed activities or deliverables for each phase is captured as part of Customer Deliverables in the Project Charter Document.</p>


                            <h4>Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="23">Project Preparation Execution</td>
                                            <td><p><strong>1.	Define project scope</strong></p>
                                                <p>The high-level project scope is defined. The project scope should cover the following:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The organization’s business units included within the SAP Implementation </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The business process areas that are to be covered within the implementation</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The systems that are to be included in scope</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Exclusions to scope are explicitly stated</p></td>
                                            <td>PGM / PM / SA / FL / TL / VPS/ SVP/ BTS</td>
                                            <td>Project Charter</td>                                    
                                            <td>Proposed Statement of Work / Contract / Request for Proposal / Customer communication</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>2.	Define Project Approach</strong></p>
                                                <p>The project approach is defined to cover the implementation methodology as well as the standards to be used. The approach focuses on:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The project objectives</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The project timeline</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The different phases of the implementation</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The key deliverables within each phase</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The entry and exit criteria for the subsequent phases of the project lifecycle</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The procedures for Scope/Change Request Management, Issues Management, Risk Management, Assumptions, Action Items, and Decisions  within each phase</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The strategies defined to manage  </p>
                                                <p class="tab-content" style="padding-left: 40px">i.	Executive Alignment</p>
                                                <p class="tab-content" style="padding-left: 40px">ii.	Customer Project Team Training</p>
                                                <p class="tab-content" style="padding-left: 40px">iii.	End User Training</p>
                                                <p class="tab-content" style="padding-left: 40px">iv.	Testing</p>
                                                <p class="tab-content" style="padding-left: 40px">v.	Data Migration and Cutover (to migrate data from Legacy system to SAP) </p>
                                                <p class="tab-content" style="padding-left: 40px">vi.	Security</p>
                                                <p class="tab-content" style="padding-left: 40px">vii.	Communication</p>
                                                <p class="tab-content" style="padding-left: 40px">viii.	Knowledge Transfer and support</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	SAP Systems Landscape</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	SAP Systems Landscape Strategy</p></td>
                                            <td>PGM / PM / SA / FL / TL / BTS </td>
                                            <td>Project Charter / Project Schedule (MPP) / Project Status Tracking Sheet</td>
                                            <td>Proposed Statement of Work / Contract </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>3.	Assess Executive Alignment </strong></p>
                                                <p>Confirm the vision of the project and ensure there is executive alignment on projected outcomes and benefits of the project (ROI Strategy).  Executive alignment session will be carried out as required</p></td>
                                            <td>BTS</td>
                                            <td>Executive Alignment Strategy document</td>
                                            <td>ROI Strategy</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>4.	Conduct the Initial Business Transformation Readiness Assessment and prepare report</strong></p>
                                                <p>Conduct the Initial Business Transformation Readiness Assessment according to the approved approach. Prepare the report based on the assessment results.</p></td>
                                            <td>BTS / Relevant Customer Stakeholders</td>
                                            <td>Initial Business Transformation Readiness Assessment Report</td>
                                            <td>Initial Business Transformation Readiness Assessment Approach / project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>5.	Define the Project Governance</strong></p>
                                                <p>The Project Organization Structure is defined. The governance structure with the roles and responsibilities, the escalation and resolution process, the meeting frequency and agenda are all defined</p></td>
                                            <td>PGM / PM / Customer PM / Relevant Stakeholders from customer</td>
                                            <td>Project Charter </td>
                                            <td>Proposed Statement of Work / Contract</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>6.	Assign the Resources</strong></p>
                                                <p>Assign all required resources for the project</p></td>
                                            <td>PGM / PM / VPS / Customer PM</td>
                                            <td>Final Resource Plan </td>
                                            <td>Project Schedule / Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>7.	Onboard the project team with internal kick-off</strong></p>
                                                <p>Bring the team up to speed regarding:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The project location</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Project related logistics</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Project Timeline</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Project Objectives</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Governance structure</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Roles and responsibilities</p></td>
                                            <td>PGM / PM </td>
                                            <td>Administrative On-boarding Guide</td>
                                            <td>Project Org Chart / Statement of Work / Contract / Request for Proposal / On-boarding Check List</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>8.	Request setup of project in PSA</strong></p>
                                                <p>The project is created in PSA, and the resource allocations are made</p></td>
                                            <td>VPS / PSA Resources</td>
                                            <td>Not Available</td>
                                            <td>Proposed Statement of Work / Contract / Final Resource Plan</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>9.	Prepare Project Charter</strong></p>
                                                <p>The project charter document is prepared and reviewed internally to ensure the completeness and review comments are incorporated. The charter includes all the strategies prepared during this phase.</p></td>
                                            <td>PGM / PM / SA / FL / TL / VPS / SVP / Practice Leads, BTS</td>
                                            <td>Project Charter</td>
                                            <td>Proposed Statement of Work / Contract</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>10.	Review and approval of Project Charter</strong></p>
                                                <p>The completed Project Charter document is shared with the customer and the acceptance is obtained through the sign off</p></td>
                                            <td>PM / SA / Costumer’s PM / Relevant Stakeholders from Customer</td>
                                            <td>Signed Project Charter</td>
                                            <td>Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>11.	Prepare the Initial 30 Day Communication Plan</strong></p>
                                                <p>Preparation of the plan document and internal review. This is shared with the customer for execution.</p></td>
                                            <td>PM / BTS</td>
                                            <td>Approved Initial 30 Day Communication Plan</td>
                                            <td>Project Charter / Customer Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>12.	Prepare Project Kick Off Presentation</strong></p>
                                                <p>Preparation of the project kick off presentation and internal review of the document</p></td>
                                            <td>PGM / PM / SA / FL / TL  / FC / TC / BTS</td>
                                            <td>Project Kick Off Presentation</td>
                                            <td>Project Charter / Statement of Work / Contract</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>13.	Conduct Project Kick Off </strong></p>
                                                <p>The project is officially kicked off with the involvement of all relevant stakeholders</p></td>
                                            <td>PGM / PM / SA / FL / TL  / FC / TC / Customer project team / All relevant stakeholders</td>
                                            <td>Not Available</td>
                                            <td>Project Kick Off Presentation</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>14.	Prepare for Blueprint Workshops</strong></p>
                                                <p>The team completes the preparation to start the Blueprint workshops covering the following:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Identify the Business Process List</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Identify the key stakeholders/process owners to be involved in the workshops</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Prepare the Blueprint Workshops schedule </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Prepare the Blueprint Tracker</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Send out the invites to the relevant stakeholders / process owners for the workshops</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Collect/document all customer business processes that will be covered in the Blueprint workshops</p></td>
                                            <td>PGM / PM / SA / FL / TL / FC / TC / BTS / Customer project team</td>
                                            <td>Blueprint Workshop Schedule  / Blueprint Tracker</td>
                                            <td>Proposed SOW / Blueprint preparation check list</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>15.	Set up Sandbox environment</strong></p>
                                                <p>The Sandbox system which is the SAP system used for R&D as well as for demonstration purposes is set up.</p></td>
                                            <td>SAP Security / PM</td>
                                            <td>Not Available</td>
                                            <td>Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>16.	Train Customer Project Team for Blueprinting</strong></p>
                                                <p>Boot camp training conducted for the customer’s project team</p></td>
                                            <td>FL / TL / FC/ BTS /  Customer Project Team</td>
                                            <td>Not Available</td>
                                            <td>Project Team Training Materials</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>17.	Prepare Master Project Schedule</strong></p>
                                                <p>The project schedule is prepared and base-lined. This is the schedule that is used throughout the project lifecycle. This schedule is updated with further details as the project progresses.</p></td>
                                            <td>PM</td>
                                            <td>Baseline Project Schedule</td>
                                            <td>Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>18.	Prepare project transformation vision statement</strong></p>
                                                <p>The project sponsor along with the BT Lead will interview executive team and prepare a project vision statement. The vision statement may be a simple as a set of bulleted items or a formal vision statement.</p></td>
                                            <td>BTS</td>
                                            <td>Approved project vision statement</td>
                                            <td></td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>19.	Identify stakeholders and prepare the listing and mapping document.</strong></p>
                                                <p>The Stakeholder listing and mapping is an important document as we invite participants to workshops, prepare plans for change management, communication, and training.</p></td>
                                            <td>BTS</td>
                                            <td>Stakeholder Identification and Mapping</td>
                                            <td>Initial Business Transformation Readiness Assessment and any interview minutes and survey results</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>20.	Conduct the Fundamentals of Change Management for key Stakeholders Workshop</strong></p>
                                                <p>This workshop presents the fundamentals of change management to key stakeholders. The content for the workshop is contained in the presentation materials and should require a minimal amount of customization for the specific project.</p></td>
                                            <td>BTS</td>
                                            <td>Workshop materials</td>
                                            <td>Initial Business Transformation Readiness Assessment</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>21.	Conduct the Leading Change for Leaders Workshop</strong></p>
                                                <p>This workshop focusses on what leaders do to lead the change effort. It expands on the Change Management Fundamentals and focusses specifically on leadership behaviors</p></td>
                                            <td>BTS</td>
                                            <td>Workshop materials</td>
                                            <td>Initial Business Transformation Readiness Assessment</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>22.	Prepare Development Standards</strong></p>
                                                <p>The technical team reviews and modifies development standards as required for the specific project.</p></td>
                                            <td>TL / TC</td>
                                            <td>ABAP, Analytics, Java, .net Development Standards</td>
                                            <td></td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p><strong>23.	Store all documents created and updated by attune during the Phase on ONEattune in the Project workspace</strong></p>
                                                <p>It is important to retain attune intellectual property on ONE attune.  This activity applies to every member of the team.  It includes documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </p></td>
                                            <td>All attune project team members</td>
                                            <td>Updated ONEattune Project work space</td>
                                            <td>Documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.  </td>
                                        </tr>


                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Master Services Agreement</p>
                                            <p>•	Proposed Statement of Work or Contract</p>
                                            <p>•	Customer charter document / presentation</p>
                                            <p>•	Project delivery model</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Review and sign off of Project Charter by Customer (all project strategies are included)</p>
                                            <p>•	On-boarding Check List fulfilled (Optional) </p>
                                            <p>•	Completed Project Kick Off Meeting</p>
                                            <p>•	Completed Master Project Schedule</p>
                                            <p>•	Completed Initial Business Transformation Readiness Assessment Report</p>
                                            <p>•	Completed Project Transformation Vision Statement</p>
                                            <p>•	Completed Blueprint Workshop Schedule</p>
                                            <p>•	Sandbox environment in place</p>
                                            <p>•	Blueprint Preparation Check List fulfilled</p>
                                            <p>•	Completed Customer Project Team Training</p>
                                            <p>•	Completed Fundamentals of Change Management for Key Stakeholders Workshop</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Request for Proposal</p>
                                            <p>•	Proposed Statement of Work</p>
                                            <p>•	Proposed Contract</p>
                                            <p>•	High-level Project Timeline</p>
                                            <p>•	Pre-sales customer meeting minutes (Optional)</p>
                                            <p>•	Pre/Initial Business Transformation Readiness Assessment (Optional)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project Charter</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Updated High-level Timeline</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Project Organizational Chart</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Issue Management Process</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Risk Management Process</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Scope Management / Change Request Process</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Project Governance Procedures</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Testing Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Knowledge Transfer Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Security Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Support Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Data Migration and Cutover Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Data Cleansing Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Updated </p>
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	Resource Plan</p>
                                            <p>•	Master Project Schedule</p>
                                            <p>•	Administrative On-boarding Guide</p>
                                            <p>•	Business Blueprint Workshop Schedule</p>
                                            <p>•	Blueprint Tracker</p>
                                            <p>•	Data Validation Process</p>
                                            <p>•	Initial Business Readiness Assessment Approach</p>
                                            <p>•	Initial Business Readiness Assessment Report</p>
                                            <p>•	Executive Alignment Strategy</p>
                                            <p>•	Executive Alignment Workshop and Workshop Materials</p>
                                            <p>•	Business Transformation Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Organizational Change Management Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Communication Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	End User Training Strategy</p>
                                            <p>•	Initial 30 Day Communication Plan</p>
                                            <p>•	Fundamentals of Change Management for Key Stakeholders Workshop Materials</p>
                                            <p>•	Customer Project Team Training Strategy</p>
                                            <p>•	Stakeholder Listing and Mapping</p>
                                            <p>•	Systems Landscape Design</p>
                                            <p>•	SAP Systems Landscape Strategy</p>
                                            <p>•	ABAP Development Standards</p>
                                            <p>•	Analytics Development Standards</p>
                                            <p>•	Java / .net Development Standards</p>
                                            <p>•	Transport Management Process</p>

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Project Preparation Phase profitability</p>
                                            <p>•	Schedule and Effort variance on the Project Preparation phase</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The project may decide to use client defined templates and standards for the deliverables from within the Blueprinting phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>

                                            <p>•	Certain activities of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>

                                            <p>•	Certain activities of this process may be delivered in another phase of the project, as per the contractual requirements.  This tailoring is documented in the Project’s Process</p>

                                            <p>•	In case the client or another team representing the client is performing some parts of the activities and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>

                                        </div>
                                    </td>
                                    </tr>     
                                    
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Project Preparation Phase Process.pdf">Download Project Preparation Phase Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>  
                                                                                              
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <%--<h2>Document Templates</h2>--%>                       
                        

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <%--<h3>Functional & Technical</h3>--%>     
                                <h4>Templates</h4>
                                <p><a href="References\ABAP Development Guidelines.docx">1.  ABAP Development Guidelines</a></p> 
                                <p><a href="References\Blueprint Tracker.xls">2. Blueprint Tracker</a></p> 
                                <p><a href="References\Business Blueprint Workshop Schedule.xlsx">3. Business Blueprint Workshop Schedule</a></p> 
                                <p><a href="References\Business Transformation Strategy.docx">4. Business Transformation Strategy</a></p> 
                                <p><a href="References\Change Management Strategy.docx">5. Change Management Strategy</a></p> 
                                <p><a href="References\Coding Standards Java.doc">6.  Coding Standards Java</a></p>    
                                <p><a href="References\Coding Standards Microsoft.Net.docx">7.  Coding Standards Microsoft.Net</a></p>  
                                <p><a href="References\Communication Strategy.docx">8. Communication Strategy</a></p> 
                                <p><a href="References\End User Training Strategy.docx">9. End User Training Strategy</a></p> 
                                <p><a href="References\Executive_Alignment_Strategy.docx">10. Executive Alignment Strategy</a></p>
                                <p><a href="References\Executive Alignment Workshop.pptx">11. Executive Alignment Workshop</a></p>
                                <p><a href="References\Fundamentals of Change Mgmt for Key Stakeholders.pptx">12. Fundamentals of Change Management for Key Stakeholders</a></p>
                                <p><a href="References\Initial 30 Day Communication Plan.docx">13. Initial 30 Day Communication Plan</a></p> 
                                <p><a href="References\Initial Business Transformation Readiness Assessment Approach.docx">14. Initial Business Transformation Readiness Assessment Approach</a></p>    
                                <p><a href="References\Initial Business Transformation Readiness Report.pptx">15. Initial Business Transformation Readiness Report</a></p>  
                                <p><a href="References\System Integration Landscape.vsd">16. Integration Landscape Diagram</a></p>
                                <p><a href="References\Issue Management Process.docx">17. Issue Management Process</a></p>
                                <p><a href="References\Master Project Schedule.mpp">18. Master Project Schedule</a></p>
                                <p><a href="References\Meeting Minutes.docx">19. Meeting Minutes</a></p>
                                <p><a href="References\Milestone Report.xlsx">20. Milestone Report</a></p>
                                <p><a href="References\Onboarding Guide.docx">21. Onboarding Guide</a></p> 
                                <p><a href="References\Project Charter.docx">22.  Project Charter</a></p>
                                <p><a href="References\Project Governance.pptx">23.  Project Governance</a></p>
                                <p><a href="References\Project Organizational Chart.pptx">24.  Project Organizational Chart</a></p>
                                <p><a href="References\Project Status Tracking Sheet.xlsm">25.  Project Status Tracking Sheet</a></p>
                                <p><a href="References\Project Team Training Strategy.docx">26. Project Team Training Strategy</a></p>
                                <p><a href="References\Risk Management Plan.docx">27.  Risk Management Plan</a></p>
                                <p><a href="References\SAP System Landscape Stratergy.docx">28.  SAP System Landscape Stratergy</a></p>
                                <p><a href="References\Scope Management & Change Request Process.docx">29.  Scope Management & Change Request Process</a></p>
                                <p><a href="References\Services Methodology Operational Review Evaluation.xlsx">30. Services Methodology Operational Review Evaluation</a></p>
                                <p><a href="References\Size Estimation Sheet.xlsx">31. Size Estimation Sheet </a></p>
                                <p><a href="References\Stage Gate Review.pptx">32. Stage Gate Review</a></p>
                                <p><a href="References\Stakeholder Identification with Grid Mapping.xlsx">33. Stakeholder Identification with Grid Mapping</a></p> 
                                <p><a href="References\Testing Strategy.docx">34. Testing Strategy</a></p>
                          </div>
<%--                          <div class="col-xs-6 col-md-4"> 
                                <h3>PMO & Testing</h3>   
                                <p><a href="https://life.oneattune.com/mlink/file/MzQ2MzY" target="_blank">1.  attune PMO Project Governance</a></p> 
                                <p><a href="https://life.oneattune.com/mlink/file/NDE4NTc" target="_blank">2.  attune ABAP development Guidelines</a></p>  
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTA">3.  attune Coding Standards Java</a></p>    
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTE">4.  attune Coding Standards Microsoft.Net</a></p> 
                          </div> --%>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>Business Transformation</h3>--%>
                                <h4>Guidelines</h4>
                                <p><a href="References\Branded aIR Diagram.pptx">1.  Branded aIR Diagram</a></p> 
                                <p><a href="References\Phase Process Summary.docx">2. Phase Process Summary</a></p> 

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>   
                                <p><a href="References\Onboarding Guide Sample.docx">1.  Onboarding Guide Sample</a></p> 
                                <p><a href="References\Project Organizational Chart Sample.pptx">2.  Project Organizational Chart Sample</a></p> 
                          </div>
                        </div>                        

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                        </div> 

                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                        </div> --%>


<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->

</asp:Content>
