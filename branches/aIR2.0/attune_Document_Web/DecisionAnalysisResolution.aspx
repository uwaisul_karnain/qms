﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="DecisionAnalysisResolution.aspx.cs" Inherits="attune_Document_Web.DecisionAnalysisResolution" %>

<asp:Content ID="decisionAnalysisResolutionPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Decision  Analysis  Resolution  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The objective of Decision Analysis and Resolution (DAR) process is to ensure that important decisions that affect projects and the organization are arrived at after formally evaluating alternatives against identified criteria.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>Important decisions related to the project are taken using the formal DAR process using the DAR Form. In addition to decisions taken within the project, certain organizational decisions that impact multiple projects need to be taken using the formal DAR process.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>GM - </strong>Group Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>DAR - </strong>Decision Analysis Resolution</p>
                                <p><strong>SOW  - </strong>Statement of Work</p>                                                                                       
                          </div>
                          <div class="col-xs-6 col-md-4">                                 
                                <p><strong>PAD  - </strong>Project Approach Document</p>
                                <p><strong>R&D  - </strong>Research and Development</p>                            
                          </div>
                        </div>

                        <div class="data">
                            <h3>Process and Steps</h3>     

                            <p>Decisions important to projects and attune need to be arrived at after thorough analysis. A formal evaluation process reduces the subjectivity of such decision and also creates a documented rationale for decision. The use of the DAR process ensures that appropriate analysis with the involvement of relevant stakeholders is done to arrive at such decisions.</p>

                            <p>The DAR process requires formal implementation of the following activities:</p>
                            <p class="tab-content" style="padding-left: 40px">•	Identifying the need for formal decision-making in a particular instance</p>
                            <p class="tab-content" style="padding-left: 40px">•	Identifying alternative solutions/ decisions</p>
                            <p class="tab-content" style="padding-left: 40px">•	Establishing evaluation criteria and evaluation methods for alternatives</p>
                            <p class="tab-content" style="padding-left: 40px">•	Performing the evaluation</p>
                            <p class="tab-content" style="padding-left: 40px">•	Selecting the solution/ decision</p>

                            <p>The DAR process needs to be applied in the following situations:</p>
                            <p class="tab-content" style="padding-left: 40px">•	The decision has reasonable high impact on the project or organization, e.g., ability to achieve project objectives, schedule delay, or cost overrun.</p>
                            <p class="tab-content" style="padding-left: 40px">•	The decision has multiple alternatives to select from, and none of the alternatives are “obvious” as the correct solution</p>
                            <p class="tab-content" style="padding-left: 40px">•	There is need to take into account the views of multiple stakeholders in making the decision</p>
                            <p class="tab-content" style="padding-left: 40px">•	The decision carries a medium to high risk</p>
                            <p class="tab-content" style="padding-left: 40px">•	Formal decision making record is required because of legal/ contractual obligations</p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td rowspan="6"><strong>Plan for Formal Decision-Making </strong></td>
                                            <td><p>Identify the need for formal decision-making.</p>
                                                <p>Initiate DAR by updating the Problem Statement section in DAR Report</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Decision to be taken</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Description of the decision</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Potential impact of the decision – justifying why it is important to take the decision formally</p>
                                                <p>(Template : DAR Form)</p></td>
                                            <td rowspan="2">PM, GM</td>
                                            <td rowspan="2">DAR Form</td>                                    
                                            <td rowspan="9">SOW, PAD, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Identify;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Activities</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Persons and Roles</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Dates</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Efforts for the required activities</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Other Resources (such as hardware, software or access to external information)</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Inform all relevant stakeholders about the plan and gets their concurrence.</p></td>
                                            <td>PM</td>
                                            <td>Mail</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Identify;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Criteria to evaluate</p>
                                                <p class="tab-content" style="padding-left: 50px">May based on a combination of numeric and non-numeric analysis.</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Possible alternatives for the decision. Use;</p>
                                                <p class="tab-content" style="padding-left: 50px">o	R&D Survey via Interned</p>
                                                <p class="tab-content" style="padding-left: 50px">o	Brainstorming sessions</p>
                                                <p class="tab-content" style="padding-left: 50px">o	Literature surveys</p>
                                                <p class="tab-content" style="padding-left: 50px">o	Review of past project data </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Evaluation Methods for all criteria</p>
                                                <p class="tab-content" style="padding-left: 50px">o	attune has formalized on the PUGH Matrix method</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Required weightage and values, Type of measures for alternatives and evaluation criteria</p></td>
                                            <td rowspan="2">PM, Team</td>
                                            <td rowspan="2">DAR Form</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The results of the evaluations are documented.</p> 
                                                <p>Additional documents are embedded/attached to support the evaluation</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Once the evaluation is complete, the results are handed over to the decision-makers identified in the Team involve on decision for taking the final decision</p></td>
                                            <td>PM</td>
                                            <td>DAR Form</td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="3"><strong>Select Alternative</strong></td>
                                            <td><p>The identified decision-makers select a solution based on the evaluation result.</p></td>
                                            <td rowspan="3">Team involve on decision</td>
                                            <td rowspan="2">DAR Form</td>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>In case there is no single, clear-cut alternative that is far superior to others (based on the evaluation), the decision-makers document the rationale along with their decision.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The final decision is approved by the immediate supervisor of the decision-maker.
                                                <p>All relevant stakeholders are informed.</p></td>
                                            <%--<td></td>--%>
                                            <td>DAR Form, Mail</td>
                                            <%--<td></td>--%>
                                        </tr>
                                    </tbody>


                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>

                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Need for formal decision-making in a project is identified during project initiation, planning, execution, or during review meetings such as management review of projects and project status reviews.</p>
                                            <p>•	Need for formal decision-making in non-project work may be identified during planning or in status reviews and management review of operations.</p>
                                            <p>•	Research or Evaluation activity declared as complete (DAR Criteria Selection)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Communication of DAR - Decision Record to all relevant stakeholders for decision making</p>
                                            <p>•	Decision / solution is selected </p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Minutes of Meetings</p>
                                            <p>•	Project related plans such as PAD and Schedule</p>
                                            <p>•	Plans related to non-project work such as organizational training, software process improvement, etc.</p>
                                            <p>•	DAR Criteria Selection DAR Criteria Selection Completed</p>
                                            <p>•	Data collected during evaluation or R&D</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	DAR Criteria Selection Completed Document (DAR Form)</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort for Decision Analysis Resolution</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	Evaluation methods other than those mentioned here may be used</p>
                                            <p>•	The Decision Analysis and Resolution Report template may be substituted/ modified to suit the context of the decision</p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\attune Decission Analysis Record Template.xlsx">1. attune Decission Analysis Resolution</a></p>                                       
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>       
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                        
                                               
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>

