﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AgileProjectPreparation.aspx.cs" Inherits="attune_Document_Web.AgileProjectPreparation" %>

<asp:Content ID="projectPreparationPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">--%>
                <%--<div class="row">
                    <div class="col-lg-12">

                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Preparation  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Project Preparation phase of the SAP implementation project lifecycle.  It covers the activities that need to be performed as well as the deliverables within the Project Preparation phase.</p>
                            <p>During the Project Preparation Phase, the project scope, the project plan, the project resources, and project approach are agreed upon.  In addition, the required project standards and procedures are agreed upon.  The FRS environment is set up and the attune Fashion Suite installed in preparation for the FRS workshops.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on the contractual requirements and the methodology imposed by the client.  The activities to be fulfilled in this phase are mentioned in detail under the Process Overview of this document.</p>
                               
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <p>A complete list of Acronyms, Abbreviations, and Definitions can be found in the document entitled “aIR Terminology”, found in the Guidelines section of the aIR Methodology portal.</p>
	

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h3>Process and Steps</h3>     
  
                            <p>The SAP Implementation lifecycle followed by attune is based upon phases defined in the attune Implementation Roadmap (aIR), derived from Agile principles, and PMI, SAP ASAP Focus, and SAP Activate methodologies. The aIR Methodology includes elements of Agile principles for business requirements gathering and development, while maintaining tried and true traditional approaches to ERP implementations.  </p>
                            <p>Project Preparation is the first phase within the aIR methodology. During the Project Preparation Phase, the project scope, the project plan, the project resources, and project approach are agreed upon. In addition, the required project standards and procedures are agreed upon. These standards and procedures cover the topics such as scope management, change control, issue management, escalation and resolution, documentation, and risk management. The Project Status Tracking Sheet is prepared and put in place in order to track the Issues, Changes, Risks, Assumptions, Decisions, and Action items in this phase as well as in the subsequent phases that follow. The Project strategies are defined, documented, and agreed to. These are included in the Project Charter.</p>
                            <p>Preparation for the FRS Workshops begins during the Project Preparation Phase.  The FRS environment is created and the attune Fashion Suite is installed.  The customer’s Business Process Owners (BPO’s) share the company’s future operating model with the attune consultants.  The FRS Workshop Schedule is prepared and invitations are sent to participants. </p>
                            <p>During the Project Preparation Phase, Business Transformation begins with the Initial Business Transformation Readiness Assessment and subsequent report presenting the results of the assessment along with recommended actions. An Initial 30 Day Communication Plan is written and shared with the customer. This initial plan is very simple and is used to get some quick communications out to members of the organization to inform them that the project is starting as well as the transformation vision and purpose. Additional Business Transformation activities include conducting the Executive Alignment Workshop and the Fundamentals of Change Management for Key Stakeholders Workshop.</p>


                            <h4>Steps</h4>
                                     
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activities</th>
                                        <th>Responsible / Participants</th>
                                        <th>Artifacts</th>
                                        <th>Reference Documents</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="1"><strong>Project Charter</strong></td>
                                            <td><p><strong>1.	Define overall project scope and approach</strong></p>
                                                <p>The high-level project scope is defined. Note:  Applicable when attune is the SI for the project. When attune partners with another SI, attune will define the FMS Scope and Approach.  The project scope should cover the following:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The organization’s business units included within the SAP Implementation </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The business process areas that are to be covered within the implementation</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The systems that are to be included in scope</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Exclusions to scope are explicitly stated</p></td>
                                            <td><strong>PGM</strong> / PM / SA / FL / TL / VPS/ SVP/ BTS</td>
                                            <td>Project Charter</td>                                    
                                            <td>Proposed Statement of Work / Contract / Request for Proposal / Customer communication</td>
                                        </tr>
                                       
                                        <tr>
                                            <td rowspan="1"><strong>Project Charter</strong></td>
                                            <td><p><strong>2.	Define Project Approach</strong></p>
                                                <p>The project approach is defined to cover the implementation methodology as well as the standards to be used. Note:  Applicable when attune is the SI for the project.  When attune partners with another SI, attune will follow the methodology as outlined by the SI.  The approach focuses on:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The project objectives</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The project timeline</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The different phases of the implementation</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The key deliverables within each phase</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The entry and exit criteria for the subsequent phases of the project lifecycle</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The procedures for Scope/Change Request Management, Issues Management, Risk Management, Assumptions, Action Items, and Decisions  within each phase</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The strategies defined to manage</p>
                                                    <p class="tab-content" style="padding-left: 40px">i.	Executive Alignment</p>
                                                    <p class="tab-content" style="padding-left: 40px">ii.	Customer Project Team Training</p>
                                                    <p class="tab-content" style="padding-left: 40px">iii.	End User Training</p>
                                                    <p class="tab-content" style="padding-left: 40px">iv.	Testing</p>
                                                    <p class="tab-content" style="padding-left: 40px">v.	Data Migration and Cutover (to migrate data from Legacy system to SAP) </p>
                                                    <p class="tab-content" style="padding-left: 40px">vi.	Security</p>
                                                    <p class="tab-content" style="padding-left: 40px">vii.	Communication</p>
                                                    <p class="tab-content" style="padding-left: 40px">viii.	Knowledge Transfer and support </p>
                                                    <p class="tab-content" style="padding-left: 40px">ix.	SAP Systems Landscape Strategy</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	Integration Landscape Diagram</p>
                                                </td>
                                            <td><strong>PGM</strong> / PM / SA / FL / TL / BTS </td>
                                            <td>Project Charter / Master Project Schedule / Project Status Tracking Sheet</td>                                    
                                            <td>Proposed Statement of Work / Contract</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Project Charter</strong></td>
                                            <td><p><strong>3.	Define the Project Governance</strong></p>
                                                <p>The Project Organization Structure is defined. The governance structure with the roles and responsibilities, the escalation and resolution process, the meeting frequency and agenda are all defined</p>
                                                </td>
                                            <td><strong>PGM</strong> / PM / Customer PM / Relevant Stakeholders from customer</td>
                                            <td>Project Charter / Project Governance Procedures /  Project Organizational Chart</td>                                    
                                            <td>Proposed Statement of Work / Contract</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Project Charter</strong></td>
                                            <td><p><strong>4.	Prepare the Project Charter</strong></p>
                                                <p>The project charter document is prepared and reviewed internally.  The charter includes all the strategies prepared during this phase.</p>
                                                </td>
                                            <td><strong>PGM</strong> / PM / SA / FL / TL / VPS / SVP / Practice Leads, BTS</td>
                                            <td>Project Charter</td>                                    
                                            <td>Proposed Statement of Work / Contract</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Project Charter</strong></td>
                                            <td><p><strong>5.	Review and approval of Project Charter</strong></p>
                                                <p>The completed Project Charter document is shared with the customer and the acceptance is obtained through the sign off.</p>
                                                </td>
                                            <td><strong>PM</strong> / SA / Customer’s PM / Relevant Stakeholders from Customer</td>
                                            <td>Signed Project Charter</td>                                    
                                            <td>Project Charter</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>BTS Project Preparation</strong></td>
                                            <td><p><strong>6.	Assess Executive Alignment </strong></p>
                                                <p>Confirm the vision of the project and ensure there is executive alignment on guiding principles, projected outcomes, and benefits of the project (ROI Strategy).  Executive alignment session will be carried out as required</p>
                                                </td>
                                            <td><strong>BTS</strong></td>
                                            <td>Executive Alignment Strategy</td>                                    
                                            <td>ROI Strategy</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>BTS Project Preparation</strong></td>
                                            <td><p><strong>7.	Conduct the Initial Business Transformation Readiness Assessment and prepare report</strong></p>
                                                <p>Conduct the Initial Business Transformation Readiness Assessment according to the approved approach. Prepare the report based on the assessment results.</p>
                                                </td>
                                            <td><strong>BTS</strong> / Relevant Customer Stakeholders</td>
                                            <td>Initial Business Transformation Readiness Report</td>                                    
                                            <td>Initial Business Transformation Readiness Assessment Approach / Project Charter</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>BTS Project Preparation</strong></td>
                                            <td><p><strong>8.	Prepare the Initial 30 Day Communication Plan</strong></p>
                                                <p>Preparation of the plan document and internal review. This is shared with the customer for execution.</p>
                                                </td>
                                            <td><strong>BTS</strong> / PM</td>
                                            <td>Approved Initial 30 Day Communication Plan</td>                                    
                                            <td>Project Charter / Customer Project Charter</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>BTS Project Preparation</strong></td>
                                            <td><p><strong>9.	Prepare Project Transformation Vision Statement</strong></p>
                                                <p>The project sponsor along with the BT Lead will interview executive team and prepare a project vision statement. The vision statement may be a simple as a set of bulleted items or a formal vision statement.</p>
                                                </td>
                                            <td><strong>BTS</strong></td>
                                            <td>Approved Business Transformation Strategy</td>                                    
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>BTS Project Preparation</strong></td>
                                            <td><p><strong>10.	Identify stakeholders and prepare the listing and mapping document.</strong></p>
                                                <p>The Stakeholder listing and mapping is an important document as we invite participants to workshops, prepare plans for change management, communication, and training.</p>
                                                </td>
                                            <td><strong>BTS</strong></td>
                                            <td>Stakeholder Identification with Grid Mapping</td>                                    
                                            <td>Initial Business Transformation Readiness Assessment and any interview minutes and survey results</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>BTS Project Preparation</strong></td>
                                            <td><p><strong>11.	Conduct the Fundamentals of Change Management for key Stakeholders Workshop</strong></p>
                                                <p>This workshop presents the fundamentals of change BTS Project Preparation management to key stakeholders. The content for the workshop is contained in the presentation materials and should require a minimal amount of customization for the specific project.</p>
                                                </td>
                                            <td><strong>BTS</strong></td>
                                            <td>Fundamentals of Change Management for Key Stakeholders</td>                                    
                                            <td>Initial Business Transformation Readiness Assessment</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>BTS Project Preparation</strong></td>
                                            <td><p><strong>12.	Conduct the Leading Change for Leaders Workshop</strong></p>
                                                <p>This workshop focusses on what leaders do to lead the change effort. It expands on the Change Management Fundamentals and focusses specifically on leadership behaviors</p>
                                                </td>
                                            <td><strong>BTS</strong></td>
                                            <td>Leading Change for Leaders Workshop Materials</td>                                    
                                            <td>Initial Business Transformation Readiness Assessment</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Onboard Project Team</strong></td>
                                            <td><p><strong>13.	Assign Project Resources</strong></p>
                                                <p>Assign all required resources for the project</p>
                                                </td>
                                            <td><strong>PGM</strong> / PM / VPS / Customer PM</td>
                                            <td>Final Resource Plan / Baseline Plan / Project Organizational Chart</td>                                    
                                            <td>Project Schedule / Project Charter </td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2"><strong>Onboard Project Team</strong></td>
                                            <td><p><strong>14.	Onboard the project team with internal kick-off</strong></p>
                                                <p>Prepare the Project Onboarding Guide Bring the team up to speed regarding:
</p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	The project location</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	Project related logistics</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	Project Timeline</p>
                                                    <p class="tab-content" style="padding-left: 20px">d.	Project Objectives</p>
                                                    <p class="tab-content" style="padding-left: 20px">e.	Governance structure</p>
                                                    <p class="tab-content" style="padding-left: 20px">f.	Roles and responsibilities</p>
                                                </td>
                                            <td><strong>PGM</strong>/ PM </td>
                                            <td>Onboarding Guide</td>                                    
                                            <td>Project Org Chart / Statement of Work / Contract / Request for Proposal / On-boarding Check List / Resource Plan</td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>15.	Perform Stakeholder and Project Team Training on attune aIR Methodology and Agile project principles. </strong></p>
                                                <p>The Stakeholder and Project Team Training consists of an introduction to aIR Methodology, including Agile principles.  This training is particularly relevant for customers for which Agile methodology is new. 
</p>
                                                </td>
                                            <td><strong>Agile Coach</strong>, All Stakeholders and Project Team Members</td>
                                            <td>Introduction to aIR Methodology</td>                                    
                                            <td>Phase Process Documents / aIR Methodology Templates / aIR Guidelines / aIR Samples</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Onboard Project Team</strong></td>
                                            <td><p><strong>16.	Request setup of project in PSA and submit the Baseline Plan</strong></p>
                                                <p>The project is created in PSA, the Baseline Plan is entered and the resource assignments are made</p>
                                                </td>
                                            <td><strong>VPS</strong> / PSA Resources</td>
                                            <td>Baseline Plan </td>                                    
                                            <td>Proposed Statement of Work / Contract / Final Resource Plan</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Onboard Project Team</strong></td>
                                            <td><p><strong>17.	Prepare Project Kick Off Presentation</strong></p>
                                                <p>Preparation of the project kick off presentation and internal review of the document</p>
                                                </td>
                                            <td><strong>PGM</strong> / PM / SA / FL / TL  / FC / TC / BTS</td>
                                            <td>Project Kick Off Meeting Presentation</td>                                    
                                            <td>Project Charter / Statement of Work / Contract</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Onboard Project Team</strong></td>
                                            <td><p><strong>18.	Conduct Project Kick-Off</strong></p>
                                                <p>The project is officially kicked off with the involvement of all relevant stakeholders </p>
                                                </td>
                                            <td><strong>PGM</strong> / PM / SA / FL / TL  / FC / TC / Customer project team / All relevant stakeholders</td>
                                            <td></td>                                    
                                            <td>Project Kick Off Presentation</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Plan FRS Workshops</strong></td>
                                            <td><p><strong>19.	Share Target Operating Model / Future Vision with attune Consultants</strong></p>
                                                <p>The Solution Architect and the PM will collaborate with the customer’s PM and BPO’s to schedule a series of informational sessions that cover the customer’s vision for how the customer wants to operate in the future.</p>
                                                <p>The customer’s BPO’s lead these informational sessions and the attune consultants participate. </p>
                                                <p>The customer may use any materials that they feel will provide the consultants with the necessary information to prepare the consultants for leading FRS workshops. These may be offered as pre-reads prior to the information session.</p>
                                                </td>
                                            <td><strong>BPOs</strong> / FL / SA / PM / Customer PM / ITSO’s</td>
                                            <td>N/A</td>                                    
                                            <td>Customer’s Target Operating Model / Future Vision  documentation</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Plan FRS Workshops</strong></td>
                                            <td><p><strong>20.	Prepare FRS Workshop Schedule</strong></p>
                                                <p>The team completes the FRS Workshop schedule:
</p>
                                                    <p class="tab-content" style="padding-left: 20px">a.	Confirm  the Business Process List relevant for the customer</p>
                                                    <p class="tab-content" style="padding-left: 20px">b.	Identify the key stakeholders/process owners to be involved in the workshops</p>
                                                    <p class="tab-content" style="padding-left: 20px">c.	Prepare the FRS Workshops schedule </p>
                                                    <p class="tab-content" style="padding-left: 20px">d.	Prepare the FRS Workshop Tracker</p>
                                                    <p class="tab-content" style="padding-left: 20px">e.	Send out the invites to the relevant stakeholders / process owners for the workshops</p>
                                                    <p class="tab-content" style="padding-left: 20px">f.	Collect all customer business processes documentation that will be covered in the FRS workshops</p>
                                                </td>
                                            <td><strong>PGM</strong> / PM / SA / FL / TL / FC / TC / BTS / Customer project team</td>
                                            <td>FRS Workshop Schedule  / FRS  Tracker</td>                                    
                                            <td>Project Charter / Proposed SOW / FRS Preparation check list / FMS Business Process List</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Plan FRS Workshops</strong></td>
                                            <td><p><strong>21.	Set up the FRS environment and install attune Fashion Suite</strong></p>
                                                <p>The FRS system which is the SAP system used for the FRS Workshops, is created and available for personalization.  The FRS system leverages the attune Fashion Suite as the starting point.  This system will be used during FRS workshops as well as for demonstration purposes.</p>
                                                <p>The sandbox is also set up at this time.</p>
                                                </td>
                                            <td><strong>TL</strong> / SAP Security / PM </td>
                                            <td>N/A</td>                                    
                                            <td>Project Charter</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Plan FRS Workshops</strong></td>
                                            <td><p><strong>22.	Train Customer Project Team for FRS Workshops</strong></p>
                                                <p>Training conducted for the customer’s project team on the methodology to prepare for the FRS Workshops in the FRS Phase of the project.</p>
                                                
                                                </td>
                                            <td><strong>FL</strong> / TL / FC/ BTS /  Customer Project Team</td>
                                            <td>N/A</td>                                    
                                            <td>Project Team Training Materials</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Master Project Plan</strong></td>
                                            <td><p><strong>23.	Prepare Master Project Schedule</strong></p>
                                                <p>The project schedule is prepared and base-lined. This is the schedule that is used throughout the project lifecycle. This schedule is updated with further details as the project progresses.  Note: Applicable when attune is the SI for the project.  When partnering with another SI, attune will prepare the FMS Project Schedule. </p>
                                                
                                                </td>
                                            <td><strong>PM</strong></td>
                                            <td>Master Project Schedule</td>                                    
                                            <td>Project Charter</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2"><strong>Development Standards</strong></td>
                                            <td><p><strong>24.	Prepare Development Standards</strong></p>
                                                <p>The technical team reviews and modifies development standards as required for the specific project.</p>
                                                
                                                </td>
                                            <td><strong>TL</strong> / TC</td>
                                            <td>ABAP Development Guidelines, Analytics Development Standards, Coding Standards Java, Coding Standards .net </td>                                    
                                            <td></td>
                                        </tr>
                                        <tr>
                                            
                                            <td><p><strong>25.	Conduct Exit Stage-Gate Review</strong></p>
                                                <p>Conduct the Exit Stage-Gate Review.  All items must be passed, and exceptions approved by the Steering Committee.</p>
                                                
                                                </td>
                                            <td><strong>PM </strong>/ Project Team Members / Steering Committee / Stakeholders</td>
                                            <td>Stage Gate Review</td>                                    
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td rowspan="1"><strong>Archive Documents</strong></td>
                                            <td><p><strong>26.	Store all documents created and updated by attune during the Phase on ONEattune in the Project workspace</strong></p>
                                                <p>It is important to retain attune intellectual property on ONE attune.  This activity applies to every member of the team.  It includes documents used for an activity (e.g. Blueprint Workshop presentations) as well as deliverable artifacts.</p>
                                                
                                                </td>
                                            <td><strong>PM</strong> / attune Project Team members</td>
                                            <td>Updated ONEattune Project work space</td>                                    
                                            <td>Documents used for an activity (e.g. FRS Workshop presentations) as well as </td>
                                        </tr>

                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Signed Master Services Agreement</p>
                                            <p>•	Proposed Statement of Work or Contract</p>
                                            <p>•	Completed Customer charter document / presentation</p>
                                            <p>•	Proposed Project delivery model</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Review and sign off of Project Charter by Customer (all project strategies are included)</p>
                                            <p>•	attune Project Team Onboarding Completed</p>
                                            <p>•	Guiding Principles are established by the Project Sponsors</p>
                                            <p>•	Completed Project Kick Off Meeting</p>
                                            <p>•	Completed Master Project Schedule</p>
                                            <p>•	Completed Initial Business Transformation Readiness Assessment Report</p>
                                            <p>•	Target Operating Model / Future Vision shared with the attune consultants by the BPO’s</p>
                                            <p>•	Applicable Business Processes (Master Process List) Identified</p>
                                            <p>•	Completed FRS Workshop Schedule and invitations sent to all participants</p>
                                            <p>•	Customer resources committed to attend scheduled FRS workshops</p>
                                            <p>•	FRS Workshop Tracker is developed</p>
                                            <p>•	Completed set up of FRS environment and attune Fashion Suite installed and available for personalization</p>
                                            <p>•	SAP Sandbox environment in place</p>
                                            <p>•	Completed Project Team Training on aIR Methodology and Agile Principles to be used during the FRS Phase</p>
                                            <p>•	Completed Customer Team Training on FRS Workshop methodology</p>
                                            <p>•	Completed Project Transformation Vision Statement</p>
                                            <p>•	Completed Fundamentals of Change Management for Key Stakeholders Workshop</p>
                                            <p>•	Completed Leading Change for Leaders Workshop</p>
                                            <p>•	Completed Project Preparation Exit Stage Gate Review with all items passed and exceptions approved by Steering Committee</p>
                                            <p>•	Customer Project Team Training completed (optional, if purchased)</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Request for Proposal</p>
                                            <p>•	Master Process List</p>
                                            <p>•	Proposed Statement of Work</p>
                                            <p>•	Proposed Contract</p>
                                            <p>•	High-level Project Timeline</p>
                                            <p>•	Pre-sales customer meeting minutes (Optional)</p>
                                            <p>•	Pre/Initial Business Transformation Readiness Assessment (Optional)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Signed-off Statement of Work / Contract</p>
                                            <p>•	Project Charter</p>
                                            <p class="tab-content" style="padding-left: 20px">o	High-level Timeline</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Project Organizational Chart</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Issue Management Process</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Risk Management Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Scope Management & Change Request Process</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Project Governance Procedures</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Testing Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Knowledge Transfer Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Security Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Support Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Data Migration and Cutover Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Business Transformation Strategy</p>
                                            <p class="tab-content" style="padding-left: 40px">•	Change Management Strategy</p>
                                            <p class="tab-content" style="padding-left: 40px">•	Change Impact Assessment Strategy</p>
                                            <p class="tab-content" style="padding-left: 40px">•	Communication Strategy</p>
                                            <p class="tab-content" style="padding-left: 40px">•	End User Training Strategy</p>

                                            <p class="tab-content" style="padding-left: 20px">o	Executive Alignment Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	Project Team Training Strategy</p>
                                            <p class="tab-content" style="padding-left: 20px">o	SAP System Landscape Strategy</p>
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	Final Resource Plan</p>
                                            <p>•	Master Project Schedule</p>
                                            <p>•	Project Onboarding Guide</p>
                                            <p>•	FRS Workshop Schedule</p>
                                            <p>•	FRS Workshop Tracker</p>
                                            <p>•	Initial Business Transformation Readiness Assessment Approach</p>
                                            <p>•	Initial Business Transformation Readiness Report</p>
                                            <p>•	Executive Alignment Workshop and Workshop Materials</p>
                                            <p>•	Initial 30 Day Communication Plan</p>
                                            <p>•	Fundamentals of Change Management for Key Stakeholders Workshop Materials</p>

                                            <p>•	Stakeholder Identification with Grid Mapping</p>
                                            <p>•	Leading Change for Leaders Workshop Materials</p>
                                            <p>•	Integration Landscape Diagram</p>
                                            <p>•	ABAP Development Standards</p>
                                            <p>•	Analytics Development Standards</p>
                                            <p>•	Java / .net Development Standards</p>
                                            <p>•	Stage Gate Review Dashboard</p>

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Project Preparation Phase profitability</p>
                                            <p>•	Schedule and Effort variance on the Project Preparation phase</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The project may decide to use client defined templates and standards for the deliverables from within the Blueprinting phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>

                                            <p>•	Certain activities of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>

                                            <p>•	Certain activities of this process may be delivered in another phase of the project, as per the contractual requirements.  This tailoring is documented in the Project’s Process</p>

                                            <p>•	In case the client or another team representing the client is performing some parts of the activities and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>

                                        </div>
                                    </td>
                                    </tr>     
                                    
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Project Preparation Phase Process.pdf">Download Project Preparation Phase Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>  
                                                                                              
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <%--<h2>Document Templates</h2>--%>                       
                        

                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <%--<h3>Functional & Technical</h3>--%>     
                                <h4>Templates</h4>
                                
                                <p><a href="References\ABAP Development Guidelines.docx">1. ABAP Development Standards</a></p>
                                <p><a href="#">2. Analytics Development Standards</a></p>
                                <p><a href="References\Business Transformation Strategy.docx">3. Business Transformation Strategy</a></p>
                                <p><a href="References\Change Management Strategy.docx">4. Change Impact Assessment Strategy</a></p>
                                <p><a href="References\Communication Strategy.docx">5. Communication Strategy</a></p>
                                <p><a href="References\Project Team Training Strategy.docx">6. Customer Project Team Training Strategy</a></p>
                                <p><a href="#">7. Data Migration and Cutover Strategy</a></p>
                                <p><a href="References\End User Training Strategy.docx">8. End User Training Strategy</a></p>
                                <p><a href="References\Executive_Alignment_Strategy.docx">9. Executive Alignment Strategy</a></p>
                                <p><a href="References\Executive Alignment Workshop.pptx">10. Executive Alignment Workshop and Workshop Materials</a></p>
                                <p><a href="References\Business Blueprint Workshop Schedule.xlsx">11. FRS Workshop Schedule</a></p>
                                <p><a href="#">12. FRS Workshop Tracker</a></p>
                                <p><a href="References\Fundamentals of Change Mgmt for Key Stakeholders.pptx">13. Fundamentals of Change Management for Key Stakeholders Workshop Materials</a></p>
                                <p><a href="References\Initial 30 Day Communication Plan.docx">14. Initial 30 Day Communication Plan</a></p>
                                <p><a href="References\Initial Business Transformation Readiness Assessment Approach.docx">15. Initial Business Readiness Assessment Approach</a></p>
                                <p><a href="References\Initial Business Transformation Readiness Report.pptx">16. Initial Business Readiness Assessment Report</a></p>
                                <p><a href="References\System Integration Landscape.vsd">17. Integration Landscape Diagram</a></p>
                                <p><a href="References\Issue Management Process.docx"">18. Issue Management Process</a></p>
                                <p><a href="#">19. Knowledge Transfer Strategy</a></p>
                                <p><a href="#">20. Leading Change for Leaders Workshop Materials</a></p>
                                <p><a href="References\Master Project Schedule.mpp">21. Master Project Schedule</a></p>
                                <p><a href="References\Project Organizational Chart.pptx">22. Organizational Change Management Strategy</a></p>
                                <p><a href="References\Project Charter.docx">23. Project Charter</a></p>
                                <p><a href="References\Project Governance.pptx">24. Project Governance Procedures</a></p>
                                <p><a href="References\Onboarding Guide.docx">25. Project Onboarding Guide</a></p>
                                <p><a href="References\Project Organizational Chart.pptx">26. Project Organizational Chart</a></p>
                                <p><a href="References\Project Status Tracking Sheet.xlsm">27. Project Status Tracking Sheet</a></p>
                                <p><a href="#">28. Resource Plan</a></p>
                                <p><a href="References\Risk Management Plan.docx">29. Risk Management Process</a></p>
                                <p><a href="References\SAP System Landscape Stratergy.docx">30. SAP Systems Landscape Strategy</a></p>
                                <p><a href="References\Scope Management &amp; Change Request Process.docx">31. Scope Management / Change Request Process</a></p>
                                <p><a href="#">32. Security Strategy</a></p>
                                <p><a href="References\Stage Gate Review.pptx">33. Stage Gate Review Dashboard</a></p>
                                <p><a href="References\Stakeholder Identification with Grid Mapping.xlsx">34. Stakeholder Identification and Grid Mapping</a></p>
                                <p><a href="#">35. Support Strategy</a></p>
                                <p><a href="References\Testing Strategy.docx">36. Testing Strategy</a></p>
                                <p><a href="#">37. Transport Management Process</a></p>

                              







































                          </div>
<%--                          <div class="col-xs-6 col-md-4"> 
                                <h3>PMO & Testing</h3>   
                                <p><a href="https://life.oneattune.com/mlink/file/MzQ2MzY" target="_blank">1.  attune PMO Project Governance</a></p> 
                                <p><a href="https://life.oneattune.com/mlink/file/NDE4NTc" target="_blank">2.  attune ABAP development Guidelines</a></p>  
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTA">3.  attune Coding Standards Java</a></p>    
                                <p><a href="https://life.oneattune.com/mlink/file/NDExMTE">4.  attune Coding Standards Microsoft.Net</a></p> 
                          </div> --%>
                          <div class="col-xs-6 col-md-4"> 
                                <%--<h3>Business Transformation</h3>--%>
                                <h4>Guidelines</h4>
                                <p><a href="References\Branded aIR Diagram.pptx">1.  Branded aIR Diagram</a></p> 
                                <p><a href="References\Phase Process Summary.docx">2. Phase Process Summary</a></p> 

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>   
                                <p><a href="References\Onboarding Guide Sample.docx">1.  Onboarding Guide Sample</a></p> 
                                <p><a href="References\Project Organizational Chart Sample.pptx">2.  Project Organizational Chart Sample</a></p> 
                          </div>
                        </div>                        

<%--                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Guidelines</h4>   
                          </div>
                        </div> 

                        <div class="row">
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <h4>Samples</h4>
                          </div>
                        </div> --%>


<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->

</asp:Content>
