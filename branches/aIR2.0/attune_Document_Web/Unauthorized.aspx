﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="Unauthorized.aspx.cs" Inherits="attune_Document_Web.Unauthorized" %>
<!DOCTYPE html>

<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>aIR</title>
    <link rel="icon" href="Images/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap -->
    <link href="Css/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="Css/style_Old.css" rel="stylesheet">
    <link href="Css/global.style.css" rel="stylesheet">
    <link href="Css/fonticons.css" rel="stylesheet">
    <link href="Css/grid.animation.css" rel="stylesheet">
    <link href="Css/slidebars.min.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    
    <script src="Scripts/modernizr.js" type="text/javascript"></script>
    <script type="text/javascript">
        function MM_swapImgRestore() { //v3.0
            var i, x, a = document.MM_sr; for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
        }
        function MM_preloadImages() { //v3.0
            var d = document; if (d.images) {
                if (!d.MM_p) d.MM_p = new Array();
                var i, j = d.MM_p.length, a = MM_preloadImages.arguments; for (i = 0; i < a.length; i++)
                    if (a[i].indexOf("#") != 0) { d.MM_p[j] = new Image; d.MM_p[j++].src = a[i]; }
            }
        }

        function MM_findObj(n, d) { //v4.01
            var p, i, x; if (!d) d = document; if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
                d = parent.frames[n.substring(p + 1)].document; n = n.substring(0, p);
            }
            if (!(x = d[n]) && d.all) x = d.all[n]; for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
            for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
            if (!x && d.getElementById) x = d.getElementById(n); return x;
        }

        function MM_swapImage() { //v3.0
            var i, j = 0, x, a = MM_swapImage.arguments; document.MM_sr = new Array; for (i = 0; i < (a.length - 2) ; i += 3)
                if ((x = MM_findObj(a[i])) != null) { document.MM_sr[j++] = x; if (!x.oSrc) x.oSrc = x.src; x.src = a[i + 2]; }
        }


        $(document).ready(function () {


            if ($(window).width() < 1024) {
                $('.pic1 img').on('click', function (e) {
                    $('.pic1').toggleClass("show");

                    if ($('.pic2').hasClass("show2")) {
                        $('.pic2').removeClass("show2");
                    }
                    if ($('.pic3').hasClass("show3")) {
                        $('.pic3').removeClass("show3");
                    }

                });
                $('.pic1 .pic-caption').on('click', function (e) {
                    $('.pic1').toggleClass("show");
                });



                $('.pic2 img').on('click', function (e) {
                    $('.pic2').toggleClass("show2");

                    if ($('.pic1').hasClass("show")) {
                        $('.pic1').removeClass("show");
                    }
                    if ($('.pic3').hasClass("show3")) {
                        $('.pic3').removeClass("show3");
                    }
                });
                $('.pic2 .pic-caption').on('click', function (e) {
                    $('.pic2').toggleClass("show2");
                });



                $('.pic3 img').on('click', function (e) {
                    $('.pic3').toggleClass("show3");

                    if ($('.pic1').hasClass("show")) {
                        $('.pic1').removeClass("show");
                    }
                    if ($('.pic2').hasClass("show2")) {
                        $('.pic2').removeClass("show2");
                    }
                });
                $('.pic3 .pic-caption').on('click', function (e) {
                    $('.pic3').toggleClass("show3");
                });
            }
            else {
                //alert('More than 1024');
            }



        });
	</script>
  </head>

  <body>
  <!-- Header Section-->
  	<section class="header-row">
          <%--<a href="Index.aspx"></a>--%>
		        <div class="logo"><a href="Index.aspx" class="visible-sm tab-menu"><i class="att att-bars sb-toggle-left"></i></a><img src="Images/logo.jpg"></div>
          
       <nav class="" role="navigation"> 
		<div class="header-rightSection">
           
       </div>
       </nav>
        <div>
               <h2 style="padding-left:10px">You are not authorized to access this site 
            </h2>
          </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/leftNav.js"></script>
    <script src="Scripts/slidebars.min.js"></script>
		<script>
		    (function ($) {
		        $(document).ready(function () {
		            
                    
		        });
		    })(jQuery);
		</script>
  </body>

</html>
