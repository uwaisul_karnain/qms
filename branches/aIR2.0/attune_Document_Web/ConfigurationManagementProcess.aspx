﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ConfigurationManagementProcess.aspx.cs" Inherits="attune_Document_Web.MeasurementandAnalysis" %>

<asp:Content ID="measurementAndAnalysisPageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Configuration Management Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The objective of Configuration Management process is to plan, implement, deploy and execute organizational configuration management process, tools and methods based on people lessons learns and  best practices.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The scope of the configuration management process cover configuration management activities in attune.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>CMP - </strong>Configuration Management Plan </p>
                                <p><strong>AI - </strong>Action Item</p>
                                <p><strong>CM   - </strong>Configuration Management</p>                                
                                <p><strong>IC - </strong>Implementation Consultant</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>CA - </strong>Configuration Auditor</p>
                                <p><strong>CMR - </strong>Configuration Manager</p>
                                <p><strong>PGM   - </strong>Program Manager</p>                                
                                <p><strong>PL - </strong>Project Lead</p>
                          </div>
                          <div class="col-xs-6 col-md-4">  
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>SOW  - </strong>Statement of Work</p>
                                <%--<p><strong>PAD   - </strong>Project Approach Document</p> --%>                               
                                <p><strong>aCMP - </strong>attune Configuration Management Process</p>
                          </div>

                        </div>

                        <div class="data">
                            <h3>Process and Steps</h3>     

                            <p>There are 3 sub process are available in CM process</p>
                                <p class="sub-para">•	Plan Configuration Management</p>
                                <p class="sub-para">•	Manage baselines</p>
                                <p class="sub-para">•	Monitor Configuration Management Changes</p>

                            <p><strong>Configuration Management</strong></p>
                            <p>Plan Configuration Management mainly focus on the prepare project level Configuration Management Plan (CMP), that describes CM activities for the project.</p>
                            <p>Document should create in the beginning of the Blue Print/Requirement gathering Phase following project approval, and then re-visited through the project lifecycle at the beginning of the Design/Realization, Testing, and Integration Phases.</p>

                            <p><strong>Manage baselines</strong></p>
                            <p>If client is not specify the Configuration Management repository, attune source files are maintain in SVN with required labeling and branches. Project documents will be maintain in respective one attune project with revision history. Required document baselines will be maintain as per the defined CMP.</p>

                            <p><strong>Monitor Configuration Management Changes</strong></p>
                            <p>Project lead and project manager is responsible for smooth usage of Configuration Management repository and usage. However there will be regular audit to assure the quality of the usage and practice.</p>
                            <p>Team maintain documents in one attune and source files in SVN (if client not mandate to maintain their own system)</p>
                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="4"><strong>Plan Configuration Management</strong></td>
                                            <td><p>Identify and create Configuration Management procedures.</p> </td>
                                            <td>PM, CMR</td>
                                            <td></td>                                    
                                            <td rowspan="4">SOW, Project Charter, CMP</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Write the CM Plan. </p></td>
                                            <td>CMR</td>
                                            <td>CMP</td>
                                            <%--<td>SOW,  Client Project charter</td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Review the CM Plan by program manager.</p></td>
                                            <td>PGM</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Baseline the CM Plan with the required approval.</p></td>
                                            <td>CMR</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                         <tr>
                                            <td rowspan="2"><strong>Manage baselines</strong></td>
                                            <td><p>Manage project baselines.</p> </td>
                                            <td>CMR</td>
                                            <td></td>                                    
                                            <td rowspan="2">CMP, aCMP</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Conduct project closure backup</p></td>
                                            <td>CMR</td>
                                            <td>One attune backup</td>
                                            <%--<td>SOW,  Client Project charter</td>--%>
                                        </tr>
                                        <tr>
                                            <td rowspan="4"><strong>Monitor Configuration Management Changes</strong></td>
                                            <td><p>Project lead and PM conduct regular checks on usage</p> </td>
                                            <td>PL, PM</td>
                                            <td></td>                                    
                                            <td rowspan="4">CMP, CM Audit reports, Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>CM Auditor conduct the project CM audit </p></td>
                                            <td>CMA</td>
                                            <td>CM audit report</td>
                                            <%--<td>SOW,  Client Project charter</td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Report CM audit findings</p></td>
                                            <td>CMA</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Team track those findings to closure and get the approval</p></td>
                                            <td>PM</td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Project kicked off for execution</p> 
                                            <p>•	Required resources for Project Manager (PM), Configuration Manager (CMR), and Configuration Auditor (CA) are identified with respective roles and responsibilities</p> 
                                            <p>•	PM,CA, CMR, leads and the team members are trained on this process and procedures</p> 
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	CMP prepared, reviewed, approved and maintained</p>
                                            <p>•	Regular CM audits conducted and findings tracked to closure</p> 
                                            <p>•	Manager project baselines and project closure activities</p>  
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Project SOW</p> 
                                            <p>•	Project Charter</p> 
                                            <p>•	Configuration Management process and templates</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Configuration Management Plan (CMP)</p> 
                                            <p>•	Configuration Management audit report</p> 
                                            <p>•	CMP review record</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Number of Audit finding per project</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>Measurement analysis is mandatory for all types of projects.</p> 
                                            <p class="tab-content" style="padding-left: 20px">•	Configuration Management is a mandatory process for any project running in the delivery center</p> 
                                             
                                        </div>
                                    </td>
                                    </tr>       
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="<%--References\Measurements And Analysis Process.pdf--%>">Configuration Management Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>                                                                                        
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>                       
                                    <p><a href="References\attune Project Configuration Management Plan.docx">1. CM Plan Template</a></p>                    
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>                                                     
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>                    
                        
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>
