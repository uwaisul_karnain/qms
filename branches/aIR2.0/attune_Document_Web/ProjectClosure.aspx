﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ProjectClosure.aspx.cs" Inherits="attune_Document_Web.ProjectClosure" %>


<asp:Content ID="projectClosurePageContent" ContentPlaceHolderID="MainContentPageSection" runat ="server">

          <!-- Page Content -->
<%--        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">--%>
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Closure  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The Project Closure is the project review process consisting of activities performed by a project team at the end of the project's life cycle or at the end of a significant release/ rollout. This will enable a smooth handover and project / release closure as well as to gather information on what worked well and what did not, so that future projects can benefit from the learning. When the closure output is of direct relevance to the customer, this activity may be conducted as a part of the project contract.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>Activities given below are conducted to ensure a smooth handover and project / release closure.</p>
                            <p class="tab-content" style="padding-left: 40px">•	Conduct project closure meeting with stakeholders</p>
                            <p class="tab-content" style="padding-left: 40px">•	After action review conducted </p>
                            <p class="tab-content" style="padding-left: 40px">•	All project documentations are updated and uploaded to the respective document repository and one attune. </p>
                            <p class="tab-content" style="padding-left: 40px">•	Inform respective stakeholders and groups</p>
                            <p class="tab-content" style="padding-left: 40px">•	PM / PL completes project performance review records</p>
                            <p class="tab-content" style="padding-left: 40px">•	Knowledge transfer to support team </p>
                            <p class="tab-content" style="padding-left: 40px">•	Support acceptance criteria signoff</p>
                            <p class="tab-content" style="padding-left: 40px">•	Finalize project commercials</p>
                            <p class="tab-content" style="padding-left: 40px">•	Close PSA project assignments</p>
                        </div>

                        <h3>Project Termination</h3>
                        <div class="data">
                            <p>Project termination may occur for one or multiple reasons. Project leadership will make the decision considering the facts and impact to the company and business.</p>
                            <p>When the project is terminated, the project team should execute appropriate project closure activities. </p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>TL  - </strong>Technical Lead</p>                                
                                <p><strong>PL - </strong>Project Lead</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>UAT  - </strong>User Acceptance Testing</p>                                
                          </div>
                          <div class="col-xs-6 col-md-4">  
                                <%--<p><strong>PAD - </strong>Project Approach Document</p>--%>
                                <p><strong>MPP  - </strong>Microsoft Project Plan</p>
                          </div>

                        </div>

                        <div class="data">
                            <h3>Process and Steps</h3>     

                            <h4>1.	Process Overview</h4>

                            <p>Project closure should be ideally completed within a two to four week window. However, the duration could vary based on the nature of the project. Project closure is the process that directly impacts the attune support team and ensures smooth project transition.</p>


                            <h4>2.	Projects Finishes According to the SOW Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="2"><strong>Plan </strong></td>
                                            <td><p>With the project Go live team starts working on the project closure activities.</p></td>
                                            <td>TL/PM</td>
                                            <td></td>                                    
                                            <td rowspan="2">SOW , <%--PAD, --%>Project charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Internal Project closure meeting and plan for the project closure activities.</p>
                                                <p>Responsibilities / tasks are allocated to the team.</p></td>                                            <td><p>PM, PL, TL</p></td>
                                            <td><p>MPP</p></td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="8"><strong>Conduct </strong></td>
                                            <td><p>Project closure documentation</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Discuss the elements, which need to be included to the project closure </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify responsibilities and ownership </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify time frame for the documentation </p> </td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>Project closure template</td>                                    
                                            <td rowspan="8"></td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Review closure documentation</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Team and Leadership review and feedbacks</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Incorporate review feedbacks</p></td>
                                            <td>Team</td>
                                            <td>Project closure template</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Create Reusable components and store in the respective locations.</p>
                                                <p>Communicate the same to Knowledge Management / Reuse groups</p>
                                                <p>Make reuse friendly tags and key words include to ONEattune </p></td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>Reusable components</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Identify internal and external parties that should communicate the project closure. Example : HR, Finance, etc </p></td>
                                            <td>PM, PL</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Inform project closure to attune Knowledge management team and invite them for the closure meeting</p></td>
                                            <td>PM, PL</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Complete project team feedbacks to the leadership</p></td>
                                            <td>PM, PL, HR</td>
                                            <td>Feedback report</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Update skill records in the attune central resource skill tracking sheet</p></td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>attune skill tracking sheet</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Project Closure Announcement – communicate all relevant stakeholders  </p>
                                                <p>Project team, Finance, HR, Global resource Canter, etc</p></td>
                                            <td>PM, PL</td>
                                            <td>email</td>
                                            <%--<td></td>--%>
                                        </tr>




                                    </table>
                                </div>
                            </div>


                            <h4>3.	Project is terminated early – Steps</h4>
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Activity</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="2"><strong>Plan </strong></td>
                                            <td><p>Project closure meeting with customer and plan the project closure activities.</p>
                                                <p>Project closure documentation</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Discuss the elements, which need to be included to the project closure </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify responsibilities and ownership</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify time frame for the documentation</p></td>
                                            <td>PM, Customer, SVP/td>
                                            <td>Closure plan</td>                                    
                                            <td rowspan="2">SOW , <%--PAD, --%>Project charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Internal Project closure meeting and plan for the project closure activities.</p>
                                                <p>Responsibilities / tasks are allocated to the team.</p></td>
                                            <td><p>PM, PL, TL</p></td>
                                            <td><p>MPP</p></td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="13"><strong>Conduct </strong></td>
                                            <td><p>Project lessons learned and case study document preparation with respect to the lesson learnt meeting outcome. </p> </td>
                                            <td>Team</td>
                                            <td>Project case study</td>                                    
                                            <td rowspan="13"></td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Complete required customer documentation and update in customer repository</p></td>
                                            <td>Team, Customer  </td>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Create Reusable components and store in the respective locations.</p>
                                            <p>Communicate the same to Knowledge Management / Reuse groups</p>
                                            <p>Make reuse friendly tags and key words in ONEattune </p></td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>Reusable components</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Create project document, source and etc archive ( one attune and or SVN)</p></td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>One attune folder</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Roll-off resource according to the resource roll-off plan. </p></td>
                                            <td>PM, SVP</td>
                                            <td>PSA</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Close assignments in PSA</p></td>
                                            <td>SVP</td>
                                            <td>PSA</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Inform project closure to attune Practice Leads and invite them to closure meeting</p></td>
                                            <td>PM, PL</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Complete project team performance reviews to the leadership and the team</p></td>
                                            <td>PM, PL, HR, team</td>
                                            <td>Feedback report</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Update skill records in the attune central resource skill tracking sheet</p></td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>attune skill tracking sheet</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Conduct project closure tasks as per the agreement with the customer.</p></td>
                                            <td>PM, Customer</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare final billing to the customer</p></td>
                                            <td>PM</td>
                                            <td>Invoice</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Close the project in PSA after final invoice is created</p></td>
                                            <td>SVP</td>
                                            <td>PSA</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Project Closure Announcement – communicate all relevant stakeholders  </p>
                                                <p>Project team, Finance, HR, Global resource Canter, etc</p></td>
                                            <td>PM</td>
                                            <td>email</td>
                                            <%--<td></td>--%>
                                        </tr>




                                    </table>
                                </div>
                            </div>


                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Project Go Live / Release completion</p>
                                            <p>•	Contract (SOW) ends </p>
                                            <p>•	The project termination has occurred</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Project closure meeting conducted and Minutes of Meetings are published and respective action items are tracked to closure</p>
                                            <p>•	After action review conducted and lessons learned document signed-off by  - Practice leads and HR </p>
                                            <p>•	All project documentation is updated and uploaded to the respective document repository and one attune signed-off by  - Practice leads </p>
                                            <p>•	Reuse components identified and communicated to Practice Leads, make re-useable </p>
                                            <p>•	Inform respective stakeholders and groups</p>
                                            <p>•	PM / PL complete project performance review records – signed- off by HR</p>
                                            <p>•	Finalize project commercials – finance approval</p>
                                            <p>•	Close PSA project assignments</p>
                                            <p>•	Closure announcement</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Project contract (SOW)</p>
                                            <p>•	Project Charter<%-- / Project approach document--%></p>
                                            <p>•	Records of meetings with client/ correspondence</p>
                                            <p>•	Go live signoff </p>
                                            <p>•	Project termination note from the leadership team</p>
                                            <p>•	Support acceptance criteria</p>
                                            <p>•	Permanent long-term support plan</p>

                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Lessons learned document</p>
                                            <p>•	Project closure meeting PPT and Minutes of Meetings</p>
                                            <p>•	Updated Project Status Tracking Sheet</p>
                                            <p>•	Updated Reuse list in one attune</p>
                                            <p>•	Updated Training materials</p>
                                            <p>•	Updated Knowledge Management repository in one attune </p>
                                            <p>•	Project performance review records</p>
                                            <p>•	Updated Skills in PSA</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort spent on project closure activities</p>
                                            <p>•	Number of reuse components </p>
                                            <p>•	Schedule and Effort Variance on closure activities</p>
                                            <p>•	% of approved project generated documents on ONEattune ( 100% target )</p>

                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td colspan="2">
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>Projects closure is mandatory for all types of projects.</p>
                                            <p>•	In addition if a project runs for a longer time period, it is recommended to conduct a lessons learned at least every 6 months. </p>
 
                                        </div>
                                    </td>
                                    </tr>  
                                    <tr class="loop">
                                        <td colspan="2">
                                            <div class="data">
                                                <div class="tab-content" style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px">
                                                       <a class="btn mybutton" href="References\Project Closure Process.pdf">Download Project Closure Process PDF &raquo;</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>                                                                                              
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>                                   
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>       
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>    
<%--                    </div>
                </div>
            </div>
        </div>--%>
        <!-- /#page-content-wrapper -->


</asp:Content>



