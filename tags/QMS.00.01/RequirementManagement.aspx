﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RequirementManagement.aspx.cs" Inherits="RequirementManagement" %>

<asp:Content ID="requirementManagementPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Requirement Management Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>Proposal preparation is the first step in any project. The propose solution is the presentation prepared to forms the basis for negotiation and contracting, and the accepted proposal / contract forms the “requirements” for the project work.</p>
                            <p>Requirements form the basis of project planning and management and all the activities performed in a software project. For successful project execution, it is necessary to ensure that the requirements are managed effectively and that all project activities are based on agreed and signed-off requirements.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The RM process ensures that proposals and contracts are based on gathered and approved requirements and that these requirements form the basis for all planning and execution activities of the project. The RM process uses this PAD, along with the Software Engineering Lifecycle process and the Project's Process, to manage requirements and maintain bi-directional traceability throughout project execution.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>VP  - </strong>Vice Precedent</p>
                                <p><strong>GM  - </strong>Group Manager</p>
                                <p><strong>PM  - </strong>Project Manager</p>                                
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>BA - </strong>Business Anasyst</p>
                          </div>
                          <div class="col-xs-6 col-md-4">                                 
                                <p><strong>RM - </strong>Requirement Management</p>
                                <p><strong>RCS  - </strong>Requirement Clarification Sheet</p>
                                <p><strong>RTM  - </strong>Requirement Traceability matrix</p>  
                                <p><strong>SRS - </strong>System Requirements Specification</p>
                                <p><strong>CR - </strong>Change Request</p>                            
                          </div>
                          <div class="col-xs-6 col-md-4">                                 
                                <p><strong>SOW  - </strong>Statement of Work</p>
                                <p><strong>PAD  - </strong>Project Approach Document</p>
                                <p><strong>aPTR  - </strong>attune Process Tailoring Process</p>
                          </div>
                        </div>



                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>attune executes software developments, enhancements and software projects for its clients. Clients could be external to attune or internal. Enhancement and maintenance projects could begin at the end of a development project.</p>
                            <p>The VP Services has the overall responsibility for identifying opportunities for procuring projects. The existing client base, industry and client contacts, innovation team product ideas, public tenders floated are all possible sources for projects.</p>
                            <p>After establishing contact with a potential client, the VP-services gathers adequate information about the scope of the work prepares an estimate and prepares a solution proposal.  Project solution proposal is reviewed by an external person (Who did not get involved in solution proposal creation process). The reviewed and approved solution proposal is email to the client. This cycle might get repeated couple of time before client gives the go-ahead in a mail or document. This email/document is taken as an acceptance of the project proposal by the client. The accepted project proposal is treated as the “requirements” for the project.</p>
                            <p>Once the contract is signed, project planning and execution are initiated. RM related activities for projects are handled through this process. These include maintaining requirements in a database and maintaining traceability between requirements and all artifacts. Changes to requirements are incorporated in a systematic way and the impact of these changes on project commitments is evaluated and reviewed before the change to the requirements is implemented.</p>
                            <p>The requirements analysis procedure in the Software Engineering Lifecycle process may be used to gather and analyze requirements. The RM process uses the PAD, along with the Software Engineering Lifecycle process and the Project's Process, to manage requirements and maintain bi-directional traceability throughout project execution.</p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="8"><strong>Prepare</strong></td>
                                            <td><p>Initiate the process for obtaining the “requirements” that will form the basis of the proposal/contract. </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Business product clarification tracking sheet is maintained to capture unclear requirements.</p>
                                                <p>(Template : Requirement Clarification Tracking Sheet)</p></td>
                                            <td>VP-Services, GM</td>
                                            <td>RCTS</td>                                    
                                            <td>SOW, Project Charter, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Identify “requirements providers” from the customer organization to provide information needed for defining the scope of the proposed project.</p> 
                                                <p>Requirements providers are selected based on criteria’s  which they;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Have the authority to decide on the requirements</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Have the information needed to detail the requirements, or have adequate access to persons who have the information</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Should adequately represent the stakeholders (business and technical) of the proposed system.</p>
                                                <p>Stakeholders may include various impacted client departments/ divisions, and government and statutory bodies)</p></td>
                                            <td rowspan="3">GM, PM</td>
                                            <td rowspan="2">Communication Mails, Minutes of Meeting</td>
                                            <td rowspan="3">Project Charter, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Identify a person who will act as a liaison for obtaining requirements and for the acceptance of requirements. </p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Obtain details of the scope of the proposed project from the requirements providers.  </p></td>
                                            <%--<td></td>--%>
                                            <td>RCTS</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Discusses the project requirements with the client liaison and obtains agreement after incorporating the comments. These requirements are baseline.</p></td>
                                            <td>PM, Client Liaison</td>
                                            <td>Mails, Meeting Minutes</td>
                                            <td>RCTS</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare an estimates and identify risks for the work identified using a suitable estimation technique.
                                                <p class="tab-content" style="padding-left: 20px">•	Size and Effort Estimates
                                                <p class="tab-content" style="padding-left: 20px">•	Estimate Cost related for the effort and other components
                                                <p class="tab-content" style="padding-left: 20px">•	Document the potential Risks</p></td>
                                            <td>PM, GM</td>
                                            <td>Estimates</td>
                                            <td rowspan="2"></td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepares a project proposal using all the above documents and send to client for approval.</p> 
                                                <p>Negotiations may take place with the client within the scope of the approved budget.</p>
                                                <p>(Template: Solution Proposal, Presentation)</p></td>
                                            <td>MG</td>
                                            <td>Solution Proposal, Mail</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Once the accepted mail received, the proposal is baseline</p></td>
                                            <td>Client, PM</td>
                                            <td>Acceptance Mail</td>
                                            <td>Solution Proposal</td>
                                        </tr>

                                        <tr>
                                            <td rowspan="4"><strong>Plan</strong></td>
                                            <td><p>The accepted solution proposal/contract that contains the “requirements” for the project, becomes the initial requirements and form the starting point for project planning and execution.</p></td>
                                            <td>PM, BA</td>
                                            <td></td>                                    
                                            <td rowspan="2">Accepted Solution Proposal, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The initial requirements are documented in a requirements database, RTM for tracking.</p>
                                                <p>All source document related to requirements of the project should check in to this data base.</p> 
                                                <p>Requirements are traced by the RTM  which;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Holds each low-level requirement along with the information needed for traceability, such as the requirement's unique identifier, its source, and change history.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Holds information on derived work-products with bi-directional traceability information.</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Supports utilities that enable understanding both horizontal and vertical relationships through the traceability data recorded in it.</p>  
                                                <p class="tab-content" style="padding-left: 20px">•	Indicates the whether the requirements are satisfied through project plans and activities like design, code, testing and bug-fixing</p> 
                                                <p>(Template : Requirement Traceability Matrix)</p></td>
                                            <td>PM, TL, BA</td>
                                            <td>Requirement Database, RTM</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare detailed system requirement specifications and use cases need to be developed using available sources of Information.</p> 
                                                <p>(Template: System Requirement Specification)</p></td>
                                            <td>BA</td>
                                            <td>SRS</td>
                                            <td>RTM, RCTS</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Addresses the Project Requirement Management details, activities and the procedure in the PAD (Requirement Management and Change Management sections).</p>
                                                <p>(Template : Project Approach Document)</p></td>
                                            <td>PM</td>
                                            <td>PAD</td>
                                            <td rowspan="3">aPTR</td>
                                        </tr>

                                        <tr>
                                            <td rowspan="2"><strong>Manage</strong></td>
                                            <td><p>Manage CRs.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Each change requirement is reviewed to ensure that it is Clearly stated, Complete, Consistent with other requirements, Testable and Feasible to implement </p>
                                                <p class="tab-content" style="padding-left: 20px">•	Impact Analysis in CR form is done according to the CM Plan.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	CRs are tracked by the CR Tracking Register in PSTS</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Maintain RTM for bi-directional traceability</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Appropriate negotiations held when project commitments are required for changed, and revise commitments are documented.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Decides the acceptance of a CR and determine the build release number for CR.</p></td>
                                            <td>PM, BA</td>
                                            <td>CR Form(s), PSTS, RTM, RCTS</td>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Review the RTM and CR Logs to identify inconsistencies between the requirements and the related plans and work products.</p>
                                                <p>In case inconsistencies are detected, corrective actions are identified as part action items</p></td>
                                            <td>PM</td>
                                            <td>PSTS</td>
                                            <%--<td></td>--%>
                                        </tr>

                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>Any one or more of the following</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Request for a proposal</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Client send a design specification</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Innovation/Marketing Team come up with an idea</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Project Kickoff Plan / Project Charter is prepared</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approval for PAD</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>Any of the following happens:</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Proposal is dropped</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approval of the PAD</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Project Closure</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Request for Proposal/Design specification or innovation/marketing team Work-product to be reviewed (Supporting material for review: specifications, standards, review checklists, templates, etc.)</p>
                                            <p>•	Statement of Work</p>
                                            <p>•	Project Charter / Kickoff Plan</p>
                                            <p>•	Project Approach Document</p>
                                            <p>•	Proposed Solution Presentation</p>
                                            <p>•	Change to Requirement</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Proposed Solution ( if available)</p>
                                            <p>•	Acceptance email from the client</p>
                                            <p>•	Requirement Clarification Tracking Sheet</p>
                                            <p>•	Records of discussions / correspondence with client</p>
                                            <p>•	Project Approach Document</p>
                                            <p>•	Requirement Database ( SRS, Product Backlog, RTM, Functional Specifications, CRs and Impact Analysis, Use case Template, Client Design Database)</p>
                                            <p>•	Estimation Sheets</p>
                                            <p>•	Review Records</p>
                                            <p>•	Project Status Tracking Sheet and Project Status Tracking Report</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort for Requirement Management</p>
                                            <p>•	Effort for Reviews</p>
                                            <p>•	Number of review comments </p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The method for estimation may be tailored to the situation as per the Guidelines for Estimation.</p>

                                            <p>•	In many cases, acceptance of a proposal or placement of an order by the client may be treated as a contract</p>

                                            <p>•	In many cases, acceptance of a proposal or placement of an order by the client may be treated as a contract</p>

                                            <p>•	RM status could be part of project status tracking report(s)</p> 
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <h2>Document Templates</h2>            
                        <p><a href="References\attune Requirement Clarification Tracking Sheet.xls">1. attune Requirement Clarification Tracking Sheet</a></p>   
                        <p><a href="References\attune RTM.xlsx">2. attune Requirement Traceability Matrix</a></p>       
                        <p><a href="References\attune System Requirement Specification.docx">3. attune System Requirement Specification</a></p>
                        <p><a href="References\attune Product Backlog.xls">4. attune Product Backlog</a></p>
                        <p><a href="References\attune Change Request.docx">5. attune Change Request</a></p>
                        <p><a href="References\attune Project Status Tracking Sheet.xlsx">6. attune Project Status Tracking Sheet</a></p>
                        <p><a href="References\attune Project Effort Estimation Sheet - 3 Point Estimation.xls">7. attune Project Effort Estimation Sheet - 3 Point Estimation.xls</a></p>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>

