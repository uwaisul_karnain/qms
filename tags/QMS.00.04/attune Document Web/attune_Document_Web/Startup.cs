﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(attune_Document_Web.Startup))]
namespace attune_Document_Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
