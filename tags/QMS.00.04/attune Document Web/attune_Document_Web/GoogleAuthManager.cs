﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Helpers;
//using System.Web.Helpers;

namespace attune_Document_Web
{
    public class GoogleAuthManager
    {
        private static GoogleAuthManager _instance;
        private static readonly object Lock = new object();
        private bool _isConfigured;
        private GoogleAuthRequest _request;

        private GoogleAuthManager()
        {
        }

        public static GoogleAuthManager Instance
        {
            get
            {
                lock (Lock)
                {
                    return _instance ?? (_instance = new GoogleAuthManager());
                }
            }
        }

        public void Configure(GoogleAuthRequest authRequest)
        {
            _isConfigured = true;
            _request = authRequest;
        }

        public void ValidateRequest(HttpContext context)
        {
            if (!_isConfigured) throw new ArgumentNullException("Configuration is null");

            if (string.IsNullOrEmpty(context.Request.QueryString["code"]))
                context.Response.Redirect(_request.FormattedAuthUrl());
            else if (!string.IsNullOrEmpty(context.Request.QueryString["code"]))
            {
                //Create a post request for to request an access token
                var client = new WebClient();

                // creates the post data for the POST request
                var values =
                    new NameValueCollection
                    {
                        {"client_id", _request.ClientId},
                        {"client_secret", _request.Secret},
                        {"redirect_uri", _request.RedirectUrl},
                        {"code", context.Request.QueryString["code"]},
                        {"grant_type", "authorization_code"}
                    };

                // Post Request
                var response = client.UploadValues(_request.TokenUrl, values);
                var result = Encoding.UTF8.GetString(response);

                
                var s = Json.Decode(result);
                var aCookie = new HttpCookie("gAuthUser");
                aCookie.Values["token"] = result;
                aCookie.Expires = DateTime.Now.AddSeconds(s.expires_in);
                context.Response.Cookies.Add(aCookie);
                context.Response.Redirect(_request.RedirectUrl);
            }
        }

        public bool IsAuthenticated(HttpContext context)
        {
            return context.Request.Cookies["gAuthUser"] != null;
        }
    }


    public abstract class AuthBase
    {
        public string RedirectUrl { get; set; }
        public string ClientId { get; set; }
        public string Secret { get; set; }
        public abstract string AuthUrl { get; }
        public abstract string TokenUrl { get; }
        public string Token { get; set; }
        public string ResponseType { get; set; }
        public string Scope { get; set; }
        public string AccessType { get; set; }
        public abstract string FormattedAuthUrl();
    }

    public sealed class GoogleAuthRequest : AuthBase
    {
        public GoogleAuthRequest()
        {
            AccessType = "online";
            ResponseType = "code";
        }

        public override string AuthUrl
        {
            get { return "https://accounts.google.com/o/oauth2/auth"; }
        }

        public override string TokenUrl
        {
            get { return "https://accounts.google.com/o/oauth2/token"; }
        }

        public override string FormattedAuthUrl()
        {
            return
                string.Format(
                    "{0}?client_id={1}&redirect_uri={2}&scope={3}&response_type={4}&access_type={5}",
                    AuthUrl, ClientId, RedirectUrl, Scope, ResponseType, AccessType);
        }
    }
}