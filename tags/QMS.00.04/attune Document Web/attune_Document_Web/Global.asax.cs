﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;


namespace attune_Document_Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GoogleAuthManager.Instance.Configure(new GoogleAuthRequest()
            {
                //ClientId =
                //    "85538684700-62ci967tevtt9euah8i770jtmh8kn1tf.apps.googleusercontent.com",
                //Secret = "SiXzVhfR8a1ewGQaXuahGt7x",
                //RedirectUrl = "http://localhost:51988/Default.aspx",
                ClientId =
                    "726683419182-qf3dq3m4998872s98gp93qvclr1m948b.apps.googleusercontent.com",
                Secret = "jgQNNlye3fP55Pi5kEMa2ihW",
                RedirectUrl = "https://air.go-attune.com/Index.aspx",


                Scope = "email+profile"
            });
        }

    }
}
