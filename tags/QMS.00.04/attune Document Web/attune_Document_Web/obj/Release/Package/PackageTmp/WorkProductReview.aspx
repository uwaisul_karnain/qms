﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="WorkProductReview.aspx.cs" Inherits="attune_Document_Web.WorkProductReview" %>

<asp:Content ID="workProductReviewPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Work  Product  Review  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The objective of Work Product Review process is to ensure that work-products / outputs of various processes in software projects meet the specifications and relevant standards.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The scope of the work-product reviews depends upon the item that is to be reviewed. The areas which are reviewed are as below,</p>
                            <p class="tab-content" style="padding-left: 20px">•	Project Charter</p>
                            <p class="tab-content" style="padding-left: 20px">•	Project Approach Document and other project planning documents (like Risk Management Strategy, CM Plan, QA Plan)</p>
                            <p class="tab-content" style="padding-left: 20px">•	Work-products created while executing the Software Engineering Lifecycle process (e.g., System Requirement Specification documents, Architecture & Detail Design Documents, Unit Specifications, User and Operations Manual, Acceptance Test Plans,  Source Code)</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>CM  - </strong>Configuration Management</p>
                                <p><strong>CMR  - </strong>Configuration Manager</p>
                                <p><strong>QA  - </strong>Quality Assurance</p>  
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PL  - </strong>Project Lead</p>
                                <p><strong>TL - </strong>Technical Lead</p>                                                                                         
                          </div>
                          <div class="col-xs-6 col-md-4">                                 
                                <p><strong>SOW  - </strong>Statement of Work</p>
                                <p><strong>PAD  - </strong>Project Approach Document</p>
                                <p><strong>aPTR - </strong>attune Process Tailoring Repository</p>                             
                          </div>
                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>A work-product review is a process where individuals / groups other than the author of a work-product, examine a work-product and identify defects and suggest improvements.</p>
                            <p>Defects in a work-product are deficiencies and discrepancies with respect to pre-defined requirements and standards and implied requirements. Testing is one way of identifying defects. However, many items cannot be “tested” in the conventional sense. Moreover, in many cases, it is not possible to perform complete testing and many defects can be identified through work-product reviews with lesser effort. The Project Approach Document will identify the work products to be reviewed for the project under review section. Generally all the configuration items must be reviewed.</p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td rowspan="3"><strong>Plan</strong></td>
                                            <td><p>Plan the reviews by;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify the work products need to be reviewed</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Identify the review methods to be used in each work product.</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify resources for review</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Identify the frequency for reviews</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Schedule</p> 
                                                <p>Consider capability of project resources, schedules and complexity of technical and business domains dealt with when planning for reviews</p></td>
                                            <td>PM</td>
                                            <td>PAD, Schedule</td>                                    
                                            <td>SOW, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Gather information required for the review. These are;</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The work-product(s)</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The supporting documents relevant to the Work Product reviewed</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The specifications of the work-product(s)</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The relevant standards / templates ( if available)</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Checklists (if available)</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Guidelines for Reviews (if available)</p></td>
                                            <td>Peer, Author</td>
                                            <td></td>
                                            <td rowspan="2">PAD, Schedule</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The author also fixes the date, time and venue for the review meeting by issuing an outlook meeting request. </p></td>
                                            <td>Author, Peer</td>
                                            <td>Meeting Request</td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="2"><strong>Conduct Review</strong></td>
                                            <td><p>The review team members perform a desk check on the work-product to identify potential issues.</p></td>
                                            <td>Peer/Review Team, TL</td>
                                            <td>work-products</td>                                    
                                            <td rowspan="4">aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>TThe Lead / Review Team and the author meet to discuss the issues identified by the reviewers.</p>
                                                <p>Issues that need to be fixed are documented in the peer review form and a record is updated to the Review log of the particular review details including the review details along with the dates.</p>
                                                <p>(Templates : attune Peer Review Form, attune Review Log)</p></td>
                                            <td>Peer/Review Team, TL, Author</td>
                                            <td>Review Comments in Peer Review  Form, Review Log Records</td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="2"><strong>Conduct Review</strong></td>
                                            <td><p>The author uses the issues documented in the review form to make changes/corrections in the work-product. After the author completes all the changes, he / she informs the Reviewer / Review Team Leader.</p></td>
                                            <td>Author</td>
                                            <td>Issues Fixed Work Product</td>                                    
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The Review Leader checks the closure of all the issues in Review Form and marks the status of the particular Review Record as “Closed” in the Review Log.</p></td>
                                            <td>PM, TL, Peer/Team</td>
                                            <td>Review Closure Record in Review Log</td>
                                            <%--<td></td>--%>
                                        </tr>

                                    </tbody>


                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>

                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>When a work-product listed in the PAD review section is “complete” according to the author</p>
                                            <p class="tab-content" style="padding-left: 20px">•	PAD review section “complete” according to the author</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Required resources for Project Manager (PM), Configuration Manager (CMR), and Configuration Auditor (CA) are identified with respective roles and responsibilities</p>
                                            <p class="tab-content" style="padding-left: 20px">•	PM, CMR, TL and the team members are trained on this process and procedures.</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Closure and verification of all issues in the Review Records</p>
                                            <p>•	Closure of review record in Review Log</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Work-product to be reviewed</p>
                                            <p>•	Supporting material for review– specifications, standards, review checklists, templates, etc.</p>                                            
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Review Records and Comments in Review Form</p>
                                            <p>•	Review Record in Review Log</p>
                                            <p>•	Modified Work-product</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort for Work Product Peer Reviews</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The composition of the review team for a work-product can be tailored as per the Guidelines for Review Team Composition. The mandatory reviewers must participate in the review. The PM can evaluate and tailor the participation of the optional reviewers.</p>
                                            <p>•	If some review team members are traveling and the some of the review team members cannot be physically present in the review meeting, the absent review team members may participate by sending their review comments in advance and participating in the meeting through telephone or Internet.</p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\attune Peer Review Form.xlsx">1. attune Peer Review Form</a></p>
                                    <p><a href="References\attune Project Review Log.xlsx">2. attune Project Review Log</a></p>                                                
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>       
                                    <p><a href="References\attune Coding Standards Guide.docx">1. <strong>Microsoft.Net - </strong> Coding Standards Guide</a></p>       
                                    <p><a href="References\attune Jquery.pptx">2. <strong>Microsoft.Net - </strong>Jquery.pptx</a></p>                                                    
                                    <p><a href="References\attune NET Portal Development Framework.docx">3. <strong>Microsoft.Net - </strong>.NET Portal Development Framework</a></p>
                                    <p><a href="References\attune Coding Standards Java.doc">4. <strong>Java - </strong>Java Coding Standards</a></p>
                                    <p><a href="References\attune Development Standard Document APEX.doc">5. <strong>Force.com - </strong>Development Standard Document APEX</a></p>                                                                                                                               
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>      
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>









