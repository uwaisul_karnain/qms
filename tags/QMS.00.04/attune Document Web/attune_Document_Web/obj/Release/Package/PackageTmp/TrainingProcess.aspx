﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="TrainingProcess.aspx.cs" Inherits="attune_Document_Web.TrainingProcess" %>


<asp:Content ID="trainingProcessPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Training  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>attune will create an organizational culture with core values for all employees to become competent with technical, leadership & soft skills in order to become globally competitive in the fashion and lifestyle industry.</p>
                            <p class="tab-content" style="padding-left: 20px">•	Training plan in place at the start of financial year and it is evaluated every 3 months in order to assess progress and alignment
                            <p class="tab-content" style="padding-left: 20px">•	Training financial budget to cover planned training expenditure
                            <p class="tab-content" style="padding-left: 20px">•	Employees fully understand their role and expected performance standard through having accurate job descriptions, annual appraisal feedbacks, business needs and individual interests</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The training process covers professional development and training opportunity for all employees within attune. The process continues leaning interventions to develop a core of well- trained individuals whose performance will enhance the company’s abilities to perform at a level that is consistent with the growth and profitability goals and objective.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>VP - </strong>Vice President</p>
                                <p><strong>GM  - </strong>Group Manager</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>PM - </strong>Project Manager</p>
                              
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>RM - </strong>Resource Manager</p>
                                <p><strong>IDP - </strong>Individual Development Plan</p>                        
                          </div>
                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>      
                                                      
<%--                            </div>
                                <img src="images/Training.jpg" data-width="625" data-height="250" />
                            <div>--%>

                            <p>Training needs are generated from four main sources:</p>
                            <p class="tab-content" style="padding-left: 20px">•	Immediate technical/ domain based on project needs</p>
                            <p class="tab-content" style="padding-left: 20px">•	Role based needs depending on the role an individual plays</p>
                            <p class="tab-content" style="padding-left: 20px">•	Long-term employee needs to enable the employee to take up higher responsibilities</p>
                            <p class="tab-content" style="padding-left: 20px">•	Long-term organization needs based on organization’s future thrust areas and growth plans</p>

                            <p>A high level Annual Training Plan is prepared by the RM (Region) based on the various inputs received from GMs/PMs/PLs. The purpose of the Annual Training Plan is to understand the volume of training and allocate appropriate budgets and resources.</p>
                            <p>Based on the immediate project needs and the Annual Training Plan, the RM prepares a Training Calendar and publishes it for the organization. The schedule is updated every month to reflect the planned training courses for the next three months.</p>

                            <p>Refer: <a href="References\attune Training Plan 2015.docx"> attune Training Plan </a> and <a href="References\attune Training Policy.doc"> attune Training Policy </a> for further details</p>
                            <h4>Steps</h4>  
                                                  
                            <div class="table-data-sep">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <td rowspan="4">Identify</td>
                                        <td>Identify Learning and Development Requirements as per Business needs.</td>
                                        <td>VP Services</td>
                                        <td rowspan="4">IDP</td>                                    
                                        <td rowspan="4">Training Request Form</td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Conduct competency and Skill Gap Review</td>
                                        <td>PM</td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Collect individual Training Request Forms</td>
                                        <td>RM (Region), Employee</td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td><p>Prioritize Training base on;</p>
                                            <p class="tab-content" style="padding-left: 20px">•	The urgency of the Training (In relation to the business requirement or Job Role)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	The Number of employees requiring the Training</p></td>
                                        <td>RM (Region)</td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                        <td rowspan="3">Plan</td>
                                        <td>Identify the appropriate Trainers, Training Providers by an evaluating the Trainers and preparing the curriculum.</td>
                                        <td>RM (Region)</td>
                                        <td>Trainer Curriculum</td>                                    
                                        <td></td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Identify/Nominate employees for the training</td>
                                        <td>GM, PM, RM(Region)</td>
                                        <td>IDP</td>
                                        <td></td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Prepare Annual Training Calendar</td>
                                        <td rowspan="3">RM(Region)</td>
                                        <td>Training Calendar</td>
                                        <td>IDP</td>
                                        </tr>

                                        <tr>
                                        <td rowspan="3">Execute</td>
                                        <td>Make arrangements to Finalize the Training Program with Trainor and participants.</td>
                                        <%--<td></td>--%>
                                        <td rowspan="2">Mail</td>                                    
                                        <td rowspan="3">Training Calendar</td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td><p>Send the colander note with;</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Details of Program (Logistics, Time, Date, Venue, Dress Code)</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Objectives of the Program</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Types of Evaluation and Feedback</p></td>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td><p>actual training program takes place</p>

                                            <p>A Training Intervention could take the following forms:</p>
                                            <p class="tab-content" style="padding-left: 20px">•	On the Job Training</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Workshops</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Conferences</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Mentoring</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Coaching</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Industry Visits</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Job Rotation</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Online / E Learning</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Literature</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Work Assignments</p></td>
                                        <td>Trainer</td>
                                        <td>Training Materials</td>
                                        <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                        <td rowspan="2">Evaluate</td>
                                        <td>Feedback Form sent to all participants after the completion of the training </td>
                                        <td>RM(Region)</td>
                                        <td rowspan="2">Feedback Form</td>                                    
                                        <td></td>
                                        </tr>
                                        <tr class="">
                                        <%--<td></td>--%>
                                        <td>Filled Feedback forms are sent back to RM</td>
                                        <td>Employee</td>
                                        <%--<td></td>--%>
                                        <td></td>
                                        </tr>


                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>Training process starts off at the beginning of every financial year</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approval of the Training Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Training Request</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Skill Gap Found</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Evaluation forms from the trainers</p>
                                            <p>•	Feedback forms from the trainee</p>
                                            <p>•	Financial Year Completed</p>  
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	attune Training Plan </p>
                                            <p>•	attune Training Policy</p>
                                            <p>•	attune Training Calendar</p>
                                            <p>•	Performance appraisal records of employees.</p>
                                            <p>•	Project Skills Tracker</p>
                                            <p>•	Training Request Form</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Individual Development Plan</p>
                                            <p>•	Training Calendar</p>
                                            <p>•	attune Training Plan</p>
                                            <p>•	Project Training Plan</p>
                                            <p>•	Updated Skill tracker</p>
                                            <p>•	Post Training Evaluation Form</p>
                                            <p>•	Feed Back Form</p>
                                            <p>•	Training Attendance Record</p>
                                            <p>•	Minutes of meeting (for Training Status review)</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort for Training</p>
                                            <p>•	Effort for Training Reviews</p>
                                        </div>
                                    </td>
                                    <td>
<%--                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	Process Tailoring record is a mandatory for any project running in the delivery center.</p>
                                        </div>--%>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                

                        <div class="row">
                              <div class="col-xs-6 col-md-4">
                                    <h3>Document Templates</h3>    
                                    <p><a href="References\attune Project Skills Tracker.xlsx">1. attune Project Skills Tracker</a></p>      
                                    <p><a href="References\attune Training Trianing Request form.docx">2. attune Training Request Form</a></p>
                                    <p><a href="References\attune Training Trainer Evaluation Form.xlsx">3. attune Trainer Evaluation Form</a></p>                                                                                       
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Guidelines</h3>                                        
                              </div>
                              <div class="col-xs-6 col-md-4"> 
                                    <h3>Samples</h3>
                              </div>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>

