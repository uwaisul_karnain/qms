﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="BusinessBluePrinting.aspx.cs" Inherits="BluePrinting" %>

<asp:Content ID="bluePrintingPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Business Blueprinting  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Blueprint phase of a SAP implementation project lifecycle.  It covers the activities that need to be performed within this phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>
                            <p class="tab-content" style="padding-left: 20px">•	The methodology imposed by the client</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>BBP - </strong>Business Blueprint Document</p>
                                <p><strong>BP - </strong>Business Process</p>
                                <p><strong>BPO - </strong>Business Process Owner</p>
                                <p><strong>CR - </strong>Change Requests</p>
                                <p><strong>FC - </strong>Functional Consultant</p>
                                <p><strong>FICO - </strong>Finance and Controlling</p>
                                <p><strong>FL - </strong>Functional Lead</p>
                                <p><strong>FS - </strong>Functional Specification</p>
                                <p><strong>MM - </strong>Materials Management</p>
                                <p><strong>PC - </strong>Project Charter</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PGM - </strong>Program Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PMO - </strong>Project Management Office</p>
                                <p><strong>PP - </strong>Planning and Production</p>
                                <p><strong>PPL - </strong>Project Plan</p>
                                <p><strong>PSD - </strong>Professional Services Director</p>
                                <p><strong>QM - </strong>Quality Management</p>
                                <p><strong>RICEFW - </strong>Reports, Interfaces, Conversions, Enhancement, Forms and Workflow</p>
                                <p><strong>RM - </strong>Resource Manager</p>
                                <p><strong>SA - </strong>Solution Architect</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SD - </strong>Sales and Distribution</p>
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>TR - </strong>Transport Request</p>
                                <p><strong>TS - </strong>Technical Specification</p>
                                <p><strong>UAT - </strong>User Acceptance Testing</p>
                                <p><strong>WBS - </strong>Work Breakdown Structure</p>
                                <p><strong>WP - </strong>Work Package</p>
                          </div>
                        </div>
		
                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h2>Process and Steps</h2>    
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived using the SAP ASAP Focus methodology. Within the aIR methodology, the Business Blueprint phase follows the Project Preparation. Within this phase, the activities begin straight after the completion of the Project Kick-Off. This is the phase within which the current business practices are understood and the requirements are elicited. </p>
                            <p>The project team (both attune and customer team members) are divided as per the modules in SAP (SD, MM, PP and FICO). The business processes covered in each area are listed, and meetings are held as per the Business Blueprint Workshop Schedule prepared during the Project Preparation Phase. All relevant stakeholders, business process owners, heads of departments are invited to these workshops in order to explain the current practices and discuss the requirements. The requirements and business practices form the basis to derive the proposed solution which is then explained in detail in the Business Blueprint Document. </p>
                            <p>The Project Status Tracking Sheet is used to document all Issues, Assumptions, Risks, Action Items and Dependencies highlighted and discussed during this phase, together with the progress of the phase.</p>
                            <p>The Business Blueprint Document that is created in this phase forms the basis for the subsequent activities to be completed in the Build phase in Realization. </p>


                            <h4>1.	Steps</h4>
                                     
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="7">Business Blueprint Execution</td>
                                            <td><p><strong>1.	Conduct Workshops</strong></p>
                                                <p>The sessions are held to review the current business practices and to obtain the requirements from the customer. The workshops should aim to focus on:
                                                <p class="tab-content" style="padding-left: 20px">a.	Review customer’s As-Is business process and practices (together with a visual representation of the flow within each process) 
                                                <p class="tab-content" style="padding-left: 20px">b.	Capture and document the requirements to be covered within each business process in the to-be solution </p></td>
                                            <td>PM  / SA / FL / TL / FC / TC / Customer Project team members / All relevant stakeholders</td>
                                            <td>Meeting Minutes of Workshops / Project Status Tracking Sheet </td>                                    
                                            <td>Business Blueprint Workshop Schedule / As-Is Business  Process Documents with process flow diagrams (prepared by Customer)</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>2.	Complete Core Team training</strong></p>
                                                <p>The core project team from the customer is taken through a SAP Overview</p>
                                            <td>FL / FC / Customer core project team</td>
                                            <td>attune Training Materials</td>
                                            <td>Core Team Training Check List</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>3.	Set up Sandbox System</strong></p>
                                                <p>This task refers to the creation and set up of the Sandbox system which is the SAP system for R&D as well as for demonstration purposes. This task is done in parallel to the workshops (or in some cases, prior to the workshops), so that the project team would have a system in place for research and to define solution options.</p>
                                                <p>The project team is given unrestricted access (“SAPALL” Access) to configure and/or develop in this system.</p></td>
                                            <td>PM / SAP Security</td>
                                            <td>Sandbox System Details</td>
                                            <td>Not Available</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>4.	Identify the Gaps</strong></p>
                                                <p>The project team identifies which processes/requirements can be mapped using SAP Standard and which processes/requirements are considered as Gaps.</p>
                                                <p>The Gap identification should cover:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Identify and document the processes /requirements that can be addressed using SAP standard functionality</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Identify the possible Gaps </p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Discuss and agree on the possible solution options to map the processes / requirements</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Use Hands on system demonstrations when necessary in order to further elaborate on the possible solutions (optional)</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Discuss and agree if some of these Gaps can be met with a business process change rather than doing customizing in the system </p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Document all agreed solutions to map the To-Be processes / requirements</p></td>
                                            <td>PM  / SA / FL / TL / FC / TC / All relevant stakeholders from Customer </td>
                                            <td>GAP List / Meeting Minutes of Workshops / Project Status Tracking Sheet / To-Be business process documents</td>
                                            <td>As-Is Business  Process Documents with process flow diagrams / Meeting Minutes of Workshops</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>5.	Define the Objects Identify</strong></p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The finalized list of objects to be configured</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The finalized list of objects to be developed</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	A naming convention for the objects. This naming convention is then shared with the customer teams and agreed upon. All the objects are to be named and referred as per the decided convention </p></td>
                                            <td>PM  / SA / FL / TL / FC / TC </td>
                                            <td>RICEFW Object List / List of Objects to be configured </td>
                                            <td>GAP List / Business Process List / As-Is Business  Process Documents with process flow diagrams / Meeting Minutes of Workshops</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>6.	Complete Documentation of Processes</strong></p>
                                                <p>The project team documents the To-Be solution for each process with a visual flow diagram</p>
                                                <p>The team completes the Business Blueprint Document</p></td>
                                            <td>PM  / SA / FL / TL / FC / TC</td>
                                            <td>To-Be Business Process Documents with process flow diagrams / Business Blueprint Document</td>
                                            <td>As-Is Business  Process Documents with process flow diagrams / RICEF Object List / List of Objects to be configured</td>
                                        </tr>
                                        <tr class="">                                            
                                            <%--<td></td>--%>
                                            <td><p><strong>7.	Blueprint Review</strong></p>
                                                <p>The project team holds the sessions to review the proposed solution with all the relevant stakeholders.</p> 
                                                <p>The review is done on documented To-Be solution and the Business Blueprint document. The review would consist of:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The To-be flow would cover the end to end scenarios rather than the individual process. The SAP documents to be used in the scenarios are highlighted. </p>
                                                <p>Once the review is completed, and if any last minute changes are accommodated, the Business Blueprint Document is updated and shared with the customer. </p>
                                                <p>The customer accepts the proposed solution presented in the Business Blueprint document, and the sign off is obtained.</p></td>
                                            <td>PM  / SA / FL / TL / FC / TC / All relevant stakeholders </td>
                                            <td>Business Blueprint Presentation / Project Status Tracking Sheet</td>
                                            <td>To-Be Business Process Documents with process flow diagrams / Business Blueprint Document</td>
                                        </tr>
                                                                                  
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Signed off Project Charter by Customer </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Project Kick Off  </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Project Schedule </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Blueprint Workshop Schedule </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Sandbox environment in place </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Blueprint Preparation Check List fulfilled </p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Successful completion of the Business Blueprint workshops </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed To-Be Process documents </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized RICEFW Objects List </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Finalized List of Configuration Objects </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Acceptance and sign-off of the Business Blueprint document</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Proposed Statement of Work </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Proposed Contract </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Project Schedule </p>
                                            <p class="tab-content" style="padding-left: 20px">•	As-Is Business Process Documents </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Workshop Schedule </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Project Status Tracking Sheet </p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	As-Is Business Process Documents with process flow diagrams </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Meeting minutes of Business Blueprint workshops </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Gap List </p>
                                            <p class="tab-content" style="padding-left: 20px">•	RICEFW Object List </p>
                                            <p class="tab-content" style="padding-left: 20px">•	List of Configuration Objects </p>
                                            <p class="tab-content" style="padding-left: 20px">•	To-Be Business Process Documents with process flow diagrams </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Presentation </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Document </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Project Status Tracking Sheet</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Schedule and effort variance for Business Blueprint Phase </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Review Efficiency</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	The project may decide to use client defined templates and standards for the deliverables from within the Blueprinting phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process. </p>

                                            <p class="tab-content" style="padding-left: 20px">•	Certain tasks of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable. </p>

                                            <p class="tab-content" style="padding-left: 20px">•	In case the client or another team representing the client is performing some parts of the tasks and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Client Sign-off of Business Blueprint Document will be decided at the project planning stage by the PGM/Business PM.</p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <h2>Document Templates</h2>                       
                        <p><a href="References\SAP Templates\attune Business Blueprint Template.docx">1. attune Business Blueprint</a></p>
                        <p><a href="References\SAP Templates\attune Integration Test Template.xls">2. attune Integration Test Template</a></p>                                   
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>
