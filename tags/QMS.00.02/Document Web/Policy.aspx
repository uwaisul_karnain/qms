﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Policy.aspx.cs" Inherits="Policy" %>

<asp:Content ID="attunePolocy" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Organization Policy</h1>
                        <h3>Objectives</h3>
                        <div class="data">
                            <p>The objective of the policy document is to establish the organizational expectations when following the organizational processes and procedures with core value added to the business to meet the organizational goals in the fashion and lifestyle industry.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The scope is to define and refine an organisational strategy for all attune employees to follow and adhere to the organizational culture of procedure. </p>
                        </div>

                        <h3>Policies</h3>
                        <h4>Requirement Management</h4>
                        <div class="data">
                            <p>Requirement Management objective should align with managing requirements and identifying inconsistencies between requirements, project plans and work products</p> 
                            <p>Build customer confidence with continues interaction and align project teams on customer expectation. </p>
                        </div>

                        <h4>Project Planning</h4>
                        <div class="data">
                            <p>Project Planning should be established to make internal and external commitments, meet organizational planning parameters and to come up with an approach to manage the project.</p>
                            <p>Plan project execution to improve organization profitability and project team to improve efficiency.</p>
                        </div>

                        <h4>Project Monitoring & Control</h4>
                        <div class="data">
                            <p>All the projects should monitor the project progress and performance against the project plan. Where there is a significant deviation is observed team should take corrective actions and track them towards the closure.  </p>
                        </div>

                        <h4>Process & Product Quality Assurance</h4>
                        <div class="data">
                            <p>Process and product quality assurance should apply to all projects, where objectively evaluate whether processes and associating work products adhere to the process descriptions, standards, procedures and the non-compliances are addressed. </p>
                        </div>

                        <h4>Configuration Management </h4>
                        <div class="data">
                            <p>Establish and maintain baselines and integrity of baselines, tracking and controlling changes to work products.</p>
                        </div>

                        <h4>Measurement and Analysis </h4>
                        <div class="data">
                            <p>Project measurement objectives should align organizational, project business, measurement objectives, and information needs.</p>
                            <p>Maintain organization metric data to provide timely leadership visibility on Time, Cost and Effort deviations.</p>
                        </div>

                        <h4>Integrated Project Management</h4>
                        <div class="data">
                            <p>Maintain the project’s defined process from project startup through the life of the project, using the project’s defined process in managing the project, and coordinating and collaborating with relevant stakeholders. </p>
                        </div>

                        <h4>Risk Management </h4>
                        <div class="data">
                            <p>Risk management should align with the defined a risk management strategy where project should identify, analyze, and mitigate risks. </p>
                        </div>

                        <h4>Decision Analysis and Resolution</h4>
                        <div class="data">
                            <p>Analyze all possible decisions using the organizational formal evaluation process to evaluate and identify alternatives against established criteria. </p>
                        </div>

                        <h4>Requirement Development</h4>
                        <div class="data">
                            <p>Collect stakeholder needs, formulate product and product component requirements, analyze and validate those requirements. </p>
                        </div>

                        <h4>Technical Solutions</h4>
                        <div class="data">
                            <p>Technical solutions should align with addressing the iterative cycle in which product or product component solutions are selected, designs are developed, and designs are implemented. </p>
                        </div>

                        <h4>Product Integration</h4>
                        <div class="data">
                            <p>Product Integration must declare the product integration strategies, procedures and the environment to ensuring the interface capability and compatibility among product components, how to assemble and deliver the product and product components.</p>
                        </div>

                        <h4>Verification</h4>
                        <div class="data">
                            <p>Review / gate process should established as per the defined organization process to assure the quality of deliverables and effort should estimate and plan. </p>
                            <p>Deliver high quality solutions to our customer and always plan for continuous improvement of delivery quality.</p>
                        </div>

                        <h4>Organization Process Focus</h4>
                        <div class="data">
                            <p>Validate process must be taken place to validate the product to see if the product meets the user accepted criteria’s and  product satisfies the end user needs in their intended environment </p>
                        </div>

                        <h4>Organization Process Definition</h4>
                        <div class="data">
                            <p>Organizational standard processes, rules & guidelines and organizational assets should be available for team members of the projects</p>
                        </div>

                        <h4>Organizational Training</h4>
                        <div class="data">
                            <p>Plan improvement of internal team competences with focused industry needs and identify the training needs of the organization with providing the trainings. </p>
                        </div>

                        <div class="data">
                        <h3>Document Templates</h3>                       
                        <p><a href="References\attune Organizational Policy Document.docx">1.  attune Organization Policy Document</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

</asp:Content>

