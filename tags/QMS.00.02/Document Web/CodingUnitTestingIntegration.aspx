﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CodingUnitTestingIntegration.aspx.cs" Inherits="CodingUnitTestingIntegration" %>


<asp:Content ID="codingUnitTestingIntegrationPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Coding, Unit Testing and Integration Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This process describes the coding, unit testing and integration activities used in software engineering lifecycle that need to be carried out in software development and maintenance projects.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>Projects are executed in a phased manner and the applicability of the various phases described in this process will depend on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The stage at which the project starts</p>
                            <p class="tab-content" style="padding-left: 20px">•	The expected deliverables</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>
                            <p class="tab-content" style="padding-left: 20px">•	The methodology imposed by the client</p>

                            <p>The coding, unit testing and integration activities defined here, along with the above, are used to derive the Project's Process at the beginning of the project (Project's Process procedure is defined in the Project Planning process)</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PC  - </strong>Process Consultant</p>
                                <p><strong>CM  - </strong>Configuration Management</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PL  - </strong>Project Lead</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>SE  - </strong>Software Engineer</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>UIL  - </strong>User Interface Lead</p>                                
                                <p><strong>UID  - </strong>User Interface Designer</p>
                                <p><strong>BA - </strong>Business Analyst</p>
                                <p><strong>QA  - </strong>Quality Assurance</p>
                                <p><strong>CR - </strong>Change Request</p>
                                <p><strong>PAD  - </strong>Project Approach Document</p>                                                          
                          </div>
                          <div class="col-xs-6 col-md-4">                                 
                                <p><strong>SRS  - </strong>System Requirement Specification</p>
                                <p><strong>RCS - </strong>Requirement Clarification Sheet</p>
                                <p><strong>RTM  - </strong>Requirement Traceability Matrix</p>
                                <p><strong>AD  - </strong>Architecture Design</p>
                                <p><strong>DD  - </strong>Detail Design</p>
                                <p><strong>aPTR - </strong>attune Process Tailoring Repository</p>                             
                          </div>
                        </div>


                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>The software development lifecycles followed by attune is agile or iterative method.</p>
                            <p>Unit Specifications are used for constructing the unit code and testing it, as described in the Perform Coding and Unit Testing procedure. Unit tested code is integrated together using the Perform Software Integration procedure to obtain sub-systems and the system.</p>
                            <p>All the procedures incorporate work-product reviews required for verification, and the CM activities for version control and establishing and maintaining baselines. </p>

                            <h4>1.	Steps</h4>                        
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr>
                                            <td rowspan="8"><strong>Coding and Unit Testing </strong></td>
                                            <td><p>The designated programmer writes/ modifies the code for the assigned unit. </p></td>
                                            <td>PM, TL, SE, UID</td>
                                            <td>Source Code, RTM</td>                                    
                                            <td rowspan="3">Schedule, Unit Specification, SRS, Product Backlog, RCS, Architecture Design, Detail Design, Coding Standards,  aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>If a change is requested, initiate the CRs. The changes are handled according to the Change Management Process</p>
                                                <p>(Template : Change Request Form)</p></td>
                                            <td>PM, TL</td>
                                            <td>CR Form(s)</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The programmer readies the code for execution and unit testing. Typically, this involves compiling the unit code. tribute / circulate a copy of the Quarterly Audit plan to relevant parties</p></td>
                                            <td>SE, UID</td>
                                            <td>Source Code</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>If scheduled, Code reviews are performed accordingly and review comments are incorporated in the code/ peer review form. Successful closure of review comments is verified.</p> 
                                                <p>(Template : Peer Review Form)</p></td>
                                            <td>PM, TL, UIL, SE, UID</td>
                                            <td>Peer Review Form(s)</td>
                                            <td>Schedule, Source Code, Standards & Guidelines</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The programmer tests the unit. Problems found are fixed in the unit test itself. The unit is debugged by the programmer and the testing repeated till the unit is tested OK.</p></td>
                                            <td>SE</td>
                                            <td>Source Code</td>
                                            <td>Unit Specification, SRS</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>If the contract requires external review of the code / unit testing by client or client representative, this is initiated. Sign-off is taken after the external review comments are incorporated.</p></td>
                                            <td>PM, SE, External QA</td>
                                            <td>Source Code, Review Comments</td>
                                            <td>Unit Specification, SRS</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Prepare the required product support manuals based on the Architecture Design. The manual may be a single manual or multiple manuals</p>
                                                <p>Update the RTM for traceability related to the above documents.</p>
                                                <p>(Templates : User and Operations Manual, Deployment Guide)</p></td>
                                            <td>TL, SE</td>
                                            <td>User & Operations Manual, Deployment Guide, RTM</td>
                                            <td>SRS, Product Backlog, Source Code</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM may arrange the internal review of the product support documents. The review comments are incorporated and verified.</p></td>
                                            <td>PM</td>
                                            <td>Peer Review Form</td>
                                            <td>User and Operations Manual, Deployment Guide</td>
                                        </tr>

                                        <tr>
                                            <td rowspan="4"><strong>Software Integration</strong></td>
                                            <td><p>The PM and his team use the Integration Test Plan for the sequence of integrating the components for linking and system/ sub-system testing.</p>
                                                <p>(Template : Integration Plan)</p></td>
                                            <td rowspan="3">Team</td>
                                            <td rowspan="3">Integrated System / sub-system</td>                                    
                                            <td rowspan="4">Integration Plan, AD, DD, PAD, aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The integration environment is established, verified and maintained.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Units may be combined into sub-systems and sub-systems may be combined into larger sub-systems / system and released for sub-system/system testing/user acceptance testing.</p>
                                                <p>Once all the components of a sub-system/ system are available, the sub-system/ system is built (compiled, linked, etc.) The build includes the corresponding documents, such as product support manuals.</p></td>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>A basic check is done to ensure that the sub-system / system can be delivered to for testing and the integrated sub-system / system is packaged, label versioning and released for the appropriate testing  team (System / sub-system testing or user acceptance).</p>
                                                <p>(Template : Release Note)</p></td>
                                            <td>SE</td>
                                            <td>Internal QA Release notification Mail, Release Note </td>
                                            <%--<td></td>--%>
                                        </tr>

                                    </tbody>


                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>






                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Unit Specifications are successfully completed reviewed </p>
                                            <p>•	Components to be integrated are available for integration.</p> 
                                            <p> (These components could be unit code / sub-systems to integrate as sub-systems / systems. Components could also be documents)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Code is unit tested and reviewed successfully </p>
                                            <p>•	Product support documents are updated </p>
                                            <p>•	Unit Testing is internally or  externally reviewed and signed-off (if applicable)</p>
                                            <p>•	Integrated system / sub-system delivered for appropriate testing.</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Unit Specifications</p> 
                                            <p>•	Development Standards</p>
                                            <p>•	Components to integrate</p>
                                            <p>•	Integration Plan</p>                                            
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Product Support Documents such as;</p>
                                            <p class="tab-content" style="padding-left: 50px">o	User and Operations Manual</p>
                                            <p class="tab-content" style="padding-left: 50px">o	Deployment Guide</p>
                                            <p>•	Release Note</p>  
                                            <p>•	Integrated sub-system / System</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort for Code Peer Review</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>Project leads / managers can decide to use client specified integration plans, in cases where client provides the integration plan</p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <h2>Document Templates</h2>                       
                        <p><a href="References\attune Integration Plan.docx">1. attune Integration Plan</a></p>
                        <p><a href="References\attune Release Note.docx">2. attune Release Note</a></p>

                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>






