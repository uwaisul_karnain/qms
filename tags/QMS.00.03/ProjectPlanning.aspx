﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProjectPlanning.aspx.cs" Inherits="ProjectPlanning" masterPageFile="~/MasterPage.master" %>

<asp:Content ID="projectPlanningPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project Planning Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>attune executes SAP developments, new technology software developments, enhancements and maintenance projects for its clients.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The term “project” is used to describe the full set of activities from the time the proposal is accepted (or a contract is signed) to the time the solution and services are delivered according to the accepted proposal/ contract.</p>
                            <p>This process ensures that a project is initiated and planned in a methodical manner.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>OM  - </strong>Operations Manager</p>
                                <p><strong>PC  - </strong>Process Consultant</p>
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>CM  - </strong>Configuration Manager</p>

                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>RM - </strong>Resource Manager</p>
                                <p><strong>M&A  - </strong>Measurement and Analysis</p>
                                <p><strong>WBS - </strong>Work Breakdown Structure</p>
                                <p><strong>SOW  - </strong>Statement Of Work</p>
                                <p><strong>PAD - </strong>Project Approach Document</p>
                          </div>
                          <div class="col-xs-6 col-md-4">  
                                <p><strong>DAR - </strong>Decision Analysis and Resolution</p>
                                <p><strong>QA Plan  - </strong>Quality Assurance Plan</p>
                                <p><strong>CM Plan - </strong>Configuration Management Plan</p>
                              <p><strong>PSTS - </strong>Project Status Tracking Sheet</p>
                          </div>

                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>When a proposal is accepted or a contract is signed, the BDGH assigns the project to a GM. The GM identifies the PM responsible for the project. Project initiation is completed when a kick-off meeting is held</p>

                            <p>The PM finalizes and documents the Project’s Process with help of project Technical lead and architect. This is reviewed by the Process Consultant (PC) and approved by the GM.</p>

                            <p>The PM plans the project using the PAD template. The PAD is created at the beginning of the project and kept up-to-date as the project progresses. The PAD contains sufficient detail to monitor and exercise control over the project execution. It is supported with a detailed schedule that contains activities for the next period for development/ enhancement projects. In maintenance projects, the detailed schedule is dynamically updated as and when maintenance requests are received and delivered. </p>
                             
                            <p>In addition to the PAD, project planning includes evolving the following set of project-related plans and documents which are used to execute the project:</p>

                            <p>•	Project QA Plan </p>
                            <p>•	Project CM Plan </p>
                            <p>•	Process Tailoring Record </p>

                            <p>All these are prepared using appropriate processes, in parallel with the PAD. All these plans are compatible with each other and with the proposal/ contract. </p>

                            <p>Once all the plans are created, reviewed and base lined, the project is executed, monitored and controlled using the Integrated Project Monitoring and Control process. </p>

                            <h4>2.	Steps</h4>                        
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="5"><strong>Initiate </strong></td>
                                            <td><p>With the SOW approval or client acceptance to the project, VP – Servicers create PSA entry to allocate Project Manager to the Project.</p> </td>
                                            <td>VP - Services</td>
                                            <td>SOW</td>                                    
                                            <td rowspan="3">SOW, Initial estimations, propose solution </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Send out RR to GRC (If planned for offshore resources, or resources are not available with the region)</p></td>
                                            <td>VP – Services/ Project Manager</td>
                                            <td>Email, PSA</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>If project is executing from the offshore center,  Offshore Team Lead Resource allocation</p></td>
                                            <td>Resource Manager -(Region)</td>
                                            <td>PSA</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Start project prep phase activities.</p>
                                                <p>•	High level project plan</p>
                                                <p>•	Detailed workshop schedule</p>
                                                <p>•	Project kickoff presentation</p>
                                                <p>•	Project charter & or Project approach Document</p>
                                                <p>•	Revalidate initial estimations (size and effort), risks, issues and client expectations</p>
	
                                                <p>Track respective project details in the project status tracking sheet.</p></td>
                                            <td>PM, Project Lead</td>
                                            <td>Plan, Project Kickoff PPT, Project Charter, PAD</td>
                                            <td rowspan="2">Initial estimations, Project Charter, SOW</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Conduct the project Client/ Team kickoff </p>
                                            <td>PM, Project Lead</td>
                                            <td>Estimation, Project Scedule, Project Charter </td>
                                            <%--<td></td>--%>
                                        </tr>

                                       <tr>
                                            <td rowspan="8"><strong>Prepare </strong></td>
                                            <td><p>Re- validate the WBS and estimation. Renegotiation on initial commitments.</p></td>
                                            <td>FC, SA, PM / Team Lead</td>
                                            <td>Estimation, Project schedule</td>                                    
                                            <td>PAD, Project Charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Update project schedule, schedule in PSA / Project tracking system.</p></td>
                                            <td>PM / Team Lead</td>
                                            <td>PSA, MPP, Project tracking system, Burn Down Charts</td>
                                            <td rowspan="3">Kickoff PPT</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Capture project milestones, assumptions, risks, issues, constraints, action items and dependencies.</p></td>
                                            <td>Team</td>
                                            <td>PSTS</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Define measureable objectives to the project with align to project success factors.</p></td>
                                            <td>PM / Team Lead</td>
                                            <td>Project Charter, PAD</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Conduct project team Skill assessment</p></td>
                                            <td>SA, Tech Lead</td>
                                            <td>Skill assessment </td>
                                            <td rowspan="4">SOW, Estimation</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Plan required trainings against to skill gaps</p></td>
                                            <td>PM/Team Lead, HR, Tech Lead, SA,FC</td>
                                            <td>Training plan</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Conduct project process tailoring </p></td>
                                            <td>PM/TL</td>
                                            <td>PTR</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Conduct project process tailoring </p></td>
                                            <td>PM/TL</td>
                                            <td>Project Charter, PAD, Skill assessment, Training plan, MPP, PTR</td>
                                            <%--<td></td>--%>
                                        </tr>
                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Acceptance of proposed solution/ signing of contract (SOW) </p>
                                            <p>•	Project Kick-off/ Charter PPT </p>
                                            <p>•	PM/PL and Team is assigned to the project </p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Completed Project Kickoff Plan </p>
                                            <p>•	Commitment to all plans by the relevant stakeholder </p>
                                            <p>•	People are assigned for project planning activities </p>
                                            <p>•	Project Approach & or Charter Document created </p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Project Kickoff/Charter PPT </p>
                                            <p>•	Acceptance of proposed solution/ signing of contract (SOW) </p>
                                            <p>•	Working papers (including the estimation sheets) accompanying the proposal/ contract </p>
                                            <p>•	Records of meetings with client/ correspondence </p>
                                            <p>•	Proposed solution (optional) </p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project Kick-off Plan </p>
                                            <p>•	Minutes of meeting Related to Kick-Off </p>
                                            <p>•	Detailed Project Schedule </p>
                                            <p>•	Work Break Down Structure / 3 point Estimation Sheet </p>
                                            <p>•	Project Skill Tracker </p>
                                            <p>•	Training Requisition </p>
                                            <p>•	Project Status Tracking Report </p>
                                            <p>•	Project Approach Document </p>
                                            <p>•	Project Charter PPT </p>
                                            <p>•	Review Records of Project Approach Document </p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort spent on project estimation</p>
                                            <p>•	Review effort spent on PAD, Estimation, and Process Tailoring</p>
                                            <p>•	Review comments count</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	The PAD may include the Project CM Plan and the Project’s Process in a single document (see tailoring options in the Configuration Management Process).</p>
                                            <p>•	There may be no phase-end/ milestone reviews for projects of less than one month duration. </p>
                                            <p>•	For projects of less than three months duration, the Project Status Report will not be prepared but a Project Status Tracking report will be created every month. </p>
                                            <p>•	In some cases, the client may insist on using their templates for PAD and Project Status Report. In such cases, the PM will map the client’s templates to attune templates and ensures that all aspects of attune templates are covered. Addendums should be provided to cover the gaps in the client’s templates. </p>
                                            <p>•	For Projects less than 20 man days effort, PM together with GM and PCGH can decide on a more light weight process, which should be documented in the PAD.</p> 
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <h2>Document Templates</h2>                       
                        <p><a href="References\attune Project Charter.pptx">1. attune Project Charter</a></p>
                        <p><a href="References\attune Project Approach Document.docx">2. attune Project Approach Document</a></p>
                        <p><a href="References\attune Project Status Tracking Sheet.xlsx">3. attune Status Tracking Sheet</a></p>
                        <p><a href="References\attune Project Effort Estimation Sheet - 3 Point Estimation.xls">4. attune Effort Estimate ( 3 - Point Estimation )</a></p>
                        <p><a href="References\attune Process Tailoring Repository.xlsx">5. attune Process Tailoring Repository</a></p>
                        <p><a href="References\attune Project Skills Tracker.xlsx">6. attune Skill Tracker</a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>