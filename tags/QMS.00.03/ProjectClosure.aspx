﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ProjectClosure.aspx.cs" Inherits="ProjectClosure" %>


<asp:Content ID="projectClosurePageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Project  Closure  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The Project Closure is the project review process consisting of activities performed by a project team at the end of the project's life cycle or at the end of significant release / phases of work / or at the end of every iteration / or every 6 months for long duration project to gather information on what worked well and what did not, so that future projects can benefit from that learning. The project closure should be ideally completed latter part of the project execution. However, duration could vary based on the nature of the project. When the closure output is of direct relevance to the customer, this activity may be conducted as a part of the project contract.</p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>Scope of the project closure process is to highlight the learning points of project/ release, in its project life cycle.  Project closure is used to describe the full set of activities conducted when project come to an end of the agreed upon phase or work stream according to the accepted proposal/ contract. Mainly below activities conduct in this stage to smooth product handover and closure.</p>
                            <p class="tab-content" style="padding-left: 40px">•	Required user trainings are completed and training materials uploaded to project closure folder in the one attune.</p>
                            <p class="tab-content" style="padding-left: 40px">•	Conduct project closure meeting with stakeholders</p>
                            <p class="tab-content" style="padding-left: 40px">•	Post mortem analysis conducted</p>
                            <p class="tab-content" style="padding-left: 40px">•	Project case study</p>
                            <p class="tab-content" style="padding-left: 40px">•	Knowledge management to the organization</p>
                            <p class="tab-content" style="padding-left: 40px">•	Team de-allocation</p>
                            <p class="tab-content" style="padding-left: 40px">•	Inform respective stakeholders and groups</p>
                        </div>

                        <h3>Project Termination</h3>
                        <div class="data">
                            <p>Project termination may be necessary due to one or multiple reasons listed below. Project leadership will take the decision with considering facts and impact to the company, client and business.</p>
                            <p class="tab-content" style="padding-left: 40px">•	Due to technical reasons</p>
                            <p class="tab-content" style="padding-left: 40px">•	Project requirements/ blueprint or specifications of the project result are not clear or unrealistic</p>
                            <p class="tab-content" style="padding-left: 40px">•	Requirements / blueprint or specifications change fundamentally so that the agreed upon contract cannot be changed accordingly</p>
                            <p class="tab-content" style="padding-left: 40px">•	Project level of client level, Lack of project planning, project monitoring and control  especially risk management</p>
                            <p class="tab-content" style="padding-left: 40px">•	The intended result or product of the project becomes obsolete, is not any longer needed</p>
                            <p class="tab-content" style="padding-left: 40px">•	To complete or execute the project :Adequate human resources or tools are not available</p>
                            <p class="tab-content" style="padding-left: 40px">•	Project make continues losses or project profit becomes significantly lower than expected</p>
                            <p class="tab-content" style="padding-left: 40px">•	Customer company or their parent organization does not longer exist</p>
                            <p class="tab-content" style="padding-left: 40px">•	Customer organization changes its strategy, and the project does not support the new strategy</p>
                            <p class="tab-content" style="padding-left: 40px">•	Lack of management support</p>
                            <p class="tab-content" style="padding-left: 40px">•	Lack of customer support</p>
                            <p class="tab-content" style="padding-left: 40px">•	Legal change or decision</p>
                            <p>With one or many reasons stated above any project come to the terminate state. They should execute mandatory/ possible project closure activities. </p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>TL  - </strong>Technical Lead</p>                                
                                <p><strong>PL - </strong>Project Lead</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>UAT  - </strong>User Acceptance Testing</p>                                
                          </div>
                          <div class="col-xs-6 col-md-4">  
                                <p><strong>PAD - </strong>Project Approach Document</p>
                                <p><strong>MPP  - </strong>Microsoft Project Plan</p>
                          </div>

                        </div>

                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>The customer should acknowledge that all deliverables - product, documentation, supporting environment, etc. - and activities, such as installation and user training, have been completed according to the SOW and its supporting plans. Project closure phase should be ideally completed within two to four weeks window; however, duration could vary based on the nature of the project. When the closure output is of direct relevance to the customer, this activity may be conducted as a part of the project contract. Even through if it is not in the SOW, due to the attune support team empowerment team conduct this activity to smooth project transition to attune support team.</p>

                            <h4>2.	Steps</h4>                        
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="3"><strong>Initiate </strong></td>
                                            <td><p>With the project UAT signoff team start working on project closure activities.</p> </td>
                                            <td>TL/PM</td>
                                            <td></td>                                    
                                            <td rowspan="3">SOW , PAD, Project charter</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Project Closure Prep Meeting and support team introduction to the customer</p>
                                            <p class="tab-content" style="padding-left: 20px">•	The Customer Facing PM will initiate a project closure meeting with the customer. The aim of this meeting is to formally close the project engagement and if in scope introduce attune support team to customer.</p></td>
                                            <td><p>PM, PL</p></td>
                                            <td><p>Meeting agenda and Minutes</p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Internal Project closure meeting and plan for the project closure activities is put together and the responsibilities / tasks are allocated to the team.</p></td>
                                            <td><p>PM, PL, TL</p></td>
                                            <td><p>MPP</p></td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="11"><strong>Prepare </strong></td>
                                            <td><p>Project closure documentation</p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Discuss the elements, which need to be included to the project closure </p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Identify responsibilities and ownership </p> 
                                                <p class="tab-content" style="padding-left: 20px">•	Identify time frame for the documentation</p> </td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>Project closure template</td>                                    
                                            <td rowspan="11"></td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Project lesson learn and case study document preparation with respect to the lesson learnt meeting outcome. </p></td>
                                            <td>Team</td>
                                            <td>Project case study</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Review closure documentation</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Team and Leadership review and feedbacks</p>
                                                <p class="tab-content" style="padding-left: 20px">•	Incorporate review feedbacks</p></td>
                                            <td>Team</td>
                                            <td>Project closure template</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Create Reusable components and store in the respective locations.</p>
                                                <p>Communicate the same to Knowledge Management / Reuse groups</p>
                                                <p>Make reuse friendly tags and key words include to the one attune </p></td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>Reusable components</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Create project document, source and etc archive ( one attune and or SVN)</p></td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>One attune folder</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Start resource de-allocation with keeping project closure related activity execute resources available </p></td>
                                            <td>PM, PL</td>
                                            <td>PSA</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Identify internal and external parties should communicate the project closure. Example : HR, Finance, etc </p></td>
                                            <td>PM, PL</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Close all user training and training materials and share with support team</p></td>
                                            <td>PM, PL</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Inform project closure to attune Knowledge management team and invite them to closure meeting</p></td>
                                            <td>PM, PL</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Complete project team feedbacks to the leadership</p></td>
                                            <td>PM, PL, HR</td>
                                            <td>Feedback report</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Update skill records in the attune central resource skill tracking sheet</p></td>
                                            <td>PM, PL, TL, Architect</td>
                                            <td>attune skill tracking sheet</td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="4"><strong>Conduct </strong></td>
                                            <td><p>Conduct Project closure presentation with all respective stakeholders</p> </td>
                                            <td>PM, PL</td>
                                            <td>Project closure PPT / Document</td>                                    
                                            <td rowspan="4"></td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Support team conduct project handover validation and provide acceptance.</p></td>
                                            <td>PM, TL, Support team manager</td>
                                            <td>Project support acceptance check list</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>De – allocate all team members</p></td>
                                            <td>PM, PL</td>
                                            <td>PSA</td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Project Closure Announcement – communicate all relevant stakeholders</p>  
                                                <p>Project team, Finance, HR, Global resource Canter, etc</p></td>
                                            <td>PM, PL</td>
                                            <td>email</td>
                                            <%--<td></td>--%>
                                        </tr>
                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Build / Release completion</p>
                                            <p>•	Proposed contract (SOW)</p>
                                            <p>•	One or more of the project termination status occurred</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	All project documents, templates and source documents are archived.</p>
                                            <p>•	Resource are de- allocated</p>
                                            <p>•	Reuse components identified, make re-useable and communicated </p>
                                            <p>•	Closure announcement</p>
                                            <p>•	Project case study</p>
                                            <p>•	Team feedbacks</p>
                                            <p>•	attune support team provide the acceptance note for support</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Proposed contract (SOW)</p>
                                            <p>•	Project Charter / Project approach document</p>
                                            <p>•	Records of meetings with client/ correspondence</p>
                                            <p>•	UAT signoff </p>
                                            <p>•	Project termination note from the leadership team</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Project case study</p>
                                            <p>•	Project closure meeting PPT</p>
                                            <p>•	Updated Reuse list</p>
                                            <p>•	Training materials</p>
                                            <p>•	Project archived folder</p>
                                            <p>•	Support acceptance criteria check list</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Effort spent on project closure activities</p>
                                            <p>•	Number of reuse components </p>
                                            <p>•	Schedule and Effort Variance on closure activities</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>•	In a longer term project team have to conduct at least every 6 month lesson learnt document / project case study</p> 
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <h2>Document Templates</h2>                       
                        <%--<p><a href="References\attune MOD.xlsx">1. Metric Objective Description</a></p>--%>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>


