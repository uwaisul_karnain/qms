﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RiskManagement.aspx.cs" Inherits="RiskManagement" %>


<asp:Content ID="riskManagementPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Risk  Management  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>The objective of Risk Management process is to ensure that risks are identified, evaluated, mitigated and plan for contingencies throughout the project life cycle.  In the time of project Preparation/ Initiation stage to project closure and or Go- Live stage all Risk Identification, Risk Analysis, Risk Response Planning, Managing the Risks and Monitoring & Re-assessment of the risk will be focused in the risk management process. </p>
                        </div>

                        <h3>Scope</h3>
                        <div class="data">
                            <p>The scope of the organizational risk management are covered on this process. </p>
                            <p class="tab-content" style="padding-left: 40px">•	Technical risks associated with the product / project </p>
                            <p class="tab-content" style="padding-left: 40px">•	Customer related project impacted risks</p>
                            <p class="tab-content" style="padding-left: 40px">•	Project management related risks</p>
                            <p class="tab-content" style="padding-left: 40px">•	Resource related risks </p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>CMP - </strong>Configuration Management Plan</p>
                                <p><strong>CM  - </strong>Configuration Management</p>                                
                                <p><strong>BA - </strong>Business Analyst</p>
                                <p><strong>QA - </strong>Quality Assurance</p>
                          </div>
                          <div class="col-xs-6 col-md-4"> 
                                <p><strong>PL - </strong>Project Lead</p>
                                <p><strong>TL  - </strong>Technical Lead</p>                                
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>SOW - </strong>Statement of Work</p>                                
                          </div>
                          <div class="col-xs-6 col-md-4">  
                                <p><strong>PAD - </strong>Project Approach Document</p>
                                <p><strong>VPS  - </strong>Vice President Services</p>                                
                                <p><strong>SA - </strong>Solution Architect</p>
                                <p><strong>PSTS - </strong>Project Status Tracking Sheet</p>  
                          </div>
                        </div>
	
                        <div class="data">
                            <h2>Process and Steps</h2>    

                            <p>During the proposal making activity, initial risk analysis is done. This involves identification of the risks, their impacts and probabilities. Risk analysis at this stage is the responsibility of the VPS and Solution Architect.</p>
                            <p>When the project starts, the PM / PL prepares a Risk Management Plan document along with the other project planning documents. The project risks are maintained in the Project Status Tracking sheet with required details. These are used to monitor risks and track mitigation/ contingency actions. In the risk tracking sheet, the “risk exposure” to each risk is computed. The quantified “risk exposure” numbers are used to decide the degree of monitoring and escalation method. The “risk exposure” is also used to decide the importance of taking mitigation and contingency actions.</p>

                            <p>The PM / PL regularly reviews risk status. The VPS reviews risk status during the Management Review of Projects. Additionally, the PM /PL may discuss risks on an event driven basis with the GM and SEPG Heads, depending on the risk exposure.</p>
                            <p>The Procedure is</p>
                            <p class="tab-content" style="padding-left: 20px">•	Plan for Risk Management</p>
                            <p class="tab-content" style="padding-left: 20px">•	Identify and Analyze Risks</p>
                            <p class="tab-content" style="padding-left: 20px">•	Manage Risks</p>

                            <h4>2.	Steps</h4>                        
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="5"><strong>Plan </strong></td>
                                            <td><p>The PM schedules a meeting with the VPS, SA, and TL team to brainstorm for project risk inherited from the pre-sale stage and new all possible risks.</p> </td>
                                            <td>PM, PL </td>
                                            <td>PSTS</td>                                    
                                            <td rowspan="5">Proposed SOW, PAD,  presale documents,</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The risks identified in the project Estimation Sheets are circulated to all participants of the risk brainstorming meeting. </p>
                                            <p class="tab-content" style="padding-left: 20px">•	The Customer Facing PM will initiate a project closure meeting with the customer. The aim of this meeting is to formally close the project engagement and if in scope introduce attune support team to customer.</p></td>
                                            <td><p>PM, PL, SA</p></td>
                                            <td><p></p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The elements of the risk management Plan that are discussed and finalized are as follows:</p>
                                                <p>•	Roles and responsibilities</p>
                                                <p>•	Risk sources (internal and external) that will be used</p>
                                                <p>•	Risk categories that will be used</p>
                                                <p>•	Risk parameters that will be used (probability, impact, exposure)</p>
                                                <p>•	Thresholds for escalation</p>
                                                <p>•	Thresholds for identifying mitigation and contingency actions</p>
                                                <p>•	Normal risk monitoring frequency and exceptional risk monitoring frequency, based on risk exposure</p>
                                                <p>•	Persons to be involved at various levels in risk management</p>
                                                <p>•	Tools and other resources required for effective risk management</p>
                                                <p>•	Training required for risk management</p>
                                                <p>•	Stakeholder involvement in risk management</p>

                                                <p>In addition to the defining the elements of the risk management Plan, the brainstorming team may identify risks and agree on the sources, category, probability, and mitigation and contingency actions.</p></td>
                                            <td><p>PM, PL, SA</p></td>
                                            <td><p>PAD</p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM may refer to the Organization level Risk Management database for the project. Risks relevant to project will inherit to current project. </p>
                                                <p>In case project team use client provided risk management plan as base, team should address the all internal risks and not covered in the client plan in the internal risk management plan</p></td>
                                            <td><p>PM, PL</p></td>
                                            <td><p>Risk Database</p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Project Risk Management Plan document is reviewed by the PC and the Practice lead. After the review comments are incorporated, the Risk Management Plan is approved by the Practice Lead and Operations Director and the PM baselines the Risk Management Plan.</p></td>
                                            <td><p>Practice lead, PC,</p></td>
                                            <td><p></p></td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="5"><strong>Identify & Analyze </strong></td>
                                            <td><p>The PM and project team members meet to identify and analyze risks. </p>
                                                <p>Using brainstorming techniques and discussions, the team identifies the risks for the project. The attune data repository database (Organization Risk Repository) is referred for understanding the risks faced by other projects. </td>
                                            <td><p>PM, PL,TL</p></td>                                            
                                            <td></td>                                    
                                            <td rowspan="5">aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>They review the risks identified in the input documents (estimation sheets, minutes of meeting for risk management Plan). Probability of occurrence and Impact will be considered and captured. </p>
                                                <p>Each risk is classified in terms of sources and categories identified.</p>
                                                <p>Identified details and documented in the tracking sheet. </p></td>
                                            <td>PM, PL,TL</td>
                                            <td></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The Risk Exposure score for each risk is computed. The total risk exposure for the project is also computed.</p></td>
                                            <td><p>PM, TL,  PC</p></td>
                                            <td><p></p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p> For risks where the risk exposure crosses the defined threshold for mitigation/ contingency planning, a risk tracking sheet is opened. Mitigation and contingency actions are identified for the risk. </p>
                                                <p> Each mitigation/ contingency action is accompanied by the names of the persons responsible and the dates by which the action needs to be completed.</p></td>
                                            <td><p>PM, TL,  PC</p></td>
                                            <td><p></p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p> The PM ensures that the Risk Tracking Sheet is complete and reflects the data as discussed in the initial risk analysis meeting.</p></td>
                                            <td><p>PM, TL,  PC, PL</p></td>
                                            <td><p>Team Meeting, PSTS</p></td>
                                            <%--<td></td>--%>
                                        </tr>

                                        <tr>
                                            <td rowspan="8"><strong>Manage </strong></td>
                                            <td><p>The PM/ PL/ TL with help of project team reviews and updates the Risk Summary and Risk Tracking Sheets to reflect revised understanding of risks and related actions. </p></td>
                                            <td><p>PM, TL,  PC, PL</p></td>
                                            <td><p>Team Meeting, PSTS</p></td>
                                            <td rowspan="8">aPTR</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>The PM/ PL/ TL with help of project team reviews and updates the Risk Summary and Risk Tracking Sheets to reflect revised understanding of risks and related actions</p></td>
                                            <td><p>PM, PL, TL, Team</p></td>
                                            <td><p>PSTS</p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>A new Entry in the Risk Tracking Sheets is created, where:</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The status of mitigation actions and contingency actions is updated. </p>
                                                <p class="tab-content" style="padding-left: 20px">•	In case some of the actions are pending beyond the due date, a revised date is specified</p>
                                                <p class="tab-content" style="padding-left: 20px">•	The person responsible may also be changed</p>
                                                <p class="tab-content" style="padding-left: 20px">•	New mitigation/ contingency actions are identified </p></td>
                                            <td><p></p></td>
                                            <td><p></p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>For risks whose risk exposure has crossed threshold for mitigation/ contingency planning, Contingency Plan Column needs to be updated by the PM / PL /TL and the Contingency plan will be discussed with Operations Director / Practice head. Tracking Notes Section will be used to track the progress of the Risk to closure.</p></td>
                                            <td><p></p></td>
                                            <td><p></p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Identified actions are tracked to closure.</p></td>
                                            <td><p></p></td>
                                            <td><p></p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>Based on the criteria defined in the Risk Management Plan, escalation to the appropriate authority is done. Criteria include the risk exposure of individual risks and the total risk exposure of the project. </p></td>
                                            <td><p></p></td>
                                            <td><p></p></td>
                                            <%--<td></td>--%>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>If any Risk materialized, that should be transferred to issues list with required details.</p></td>
                                            <td><p></p></td>
                                            <td><p></p></td>
                                            <%--<td></td>--%>
                                        </tr>


                                    </tbody>

                                    </table>
                                </div>
                            </div>

                        </div>

                        <div>

                        </div>
                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Draft Risk Management Plan </p>
                                            <p>•	Risk Tracking Sheet finalized</p>
                                            <p>•	Initial prep stage risks are identified and documented</p>
                                            <p>•	Project high level schedule and delivery model</p>
                                            <p>•	Project resources plan, offshore / onsite project roles and responsibilities</p>
                                            <p>•	Proposed contract (SOW)</p>
                                            <p>•	PM or Project Lead, Functional and Technical Leads are assigned to the project</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p>•	Approval of Risk Management Plan </p>
                                            <p>•	Updated Risk Tracking Sheet</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p>•	Proposed contract (SOW)</p>
                                            <p>•	Initial prep stage risks are identified and documented</p>
                                            <p>•	Records of meetings with client/ correspondence</p>
                                            <p>•	Proposed solution (optional) </p>
                                            <p>•	Project Status Tracking Sheet</p>
                                            <p>•	Estimation sheets with respective risks and assumptions</p>
                                            <p>•	Risk Management Plan</p>
                                            <p>•	Minutes of Meeting where risk management Plan is decided</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p>•	Risk Management Plan / Update the Project Approach for Risk Management tailoring</p>
                                            <p>•	Risk Tracking Sheet (Project Status Tracking Sheet)</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p>•	Number of project risk team found before it occurred ( project issues and same oversee rate)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p>Risk management is a mandatory practice for all projects.</p>
                                            <p>•	The project may use a database instead of the risk summary and risk tracking spreadsheets.</p>
                                            <p>•	The frequency of revisiting risks may be higher than once a week.</p>
                                            <p>•	Projects may chose and define a different form of categorization of risks and also define different thresholds for mitigation and  contingency planning </p> 
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>
                                                
                        <h2>Document Templates</h2>                       
                        <p><a href="References\attune Risk Management Process.docx">1. Risk Management Process Document</a></p>
                        <p><a href="References\attune Project Status Tracking Sheet.xlsx">2. attune Project Status Tracking Sheet Template</a></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>



