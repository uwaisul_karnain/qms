﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="IntegrationTestingAndUAT.aspx.cs" Inherits="SAPWebPages_IntegrationTestingAndUAT" %>

<asp:Content ID="integrationAndUATPageContent" ContentPlaceHolderID="ContentPlaceHolder1" runat ="server">

          <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <%--<a href="#menu-toggle" class="btn tog btn-default visible-xs glyphicon glyphicon-list" id="menu-toggle"></a>--%>
                        <h1>Realization – Integration  Testing  &  UAT  Process</h1>
                        <h3>Objective</h3>
                        <div class="data">
                            <p>This document describes the process to be followed in the Integration Testing and UAT phase of Realization of a SAP implementation project lifecycle.  It covers the activities that need to be performed within this phase.</p>
                        </div>
                        <h3>Scope</h3>
                        <div class="data">
                            <p>All SAP implementation projects are executed in phases. The applicability of the various activities described in this process depends on:</p>
                            <p class="tab-content" style="padding-left: 20px">•	The expected deliverables</p>
                            <p class="tab-content" style="padding-left: 20px">•	The contractual requirements</p>
                            <p class="tab-content" style="padding-left: 20px">•	The methodology imposed by the client</p>

                            <p>The activities related to the Integration Testing and UAT phase that are defined here derive the process followed during this phase of the project.</p>
                        </div>

                        <h3>Definitions, Acronyms, and Abbreviations</h3>
                        <div class="row">
                          <div class="col-xs-6 col-md-4">
                                <p><strong>BBP - </strong>Business Blueprint Document</p>
                                <p><strong>BP - </strong>Business Process</p>
                                <p><strong>BPO - </strong>Business Process Owner</p>
                                <p><strong>CR - </strong>Change Requests</p>
                                <p><strong>FC - </strong>Functional Consultant</p>
                                <p><strong>FICO - </strong>Finance and Controlling</p>
                                <p><strong>FL - </strong>Functional Lead</p>
                                <p><strong>FS - </strong>Functional Specification</p>
                                <p><strong>MM - </strong>Materials Management</p>
                                <p><strong>PC - </strong>Project Charter</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>PGM - </strong>Program Manager</p>
                                <p><strong>PM - </strong>Project Manager</p>
                                <p><strong>PMO - </strong>Project Management Office</p>
                                <p><strong>PP - </strong>Planning and Production</p>
                                <p><strong>PPL - </strong>Project Plan</p>
                                <p><strong>PSD - </strong>Professional Services Director</p>
                                <p><strong>RICEF - </strong>Reports, Interfaces, Custom Enhancements and Forms</p>
                                <p><strong>RM - </strong>Resource Manager</p>
                                <p><strong>SA - </strong>Solution Architect</p>
                                <p><strong>SD - </strong>Sales and Distribution</p>
                          </div>
                          <div class="col-xs-6 col-md-4">
                                <p><strong>SOW - </strong>Statement of Work</p>
                                <p><strong>TA - </strong>Technical Architect</p>
                                <p><strong>TC - </strong>Technical Consultant</p>
                                <p><strong>TL - </strong>Technical Lead</p>
                                <p><strong>TR - </strong>Transport Request</p>
                                <p><strong>TS - </strong>Technical Specification</p>
                                <p><strong>UAT - </strong>User Acceptance Testing</p>
                                <p><strong>WBS - </strong>Work Breakdown Structure</p>
                                <p><strong>WP - </strong>Work Package</p>
                          </div>
                        </div>

                        <div class="data">
                            <p>                             </p>
                        </div>


                        <div class="data">
                            <h2>Process and Steps</h2>    
  
                            <p>The SAP Implementation lifecycle followed by attune is based on the phases defined in the attune Implementation Roadmap (aIR), which was derived using the SAP ASAP Focus methodology. Within the aIR methodology, the Realization phase follows the Business Blueprint phase. The Realization phase is broken further into two, namely Build and Integration Testing & UAT. The Build part of the Realization covers the activities performed to realize the proposed solution mentioned in detail in the Business Blueprint Document.</p> 
                            <p>It is in this phase that the project team defines the WBS or the WP, as per the finalized RICEF and Configuration list for every business process, with the agreed naming conventions. The resources from the project team (the FCs and TCs) are then allocated to each WBS or WP. The required specification documents for the RICEFs and Configurations are prepared by the resources, which are then reviewed together with the relevant stakeholders from the customer, and the necessary approvals are obtained prior to starting the development or configuration. The developments are carried out as per the given standards and guidelines. Developments and configuration are tracked using the tracking sheet in order to ensure that the progress is as per the plan. The unit testing is completed for the configured and developed objects, one by one, in order to ensure that these objects are as per the requirements and are fit for the purpose, before it is provided to the business users to test.</p> 
                            <p>Prior to the activities mentioned above, the systems are put in place and the system landscape defined in the Project Charter is set up.  </p>
                            <p>Additionally, the ground work required to start the Integration testing and UAT is done in this phase as well. The end to end business scenarios which are to be used for the Integration testing and UAT are defined. The test scripts are prepared as per the defined scenarios and validated by the relevant stakeholders from the customer. The detailed project plan is updated to define the timeline for the testing. A schedule is prepared on the Integration testing and UAT and is shared with the relevant stakeholders from the customer.</p>
                            <p>In parallel, a training schedule is defined in order to train the users who are to be part of the testing team confirmed by the customer. The detailed project plan would also include the timelines for the training. The required training materials are created as well. </p>
                            <p>The Project Status Tracking Sheet is used to document all Issues, CRs, Assumptions, Risks, Action Items highlighted and discussed during this phase.</p>
                            <p>Therefore, the table below highlights each of the activities mentioned above in detail. It provides information on the activities, the responsible person or team, the documents created in each activity (i.e. artifacts) and the reference documents used. </p>


                            <h4>1.	Steps</h4>
                                     
                            <div class="table-data">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        <th>Sub Process</th>
                                        <th>Task</th>
                                        <th>Responsibility</th>
                                        <th>Artifacts</th>
                                        <th>Reference</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="7">Realization - Execution of Integration Testing & UAT</td>
                                            <td><p>1.	Conduct Testing Kick off</p>
                                                <p>In this task, the Integration Testing is initiated and the preparations are done for UAT. Here the following takes place:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Conduct the Testing Kick off meeting with the relevant stakeholders from customer, the customer’s core project team, the attune project team, the testing team from customer</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	Provide access to the customer’s core project team, attune project team, the testing team for the Quality box setup in the Build phase with the required authorizations</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	Move all of the changes, both configuration and development, to the Quality box using the Transport Tracking sheet in the right sequence</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Share the testing schedule, testing timeline, the test scripts to be used for testing with all relevant stakeholders from the customer, the customer’s core project team, attune project team and testing team in order to create the awareness and start the testing</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Update detailed Project Plan with the Testing timelines</p></td>
                                            <td>PM / SA / FL / TL / FC / TC / SAP Security / Customer’s core project team</td>
                                            <td>Updated Testing Schedule / Updated Detailed Project Plan</td>                                    
                                            <td>Detailed Project Plan / Test Scripts / Testing Schedule</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>2.	Conduct Testing and Manage Issues & Defects</p>
                                                <p>This task covers both the performance of the Integration testing and the subsequent UAT. Hence, the tasks given are broken down for each testing phase.</p>
                                                <p>Activities for Integration testing:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Integration testing is done by the attune project team together with the customer’s core project team.  </p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The already defined test scripts covering the end to end business scenarios are used for the testing</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The tests  scripts are updated with the test results </p>
                                                <p class="tab-content" style="padding-left: 20px">d.	Issues faced are recorded in a separate Issue log together with the fixes to resolve the issues</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Any CRs are added into either the Project Status Tracking sheet or in a separate CR Log</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	These CRs are discussed with the relevant stakeholders from the customer and the decision is taken on whether any of the CRs are accommodated prior to UAT based on business criticality</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	The CRs could be to change an existing configuration or development object or for a completely new object</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	For those CRs that are accommodated, the Build Tracker is updated and the development  starts (The Business Blueprint document and the To-Be business process documents with the process flows are updated with the changes as well)</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	The FS is prepared and reviewed with the relevant stakeholders from the customer and the approval is obtained</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	The TS are updated within the same FS and the development starts (for Development Objects only)</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	The unit testing is completed and then the object is moved to the Quality box in order to complete the Integration testing</p>
                                                <p class="tab-content" style="padding-left: 20px">l.	The TRs generated for the new object is added into the Transport Tracking Sheet in the right sequence</p>
                                                <p class="tab-content" style="padding-left: 20px">m.	All the end to end business scenarios documented in the test scripts are tested together with the interfaces to other third party systems </p>
                                                <p class="tab-content" style="padding-left: 20px">n.	Once all the scenarios are successfully completed with no further issues, the results are reviewed and the decision is made together with the relevant stakeholders of the customer in order to complete the Integration testing phase</p>

                                                <p>Activities for UAT:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The UAT is done by the testing team from the customer together with the customer’s core project team and attune project team</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	In order to prepare the testing team for UAT, the training takes place (this training may happen in parallel to Integration testing)</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The testing team is given access to the Quality box with the required authorizations for training as well as testing</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	With the training completed, the UAT commences using the Test Scripts already in place which covers all of the end to end scenarios to be tested</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	Issues faced are recorded in a separate Issue log together with the fixes to resolve the issues</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Any CRs are added into either the Project Status Tracking sheet or in a separate CR Log</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	These CRs are discussed with the relevant stakeholders from the customer and the decision is taken on whether any of the CRs are accommodated based on business criticality</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	The CRs could be to change an existing configuration or development object or for a completely new object</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	For those CRs that are accommodated, the Build Tracker is updated and the development starts (The Business Blueprint document and the To-Be business process documents with the process flows are updated with the changes as well)</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	The FS is prepared and reviewed with the relevant stakeholders from the customer and the approval is obtained</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	The TS are updated within the same FS and the development starts (for Development Objects only)</p>
                                                <p class="tab-content" style="padding-left: 20px">l.	The unit testing is completed and then the object is moved to the Quality box in order to complete the UAT</p>
                                                <p class="tab-content" style="padding-left: 20px">m.	The TRs generated for the new object is added into the Transport Tracking Sheet in the right sequence</p>
                                                <p class="tab-content" style="padding-left: 20px">n.	All the end to end business scenarios documented in the test scripts are tested together with the interfaces to other third party systems </p>
                                                <p class="tab-content" style="padding-left: 20px">o.	Once all the scenarios are successfully completed with no further issues, the results are reviewed and the decision is made together with the relevant stakeholders of the customer in order to complete the UAT</p>
                                                <p class="tab-content" style="padding-left: 20px">p.	The sign off is then obtained from all relevant stakeholders from the customer in order to close the UAT</p></td>
                                            <td>PM / SA / FL / TL / FC / TC / Customer’s Core Project team / SAP Security /  All relevant stakeholders from the customer</td>
                                            <td>Updated or New Functional Specification Document for Development or Configuration Objects / Updated Build Tracker / Updated Project Status Tracking Sheet / Updated Test Scripts with test results / Issue Log (optional) / CR Log (optional)</td>                                   
                                            <td>Updated Detailed Project Plan / Updated Testing Schedule / Test Scripts / Training Schedule / Training Materials</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>3.	Prepare Production System</p> 
                                                <p>Within this activity the following takes place: </p> 
                                                <p class="tab-content" style="padding-left: 20px">a.	The Final Transport Tracking sheet is confirmed with the TRs in the right sequence</p> 
                                                <p class="tab-content" style="padding-left: 20px">b.	The list of user IDs to be created with the relevant authorizations and accesses are prepared for the business users</p> 
                                                <p class="tab-content" style="padding-left: 20px">c.	The Production system is built and set up for Go Live</p> 
                                                <p class="tab-content" style="padding-left: 20px">d.	All connections to third party systems are set up from the Production system</p></td>
                                            <td>PM / FL / TL / SAP Security / Customer’s core project team </td>
                                            <td>Authorization Matrix and User ID List</td>                                    
                                            <td>Not available</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>4.	Conduct End User Training</p>
                                                <p>This task is to be completed by the Customer’s core project team with the support of the attune project team.</p>
                                                <p>The list of End Users are identified for each business process listed under each SAP module (as per the As-Is and To-Be process documents completed during the Blueprint Phase)</p>
                                                <p>The customer’s core project team member responsible for each module would take the identified End Users through the training.</p>
                                                <p>The training held would cover: </p>   
                                                <p class="tab-content" style="padding-left: 20px">a.	SAP Navigation</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The business processes covered within each SAP module (focus is on the To-Be business processes documents with the process flows as per the Business Blueprint Document)</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The features and functions within each business process</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The concept of master data, how it is created or maintained and how it is used</p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The concept of the transactional data and how transactions are entered in SAP for each step in the To-be process flow together with the main SAP transactions</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	Hands on training with access to SAP system (In general it is the Quality box that is used for this purpose)</p>
                                                <p>The customer’s core project team members would create the Training Schedule, send out the meeting invites to the teams, and conduct the training. </p>
                                                <p>An attune project team member from each module would be present to support the customer core team member to help answer any questions or provide clarifications</p>
                                                <p>The Training manuals already in place would be updated by the Customer core team members in order to fine tune the documents to cover all the To-Be business processes</p></td>
                                            <td>Customer’s Core project team / FC / SAP Security</td>
                                            <td>End User Training Schedule / End User Training Materials</td>                                    
                                            <td>Training Schedule used for testing team and core team training / Training Materials used for testing team and core team training / Business Blueprint Document / To-Be Business Process Documents with process flow diagrams</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>5.	Mock Cutover 2 (Dry Run)</p>
                                                <p>This involves a trial run of the activities to be performed during Go Live. The following activities are completed here:</p>
                                                <p class="tab-content" style="padding-left: 20px">a.	The Cutover Plan which consists of all the activities to be performed during Go Live is prepared in the right sequence</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	A copy of the Production system is set up</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The data, both the master data and the transaction data to be migrated to the Production system are identified.</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The date is prepared into the formats required for the Data Conversion Tools (the same tools used during Mock Cutover 1 during the Build phase) </p>
                                                <p class="tab-content" style="padding-left: 20px">e.	The data are reviewed and validated by the relevant stakeholders from the customer</p>
                                                <p class="tab-content" style="padding-left: 20px">f.	The TRs listed in the Transport Request Tracking sheet are moved into the copy of the Production System</p>
                                                <p class="tab-content" style="padding-left: 20px">g.	With the above step, all of the objects both configuration and development, are available in the copy of the Production System</p>
                                                <p class="tab-content" style="padding-left: 20px">h.	The validated data are loaded into the copy of the Production System starting with the master data and then moving to the transactional data</p>
                                                <p class="tab-content" style="padding-left: 20px">i.	All of the loaded configuration objects, development objects, the migrated data are then validated and confirmed</p>
                                                <p class="tab-content" style="padding-left: 20px">j.	Once it is confirmed that everything is in order, the Dry Run or Mock Cutover 2 is considered complete</p>
                                                <p class="tab-content" style="padding-left: 20px">k.	If any unexpected issues arise, these are discussed and the appropriate actions are taken to resolve them</p>
                                                <p class="tab-content" style="padding-left: 20px">l.	These issues and actions taken are documented in the Project Status Tracking Sheet</p>
                                                <p class="tab-content" style="padding-left: 20px">m.	The status and the results of the Dry Run is then shared with all relevant stakeholders of the customer, the customer’s core project team and, if required, the customer’s and attune management team</p>
                                                <p class="tab-content" style="padding-left: 20px">n.	The timing taken to complete the Dry Run is captured and the Detailed Project Plan is updated with the timeline</p>
                                                <p class="tab-content" style="padding-left: 20px">o.	The Cutover Plan is then confirmed and shared with all relevant stakeholders from the customer and attune, the customer’s and attune project teams</p></td>
                                            <td>PM  / SA / FL / TL / FC / TC / SAP Security /  Customer’s Core Project team / All relevant stakeholders from Customer</td>
                                            <td>Cutover Plan / Updated Project Status Tracking Sheet / Updated Detailed Project Plan / Meeting Minutes of discussions taken place to discuss and overcome issues faced during the Dry Run</td>                                    
                                            <td>Detailed Project Plan / Project Status Tracking Sheet / Project Charter </td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>6.	Assess Organizational readiness (Optional)</p>
                                                <p>This tasks is performed by the customer Management team together with the project PMO. This involves: </p>
                                                <p class="tab-content" style="padding-left: 20px">a.	Assessing the readiness of the organization in going live in the new system (and decommissioning the legacy system where applicable)</p>
                                                <p class="tab-content" style="padding-left: 20px">b.	The methods to be used to communicate the impending Go Live are discussed and agreed upon</p>
                                                <p class="tab-content" style="padding-left: 20px">c.	The Project Steering committee is given the  responsibility for this activity</p>
                                                <p class="tab-content" style="padding-left: 20px">d.	The timelines for when the communications are to be sent out are defined</p></td>
                                            <td>PGM / PM / Customer Core Project team / The customer’s Management team / Steering Committee</td>
                                            <td>Meeting Minutes of discussions held </td>                                    
                                            <td>Updated Detailed Project Plan / Project Charter / Project Kick Off presentation / Updated Project Status Tracking Sheet</td>
                                        </tr>
                                        <tr class="">
                                            <%--<td></td>--%>
                                            <td><p>7.	Prepare Organization Communication (Optional)</p>
                                                <p>In this task, the communications are prepared and shared in order to pass the message to everyone in the organization during the decided timeline on the impending Go Live and the support of the whole organization is garnered in order to ensure the Go Live is a success</p></td>
                                            <td>Customer Management Team / Project Steering Committee</td>
                                            <td>Visual / Email Communications</td>                                    
                                            <td></td>
                                        </tr>
                                                                                                                                                                                                                                                                                      
                                    </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>


                        <div class="table-data-sep">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                <tbody>
                                    <tr>
                                    <%--<th>1</th>--%>
                                    <td>
                                        <h3>Entry Criteria</h3>
                                        <div class="data">
                                            <p>•	Complete, updated and approved Functional and Technical Specification documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completely Unit Tested configured and developed objects</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed System with all configurations and developments</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Exit Criteria</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Completed Integration Testing </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Completed UAT</p>
                                            <p class="tab-content" style="padding-left: 20px">•	UAT Sign Off</p>
                                        </div>
                                    </td>
                                    </tr>
                                    <tr class="loop">
                                    <td>
                                    <h3>Input Documents</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Business Blueprint Document</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Detailed Project Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	RICEF Object List</p>
                                            <p class="tab-content" style="padding-left: 20px">•	List of Objects to be configured</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Approved Functional and Technical Specification Documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Testing and UAT Test Scripts</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Testing and UAT Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Testing team Training Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Build Tracker</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Transport Request Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Project Status Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Training Materials</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Artifacts Created and updated</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Testing Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated or New Functional and Technical Specification Documents</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Detailed Project Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Build Tracker </p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Transport Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Integration Testing and UAT Issue Log/ CR Log</p>
                                            <p class="tab-content" style="padding-left: 20px">•	End User Training Schedule</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Test Scripts with test results</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Updated Project Status Tracking Sheet</p>
                                            <p class="tab-content" style="padding-left: 20px">•	All required End User Training Materials</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Cutover Plan</p>
                                            <p class="tab-content" style="padding-left: 20px">•	User ID List and Authorization Matrix</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Meeting Minutes of discussion held</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Visual / Email Communications (Optional)</p>
                                        </div>
                                    </td>
                                    </tr>           
                                    <tr class="loop">
                                    <td>
                                    <h3>Measurement of Improvement</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	Coverage of Integration Testing and UAT (covering all To-Be business processes)</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h3>Tailoring</h3>
                                        <div class="data">
                                            <p class="tab-content" style="padding-left: 20px">•	The project may decide to use client defined templates and standards for the deliverables from within the Blueprinting phases. In this case, the PM shall map client defined templates to attune templates and ensure that all required aspects of attune templates and procedures are covered by the client’s templates. This tailoring is documented in the Project’s Process.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	Certain tasks of this process may not be applicable in certain projects, as per the contractual requirements. Such non-applicability is documented when applicable.</p>
                                            <p class="tab-content" style="padding-left: 20px">•	In case the client or another team representing the client is performing some parts of the tasks and providing deliverables to the project team, these deliverables are reviewed for completeness. Handling of client supplied deliverables for each phase or activity is defined as Customer Deliverables in the Project Charter Document. </p>
                                        </div>
                                    </td>
                                    </tr>                                                           
                                </tbody>
                                </table>
                            </div>
                        </div>

                        <h2>Document Templates</h2>                       
<%--                        <p><a href="References\SAP Templates\attune Business Blueprint Template.docx">1. attune Business Blueprint</a></p>
                        <p><a href="References\SAP Templates\attune Integration Test Template.xls">2. attune Integration Test Template</a></p>   --%>                                
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->


</asp:Content>



